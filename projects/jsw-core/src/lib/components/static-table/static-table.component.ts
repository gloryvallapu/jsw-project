import { Component, OnInit,Input } from '@angular/core';
import { ColumnType } from  '../../enum/common-enum';
@Component({
  selector: 'lib-static-table',
  templateUrl: './static-table.component.html',
  styleUrls: ['./static-table.component.css']
})
export class StaticTableComponent implements OnInit {
 @Input() columns: any = [];
 @Input() tableData:any;
 @Input() styleClass: any;
 public columnType = ColumnType;
  constructor() { }

  ngOnInit(): void {
  }

}
