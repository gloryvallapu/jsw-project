import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { JswCoreService } from '../../jsw-core.service';
import { FieldType } from  '../../enum/common-enum';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'lib-jsw-form',
  templateUrl: './jsw-form.component.html',
  styleUrls: ['./jsw-form.component.css'],
})
export class JswFormComponent implements OnInit {
  @ViewChild('jswForm') viewChildForm: NgForm;
  @Input() formObject: any;
  @Output() changeEvent = new EventEmitter<Event>();
  @Output() setForm = new EventEmitter<any>();
  public fieldType = FieldType;

  constructor(public jswService: JswCoreService) {}

  ngOnInit() {}

  ngAfterViewInit(){
    this.setForm.emit(this.viewChildForm);
  }

  public dataValidation(fieldType: any, item:any, value: any, index: any, event: any) {
      switch(fieldType){
        case this.fieldType.number:
          if(item.minLength == null && item.maxLength ==null){
            this.setFalseErrorStatus(item, value, index);
            break;
          }
          if(value != null && value != '' && item.minLength > 0){
            if(value.toString().length == item.minLength){
              this.setFalseErrorStatus(item, value, index);
            }
           else if(item.maxLength > 0){
            if(value.toString().length <= item.maxLength && value.toString().length >= item.minLength){
              this.setFalseErrorStatus(item, value, index);
            }
            else {
              this.formObject.formFields[index].isError = true;
              this.formObject.formFields[index].errorMsg = 'Please enter ' + (item.minLength != item.maxLength ? 'minimum ' + item.minLength : item.minLength) + ' digits';
            }
              
            }else {
              this.formObject.formFields[index].isError = true;
              this.formObject.formFields[index].errorMsg = 'Please enter ' + (item.minLength != item.maxLength ? 'minimum ' + item.minLength : item.minLength) + ' digits';
            }
          }else if(this.formObject.formFields[index].isRequired) {
            this.setTrueErrorStatus(item, value, index);
          }
          break;

        case this.fieldType.numberMask:
          if(value != null && value != '' && item.minLength > 0){
            if(value.length == item.minLength){
              this.setFalseErrorStatus(item, value, index);
            }else {
              this.formObject.formFields[index].isError =  true;
              this.formObject.formFields[index].errorMsg = 'Please enter ' + item.minLength + ' digits';
            }
          }else if(this.formObject.formFields[index].isRequired) {
            this.setTrueErrorStatus(item, value, index);
          }
          break;

        case this.fieldType.textWithFilter:
          if(value == null || value == ''){
            this.setTrueErrorStatus(item, value, index);
          }else if(this.formObject.formFields[index].isRequired){
            this.setFalseErrorStatus(item, value, index)
          }
          break;

        case this.fieldType.text:
          if(value != null && value != ''){
            this.setFalseErrorStatus(item, value, index);
          }else if(this.formObject.formFields[index].isRequired){
            this.setTrueErrorStatus(item, value, index);
          }
          break;

        case this.fieldType.email:
          if(value == null || value == ''){
            this.setTrueErrorStatus(item, value, index);
          }else if(this.formObject.formFields[index].isRequired){
            this.setFalseErrorStatus(item, value, index)
          }
          break;

        case this.fieldType.textarea:
          if(value == null || value == ''){
            this.setTrueErrorStatus(item, value, index);
          }else{
            this.setFalseErrorStatus(item, value, index)
          }
          break;

        case this.fieldType.dropdown:
          if(item.changeEvent == 'dropdownChange'){
            this.changeEvent.emit(item);
          }
          if(value != null && value != ''){
            this.setFalseErrorStatus(item, value, index);
          }else if(this.formObject.formFields[index].isRequired){
            this.setTrueErrorStatus(item, value, index);
          }
          break;

        case this.fieldType.multiselect:
          if(item.changeEvent == 'multiSelectchange'){
            this.changeEvent.emit(item);
          }
          if(value != null && value != ''){
            this.setFalseErrorStatus(item, value, index);
          }
         else if(item.selectedItems.length){
            this.setFalseErrorStatus(item, value, index);
          }
          else if(this.formObject.formFields[index].isRequired){
            this.setTrueErrorStatus(item, value, index);
          }
          break;


        case this.fieldType.calendar:
          if(value != null && value != ''){
            this.setFalseErrorStatus(item, value, index);
            if(item.changeEvent == 'startDate'){
              this.validateStartAndEndDate(this.formObject.formFields[index].ref_key, this.formObject.formFields[index+1].ref_key, this.formObject.formFields);
            }else if(item.changeEvent == 'endDate'){
              this.validateStartAndEndDate(this.formObject.formFields[index-1].ref_key, this.formObject.formFields[index].ref_key, this.formObject.formFields);
            }
          }else if(this.formObject.formFields[index].isRequired){
            this.setTrueErrorStatus(item, value, index)
          }
          break;

        default: break;
      }
  }

  public setTrueErrorStatus(item:any, value: any, index: any){
    this.formObject.formFields[index].isError = true;
    this.formObject.formFields[index].errorMsg = 'Required';
  }

  public setFalseErrorStatus(item:any, value: any, index: any){
    this.formObject.formFields[index].isError = false;
    this.formObject.formFields[index].errorMsg = '';
  }

  public textValidation(event: any, value: any, index: any) {
    if(value == null || value == ''){
      this.formObject.formFields[index].isError = true;
      this.formObject.formFields[index].errorMsg = 'Required';
    } else {
      this.formObject.formFields[index].isError = false;
      this.formObject.formFields[index].errorMsg = '';
    }
  }

  public validateRequiredField(formObject: any) {
    formObject.filter((ele: any) => {
      if (!ele.isError && ele.isRequired) {
        if ((ele.fieldType != this.fieldType.multiselect && ele.value == null) || (ele.fieldType == this.fieldType.multiselect && !ele.selectedItems.length)) {
          ele.isError = true;
          ele.errorMsg = 'Required';
        } else {
          ele.isError = false;
          ele.errorMsg = '';
        }
      }
    });
    return formObject;
  }

  public validateStartAndEndDate(startDateRefKey: any, endDateRefKey: any, formObject: any) {
    let startDate = formObject.filter((ele: any) => ele.ref_key == startDateRefKey)[0].value;
    let endDate = formObject.filter((ele: any) => ele.ref_key == endDateRefKey)[0].value;
    if (startDate && endDate) {
      if (new Date(startDate) > new Date(endDate)) {
        formObject.filter((ele: any) => ele.ref_key == endDateRefKey)[0].isError = true;
        formObject.filter((ele: any) => ele.ref_key == endDateRefKey)[0].errorMsg = 'To Date Should be Greater than From Date';
      } else {
        formObject.filter((ele: any) => ele.ref_key == endDateRefKey)[0].isError = false;
        formObject.filter((ele: any) => ele.ref_key == endDateRefKey)[0].errorMsg = '';
      }
    }
    //return formObject.filter((ele: any) => ele.endDateRefKey == endDateRefKey)[0].isError;
  }

  public isErrorCheck(formObject: any) {
    let errObj = formObject.find((ele: any) => ele.isError == true);
    if (errObj) {
      return true;
    } else {
      return false;
    }
  }

  public onSubmit(form: any, formData: any) {
    if(this.isErrorCheck(formData)){
      this.jswService.setFormData(undefined);
    }else{
      let fData = this.validateRequiredField(formData);
      if(this.isErrorCheck(fData)){
        this.jswService.setFormData(undefined);
      }else{
        this.jswService.setFormData(fData);
      }
    }
  }

  public resetForm(formObject: any, formRef?: NgForm) {
    if(formRef) formRef.reset();
    formObject.formFields.map((ele: any,index:any) => {
      (<HTMLInputElement>document.getElementById(`${index}_${ele.ref_key}`)).classList.remove("ng-dirty")
      // if(!formRef){
      //   (<HTMLInputElement>document.getElementById(`${index}_${ele.ref_key}`)).classList.remove("ng-dirty")
      // }
      ele.value = null;
      ele.isError = false;
      ele.errorMsg = '';
      if(ele.fieldType == this.fieldType.multiselect){
        ele.selectedItems = [];
      }
    });
  }

  // public reset(){
  //   (<HTMLFormElement>document.getElementById("jswFo")).reset();
  // }
}
