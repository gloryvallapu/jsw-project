import { ComponentFixture, TestBed } from '@angular/core/testing';
import {JswCoreService} from '../../jsw-core.service';
import { JswFormComponent } from './jsw-form.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
describe('JswFormComponent', () => {
  let component: JswFormComponent;
  let fixture: ComponentFixture<JswFormComponent>;
  let jswCoreService :JswCoreService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JswFormComponent ],
      providers:[JswCoreService],
      imports:[FormsModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JswFormComponent);
    component = fixture.componentInstance;
    component.formObject = { formName:"", formFields:""}
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
