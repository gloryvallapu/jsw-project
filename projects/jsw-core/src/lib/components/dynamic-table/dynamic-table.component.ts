import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ColumnType } from  '../../enum/common-enum';
import { ConfirmationService } from 'primeng/api';
import { Table } from 'primeng/table/table';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'lib-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.css'],
  providers: [ConfirmationService]
})
export class DynamicTableComponent implements OnInit {
  @Input() noOfRows:any;
  @Output() openModalEvent = new EventEmitter<boolean>();
  @Input() createButton : boolean
  @Input() reapplyButton : boolean
  @Input() saveButton : boolean
  @Input() columns: any = [];
  @Input() tableData: any = [];
  @Input() styleClass: any;
  @Input() globalFilterArray: any = [];
  @Input() isEditAction: boolean;
  @Input() isDeleteAction: boolean;
  @Input() isMultiDelete: boolean;
  @Input() showGlobalFilter: boolean;
  @Input() disableFields: boolean;
  @Output() editedTableData = new EventEmitter<Event>();
  @Output() getRow = new EventEmitter<Event>();
  @Output() btnClickEvent = new EventEmitter<Event>();
  @Output() editEvent = new EventEmitter<Event>();
  @Output() deleteEvent = new EventEmitter<any>();
  @Output() deleteMultipleEvent = new EventEmitter<any>();
  @Output() numericValidationEvent = new EventEmitter<any>();
  @Output() dropdownEvent = new EventEmitter<Event>();
  @Output() sentToEvent = new EventEmitter<Event>();
  @Output() SAPEvent = new EventEmitter<Event>();
  @Input() clearSelectionEvent: Observable<boolean>;
  @Input() isClearSelection: boolean;
  public columnType = ColumnType;
  public selectedRecords: any = [];
  @ViewChild('dt') table: Table;
  @Input() paginator: boolean;
  @Input() isRefreshBtn: boolean;
  @Input() isReleaseBtn: boolean;
  @Input() sentToStock:boolean;
  @Input() sentToSAP:boolean;
  @Input() btnName: string;
  @Output() setTable = new EventEmitter<any>();

  @Input() frozenCols: any = [];
  @Input() frznWidth: any = null;
  @ViewChild('dt') tableElement:ElementRef;
  @Input() first:number;
  @Input() childNumOfRows: any = null;
  @Input() tdClass: any;
  private eventSubscription: Subscription;

  constructor(private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.first = this.first == undefined ? 0 : this.first;
    if(this.childNumOfRows){
      this.noOfRows = this.childNumOfRows;
    }
    this.noOfRows = this.noOfRows == undefined ? 10 : this.noOfRows;
    this.paginator = this.paginator == false ? this.paginator : true;
    if(this.isClearSelection){
      this.eventSubscription = this.clearSelectionEvent.subscribe((data) =>{
        this.selectedRecords = [];
      });
    }
  }

  ngAfterViewInit(){
    this.setTable.emit(this.table);
    // if(this.frozenCols.length){
    //   this.tableElement.nativeElement.setAttribute('frozenColumns',this.frozenCols);
    //   this.tableElement.nativeElement.setAttribute('frozenWidth',this.frznWidth);
    // }
  
    
  }

  ngOnDestroy() {
    if(this.isClearSelection){
      this.eventSubscription.unsubscribe();
    }
  }

  public saveData(){
     this.editedTableData.emit(this.tableData)
  }

  public showDialog(){
    this.openModalEvent.emit(true)
  }

  public btnClicked(event){
    if(event.target.name=='release')
    {
      this.btnClickEvent.emit(event)
    }
    else if(event.target.name=='refresh')
    {
      this.btnClickEvent.emit(event)
    }
  }

  public sentToClick(event){
    this.sentToEvent.emit(event)

  }

  public SAPToClick(event){
    this.SAPEvent.emit(event)
  }
  public deleteRecords(data: any) {
    let key = this.columns.filter(ele => ele.columnType == this.columnType.uniqueKey)[0].field;
    this.confirmationService.confirm({
        message: '<span class="font-16 text-grey">Do you want to delete - ' + data[key] + ' record ?</span>',
        header: 'Delete Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
          this.deleteEvent.emit(data);
        },
        reject: () => {

        }
    });
  }

  public deleteMultipleRecords(data: any){
    this.confirmationService.confirm({
      message: '<span class="font-16 text-grey">Do you want to delete selected records?</span>',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.deleteMultipleEvent.emit(data);
      },
      reject: () => {
        this.selectedRecords.length = 0;
      }
    });
  }


  public gerRowOnclick(data:any,actionItem:any){
    if(actionItem == 'radio'){
      delete data.radio
      this.getRow.emit(data)
    }
    if(actionItem == 'checkbox')
    {this.getRow.emit(this.selectedRecords)}

    if(actionItem == 'hyperlink')
    {this.getRow.emit(data)}
  }

  public editRecords(data: any){
    this.editEvent.emit(data);
  }

  public filterTable(event: any){
    this.table.filterGlobal(event.target.value, 'contains');
  }

  public validateNumericField(rowData, colField, rowIndex){
    let obj = {
      rowData: rowData,
      colField: colField,
      rowIndex: rowIndex,
      tableData:this.tableData
    }
    this.numericValidationEvent.emit(obj);
  }

  public onDropdownChange(rowData){
    this.dropdownEvent.emit(rowData);
  }
}
