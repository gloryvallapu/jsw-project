import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ComponentType } from './enum/common-enum';
import { Observable, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'lib-jsw-core',
  templateUrl: './jsw-core.component.html',
  styles: [
  ]
})
export class JswCoreComponent implements OnInit {
  @Input() componentType:any;

  //form variable declarations
  @Input() formData:any;

  //static/dynamic table variable declarations
  @Input() noOfRows:any;
  @Input() paginator: boolean;
  @Output() openModalEvent = new EventEmitter<any>();
  @Input() saveButton: boolean;
  @Input() createButton:any;
  @Input() reapplyButton:any;
  @Input() tableData:any;
  @Input() columns:any
  @Input() globalFilterArray: any;
  @Input() styleClass:any;
  @Input() showGlobalFilter: boolean;
  @Input() isDeleteAction: boolean;
  @Input() isMultiDelete: boolean;
  @Input() isEditAction: boolean;
  @Input() disableFields: boolean;
  @Output() editedTableData = new EventEmitter<Event>();
  @Output() editEvent = new EventEmitter<Event>();
  @Output() deleteEvent = new EventEmitter<any>();
  @Output() deleteMultipleEvent = new EventEmitter<any>();
  @Output() changeEvent = new EventEmitter<any>();
  @Output() getRow = new EventEmitter<Event>();
  @Output() btnClickEvent = new EventEmitter<Event>();
  @Output() numericValidationEvent = new EventEmitter<any>();
  @Output() dropdownEvent =  new EventEmitter<any>();
  @Output() sentToEvent = new EventEmitter<Event>();
  @Output() SAPEvent = new EventEmitter<Event>();
  @Output() ptableEvent = new EventEmitter<any>();
  @Output() jswFormEvent = new EventEmitter<any>();
  @Input() isRefreshBtn: boolean;
  @Input() isReleaseBtn: boolean;
  @Input() sentToStock:boolean;
  @Input() sentToSAP:boolean;
  @Input() isClearSelection: boolean;
  @Input() clearSelectionEvent: Observable<boolean>;

  @Input() frozenCols: any = [];
  @Input() frznWidth: any = '0px';
  @Input() first:number;
  @Input() childNumOfRows: any = null;
  @Input() tdClass: any;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public viewType = ComponentType;
  private eventSubscription: Subscription;

  constructor() { }

  ngOnInit(): void {
    if(this.isClearSelection){
      this.eventSubscription = this.clearSelectionEvent.subscribe((data) =>{
        this.clearSelection.next(true)
      });
    }
  }

  ngOnDestroy() {
    if(this.isClearSelection){
      this.eventSubscription.unsubscribe();
    }
  }

  public sendTableData(data:any){
    this.editedTableData.emit(data);
  }

  public getRowInfo(data: any){
    this.getRow.emit(data);
  }

  public btnClicked(data: any){
    this.btnClickEvent.emit(data);
  }

  public editRecord(data: any){
    this.editEvent.emit(data);
  }

  public confirmDelete(data: any){
    this.deleteEvent.emit(data);
  }

  public deleteMultipleRecords(data: any){
    this.deleteMultipleEvent.emit(data);
  }

  public triggerChangeEvent(data: any){
    this.changeEvent.emit(data);
  }

  public opneModalPopup(event:any){
    this.openModalEvent.emit(true)
  }

  public triggerNumValEvent(event: any){
    this.numericValidationEvent.emit(event);
  }

  public dropdownChangeEvent(event: any){
    this.dropdownEvent.emit(event);
  }
  public sentToClick(event){
    this.sentToEvent.emit(event)

  }
  public SAPToClick(event){
    this.SAPEvent.emit(event)
  }

  public tableEvent(event){
    this.ptableEvent.emit(event);
  }

  public formEvent(event){
    this.jswFormEvent.emit(event);
  }
}
