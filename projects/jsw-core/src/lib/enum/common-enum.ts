export enum ComponentType {
  form = 'Form',
  staticTable = 'StaticTable',
  dynamicTable = 'DynamicTable'
}

export enum ColumnType {
  number = 'Number',
  string = 'String',
  date = "Date",
  checkbox = 'Checkbox',
  text = "Text",
  dropdown = "Dropdown",
  tooltip = "Tooltip",
  action = "Action",
  uniqueKey = "Key",
  hyperlink = "hyperlink",
  radio ="radio",
  textWithValidations = 'validationNumericField',
  numericValidationField = 'numericValidationField',
  textField = 'textField',
  checkboxField = 'checkboxField',
  calender='calender',
  ellipsis = 'ellipsis'
}

export enum FieldType {
  number,
  numberMask,
  textWithFilter ,
  text,
  email,
  dropdown,
  multiselect,
  calendar,
  textarea,
  checkbox,
  radio
}

