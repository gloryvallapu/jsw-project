import { TestBed } from '@angular/core/testing';

import { JswCoreService } from './jsw-core.service';

describe('JswCoreService', () => {
  let service: JswCoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JswCoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
