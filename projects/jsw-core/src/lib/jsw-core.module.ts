import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JswCoreComponent } from './jsw-core.component';
import { JswFormComponent } from './components/jsw-form/jsw-form.component';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputNumberModule } from 'primeng/inputnumber';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KeyFilterModule } from 'primeng/keyfilter';
import { FieldsetModule } from 'primeng/fieldset';
import { CalendarModule } from 'primeng/calendar';
import { MessageModule } from 'primeng/message';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputMaskModule } from 'primeng/inputmask';
import { StaticTableComponent } from './components/static-table/static-table.component';
import { DynamicTableComponent } from './components/dynamic-table/dynamic-table.component';
import { WordTruncatePipe } from './pipes/word-truncate.pipe';
import { TooltipModule } from 'primeng/tooltip';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AccordionModule } from 'primeng/accordion';
import { CardModule } from 'primeng/card';
import { ChartModule } from 'primeng/chart';
import { DialogModule } from 'primeng/dialog';
import { DragDropModule } from 'primeng/dragdrop';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { TabMenuModule } from 'primeng/tabmenu';
import { TabViewModule } from 'primeng/tabview';
import { TreeModule } from 'primeng/tree';

@NgModule({
  declarations: [JswCoreComponent, JswFormComponent, StaticTableComponent, DynamicTableComponent, WordTruncatePipe],
  // imports: [
  //   CommonModule,
  //   TreeModule,
  //   DialogModule,
  //   TabMenuModule,
  //   TableModule,
  //   DropdownModule,
  //   ButtonModule,
  //   InputTextModule,
  //   FormsModule,
  //   TabViewModule,
  //   KeyFilterModule,
  //   CalendarModule,
  //   DynamicDialogModule,
  //   RadioButtonModule,
  //   CheckboxModule,
  //   AccordionModule,
  //   ConfirmDialogModule,
  //   InputNumberModule,
  //   DragDropModule,
  //   MultiSelectModule,
  //   CardModule,
  //   JswCoreModule,
  //   ChartModule
  // ],
  imports: [
    CommonModule,
    MessageModule,
    CalendarModule,
    FieldsetModule,
    InputTextModule,
    CheckboxModule,
    ButtonModule,
    RadioButtonModule,
    InputTextareaModule,
    DropdownModule,
    MultiSelectModule,
    FormsModule,
    InputSwitchModule,
    InputNumberModule,
    KeyFilterModule,
    TableModule,
    InputMaskModule,
    TooltipModule,
    ConfirmDialogModule
  ],
  exports: [JswCoreComponent],
})
export class JswCoreModule {}
