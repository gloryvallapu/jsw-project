import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wordTruncate'
})
export class WordTruncatePipe implements PipeTransform {

  transform(value: string, args?: any): any {
    var str: string = value;
    var word = str.split(",").splice(0,1).join("");
    var wordLen = str.split(",").length - 1;
    var finalWord = wordLen > 0 ? (word + " +" + wordLen) : word;
    return finalWord;
  }

}
