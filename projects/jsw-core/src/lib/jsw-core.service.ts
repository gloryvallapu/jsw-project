import { Injectable } from '@angular/core';@Injectable({
  providedIn: 'root'
})
export class JswCoreService {
  public formData:any

  constructor() { }

  public setFormData(data:any){
    this.formData = data;
  }

  public getFormData(){
    return this.formData;
  }
}
