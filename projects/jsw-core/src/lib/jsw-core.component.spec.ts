import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JswCoreComponent } from './jsw-core.component';

describe('JswCoreComponent', () => {
  let component: JswCoreComponent;
  let fixture: ComponentFixture<JswCoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JswCoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JswCoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
