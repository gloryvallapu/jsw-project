import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { JswCoreModule } from 'jsw-core';
import { JswFormComponent } from 'jsw-core';
import { API_Constants } from './shared/constants/api-constants';
import { LoaderInterceptor } from './shared/interceptors/loader.interceptor';
import { HttpErrorInterceptor } from './shared/interceptors/http-error.interceptor';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { CommonAPIEndPoints } from './shared/constants/common-api-end-points';


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    JswCoreModule
  ],
  exports: [JswCoreModule],
  providers: [
    JswFormComponent,
    API_Constants,
    CommonAPIEndPoints,
    { provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent],
})
export class AppModule {}
