export interface UserDetails {
  username: string;
  userId: string;
  firstName: string;
  emailId: string;
  currentRole: string;
  assignedModules: Array<any>;
}
