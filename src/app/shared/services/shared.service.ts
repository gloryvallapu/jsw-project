import { formatDate } from '@angular/common';
import { Injectable } from '@angular/core';
import { JswFormComponent } from 'jsw-core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { DynamicReportsForm } from 'src/app/layout/modules/reports/form-objects/reports-form-objects';
import { ToastrType, FieldType } from 'src/assets/enums/common-enum';
import { UserDetails } from '../interfaces/common-interfaces';
@Injectable({
  providedIn: 'root',
})
export class SharedService {
  //new trial for multiple error
  public errorAuthCount = 0;
  //end
  public toastType = ToastrType;

  //to update menu list as per user rights after login and access it
  public menuList = new BehaviorSubject<Array<any>>([]);
  public getMenuList = this.menuList.asObservable();
  public setMenuList(data: any) {
    this.menuList.next(data);
  }
  public loggedInUserDetails: UserDetails = {
    userId: 'JSW1',
    username: 'JSW1',
    firstName: '',
    emailId: '',
    currentRole: '',
    assignedModules: []
  };

  public reportFormats: any = {
      excel:'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8',
      pdf:'application/pdf'
  }

  constructor(public toastr: ToastrService, public jswComponent:JswFormComponent,) {}

  public displayToastrMessage(toastrType: any, toastrDetails: any) {
    switch (toastrType) {
      case this.toastType.Success: {
        return this.toastr.success(toastrDetails.message, 'Success!');
      }
      case this.toastType.Info: {
        return this.toastr.info(toastrDetails.message, 'Info!');
      }
      case this.toastType.Warning: {
        return this.toastr.warning(toastrDetails.message, 'Warning!');
      }
      case this.toastType.Error: {
        if (typeof toastrDetails == 'object') {
          return this.toastr.error(toastrDetails.message, 'Error!');
        } else if (
          typeof JSON.parse(toastrDetails) == 'object' &&
          JSON.parse(toastrDetails).message
        ) {
          return this.toastr.error(JSON.parse(toastrDetails).message, 'Error!');
        } else {
          return this.toastr.error(
            'An error has occured. Please contact administrator.',
            'Error!'
          );
        }
      }
      default:
        return 0;
    }
  }

  public downloadCSV(data: any, fileName: string, tableHeader: any) {
    import('xlsx').then((xlsx) => {
      const ws = xlsx.utils.book_new();
      xlsx.utils.sheet_add_aoa(ws, tableHeader);
      const worksheet = xlsx.utils.sheet_add_json(ws, data, { origin: 'A2', skipHeader: true });
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(excelBuffer, fileName);
    });
  }

  // To Map Form Object to the request body of the API
  public mapFormObjectToReqBody(formObject: any, requestObj: any) {
    var isEmptyObject = requestObj ? this.isEmpty(requestObj) : true;
    if(!isEmptyObject){
      var requestObjKeys = Object.keys(requestObj);
      formObject.forEach((formElement: any) => {
        var reqObjKey = requestObjKeys.find(key => key === formElement.ref_key);
        if (reqObjKey) {
          if(formElement.fieldType == FieldType.multiselect){
            requestObj[reqObjKey] = formElement.selectedItems;
          }         
          else{
            requestObj[reqObjKey] = formElement.value;
          }
        }
      });
      return requestObj;
    }else{
      return requestObj;
    }
  }

  // To map response or data sent by an API to form object
  public mapResponseToFormObj(formObject: any, responseObj: any){
    var isEmptyObject = responseObj ? this.isEmpty(responseObj) : true;
    if (!isEmptyObject) {
      var formObjectArray: any = [];
      var responseObjKeys = Object.keys(responseObj)
      formObject.forEach((formElement: any) => {
        var responseKey = responseObjKeys.find(key => key == formElement.ref_key);
        if(responseKey) {
          var value = responseObj[responseKey];
          if(formElement.fieldType == FieldType.calendar){
            formElement.value = value ? new Date(value) : null;
          }
          // else if(formElement.fieldType == FieldType.dropdown){
          //   formElement.list.forEach((ele: any) => {
          //       formElement.value=ele.modelValue;
          //     // else
          //     // {
          //     //   formElement.value=value;
          //     // }
          //   });
          // }
          else{
            formElement.value = value;
          }
          formObjectArray.push(formElement);
        }
      });
      return formObjectArray;
    } else {
      return formObject;
    }
  }


  public isEmpty(obj: any) {
    return Object.keys(obj).length === 0;
  }

  public saveAsExcelFile(buffer: any, fileName: string): void {
    let date = formatDate(new Date(), 'yyyy/MM/dd hh:mm:ss', 'en');
    import("file-saver").then(FileSaver => {
        let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        let EXCEL_EXTENSION = '.xlsx';
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '_' + date + EXCEL_EXTENSION);
    });
  }

  public scroll(el: HTMLElement) {
    el.scrollIntoView();
  }

  public generateUniqueId() {
    // Math.random should be unique because of its seeding algorithm.
    // Convert it to base 36 (numbers + letters), and grab the first 9 characters after the decimal.
    return Math.random().toString(36).substr(2, 9);
  }

  public setLoggedInUserDetails(data: UserDetails){
    this.loggedInUserDetails = data;
  }

  public getLoggedInUserDetails(){
    return this.loggedInUserDetails;
  }

  public getDropdownData(data: any, displayName: string, modelValue: string){
    let list: any = [];
    if(data && data.length){
      list = data.map((x:any) => {
        return {
          displayName: x[displayName],
          modelValue: x[modelValue]
        }
      })
    }
    return list;
  }

  get_MM_DD_YYYY_Date(date) {
    var date1 = new Date(date);
    var tempDate =
      (date1.getMonth() + 1 < 10 ? '0' + (date1.getMonth() + 1) : date1.getMonth() + 1) + '/' +
      (date1.getDate() < 10 ? '0' + date1.getDate() : date1.getDate()) + '/' + date1.getFullYear();
      return tempDate;
  }

  public getDate_MM_DD_YYYY_With_Time(date){
    var date1 = new Date(date);
    var tempDate = ((date1.getMonth() + 1) <= 9 ? "0" + (date1.getMonth() + 1) : (date1.getMonth() + 1)) + "/" +
                    (date1.getDate() <= 9 ? ("0" + date1.getDate()) : date1.getDate()) + "/" +
                    date1.getFullYear() + " " + (date1.getHours() <= 9 ? "0" + date1.getHours() : date1.getHours() + ":" + (date1.getMinutes() <= 9 ? "0" + date1.getMinutes() : date1.getMinutes()));
    return tempDate;
  }

  get_MM_DD_YYYY_withTime(date) {
    var m = new Date(date);
    var tempDate = m.getUTCFullYear() +"/"+ (m.getUTCMonth()+1) +"/"+ m.getUTCDate() + " " + m.getUTCHours() + ":" + m.getUTCMinutes() + ":" + m.getUTCSeconds();
    return tempDate;
  }

  getUniqueRecords(data){
    let uniqArr: any = [];
    if(data && data.length){
      uniqArr = [
        ...new Set(data.map((o) => JSON.stringify(o))),
      ].map((s: any) => JSON.parse(s));
    }
    return uniqArr;
  }

  sortData(data, sortBy){
    if(data && data.length){
      data.sort(function(a, b) {
        if (a[sortBy] < b[sortBy]) { return -1; }
        if (a[sortBy] > b[sortBy]) { return 1;  }
        // index must be equal
        return 0;
      });
    }
    return data;
  }

  public isButtonVisible(isSubScreenBtn: boolean, buttonId: string, subScreenId: number, screenStructure: any){
    if(isSubScreenBtn){// if sub section of the screen
      let subScreen = screenStructure.subScreens.filter(subScreen => subScreen.screenId == subScreenId); // check if subScreen i.e. parent of the button exists
      if(subScreen.length > 0){ // if buttons parent exists
        if(subScreen[0].buttons && subScreen[0].buttons.length > 0 && subScreen[0].buttons.filter(button => button == buttonId).length > 0){ // check if button exists under the list of buttons returned in the subScreen structure
          return true
        }else{
          return false;
        }
      }else {
        return false;
      }
    }else{// if main screen (common buttons at the screen level)
      if(screenStructure.buttons && screenStructure.buttons.length > 0 && screenStructure.buttons.filter(button => button == buttonId).length > 0){
        return true;
      }else{
        return false;
      }
    }
  }

  public isSectionVisible(subScreenId: number, screenStructure: any){
    if((screenStructure.subScreens.filter(subScreen => subScreen.screenId == subScreenId)).length > 0){
      return true
    }else {
      return false;
    }
  };

   // Function To getReport In System
  public generateReport(type,Response,ReportName) {
    let date = formatDate(new Date(), 'yyyy/MM/dd hh:mm:ss', 'en');
    if (type == 'excel') {
      let file = new Blob([Response], {
        type: this.reportFormats.excel,
      });
      var fileURL = URL.createObjectURL(file);
      //window.open(fileURL);
      var anchor = document.createElement("a");
      anchor.download = ReportName + '_' + date + '.xlsx';
      anchor.href = fileURL;
      anchor.click();
    };

    if (type == 'pdf') {
      let file = new Blob([Response], {
        type: this.reportFormats.pdf
      });
      var fileURL = URL.createObjectURL(file);
      //window.open(fileURL);
      var anchor = document.createElement("a");
      anchor.download = ReportName + '_' + date +'.pdf';
      anchor.href = fileURL;
      anchor.click();
    }
    if (type == 'png') {
      let file = new Blob([Response], {
        type: this.reportFormats.png
      });
      var fileURL = URL.createObjectURL(file);
      //window.open(fileURL);
      var anchor = document.createElement("a");
      anchor.download = ReportName + '_' + date +'.png';
      anchor.href = fileURL;
      anchor.click();
    }
  };
// End


// Function To build reqBody For Report
   public getReqBody(formObject):any{
    let getListOfKeys =formObject.map((objKeys)=>{return {[`${objKeys.ref_key}`]:objKeys.value}});
    let reqBody= getListOfKeys.reduce(function(result, item) {
        var key = Object.keys(item)[0];
        result[key] = item[key];
        return result;
      }, {});
     return reqBody;
    };
// End

  public getDataForExcelAsPerColKeys(data, keyArr){
    return data.map(ele => {
      return keyArr.reduce((acc, k) => ({ ...acc, ...(ele.hasOwnProperty(k) && {   [k]: ele[k] }) }), {});
    })
  }


  //reset form generic
  public getFormRef(event: any, formRefName: any){
    formRefName = event;
  }

  public resetForm(formObj: any, formRefName: any){
    this.jswComponent.resetForm(formObj, formRefName);
  }

};


