import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonAPIEndPoints } from '../constants/common-api-end-points';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommonApiService {

  constructor(public http: HttpClient, public commonEndPoints: CommonAPIEndPoints) { }

  //Common API calls
  public getWCBasedProductSizeList(){
    return this.http.get(`${this.commonEndPoints.getWCBasedProductSizeList}`).pipe(map((response: any) => response));
  }

  public getWorkCenterList(){
    return this.http.get(`${this.commonEndPoints.getWorkCenterList}`).pipe(map((response: any) => response));
  }

  public getWorkCenterListByUserId(){
    return this.http.get(`${this.commonEndPoints.getWorkCenterListByUserId}`).pipe(map((response: any) => response));
  }

  public getProductSizeList(){
    return this.http.get(`${this.commonEndPoints.getProductSizes}`).pipe(map((response: any) => response));
  }

  public getLovs(lovCode: any){
    return this.http.get(`${this.commonEndPoints.getLovs}/${lovCode}`).pipe(map((response: any) => response));
  }

 
  //Start Batch Transfer
public getBatchTransferList(wcName, heatNo) {
  return this.http
    .get(
      `${this.commonEndPoints.getBatchTransferList}/${wcName}/${heatNo}`
    )
    .pipe(map((response: any) => response));
}

public getYardList(wcName) {
  return this.http
    .get(`${this.commonEndPoints.getYards}/${wcName}`)
    .pipe(map((response: any) => response));
}

public batchTransfer(inTransitId, userId, reqBody) {
  return this.http
    .post(
      `${this.commonEndPoints.batchTransfer}/${inTransitId}/${userId}`,
      reqBody,
      { responseType: 'text' }
    )
    .pipe(map((response: any) => response));
}


public getFilterList(userId) {
  return this.http
    .get(`${this.commonEndPoints.getFilterList}/${userId}`)
    .pipe(map((response: any) => response));
}


//End Batch Transfer
}
