import { Injectable } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { moduleIds } from 'src/assets/constants/MENU/menu-list';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(public router: Router, public authService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
      if ((route.data.moduleId == 0 && !this.authService.token) || (route.data.moduleId != 0 && !this.authService.isAuthenticated(route.data.moduleId, route.data.screenId))) {
        this.router.navigate(['/user-auth']);
        return false;
      }
      return true;
 }
}
