export const alphaNumericWithSpace: RegExp = /^[a-zA-Z0-9 ]*$/;
export const threeDigitsAfterDecimal: RegExp = /^\d{1,3}(\.\d{1,3})?$/;
export const alphaWithSpace: RegExp = /^[a-zA-Z ]*$/;     // key filter to allow alphabets with space
