import { API_Constants } from 'src/app/shared/constants/api-constants';

export class CommonAPIEndPoints {
  public apiURL = new API_Constants();
  public masterBaseUrl = `${this.apiURL.baseUrl}${this.apiURL.masterGateWay}`;
  public readonly getLovs = this.masterBaseUrl + '/lovValues';
  public readonly getWorkCenterList = this.masterBaseUrl + '/workCenters';
  public readonly getWorkCenterListByUserId = this.masterBaseUrl + '/workCenters/getUserBasedWorkCenters';
  public readonly getProductSizes = this.masterBaseUrl + '/getAllSizesBasedOnPrd';
  public readonly getWCBasedProductSizeList = this.masterBaseUrl + '/getWCBasedProductSizeList';
 
  // Batch transfer screen End Points
  public batchTransferFilterList = `${this.apiURL.baseUrl}${this.apiURL.smsGateway}/batchTransfer/getFilterList`;
  public readonly getBatchTransferList=`${this.apiURL.baseUrl}${this.apiURL.smsGateway}/batchTransfer/getFilteredBatches`;
  public readonly getYards = `${this.apiURL.baseUrl}${this.apiURL.smsGateway}/batchTransfer/getYards`;
  public readonly batchTransfer=`${this.apiURL.baseUrl}${this.apiURL.smsGateway}/batchTransfer`;
  public readonly getFilterList = `${this.apiURL.baseUrl}${this.apiURL.smsGateway}/batchTransfer/getFilterList`;
 // End Of Batch Transfer Screen End Points
}
