export class API_Constants {
  // public readonly baseUrl = "http://10.181.0.44:8443";
  //public readonly baseUrl = "https://jswispllpmes-uat.jsw.in:8443";
  public readonly baseUrl = "https://jswispllpmes-qa.jsw.in:8452";
  // public readonly baseUrl = "http://localhost:9002";
  public readonly localUrl = "assets";
  public readonly masterGateWay = "/jispl/master"; //gateway for user/master modules
  public readonly ppcGateWay = "/jispl/ppc"; //gateway for ppc
  public readonly brmGateway = "/jispl/brm"; // gateway for brm
  public readonly smsGateway = "/jispl/brm"; // gateway for brm
  public readonly brmQaGateway = "/jispl/brm"; // gateway for brm
  public readonly interfaceGateway = "/jispl/interface"; // gateway for interface
  public readonly reportsGateway = "/jispl/report"; //gateway for reports
}
