import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { SharedService } from '../services/shared.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(private sharedService: SharedService, public router: Router) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //debugger
    return next.handle(request)
      .pipe(
       // retry(1),
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
            this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: errorMessage});
          } else {
            // server-side error
            let statusCode = error.error ? error.error.status : error.status;
            if(statusCode){
              errorMessage = `Error Code: ${statusCode} \n Message: ${error.error ? error.error.message : error.message}`;

              if(statusCode == 401 && this.sharedService.errorAuthCount == 0){
                //debugger
                this.sharedService.errorAuthCount = 1;
                this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: errorMessage});
                this.router.navigate(['/user-auth']);
              }
              else if(statusCode == 401 && this.sharedService.errorAuthCount == 1){
                this.router.navigate(['/user-auth']);
                //this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: errorMessage});
              }
              else{
                this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: errorMessage});
              }
            }else if(!statusCode && typeof error.error == 'string'){
              this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: error.error});
            }
          }
         // window.alert(errorMessage);
          //this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: errorMessage});
          return throwError(errorMessage);
        })
      )
  }
}
