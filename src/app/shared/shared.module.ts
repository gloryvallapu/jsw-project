import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './components/loader/loader.component';
import { MustMatchDirective } from './directives/must-match.directive';
import { TreeModule } from 'primeng/tree';
import { DialogModule } from 'primeng/dialog';
import { TabMenuModule } from 'primeng/tabmenu';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { AddRowDirective } from './directives/add-row.directive';
import { TabViewModule } from 'primeng/tabview';
import { KeyFilterModule } from 'primeng/keyfilter';
import { CalendarModule } from 'primeng/calendar';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { RadioButtonModule } from 'primeng/radiobutton';
import { AccordionModule } from 'primeng/accordion';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputNumberModule } from 'primeng/inputnumber';
import { DragDropModule } from 'primeng/dragdrop';
import { MultiSelectModule } from 'primeng/multiselect';
import { CardModule } from 'primeng/card';
import { WordTruncatePipe } from './pipes/word-truncate.pipe';
import { ChartModule } from 'primeng/chart';
import { JswCoreModule } from 'jsw-core';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  declarations: [LoaderComponent, MustMatchDirective, AddRowDirective, WordTruncatePipe],
  imports: [
    CommonModule,
    TreeModule,
    DialogModule,
    TabMenuModule,
    TableModule,
    DropdownModule,
    ButtonModule,
    InputTextModule,
    FormsModule,
    TabViewModule,
    KeyFilterModule,
    CalendarModule,
    DynamicDialogModule,
    RadioButtonModule,
    CheckboxModule,
    AccordionModule,
    ConfirmDialogModule,
    InputNumberModule,
    DragDropModule,
    MultiSelectModule,
    CardModule,
    JswCoreModule,
    ChartModule,
    TooltipModule
  ],
  exports: [
    LoaderComponent,
    MustMatchDirective,
    TreeModule,
    DialogModule,
    TabMenuModule,
    TableModule,
    DropdownModule,
    ButtonModule,
    InputTextModule,
    FormsModule,
    AddRowDirective,
    TabViewModule,
    KeyFilterModule,
    CalendarModule,
    DynamicDialogModule,
    RadioButtonModule,
    AccordionModule,
    InputNumberModule,
    CheckboxModule,
    ConfirmDialogModule,
    DragDropModule,
    MultiSelectModule,
    CardModule,
    JswCoreModule,
    WordTruncatePipe,
    ChartModule,
    TooltipModule
  ],
})
export class SharedModule {}
