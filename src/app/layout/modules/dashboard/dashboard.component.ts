import { Component, OnInit } from '@angular/core';
import { DashboardService } from './services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public shiftData: any = [];
  public tableType: any;
  public currentShiftName: any;
  public basicData: any;
  public basicOptions: any;

  constructor(public dashboardSvc: DashboardService) { }

  ngOnInit(): void {
    this.getShiftDetails();
    this.basicOptions = {
      legend: {
        labels: {
          fontColor: '#00008B'
        }
      },
      scales: {
        xAxes: [{
          ticks: {
            fontColor: '#00008B'
          },
          gridLines: {
            color: 'rgba(255,255,255,0.2)'
          }
        }],
        yAxes: [{
          ticks: {
            fontColor: '#00008B'
          },
          gridLines: {
            color: 'rgba(255,255,255,0.2)'
          }
        }]
      }
    };
  }

  public getShiftDetails() {
    this.dashboardSvc.getShiftDetails().subscribe(data => {
      this.currentShiftName = data.currentShiftName;
      this.shiftData = data.shiftWiseData;
      this.setGraphData();
    })
  }

  public setGraphData() {
    this.basicData = {
      labels: this.shiftData.map(ele => ele.statusType),
      datasets: [
        {
          label: 'Shift A',
          backgroundColor: '#42A5F5',
          data: this.getShiftData('A')
        },
        {
          label: 'Shift B',
          backgroundColor: '#FFA726',
          data: this.getShiftData('B')
        },
        {
          label: 'Shift C',
          backgroundColor: '#3BC942',
          data: this.getShiftData('C')
        }
      ]
    };
  }

  public getShiftData(shiftType) {
    if (shiftType == 'A') {
      return this.shiftData.map(ele => ele.shiftAQty)
      // let data = [10, 12, 12, 15, 8, 6]
      // return data
    }
    if (shiftType == 'B') {
      return this.shiftData.map(ele => ele.shiftBQty)
      // let data = [6, 8, 10, 3, 6, 16]
      // return data
    }
    if (shiftType == 'C') {
      return this.shiftData.map(ele => ele.shiftCQty)
      // let data = [9, 8, 13, 18, 7, 5]
      // return data
    }
    return []
  }
}
