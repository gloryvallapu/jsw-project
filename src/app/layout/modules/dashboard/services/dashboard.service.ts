import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { API_Constants } from 'src/app/shared/constants/api-constants';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  public baseUrl: string;
  constructor(public http: HttpClient, private apiURL: API_Constants) {
    this.baseUrl = `${this.apiURL.baseUrl}${this.apiURL.brmGateway}`;
  }

  public getShiftDetails(){
    return this.http
      .get(`${this.baseUrl}/shift/shiftWiseDetails`)
      .pipe(map((response: any) => response));
  }
}
