import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { PpcService } from '../../../services/ppc.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-order-book-table',
  templateUrl: './order-book-table.component.html',
  styleUrls: ['./order-book-table.component.css']
})
export class OrderBookTableComponent implements OnInit, OnDestroy {
  public columns: any = [];
  public tableType: any;
  public productList: any = [];
  public gradeList: any = [];
  public sizeList: any = [];
  public soIdList: any = [];
  public psnList: any = [];
  public customerList: any = [];
  public columnType = ColumnType;
  public selectedOrderlist: any = [];
  public selectedQuantity: any;
  public orderList: any = [];
  //@Input() public showCheckbox: boolean;
  @Input() public showSelectedQuantity: boolean;
  @Input() public scheduleType: any;
  @Input() public scheduleId: any;
  @Input() public scheduleAction: any;
  @Output() closeModalEvent = new EventEmitter<Event>();
  @Input() public isInsertBtn: boolean;
  public subscription: Subscription;
  public frozenCols: any =[];
  constructor(public ppcService: PpcService, public sharedService: SharedService) { }

  ngOnInit(): void {
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.frozenCols = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: ''
      },
      {
        field: 'soId',
        header: 'SO Id',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'lineNo',
        header: 'Line No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'productionOrderNo',
        header: 'Prod. Order No',
        columnType: ColumnType.number,
        width: '180px',
        sortFieldName: '',
      }
    ]

    this.columns = [
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: 'productType',
      },
      {
        field: 'psn',
        header: 'PSN',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Product Type',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: 'srNo',
      },
      {
        field: 'size',
        header: 'Size',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: 'soId',
      },
      {
        field: 'orderQty',
        header: 'Order Qty',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Length(mm)',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'lengthMin',
        header: 'Length Min(mm)',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'lengthMax',
        header: 'Length Max(mm)',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'supplyCondition',
        header: 'Supply Condition',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'customer',
        header: 'Customer',
        columnType: ColumnType.ellipsis,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'btc',
        header: 'BTC(Tons)',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'btr',
        header: 'BTR(Tons)',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'productQuantity',
        header: 'Product Qty',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'stockQuantity',
        header: 'Stock Qty',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'orderRemark',
        header: 'Order Remark',
        columnType: ColumnType.text,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'finalMaterial',
        header: 'Final Material',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'materialId',
        header: 'Material Id',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'equivalentSpecification',
        header: 'Equivalent Specification',
        columnType: ColumnType.number,
        width: '300px',
        sortFieldName: '',
      },
      {
        field: 'soReleaseDate',
        header: ' So Rel Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'reqDeliveryDate',
        header: 'Req Delivery Date',
        columnType: ColumnType.date,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'rollUnit',
        header: 'Roll Unit',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'destination',
        header: 'Destination',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'sizeTolarance',
        header: 'Size Tolerance',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'soDocumentType',
        header: 'So Document Type',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'endApplication',
        header: 'End Application',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'qaReject',
        header: 'QA Reject',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'processRoute',
        header: 'Process Route',
        columnType: ColumnType.ellipsis,
        width: '150px',
        sortFieldName: '',
      },
    ];
    this.subscription = this.ppcService.getOrderList.subscribe(data => {
      this.orderList = data;
      this.setFilters(data);
    })

  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }


  public setFilters(data){
    if(data && data.length > 0){
      this.productList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.productType, modelValue: x.productType };
      }));
      this.gradeList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.grade, modelValue: x.grade };
      }));
      this.sizeList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.size, modelValue: x.size };
      }));
      this.soIdList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.soId, modelValue: x.soId };
      }));
      this.psnList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.psn, modelValue: x.psn };
      }));
      this.customerList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.customer, modelValue: x.customer };
      }));
    }
  }

  public insertOrder():any{
    if(this.scheduleAction == 'viewInsert'){
      this.viewAndInsertOrders()
    }

    if(this.scheduleAction == 'addUnplanned'){
      this.addUnplannedOrders()
    }
  }

  public addUnplannedOrders():any{
    if(this.selectedOrderlist.length == 0){
      return this.sharedService.displayToastrMessage(
         this.sharedService.toastType.Warning, {
           message: 'Please select sales orders'
         }
       );
     }
    if(this.scheduleType == 'lp'){
      this.ppcService.insertUnplannedLpOrder(this.selectedOrderlist,this.scheduleId,this.sharedService.loggedInUserDetails.userId).subscribe(Response => {
        this.closeModalEvent.emit();
        this.selectedOrderlist = [];
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Selected orders inserted successfully'});

      })
    }

    if(this.scheduleType == 'caster'){
    //  API pending from Rashmi
      this.ppcService.insertUnPlannedOBToCaster(this.scheduleId, this.sharedService.loggedInUserDetails.userId, this.selectedOrderlist).subscribe(
        data => {
          this.closeModalEvent.emit();
          this.selectedOrderlist = [];
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Unplanned orders inserted successfully'});
        })
      }
    }

  public viewAndInsertOrders():any{
    if(this.selectedOrderlist.length == 0){
      return this.sharedService.displayToastrMessage(
         this.sharedService.toastType.Warning, {
           message: 'Please select orders'
         }
       );
     }
    if(this.scheduleType == 'caster'){
      this.ppcService.insertOrderBookToCasterSch(this.selectedOrderlist, this.scheduleId, this.sharedService.loggedInUserDetails.userId).subscribe(
        data => {
          this.selectedOrderlist = []
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Selected orders inserted successfully'});
          this.closeModalEvent.emit();
        }
      )
    }

    if(this.scheduleType == 'lp'){
      this.ppcService.insertLpOrderBook(this.selectedOrderlist,this.scheduleId,this.sharedService.loggedInUserDetails.userId).subscribe(Response => {
        this.closeModalEvent.emit();
        this.selectedOrderlist = [];
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Selected orders inserted successfully'});

      })
    }

  }

  public getTotalQuantity(){
    let initialValue = 0;
    if(this.selectedOrderlist && this.selectedOrderlist.length > 0){
       this.selectedQuantity = this.selectedOrderlist.reduce((total, item )=>{
          return Number(total) + Number(item.orderQty);}, initialValue).toFixed(2);
    }else{
      this.selectedQuantity = 0;
    }
  };

}
