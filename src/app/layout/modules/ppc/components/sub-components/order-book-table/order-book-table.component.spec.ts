import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { PpcService } from '../../../services/ppc.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Subscription } from 'rxjs';
import { OrderBookTableComponent } from './order-book-table.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConfirmationService } from 'primeng/api';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { OrderBookForms } from 'src/assets/constants/PPC/ppc-form-objects';
import { PpcEndPoints } from '../../../form-objects/ppc-api-endpoints';
import { BilletacknowledgementForms, BilletAttachForms, CasterScheduleForms, LongProductScheduleForms, PoamendForms, ReApplicationScreen, SoModificationFomrs } from '../../../form-objects/ppc-form-objects';
describe('OrderBookTableComponent', () => {
  let component: OrderBookTableComponent;
  let fixture: ComponentFixture<OrderBookTableComponent>;
  let ppcService: PpcService;
  let sharedService: SharedService;
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderBookTableComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        ConfirmationService,
        CommonApiService,
        API_Constants,
        CommonAPIEndPoints,
        OrderBookForms,
       PpcEndPoints,
       PpcService,
    CasterScheduleForms,
    LongProductScheduleForms,
    BilletacknowledgementForms,
    SoModificationFomrs,
    ReApplicationScreen,
    BilletAttachForms,
    PoamendForms,
    { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderBookTableComponent);
    component = fixture.componentInstance;
    ppcService = fixture.debugElement.injector.get(PpcService);
    sharedService = fixture.debugElement.injector.get(SharedService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
