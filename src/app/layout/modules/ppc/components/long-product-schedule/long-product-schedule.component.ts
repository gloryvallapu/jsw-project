import {
  Component,
  OnInit
} from '@angular/core';
import {
  LongProductScheduleForms
} from '../../form-objects/ppc-form-objects';
import {
  ITableHeader
} from 'src/assets/interfaces/table-headers';
import {
  TableType,
  ColumnType
} from 'src/assets/enums/common-enum';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  JswCoreService,
  JswFormComponent
} from 'jsw-core';
import {
  PpcService
} from '../../services/ppc.service';
import {
  ComponentType
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  CommonApiService
} from 'src/app/shared/services/common-api.service';
import {
  DialogService,
  DynamicDialogRef
} from 'primeng/dynamicdialog';
import {
  WorkCenter,
  Product,
  SizeCode,
  CommonAPIObject,
  LPAction,
  SchedulStatus
} from 'src/assets/enums/common-enum';
import {
  ViewAndInsertOrderbookComponent
} from '../caster-schedule/sub-components/view-and-insert-orderbook/view-and-insert-orderbook.component';
import {
  AddUnplannedHeatComponent
} from '../caster-schedule/sub-components/add-unplanned-heat/add-unplanned-heat.component';
import {
  ConfirmationService
} from 'primeng/api';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-long-product-schedule',
  templateUrl: './long-product-schedule.component.html',
  styleUrls: ['./long-product-schedule.component.css'],
  providers: [DialogService, ConfirmationService]
})
export class LongProductScheduleComponent implements OnInit {
  columns: ITableHeader[] = [];
  tableType: any;
  public insertOrderBookModalRef: DynamicDialogRef;
  public scheduleIdList: any = [];
  public selectedScheduleId: any;
  public scheduleStatus: any;
  public selectedAction: any;
  public actionList: any = [];
  public scheduleHeaderFormObject: any;
  public viewType = ComponentType;
  public newScheduleDetails: any;
  public ScheduleDetails: any;
  lpList: any = [];
  globalFilterArray: any = [];
  public productList: any = [];
  public workCenterList: any = [];
  addNewLPSchedule: any;
  public isSearchTriggered: boolean = false;
  public scheduleDetails: any;
  public scheduleList: any = [];
  public WCBasedProductSizeList: any;
  updtedSalesOrder: any = [];
  masterProductList: any;
  public disableSchActions: boolean = false;
  allProductList: any;
  scheduleStatusDisplayName: any;
  mappingProducts: any;
  deleteSalesOrder: any;
  public screenStructure: any = [];
  public isSearchBtn: boolean = false;
  public isProceedBtn: boolean = false;
  public isSaveSchHeaderBtn: boolean = false;
  public isResetSchHeaderBtn: boolean = false;
  public isSchListSection: boolean = false;
  public isSchListSaveBtn: boolean = false;
  public isViewInsertSection: boolean = false;
  public isAddUnplannedSection: boolean = false;
  public menuConstants = menuConstants;
  public validationFlags = {
    soWiseWcEnabled: false,
    plannedDateEnabled: false,
    cutLengthEnabled: false
  };
  public formRef: NgForm;

  //reference for lp list
  public reflpList: any;
  constructor(public dialogService: DialogService,
    public longProductScheduleForm: LongProductScheduleForms,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public ppcService: PpcService,
    public commonService: CommonApiService,
    private confirmationService: ConfirmationService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.LP_Schedule.id)[0];
    this.isSearchBtn = this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.Action_Line.buttons.search, menuConstants.lpSchSectionIds.Action_Line.id, this.screenStructure);
    this.isProceedBtn = this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.Action_Line.buttons.proceed, menuConstants.lpSchSectionIds.Action_Line.id, this.screenStructure);
    this.isSaveSchHeaderBtn = this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.Schedule_Header.buttons.save, menuConstants.lpSchSectionIds.Schedule_Header.id, this.screenStructure);
    this.isResetSchHeaderBtn = this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.Schedule_Header.buttons.reset, menuConstants.lpSchSectionIds.Schedule_Header.id, this.screenStructure);
    this.isSchListSection = this.sharedService.isSectionVisible(menuConstants.lpSchSectionIds.Schedule_List.id, this.screenStructure);
    this.isSchListSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.Schedule_List.buttons.save, menuConstants.lpSchSectionIds.Schedule_List.id, this.screenStructure);
    this.getMasterWCBasedProductSizeList();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.columns = this.createColumns()
    this.actionList = [{
      displayName: 'New Schedule',
      modelValue: LPAction.Create_Schedule
    }]
    this.selectedAction = this.actionList[0].modelValue;
    this.scheduleHeaderFormObject = JSON.parse(JSON.stringify(this.longProductScheduleForm.scheduleHeaderForm));
    this.getEnabledValidations();
    this.getWorkCenterList();
    this.getScheduleIds();
    this.getScheduleStatus();
    this.getWCBasedProductSizeList();
  }

  public getEnabledValidations(){
    this.ppcService.getLPValidations().subscribe(data => {
      this.validationFlags.plannedDateEnabled = data.plannedDateEnabled.toLowerCase() == 'y' ? true : false;
      this.validationFlags.soWiseWcEnabled = data.soWiseWcEnabled.toLowerCase() == 'y' ? true : false;
      this.validationFlags.cutLengthEnabled = data.cutLengthEnabled.toLowerCase() == 'y' ? true : false;
    })
  }

  public createColumns() {
    return [{
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'salesOrderNo',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'productionOrderNo',
        header: 'Production Order',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'product',
        header: 'Product',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'size',
        header: 'Size',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'equivalentSpec',
        header: 'Equivalent Specification (International Grade)',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'orderQty',
        header: 'Order Qty',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'producedQty',
        header: 'Produced Qty',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'balancedToRoll',
        header: 'Balance to Roll',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'inputProduct',
        header: 'Input Product',
        columnType: ColumnType.dropdown,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'inputSize',
        header: 'Input Size',
        columnType: ColumnType.dropdown,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'plannedPcs',
        header: 'Planned Qty (pcs)',
        columnType: ColumnType.textWithValidations,
        width: '200px',
        sortFieldName: '',
        isRequired: true,
        isError: true,
        errorMsg: 'Required'
      },
      {
        field: 'plannedQty',
        header: 'Planned Qty (tons)',
        columnType: ColumnType.textWithValidations,
        width: '200px',
        sortFieldName: '',
        isRequired: true,
        isError: true,
        errorMsg: 'Required'
      },
      {
        field: 'selectedPcs',
        header: 'Attached Qty (pcs)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'chargedPcs',
        header: 'Charged Qty (pcs)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'rolledPcs',
        header: 'Rolled Qty (pcs)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'cobblePcs',
        header: 'cobble Qty (pcs)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'hotoutPcs',
        header: 'Hotout Qty (PCS)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'finishedProductLengthMin',
        header: 'Finished Product Length Min (m)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'finishedProductLengthMax',
        header: 'Finished Product Length Max (m)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
    ];
  }

  public getProducts(selectedUnit) {
    this.productList = this.workCenterList.filter((product: any) => product.wcCode == selectedUnit)[0].productList;
    this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'productId')[0].list = this.sharedService.getDropdownData(
      this.productList,
      Product.displayName,
      Product.modelValue
    )
  }


  public getWCBasedProductSizeList() {
    this.commonService.getWCBasedProductSizeList().subscribe(Response => {
      this.workCenterList = Response.filter((ele: any) => ele.wcType == 'LP')[0].wcNameList;
      this.scheduleHeaderFormObject.formFields.filter(
        (ele) => ele.ref_key == 'schdWC'
      )[0].list = this.sharedService.getDropdownData(
        this.workCenterList,
        WorkCenter.displayName,
        WorkCenter.modelValue
      );
    })
  }

  public getScheduleStatus() {
    this.ppcService.lpScheduleStatus().subscribe(Response => {
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdStatus')[0].list = this.sharedService.getDropdownData(Response, CommonAPIObject.displayName, CommonAPIObject.modelValue)
    })
  }

  public editRecord(event) {
    this.updtedSalesOrder = event
  }

  public getScheduleIds() {
    this.ppcService.getLPScheduleIdList().subscribe(data => {
      this.scheduleIdList = data.map((x: any) => {
        return {
          displayName: x,
          modelValue: x
        }
      })
    })
  }

  public dropdownChangeEvent(data) {
    if (data.ref_key == 'productId' && data.value != null) {
      this.getSizeCodesByProduct(data.value);
    }

    if (data.ref_key == 'schdWC' && data.value != null) {
      this.getProducts(data.value)
    }
  }

  public getSizeCodesByProduct(id) {
    let sizeCodes = this.productList.filter((ele) => ele.productId == id)[0].sizeList;
    let sizeCodeObj = this.scheduleHeaderFormObject.formFields.filter(
      (ele) => ele.ref_key == 'sizeCode'
    )[0];
    sizeCodeObj.list = sizeCodes.map((x: any) => {
      return {
        displayName: x.sizeDesc,
        modelValue: x.sizeCode
      };
    });
    //sizeCodeObj.value = sizeCodeObj.list[0].modelValue;
  }

  public resTrictUser() {
    this.disableSchActions = true
    this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdWC')[0].disabled = true
    this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'productId')[0].disabled = true
    this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'sizeCode')[0].disabled = true
    this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'planStartDate')[0].disabled = true
    this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'duration')[0].disabled = true
  }

  public getScheduleDetails(): any { // API call to fetch the Schedule Header Details
    if (this.selectedScheduleId == undefined) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please select schedule Id',
        }
      );
    }
    this.isSearchTriggered = true;
    if (!this.selectedScheduleId) {
      return;
    }
    this.disableSchActions = false
    this.ppcService.getLpScheduleDetailsById(this.selectedScheduleId).subscribe((data): any => {
      this.scheduleDetails = data;
      this.sharedService.mapResponseToFormObj(this.scheduleHeaderFormObject.formFields, data);
      let selectedUnit = this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdWC')[0].value;
      if(selectedUnit){
        this.getProducts(selectedUnit);
      }
      
      let prodId = this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'productId')[0].value;
      let sizeCodeObj = this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'sizeCode')[0];
      let selectedStatus = this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdStatus')[0].value;
      this.scheduleStatus = selectedStatus;
      this.scheduleStatusDisplayName = this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdStatus')[0].list.filter((x: any) => x.modelValue == selectedStatus)[0].displayName
      if (this.scheduleStatus == SchedulStatus.Cancel_Schedule) {
        this.resTrictUser()
      } else {
        if (!selectedUnit) {
          this.enableFormFields();
        } else {
          this.disableFormFields(false);
        }
        this.disableSchActions = false
      }
      this.lpActionMapping(selectedStatus)
      if (prodId) {
        let sizeCodes = this.productList.filter(ele => ele.productId == prodId)[0].sizeList;
        sizeCodeObj.list = this.sharedService.getDropdownData(sizeCodes, SizeCode.displayName, SizeCode.modelValue);
      } else {
        sizeCodeObj.list = [];
      }
      this.getScheduleList();
    })
  }

  public enableFormFields() {
    this.scheduleHeaderFormObject.formFields.forEach((element) => {
      if (element.ref_key == 'schdWC' ||
        element.ref_key == 'productId' ||
        element.ref_key == 'sizeCode' ||
        element.ref_key == 'planStartDate' || element.ref_key == 'duration'
      ) {
        element.disabled = false;
      }
    });
  }

  public disableFormFields(isDisableAll) {
    if (isDisableAll) {
      this.scheduleHeaderFormObject.formFields.forEach((element) => {
        element.disabled = true;
      });
    } else {
      this.scheduleHeaderFormObject.formFields.forEach((element) => {
        if (element.ref_key == 'schdWC' || element.ref_key == 'productId' || element.ref_key == 'sizeCode') {
          element.disabled = this.lpList.length ? true : false;
        }
      });
    }
  }

  public lpActionMapping(status): any {
    if (this.scheduleStatus == SchedulStatus.Cancel_Schedule) {
      this.actionList = [{
        displayName: 'New Schedule',
        modelValue: LPAction.Create_Schedule
      }];
      return true
    }
    this.ppcService.lpActionMapping(status).subscribe(Response => {
      this.actionList = this.sharedService.getDropdownData(
        Response,
        CommonAPIObject.displayName,
        CommonAPIObject.modelValue
      );
    })
  }

  public getScheduleList() { // call API to get tha Caster Schedule List
    let prodId = this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'productId')[0].value;
    if(prodId){
      this.getMapping(prodId)
    }
    //this.getMapping(this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'productId')[0].value)
    this.ppcService.setlpSelectedRecords([])
    this.ppcService.getLPScheduleList(this.selectedScheduleId).subscribe(data => {
      data.map((order: any) => {
        order.status = order.orderSchdStatus == SchedulStatus.Released_to_Mill
        if (order.inputSizeList = this.masterProductList.filter((x: any) => x.modelValue == order.inputProduct).length > 0) {
          order.inputSizeList = this.masterProductList.filter((x: any) => x.modelValue == order.inputProduct)[0].sizeList
        }
        return order
      })
      this.reflpList = JSON.parse(JSON.stringify(data))
      this.lpList = JSON.parse(JSON.stringify(data));
      
      this.disableFormFields(false);
    })
  }

  public performScheduleAction(): any {
    ( < HTMLInputElement > document.getElementById(`action`)).classList.remove("ng-dirty")
    if (this.selectedAction == undefined || this.actionList.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please select schedule Id and get available action list',
        }
      );
    }

    if (this.selectedAction == LPAction.Clear) {
      this.resetFilter();
    };

    if (this.selectedAction == LPAction.Delete_Order) {
      this.deleteSelectedOrders()
    }

    if (this.selectedAction == LPAction.Cancel_Schedule) {
      this.cancelSchedule();
    }

    if (this.selectedAction == LPAction.Release_Schedule || this.selectedAction == LPAction.Release_Unplanned_Heat) {
      this.releaseUnplannedBookModal()
    }

    if (this.selectedAction == LPAction.Add_Unplanned_Heat) {
      if (this.selectedScheduleId == undefined) {
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning, {
            message: 'Please select schedule Id and get schedule details',
          }
        );
      }
      if(this.sharedService.isSectionVisible(this.menuConstants.lpSchSectionIds.Unplanned_Heat.id, this.screenStructure)){
        this.openUnplannedBookModal();
      }else{
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access Add Unplanned Heat section. Please contact Admin."})
      }
    }

    if (this.selectedAction == LPAction.Create_Schedule) {
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdWC')[0].disabled = false
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'productId')[0].disabled = false
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'sizeCode')[0].disabled = false
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdWC')[0].value = null
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'productId')[0].value = null
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'sizeCode')[0].value = null
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'planStartDate')[0].disabled = false
      this.scheduleHeaderFormObject.formFields.filter(
        (ele) => ele.ref_key == 'duration'
      )[0].disabled = false;
      this.createNewLPSchedule();
    }
    if (this.selectedAction == LPAction.View_and_Insert_Order) {
      if (this.selectedScheduleId == undefined) {
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning, {
            message: 'Please select schedule Id and get schedule details',
          }
        );
      }
      if(this.sharedService.isSectionVisible(this.menuConstants.lpSchSectionIds.View_And_Insert.id, this.screenStructure)){
        this.viewInsertOrders();
      }else{
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access View & Insert Order Book section. Please contact Admin."})
      }
    }
  }

  public deleteOrderService(): any {
    this.ppcService.deleteLpOrderBooks(this.sharedService.loggedInUserDetails.userId, this.selectedScheduleId, this.deleteSalesOrder.map((element: any) => {
      return element.orderSequenceNo
    })).subscribe((Response) => {
      if (Response) {
        this.deleteSalesOrder = [];
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success, {
            message: `Order Sequence No: ${Response} deleted from order book`
          }
        );
        this.getScheduleList();
      }
    })
  }

  public deleteSelectedOrders(): any {
    if (this.deleteSalesOrder.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please select order'
        }
      );
    }
    let orders = this.deleteSalesOrder.map((SoDetails: any) => {
      return SoDetails.salesOrderNo
    })

    this.confirmationService.confirm({
      message: `<span class="font-16 text-grey">Do you want to delete <h7 class = "font-weight-bold text-grey mb-3">${orders}</h7> Order(s)?</span>`,
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.deleteOrderService()
      },
      reject: () => {
        this.deleteSalesOrder = [];
        this.getScheduleList()
      }
    });
  }

  public createNewLPSchedule() {
    this.disableSchActions = false
    this.ppcService.getAddNewLPSchedule(this.sharedService.loggedInUserDetails.userId).subscribe((data) => {
      this.lpList = [];
      this.reflpList = [];
      this.newScheduleDetails = data;
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdID')[0].value = data.schdID;
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdStatus')[0].value = data.schdStatus;
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdDate')[0].value = new Date();
      this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'planStartDate')[0].value = new Date();
      this.scheduleStatus = data.schdStatus;
      this.scheduleStatusDisplayName = this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdStatus')[0].list.filter((x: any) => x.modelValue == data.schdStatus)[0].displayName
      this.scheduleHeaderFormObject.formFields.filter(
        (ele) => ele.ref_key == 'duration'
      )[0].value = 40;
      this.getScheduleIds();
    });
  }

  public viewInsertOrders() {
    this.insertOrderBookModalRef = this.dialogService.open(ViewAndInsertOrderbookComponent, {
      header: 'View and Insert Orders',
      width: '80%',
      contentStyle: {
        "max-height": "500px",
        "overflow": "auto"
      },
      autoZIndex: false,
      data: {
        scheduleId: this.scheduleDetails.schdID,
        productName: this.productList.filter(ele => ele.productId == this.scheduleDetails.productId)[0].productName,
        sizeCode: this.scheduleDetails.sizeCode,
        schdStatus: this.scheduleDetails.schdStatus,
        gradeGroup: null,
        gradeCategory: null,
        scheduleType: 'lp',
        isRetrieveBtn: this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.View_And_Insert.buttons.retrieve, menuConstants.lpSchSectionIds.View_And_Insert.id, this.screenStructure),
        isRefreshBtn: this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.View_And_Insert.buttons.refresh, menuConstants.lpSchSectionIds.View_And_Insert.id, this.screenStructure),
        isInsertOrderBtn: this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.View_And_Insert.buttons.insertOrder, menuConstants.lpSchSectionIds.View_And_Insert.id, this.screenStructure)
      }
    });

    this.insertOrderBookModalRef.onClose.subscribe((data) => {
      if (data != undefined) {
        this.getScheduleList();
        this.getScheduleDetails();
      }
    });
  }

  public cancelSchedule() {
    this.jswComponent.onSubmit(
      this.scheduleHeaderFormObject.formName,
      this.scheduleHeaderFormObject.formFields
    );
    var formData = this.jswService.getFormData();
    let object = {
      "actualEndDate": "string",
      "actualStartDate": "string",
      "gradeCategory": "string",
      "gradeGroup": "string",
      "planEndDate": "string",
      "planStartDate": "string",
      "productId": "string",
      "schdDate": "string",
      "schdID": 0,
      "schdStatus": "string",
      "schdWC": "string",
      "duration": 0,
      "sizeCode": "string"
    }
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, object);
    this.ppcService.cancelLPSchedule(this.selectedScheduleId, this.sharedService.loggedInUserDetails.userId, formObj).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `Schedule No : ${Response.schdID} cancelled successfully`,
        }
      );
      this.getScheduleIds();
      this.getScheduleDetails();
    })
  }

  public releaseUnplannedBookModal() {
    if(this.validationFlags.plannedDateEnabled){
      let planDate = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'planStartDate')[0].value;
      let hours = new Date(this.scheduleDetails.planStartDate).getHours();
      let minutes = new Date(this.scheduleDetails.planStartDate).getMinutes();
      if(new Date(planDate) < new Date()){
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          { message: `Planned Start Date should be greater than Current Date.` }
        );
        return;
      }else if((new Date(planDate) > new Date()) && (new Date(planDate).getHours() != hours || new Date(planDate).getMinutes() != minutes)){
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          { message: `Please save schedule header details.` }
        );
        return;
      }
    }
    this.jswComponent.onSubmit(
      this.scheduleHeaderFormObject.formName,
      this.scheduleHeaderFormObject.formFields
    );
    var formData = this.jswService.getFormData();
    let object = {
      "actualEndDate": "string",
      "actualStartDate": "string",
      "gradeCategory": "string",
      "gradeGroup": "string",
      "planEndDate": "string",
      "planStartDate": "string",
      "productId": "string",
      "schdDate": "string",
      "schdID": 0,
      "schdStatus": "string",
      "schdWC": "string",
      "duration": 0,
      "sizeCode": "string"
    }
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, object);
    this.ppcService.releaseLPSchedule(this.selectedScheduleId, this.sharedService.loggedInUserDetails.userId, formObj).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `Schedule No : ${Response.schdID} released successfully`,
        }
      );
      this.getScheduleDetails();
    })
  }

  public openUnplannedBookModal() {
    this.insertOrderBookModalRef = this.dialogService.open(AddUnplannedHeatComponent, {
      header: 'Add Unplanned Heat',
      width: '80%',
      contentStyle: {
        "max-height": "500px",
        "overflow": "auto"
      },
      autoZIndex: false,
      data: {
        scheduleId: this.scheduleDetails.schdID,
        productName: this.productList.filter(ele => ele.productId == this.scheduleDetails.productId)[0].productName,
        sizeCode: this.scheduleDetails.sizeCode,
        schdStatus: this.scheduleDetails.schdStatus,
        gradeGroup: "null",
        gradeCategory: "null",
        scheduleType: 'lp',
        isRetrieveBtn: this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.Unplanned_Heat.buttons.retrieve, menuConstants.lpSchSectionIds.Unplanned_Heat.id, this.screenStructure),
        isRefreshBtn: this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.Unplanned_Heat.buttons.refresh, menuConstants.lpSchSectionIds.Unplanned_Heat.id, this.screenStructure),
        isInsertOrderBtn: this.sharedService.isButtonVisible(true, menuConstants.lpSchSectionIds.Unplanned_Heat.buttons.insertOrder, menuConstants.lpSchSectionIds.Unplanned_Heat.id, this.screenStructure)
      }
    });

    this.insertOrderBookModalRef.onClose.subscribe((data) => {
      if (data != undefined) {
        this.getScheduleList()
        this.getScheduleDetails();
      }
    });
  }

  public saveScheduleHeader(): any {
    if (this.scheduleStatus == SchedulStatus.Cancel_Schedule) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Schedule already cancelled, please reset screen to create new schedule`
        }
      );
    }
    this.jswComponent.onSubmit(
      this.scheduleHeaderFormObject.formName,
      this.scheduleHeaderFormObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let obj = {
        "schdID": null,
        "productId": null,
        "sizeCode": "TMT_size",
        "schdWC": "Pune",
        "planStartDate": "",
        "schdDate": "",
        "schdStatus": "",
        "duration": ""
      }
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, obj);
      let reqBodyObj = {
        "schdID": formObj.schdID,
        "productId": formObj.productId,
        "sizeCode": formObj.sizeCode,
        "schdWC": formObj.schdWC,
        "planStartDate": formObj.planStartDate,
        "schdDate": formObj.schdDate,
        "schdStatus": formObj.schdStatus,
        "duration": formObj.duration
      }
      this.ppcService.saveLpScheduleDetails(reqBodyObj).subscribe((data) => {
        this.selectedScheduleId = data.schdID
        this.scheduleDetails = data
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success, {
            message: `Schedule details for Schedule No. ${data.schdID} saved successfully`
          }
        );
        let selectedStatus = formObj.schdStatus
        this.lpActionMapping(selectedStatus)
        if (this.selectedScheduleId !== undefined) {
          this.getScheduleList()
        }
        this.disableFormFields(false);
        this.getScheduleIds();
      });
    }
  }

  public updateOrder(event): any {
    if (event) {
      this.getScheduleDetails();
      this.getScheduleList();
    }
  }

  public resetFilter() {
    this.isSearchTriggered = false;
    this.selectedScheduleId = undefined;
    this.scheduleStatus = undefined;
    this.scheduleStatusDisplayName = undefined;
    this.actionList = [{
        displayName: 'New Schedule',
        modelValue: LPAction.Create_Schedule
      },
      {
        displayName: 'clear',
        modelValue: LPAction.Clear
      }
    ];
    this.lpList = [];
    this.reflpList = [];
    var checkDirtyClass = ( < HTMLInputElement > document.getElementById(`schId`)).classList[0];
    var indexOfDirtyClass = checkDirtyClass.indexOf("ng-dirty");
    ( < HTMLInputElement > document.getElementById(`schId`)).classList.remove("ng-dirty");
    var checkDirtyClass = ( < HTMLInputElement > document.getElementById(`action`)).classList[0];
    var indexOfDirtyClass = checkDirtyClass.indexOf("ng-dirty");
    ( < HTMLInputElement > document.getElementById(`action`)).classList.remove("ng-dirty")
    //this.jswComponent.resetForm(this.scheduleHeaderFormObject);
    this.sharedService.resetForm(this.scheduleHeaderFormObject, this.formRef)
    this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'schdWC')[0].disabled = false
    this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'productId')[0].disabled = false
    this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'sizeCode')[0].disabled = false
    this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'planStartDate')[0].disabled = false
    this.scheduleHeaderFormObject.formFields.filter(ele => ele.ref_key == 'duration')[0].disabled = false
  }

  public getListToUpdateOrDelete(event) {
    this.updtedSalesOrder = event
  }


  public deleteSelectedRecords(event) {
    this.deleteSalesOrder = event
  }

  public getWorkCenterList() {
    this.commonService.getProductSizeList().subscribe(Response => {
      this.allProductList = Response;
    })
  }

  public getMapping(productId) {
    let productName = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'productId')[0].list.filter((x: any) => x.modelValue == productId)[0].displayName;
    this.ppcService.getMapping(productId).subscribe(Response => {
      this.mappingProducts = Response.map((ele: any) => {
        return {
          displayName: ele.materialDesc,
          modelValue: ele.materialId,
        }
      })
    })
  }

  public getMasterWCBasedProductSizeList() {
    this.commonService.getProductSizeList().subscribe(Response => {
      this.masterProductList = Response.filter(ele => ele.productType == 'SMS').map((ele: any) => {
        return {
          displayName: ele.productName,
          modelValue: ele.productId,
          sizeList: ele.sizeList.map((size: any) => {
            return {
              displayName: size.sizeDesc,
              modelValue: size.sizeCode,
            }
          })
        }
      })
    })
  }

  public getFormRef(event){
    this.formRef = event;
  }

}
