import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  ColumnType,
  TableType,
  LOV,
  LPAction,
  SchedulStatus
} from 'src/assets/enums/common-enum';
import {
  ComponentType
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  ITableHeader
} from 'src/assets/interfaces/table-headers';
import {
  PpcService
} from '../../../../services/ppc.service';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  CommonApiService
} from 'src/app/shared/services/common-api.service';

@Component({
  selector: 'app-lp-schedule-list',
  templateUrl: './lp-schedule-list.component.html',
  styleUrls: ['./lp-schedule-list.component.css']
})
export class LpScheduleListComponent implements OnInit {
  public columns: ITableHeader[] = [];
  @Input() public inputProductList: any[]
  @Input() public masterProductList: any[]
  @Input() public scheduleId: any;
  @Input() public scheduleList: any = [];
  @Input() public refrenceList: any = [];
  @Input() public disableSchActions: boolean = false;
  @Output() public getLpSchList = new EventEmitter < Event > ();
  @Output() public deleteLpSchList = new EventEmitter < Event > ();
  @Output() public saveOrder = new EventEmitter < boolean > ();
  @Input() public isSaveBtn: boolean;
  @Input() validationFlags: any;
  public selectedRecords: any = [];
  public saveSelectedRecords: any = [];
  public tableType: any;
  public viewType = ComponentType;
  public columnType = ColumnType;
  public flyRemarks: any = [];
  public flyRequired: any = [];
  public submitted: boolean = false;
  productList: {
    displayName: string;modelValue: string;
  } [];
  sizeList: {
    displayName: string;modelValue: string;
  } [];

  public yield = 0;
  constructor(public ppcService: PpcService, public sharedService: SharedService, public commonService: CommonApiService) {}

  ngOnInit(): void {

    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.columns = this.createColumns();
    this.getLovs();
    this.ppcService.getlpSelectedRecords.subscribe(Response => {
      this.selectedRecords = Response;
    })
  };

  public createColumns() {
    return [{
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'salesOrderNo',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      // {
      //   field: 'lineNo',
      //   header: 'Order Line ID',
      //   columnType: ColumnType.string,
      //   width: '120px',
      //   sortFieldName: '',
      // },


      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'equivalentSpec',
        header: 'Eq Spec',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'orderQty',
        header: 'Order Qty',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'balancedToRoll',
        header: 'BTR(Tons)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'inputProduct',
        header: 'I/P Product',
        columnType: ColumnType.dropdown,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'inputSize',
        header: 'I/P Size',
        columnType: ColumnType.dropdown,
        width: '120px',
        sortFieldName: '',

      },
      {
        field: 'cutLength',
        header: 'Cut Len(mm)',
        columnType: ColumnType.textWithValidations,
        width: '110px',
        sortFieldName: '',
        isRequired: true,
        isError: true,
        errorMsg: 'Required'
      },
      {
        field: 'plannedPcs',
        header: 'Plnd Qty(pcs)',
        columnType: ColumnType.textWithValidations,
        width: '100px',
        sortFieldName: '',
        isRequired: true,
        isError: true,
        errorMsg: 'Required'
      },
      {
        field: 'plannedQty',
        header: 'Plnd Qty(tons)',
        columnType: ColumnType.textWithValidations,
        width: '110px',
        sortFieldName: '',
        isRequired: true,
        isError: true,
        errorMsg: 'Required'
      },
      {
        field: 'opQty',
        header: 'OP Qty(tons)',
        columnType: ColumnType.textWithValidations,
        width: '110px',
        sortFieldName: '',
        isRequired: true,
        isError: true,
        errorMsg: 'Required'
      },
      {
        field: 'density',
        header: 'Density(kg/m)',
        columnType: ColumnType.textWithValidations,
        width: '110px',
        sortFieldName: '',
        isRequired: true,
        isError: true,
        errorMsg: 'Required'
      },

      {
        field: 'onePieceWeight',
        header: '1 Pc Wt(tons)',
        columnType: ColumnType.textWithValidations,
        width: '100px',
        sortFieldName: '',
        isRequired: true,
        isError: true,
        errorMsg: 'Required'
      },
      {
        field: 'product',
        header: 'Product',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: ''
      },
      {
        field: 'size',
        header: 'Size',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'producedQty',
        header: 'Prdcd Qty',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'smsProductLengthMax',
        header: 'SMS Prod Len Max',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'smsProductLengthMin',
        header: 'SMS Prod Len Min',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'attached',
        header: 'Atcd Qty(pcs)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'attached',
        header: 'Atcd Qty(pcs)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'charged',
        header: 'Chrgd Qty(pcs)',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'discharged',
        header: 'Dischrgd Qty(pcs)',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'rolled',
        header: 'Rolled Qty(pcs)',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'cobbled',
        header: 'Cobbled Qty(pcs)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'hotout',
        header: 'Hotout Qty(pcs)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'mixed',
        header: 'Mxd Qty(pcs)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'bundle',
        header: 'Bun Prod.',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'finishedProductLengthMin',
        header: 'Fin Prod Len Min(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'finishedProductLengthMax',
        header: 'Fin Prod Len Max(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.tooltip,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'productionOrderNo',
        header: 'PO ID',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
    ];
  };

  public getSelectedSchList() {
    if (this.selectedRecords.length > 0) {
      this.selectedRecords.map((ele: any) => {
        if (!ele.plannedPcs) {
          ele.plannedPcsError = true;
          ele.cutlengthError = !ele.cutLength ? true : false
        }
        return ele
      });
    }
    this.deleteLpSchList.emit(this.selectedRecords);
  };

  public getDensityByProductSize(rowData) {
    // this.saveSelectedRecords.push(rowData);
    if (this.scheduleList.length > 0) {
      this.scheduleList.map((ele: any) => {
        if (!ele.plannedPcs) {
          ele.plannedPcsError = true;
          ele.cutlengthError = !ele.cutLength ? true : false
        }
        return ele
      });
    }
    this.getLpSchList.emit(this.scheduleList);
    this.ppcService.getDensity(rowData.inputProduct, rowData.inputSize, this.scheduleId, rowData.orderSequenceNo).subscribe(Response => {
      if (Response) {
        rowData.density = parseFloat(Response)
        if (rowData.cutLength) {
          this.calculateOncePieceWeight(rowData);
          if (rowData.plannedPcs) {
            this.onplannedQtyPcshange(rowData)
          }
          if (rowData.plannedQty) {
            this.onplannedQtyTonshange(rowData)
          }
        }
      }
    })
  }

  public calculateOncePieceWeight(rowData): any {
    if (!rowData.density) {
      rowData.cutlengthError = rowData.cutLength ? false : true;
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please get Density value by selecting Input Product and Input Size',
        }
      );
    }
    if (!rowData.cutLength) {
      rowData.cutlengthError = true;
      rowData.onePieceWeight = null;
      return;
    }

    rowData.cutlengthError = false;
    let oncePieceWeight = ((rowData.density * (rowData.cutLength/1000)) / 1000)
    rowData.onePieceWeight = oncePieceWeight;
    if (rowData.plannedPcs != null || rowData.plannedPcs != '') {
      this.onplannedQtyPcshange(rowData)
    }

    if (rowData.plannedQty) {
      this.onplannedQtyTonshange(rowData)
    }
  }

  public onplannedQtyPcshange(rowData): any {
    if (!rowData.density) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please get Density value by selecting Input Product and Input Size',
        }
      );
    }
    if (!rowData.cutLength) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please enter Cut Length value',
        }
      );
    }
    if (!rowData.plannedPcs) {
      rowData.plannedPcsError = true
      return true
    }
    rowData.plannedQty = ((rowData.onePieceWeight * rowData.plannedPcs).toFixed(3));
    rowData.opQty = ((rowData.plannedQty / this.yield).toFixed(3));
    rowData.plannedPcsError = false;
  }

  public onplannedQtyTonshange(rowData): any {
    if (!rowData.cutLength) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please enter Cut Length value',
        }
      );
    };


    if (rowData.plannedQty == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Planned Qty should be greater than 0',
        }
      );
    }
    this.refrenceList.forEach(element => {
      if(element.salesOrderNo == rowData.salesOrderNo){
        if(rowData.plannedQty > (element.balancedToRoll + element.plannedQty)){
          rowData.plannedQtyError = true
            this.sharedService.displayToastrMessage(
            this.sharedService.toastType.Warning, {
              message: 'Planned Qty should not be greater than BTR',
            }
          );
        }
        else{
          rowData.plannedQtyError = false;
        }

      }

    });
    // if (rowData.plannedQty > rowData.balancedToRoll) {
    //   rowData.plannedQtyError = true
    //   return this.sharedService.displayToastrMessage(
    //     this.sharedService.toastType.Warning, {
    //       message: 'Planned Qty should not be greater than BTR',
    //     }
    //   );
    // }

    rowData.plannedPcs = parseInt(Math.floor(rowData.plannedQty / rowData.onePieceWeight).toFixed(3));
    rowData.plannedQty = ((rowData.onePieceWeight * rowData.plannedPcs).toFixed(3));
    rowData.opQty = ((rowData.plannedQty / this.yield).toFixed(3));
    //rowData.plannedQtyError = false
    rowData.plannedPcsError = false;
    rowData.cutlengthError = false;
  }

  public getProductTypeAndUpdateSizeList(rowData, event) {
    rowData.inputSizeList = this.masterProductList.filter((x: any) => x.modelValue == rowData.inputProduct)[0].sizeList
  }

  public getDensity(rowData, productId, sizeCode, schdId, orderSeqNo) {
    this.ppcService.getDensity(productId, sizeCode, schdId, orderSeqNo).subscribe(Response => {
      if (Response) {
        return Response
      }
    })
  }

  ///new change op qty
  public onOPQtyTonChange(rowData): any {
    if (!rowData.cutLength) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please enter Cut Length value',
        }
      );
    };


    if (rowData.opQty == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Planned Qty should be greater than 0',
        }
      );
    }
    rowData.plannedQty = ((rowData.opQty * this.yield).toFixed(3));
    rowData.plannedPcs = parseInt(Math.floor(rowData.plannedQty / rowData.onePieceWeight).toFixed(3));
    rowData.plannedQty = ((rowData.onePieceWeight * rowData.plannedPcs).toFixed(3));
    rowData.plannedPcsError = false;
    rowData.cutlengthError = false;
  }
   //To fetch the load cell value
   public getLovs() {
    this.commonService.getLovs('Yield_Value').subscribe((data) => {
      this.yield = data[0].codeValue;
    });
  }
  //end

  public untouchedSaveClickValidations(){
    let checkDensityValidations = this.saveSelectedRecords.filter(ele => !ele.density);
    if (checkDensityValidations.length > 0) {
      return this.saveSelectedRecords.map(ele => {
      if(!ele.density){
        ele.plannedPcsError = true;
        ele.cutlengthError = true;
      }
        return ele
      });
    }else{
      return false
    }
  }

  public saveScheduleList(): any {
    this.saveSelectedRecords = this.scheduleList;
    if(this.untouchedSaveClickValidations() != false){
      return this.sharedService.displayToastrMessage(
            this.sharedService.toastType.Warning, {
              message: 'Please enter required values'
            }
          );
    }

    let checkValidations = this.saveSelectedRecords.filter(ele => ele.plannedPcsError == true)
    if (checkValidations.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please enter required values'
        }
      );
    };

    //22-11

    let checkQtyValidations = this.saveSelectedRecords.filter(ele => ele.plannedQtyError == true)
    if (checkQtyValidations.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please enter required values'
        }
      );
    };
    //

    let checkPlanQtyValidations = this.saveSelectedRecords.filter(ele => ele.plannedQty == 0 || ele.plannedPcs == 0)
    if (checkPlanQtyValidations.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Planned Qty can not be 0'
        }
      );
    }

    let tempArr = this.saveSelectedRecords.filter(ele => !ele.cutLength || (ele.cutLength && this.validationFlags.cutLengthEnabled && ((ele.cutLength < ele.smsProductLengthMin) || (ele.cutLength > ele.smsProductLengthMax))));
    if(tempArr.length){
    this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please enter required fields in correct format'});
    return;
  }

    let responseBody = this.saveSelectedRecords.map((order: any) => {
      return {
        "ipMaterialId": order.inputProduct,
        "ipSizeCode": order.inputSize,
        "ordSeqNo": order.orderSequenceNo,
        "schdId": this.scheduleId,
        "schdQtyPcs": parseFloat(order.plannedPcs),
        "schdQtyTons": parseFloat(order.plannedQty),
        "cutLength": parseFloat(order.cutLength),
        "onePieceWeight": parseFloat(order.onePieceWeight),
      }
    })

    this.ppcService.updateLPScheduleOBList(this.scheduleId, this.sharedService.loggedInUserDetails.userId, responseBody).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: 'Selected Order(s) Updated'
        }
      );
      this.saveOrder.emit(true);
      this.saveSelectedRecords = [];
    })
  }

  public isReleasedOrder() {
    if (this.scheduleList.filter(ele => ele.orderSchdStatus == SchedulStatus.Released_to_Mill).length > 0) {
      return true;
    } else {
      return false;
    }
  }
}
