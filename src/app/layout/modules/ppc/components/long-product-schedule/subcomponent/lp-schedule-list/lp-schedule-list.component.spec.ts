import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LpScheduleListComponent } from './lp-schedule-list.component';

describe('LpScheduleListComponent', () => {
  let component: LpScheduleListComponent;
  let fixture: ComponentFixture<LpScheduleListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LpScheduleListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LpScheduleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
