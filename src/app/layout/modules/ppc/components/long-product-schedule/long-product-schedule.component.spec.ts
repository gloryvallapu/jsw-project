import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
  Component,
  OnInit
} from '@angular/core';
import {
  CasterScheduleForms,
  LongProductScheduleForms
} from '../../form-objects/ppc-form-objects';
import {
  ITableHeader
} from 'src/assets/interfaces/table-headers';
import {
  TableType,
  ColumnType
} from 'src/assets/enums/common-enum';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  JswCoreService,
  JswFormComponent
} from 'jsw-core';
import {
  PpcService
} from '../../services/ppc.service';
import {
  ComponentType
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  CommonApiService
} from 'src/app/shared/services/common-api.service';
import {
  DialogService,
  DynamicDialogRef
} from 'primeng/dynamicdialog';
import {
  WorkCenter,
  Product,
  SizeCode,
  CommonAPIObject,
  LPAction,
  SchedulStatus
} from 'src/assets/enums/common-enum';
import {
  ViewAndInsertOrderbookComponent
} from '../caster-schedule/sub-components/view-and-insert-orderbook/view-and-insert-orderbook.component';
import {
  AddUnplannedHeatComponent
} from '../caster-schedule/sub-components/add-unplanned-heat/add-unplanned-heat.component';
import {
  ConfirmationService, SharedModule
} from 'primeng/api';

import { LongProductScheduleComponent } from './long-product-schedule.component';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { OrderBookForms } from 'src/assets/constants/PPC/ppc-form-objects';
import { PpcEndPoints } from '../../form-objects/ppc-api-endpoints';

describe('LongProductScheduleComponent', () => {
  let component: LongProductScheduleComponent;
  let fixture: ComponentFixture<LongProductScheduleComponent>;
  let sharedService: SharedService;
  let jswService: JswCoreService;
  let jswComponent: JswFormComponent;
  let ppcService: PpcService;
  let commonService: CommonApiService;
  let dialogService: DialogService;
  let confirmationService: ConfirmationService;
 // let formObjects : CasterScheduleForms["scheduleHeaderForm"];
     const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LongProductScheduleComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        ConfirmationService,
        CommonApiService,
        API_Constants,
        CommonAPIEndPoints,
        OrderBookForms,
       PpcEndPoints,
       PpcService,
    CasterScheduleForms,
    LongProductScheduleForms,
    JswFormComponent,
    { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LongProductScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
