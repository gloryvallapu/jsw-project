import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeatAndBatchLinesComponent } from './heat-and-batch-lines.component';

describe('HeatAndBatchLinesComponent', () => {
  let component: HeatAndBatchLinesComponent;
  let fixture: ComponentFixture<HeatAndBatchLinesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeatAndBatchLinesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeatAndBatchLinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
