import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUnplannedHeatComponent } from './add-unplanned-heat.component';

describe('AddUnplannedHeatComponent', () => {
  let component: AddUnplannedHeatComponent;
  let fixture: ComponentFixture<AddUnplannedHeatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddUnplannedHeatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUnplannedHeatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
