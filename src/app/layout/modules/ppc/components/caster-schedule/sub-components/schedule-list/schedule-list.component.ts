import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ColumnType, TableType, LOV, SMSStatus} from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { PpcService } from '../../../../services/ppc.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-schedule-list',
  templateUrl: './schedule-list.component.html',
  styleUrls: ['./schedule-list.component.css']
})
export class ScheduleListComponent implements OnInit {
  public columns: ITableHeader[] = [];
  @Input() public scheduleId: any;
  @Input() public scheduleList: any = [];
  @Input() public disableSchActions: boolean;
  @Input() clearSelectionEvent: Observable<boolean>;
  @Input() isSaveBtn: boolean = false;
  @Input() validationFlags: any;
  @Output() public getCasterSchList = new EventEmitter<Event>();
  @Output() public refreshSchList = new EventEmitter<Event>();
  public selectedRecords: any = [];
  public tableType: any;
  public viewType = ComponentType;
  public columnType = ColumnType;
  public flyRemarks: any = [];
  public flyRequired: any = [];
  public submitted: boolean = false;
  private eventSubscription: Subscription;
  public smsStatus = SMSStatus;

  //unplanned build emmiter
  @Output() public buildUnplanned = new EventEmitter<Event>();
  constructor(public ppcService: PpcService, public sharedService: SharedService, public commonService: CommonApiService) { }

  ngOnInit(): void {
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.flyRequired = [
      { displayName: 'Yes', modelValue: 'Y' },
      { displayName: 'No', modelValue: 'N' }
    ]
    this.columns = [
      { field: 'checkbox', header: '', columnType: ColumnType.checkbox, width: '50px', sortFieldName: '' },
      { field: 'orderId', header: 'Sales Order', columnType: ColumnType.string, width: '110px', sortFieldName: 'userId'},
      // { field: 'orderLineId', header: 'Order Line ID', columnType: ColumnType.string, width: '120px', sortFieldName: ''},


      { field: 'grade', header: 'Grade', columnType: ColumnType.string, width: '110px', sortFieldName: ''},
      { field: 'psn', header: 'PSN', columnType: ColumnType.string, width: '150px', sortFieldName: ''},
      { field: 'productLength', header: 'Prod. Len(mm)', columnType: ColumnType.text, width: '110px', sortFieldName: ''},
      { field: 'rollUnit', header: 'Next Unit', columnType: ColumnType.string, width: '80px', sortFieldName: ''},
      { field: 'btc', header: 'BTC(tons)', columnType: ColumnType.number, width: '100px', sortFieldName: ''},
      { field: 'oncePieceWeight', header: '1 Pc Wgt(tons)', columnType: ColumnType.number, width: '105px', sortFieldName: ''},
      { field: 'heatQty', header: 'Heat Qty', columnType: ColumnType.text, width: '80px', sortFieldName: ''},
      { field: 'scheduleQty', header: 'Schd Qty(tons)', columnType: ColumnType.text, width: '110px', sortFieldName: ''},
      { field: 'schedulePcs', header: 'Schd Pcs', columnType: ColumnType.text, width: '100px', sortFieldName: ''},
      { field: 'productWeightMin', header: 'Prod Wgt Min(tons)', columnType: ColumnType.number, width: '140px', sortFieldName: ''},
      { field: 'productWeightMax', header: 'Prod Wgt Max(tons)', columnType: ColumnType.number, width: '140px', sortFieldName: ''},
      { field: 'cutLengthMin', header: 'Cut Len Min(mm)', columnType: ColumnType.number, width: '125px', sortFieldName: ''},
      { field: 'cutLengthMax', header: 'Cut Len Max(mm)', columnType: ColumnType.number, width: '125px', sortFieldName: ''},
      { field: 'density', header: 'Density(kg/m)', columnType: ColumnType.number, width: '105px', sortFieldName: ''},

     // { field: 'cutLength', header: 'Cut Length', columnType: ColumnType.text, width: '150px', sortFieldName: ''}, //confirm with Ankur

      { field: 'isFlyRequired', header: 'Is Fly Required', columnType: ColumnType.dropdown, width: '105px', sortFieldName: ''},
      { field: 'flyRemarks', header: 'Fly Remarks', columnType: ColumnType.dropdown, width: '120px', sortFieldName: ''},
      { field: 'ncoRemarks', header: 'NCO Remarks', columnType: ColumnType.string, width: '110px', sortFieldName: ''},
      { field: 'brmMinLength', header: 'BRM Min Len(mm)', columnType: ColumnType.number, width: '130px', sortFieldName: ''},
      { field: 'brmMaxLength', header: 'BRM Max Len(mm)', columnType: ColumnType.number, width: '130px', sortFieldName: ''},
      { field: 'customer', header: 'Customer', columnType: ColumnType.tooltip, width: '100px', sortFieldName: ''},
      { field: 'destination', header: 'Destination', columnType: ColumnType.string, width: '120px', sortFieldName: ''},
      { field: 'branch', header: 'Branch', columnType: ColumnType.string, width: '80px', sortFieldName: ''},
      { field: 'port', header: 'Port', columnType: ColumnType.string, width: '80px', sortFieldName: ''},
      { field: 'eqSpec', header: 'Eq Spec', columnType: ColumnType.string, width: '100px', sortFieldName: ''},
      { field: 'finalProductDueDate', header: 'Final Prod Due Date', columnType: ColumnType.date, width: '140px', sortFieldName: ''},
      { field: 'productionOrderNo', header: 'PO ID', columnType: ColumnType.string, width: '80px', sortFieldName: 'soId' },
      { field: 'scheduleId', header: 'Schd ID', columnType: ColumnType.string, width: '100px', sortFieldName: ''},

    ]
    this.getFlyRemarks();
    this.selectedRecords = [];
    this.eventSubscription = this.clearSelectionEvent.subscribe(() => this.selectedRecords = []);
    // this.setDropdownData();
  }

  ngOnDestroy() {
    this.eventSubscription.unsubscribe();
  }

  public getFlyRemarks(){
    this.commonService.getLovs('Fly_Remark').subscribe( data => {
      this.flyRemarks = this.sharedService.getDropdownData(data, LOV.displayName, LOV.modelValue);
    })
  }

  public setDropdownData(){
    this.scheduleList = this.scheduleList.map((ele: any) => {
      ele.isFlyRequired = ele.isFlyRequired ? ele.isFlyRequired : 'N';
      return ele;
    });
  }

  public onProdLenChange(rowData: any){
    if(rowData.scheduleQty && rowData.heatQty && rowData.productLength){
      rowData.oncePieceWeight = (((rowData.productLength/1000) * rowData.density)/1000).toFixed(3); // 240 is density - later API will return this value
      if(rowData.oncePieceWeight){
        rowData.schedulePcs = parseInt(Math.floor(rowData.scheduleQty / rowData.oncePieceWeight).toFixed(3));
      }
    }

    if(rowData.productLength && rowData.btc && !rowData.heatQty && !rowData.scheduleQty){
      rowData.oncePieceWeight = (((rowData.productLength/1000) * rowData.density)/1000).toFixed(3); // 240 is density - later API will return this value
      if(rowData.oncePieceWeight){
        rowData.schedulePcs = parseInt(Math.floor(rowData.btc / rowData.oncePieceWeight).toFixed(3));
      }
      if(rowData.schedulePcs){
        rowData.scheduleQty = parseFloat((rowData.schedulePcs * rowData.oncePieceWeight).toFixed(3));
      }
      if(rowData.scheduleQty){
        rowData.heatQty = parseFloat((rowData.scheduleQty / 100).toFixed(3));             // 100 is a 1 heat capacity.. should come from backend
      }
    }
  }

  public onHeatQtyChange(rowData: any){
    if(rowData.heatQty){
      rowData.scheduleQty = parseFloat((rowData.heatQty * 100).toFixed(3));
    }
    if(rowData.scheduleQty && rowData.oncePieceWeight){
      rowData.schedulePcs = parseInt(Math.floor(rowData.scheduleQty / rowData.oncePieceWeight).toFixed(3));
    }
    if(rowData.schedulePcs && rowData.oncePieceWeight){
      rowData.scheduleQty = parseFloat((rowData.schedulePcs * rowData.oncePieceWeight).toFixed(3));
    }
    if(rowData.scheduleQty){
      rowData.heatQty = parseFloat((rowData.scheduleQty / 100).toFixed(3));
    }
  }

  public onSchQtyChange(rowData: any){
    if(rowData.scheduleQty && rowData.oncePieceWeight){
      rowData.schedulePcs = parseInt(Math.floor(rowData.scheduleQty / rowData.oncePieceWeight).toFixed(3));
    }
    if(rowData.schedulePcs && rowData.oncePieceWeight){
      rowData.scheduleQty = parseFloat((rowData.schedulePcs * rowData.oncePieceWeight).toFixed(3));
    }
    if(rowData.scheduleQty){
      rowData.heatQty = parseFloat((rowData.scheduleQty / 100).toFixed(3));
    }
  }

  public onSchPcsChange(rowData: any){
    if(rowData.schedulePcs && rowData.oncePieceWeight){
      rowData.scheduleQty = parseFloat((rowData.oncePieceWeight * rowData.schedulePcs).toFixed(3));
    }
    if(rowData.scheduleQty){
      rowData.heatQty = parseFloat((rowData.scheduleQty / 100).toFixed(3));
    }
  }

  public saveCasterScheduleList(){
    this.submitted = true;
    if(this.scheduleList && this.scheduleList.length){
      let tempArr = this.scheduleList.filter(ele => !ele.productLength || (ele.productLength && this.validationFlags.cutLengthEnabled && ((ele.productLength < ele.cutLengthMin) || (ele.productLength > ele.cutLengthMax))) ||
                                                       !ele.heatQty || !ele.scheduleQty || (ele.scheduleQty && ele.schdStatus != this.smsStatus.Release_to_SMS && (ele.scheduleQty > (ele.btc + ele.originalSchQty))) ||
                                                       !ele.schedulePcs || (ele.isFlyRequired == 'Y' && !ele.flyRemarks) ||
                                                       (ele.oncePieceWeight && this.validationFlags.onePieceWtEnabled && ((ele.oncePieceWeight < ele.productWeightMin) || (ele.oncePieceWeight > ele.productWeightMax))));
      if(tempArr.length){
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please enter required fields in Caster Schedule List in correct format'});
        return;
      }
      let schId = this.scheduleId ? this.scheduleId : this.scheduleList[0].scheduleId;
      this.ppcService.updateCasterScheduleList(this.scheduleList, schId, this.sharedService.loggedInUserDetails.userId).subscribe(
        data => {
          this.submitted = false;
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Caster Schedule List saved successfully'});
          this.selectedRecords = [];
          if(this.scheduleList[this.scheduleList.length -1].isPlannedHeat == 'N'){
            this.buildUnplanned.emit(this.scheduleList);
          }

          //this.getCasterSchList.emit(this.scheduleList);
          //this.buildHeat(this.scheduleList)
          //this.refreshSchList.emit();
          // this.getCasterSchList.emit(this.selectedRecords);
        }
      )
    }else{
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'No records available to save'});
      return;
    }
  }

  public testSave(){

  }
  public getSelectedSchList(){
    this.getCasterSchList.emit(this.selectedRecords);
  }

  public isReleasedOrder(){
    if(this.scheduleList.filter(ele => ele.schdStatus == this.smsStatus.Release_to_SMS).length > 0){
      return true;
    }else{
      return false;
    }
  }



  //build unplanned heat 10-12-2021
  public buildHeat(plannedStr) {
    // selected records from caster schedule list will be sent to this API
    if(this.selCasterSchListError(false, this.scheduleList)){ return; }
    //let schList = plannedStr == 'planned' ? this.scheduleList : this.scheduleList.filter(ele => ele.schdStatus != 'releas_sms')
    this.ppcService
      .buildHeat(
        this.scheduleList, // this.selectedSchList,
        this.sharedService.loggedInUserDetails.userId,
        plannedStr
      )
      .subscribe((data) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: 'Heat is built successfully' }
        );
        //this.heatLines = data;
       // this.setHeatLines.next(data);
        //this.getScheduleStatusBySchId(this.scheduleDetails.schdID);
        //this.unplannedHeatAdded = false;
        //this.refreshScheduleHeader();
        //this.clearSchListSelection(true);
        this.ppcService.setClearAction(true);
        //this.getScheduleDetails();
        //this.selectedSchList = [];
      });
  }
  public selCasterSchListError(isDeleteAction, schList){
    if(schList && schList.length){
      if(!isDeleteAction){
        let tempArr = this.validateRequiredFields(schList);
        if(tempArr.length){
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please enter required fields in Caster Schedule List in correct format'});
          return true;
        }
      }
    }
    else {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning,
        { message: 'Please select caster schedules' }
      );
      return true;
    }
    return false;
  }

  public validateRequiredFields(data){
    // let tempArr = data.filter(ele => !ele.productLength || (ele.productLength && ((ele.productLength < ele.cutLengthMin) || (ele.productLength > ele.cutLengthMax))) ||
    //                                                    !ele.heatQty || !ele.scheduleQty || (ele.scheduleQty && (ele.scheduleQty > ele.btc)) ||
    //                                                    !ele.schedulePcs || (ele.isFlyRequired == 'Y' && !ele.flyRemarks));
    let tempArr = this.scheduleList.filter(ele => !ele.productLength || (ele.productLength && this.validationFlags.cutLengthEnabled && ((ele.productLength < ele.cutLengthMin) || (ele.productLength > ele.cutLengthMax))) ||
                                                       !ele.heatQty || !ele.scheduleQty || (ele.scheduleQty && ele.schdStatus != SMSStatus.Release_to_SMS && (ele.scheduleQty > (ele.btc + ele.originalSchQty))) ||
                                                       !ele.schedulePcs || (ele.isFlyRequired == 'Y' && !ele.flyRemarks) ||
                                                       (ele.oncePieceWeight && this.validationFlags.onePieceWtEnabled && ((ele.oncePieceWeight < ele.productWeightMin) || (ele.oncePieceWeight > ele.productWeightMax))));
    return tempArr;
  }

}
