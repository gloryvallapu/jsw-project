import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { TableType, ColumnType } from 'src/assets/enums/common-enum';
import { PpcService } from '../../../../services/ppc.service';
import { Subscription, Observable } from 'rxjs';
import { SharedService } from 'src/app/shared/services/shared.service';

@Component({
  selector: 'app-heat-and-batch-lines',
  templateUrl: './heat-and-batch-lines.component.html',
  styleUrls: ['./heat-and-batch-lines.component.css'],
})
export class HeatAndBatchLinesComponent implements OnInit {
  public heatLineColumns: ITableHeader[] = [];
  public batchColumns: ITableHeader[] = [];
  public originalBatchColumns: ITableHeader[] = [];
  public heatLineData: any = [];
  @Input() public disableSchActions: boolean;
  @Input() setHeatLinesEvent: Observable<boolean>;
  @Input() isSaveBtn: boolean = false;
  public batchLineData: any = [];
  public viewType = ComponentType;
  public tableType: any;
  public subscription: Subscription;
  public eventSubscription: Subscription;
  public selectedRecord: any;
  public tdClass ="height-max-content pt-0 pb-0";
  public enableRelAbortBtn = false;
  //refresh grid
  @Output() public refreshSchList = new EventEmitter<Event>();
  constructor(public ppcService: PpcService, public sharedService: SharedService) {}

  ngOnInit(): void {
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.heatLineColumns = [
      {
        field: 'radio',
        header: '',
        columnType: ColumnType.radio,
        width: '50px',
        sortFieldName: ''
      },
      {
        field: 'heatSrNo',
        header: 'Sr. No',
        columnType: ColumnType.number,
        width: '60px',
        sortFieldName: '',
      },
      {
        field: 'planHeatId',
        header: 'Plnd Heat ID',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'heatGrade',
        header: 'Heat Grade',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      // {
      //   field: 'heatWidth',
      //   header: 'Heat Width',
      //   columnType: ColumnType.number,
      //   width: '50px',
      //   sortFieldName: '',
      // },
      // { field: 'heatQty', header: 'Heat Qty', columnType: ColumnType.number, width: '50px', sortFieldName: ''},
      {
        field: 'plannedStartDate',
        header: 'Plnd Start Dt',
        columnType: ColumnType.date,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'plannedEndDate',
        header: 'Plnd End Dt',
        columnType: ColumnType.date,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'actualStartDate',
        header: 'Act. Start Date',
        columnType: ColumnType.date,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'actualEndDate',
        header: 'Act. End Date',
        columnType: ColumnType.date,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'actualHeatId',
        header: 'Act. Heat ID',
        columnType: ColumnType.number,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'isPlannedHeat',
        header: 'Is Plnd Heat',
        columnType: ColumnType.dropdown,
        width: '115px',
        sortFieldName: '',
        list: [{
          modelValue: 'N',
          displayName: 'No'
        },
        {
          modelValue: 'Y',
          displayName: 'Yes'
        }],
        disableCol: true
      },
      {
        field: 'unPlannedHeatRemark',
        header: 'Un-Plnd Heat Remarks',
        columnType: ColumnType.textField,
        width: '170px',
        sortFieldName: ''
      },
      {
        field: 'heatLineStatus',
        header: 'Heat Line Status',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'scheduleId',
        header: 'Schd ID',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
    ];

    this.originalBatchColumns = [
      {
        field: 'orderId',
        header: 'Sales Order',
        columnType: ColumnType.number,
        width: '130px',
        sortFieldName: '',
      },
      // {
      //   field: 'orderLineId',
      //   header: 'Order Line ID',
      //   columnType: ColumnType.number,
      //   width: '120px',
      //   sortFieldName: '',
      // },
      {
        field: 'batchLength',
        header: 'Batch Len(mm)',
        columnType: ColumnType.number,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'batchQty',
        header: 'Batch Qty',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'totalBatchWight',
        header: 'Total Batch Wt(tons)',
        columnType: ColumnType.number,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'batchWeight',
        header: '1 Batch Wt(tons)',
        columnType: ColumnType.number,
        width: '115px',
        sortFieldName: '',
      },

      {
        field: 'planHeatId',
        header: 'Pln Heat ID',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Prod. Type',
        columnType: ColumnType.string,
        width: '85px',
        sortFieldName: '',
      },
      {
        field: 'productSize',
        header: 'Prod. Size',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
      },
      // {
      //   field: 'batchThick',
      //   header: 'Batch Thickness',
      //   columnType: ColumnType.number,
      //   width: '150px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'batchWidth',
      //   header: 'Batch Width',
      //   columnType: ColumnType.number,
      //   width: '120px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'batchDiameter',
      //   header: 'Batch Diameter',
      //   columnType: ColumnType.number,
      //   width: '150px',
      //   sortFieldName: '',
      // },
      
      
      
      
      
      {
        field: 'batchGrade',
        header: 'Batch Grade',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'batchPsn',
        header: 'Batch PSN',
        columnType: ColumnType.number,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'tcmRemark',
        header: 'TCM Remarks',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'planOrderId',
        header: 'PO ID',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'schdId',
        header: 'Schd ID',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'actualBatchNo',
        header: 'Act. Batch No',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
      },
    ];
    this.batchColumns = JSON.parse(JSON.stringify(this.originalBatchColumns));
    this.subscription = this.ppcService.getClearAction.subscribe((data) => {
      if (data) {
        this.batchLineData = [];
        this.ppcService.setClearAction(false);
      }
    });
    this.eventSubscription = this.setHeatLinesEvent.subscribe((data) => {
      this.heatLineData = this.sharedService.sortData(data, 'heatSrNo');
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.eventSubscription.unsubscribe();
  }

  public getSelectedRecord($event) {
    if ($event && $event.planHeatId) {
      this.getBatchLineData($event.planHeatId);
      this.selectedRecord = $event;
      let checkisPlannedHeatFlag = this.selectedRecord.isPlannedHeat == 'Y'? false: true;
      // if(checkisPlannedHeatFlag && (this.selectedRecord.heatLineStatus == null)){
      //   //if(checkisPlannedHeatFlag ){
      //   this.enableRelAbortBtn = true;
      // }
      //commented as new requirement to enable release on status of un condition heat new added and built with status 'new unplanned heat'
      if(checkisPlannedHeatFlag && (this.selectedRecord.heatLineStatus == null || this.selectedRecord.heatLineStatus == 'New Unplanned Heat')){
        //if(checkisPlannedHeatFlag ){
        this.enableRelAbortBtn = true;
      }
      
    }
  }

  public getBatchLineData(heatId: any) {
    this.ppcService.getBatchLinesByHeatId(heatId).subscribe((data) => {
      if(data && data.length > 0){
        this.batchLineData = data;
        // this.batchLineData = data.filter(ele => (ele.batchQty && (ele.batchQty > 0)));
        // let temp = this.batchLineData.filter(ele => ele.productType.toLowerCase().includes('billet') || ele.productType.toLowerCase().includes('bloom'));
        // if(temp.length > 0){
        //   this.batchColumns = JSON.parse(JSON.stringify(this.originalBatchColumns.filter(ele => ele.field != 'batchDiameter')))
        // }else {
        //   let temp1 = this.batchLineData.filter(ele => ele.productType.toLowerCase().includes('round'));
        //   if(temp1.length > 0){
        //     this.batchColumns = JSON.parse(JSON.stringify(this.originalBatchColumns.filter(ele => !(ele.field == 'batchThick' || ele.field == 'batchWidth'))))
        //   }
        // }
      }
    });
  }

  public saveHeatLines(event){
    this.ppcService.saveHeatLines(this.sharedService.loggedInUserDetails.userId, this.heatLineData).subscribe(
      data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Heat details saved successfully'});
        this.getHeatLines();
      }
    )
  }

  public releaseHeatLines(event){
    this.ppcService.releaseHeatLines(this.selectedRecord.scheduleId,
      this.selectedRecord.planHeatId,this.sharedService.loggedInUserDetails.userId).subscribe((data)=>{
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: data }
        );
        this.refreshSchList.emit();
        this.getHeatLines();
      })
  }

  public abortHeatLines(event){
    this.ppcService.aborteHeatLines(this.selectedRecord.scheduleId,
      this.selectedRecord.planHeatId,this.sharedService.loggedInUserDetails.userId).subscribe((data)=>{
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: data }
        );
        this.refreshSchList.emit();
        this.getHeatLines();
      })
  }

  public getHeatLines(){
    this.enableRelAbortBtn = false;
    this.ppcService.getHeatLinesBySchId(this.heatLineData[0].scheduleId).subscribe(data => {
      this.heatLineData = data;
    })
  }
}
