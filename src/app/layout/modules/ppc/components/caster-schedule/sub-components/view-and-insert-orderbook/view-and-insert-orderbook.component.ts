import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { CasterScheduleForms } from '../../../../form-objects/ppc-form-objects';
import { JswFormComponent, JswCoreService } from 'jsw-core';
import { PpcService } from '../../../../services/ppc.service';
import { SharedService } from 'src/app/shared/services/shared.service';

@Component({
  selector: 'app-view-and-insert-orderbook',
  templateUrl: './view-and-insert-orderbook.component.html',
  styleUrls: ['./view-and-insert-orderbook.component.css']
})
export class ViewAndInsertOrderbookComponent implements OnInit {
  public viewType = ComponentType;
  public filterForm: any;
  //public scheduleDetails: any;
  public orderList: any = [];
  public originalOrderList: any = [];
  public gradeGroupList: any = [];
  public isRetrieveBtn: boolean = false;
  public isRefreshBtn: boolean = false;
  public isInsertOrderBtn: boolean = false;
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, public casterScheduleForm: CasterScheduleForms, public jswComponent:JswFormComponent,
              public jswService:JswCoreService, public ppcService: PpcService, public sharedService: SharedService) { }

  ngOnInit(): void {
    this.getGradeGropus();
    this.filterForm = JSON.parse(JSON.stringify(this.casterScheduleForm.viewAndInsertFilterForm));
    this.filterForm.formFields.filter(ele => ele.ref_key == 'scheduleId')[0].value = this.config.data.scheduleId;
    this.filterForm.formFields.filter(ele => ele.ref_key == 'productName')[0].value = this.config.data.productName;
    this.filterForm.formFields.filter(ele => ele.ref_key == 'productSize')[0].value = this.config.data.sizeCode;
    this.filterForm.formFields.filter(ele => ele.ref_key == 'gradeGroup')[0].value = this.config.data.gradeGroup;
    this.filterForm.formFields.filter(ele => ele.ref_key == 'gradeCategory')[0].value = this.config.data.gradeCategory;
    this.isRetrieveBtn = this.config.data.isRetrieveBtn;
    this.isRefreshBtn = this.config.data.isRefreshBtn;
    this.isInsertOrderBtn = this.config.data.isInsertOrderBtn;
    this.retrieveOrders();
  }

  public retrieveOrders(){
    this.jswComponent.onSubmit(this.filterForm.formName, this.filterForm.formFields);
    var formData = this.jswService.getFormData();
    if(formData && formData.length){
      var retriveParams = {
        productType: this.filterForm.formFields.filter(ele => ele.ref_key == 'productName')[0].value,
        productSize: this.filterForm.formFields.filter(ele => ele.ref_key == 'productSize')[0].value,
        gradeCategory: this.filterForm.formFields.filter(ele => ele.ref_key == 'gradeCategory')[0].value ? this.filterForm.formFields.filter(ele => ele.ref_key == 'gradeCategory')[0].value : 'null',
        gradeGroup: this.filterForm.formFields.filter(ele => ele.ref_key == 'gradeGroup')[0].value ? this.filterForm.formFields.filter(ele => ele.ref_key == 'gradeGroup')[0].value : 'null',
        schdId: this.config.data.scheduleId,
        schdStatus: this.config.data.schdStatus
      }
        if(this.config.data.scheduleType == 'lp'){
          this.lpscheduleWiseData(retriveParams);
        }

        if(this.config.data.scheduleType == 'caster'){
          this.casterSchWiseData(retriveParams);
        }
    }else{
      if(!this.config.data.productName || !this.config.data.sizeCode){
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please check if product and sizecode is assigned to schedule'});
      }
    }
  }

  public casterSchWiseData(retriveParams){
    this.ppcService.getFilteredCasterOrderBook(retriveParams).subscribe(Response => {
      this.originalOrderList = Response;
      var modifiedData = Response.map((x: any) => {
        x.reqDeliveryDate = new Date(this.sharedService.get_MM_DD_YYYY_Date(x.reqDeliveryDate));
        x.soReleaseDate = new Date(this.sharedService.get_MM_DD_YYYY_Date(x.soReleaseDate));
        return x;
      });
      this.ppcService.setOrderList(modifiedData);
      //this.orderList = modifiedData;
    })
  }

  public lpscheduleWiseData(retriveParams){
    this.ppcService.getFilteredOrderBookLpUnplanned(retriveParams).subscribe(Response => {
      this.originalOrderList = Response;
      var modifiedData = Response.map((x: any) => {
        x.reqDeliveryDate = new Date(this.sharedService.get_MM_DD_YYYY_Date(x.reqDeliveryDate));
        x.soReleaseDate = new Date(this.sharedService.get_MM_DD_YYYY_Date(x.soReleaseDate));
        return x;
      });
      this.ppcService.setOrderList(modifiedData);
      //this.orderList = modifiedData;
    })
  }


  public getGradeGropus(){
    this.ppcService.getGradeGroupsCategory().subscribe(data => {
      this.gradeGroupList = data;
      this.filterForm.formFields.filter(ele => ele.ref_key == 'gradeGroup')[0].list = data.map((x: any) => {
        return { modelValue: x.gradeGroup, displayName: x.gradeGroup}
      })
    })
  }

  public refreshOrders(){
    this.retrieveOrders();
    if(this.config.data.scheduleType == 'lp'){
      this.filterForm.formFields.filter(ele => ele.ref_key == 'gradeGroup')[0].value = "null";
    this.filterForm.formFields.filter(ele => ele.ref_key == 'gradeCategory')[0].value = "nul";
    }
  }

  public dropdownChangeEvent(data){
    if (data.value) {
      this.getCategoriesByGroup(data.value);
    }
  }

  public getCategoriesByGroup(id){
    let categories = this.gradeGroupList.filter(ele => ele.gradeGroup == id)[0].gradeCategoryList;
    let categoryObj = this.filterForm.formFields.filter(ele => ele.ref_key == 'gradeCategory')[0];
    categoryObj.list = categories.map((x: any) => { return { displayName: x, modelValue: x}})
    categoryObj.value = categoryObj.list[0].modelValue;
  }

  public closeModal() {
    this.ref.close(this.config.data);
  }

}
