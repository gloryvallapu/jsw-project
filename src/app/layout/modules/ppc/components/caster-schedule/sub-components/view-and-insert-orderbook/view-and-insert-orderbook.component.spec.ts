import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAndInsertOrderbookComponent } from './view-and-insert-orderbook.component';

describe('ViewAndInsertOrderbookComponent', () => {
  let component: ViewAndInsertOrderbookComponent;
  let fixture: ComponentFixture<ViewAndInsertOrderbookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewAndInsertOrderbookComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAndInsertOrderbookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
