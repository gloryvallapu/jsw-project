import { Component, OnInit } from '@angular/core';
import { CasterScheduleForms } from '../../form-objects/ppc-form-objects';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { PpcService } from '../../services/ppc.service';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  WorkCenter,
  Product,
  SizeCode,
  SMSAction,
  SMSStatus
} from 'src/assets/enums/common-enum';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ViewAndInsertOrderbookComponent } from './sub-components/view-and-insert-orderbook/view-and-insert-orderbook.component';
import { AddUnplannedHeatComponent } from '../caster-schedule/sub-components/add-unplanned-heat/add-unplanned-heat.component';
import { Subject } from 'rxjs';
import { ConfirmationService } from 'primeng/api';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-caster-schedule',
  templateUrl: './caster-schedule.component.html',
  styleUrls: ['./caster-schedule.component.css'],
  providers: [DialogService, ConfirmationService],
})
export class CasterScheduleComponent implements OnInit {
  public scheduleIdList: any = [];
  public selectedScheduleId: any = null;
  public scheduleStatus: any;
  public selectedAction: any;
  public actionList: any = [];
  public scheduleHeaderFormObject: any;
  public viewType = ComponentType;
  public newScheduleDetails: any;
  public productList: any = [];
  public workCenterList: any = [];
  public scheduleList: any = [];
  public isSearchTriggered: boolean = false;
  public isPerformAction: boolean = false;
  public insertOrderBookModalRef: DynamicDialogRef;
  public insertUnplannedOrderBookModalRef: DynamicDialogRef;
  public scheduleDetails: any;
  public selectedSchList: any = [];
  public scheduleStatusList: any = [];
  public disableSchActions: boolean = false;
  public heatLines: any = [];
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public setHeatLines: Subject<any> = new Subject<any>();
  public screenStructure: any = [];
  public isSearchBtn: boolean = false;
  public isProceedBtn: boolean = false;
  public isSaveSchHeaderBtn: boolean = false;
  public isResetSchHeaderBtn: boolean = false;
  public isSchListSection: boolean = false;
  public isSchListSaveBtn: boolean = false;
  public isHeatLinesSection: boolean = false;
  public isHeatLinesSaveBtn: boolean = false;
  public isViewInsertSection: boolean = false;
  public isAddUnplannedSection: boolean = false;
  public menuConstants = menuConstants;
  public userId:string;
  public validationFlags = {
    cutLengthEnabled: false,
    incompleteSeqRemarkEnabled: false,
    onePieceWtEnabled: false,
    plannedDateEnabled: false,
    soWiseWcEnabled: false
  };
  public formRef: NgForm;

  //auto build unplanned heat
  public unplannedHeatAdded: boolean = false;
  constructor(
    public casterScheduleForm: CasterScheduleForms,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public ppcService: PpcService,
    public commonService: CommonApiService,
    public dialogService: DialogService,
    private confirmationService: ConfirmationService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.Caster_Schedule.id)[0];
    this.isSearchBtn = this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.Action_Line.buttons.search, menuConstants.casterSchSectionIds.Action_Line.id, this.screenStructure);
    this.isProceedBtn = this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.Action_Line.buttons.proceed, menuConstants.casterSchSectionIds.Action_Line.id, this.screenStructure);
    this.isSaveSchHeaderBtn = this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.Schedule_Header.buttons.save, menuConstants.casterSchSectionIds.Schedule_Header.id, this.screenStructure);
    this.isResetSchHeaderBtn = this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.Schedule_Header.buttons.reset, menuConstants.casterSchSectionIds.Schedule_Header.id, this.screenStructure);
    this.isSchListSection = this.sharedService.isSectionVisible(menuConstants.casterSchSectionIds.Schedule_List.id, this.screenStructure);
    this.isSchListSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.Schedule_List.buttons.save, menuConstants.casterSchSectionIds.Schedule_List.id, this.screenStructure);
    this.isHeatLinesSection = this.sharedService.isSectionVisible(menuConstants.casterSchSectionIds.Heat_Line.id, this.screenStructure);
    this.isHeatLinesSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.Heat_Line.buttons.save, menuConstants.casterSchSectionIds.Heat_Line.id, this.screenStructure);
    this.getEnabledValidations();
    this.getSchHeaderForm();
    this.setDefaultActionList();
    this.selectedAction = this.actionList[0].modelValue;
    this.getWorkCenters();
    // this.getProducts();
    this.getScheduleStatus();
    this.getScheduleIds();
  }

  ngOnDestroy() {
    if (this.insertOrderBookModalRef) {
      this.insertOrderBookModalRef.close();
    }
    if (this.insertUnplannedOrderBookModalRef) {
      this.insertUnplannedOrderBookModalRef.close();
    }
  }

  public getEnabledValidations(){
    this.ppcService.getCasterValidations().subscribe(data => {
      this.validationFlags.cutLengthEnabled = data.cutLengthEnabled.toLowerCase() == 'y' ? true : false;
      this.validationFlags.incompleteSeqRemarkEnabled = data.incompleteSeqRemarkEnabled.toLowerCase() == 'y' ? true : false;
      this.validationFlags.onePieceWtEnabled = data.onePieceWtEnabled.toLowerCase() == 'y' ? true : false;
      this.validationFlags.plannedDateEnabled = data.plannedDateEnabled.toLowerCase() == 'y' ? true : false;
      this.validationFlags.soWiseWcEnabled = data.soWiseWcEnabled.toLowerCase() == 'y' ? true : false;
    })
  }

  public getSchHeaderForm() {
    this.scheduleHeaderFormObject = JSON.parse(
      JSON.stringify(this.casterScheduleForm.scheduleHeaderForm)
    );
  }

  public getWorkCenters() {
    this.commonService.getWCBasedProductSizeList().subscribe((Response) => {
      this.workCenterList = Response.filter(
        (ele: any) => ele.wcType == 'SMS'
      )[0].wcNameList;
      this.scheduleHeaderFormObject.formFields.filter(
        (ele) => ele.ref_key == 'casterWC'
      )[0].list = this.workCenterList && this.workCenterList.length ? this.sharedService.getDropdownData(this.workCenterList, WorkCenter.displayName, WorkCenter.modelValue) : [];
    });
  }

  public getProducts(selectedUnit) {
    this.productList = this.workCenterList.filter(
      (wc: any) => wc.wcCode == selectedUnit
    )[0].productList;
    this.scheduleHeaderFormObject.formFields.filter(
      (ele) => ele.ref_key == 'productId'
    )[0].list = this.sharedService.getDropdownData(
      this.productList,
      Product.displayName,
      Product.modelValue
    );
  }

  public getScheduleStatus() {
    this.ppcService.getCasterSchStatusList().subscribe((data) => {
      this.scheduleStatusList = data;
      this.scheduleHeaderFormObject.formFields.filter(
        (ele) => ele.ref_key == 'schdStatus'
      )[0].list = this.sharedService.getDropdownData(
        this.scheduleStatusList,
        'objectStatus',
        'objectStatusId'
      );
    });
  }

  public getScheduleIds() {
    this.ppcService.getScheduleIdList().subscribe((data) => {
      this.scheduleIdList = data.map((x: any) => {
        return { displayName: x, modelValue: x };
      });
    });
  }

  public dropdownChangeEvent(data) {
    if (data.ref_key == 'casterWC') {
      this.getProducts(data.value);
    }

    if (data.ref_key == 'productId') {
      this.getSizeCodesByProduct(data.value);
    }
  }

  public getSizeCodesByProduct(id) {
    let sizeCodes = this.productList.filter((ele) => ele.productId == id)[0]
      .sizeList;
    let sizeCodeObj = this.scheduleHeaderFormObject.formFields.filter(
      (ele) => ele.ref_key == 'sizeCode'
    )[0];
    sizeCodeObj.list = sizeCodes.map((x: any) => {
      return {
        displayName: x.sizeDesc,
        modelValue: x.sizeCode,
      };
    });
    //sizeCodeObj.value = sizeCodeObj.list[0].modelValue;
  }

  public getScheduleDetails(): any {
    // API call to fetch the Schedule Header Details
    let schId = this.selectedScheduleId;
    this.clear();
    this.selectedScheduleId = schId;
    this.isSearchTriggered = true;
    if (!this.selectedScheduleId) {
      return;
    }
    this.getSchDetailsBySchId(this.selectedScheduleId);
  }

  public getSchDetailsBySchId(scheduleId) {
    this.ppcService
      .getScheduleDetailsById(scheduleId)
      .subscribe((data): any => {
        this.scheduleDetails = data;
        //this.getSchHeaderForm(); removed as dropdown list is set as empty
        //this.resetForm();
        this.scheduleHeaderFormObject.formFields = this.sharedService.mapResponseToFormObj(
          this.scheduleHeaderFormObject.formFields,
          data
        );
        let selectedUnit = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'casterWC')[0].value;
        if (selectedUnit) {
          this.getProducts(selectedUnit); // product list binding to product dropdown based on unit ""
          let prodId = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'productId')[0].value;
          let sizeCodeObj = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'sizeCode')[0];
          if (prodId) {
            let sizeCodes = this.productList.filter(
              (ele) => ele.productId == prodId
            )[0].sizeList;
            sizeCodeObj.list = this.sharedService.getDropdownData(
              sizeCodes,
              SizeCode.displayName,
              SizeCode.modelValue
            );
          } else {
            sizeCodeObj.list = [];
          }
        } else {
          this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'productId')[0].list = [];
          this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'sizeCode')[0].list = [];
        }
        if(data.schdStatus == SMSStatus.Created_Schedule && !this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'schdDate')[0].value){
          this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'schdDate')[0].value = new Date();
        }
        if(data.schdStatus == SMSStatus.Created_Schedule && !this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'planStartDate')[0].value){
          this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'planStartDate')[0].value = new Date();
        }
        if(data.schdStatus == SMSStatus.Created_Schedule && !this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'setupDuration')[0].value){
          this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'setupDuration')[0].value = 40;
        }
        this.updateScheduleStatus(data.schdStatus);
        this.getScheduleList(scheduleId);
        this.getHeatLinesBasedOnSchId(scheduleId);
        this.setFormFieldsByStatus(data);
      });
  }

  public updateScheduleStatus(statusCode) {
    this.scheduleStatus = this.scheduleStatusList.filter(
      (ele) => ele.objectStatusId == statusCode
    )[0];
    this.scheduleHeaderFormObject.formFields.filter(
      (ele) => ele.ref_key == 'schdStatus'
    )[0].value = statusCode;
    if(this.selectedAction == SMSAction.Delete_Order){
      if(!this.scheduleList.length){
        this.setHeatLines.next([]);
        this.ppcService.setClearAction(true);
        this.getActionList(statusCode);
        return;
      }
      if(statusCode == SMSStatus.Ready_to_Build || statusCode == SMSStatus.Ready_to_Release){
        this.buildHeatAfterDeleteOrd('planned');
      }else if(statusCode == SMSStatus.Ready_to_Build_Unplanned_Heat || statusCode == SMSStatus.Ready_to_Release_Unplanned_Heat){
        this.buildHeatAfterDeleteOrd('unplanned');
      }else{
        let schId = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'schdID')[0].value;
        this.getHeatLinesBasedOnSchId(schId);
      }
    }
    this.getActionList(statusCode);
  }

  public buildHeatAfterDeleteOrd(planUnplanStr){
    if(this.validateRequiredFields(this.scheduleList).length){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning , { message : `Unable to build ${planUnplanStr} heat as required fields are not entered` });
    }else{
      this.buildHeat(planUnplanStr);
    }
  }

  public getScheduleList(scheduleId) {
    // call API to get tha Caster Schedule List
    this.clearSchListSelection(true);
    this.selectedSchList = [];
    this.ppcService.getScheduleList(scheduleId).subscribe((data) => {
      this.scheduleList = data.map((ele: any) => {
        ele.isFlyRequired = ele.isFlyRequired ? ele.isFlyRequired : 'N';
        ele.originalSchQty = ele.scheduleQty;
        return ele;
      });
      this.setFormFieldsByStatus({schdStatus: this.scheduleStatus.objectStatusId});
      // if(this.selectedAction == SMSAction.Delete_Order){
      //   this.getScheduleStatusBySchId(this.scheduleDetails.schdID);
      // }
      //this.disableFormFields(false);
    });
  }

  public getHeatLinesBasedOnSchId(scheduleId){
    this.ppcService.getHeatLinesBySchId(scheduleId).subscribe(
      data => {
        this.heatLines = data;
        this.setHeatLines.next(data);
        this.ppcService.setClearAction(true);
      }
    )
  }

  public getActionList(status) {
    this.selectedAction = null;
    this.isPerformAction = false;
    this.ppcService.getSMSActionList(status).subscribe((data) => {
      this.actionList = data.map((x: any) => {
        return { displayName: x.objectStatus, modelValue: x.objectStatusId };
      });
    });
  }

  public performScheduleAction(): any {
    //this.isPerformAction = true;
    (<HTMLInputElement>document.getElementById(`action`)).classList.remove(
      'ng-dirty'
    );
    if(!this.selectedAction){
      return;
    }
    if (this.selectedAction == SMSAction.Create_Schedule) {
      this.createSchedule();
    }

    if (this.selectedAction == SMSAction.View_and_Insert_Order) {
      let selectedUnit = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'casterWC')[0].value;
      if(this.sharedService.isSectionVisible(this.menuConstants.casterSchSectionIds.View_And_Insert.id, this.screenStructure)){
        if(!selectedUnit){
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "Please save schedule header details"});
          return;
        }else{
          this.unplannedHeatAdded = false;
          this.openInsertOrderBookModal();
        }
      }else{
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access View & Insert Order Book section. Please contact Admin."})
      }
    }

    if (this.selectedAction == SMSAction.Build_Heat) {
      this.buildHeat('planned');
    }

    if (this.selectedAction == SMSAction.Release_Schedule) {
      this.releaseSchedule('planned');
    }

    if (this.selectedAction == SMSAction.Add_Unplanned_Heat) {
      if(this.sharedService.isSectionVisible(this.menuConstants.casterSchSectionIds.Unplanned_Heat.id, this.screenStructure)){
        this.openUnplannedBookModal();
        this.unplannedHeatAdded = true;
      }else{
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access Add Unplanned Heat section. Please contact Admin."})
      }
    }

    if (this.selectedAction == SMSAction.Build_Unplanned_Heat) {
      this.buildHeat('unplanned');
    }

    if (this.selectedAction == SMSAction.Release_Unplanned_Heat) {
      this.releaseSchedule('unplanned');
    }

    if (this.selectedAction == SMSAction.Cancel_Schedule) {
      this.cancelCasterSch();
    }

    if (this.selectedAction == SMSAction.Delete_Order) {
      this.deleteCasterSchOrder();
    }

    if (this.selectedAction == SMSAction.Clear) {
      this.clear();
    }
  }

  public createSchedule(){
    this.selectedScheduleId = null;
    this.clear();
    // this.resetForm();
    this.userId=this.sharedService.loggedInUserDetails.userId;
    this.ppcService.getNewScheduleDetails(this.userId).subscribe((data) => {
      this.newScheduleDetails = data;
      this.scheduleHeaderFormObject.formFields.filter(
        (ele) => ele.ref_key == 'schdID'
      )[0].value = data.schdID;
      this.scheduleHeaderFormObject.formFields.filter(
        (ele) => ele.ref_key == 'schdStatus'
      )[0].value = data.schdStatus;
      this.scheduleHeaderFormObject.formFields.filter(
        (ele) => ele.ref_key == 'schdDate'
      )[0].value = new Date();
      this.scheduleHeaderFormObject.formFields.filter(
        (ele) => ele.ref_key == 'planStartDate'
      )[0].value = new Date();
      this.scheduleHeaderFormObject.formFields.filter(
        (ele) => ele.ref_key == 'setupDuration'
      )[0].value = 40;
      this.getScheduleIds();
      this.updateScheduleStatus(data.schdStatus);
    });
  }

  public openInsertOrderBookModal() {
    this.insertOrderBookModalRef = this.dialogService.open(
      ViewAndInsertOrderbookComponent,
      {
        header: 'View and Insert Order Book',
        width: '80%',
        contentStyle: { 'max-height': '500px', overflow: 'auto' },
        autoZIndex: false,
        data: {
          scheduleType: 'caster',
          scheduleId: this.scheduleDetails.schdID, //this.selectedScheduleId,
          productName: this.productList.filter(
            (ele) => ele.productId == this.scheduleDetails.productId
          )[0].productName,
          sizeCode: this.scheduleDetails.sizeCode,
          schdStatus: this.scheduleStatus.objectStatusId,
          gradeGroup: null,
          gradeCategory: null,
          isRetrieveBtn: this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.View_And_Insert.buttons.retrieve, menuConstants.casterSchSectionIds.View_And_Insert.id, this.screenStructure),
          isRefreshBtn: this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.View_And_Insert.buttons.refresh, menuConstants.casterSchSectionIds.View_And_Insert.id, this.screenStructure),
          isInsertOrderBtn: this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.View_And_Insert.buttons.insertOrder, menuConstants.casterSchSectionIds.View_And_Insert.id, this.screenStructure)
        },
      }
    );

    this.insertOrderBookModalRef.onClose.subscribe((data) => {
      if (data != undefined) {
        //this.getScheduleStatusBySchId(this.scheduleDetails.schdID);
        this.refreshScheduleHeader();
        this.getScheduleList(this.scheduleDetails.schdID);
      }
    });
  }

  public buildHeat(plannedStr) {
    // selected records from caster schedule list will be sent to this API
    if(this.selCasterSchListError(false, this.scheduleList)){ return; }
    //let schList = plannedStr == 'planned' ? this.scheduleList : this.scheduleList.filter(ele => ele.schdStatus != 'releas_sms')
    this.ppcService
      .buildHeat(
        this.scheduleList, // this.selectedSchList,
        this.sharedService.loggedInUserDetails.userId,
        plannedStr
      )
      .subscribe((data) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: 'Heat is built successfully' }
        );
        this.heatLines = data;
        this.setHeatLines.next(data);
        //this.getScheduleStatusBySchId(this.scheduleDetails.schdID);
        this.unplannedHeatAdded = false;
        this.refreshScheduleHeader();
        this.clearSchListSelection(true);
        this.ppcService.setClearAction(true);
        //patch
        this.selectedScheduleId = data[0].scheduleId
        this.getScheduleDetails();
        this.selectedSchList = [];
      });
  }

  public releaseSchedule(planUnplanStr) {
    if(this.validationFlags.plannedDateEnabled && planUnplanStr == 'planned'){
      let planDate = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'planStartDate')[0].value;
      let hours = new Date(this.scheduleDetails.planStartDate).getHours();
      let minutes = new Date(this.scheduleDetails.planStartDate).getMinutes();
      if(new Date(planDate) < new Date()){
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          { message: `Planned Start Date should be greater than Current Date.` }
        );
        //return;
      }else if((new Date(planDate) > new Date()) && (new Date(planDate).getHours() != hours || new Date(planDate).getMinutes() != minutes)){
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          { message: `Please save schedule header details.` }
        );
       // return;
      }
    }
    if(this.validationFlags.incompleteSeqRemarkEnabled && this.heatLines.length < 4){
      let seqRemark = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'incompleteSeqRemark')[0];
      seqRemark.disabled = false;
      if(!seqRemark.value){
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          { message: `Heat count is less than 4. Please enter Sequence Remark.` }
        );
        return;
      }
    }
    if(this.selCasterSchListError(false, this.scheduleList)){ return; }
    let seqRemarkVal = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'incompleteSeqRemark')[0].value;
    this.ppcService
      .releaseCasterSchedule(
        this.scheduleDetails.schdID,
        planUnplanStr,
        this.sharedService.loggedInUserDetails.userId,
        this.scheduleList, // this.selectedSchList
        seqRemarkVal
      )
      .subscribe((data) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: `Schedule ${this.scheduleDetails.schdID} is released successfully` }
        );
        //this.getScheduleStatusBySchId(this.scheduleDetails.schdID);
        this.refreshScheduleHeader();
        this.getScheduleList(this.scheduleDetails.schdID);
        this.clearSchListSelection(true);
        this.selectedSchList = [];
        this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'incompleteSeqRemark')[0].disabled = true;
      });
  }

  public openUnplannedBookModal() {
    this.insertUnplannedOrderBookModalRef = this.dialogService.open(
      AddUnplannedHeatComponent,
      {
        header: 'Add Unplanned Heat',
        width: '80%',
        contentStyle: {
          'max-height': '500px',
          overflow: 'auto',
        },
        autoZIndex: false,
        data: {
          scheduleType: 'caster',
          scheduleId: this.scheduleDetails.schdID, //this.selectedScheduleId,
          productName: this.productList.filter(
            (ele) => ele.productId == this.scheduleDetails.productId
          )[0].productName,
          sizeCode: this.scheduleDetails.sizeCode,
          schdStatus: this.scheduleStatus.objectStatusId, //this.scheduleDetails.schdStatus,
          gradeGroup: null,
          gradeCategory: null,
          isRetrieveBtn: this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.Unplanned_Heat.buttons.retrieve, menuConstants.casterSchSectionIds.View_And_Insert.id, this.screenStructure),
          isRefreshBtn: this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.Unplanned_Heat.buttons.refresh, menuConstants.casterSchSectionIds.View_And_Insert.id, this.screenStructure),
          isInsertOrderBtn: this.sharedService.isButtonVisible(true, menuConstants.casterSchSectionIds.Unplanned_Heat.buttons.insertOrder, menuConstants.casterSchSectionIds.View_And_Insert.id, this.screenStructure)
        },
      }
    );

    this.insertUnplannedOrderBookModalRef.onClose.subscribe((data) => {
      if (data != undefined) {
        this.getScheduleStatusBySchId(this.scheduleDetails.schdID);
        this.getScheduleList(this.scheduleDetails.schdID);
      }
    });
  }

  public cancelCasterSch() {
    this.ppcService
      .cancelCasterSchedule(
        this.scheduleDetails.schdID,
        this.sharedService.loggedInUserDetails.userId
      )
      .subscribe((data) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: `Schedule ${this.scheduleDetails.schdID} cancelled successfully` }
        );
        this.refreshScheduleHeader();
        // this.disableSchActions = true;
        // this.disableFormFields(true);
        // this.getScheduleStatusBySchId(this.scheduleDetails.schdID);
      });
  }

  public deleteCasterSchOrder(){
    // API call to delete caster sch order.. ganesh has to update the API
    if(this.selCasterSchListError(true, this.selectedSchList)){ return; }
    this.getDeleteConfirmation();
  }

  public getDeleteConfirmation(): any {
    let orders = this.selectedSchList.map((ele: any) => {
      return ele.orderId
    })

    this.confirmationService.confirm({
      message: `<span class="font-16 text-grey">Do you want to delete <h7 class = "font-weight-bold text-grey mb-3">${orders}</h7> Order(s)?</span>`,
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        var orderSeqArr = this.selectedSchList.map(element => {
          return element.ordSeqNo;
        });
        this.ppcService.deleteCasterSchOrder(this.sharedService.loggedInUserDetails.userId, this.scheduleDetails.schdID, orderSeqArr).subscribe(data => {
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Selected orders deleted successfully'});
          this.ppcService.getScheduleDetailsById(this.scheduleDetails.schdID).subscribe((data): any => {
            this.scheduleDetails = data;
            this.scheduleHeaderFormObject.formFields = this.sharedService.mapResponseToFormObj(
              this.scheduleHeaderFormObject.formFields,
              data
            );
            this.clearSchListSelection(true);
            this.selectedSchList = [];
            this.ppcService.getScheduleList(data.schdID).subscribe((data) => {
              this.scheduleList = data.map((ele: any) => {
                ele.isFlyRequired = ele.isFlyRequired ? ele.isFlyRequired : 'N';
                ele.originalSchQty = ele.scheduleQty;
                return ele;
              });
              this.getScheduleStatusBySchId(this.scheduleDetails.schdID);
              this.setFormFieldsByStatus({schdStatus: this.scheduleStatus.objectStatusId});
          });
        });
        })
      },
      reject: () => {
        this.selectedSchList = [];
        this.clearSchListSelection(true);
      }
    });
  }

  public selCasterSchListError(isDeleteAction, schList){
    if(schList && schList.length){
      if(!isDeleteAction){
        let tempArr = this.validateRequiredFields(schList);
        if(tempArr.length){
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please enter required fields in Caster Schedule List in correct format'});
          return true;
        }
      }
    }
    else {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning,
        { message: 'Please select caster schedules' }
      );
      return true;
    }
    return false;
  }

  public validateRequiredFields(data){
    // let tempArr = data.filter(ele => !ele.productLength || (ele.productLength && ((ele.productLength < ele.cutLengthMin) || (ele.productLength > ele.cutLengthMax))) ||
    //                                                    !ele.heatQty || !ele.scheduleQty || (ele.scheduleQty && (ele.scheduleQty > ele.btc)) ||
    //                                                    !ele.schedulePcs || (ele.isFlyRequired == 'Y' && !ele.flyRemarks));
    let tempArr = this.scheduleList.filter(ele => !ele.productLength || (ele.productLength && this.validationFlags.cutLengthEnabled && ((ele.productLength < ele.cutLengthMin) || (ele.productLength > ele.cutLengthMax))) ||
                                                       !ele.heatQty || !ele.scheduleQty || (ele.scheduleQty && ele.schdStatus != SMSStatus.Release_to_SMS && (ele.scheduleQty > (ele.btc + ele.originalSchQty))) ||
                                                       !ele.schedulePcs || (ele.isFlyRequired == 'Y' && !ele.flyRemarks) ||
                                                       (ele.oncePieceWeight && this.validationFlags.onePieceWtEnabled && ((ele.oncePieceWeight < ele.productWeightMin) || (ele.oncePieceWeight > ele.productWeightMax))));
    return tempArr;
  }

  public disableFormFields(isDisableAll) {
    if(isDisableAll){
      this.scheduleHeaderFormObject.formFields.forEach((element) => {
        element.disabled = true;
      });
    }else{
      this.scheduleHeaderFormObject.formFields.forEach((element) => {
        if (element.ref_key == 'casterWC' || element.ref_key == 'productId' || element.ref_key == 'sizeCode') {
          element.disabled = this.scheduleList.length ? true : false;
        }
      });
    }
  }

  public enableFormFields() {
    this.scheduleHeaderFormObject.formFields.forEach((element) => {
      if (element.ref_key == 'casterWC' ||
        element.ref_key == 'productId' ||
        element.ref_key == 'sizeCode' ||
        element.ref_key == 'planStartDate' || element.ref_key == 'setupDuration'
      ) {
        element.disabled = false;
      }
    });
  }

  public getScheduleStatusBySchId(schId) {
    this.ppcService.getScheduleStatusById(schId).subscribe((data) => {
      this.updateScheduleStatus(data.schdStatus);
    });
  }

  public saveScheduleHeader() {
    this.jswComponent.onSubmit(
      this.scheduleHeaderFormObject.formName,
      this.scheduleHeaderFormObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let obj = {
        schdID: null,
        productId: null,
        sizeCode: '',
        casterWC: '',
        planStartDate: '',
        schdDate: '',
        schdStatus: '',
        setupDuration: '',
        planEndDate: '',
        incompleteSeqRemark: ''
      };
      //let obj = formData.map((ele) => ele.ref_key);
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, obj);
      let reqBodyObj = {
        schdID: formObj.schdID,
        productId: formObj.productId,
        sizeCode: formObj.sizeCode,
        casterWC: formObj.casterWC,
        planStartDate: formObj.planStartDate,
        schdDate: formObj.schdDate,
        schdStatus: formObj.schdStatus,
        setupDuration: formObj.setupDuration,
        planEndDate: formObj.planEndDate ? formObj.planEndDate : null,
        incompleteSeqRemark: formObj.incompleteSeqRemark ? formObj.incompleteSeqRemark : null
      };
      this.ppcService.saveScheduleDetails(reqBodyObj).subscribe((data) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: 'Schedule details are saved successfully' }
        );
        this.getSchDetailsBySchId(data.schdID);
        // this.scheduleDetails = data;
        // this.scheduleStatus = this.scheduleStatusList.filter(
        //   (ele) => ele.objectStatusId == this.scheduleDetails.schdStatus
        // )[0];
        // this.getActionList(data.schdStatus);
        // this.disableFormFields(false);
      });
    }
  }

  public clear() {
    this.isSearchTriggered = false;
    this.isPerformAction = false;
    //this.selectedScheduleId = null;
    this.disableSchActions = false;
    this.scheduleStatus = null;
    this.setDefaultActionList();
    this.selectedAction = this.actionList[0].modelValue;
    this.scheduleList = [];
    this.selectedSchList = [];
    this.heatLines = [];
    this.setHeatLines.next([]);
    (<HTMLInputElement>document.getElementById(`schId`)).classList.remove(
      'ng-dirty'
    );
    (<HTMLInputElement>document.getElementById(`action`)).classList.remove(
      'ng-dirty'
    );
    //this.jswComponent.resetForm(this.scheduleHeaderFormObject);
    this.resetForm();
    this.enableFormFields();
    this.ppcService.setClearAction(true);
  }

  public resetForm(){
    this.sharedService.resetForm(this.scheduleHeaderFormObject, this.formRef);
  }

  public getCasterSchList(data) {
    this.selectedSchList = data;
    // if(this.unplannedHeatAdded){
    //   this.buildHeat('unplanned');
    // }

  }

  //run unPlanned heat
  public runUnplannedHeat(data) {
    this.selectedSchList = data;
    this.buildHeat('unplanned');

  }

  public refreshSchList(){
    this.getScheduleList(this.scheduleDetails.schdID)
    this.refreshScheduleHeader();
  }

  public setDefaultActionList(){
    this.actionList = [
      { displayName: 'New Schedule', modelValue: SMSAction.Create_Schedule },
    ];
  }

  public clearSchListSelection(isClrSel){
    this.clearSelection.next(isClrSel);
  }

  public getFormRef(event){
    this.formRef = event;
  }

  public refreshScheduleHeader(){
    this.ppcService.getScheduleDetailsById(this.scheduleDetails.schdID).subscribe((data): any => {
        // this.resetForm();
        this.scheduleDetails = data;
        //this.getSchHeaderForm(); removed as dropdown list is set as empty
        this.scheduleHeaderFormObject.formFields = this.sharedService.mapResponseToFormObj(
          this.scheduleHeaderFormObject.formFields,
          data
        );
        this.updateScheduleStatus(data.schdStatus);
        this.setFormFieldsByStatus(data);
    });
  }

  public setFormFieldsByStatus(data){
    let selectedUnit = this.scheduleHeaderFormObject.formFields.filter((ele) => ele.ref_key == 'casterWC')[0].value;
    if (data.schdStatus == SMSAction.Cancel_Schedule || data.schdStatus == SMSStatus.Schedule_Completed) {
      this.disableFormFields(true);
      this.disableSchActions = true;
    }
    else if(data.schdStatus == SMSStatus.Release_to_SMS || data.schdStatus == SMSStatus.Ready_to_Build_Unplanned_Heat ||
            data.schdStatus == SMSStatus.Ready_to_Release_Unplanned_Heat || data.schdStatus == SMSStatus.Release_Unplanned_Heat)
    {
      this.disableFormFields(true);
      this.disableSchActions = false;
    }
    else if(data.schdStatus == SMSStatus.Created_Schedule)
    {
      this.enableFormFields();
    }
    else{
      if(!selectedUnit){
        this.enableFormFields();
      }else{
        this.disableFormFields(false);
      }
      this.disableSchActions = false;
    }
  }
}
