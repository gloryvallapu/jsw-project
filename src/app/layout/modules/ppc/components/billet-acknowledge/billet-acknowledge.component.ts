import {
  Component,
  OnInit
} from '@angular/core';
import {
  ComponentType
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  ITableHeader
} from 'src/assets/interfaces/table-headers';
import {
  BilletacknowledgementForms
} from '../../form-objects/ppc-form-objects';
import {
  TableType,
  ColumnType
} from 'src/assets/enums/common-enum';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  JswCoreService,
  JswFormComponent
} from 'jsw-core';
import {
  PpcService
} from '../../services/ppc.service';
import {
  CommonApiService
} from 'src/app/shared/services/common-api.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-billet-acknowledge',
  templateUrl: './billet-acknowledge.component.html',
  styleUrls: ['./billet-acknowledge.component.css']
})
export class BilletAcknowledgeComponent implements OnInit {
  public filterCriteria: any = [];
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public billetList: any = [];
  public selectedBatches: any[];
  public screenStructure: any = [];
  public isFilterCrtSec: boolean = false;
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isListSection: boolean = false;
  public isAckBtn: boolean = false;
  public formRef: NgForm;
  constructor(public billetacknowledgementForms: BilletacknowledgementForms,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public ppcService: PpcService,
    public commonService: CommonApiService,
    public authService: AuthService) {}


  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.Batch_Ack.id)[0];
    this.isFilterCrtSec = this.sharedService.isSectionVisible(menuConstants.batchAckSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.batchAckSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.batchAckSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.batchAckSectionIds.Filter_Criteria.buttons.reset, menuConstants.batchAckSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.batchAckSectionIds.List.id, this.screenStructure);
    this.isAckBtn = this.sharedService.isButtonVisible(true, menuConstants.batchAckSectionIds.List.buttons.acknowledge, menuConstants.batchAckSectionIds.List.id, this.screenStructure);
    this.filterCriteria = JSON.parse(JSON.stringify(this.billetacknowledgementForms.filterCriteria));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.columns = this.createColumns();
    this.getWorkCenterList();
    //this.getAllBilletBatchesInTransit();
    this.selectedBatches = [];
  };

  public acknowledgement(): any {
    if (this.selectedBatches.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please Select Schedule Batch',
        }
      );
    }
    let ackObjet = this.selectedBatches.map((batch: any) => {
      return batch.batchId
    })

    this.ppcService.batchAcknowlegdement(this.sharedService.loggedInUserDetails.userId, ackObjet).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `${Response}`,
        }
      );
     // this.getAllBilletBatchesInTransit();
     this.retrive();
      this.selectedBatches = [];
    })
  };

  public getWorkCenterList() {
    this.ppcService.getAuthorizedWCByUserId(this.sharedService.loggedInUserDetails.userId).subscribe(Response => {
      this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'workCenter')[0].list = Response.map((Wc: any) => {
        return {
          displayName: Wc.yardName,
          modelValue: Wc.yardCode,
          heatList: Wc.headIdList
        }
      })
    })
  };

  public getAllBilletBatchesInTransit() {
    this.ppcService.getAllBilletBatchesInTransit(this.sharedService.loggedInUserDetails.userId).subscribe(Response => {
      this.billetList = Response
    })
  };


  public editRecord(row) {
    this.selectedBatches = row
  };

  public createColumns() {
    return [{
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '30px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch Id',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'batchStatus',
        header: 'Status',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'salesOrderId',
        header: 'SO ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      // {
      //   field: 'orderId', // check if this is PO ID
      //   header: 'Order Id',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      {
        field: 'heatId',
        header: 'Heat Id',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'jswGrade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'nextWc',
        header: 'Next Unit',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      // {
      //   field: 'thickness',
      //   header: 'Thickness',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'width',
      //   header: 'Width',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      {
        field: 'length',
        header: 'Len(mm)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Wt(tons)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'productionRemark',
        header: 'Prod Remark',
        columnType: ColumnType.string,
        width: '170px',
        sortFieldName: '',
      }
    ];
  }

  public dropdownChangeEvent(event) {
    if(event.value != null && event.value != '' && event.ref_key == 'workCenter'){
      let wcList = this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'workCenter')[0].list
      let heatList = wcList.filter((x:any)=> x.modelValue == event.value)[0].heatList.map((Wc:any)=>{
        return {
          displayName: Wc,
          modelValue: Wc,
        }
      });
      this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'heatNo')[0].list = heatList

    }
    //
  }

  public retrive() {
    this.jswComponent.onSubmit(
      this.filterCriteria.formName,
      this.filterCriteria.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData != undefined) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, {
        "heatNo": null,
        "workCenter": null,
      });
      this.ppcService.getBatchesByUnitAndHeatId(this.sharedService.loggedInUserDetails.userId,formObj).subscribe(Response => {
        this.billetList = Response
      })
    }
  }

  public resetFilter() {
    //this.getAllBilletBatchesInTransit();
    this.billetList = [];
    //this.jswComponent.resetForm(this.filterCriteria);

  }
  public getFormRef(event){
    this.formRef = event;
  }


}
