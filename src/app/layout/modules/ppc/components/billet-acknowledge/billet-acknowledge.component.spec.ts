import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { BilletacknowledgementForms } from '../../form-objects/ppc-form-objects';
import { TableType, ColumnType } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { PpcService } from '../../services/ppc.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';

import { BilletAcknowledgeComponent } from './billet-acknowledge.component';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { PpcEndPoints } from '../../form-objects/ppc-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('BilletAcknowledgeComponent', () => {
  let component: BilletAcknowledgeComponent;
  let fixture: ComponentFixture<BilletAcknowledgeComponent>;
  let sharedService;
  let jswServices: JswCoreService;
  let ppcService: PpcService
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BilletAcknowledgeComponent ],

      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        PpcService,
       CommonApiService,
        JswFormComponent,
        PpcEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        BilletacknowledgementForms,
        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BilletAcknowledgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
