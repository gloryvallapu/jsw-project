import { Component, OnInit, ViewChild } from '@angular/core';
import { TableType, ColumnType } from 'src/assets/enums/common-enum';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ReApplicationScreen } from '../../form-objects/ppc-form-objects';
import { PpcService } from '../../services/ppc.service';
import { Table } from 'primeng/table/table';
import { Reapply, ReapplyForm } from '../../interfaces/reapply-interface';
import { ConfirmationService } from 'primeng/api';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import {ReapplicationForms} from '../../form-objects/ppc-form-objects';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-re-application',
  templateUrl: './re-application.component.html',
  styleUrls: ['./re-application.component.css'],
  providers: [ConfirmationService],
})
export class ReApplicationComponent implements OnInit {
  public formObject: any;
  public viewType = ComponentType;
  public columns: any = [];
  public globalFilterArray: any;
  public tableType: any;
  public orderDetails: any;
  public reapply: boolean = true;
  public display: boolean = false;
  formObject1: any;
  @ViewChild('dt') table: Table;
  public reapplicationList: any = [];
  public columnType = ColumnType;
  productList: any;
  heatList: any;
  gradeList: any;
  salesorderList: any;
  customernameList: any;
  productsizeList: any;
  lengthList: any;
  weightList: any;
  batchList: any;
  salesorderTypeList: any = [];
  public selectedQuantity: any;
  public selectedOrderlist: any = [];
  reapplyDetails: any = [];
  selectedWeight: any;
  reapplyForm: ReapplyForm;
  ncoClicked: boolean = false;
  lengthavalidation: boolean;
  public screenStructure: any = [];
  public isListSection: boolean = false;
  public isReapplyListBtn: boolean = false;
  public isReapplyModalSection: boolean = false;
  public isReapplyModalBtn: boolean = false;
  public isCloseBtn: boolean = false;
  public filterCriteria:any = [];
  public formRef: NgForm;
  public scheduleDetails:any = [];
  public soIDDetails: any = [];
  public scheduleTypes: any = [];

  constructor(
    public sharedService: SharedService,
    public reApplicationScreen: ReApplicationScreen,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public ppcService: PpcService,
    private confirmationService: ConfirmationService,
    public authService: AuthService,
    public reapplicationForms : ReapplicationForms,
  ) {
    this.reapplyForm = new ReapplyForm();
  }


  public getFilterInputDetails(){
    this.ppcService.getFilterInputDetails().subscribe(Response => {
      this.soIDDetails = Response;
      this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'soId')[0].list = Response.map((x:any)=> {return{
        displayName:x.soId,
        modelValue:x.soId,
        schdDetails:x.schdDetails
      }})

    })
  }

  public getFormRef(event){
    this.formRef = event;
  }


  public setScheduleIds(event){
    this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'schdId')[0].value = null;
    // this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'schdType')[0].value = null;
    this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'heatId')[0].value = null;
    this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'batchId')[0].value = null
    this.scheduleDetails =this.soIDDetails.filter((x:any)=> x.soId == event.value)[0].schdDetails.map((x:any)=>{
      return {
        displayName:`${x.schdId}-${x.schdType}`,
        modelValue:`${x.schdId}-${x.schdType}`,
        schdType:x.schdType,
        heatDetails:x.heatDetails
      }
    });
    this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'schdId')[0].list = this.scheduleDetails;
  };


public setscheduleTypes(event){
  this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'schdType')[0].value = null;
  this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'heatId')[0].value = null;
  this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'batchId')[0].value = null
  this.scheduleTypes = this.scheduleDetails.map((x:any)=>{
    return {
      displayName:x.schdType,
      modelValue:x.schdType,
      heatDetails:x.heatDetails
    }
  });
  var dataArr = this.scheduleTypes.map(item=>{
    return [item.displayName,item]
}); // creates array of array
 var maparr = new Map(dataArr); // create key value pair from array of array
 var result = [...maparr.values()];//converting back to array from mapobject
  this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'schdType')[0].list = result
  }

public setheatIds(event){
  this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'heatId')[0].value = null;
  this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'batchId')[0].value = null;
  // let schType =  event.value.split('-');
  let heatIds = this.scheduleDetails.filter((x:any)=>x.modelValue == event.value)[0].heatDetails.map((x:any)=>{
    return {
      displayName:x.heatId,
      modelValue:x.heatId,
      batchIds:x.batchIds
    }
  });
  this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'heatId')[0].list = heatIds;
}

public setBatchIds(event){
  this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'batchId')[0].value = null
  let heatWiseBatches = this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'heatId')[0].list
  let batchIds =  heatWiseBatches.filter((x:any)=> x.modelValue == event.value)[0].batchIds.map((x:any)=>{
    return {
      displayName:x,
      modelValue:x,
    }
  });
  this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'batchId')[0].list = batchIds
}

  public dropdownChangeEventForFilter(event){
    if(event.value != null && event.value != ''){
      switch(event.ref_key){
        case 'soId':this.setScheduleIds(event);
        break;
        case 'schdId':this.setheatIds(event);
        break;
        case 'schdType':this.setheatIds(event);
        break;
        case 'heatId':this.setBatchIds(event);
        break;

        default : break;
      }

    }


  }


  public retrive(){
    this.jswComponent.onSubmit(
      this.filterCriteria.formName,
      this.filterCriteria.formFields
    );
    let  formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, {
        "batchId": null,"heatId": null,"lpSchdId": null,"schdId": null,"soId": null,'schdType':null});
      let schType = formObj.schdId != null?  formObj.schdId.split('-'):null
      formObj.lpSchdId = schType !==null && schType.length >0 && schType[1] == 'LP'? schType[0]:null;
      formObj.lpSchdId = schType !==null && schType.length >0 && schType[1] == 'LP'? schType[0]:null;
      formObj.schdId = schType !==null && schType.length >0 && schType[1] == 'LP'? null:( schType !==null && schType.length >0 ? schType[0] : null)

      this.ppcService.filteredDetailsOrder(formObj).subscribe(data =>{
        data.map((x: any) => {
          x.disabled = false;
          return x;
        });
        this.reapplicationList = data;
        this.productList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.product, modelValue: x.product };
        }));
        this.batchList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.batchID, modelValue: x.batchID };
        }));
        this.heatList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.heatNo, modelValue: x.heatNo };
        }));
        this.gradeList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.grade, modelValue: x.grade };
        }));
        this.salesorderList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.salesorder, modelValue: x.salesorder };
        }));
        this.customernameList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.customerName, modelValue: x.customerName };
        }));
        this.productsizeList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.productSize, modelValue: x.productSize };
        }));
        this.lengthList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.length, modelValue: x.length };
        }));
        this.weightList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.weight, modelValue: x.weight };
        }));
      })
    }

  }

  public resetFilter(){
    this.reapplicationList = [];

  }

  ngOnInit(): void {
    this.getFilterInputDetails();
    this.filterCriteria = this.reapplicationForms.ReapplicationFilterCriteriaForms;
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.Re_Application.id)[0];
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.reApplicationSectionIds.List.id, this.screenStructure);
    this.isReapplyListBtn = this.sharedService.isButtonVisible(true, menuConstants.reApplicationSectionIds.List.buttons.reapply, menuConstants.reApplicationSectionIds.List.id, this.screenStructure);
    this.isReapplyModalSection = this.sharedService.isSectionVisible(menuConstants.reApplicationSectionIds.Reapply_Modal.id, this.screenStructure);
    this.isReapplyModalBtn = this.sharedService.isButtonVisible(true, menuConstants.reApplicationSectionIds.Reapply_Modal.buttons.reapply, menuConstants.reApplicationSectionIds.Reapply_Modal.id, this.screenStructure);
    this.isCloseBtn = this.sharedService.isButtonVisible(true, menuConstants.reApplicationSectionIds.Reapply_Modal.buttons.close, menuConstants.reApplicationSectionIds.Reapply_Modal.id, this.screenStructure);
    // this.getAllDetailsOrder();
    this.formObject = JSON.parse(JSON.stringify(this.reApplicationScreen.filterCriteria));
    this.formObject1 = JSON.parse(JSON.stringify(this.reApplicationScreen.reApply));
    this.columns = this.detailsOfOrderTableColums();
    this.globalFilterArray = this.columns.map((element) => {
      return element.field;
    });
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
  }

  public getAllDetailsOrder() {
    this.ppcService.getAllDetailsOrder().subscribe((data) => {
      data.map((x: any) => {
        x.disabled = false;
        return x;
      });
      this.reapplicationList = data;
      this.productList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.product, modelValue: x.product };
      }));
      this.batchList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.batchID, modelValue: x.batchID };
      }));
      this.heatList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.heatNo, modelValue: x.heatNo };
      }));
      this.gradeList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.grade, modelValue: x.grade };
      }));
      this.salesorderList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.salesorder, modelValue: x.salesorder };
      }));
      this.customernameList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.customerName, modelValue: x.customerName };
      }));
      this.productsizeList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.productSize, modelValue: x.productSize };
      }));
      this.lengthList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.length, modelValue: x.length };
      }));
      this.weightList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.weight, modelValue: x.weight };
      }));
    });
  }

  public dropdownChangeEvent(data) {
    this.getLineNOBySO(data.value, data.ref_key);
  }

  public salesOrderLineNos() {
    this.ppcService
      .salesOrderLineNos(
        this.selectedWeight,
        this.selectedOrderlist[0].materialId
      )
      .subscribe((data) => {
        this.salesorderTypeList = data;
        this.formObject1.formFields.filter(
          (ele) => ele.ref_key == 'salesOrder'
        )[0].list = data.map((x: any) => {
          return { displayName: x.salesOrder, modelValue: x.salesOrder };
        });
      });
  }

  public getLineNOBySO(id, ref_key) {
    let products;
    if (ref_key == 'lineNo') {
      products = this.formObject1.formFields.filter(
        (ele) => ele.ref_key == 'salesOrder'
      )[0].value;
      this.reapplyRequiredDetails(products, id,this.selectedOrderlist[0].materialId);
    } else {
      products = this.salesorderTypeList.filter(
        (ele) => ele.salesOrder == id
      )[0].lineNoList;
      let prodObj = this.formObject1.formFields.filter(
        (ele) => ele.ref_key == 'lineNo'
      )[0];
      prodObj.list = products.map((x: any) => {
        return {
          displayName: x,
          modelValue: x,
        };
      });
      prodObj.value = prodObj.list[0].modelValue;
      this.reapplyRequiredDetails(id, products[0],this.selectedOrderlist[0].materialId);
    }
  }

  public reapplyRequiredDetails(soId, lineNo,materialId) {
    this.ppcService.reapplyRequiredDetails(soId, lineNo,materialId).subscribe((data) => {
      this.reapplyDetails = data;
      this.formObject1.formFields.filter(
        (ele) => ele.ref_key == 'promiseDate'
      )[0].value = data.promiseDate;
      this.formObject1.formFields.filter(
        (ele) => ele.ref_key == 'soGrade'
      )[0].value = data.soGrade;
      this.formObject1.formFields.filter(
        (ele) => ele.ref_key == 'dimension'
      )[0].value = data.size;
      this.formObject1.formFields.filter(
        (ele) => ele.ref_key == 'customerName'
      )[0].value = data.customerName;
      this.formObject1.formFields.filter(
        (ele) => ele.ref_key == 'btc'
      )[0].value = data.btc;
      this.formObject1.formFields.filter(
        (ele) => ele.ref_key == 'length'
      )[0].value = data.length;
    });
  }

  public getReapplyDetails(comefrom) {
    this.jswComponent.onSubmit(
      this.formObject1.formName,
      this.formObject1.formFields
    );
    var lengthval = false;
    if (comefrom == 'acceptLength') {
      lengthval = true;
    }
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(
        formData,
        this.reapplyForm.getReapplyForm()
      );
      let orderList: any = [];
      this.selectedOrderlist.forEach((element) => {
        let reqBody: Reapply = {
          batchID: element.batchID,
          customerName: element.customerName,
          diameter: element.diameter,
          grade: element.grade,
          heatNo: element.heatNo,
          length: element.length,
          lengthValidation: lengthval,
          materialclass: element.materialClassCode,
          product: element.product,
          productSize: element.productSize,
          psn: element.psn,
          salesorder: element.salesorder,
          thickness: element.thickness,
          weight: element.weight,
          width: element.width,
          materialId: element.materialId
          //materialClassCode: element.materialClassCode
        };
        orderList.push(reqBody);
      });
      if (this.ncoClicked) {
        this.reapplyNco(
          this.sharedService.loggedInUserDetails.userId,
          orderList

        );
        this.closeModal();
      } else {
        this.reapplydetails(
          this.formObject1.formFields.filter(
            (ele) => ele.ref_key == 'salesOrder'
          )[0].value,
          this.sharedService.loggedInUserDetails.userId,
          this.formObject1.formFields.filter(
            (ele) => ele.ref_key == 'lineNo'
          )[0].value,
          orderList
        );

        this.closeModal();
      }
    }


  }

  public reapplyNco(userId, reqBody) {
    this.ppcService.reapplyNco(userId, reqBody).subscribe((data) => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        { message: 'Reapplication with NCO updated successfully!' }
      );
      this.retrive();
    });
  }

  public reapplydetails(soId, userId, lineNo, reqBody) {
    this.ppcService.reapply(soId, lineNo, userId, reqBody).subscribe((data) => {
      var indexSelected;
      var isSuccess = '';
      for (var j = 0; j < data.length; j++) {
        if (data[j] != null && data[j].message == null) {
          isSuccess += 'true';
          //as ankur suggested in sit
         // this.getAllDetailsOrder();
          this.retrive();
        } else {
          isSuccess += 'false';
        }
        for (var i = 0; i < this.reapplicationList.length; i++) {
          if (
            this.reapplicationList[i] != null &&
            this.reapplicationList[i].batchID === data[j].batchID
          ) {
            indexSelected = i;
            if (
              data[j].message == 'Diamension/size of batch are not matching'
            ) {
              // let index = this.reapplicationList.indexOf(
              //   (ele) => ele.batchID == data[j].batchID
              // );
              this.reapplicationList[indexSelected].isProductSizeError = true;

              //as per new requirement show warning msg as same as api resp
              // this.sharedService.displayToastrMessage(
              //   this.sharedService.toastType.Error,
              //   { message: 'Reapplication FAILED!' }
              // );
              this.sharedService.displayToastrMessage(
                this.sharedService.toastType.Warning,
                { message: 'Diamension/size of batch are not matching!' }
              );

            } else if (data[j].message == 'Grade of batch is not matching') {
              // let index = this.reapplicationList.indexOf(
              //   (ele) => ele.batchID == data[j].batchID
              // );
              this.reapplicationList[indexSelected].isGradeSizeError = true;
              // this.sharedService.displayToastrMessage(
              //   this.sharedService.toastType.Error,
              //   { message: 'Reapplication FAILED!' }
              // );
              this.sharedService.displayToastrMessage(
                this.sharedService.toastType.Warning,
                { message: 'Grade of batch is not matching!' }
              );
            }

            //vaibhav
            else if (data[j].message == 'Heat chemistry is not matching') {
              this.reapplicationList[indexSelected].isGradeSizeError = true;
              this.sharedService.displayToastrMessage(
                this.sharedService.toastType.Warning,
                { message: 'Heat chemistry is not matching !' }
              );
            }









            else if (data[j].message == 'Class is not Prime') {
              // let index = this.reapplicationList.indexOf(
              //   (ele) => ele.batchID == data[j].batchID
              // );
              this.reapplicationList[indexSelected].isClassError = true;
              // this.sharedService.displayToastrMessage(
              //   this.sharedService.toastType.Error,
              //   { message: 'Reapplication FAILED!' }
              // );
              this.sharedService.displayToastrMessage(
                this.sharedService.toastType.Warning,
                { message: 'Class is not Prime!' }
              );
            }
             else if (
              data[j].message != null &&
              data[j].message.includes('Weight of batch is not matching')
            ) {
              // let index = this.reapplicationList.indexOf(
              //   (ele) => ele.batchID == data[j].batchID
              // );
              this.reapplicationList[indexSelected].isWeightError = true;
              // this.sharedService.displayToastrMessage(
              //   this.sharedService.toastType.Error,
              //   { message: 'Reapplication FAILED!' }
              // );
              this.sharedService.displayToastrMessage(
                this.sharedService.toastType.Warning,
                { message: 'Weight of batch is not matching!' }
              );
              this.lengthavalidation = data[j].lengthValidation;
            } else {
              if (
                this.ncoClicked == false &&
                data[0].message == 'length of batch is not matching'
              ) {
                this.confirmationService.confirm({
                  message:
                    '<span class="font-16 text-grey">Length is not matching , Do you want to continue ?</span>',
                  header: 'Length Confirmation',
                  icon: 'pi pi-info-circle',
                  accept: () => {
                    this.sharedService.displayToastrMessage(
                      this.sharedService.toastType.Success,
                      { message: 'Reapplication updated successfully!' }
                    );
                    this.getReapplyDetails('acceptLength');
                  },
                  reject: () => {
                    if (data[j].message == 'length of batch is not matching') {
                      // let index = this.reapplicationList.indexOf(
                      //   (ele) => ele.batchID == data[j].batchID
                      // );
                      this.reapplicationList[
                        indexSelected
                      ].isLengthError = true;
                      // this.sharedService.displayToastrMessage(
                      //   this.sharedService.toastType.Error,
                      //   { message: 'Reapplication FAILED!' }
                      // );
                      this.sharedService.displayToastrMessage(
                        this.sharedService.toastType.Warning,
                        { message: 'length of batch is not matching!' }
                      );
                    }
                  },
                });
              }
            }
          }
        }
      }

      if (isSuccess.indexOf('false') >= 0) {
      } else {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: 'Reapplication Succesful!' }
        );
      }
    });
  }

  public doSomething(event){
  this.closeModal();
  }

  public showDialog() {
    if(!this.isReapplyModalSection){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access Reapplication Modal popup. Please contact Admin."});
      return;
    }
    if (this.ncoClicked) {
      this.ncoClicked = false;
      this.formObject1.formFields.filter(
        (ele: any) => ele.ref_key == 'salesOrder'
      )[0].disabled = false;
      this.formObject1.formFields.filter(
        (ele: any) => ele.ref_key == 'lineNo'
      )[0].disabled = false;
    }
    this.salesOrderLineNos();
    this.display = true;
  }

  public TextBox(event: any) {
    this.table.filterGlobal(event.target.value, 'contains');
  }

  public NCOclicked(e) {
    if (e.target.checked) {
      this.ncoClicked = true;
      this.formObject1.formFields.filter(
        (ele: any) => ele.ref_key == 'salesOrder'
      )[0].disabled = true;
      this.formObject1.formFields.filter(
        (ele: any) => ele.ref_key == 'lineNo'
      )[0].disabled = true;
    } else {
      this.ncoClicked = false;
      this.formObject1.formFields.filter(
        (ele: any) => ele.ref_key == 'salesOrder'
      )[0].disabled = false;
      this.formObject1.formFields.filter(
        (ele: any) => ele.ref_key == 'lineNo'
      )[0].disabled = false;
    }
  }

  public closeModal() {
    this.display = false;
    this.selectedOrderlist = [];
    this.selectedQuantity = [];
    this.jswComponent.resetForm(this.formObject1);
  }

  public detailsOfOrderTableColums() {
    return [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'salesorder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'batchID',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'product',
        header: 'Product',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'productSize',
        header: 'Product Size',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'heatNo',
        header: 'Heat No',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'materialclass',
        header: 'Material Class',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'psn',
        header: 'PSN',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Length(mm)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'processStatus',
        header: 'Process Status',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Weight(tons)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
    ];
  }

  public getTotalQuantity(): any {
    if (this.selectedOrderlist.length == 0) {
      this.selectedQuantity = 0;
      this.reapplicationList.map((list: any) => {
        list.disabled = false;
        return list;
      });
      return true;
    }

    this.reapplicationList.map((list: any) => {
      if (list.product != this.selectedOrderlist[0].product) {
        list.disabled = true;
      } else {
        list.disabled = false;
      }
      return list;
    });
    let initialValue = 0;
    if (this.selectedOrderlist && this.selectedOrderlist.length > 0) {
      this.selectedQuantity = this.selectedOrderlist
        .reduce((total, item) => {
          this.selectedWeight = Number(total) + Number(item.weight);
          return this.selectedWeight;
        }, initialValue)
        .toFixed(2);
    } else {
      this.selectedQuantity = 0;
    }
  }
}
