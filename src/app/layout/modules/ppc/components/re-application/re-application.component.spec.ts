import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ReApplicationScreen } from '../../form-objects/ppc-form-objects';
import { PpcService } from '../../services/ppc.service';
import { Table } from 'primeng/table/table';
import { Reapply, ReapplyForm } from '../../interfaces/reapply-interface';
import { ConfirmationService, SharedModule } from 'primeng/api';
import { ReApplicationComponent } from './re-application.component';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { PpcEndPoints } from '../../form-objects/ppc-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';


describe('ReApplicationComponent', () => {
  let component: ReApplicationComponent;
  let fixture: ComponentFixture<ReApplicationComponent>;
  let sharedService;
  let jswServices: JswCoreService;
  let ppcService: PpcService
 const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReApplicationComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        PpcService,
       CommonApiService,
        JswFormComponent,
        PpcEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        ReApplicationScreen,
        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
