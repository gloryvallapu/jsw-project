import { Component, OnInit, ViewChild } from '@angular/core';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { PpcService } from '../../services/ppc.service';
import {  SoModificationFomrs} from '../../form-objects/ppc-form-objects';
import { TableType, ColumnType } from 'src/assets/enums/common-enum';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { NgForm } from '@angular/forms';
import { Table } from 'primeng/table';
@Component({
  selector: 'app-so-modification',
  templateUrl: './so-modification.component.html',
  styleUrls: ['./so-modification.component.css']
})
export class SoModificationComponent implements OnInit {
  @ViewChild('dt') table: Table;
  public filterCriteria:any = [];
  public viewType = ComponentType;
  public sOcolumns: ITableHeader[] = [];
  public sOHistorycolumns: ITableHeader[] = [];
  public tableType: any;
  public soDetailsList:any = [];
  public soEditHistoryDetailsList:any = [];
  public soModificationDetails:any;
  public sOlistWiseCustomer: any;
  public updateBatch: any = [];
  public screenStructure: any = [];
  public isFilterCrtSec: boolean = false;
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isListSection: boolean = false;
  public isSaveBtn: boolean = false;
  public isHistorySection: boolean = false;
  public lengthValidationFlag: boolean = false;
  public formRef: NgForm;
  public selectedRow: any;
  public first:number= 0;
  constructor( public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public ppcService: PpcService,
    public commonService: CommonApiService,
    public soModificationFomrs : SoModificationFomrs,
    public authService: AuthService
) { }

  ngOnInit(): void {
    this.getLenValidationFlag();
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.SO_Modification.id)[0];
    this.isFilterCrtSec = this.sharedService.isSectionVisible(menuConstants.soModificationSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.soModificationSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.soModificationSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.soModificationSectionIds.Filter_Criteria.buttons.reset, menuConstants.soModificationSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.soModificationSectionIds.List.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.soModificationSectionIds.List.buttons.save, menuConstants.soModificationSectionIds.List.id, this.screenStructure);
    this.isHistorySection = this.sharedService.isSectionVisible(menuConstants.soModificationSectionIds.Edit_History.id, this.screenStructure);
    this.filterCriteria = JSON.parse(JSON.stringify(this.soModificationFomrs.filterCriteria));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.sOcolumns = this.createSOcolumns();
    this.sOHistorycolumns = this.createSOHistorycolumns();
    this.getSoModFilters();
  }

  public getLenValidationFlag(){
    this.ppcService.getLengthValidationFlag().subscribe(data => {
      this.lengthValidationFlag = data == 'Y' ? true : false;
    })
  }

  public getSoModFilters(){
    this.ppcService.getSoModFilters().subscribe(Response =>{
      this.sOlistWiseCustomer = Response.soIdCustomerList
      this.soModificationDetails = Response.soIdLineItemList
      let SoList = Object.keys(Response.soIdLineItemList);
      this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'SaleOrderNo')[0].list = SoList.map((li:any)=> {return{
        displayName:li,
        modelValue:li
      }})
      this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'customerName')[0].list = Response.customerList.map((li:any)=>{
        return {
          displayName:li,
          modelValue:li
        }
      })
    })
  }

  public retrive():any{
    this.soDetailsList = [];
    this.jswComponent.onSubmit(
      this.filterCriteria.formName,
      this.filterCriteria.formFields
    );
    var formData = this.jswService.getFormData();
    if(formData != undefined){
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, {
      "SaleOrderNo": null,
      "lineItme": null,
      "customerName":null,
    });
    if(formObj.customerName == null && formObj.SaleOrderNo == null){
      return  this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please select either Sales Order No or Customer Name to get SO Details'
        }
      );
    }
    if(formObj.customerName != null && formObj.SaleOrderNo == null ){
      this.ppcService.getCustomerBasedOrderBookDetails(formObj.customerName).subscribe(Response =>{
        Response.map((data:any)=>{
          data.reqDeliveryDate = new Date(data.reqDeliveryDate),//this.sharedService.get_MM_DD_YYYY_Date( data.reqDeliveryDate);
          data.soReleaseDate = new Date(data.soReleaseDate)
          return data
        })
        this.soDetailsList = Response

      })
    }
    if(formObj.SaleOrderNo != null )
    { this.ppcService.getSoBasedOrderBookDetails(formObj.SaleOrderNo,formObj.lineItme).subscribe(Response => {
      Response.map((data:any)=>{
        data.reqDeliveryDate = new Date( data.reqDeliveryDate);
        data.soReleaseDate = new Date( data.soReleaseDate)
        return data
      })
       this.soDetailsList = Response
     })}
    }
  }


  public resetFilter(){
    // this.table._first=0
    this.soDetailsList = [];
    this.soEditHistoryDetailsList = [];
    this.updateBatch =[];
    this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'customerName')[0].disabled = false
    //this.jswComponent.resetForm(this.filterCriteria);
  }

  public dropdownChangeEvent(event){
    if(event.value != null){
      this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'customerName')[0].disabled = true
      this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'customerName')[0].value = this.sOlistWiseCustomer[event.value]
      this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'lineItme')[0].list = this.soModificationDetails[event.value].map((li:any)=>{
        return {
          displayName:li,
          modelValue:li
        }
      })
    }else{
      this.filterCriteria.formFields.filter((ele) => ele.ref_key == 'customerName')[0].disabled = false
    }

  }

  public editRecord(event){
    this.updateBatch[0] =event;
    this.selectedRow = event;
    let eventData = event
    this.hisatoryDetails(eventData);
  }

  public hisatoryDetails(data){
    this.soEditHistoryDetailsList = [];
    this.ppcService.getEditHostoryForSoDetails(data).subscribe(Response =>{
      this.updateBatch.map((data:any)=>{
        data.reqDeliveryDate = new Date( data.reqDeliveryDate);
        data.soReleaseDate = new Date( data.soReleaseDate)
       return data
       })
      this.soEditHistoryDetailsList = Response.map((history:any)=>{
        history.modifiedDate = new Date(history.modifiedDate);
        return history
      })
    })
  }

  public saveChanges():any{
    if (this.updateBatch.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please Select batch Before Save',
        }
      );
    }

   if(this.lengthValidationFlag && (this.updateBatch[0].lengthMin > this.updateBatch[0].length)){
    return this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Warning, {
        message: `Min Length should be less than ${this.updateBatch[0].length}`,
      }
    );
   };

   if(this.updateBatch[0].lengthMin == null || this.updateBatch[0].lengthMin == '' || this.updateBatch[0].lengthMax == null || this.updateBatch[0].lengthMax == ''){
    return this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Warning, {
        message: 'Min Length Or Max Length can not be empty',
      }
    );
   };

   if(parseFloat(this.updateBatch[0].lengthMin) < 6000){ // confirm this with Ankur
    return this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Warning, {
        message: 'Min Length can not be less than 6000 mm',
      }
    );
   };

   if( parseFloat(this.updateBatch[0].lengthMax) > 12150){ // confirm this with Ankur
    return this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Warning, {
        message: 'Max Length can not be above 12150 mm',
      }
    );
   };

   if(this.lengthValidationFlag && (this.updateBatch[0].lengthMax < this.updateBatch[0].length)){ // confirm this with Ankur
    return this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Warning, {
        message: `Max Length should be greater than ${this.updateBatch[0].length}`,
      }
    );
   }

   this.updateBatch.map((data:any)=>{
    data.reqDeliveryDate = new Date(data.reqDeliveryDate)
    data.soReleaseDate = new Date(data.soReleaseDate)
   return data
   })

   this.ppcService.updateSOLengthDetails(this.sharedService.loggedInUserDetails.userId,this.updateBatch).subscribe(Response =>{
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `Min and Max length updated for Product type ${this.updateBatch[0].productType}`,
        }
      );
      this.hisatoryDetails(this.selectedRow)
      this.updateBatch.map((data:any)=>{
        data.reqDeliveryDate = new Date( data.reqDeliveryDate);
        data.soReleaseDate = new Date( data.soReleaseDate)
       return data
       })
      // this.updateBatch = [];
   })
  }

  public createSOHistorycolumns(){
    return [
    {
      field: 'userName',
      header: 'User Name',
      columnType: ColumnType.string,
      width: '80px',
      sortFieldName: '',
    },
    {
      field: 'role',
      header: 'Role',
      columnType: ColumnType.string,
      width: '100px',
      sortFieldName: '',
    },
    {
      field: 'productType',
      header: 'Prod Type',
      columnType: ColumnType.string,
      width: '100px',
      sortFieldName: '',
    },
    {
      field: 'lengthMin',
      header: 'Len(Min)',
      columnType: ColumnType.string,
      width: '100px',
      sortFieldName: '',
    },
    {
      field: 'lengthMax',
      header: 'Len(Max)',
      columnType: ColumnType.string,
      width: '100px',
      sortFieldName: '',
    },
    {
      field: 'modifiedDate',
      header: 'Date & Time',
      columnType: ColumnType.date,
      width: '100px',
      sortFieldName: '',
    }
  ];
  }


  public createSOcolumns(){
    return [
      {
        field: 'radio',
        header: '',
        columnType: ColumnType.radio,
        width: '40px',
        sortFieldName: '',
      },
    {
      field: 'productType',
      header: 'Prod Type',
      columnType: ColumnType.string,
      width: '100px',
      sortFieldName: '',
    },
    {
      field: 'soReleaseDate',
      header: 'SO Rel Date',
      columnType: ColumnType.date,
      width: '130px',
      sortFieldName: '',
    },
    {
      field: 'reqDeliveryDate',
      header: 'Req Delivery Date',
      columnType: ColumnType.date,
      width: '130px',
      sortFieldName: '',
    },
    {
      field: 'grade',
      header: 'Grade',
      columnType: ColumnType.string,
      width: '110px',
      sortFieldName: '',
    },
    {
      field: 'size',
      header: 'Size',
      columnType: ColumnType.string,
      width: '80px',
      sortFieldName: '',
    },
    {
      field: 'orderQty',
      header: 'Order Qty',
      columnType: ColumnType.string,
      width: '100px',
      sortFieldName: '',
    },
    {
      field: 'length',
      header: 'Len(mm)',
      columnType: ColumnType.string,
      width: '80px',
      sortFieldName: '',

    },
    {
      field: 'lengthMin',
      header: 'Len(Min)',
      columnType: ColumnType.text,
      width: '80px',
      sortFieldName: '',
    },
    {
      field: 'lengthMax',
      header: 'Len(Max)',
      columnType: ColumnType.text,
      width: '80px',
      sortFieldName: '',
    },
    {
      field: 'psn',
      header: 'PSN',
      columnType: ColumnType.string,
      width: '150px',
      sortFieldName: '',
    },
    {
      field: 'customer',
      header: 'Customer',
      columnType: ColumnType.tooltip,
      width: '180px',
      sortFieldName: '',
    },
    {
      field: 'processRoute',
      header: 'Process Route',
      columnType: ColumnType.string,
      width: '250px',
      sortFieldName: '',
    },
    // {
    //   field: 'roleId',
    //   header: 'User Name',
    //   columnType: ColumnType.string,
    //   width: '120px',
    //   sortFieldName: '',
    // }
    ];
  }

  public getFormRef(event){
    this.formRef = event;
  }

}
