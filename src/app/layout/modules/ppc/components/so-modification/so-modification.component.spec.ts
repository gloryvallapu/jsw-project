import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { PpcService } from '../../services/ppc.service';
import {  BilletacknowledgementForms, SoModificationFomrs} from '../../form-objects/ppc-form-objects';
import { TableType, ColumnType } from 'src/assets/enums/common-enum';

import { SoModificationComponent } from './so-modification.component';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { PpcEndPoints } from '../../form-objects/ppc-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('SoModificationComponent', () => {
  let component: SoModificationComponent;
  let fixture: ComponentFixture<SoModificationComponent>;
  let sharedService;
  let jswServices: JswCoreService;
  let ppcService: PpcService
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoModificationComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        PpcService,
       CommonApiService,
        JswFormComponent,
        PpcEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        BilletacknowledgementForms,
        SoModificationFomrs,
        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoModificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
