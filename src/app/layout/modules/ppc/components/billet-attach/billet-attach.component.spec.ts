import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { BilletacknowledgementForms, BilletAttachForms, SoModificationFomrs } from '../../form-objects/ppc-form-objects';
import { JswFormComponent } from 'jsw-core';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService } from 'jsw-core';
import { PpcService } from '../../services/ppc.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { BatchAttachComponent } from './sub-components/batch-attach/batch-attach.component';
import { BatchDetachComponent } from './sub-components/batch-detach/batch-detach.component';
import { BilletAttachComponent } from './billet-attach.component';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PpcEndPoints } from '../../form-objects/ppc-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('BilletAttachComponent', () => {
  let component: BilletAttachComponent;
  let fixture: ComponentFixture<BilletAttachComponent>;
  let sharedService;
  let jswServices: JswCoreService;
  let ppcService: PpcService;
  let ref: DynamicDialogRef;
   let config: DynamicDialogConfig
   const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BilletAttachComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        PpcService,
       CommonApiService,
        JswFormComponent,
        PpcEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        BilletacknowledgementForms,
        SoModificationFomrs,
        BilletAttachForms,
        DynamicDialogRef,
        DynamicDialogConfig,

        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BilletAttachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
