import { Component, OnInit } from '@angular/core';
import { BilletAttachForms } from '../../form-objects/ppc-form-objects';
import { JswFormComponent } from 'jsw-core';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { HttpClient } from '@angular/common/http';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService } from 'jsw-core';
import { PpcService } from '../../services/ppc.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { BatchAttachComponent } from './sub-components/batch-attach/batch-attach.component';
import { BatchDetachComponent } from './sub-components/batch-detach/batch-detach.component';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-billet-attach',
  templateUrl: './billet-attach.component.html',
  styleUrls: ['./billet-attach.component.css'],
  providers: [DialogService]
})
export class BilletAttachComponent implements OnInit {
  billetFilterCriteriaFormObject: any;
  tableType: any;
  columns: ITableHeader[] = [];
  viewType = ComponentType;
  billetAttachList: any = [];
  workCenterTypeList: any = [];
  unitList: any = [];
  productionOrderList: any = [];
  public batchAttachModalRef: DynamicDialogRef;
  public batchDetachModalRef: DynamicDialogRef;
  public selectedRecord: any;
  public screenStructure: any = [];
  public isFilterCrtSec: boolean = false;
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isListSection: boolean = false;
  public isAttachBtn: boolean = false;
  public isUnattachBtn: boolean = false;
  public isAttachModal: boolean = false;
  public isUnattachModal: boolean = false;
  public formRef: NgForm;
  constructor(
    public sharedService: SharedService,
    public billetAttachForms: BilletAttachForms,
    public http: HttpClient,
    public jswComponent: JswFormComponent,
    public jswService: JswCoreService,
    public ppcService: PpcService,
    public commonService: CommonApiService,
    public dialogService: DialogService, public authService: AuthService,
  ) {}

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.Batch_Attach.id)[0];
    this.isFilterCrtSec = this.sharedService.isSectionVisible(menuConstants.batchAttachSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.batchAttachSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.batchAttachSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.batchAttachSectionIds.Filter_Criteria.buttons.reset, menuConstants.batchAttachSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.batchAttachSectionIds.List.id, this.screenStructure);
    this.isAttachBtn = this.sharedService.isButtonVisible(true, menuConstants.batchAttachSectionIds.List.buttons.attach, menuConstants.batchAttachSectionIds.List.id, this.screenStructure);
    this.isUnattachBtn = this.sharedService.isButtonVisible(true, menuConstants.batchAttachSectionIds.List.buttons.unattach, menuConstants.batchAttachSectionIds.List.id, this.screenStructure);
    this.isAttachModal = this.sharedService.isSectionVisible(menuConstants.batchAttachSectionIds.Attach_Screen.id, this.screenStructure);
    this.isUnattachModal = this.sharedService.isSectionVisible(menuConstants.batchAttachSectionIds.Unattach_Screen.id, this.screenStructure);
    this.billetFilterCriteriaFormObject = JSON.parse(JSON.stringify(this.billetAttachForms.billetFilterCriteriaForms));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getWcTypes();
    this.columns = [
      {
        field: 'radio',
        header: '',
        columnType: ColumnType.radio,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'salesOrderNo',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      // {
      //   field: 'orderLineId',
      //   header: 'Order Line',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      {
        field: 'productionOrderNo',
        header: 'PO ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'schdID',
        header: 'Schedule ID',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field:  'productName', //'soMaterial',
        header: 'SO Material',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'inputProduct', //inputProductType --> confirm with Ankur
        header: 'Input Product Type',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      // {
      //   field: 'productGrade',
      //   header: 'Product Grade',
      //   columnType: ColumnType.string,
      //   width: '150px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'orderDimentions',
      //   header: 'Order Dimension',
      //   columnType: ColumnType.string,
      //   width: '150px',
      //   sortFieldName: '',
      // },
      {
        field: 'ipBatchGrade',
        header: 'Input Product Grade',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      // {
      //   field: 'ipBatchSection',
      //   header: 'Input Product Section',
      //   columnType: ColumnType.string,
      //   width: '150px',
      //   sortFieldName: '',
      // },
      {
        field: 'ipBatchSection',
        header: 'Batch Size',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'attached',  //totalBatch
        header: 'Attached',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'charged',
        header: 'Charged',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'discharged',
        header: 'Discharged',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'rolled',
        header: 'Rolled',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'cobbled',
        header: 'Cobbled',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'hotout',
        header: 'Hot Out',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'mixed',
        header: 'Mixed',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'bundle',
        header: 'Bundle',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'plannedQty',
        header: 'Total Tons Planned',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'plannedPcs',
        header: 'Total Pcs Planned',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'schdStatus',
        header: 'Schedule Line Status',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
    ];
  }

  public getWcTypes() {
    this.ppcService.getUnitList().subscribe((data) => {
      this.unitList = data;
      this.billetFilterCriteriaFormObject.formFields.filter(
        (obj: any) => obj.ref_key == 'wcName'
      )[0].list = data.map((x: any) => {
        return { displayName: x.wcName, modelValue: x.wcName };
      });
    });
  }

  public dropdownChangeEvent(data) {
    if (data.ref_key == 'wcName') { // unit dropdown change event
      this.getSchdByWC(data.value);
    }
    if (data.ref_key == 'schdID') {  // schedule Id dropdown change event
      this.getProductType(data.value);
    }
  }

  public getSchdByWC(id) {
    let schArr = this.unitList.filter((ele) => ele.wcName == id)[0].scheduleInfo; // get the scheduleInfo array for the selected unit
    let schObj = this.billetFilterCriteriaFormObject.formFields.filter((ele) => ele.ref_key == 'schdID')[0]; // get the schObj from formArray
    schObj.list = schArr.map((x: any) => { // assign schedule data to dropdown list
      return {
        displayName: x.schdID,
        modelValue: x.schdID,
      };
    });
    schObj.value = schObj.list[0].modelValue; // assign first value of array to the schId dropdown (will be displayed as 1st selected value)
    this.getProductType(schObj.value);  // get the product details for selected schId
  }

  public getProductType(schID) {
    let wc = this.billetFilterCriteriaFormObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].value; // need to find the unit value selected in the dropdwon
    if(wc){
      let schArr = this.unitList.filter((ele) => ele.wcName == wc)[0].scheduleInfo;  // get the sch details of the selected unit
      let schObj = schArr.filter((ele) => ele.schdID == schID)[0];   // get the schObj for the selected schId in the dropdwon
      this.billetFilterCriteriaFormObject.formFields.filter((ele) => ele.ref_key == 'productName')[0].value = schObj.productName; // assign productName
      this.billetFilterCriteriaFormObject.formFields.filter((ele) => ele.ref_key == 'sizeCode')[0].value = schObj.sizeCode;
      this.billetFilterCriteriaFormObject.formFields.filter((ele) => ele.ref_key == 'schdStatus')[0].value = schObj.schdStatus;
    }
  }

  public getSelectedRecord(rowData) {
    this.selectedRecord = rowData;
  }

  public attachBatch() {
    if(!this.isAttachModal){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access Attach Popup. Please contact Admin."});
      return;
    }
    this.batchAttachModalRef = this.dialogService.open(BatchAttachComponent, {
     // header: 'Attach',
      width: '80%',
      contentStyle: {"max-height": "700px", "overflow": "auto"},
      styleClass: 'batch-attach-dialog',
      // autoZIndex: true,
      baseZIndex: 10000,
      data: this.selectedRecord
    });

    this.batchAttachModalRef.onClose.subscribe((data) => {
      if(data != undefined){
        this.retrieve();
      }
    });
  }

  public detachBatch() {
    if(!this.isUnattachModal){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access Unattach Popup. Please contact Admin."});
      return;
    }
    this.batchDetachModalRef = this.dialogService.open(BatchDetachComponent, {
      //header: 'Unattach',
      width: '80%',
      contentStyle: {"max-height": "700px", "overflow": "auto"},
      styleClass: 'batch-unattach-dialog',
      // autoZIndex: true,
      baseZIndex: 10000,
      data: this.selectedRecord
    });

    this.batchDetachModalRef.onClose.subscribe((data) => {
      if(data != undefined){
        this.retrieve();
      }
    });
  }

  public resetFilter() {
    // this.jswComponent.resetForm(this.billetFilterCriteriaFormObject);
    this.billetAttachList =[];
  }

  public retrieve() {
    this.jswComponent.onSubmit(
      this.billetFilterCriteriaFormObject.formName, this.billetFilterCriteriaFormObject.formFields
    );
    var formData = this.jswService.getFormData();
    if(formData && formData.length){
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, {
        productId: null,
        productName: null,
        schdID: null,
        //schdStatus: null,
        sizeCode: null,
        wcName: null
      });
      let schArr = this.unitList.filter((ele) => ele.wcName == formObj.wcName)[0].scheduleInfo;
      let schObj = schArr.filter((ele) => ele.schdID == formObj.schdID)[0];
      formObj.productId = schObj.productId;
      this.ppcService.getFilteredLpHdrScheduleList(formObj).subscribe(Response => {
        this.billetAttachList = Response;
      })
    }
  }

  public getFormRef(event){
    this.formRef = event;
  }

}
