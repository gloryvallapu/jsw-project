import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { PpcService } from '../../../../services/ppc.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';

import { BatchAttachComponent } from './batch-attach.component';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { PpcEndPoints } from '../../../../form-objects/ppc-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { BilletacknowledgementForms, BilletAttachForms, SoModificationFomrs } from '../../../../form-objects/ppc-form-objects';

describe('BatchAttachComponent', () => {
  let component: BatchAttachComponent;
  let fixture: ComponentFixture<BatchAttachComponent>;
  let sharedService;
  let jswServices: JswCoreService;
  let ppcService: PpcService;
  let ref: DynamicDialogRef;
   let config: DynamicDialogConfig
   const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BatchAttachComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        PpcService,
       CommonApiService,
        JswFormComponent,
        PpcEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        BilletacknowledgementForms,
        SoModificationFomrs,
        BilletAttachForms,
        DynamicDialogRef,
        DynamicDialogConfig,

        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchAttachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
