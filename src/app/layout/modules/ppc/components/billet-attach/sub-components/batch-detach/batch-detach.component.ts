import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { PpcService } from '../../../../services/ppc.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-batch-detach',
  templateUrl: './batch-detach.component.html',
  styleUrls: ['./batch-detach.component.css']
})
export class BatchDetachComponent implements OnInit {
  public stockList: any = [];
  public columns: ITableHeader[] = [];
  public viewType = ComponentType;
  public tableType = TableType.gridlines + ' ' + TableType.normal;
  public globalFilterArray: any = [];
  public selectedRecords: any = [];
  public screenStructure: any = [];
  public isSaveBtn: boolean = false;
  public tableRef: any;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, public ppcService: PpcService, public sharedService: SharedService, public authService: AuthService) { }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.Batch_Attach.id)[0];
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.batchAttachSectionIds.Unattach_Screen.buttons.save, menuConstants.batchAttachSectionIds.Unattach_Screen.id, this.screenStructure);
    this.columns = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '40px',
        sortFieldName: '',
    },
    {
      field: 'heatId',
      header: 'Heat ID',
      columnType: ColumnType.string,
      width: '100px',
      sortFieldName: '',
    },
    {
      field: 'vendorHeatId',
      header: 'Purchased Heat No',
      columnType: ColumnType.string,
      width: '130px',
      sortFieldName: '',
    },
    {
      field: 'materialId',
      header: 'Material',
      columnType: ColumnType.string,
      width: '100px',
      sortFieldName: '',
    },
    {
      field: 'grade',
      header: 'Product Grade',
      columnType: ColumnType.string,
      width: '120px',
      sortFieldName: '',
    },
    {
      field: 'size',
      header: 'Product Size',
      columnType: ColumnType.string,
      width: '120px',
      sortFieldName: '',
    },
    {
      field: 'length',
      header: 'Batch Len(mm)',
      columnType: ColumnType.string,
      width: '110px',
      sortFieldName: '',
    },
    // {
    //   field: 'attached',
    //   header: 'Batch Attached',
    //   columnType: ColumnType.string,
    //   width: '140px',
    //   sortFieldName: '',
    // },
    // {
    //   field: 'charged',
    //   header: 'Batch Charged',
    //   columnType: ColumnType.string,
    //   width: '130px',
    //   sortFieldName: '',
    // },
    // {
    //   field: 'weight',
    //   header: 'Batch Wt(tons)',
    //   columnType: ColumnType.string,
    //   width: '120px',
    //   sortFieldName: '',
    // },
    {
      field: 'unattached',
      header: 'Batch Unattached',
      columnType: ColumnType.numericValidationField,
      width: '150px',
      sortFieldName: '',
      isError: false,
      errorMsg: ''
    },
    {
      field: 'pendingForCharge',
      header: 'Pending For Charge',
      columnType: ColumnType.string,
      width: '140px',
      sortFieldName: ''
    },
    {
      field: 'selectedWeight',
      header: 'Unattached Batch Wt(tons)',
      columnType: ColumnType.string,
      width: '180px',
      sortFieldName: '',
      isError: false,
      errorMsg: ''
    },
    {
      field: 'customerName',
      header: 'Customer Name',
      columnType: ColumnType.ellipsis,
      width: '120px',
      sortFieldName: '',
    },
    {
      field: 'soId',
      header: 'Sales Order',
      columnType: ColumnType.string,
      width: '120px',
      sortFieldName: '',
    },
    {
      field: 'orderId',
      header: 'PO ID',
      columnType: ColumnType.string,
      width: '80px',
      sortFieldName: '',
    },
  ]
  this.globalFilterArray = this.columns.map(element => { return element.field;});
  this.getStockList();
  }

  public getStockList(){
    this.ppcService.getStockDetailsForUnattach(this.config.data.inputProductType, this.config.data.ipBatchGrade, this.config.data.ipBatchSection, this.config.data.schdID, this.config.data.orderSeqNo).subscribe(
      data => {
        this.stockList = data;
      }
    )
  }

  public batchDetach(){
    debugger
    if(!this.selectedRecords.length){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please select records'});
      return;
    }
    if(this.columns.filter(ele => ele.isError == true).length > 0 || this.selectedRecords.filter(ele => ele.selected <= 0 ).length > 0){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please enter Batch Unattached count'});
      return;
    }
    this.ppcService.unattachStockDetails(this.sharedService.loggedInUserDetails.userId,this.config.data?.schdID, this.config.data?.orderSeqNo, this.selectedRecords).subscribe(
      data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Selected batches are detached successfully'});
        this.closeModal();
      }
    )
  }

  public validateData(event){
    if(event.colField == 'unattached'){
      let colObj = this.columns.filter(ele => ele.field == 'unattached')[0];
      colObj.rowIndex = event.rowIndex;
      if(event.rowData.unattached > event.rowData.pendingForCharge){
        colObj.isError = true;
        colObj.errorMsg = 'Batch unattached value should not be greater than ' + event.rowData.attached;
      }else{
        colObj.isError = false;
        colObj.errorMsg = '';
      }
      event.rowData.selectedWeight = event.rowData.unattached * event.rowData.weight;
    }
  }

  public getSelectedRecord($event: any){
    this.selectedRecords = $event;
  }

  public closeModal() {
    this.ref.close(this.config.data);
  }

  public getTableRef(event){
    this.tableRef = event;
  }

  public filterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }
}
