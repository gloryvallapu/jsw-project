import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchDetachComponent } from './batch-detach.component';

describe('BatchDetachComponent', () => {
  let component: BatchDetachComponent;
  let fixture: ComponentFixture<BatchDetachComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BatchDetachComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchDetachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
