import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { PpcService } from '../../../../services/ppc.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-batch-attach',
  templateUrl: './batch-attach.component.html',
  styleUrls: ['./batch-attach.component.css']
})
export class BatchAttachComponent implements OnInit {
  public heatNoList: any = [];
  public selectedHeatNo: any;
  public isSearchTriggered: boolean = false;
  public stockList: any = [];
  public columns: ITableHeader[] = [];
  public viewType = ComponentType;
  public tableType = TableType.gridlines + ' ' + TableType.normal;
  public globalFilterArray: any = [];
  public selectedRecords: any = [];
  public screenStructure: any = [];
  public isSaveBtn: boolean = false;
  public tableRef: any;


  //new
  public totalCalPcs=0;
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, public ppcService: PpcService, public sharedService: SharedService, public authService: AuthService) { }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.Batch_Attach.id)[0];
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.batchAttachSectionIds.Attach_Screen.buttons.save, menuConstants.batchAttachSectionIds.Attach_Screen.id, this.screenStructure);
    this.getHeatNo();
    this.columns = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '40px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'vendorHeatId',
        header: 'Purchased Heat No',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'I/P Product Grade',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'I/P Len(mm)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'inSTock',
        header: 'Stock Pcs',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Batch Wt(tons)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'selected',
        header: 'Batch Attached',
        columnType: ColumnType.numericValidationField,
        width: '140px',
        sortFieldName: '',
        isError: false,
        errorMsg: ''
      },
      // {
      //   field: 'selectedWeight',
      //   header: 'Attached Batch Wt(tons)',
      //   columnType: ColumnType.string,
      //   width: '190px',
      //   sortFieldName: '',
      //   isError: false,
      //   errorMsg: ''
      // },
      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.ellipsis,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'soId',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'orderId',
        header: 'PO ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      }
  ]
  this.globalFilterArray = this.columns.map(element => { return element.field;});
  this.getStockList();
  }

  public getHeatNo(){
    this.ppcService.getHeatNos().subscribe(data => {
      this.heatNoList = data.map((x: any) => {
        return { displayName: x, modelValue: x };
      });
    })
  }

  public getStockList(){
    this.ppcService.getStockDetails(this.config.data?.schdID, this.config.data?.orderSeqNo).subscribe(
      data => {
        this.stockList = data;
      }
    )
  }

  public batchAttach(){
    if(!this.selectedRecords.length){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please select records'});
      return;
    }
    if(this.columns.filter(ele => ele.isError == true).length > 0 || this.selectedRecords.filter(ele => !ele.selected || ele.selected == 0).length > 0){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please enter Batch Attached count'});
      return;
    }
    if(this.totalCalPcs > (this.config.data.plannedPcs - this.config.data.attached)){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Selected Pcs is more than Planned Pcs'});
      return;
    }
    this.ppcService.attachStockDetails(this.config.data.schdID, this.config.data.orderSeqNo, this.sharedService.loggedInUserDetails.userId, this.selectedRecords).subscribe(
      data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Selected batches are attached successfully'});
        this.closeModal();
      }
    )
  }

  public getSelectedRecord($event: any){
    this.totalCalPcs = 0;
    this.selectedRecords = $event;
    this.selectedRecords.forEach(element => {
      this.totalCalPcs = this.totalCalPcs + element.selected;
    });
  }

  public validateData(event){
    if(event.colField == 'selected'){
      let colObj = this.columns.filter(ele => ele.field == 'selected')[0];
      colObj.rowIndex = event.rowIndex;
      if(event.rowData.selected > event.rowData.inSTock){
        colObj.isError = true;
        colObj.errorMsg = 'Batch attached value should be less than or equal to ' + event.rowData.inSTock;
      }else if(event.rowData.selected > (this.config.data.plannedPcs - (this.config.data.productAttached ? this.config.data.productAttached : 0))){
        colObj.isError = false;
        colObj.errorMsg = 'Batch attached value should be less than Batches available to attach';
      }else{
        colObj.isError = false;
        colObj.errorMsg = '';
      }
      event.rowData.selectedWeight = event.rowData.selected * event.rowData.weight;
      //this.totalCalPcs = this.totalCalPcs + event.rowData.selected;
      this.totalCalPcs = 0;
      this.selectedRecords.forEach(element => {
      this.totalCalPcs = this.totalCalPcs + element.selected;
    });
    }
  }

  public closeModal() {
    this.ref.close(this.config.data);
  }

  public getTableRef(event){
    this.tableRef = event;
  }

  public filterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }
}
