import { Component, OnInit, ViewChild } from '@angular/core';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ColumnType } from 'src/assets/enums/common-enum';
import { TableType } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService } from 'jsw-core';
import { JswFormComponent } from 'jsw-core';
import { Table } from 'primeng/table';
import { PpcService } from '../../services/ppc.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';

const MIME_TYPES = {
  pdf: 'application/pdf',
};
@Component({
  selector: 'app-order-book',
  templateUrl: './order-book.component.html',
  styleUrls: ['./order-book.component.css'],
})
export class OrderBookComponent implements OnInit {
  @ViewChild('jswForm') JswFormComponent: any;
  products: any = [];
  formObject: any;
  columns: ITableHeader[];
  tableType: any;
  viewType = ComponentType;
  public productType: any;
  @ViewChild('dt') table: Table;
  productList: any;
  tempProducts: any;
  gradeList: any;
  sizeList: any;
  soIdList: any;
  psnList: any;
  customerList: any;
  globalFilterArray: any = [];
  blob: Blob;
  public columnType = ColumnType;
  public screenStructure: any = [];
  public isPDFBtn: boolean = false;
  public isExcelBtn: boolean = false;
  public isRefreshBtn: boolean = false;
  public menuConstants = menuConstants;
  public frozenCols: any =[];
  public scrollableCols: any[];
  public frznWidth = '800px'
  public maxConstraint = 4;

  //new
  public totalBtr;
  public totalBtc;
  public totalSmsWip;
  public totalLpWip;

  constructor(
    public sharedService: SharedService,
    public core: JswFormComponent,
    public jswService: JswCoreService,
    public ppcService: PpcService,
    public authService: AuthService
  ) {
    this.columns = [
      {
        field: 'productType',
        header: 'Product Type',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: 'srNo',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: 'productType',
      },
      {
        field: 'size',
        header: 'Size',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: 'soId',
      },
      {
        field: 'soId',
        header: 'SO Id',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'lineNo',
        header: 'Line No',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'orderQty',
        header: 'Order Qty',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Length(mm)',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'lengthMin',
        header: 'Length Min(mm)',
        columnType: ColumnType.number,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'lengthMax',
        header: 'Length Max(mm)',
        columnType: ColumnType.number,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'productionOrderNo',
        header: 'Production Order No',
        columnType: ColumnType.number,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'supplyCondition',
        header: 'Supply Condition',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'psn',
        header: 'PSN',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'customer',
        header: 'Customer',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'btc',
        header: 'BTC(Tons)',
        columnType: ColumnType.number,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'btr',
        header: 'BTR(Tons)',
        columnType: ColumnType.number,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'productQuantity',
        header: 'Product Quantity',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'stockQuantity',
        header: 'Stock Quantity',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'orderRemark',
        header: 'Order Remark',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'finalMaterial',
        header: 'Final Material',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'materialId',
        header: 'Material Id',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'equivalentSpecification',
        header: 'Equivalent Specification',
        columnType: ColumnType.number,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'soReleaseDate',
        header: ' SO Rel Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'reqDeliveryDate',
        header: 'Req Delivery Date',
        columnType: ColumnType.date,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'rollUnit',
        header: 'Roll Unit',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'destination',
        header: 'Destination',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'sizeTolarance',
        header: 'Size Tolerance',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'soDocumentType',
        header: 'So Document Type',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'endApplication',
        header: 'End Application',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'qaReject',
        header: 'QA Reject',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'processRoute',
        header: 'Process Route',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'tpiFlag',
        header: 'TPI Hold',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'orderStatus',
        header: 'Order Status',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      }
    ];
    this.frozenCols = [
      {
        field: 'productType',
        header: 'Type',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: 'productType',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '70px',
        sortFieldName: 'grade',
      },
      {
        field: 'size',
        header: 'Size',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: 'soId',
      },
      {
        field: 'soId',
        header: 'SO ID',
        columnType: ColumnType.number,
        width: '65px',
        sortFieldName: '',
      },
      {
        field: 'lineNo',
        header: 'Line No',
        columnType: ColumnType.string,
        width: '45px',
        sortFieldName: '',
      },
      {
        field: 'orderQty',
        header: 'Order Qty',
        columnType: ColumnType.string,
        width: '55px',
        sortFieldName: '',
        tooltip:'SO Qty [from SAP]'
      },
      {
        field: 'btc',
        header: 'BTC(Tons)',
        columnType: ColumnType.number,
        width: '50px',
        sortFieldName: '',
        tooltip:'BTP SMS - ( SMS WIP Qty)'
      },
      {
        field: 'btr',
        header: 'BTR(Tons)',
        columnType: ColumnType.number,
        width: '50px',
        sortFieldName: '',
        tooltip:'BTP LP - ( LP WIP Qty)'
      }
    ]
    this.scrollableCols = [


      {
        field: 'length',
        header: 'Len(mm)',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'lengthMin',
        header: 'Len Min(mm)',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'lengthMax',
        header: 'Len Max(mm)',
        columnType: ColumnType.number,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'smsProductionOrderNo',
        header: 'SMS PO ID',
        columnType: ColumnType.number,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'lpProductionOrderNo',
        header: 'LP PO ID',
        columnType: ColumnType.number,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'supplyCondition',
        header: 'Supply Cond.',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'psn',
        header: 'PSN',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'customer',
        header: 'Customer',
        columnType: ColumnType.ellipsis,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'productQuantity',
        header: 'Prod Qty',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
        tooltip:'Total : Pending to Conf Qty+ Hold qty + Ready for Dispatch + Ready for Next Process [for LP Products]'
      },
      {
        field: 'stockQuantity',
        header: 'Stock Qty',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
        tooltip:'Total : Pending to Conf Qty+ Hold qty + Ready for Dispatch + Ready for Next Process [for SMS Products]'
      },
      {
        field: 'orderRemark',
        header: 'Order Remark',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'finalMaterial',
        header: 'Fin Material',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'materialId',
        header: 'Material Id',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'equivalentSpecification',
        header: 'Eqv Spec',
        columnType: ColumnType.ellipsis,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'soReleaseDate',
        header: ' SO Rel Date',
        columnType: ColumnType.date,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'reqDeliveryDate',
        header: 'Req Dlvry Date',
        columnType: ColumnType.date,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'rollUnit',
        header: 'Roll Unit',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'destination',
        header: 'Destination',
        columnType: ColumnType.ellipsis,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'sizeTolarance',
        header: 'Size Tolerance',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'soDocumentType',
        header: 'So Doc Type',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'endApplication',
        header: 'End Applctn',
        columnType: ColumnType.ellipsis,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'processRoute',
        header: 'Process Route',
        columnType: ColumnType.ellipsis,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'tpiFlag',
        header: 'TPI Hold',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'orderStatus',
        header: 'Order Status',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      // {
      //   field: 'SOQty',
      //   header: 'SO Qty',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      {
        field: 'yield',
        header: 'Yield',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'lpPoQty',
        header: 'LP PO Qty',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
        tooltip:'LP PO Qty',
        color:'primary'
      },
      {
        field: 'dispatchedQty',
        header: 'Disp Qty',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
        tooltip:'Total Dispatched Qty [from SAP]',
        color:'primary'
      },
      {
        field: 'lpQaClearQty',
        header: 'LP QA Clear Qty',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
        tooltip:'Total Ready To Dispatch Qty',
        color:'primary'
      },
      {
        field: 'lpQaRejectQty',
        header: 'LP QA Reject Qty',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
        tooltip:'Total Qty : Current SO is not same as Planned SO',
        color:'primary'
      },
      {
        field: 'lpQaPendingQty',
        header: 'LP QA Pend Qty',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
        tooltip:'Total Hold Qty',
        color:'primary'
      },
      {
        field: 'lpProdConfPendingQty',
        header: 'LP Conf Pend Qty',
        columnType: ColumnType.number,
        width: '130px',
        sortFieldName: '',
        tooltip:'Total Confirmation Pending Qty',
        color:'primary'
      },
      {
        field: 'lpSchdQty',
        header: 'LP Schd Qty',
        columnType: ColumnType.number,
        width: '110px',
        sortFieldName: '',
        tooltip:'Total Schd Qty',
        color:'primary'
      },
      {
        field: 'lpPendingToAttachQty',
        header: 'LP Pend Atch Qty',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
        tooltip:'Total Schd Qty - Total Qty  (Attached , Charge , Discharge , Rolled , Hot out , Cobble , Mix , Scrap , Back to Stock) [from Schd Tracking]',
        color:'primary'
      },
      {
        field: 'lpAttachedQty',
        header: 'LP Atch Qty',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
        tooltip:'Total Attach Qty  [from Schd Tracking]',
        color:'primary'
      },
      {
        field: 'lpChargedQty',
        header: 'LP Chrg Qty',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
        tooltip:'Total Charge Qty  [from Schd Tracking]',
        color:'primary'
      },
      {
        field: 'lpWipQty',
        header: 'LP WIP Qty',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
        tooltip:'Pending to Attach qty + Total Qty (Attached , Charge , Discharge , Rolled , Mix) [from Schd Tracking]',
        color:'primary'
      },
      {
        field: 'btdQty',
        header: 'BTD LP',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
        tooltip:'LP PO Qty - Dispatched Qty',
        color:'primary'
      },
      {
        field: 'lpBtpQty',
        header: 'BTP LP',
        columnType: ColumnType.number,
        width: '80px',
        sortFieldName: '',
        tooltip:'BTD - (LP QA Clear Qty + LP QA Pending Qty + LP Production Confirmation Pending Qty )',
        color:'primary'
      },
      // {
      //   field: 'btr',
      //   header: 'BTR(Tons)',
      //   columnType: ColumnType.number,
      //   width: '90px',
      //   sortFieldName: '',
      // },
      {
        field: 'smsPoQty',
        header: 'SMS PO Qty',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
        tooltip:'SMS PO Qty / ( BTR + LP Pending to Attach Qty ) * Process Yield',
        color:'info'
      },
      {
        field: 'smsQaClearQty',
        header: 'SMS QA Clear Qty',
        columnType: ColumnType.number,
        width: '130px',
        sortFieldName: '',
        tooltip:'Total Ready To Dispatch Qty / Total Ready for Next Process Qty',
        color:'info'
      },
      {
        field: 'smsQaRejectQty',
        header: 'SMS QA Reject Qty',
        columnType: ColumnType.number,
        width: '130px',
        sortFieldName: '',
        tooltip:'Total Qty : Current SO is not same as Planned SO',
        color:'info'
      },
      {
        field: 'smsQaPendingQty',
        header: 'SMS QA Pend Qty',
        columnType: ColumnType.number,
        width: '130px',
        sortFieldName: '',
        tooltip:'Total Hold Qty',
        color:'info'
      },
      {
        field: 'smsSchdQty',
        header: 'SMS Schd Qty',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
        tooltip:'Total Schd Qty',
        color:'info'
      },
      {
        field: 'smsWipQty',
        header: 'SMS WIP Qty',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
        tooltip:'Total Schd Qty (Created , Ready to Build ,  Ready to Release) + Total Heat Qty (Heat Under Process , Ready to Process , New Unplanned Heat)',
        color:'info'
      },
      {
        field: 'smsCancSchdQty',
        header: 'Schd Cancel Qty',
        columnType: ColumnType.number,
        width: '130px',
        sortFieldName: '',
        tooltip:'Total Schd Cancel Qty',
        color:'info'
      },
      {
        field: 'smsBtpQty',
        header: 'BTP SMS',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
        tooltip:'BTD - (SMS QA Clear Qty + SMS QA Pending Qty )',
        color:'info'
      },
      // {
      //   field: 'btc',
      //   header: 'BTC(Tons)',
      //   columnType: ColumnType.number,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      {
        field: 'nextUnit',
        header: 'Next Unit',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'obStatus',
        header: 'OB Status',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'orderId',
        header: 'Order Id',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'routeIndicator',
        header: 'Route Indicator',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      }
    ]
    this.globalFilterArray = this.columns.map((element) => {
      return element.field;
    });
  }

  ngOnInit() {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.Order_Book.id)[0];
    this.isPDFBtn = this.sharedService.isButtonVisible(true, menuConstants.orderBookSectionIds.List.buttons.pdf, menuConstants.orderBookSectionIds.List.id, this.screenStructure);
    this.isExcelBtn = this.sharedService.isButtonVisible(true, menuConstants.orderBookSectionIds.List.buttons.excel, menuConstants.orderBookSectionIds.List.id, this.screenStructure);
    this.isRefreshBtn = this.sharedService.isButtonVisible(true, menuConstants.orderBookSectionIds.List.buttons.refresh, menuConstants.orderBookSectionIds.List.id, this.screenStructure);
    this.orderBookDetails();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
  }

  public filterTable(event: any) {
    this.table.filterGlobal(event.target.value, 'contains');
  }

 downloadFile(fileType: string) {
    if (fileType == 'excel') {
      // let tableHeader = [this.columns.map((ele) => ele.header)];
      // let keyArr = this.columns.map((ele:any) => ele.field);
      // let data = this.sharedService.getDataForExcelAsPerColKeys(this.products, keyArr);
      // this.sharedService.downloadCSV(data, 'Sales Order', tableHeader);
      //this.sharedService.downloadCSV(this.products, 'Sales Order', tableHeader);
      this.ppcService.downloadOrderBook('EXCEL')
        .subscribe((Response) => {
          this.sharedService.generateReport(fileType, Response, 'SalesOrder')
        });
    }
    if (fileType == 'pdf') {
      this.ppcService.downloadOrderBook('PDF')
        .subscribe((Response) => {
          this.sharedService.generateReport(fileType, Response, 'SalesOrder')
        });
    }





    // if (fileType == 'pdf') {
    //   var URL = this.ppcService.downloadOrderBookPDF('PDF').toString();
    //   .subscribe((Response) => {
    //     this.sharedService.generateReport(event, Response)
    //   });
    //   window.open(URL,'_blank');
    //   //window.open("http://ec2-54-243-199-173.compute-1.amazonaws.com:8452/jispl/ppc/downloadOrderBookPDF",'_blank');
    // }
  }

  public orderBookDetails() {
    this.ppcService.orderBookDetails().subscribe((data) => {
      this.tempProducts = data.orderBookBoList;
      this.totalBtr = data.totBtr;
      this.totalBtc = data.totBtc;
      this.totalSmsWip = data.totSmsWip;
      this.totalLpWip = data.totLpWip;
      var modifiedData1 = data.orderBookBoList.map((x: any) => {
        x.reqDeliveryDate = new Date(this.sharedService.get_MM_DD_YYYY_Date(x.reqDeliveryDate));
        x.soReleaseDate = new Date(this.sharedService.get_MM_DD_YYYY_Date(x.soReleaseDate));
        x.tpiFlag = x.tpiFlag && x.tpiFlag.toLowerCase() == 'y' ? 'Y' : 'N'
        return x;
      });
      this.products = modifiedData1;
      this.productList = this.sharedService.getUniqueRecords(data.orderBookBoList.map((x: any) => {
        return { displayName: x.productType, modelValue: x.productType };
      }));
      this.gradeList = this.sharedService.getUniqueRecords(data.orderBookBoList.map((x: any) => {
        return { displayName: x.grade, modelValue: x.grade };
      }));
      this.sizeList = this.sharedService.getUniqueRecords(data.orderBookBoList.map((x: any) => {
        return { displayName: x.size, modelValue: x.size };
      }));
      this.soIdList = this.sharedService.getUniqueRecords(data.orderBookBoList.map((x: any) => {
        return { displayName: x.soId, modelValue: x.soId };
      }));
      this.psnList = this.sharedService.getUniqueRecords(data.orderBookBoList.map((x: any) => {
        return { displayName: x.psn, modelValue: x.psn };
      }));
      this.customerList = this.sharedService.getUniqueRecords(data.orderBookBoList.map((x: any) => {
        return { displayName: x.customer, modelValue: x.customer };
      }));
    });
  }

  public dataFilter() {
    var data = this.products.filter(
      (x: any) => x.productType == this.productType[0]
    );
    this.products = data;
    if (this.products.length == 0) {
      this.products = this.tempProducts;
    }
  }

  public productfilter(eve: any){
  }

  public refreshData(){
    this.table.filterGlobal('', 'contains');
    this.orderBookDetails();
  }
}
