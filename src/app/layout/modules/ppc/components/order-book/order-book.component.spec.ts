import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderBookComponent } from './order-book.component';
import { OrderBookForms } from 'src/assets/constants/PPC/ppc-form-objects';
import { SharedService } from 'src/app/shared/services/shared.service';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import{JswCoreComponent,JswCoreService,JswCoreModule} from 'jsw-core';

describe('OrderBookComponent', () => {
  let component: OrderBookComponent;
  let fixture: ComponentFixture<OrderBookComponent>;
  let sharedService : SharedService;
  let orderBookForms : OrderBookForms;
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderBookComponent ],
      imports:[JswCoreModule],
      providers:[SharedService,
      OrderBookForms,
      { provide: ToastrService, useValue: toastrService }]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderBookComponent);
    component = fixture.componentInstance;
    sharedService = fixture.debugElement.injector.get(SharedService);
    orderBookForms = fixture.debugElement.injector.get(OrderBookForms);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
