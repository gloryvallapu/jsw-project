import {
  Component,
  OnInit
} from '@angular/core';
import {
  ComponentType
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  ITableHeader
} from 'src/assets/interfaces/table-headers';
import {
  TableType,
  ColumnType,
  LovCodes
} from 'src/assets/enums/common-enum';
import {
  PoamendForms
} from '../../form-objects/ppc-form-objects';
import {
  PpcService
} from '../../services/ppc.service';
import {
  JswCoreService,
  JswFormComponent
} from 'jsw-core';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  PrimeNGConfig
} from 'primeng/api';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import {
  Subject
} from 'rxjs';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-po-amend',
  templateUrl: './po-amend.component.html',
  styleUrls: ['./po-amend.component.css']
})
export class POAmendComponent implements OnInit {
  public totalRecords:number = 0;
  public clearSelection: Subject < boolean > = new Subject < boolean > ();
  public  display: boolean = false;
  public poAmendList: any = [];
  public columnType = ColumnType;
  public filterCriteria: any = [];
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public billetList: any = [];
  public selectedBatches: any[];
  public open: boolean = true;
  public hold: boolean = false;
  public holdRolling: boolean = false;
  public close: boolean = false;
  public selectedRecords: any = []
  public statusList: any;
  public paginationCount: number = 1;
  public productionControlstatus: any = {
    open: true,
    hold: false,
    holdRolling: false,
    close: false
  }
  public screenStructure: any = [];
  public isFilterCrtSec: boolean = false;
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isListSection: boolean = false;
  public isSaveBtn: boolean = false;
  public negativePOColums:any = [];
  negativePOlist: any;
  negativeBTCBtr: any;
  public formRef: NgForm;
  public maxDate = new Date();
  constructor(public poAmendForms: PoamendForms, public ppcSevice: PpcService, private primengConfig: PrimeNGConfig,
    public jswService: JswCoreService, public authService: AuthService,
    public jswComponent: JswFormComponent, public sharedService: SharedService) {}

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.PO_Amend.id)[0];
    this.isFilterCrtSec = this.sharedService.isSectionVisible(menuConstants.poAmendSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.poAmendSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.poAmendSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.poAmendSectionIds.Filter_Criteria.buttons.reset, menuConstants.poAmendSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.poAmendSectionIds.List.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.poAmendSectionIds.List.buttons.save, menuConstants.poAmendSectionIds.List.id, this.screenStructure);
    this.primengConfig.ripple = true;
    this.filterCriteria = JSON.parse(JSON.stringify(this.poAmendForms.poAmendFilterCriteriaForms));
    this.maxDate.setDate(this.maxDate.getDate());
    this.filterCriteria.formFields.forEach(element => {
      if(element.ref_key == 'toDate' || element.ref_key == 'fromDate'){
        element.maxDate = this.maxDate
      }
    });
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.columns = this.createColumns();
    this.negativePOColums = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '40px',
        sortFieldName: '',
      },
      {
        field: 'productionOrderNo',
        header: 'PO ID',
        columnType: ColumnType.string,
        width: '60px',
        sortFieldName: '',
      },
      {
        field: 'btc',
        header: 'BTC(Tons)',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'btr',
        header: 'BTR(Tons)',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },

    ]
    this.getProductOrderQuantityList(this.paginationCount);
    this.getDistinctMaterialIdList();
    this.getDistinctGradeList();
    this.prodControlStatus();
  };

  public getSelectedRecord(event){
    this.negativeBTCBtr= event;
    // this.display = false;
  }

  public retrive(): any {
    this.jswComponent.onSubmit(
      this.filterCriteria.formName,
      this.filterCriteria.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData == undefined) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please enter Required Values '
        }
      );
    }

    let object = {
      "salseOrder": "string",
      "fromDate": "string",
      "toDate": "string",
      "gradeGrp": "string",
      "MID": "string",
    }
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, object);
    let requestBody = {

      "dateFrom": formObj.fromDate,
      "dateTo": formObj.toDate,
      "grade": formObj.gradeGrp,
      "isCloseSelected": this.productionControlstatus.close,
      "isHoldRollingSelected": this.productionControlstatus.holdRolling,
      "isHoldSelected": this.productionControlstatus.hold,
      "isOpenSelected": this.productionControlstatus.open,
      "materialId": formObj.MID,
      "materialDesc": formObj.MID == null ? null : this.filterCriteria.formFields.filter((ele: any) => ele.ref_key == "MID")[0].list.filter((x: any) => x.modelValue == formObj.MID)[0].displayName,
      "salesOrder": formObj.salseOrder

    }
    this.ppcSevice.getFilteredPrductOrderQuantity('1', requestBody).subscribe(Response => {
      this.totalRecords = Response.totalProdOrdsCount;
      Response.productionOrderQuantityBo.map((x: any) => {
        x.OrderSection = x.lpSchedule == true ? 'LP' : "Caster",
          x.btr = x.smsSchedule == true ? null : x.btr
        x.disabledAll = x.prodControlStatus == "Close" ? true : false
        x.onlySpecificFileds = x.prodControlStatus == "Hold" ? true : false

        if (x.prodControlStatus == 'Hold_Rolling') {
          x.disabledSMS = x.smsSchedule == true ? x.smsSchedule : false
          x.disabledLP = x.lpSchedule == true ? x.lpSchedule : false
        }

        if (x.prodControlStatus == 'Open') {
          x.disabledSMS = x.smsSchedule == true ? x.smsSchedule : false
        }
        x.materialId = this.filterCriteria.formFields.filter((ele: any) => ele.ref_key == "MID")[0].list.filter((y: any) => y.modelValue == x.materialId)[0].displayName
        x.poAmendQty = 0;
        return x
      })
      this.poAmendList = Response.productionOrderQuantityBo;
    })
  }

  public resetFilter() {
    this.getProductOrderQuantityList(1);
    //this.jswComponent.resetForm(this.filterCriteria);
    this.productionControlstatus = {
      open: true,
      hold: false,
      holdRolling: false,
      close: false
    }
  }

  public getSelectedSchList() {
    //
  }

  public checkProductionStatus(rowData) {
    //
  }

  public createColumns() {
    return [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '40px',
        sortFieldName: '',
      },
      {
        field: 'soId',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'customer',
        header: 'Customer Name',
        columnType: ColumnType.ellipsis,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'poStatus',
        header: 'Status',
        columnType: ColumnType.string,
        width: '70px',
        sortFieldName: '',
      },
      {
        field: 'materialId',
        header: 'Material ID',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'soFinalMaterial',
        header: 'Final Material',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'prodControlStatus',
        header: 'SO Prod Sts',
        columnType: ColumnType.dropdown,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'poAmendQty',
        header: 'Amend Qty(Tons)',
        columnType: ColumnType.text,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'poOrigQty',
        header: 'Original PO Qty(Tons)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'poQty',
        header: 'PO Qty(Tons)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'poExcessQty',
        header: 'PO Excess Qty(Tons)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'btc',
        header: 'BTC(Tons)',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'btr',
        header: 'BTR(Tons)',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'productionOrderNo',
        header: 'PO ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'OrderSection',
        header: 'Caster/LP',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
    ];
  }

  public showDialog() {
    this.display = true;
}


public closeDialog(){
  this.display = false;
  this.savePOAmmendData(this.selectedRecords.filter((x:any)=>x.btr >0 || x.btc>0));
}


public ContinueWithNegative(){
  this.display = false;
  let positiveRecords= this.selectedRecords.filter((x:any)=>x.btr >0 || x.btc>0).concat(this.negativeBTCBtr);
  this.savePOAmmendData(positiveRecords);
}


  public updateSchedule(): any {
    if (this.selectedRecords.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Select SO Order to Save`,
        }
      );
    }
    if (this.selectedRecords.filter((x: any) => x.smsAmendQtyError == true || x.lPAmendQtyError == true).length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter Required fields`,
        }
      );
    }
    let negativebtrPOlist = this.selectedRecords.filter((x:any)=>x.btr < 0 );
    let negativebtcPOlist = this.selectedRecords.filter((x:any)=> x.btc < 0);
    this.negativePOlist = negativebtrPOlist.concat(negativebtcPOlist);
    this.display = this.negativePOlist.length > 0
    if(this.negativePOlist.length == 0){
      this.savePOAmmendData(this.selectedRecords)
    }
  };


  public savePOAmmendData(records){
    let requestBody = records.map((x: any) => {
      return {
        "lineNo": x.lineNo,
        "lpSchedule": x.lpSchedule,
        "poAmendQty": x.poAmendQty,
        "poOrigQty": x.poOrigQty,
        "poQty": x.poQty,
        "poStatus": x.poStatus,
        "prodControlStatus": x.prodControlStatus,
        "productionOrderNo": x.productionOrderNo,
        "smsSchedule": x.smsSchedule,
        "soId": x.soId,
        "userId": this.sharedService.loggedInUserDetails.userId
      }

    })
    this.ppcSevice.amendProductionOrderQty(requestBody).subscribe(Response => {
      this.negativeBTCBtr = [];
      records.map((x: any) => {
        x.poAmendQty = 0
      })
     // this.clearSelection.next(true);
      // this.sharedService.resetForm(this.filterCriteria,this.formRef);
      // this.productionControlstatus = {
      //   open: true,
      //   hold: false,
      //   holdRolling: false,
      //   close: false
      // }
      // this.getProductOrderQuantityList(this.paginationCount);

      this.selectedRecords = []
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: Response,
        }
      );
    })

  }


  public getSMSAmendQty(rowData): any {
    //new ravi sir change sugg
    // if (parseInt(rowData.poAmendQty) > parseInt(rowData.poOrigQty)) {
    //   rowData.smsAmendQtyError = true
    //   return this.sharedService.displayToastrMessage(
    //     this.sharedService.toastType.Warning, {
    //       message: `Amend Quantity should be less than PO Original Quantity`,
    //     }
    //   );
    // }
    rowData.smsAmendQtyError = false
    rowData.smsPOQty = parseInt(rowData.poOrigQty) + parseInt(rowData.poAmendQty)
  }

  public getLPAmendQty(rowData): any {
    if (parseInt(rowData.lpAmendQty) > parseInt(rowData.lpPOOrigQty)) {
      rowData.lPAmendQtyError = true
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `LP Amend Quantity should be less than LP PO Original Quantity`,
        }
      );
    }
    rowData.lPAmendQtyError = false
    rowData.lpPOQty = parseInt(rowData.lpPOOrigQty) + parseInt(rowData.lpAmendQty)
  }


  public getProductOrderQuantityList(count) {
    this.ppcSevice.getProductOrderQuantityList(count == 0 || count == -1 ? 1 : count).subscribe(Response => {
      this.totalRecords = Response.totalProdOrdsCount;
      Response.productionOrderQuantityBo.map((x: any) => {
        x.OrderSection = x.lpSchedule == true ? 'LP' : "Caster",
          x.btr = x.smsSchedule == true ? null : x.btr,
          x.disabledAll = x.prodControlStatus == "Close" ? true : false
        x.onlySpecificFileds = x.prodControlStatus == "Hold" ? true : false
        if (x.prodControlStatus == 'Hold_Rolling') {
          x.disabledSMS = x.smsSchedule == true ? x.smsSchedule : false
          x.disabledLP = x.lpSchedule == true ? x.lpSchedule : false
        }
        if (x.prodControlStatus == 'Open') {
          x.disabledSMS = x.smsSchedule == true ? x.smsSchedule : false
        }
        x.materialId = this.filterCriteria.formFields.filter((ele: any) => ele.ref_key == "MID")[0].list.filter((y: any) => y.modelValue == x.materialId)[0].displayName
        x.poAmendQty = 0;
        return x
      })
      this.poAmendList = Response.productionOrderQuantityBo;
    })
  }


  public getDistinctMaterialIdList() {
    this.ppcSevice.getDistinctMaterialIdList().subscribe(Response => {
      this.filterCriteria.formFields.filter((ele: any) => ele.ref_key == "MID")[0].list = Response.map((ele: any) => {
        return {
          displayName: ele.materialDesc,
          modelValue: ele.materialId
        }
      });
    })
  }

  public getDistinctGradeList() {
    this.ppcSevice.getDistinctGradeList().subscribe(Response => {
      this.filterCriteria.formFields.filter((ele: any) => ele.ref_key == "gradeGrp")[0].list = Response.map((ele: any) => {
        return {
          displayName: ele,
          modelValue: ele
        }
      });
    })
  }


  public prodControlStatus() {
    this.ppcSevice.prodControlStatus().subscribe(Response => {
      this.statusList = Response.map((ele: any) => {
        return {
          displayName: ele.valueDescription,
          modelValue: ele.valueShortCode
        }
      })
    })
  }

  public nextPage(event) {
    this.getProductOrderQuantityList(event.first / 50 + 1)
  }
  public getFormRef(event){
    this.formRef = event;
  }


}
