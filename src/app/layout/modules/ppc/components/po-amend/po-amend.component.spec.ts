import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';

import { PpcService } from '../../services/ppc.service';
import { JswCoreService,  JswFormComponent} from 'jsw-core';
import { SharedService } from 'src/app/shared/services/shared.service';
import { PrimeNGConfig, SharedModule } from 'primeng/api';

import { POAmendComponent } from './po-amend.component';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { PpcEndPoints } from '../../form-objects/ppc-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { PoamendForms } from '../../form-objects/ppc-form-objects';

describe('POAmendComponent', () => {
  let component: POAmendComponent;
  let fixture: ComponentFixture<POAmendComponent>;
  let poAmendForms:PoamendForms;
  let ppcSevice: PpcService;
  let primengConfig: PrimeNGConfig;
  let jswService: JswCoreService;
  let jswComponent: JswFormComponent;
  let sharedService:SharedService;
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ POAmendComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        PpcService,
       CommonApiService,
        JswFormComponent,
        PpcEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        PoamendForms,
        PrimeNGConfig,
        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(POAmendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
