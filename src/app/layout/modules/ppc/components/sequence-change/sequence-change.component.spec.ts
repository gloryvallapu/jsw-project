import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ColumnType, TableType, } from 'src/assets/enums/common-enum';
import { PpcService } from '../../services/ppc.service';

import { SequenceChangeComponent } from './sequence-change.component';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmationService } from 'primeng/api';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { SharedModule } from 'src/app/shared/shared.module';
import { OrderBookForms } from 'src/assets/constants/PPC/ppc-form-objects';
import { PpcEndPoints } from '../../form-objects/ppc-api-endpoints';
import { CasterScheduleForms, LongProductScheduleForms, BilletacknowledgementForms, SoModificationFomrs, ReApplicationScreen, BilletAttachForms, PoamendForms } from '../../form-objects/ppc-form-objects';

describe('SequenceChangeComponent', () => {
  let component: SequenceChangeComponent;
  let fixture: ComponentFixture<SequenceChangeComponent>;
  let sharedService: SharedService;
    let jswService: JswCoreService;
    let jswComponent: JswFormComponent;
    let ppcService: PpcService;
    let commonService: CommonApiService;
    const toastrService = {
      success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
      error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
    };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SequenceChangeComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        ConfirmationService,
        CommonApiService,
        API_Constants,
        CommonAPIEndPoints,
        OrderBookForms,
       PpcEndPoints,
       PpcService,
    CasterScheduleForms,
    LongProductScheduleForms,
    BilletacknowledgementForms,
    SoModificationFomrs,
    ReApplicationScreen,
    BilletAttachForms,
    PoamendForms,
    JswFormComponent,
    { provide: ToastrService, useValue: toastrService },
      ]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SequenceChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
