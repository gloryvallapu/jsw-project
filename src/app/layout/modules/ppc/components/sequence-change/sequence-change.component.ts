import { Component, OnInit } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ColumnType, TableType, } from 'src/assets/enums/common-enum';
import { PpcService } from '../../services/ppc.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-sequence-change',
  templateUrl: './sequence-change.component.html',
  styleUrls: ['./sequence-change.component.css']
})
export class SequenceChangeComponent implements OnInit {
  public unitList:any =[]
  public selectedUnit:any;
  public sequenceList:any = [];
  public columns:any = [];
  public tableType: any;
  public viewType = ComponentType;
  public columnType = ColumnType;
  public screenStructure: any = [];
  public isFilterCrtSec: boolean = false;
  public isRetrieveBtn: boolean = false;
  public isListSection: boolean = false;
  public isSaveBtn: boolean = false;

  constructor(public commonService: CommonApiService, public ppcService: PpcService, public sharedService: SharedService, public authService: AuthService) { }
  public createColumns() {
    return [{
        field: 'sequence',
        header: 'Sequence',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'schdId',
        header: 'Schedule ID',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },

      {
        field: 'productType',
        header: 'Product Type',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'sizeCode',
        header: 'Product Size',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },

      {
        field: 'plannedStartDate',
        header: 'Planned Start Date',
        columnType: ColumnType.calender,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'plannedEndDate',
        header: 'Planned End Date',
        columnType: ColumnType.calender,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'duration',
        header: 'Duration',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'timeGap',
        header: 'Time Gap',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'noOfHeats',
        header: 'No of Heats',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'hetaQty',
        header: 'Heat Qty',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
    ];
  }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.PPC)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.ppcScreenIds.Sequence_Change.id)[0];
    this.isFilterCrtSec = this.sharedService.isSectionVisible(menuConstants.seqChangeSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.seqChangeSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.seqChangeSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.seqChangeSectionIds.List.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.seqChangeSectionIds.List.buttons.save, menuConstants.seqChangeSectionIds.List.id, this.screenStructure);
    this.getWorkCenterList();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.columns = this.createColumns();
  }

  public updateSequence(){
    this.sequenceList.map((x:any) => {
      x.userId = this.sharedService.loggedInUserDetails.userId;
      x.plannedStartDate = new Date(x.plannedStartDate);
      x.plannedEndDate = x.plannedEndDate != null ? new Date(x.plannedEndDate) : null;
      return x
    })
    this.ppcService.saveSchduleSequence(this.sequenceList).subscribe(Response =>{
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: Response,
        }
      );
      this.retrive();
    })
  }

  private getTime(date?: Date) {
    return date != null ? date.getTime() : 0;
  }

  public retrive(){
    this.ppcService.getScheduleSequence(this.selectedUnit).subscribe(Response =>{
      Response.map((x:any)=>{
        x.plannedStartDate = x.plannedStartDate ? new Date(x.plannedStartDate) : null
        x.plannedEndDate = x.plannedEndDate ? new Date(x.plannedEndDate) : null
      return x
      })
      Response.sort((a: any, b: any) => {
        return this.getTime(a.plannedStartDate) - this.getTime(b.plannedStartDate);
    })
    this.sequenceList = Response;
    })
  }

  public getWorkCenterList(){
    this.commonService.getWorkCenterListByUserId().subscribe(Response =>{
      this.unitList = Response;
    })
  }

  public saveReorderedColumns(event){
    if(event.dropIndex == 0){
      this.sequenceList[event.dropIndex].plannedStartDate = this.sequenceList[1].plannedStartDate;
      this.sequenceList[event.dropIndex].plannedEndDate = this.sequenceList[1].plannedEndDate;
    }
    if(event.dropIndex != 0){
      this.sequenceList[event.dropIndex].plannedStartDate = this.sequenceList[event.dragIndex].plannedStartDate;
      this.sequenceList[event.dropIndex].plannedEndDate = this.sequenceList[event.dragIndex].plannedEndDate;
    }
    this.sequenceList.map((x:any,index:any)=>{
       let duration = x.duration
       let durationGapVal = new Date(duration);
       let durationTime = durationGapVal.getMinutes();
       x.plannedEndDate = new Date(x.plannedStartDate)
       x.plannedEndDate.setMinutes(x.plannedEndDate.getMinutes()  + duration);
       if(index > 0){
        let timeGap = this.sequenceList[index-1].timeGap
        let timeGapVal = new Date(timeGap);
        let timeTime = timeGapVal.getMinutes();
        x.plannedStartDate = this.sequenceList[index-1].plannedEndDate
        x.plannedStartDate = new Date(x.plannedStartDate)
        x.plannedStartDate.setMinutes(x.plannedStartDate.getMinutes() + this.sequenceList[index-1].timeGap)
        x.plannedEndDate = new Date(x.plannedStartDate)
        let duration = x.duration
        let durationGapVal = new Date(duration);
        let durationTime = durationGapVal.getMinutes();
        x.plannedEndDate.setMinutes(x.plannedEndDate.getMinutes() + x.duration)
       }
    })
  }

  public dataValidation(rowData){
    let time1:any = new Date(rowData.plannedEndDate)
    let time2:any= new Date(rowData.plannedStartDate)
    let timeGap =  Math.abs(time1 - time2)
    var minutes = Math.floor((timeGap/1000)/60);
    rowData.duration = minutes;
  }

}
