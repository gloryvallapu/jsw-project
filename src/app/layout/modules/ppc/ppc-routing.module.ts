import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderBookComponent } from './components/order-book/order-book.component';
import { PpcComponent } from './ppc.component';
import { CasterScheduleComponent } from './components/caster-schedule/caster-schedule.component';
import { LongProductScheduleComponent } from './components/long-product-schedule/long-product-schedule.component';
import { ReApplicationComponent } from './components/re-application/re-application.component';
import { BilletAcknowledgeComponent } from './components/billet-acknowledge/billet-acknowledge.component';
import { SoModificationComponent } from './components/so-modification/so-modification.component';
import { BilletAttachComponent } from './components/billet-attach/billet-attach.component';
import { POAmendComponent } from './components/po-amend/po-amend.component';
import { SequenceChangeComponent } from './components/sequence-change/sequence-change.component';
import { AuthGuardService } from 'src/app/shared/services/auth-guard.service';
import { moduleIds, ppcScreenIds } from 'src/assets/constants/MENU/menu-list';
import { BatchTransferComponent } from './components/batch-transfer/batch-transfer.component';

const routes: Routes = [
  {
    path: '',
    component: PpcComponent,
    children: [
      // { path: '', pathMatch: 'full', redirectTo: 'orderBook' },
      {
        path: 'orderBook',
        component: OrderBookComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.PPC, screenId: ppcScreenIds.Order_Book.id }
      },
      {
        path: 'casterSchedule',
        component: CasterScheduleComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.PPC, screenId: ppcScreenIds.Caster_Schedule.id }
      },
      {
        path: 'longProductSchedule',
        component: LongProductScheduleComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.PPC, screenId: ppcScreenIds.LP_Schedule.id }
      },
      {
        path: 'reApplication',
        component: ReApplicationComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.PPC, screenId: ppcScreenIds.Re_Application.id }
      },
      {
        path: 'billetAcknowledge',
        component: BilletAcknowledgeComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.PPC, screenId: ppcScreenIds.Batch_Ack.id }
      },
      {
        path: 'soModification',
        component: SoModificationComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.PPC, screenId: ppcScreenIds.SO_Modification.id }
      },
      {
        path: 'billetAttach',
        component: BilletAttachComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.PPC, screenId: ppcScreenIds.Batch_Attach.id }
      },
      {
        path: 'sequenceChange',
        component: SequenceChangeComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.PPC, screenId: ppcScreenIds.Sequence_Change.id }
      },
      {
        path: 'poAmend',
        component: POAmendComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.PPC, screenId: ppcScreenIds.PO_Amend.id }
      },
      {
        path: 'batchTransfer',
        component: BatchTransferComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.PPC,
          screenId: ppcScreenIds.Batch_transfer.id,
        }
      }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})
export class PpcRoutingModule { }
