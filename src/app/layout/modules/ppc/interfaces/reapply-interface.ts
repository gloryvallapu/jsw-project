

export class ReapplyForm{

  public formObj = {

    batchID:'' ,
    customerName:'' ,
    diameter: '',
    grade: '',
    heatNo: '',
    length: '',
    materialclass: '',
    message: '',
    orderId :'',
    processStatus: '',
    product: '',
    productSize: '',
    psn: '',
    salesorder: '',
    thickness: '',
    weight: '',
    width: ''

  }
  public getReapplyForm(){
    return this.formObj;
  }
}


export interface Reapply{

  batchID: string,
    customerName: string,
    diameter: string,
    grade: string,
    heatNo: string,
    length: string,
    lengthValidation:boolean,
    materialclass: string,
    //message: string,
    //processStatus: string,
    product: string,
    productSize: string,
    psn: string,
    salesorder: string,
    thickness: string,
    weight: string,
    width: string,
    materialId: string,
   // materialClassCode: string
}
