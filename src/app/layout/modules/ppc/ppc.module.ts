import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderBookComponent } from './components/order-book/order-book.component';
import { PpcRoutingModule } from './ppc-routing.module';
import { PpcComponent } from './ppc.component';
import { CasterScheduleComponent } from './components/caster-schedule/caster-schedule.component';
import { LongProductScheduleComponent } from './components/long-product-schedule/long-product-schedule.component';
import { ReApplicationComponent } from './components/re-application/re-application.component';
import { BilletAcknowledgeComponent } from './components/billet-acknowledge/billet-acknowledge.component';
import { SoModificationComponent } from './components/so-modification/so-modification.component';
import { BilletAttachComponent } from './components/billet-attach/billet-attach.component';
import { POAmendComponent } from './components/po-amend/po-amend.component';
import { JswCoreModule } from 'projects/jsw-core/src/public-api';
import { PpcEndPoints } from './form-objects/ppc-api-endpoints';
import { PpcService } from './services/ppc.service';
import { SequenceChangeComponent } from './components/sequence-change/sequence-change.component';
import { SharedModule } from 'src/app/shared/shared.module';
import {
  BilletacknowledgementForms,
  CasterScheduleForms,
  SoModificationFomrs,
  ReApplicationScreen, BilletAttachForms, PoamendForms, BatchTransferForms, ReapplicationForms
} from './form-objects/ppc-form-objects';
import { ViewAndInsertOrderbookComponent } from './components/caster-schedule/sub-components/view-and-insert-orderbook/view-and-insert-orderbook.component';
import { OrderBookTableComponent } from './components/sub-components/order-book-table/order-book-table.component';
import { LongProductScheduleForms } from './form-objects/ppc-form-objects';
import { ScheduleListComponent } from './components/caster-schedule/sub-components/schedule-list/schedule-list.component';
import { HeatAndBatchLinesComponent } from './components/caster-schedule/sub-components/heat-and-batch-lines/heat-and-batch-lines.component';
import { AddUnplannedHeatComponent } from './components/caster-schedule/sub-components/add-unplanned-heat/add-unplanned-heat.component';
import { LpScheduleListComponent } from './components/long-product-schedule/subcomponent/lp-schedule-list/lp-schedule-list.component';
import { BatchAttachComponent } from './components/billet-attach/sub-components/batch-attach/batch-attach.component';
import { BatchDetachComponent } from './components/billet-attach/sub-components/batch-detach/batch-detach.component';
import { BatchTransferComponent } from './components/batch-transfer/batch-transfer.component';

@NgModule({
  declarations: [
    PpcComponent,
    OrderBookComponent,
    CasterScheduleComponent,
    LongProductScheduleComponent,
    ReApplicationComponent,
    BilletAcknowledgeComponent,
    SoModificationComponent,
    BilletAttachComponent,
    POAmendComponent,
    SequenceChangeComponent,
    ViewAndInsertOrderbookComponent,
    OrderBookTableComponent,
    ScheduleListComponent,
    HeatAndBatchLinesComponent,
    AddUnplannedHeatComponent,
    LpScheduleListComponent,
    BatchAttachComponent,
    BatchDetachComponent,
    BatchTransferComponent,
  ],
  imports: [
    CommonModule,
    PpcRoutingModule,
    // JswCoreModule,
    SharedModule
  ],
  providers: [
    PpcEndPoints,
    PpcService,
    CasterScheduleForms,
    LongProductScheduleForms,
    BilletacknowledgementForms,
    SoModificationFomrs,
    ReApplicationScreen,
    BilletAttachForms,
    PoamendForms,
    BatchTransferForms,
    ReapplicationForms
  ],
})
export class PpcModule {}
