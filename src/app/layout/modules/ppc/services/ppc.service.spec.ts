import { TestBed } from '@angular/core/testing';

import { PpcService } from './ppc.service';

describe('PpcService', () => {
  let service: PpcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PpcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
