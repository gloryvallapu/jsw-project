import {
  Injectable
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  API_Constants
} from 'src/app/shared/constants/api-constants';
import {
  map
} from 'rxjs/operators';
import {
  AuthService
} from 'src/app/core/services/auth.service';
import {
  PpcEndPoints
} from '../form-objects/ppc-api-endpoints';
import {
  CommonAPIEndPoints
} from 'src/app/shared/constants/common-api-end-points';
import {
  BehaviorSubject
} from 'rxjs';
import { SharedService } from 'src/app/shared/services/shared.service';

@Injectable({
  providedIn: 'root'
})
export class PpcService {
  public baseUrl: string;
  public masterBaseUrl: string;
  public orderList: BehaviorSubject < Array < any >> = new BehaviorSubject([]);
  public getOrderList = this.orderList.asObservable();
  public setOrderList(data: any) {
    this.orderList.next(data);
  }

  public clearAction: BehaviorSubject<Boolean> = new BehaviorSubject(false);
  public getClearAction = this.clearAction.asObservable();
  public setClearAction(data: any) {
    this.clearAction.next(data);
  }

  constructor(public http: HttpClient, private apiURL: API_Constants, public commonEndPoints: CommonAPIEndPoints,
    public ppcEndPoints: PpcEndPoints, public authService: AuthService, public sharedService: SharedService) {
    this.baseUrl = `${this.apiURL.baseUrl}${this.apiURL.ppcGateWay}`;
    this.masterBaseUrl = `${this.apiURL.baseUrl}${this.apiURL.masterGateWay}`;
  }

  // Order Book APIs
  public orderBookDetails() {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.orderBookDetails}`).pipe(map((response: any) => response));
  }

  public downloadOrderBook(fileFormat) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.downloadOrderBook}/${fileFormat}`, { responseType: 'blob' }).pipe(map((response: any) => response));
  //  return this.baseUrl + this.ppcEndPoints.downloadOrderBookPDF;
  }
  // End of Order Book APIs

  // Caster Schedule APIs
  public getCasterValidations() {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getCasterValidations}`).pipe(map((response: any) => response));
  }

  public getCasterSchStatusList() {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getCasterSchStatusList}`).pipe(map((response: any) => response));
  }

  public getNewScheduleDetails(userId:any) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getNewScheduleDetails}/${userId}`).pipe(map((response: any) => response));
  }

  public saveScheduleDetails(data: any) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.saveScheduleDetails}`, data).pipe(map((response: any) => response));
  }

  public getScheduleList(scheduleId: any) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getScheduleList}/${scheduleId}`).pipe(map((response: any) => response));
  }

  public getScheduleIdList() {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getScheduleIdList}`).pipe(map((response: any) => response));
  }

  public getScheduleDetailsById(scheduleId: any) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getScheduleDetailsById}/${scheduleId}`).pipe(map((response: any) => response));
  }

  public getScheduleStatusById(scheduleId: any) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getSchStatusById}/${scheduleId}`).pipe(map((response: any) => response));
  }

  public getGradeGroupsCategory() {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getGradeGroupsCategory}`).pipe(map((response: any) => response));
  }

  public insertOrderBookToCasterSch(orderList, scheduleId, userId) {
    return this.http.post(`${this.baseUrl}${this.ppcEndPoints.insertOrderBookToCasterSch}/${scheduleId}/${userId}`, orderList).pipe(map((response: any) => response));
  }

  public updateCasterScheduleList(schList, scheduleId, userId) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.updateCasterScheduleList}/${scheduleId}/${userId}`, schList).pipe(map((response: any) => response));
  }

  public getSMSActionList(status: any) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getSMSActionList}${status}`).pipe(map((response: any) => response));
  }

  public getFilteredCasterOrderBook(params: any) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.getFilteredCasterOrderBook}`, params).pipe(map((response: any) => response));
  }

  public buildHeat(data, userId, planUnPlanStr) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.buildHeat}/${userId}/${planUnPlanStr}`, data).pipe(map((response: any) => response));
  }

  public getBatchLinesByHeatId(heatId) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getBatchLinesByHeatId}/${heatId}`).pipe(map((response: any) => response));
  }

  public releaseCasterSchedule(schdId, schdStatus, userId, data, seqRemarkVal) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.releaseCasterSch}/schdId/${schdId}/schdStatus/${schdStatus}/userId/${userId}/${seqRemarkVal}`, data, { responseType: 'text'}).pipe(map((response: any) => response));
  }

  public insertUnPlannedOBToCaster(schdId, userId, data) {
    return this.http.post(`${this.baseUrl}${this.ppcEndPoints.insertUnplannedOrderBookToCasterSch}/${schdId}/${userId}`, data).pipe(map((response: any) => response));
    // return this.http.put(`${this.baseUrl}${this.ppcEndPoints.releaseCasterSch}/schdId/${schdId}/schdStatus/${schdStatus}/userId/${userId}`, data, { responseType: 'text'}).pipe(map((response: any) => response));
  }

  public cancelCasterSchedule(schdId, userId) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.cancelCasterSchedule}/${schdId}/${userId}`, {}).pipe(map((response: any) => response));
  }

  public getHeatLinesBySchId(schdId){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getHeatLinesBySchId}/${schdId}`).pipe(map((response: any) => response));
  }

  public saveHeatLines(userId, heatLines){
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.saveHeatLines}/${userId}`, heatLines).pipe(map((response: any) => response));
  }

  public deleteCasterSchOrder(userId, schdId, data){
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.deleteCasterSchOrder}/${userId}/${schdId}`, data, { responseType: 'text'})
    .pipe(map((response: any) => response));
  }

  //newly added abort and release
  public aborteHeatLines(schId,unPlannedId,userId){
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.releaseAbortHeatLines}/${schId}/${unPlannedId}/abort/${userId}`, {},{ responseType: 'text'}).pipe(map((response: any) => response));
    // return this.http.put(`${this.baseUrl}${this.ppcEndPoints.releaseAbortHeatLines}/${schId}/${unPlannedId}/abort/${userId}`, {})
    // .pipe(map((response: any) => response));
  }
  public releaseHeatLines(schId,unPlannedId,userId){
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.releaseAbortHeatLines}/${schId}/${unPlannedId}/release/${userId}`, {},{ responseType: 'text'}).pipe(map((response: any) => response));
    // return this.http.put(`${this.baseUrl}${this.ppcEndPoints.releaseAbortHeatLines}/${schId}/${unPlannedId}/release/${userId}`, {})
    // .pipe(map((response: any) => response));
  }

  // Long-Product Schedule APIs

  public lpSelectedRecords = new BehaviorSubject<Object>([]);
  public getlpSelectedRecords = this.lpSelectedRecords.asObservable();
  public setlpSelectedRecords(data: any) {
    this.lpSelectedRecords.next(data);
  }

  public getLPValidations() {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getLPValidations}`).pipe(map((response: any) => response));
  }

  public deleteLpOrderBooks(userId,schdId,orderDetail) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.deleteLpOrderBooks}/${userId}/${schdId}`,orderDetail).pipe(map((response: any) => response));
  }

  public cancelLPSchedule(scheduleId: any, userId: any, scheduleDetails) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.cancelLPSchedule}/${scheduleId}/${userId}`, scheduleDetails).pipe(map((response: any) => response));
  }

  public saveLpScheduleDetails(data: any) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.updateLPSchedule}`, data).pipe(map((response: any) => response));
  }

  public releaseLPSchedule(scheduleId: any, userId: any, scheduleDetails) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.releaseLPSchedule}/${scheduleId}/${userId}`, scheduleDetails).pipe(map((response: any) => response));
  }

  public getLpScheduleDetailsById(scheduleId: any) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getLpScheduleDetailsById}/${scheduleId}`).pipe(map((response: any) => response));
  }

  public getLPScheduleList(scheduleId: any) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getLPScheduleList}/${scheduleId}`).pipe(map((response: any) => response));
  }

  public lpActionMapping(scheduleStatus) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.lpActionMapping}${scheduleStatus}`).pipe(map((response: any) => response));
  }

  public insertLpOrderBook(orderList, scheduleId, userId) {
    return this.http.post(`${this.baseUrl}${this.ppcEndPoints.insertLpOrderBook}/${scheduleId}/${userId}`, orderList).pipe(map((response: any) => response));
  }

  public insertUnplannedLpOrder(orderList, scheduleId, userId) {
    return this.http.post(`${this.baseUrl}${this.ppcEndPoints.insertUnplannedLpOrder}/${scheduleId}/${userId}`, orderList).pipe(map((response: any) => response));
  }

  public getFilteredOrderBookLpUnplanned(params) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.getFilteredOrderBookLpUnplanned}`, params).pipe(map((response: any) => response));
  }

  public getScheduleDetails(scId) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getLpScheduleDetailsById}/${scId}`, ).pipe(map((response: any) => response));
  }

  public getAddNewLPSchedule(userID) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getAddNewLPSchedule}/${userID}`).pipe(map((response: any) => response));
  }

  public updateLPSchedule(data: any) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.updateLPSchedule}`, data).pipe(map((response: any) => response));
  }

  public getLPScheduleIdList() {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getLPScheduleIdList}`).pipe(map((response: any) => response));
  }

  public lpScheduleStatus() {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.lpScheduleStatus}`).pipe(map((response: any) => response));

  }

  public getlpScheduleListById(schdId) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getlpScheduleListById}/${schdId}`).pipe(map((response: any) => response));
  }

  public updateLPScheduleOBList(schdID, userId, orderList) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.updateLPScheduleOBList}/${schdID}/${userId}`, orderList).pipe(map((response: any) => response));
  }

  public getDensity(productId,sizeCode,schdId,orderSeqNo){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getDensity}/${productId}/${sizeCode}/${schdId}/${orderSeqNo}`).pipe(map((response: any) => response));
  }

  public getMapping(productName){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getMapping}/${productName}`).pipe(map((response: any) => response));
  }
  // End of Long-Product Schedule APIs updateLPSchedule


  // Start of SO Modification APIs updateLPSchedule
  public getLengthValidationFlag() {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getLengthValidationFlag}`,{ responseType: 'text' }).pipe(map((response: any) => response));
  }

  public getEditHostoryForSoDetails(batch) {
    batch.reqDeliveryDate = new Date(batch.reqDeliveryDate)
    batch.soReleaseDate = new Date(batch.soReleaseDate)
    batch.roleId = '3' // need to update after confirmation with ankur and rohit
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.getEditHostoryForSoDetails}`, batch).pipe(map((response: any) => response));
  }

  public getSoModFilters() {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getSoModFilters}`).pipe(map((response: any) => response));
  }

  public getSoBasedOrderBookDetails(soId, lineItem) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getSoBasedOrderBookDetails}/${soId}/${lineItem}`).pipe(map((response: any) => response));
  }

  public getCustomerBasedOrderBookDetails(customertName) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getCustomerBasedOrderBookDetails}/${customertName}`).pipe(map((response: any) => response));
  }
  // End of SO Modification APIs updateLPSchedule


  //Start of Billet  ACK Screen API's
  public updateSOLengthDetails(userId, batch) {
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.updateSOLengthDetails}/${userId}`, batch).pipe(map((response: any) => response));
  }

  public batchAcknowlegdement(userId,batch) {
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.batchAcknowlegdement}/${userId}`, batch,{ headers, responseType: 'text' }).pipe(map((response: any) => response));
  }

  public getAllBilletBatchesInTransit(userId) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getAllBilletBatchesInTransit}/${userId}`).pipe(map((response: any) => response));
  }

  public getAuthorizedWCByUserId(userId) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getAuthorizedWCByUserId}/${this.sharedService.loggedInUserDetails.userId}`).pipe(map((response: any) => response));
  }

  public getBatchesByUnitAndHeatId(userId,filterDetails) {
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getBatchesByUnitAndHeatId}/${filterDetails.workCenter}/${filterDetails.heatNo}/${userId}`).pipe(map((response: any) => response));
  }

  //End of Billet  ACK Screen API's

  //Billet attach
  public getUnitList(){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getLPHeaderScheduleList}/${this.sharedService.loggedInUserDetails.userId}`).pipe(map((response: any) => response));
  }

  public getFilteredLpHdrScheduleList(params){
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.getFilteredLpHdrScheduleList}`, params).pipe(map((response: any) => response));
  }

  public getHeatNos(){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getHeatNos}`).pipe(map((response: any) => response));
  }


//Start of Reapplication API'S

public filteredDetailsOrder(reqBody){
  return this.http.put(`${this.baseUrl}${this.ppcEndPoints.filteredDetailsOrder}`,reqBody).pipe(map((response: any) => response)); 

}
public getFilterInputDetails(){
  return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getFilterInputDetails}`).pipe(map((response: any) => response)); 
}

public salesOrderLineNos(selectedWeight,materialId){
  return this.http.get(`${this.baseUrl}${this.ppcEndPoints.salesOrderLineNos}/${selectedWeight}/${materialId}`).pipe(map((response: any) => response));
}

  public reapplyRequiredDetails(soId,lineNo,materialId){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.reapplyRequiredDetails}/${soId}/${lineNo}/${materialId}`).pipe(map((response: any) => response));
}

  public reapply(soID,linNO,userID,reqBody){
      return this.http.put(`${this.baseUrl}${this.ppcEndPoints.reapply}/${soID}/${linNO}/${userID}`,reqBody).pipe(map((response: any) => response));
}

public reapplyNco(userID,reqBody)
{
  return this.http.put(`${this.baseUrl}${this.ppcEndPoints.reapplyNco}/${userID}`,reqBody,{responseType:'text'}).pipe(map((response: any) => response));
}

public getAllDetailsOrder() {
  return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getAllDetailsOrder}`).pipe(map((response: any) => response));
}

//End of Reapplication API'S

public getStockDetails(schdId, orderSeqNO){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getStockDetails}/${schdId}/${orderSeqNO}`).pipe(map((response: any) => response));
  }

  public attachStockDetails(schdId, orderSeqNO, userId, reqBody){
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.attachStockDetails}/${schdId}/${orderSeqNO}/${userId}`, reqBody, { responseType: 'text'}).pipe(map((response: any) => response));
  }

  public getStockDetailsForUnattach(materialId, grade, size, schId, orderSeqNO){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getStockDetailsForUnattach}/${materialId}/${grade}/${size}/${schId}/${orderSeqNO}`).pipe(map((response: any) => response));
  }

  public unattachStockDetails(userId,schdId, orderSeqNO, reqBody){
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.unattachStockDetails}/${userId}/${schdId}/${orderSeqNO}`, reqBody, { responseType: 'text'}).pipe(map((response: any) => response));
  }
  //End billet Attach


  // Sequence Change Apis

  public getScheduleSequence(workCenter){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getScheduleSequence}/${workCenter}`).pipe(map((response: any) => response));
  };

  public saveSchduleSequence(sequenceList){
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });
    return this.http.post(`${this.baseUrl}${this.ppcEndPoints.saveSchduleSequence}`,sequenceList,{ headers, responseType: 'text' }).pipe(map((response: any) => response));
  }
  // End Sequence Change APi


  // PO Amend APi

  public amendProductionOrderQty(Schedule){
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.amendProductionOrderQty}`,Schedule,{ headers, responseType: 'text' }).pipe(map((response: any) => response));

  }

  public getFilteredPrductOrderQuantity(paginationNo,filterCriteria){
    return this.http.put(`${this.baseUrl}${this.ppcEndPoints.getFilteredPrductOrderQuantity}/${paginationNo}`,filterCriteria).pipe(map((response: any) => response));

  }

  public getProductOrderQuantityList(paginationNo){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getProductOrderQuantityList}/${paginationNo}`).pipe(map((response: any) => response));
  }

  public getDistinctMaterialIdList(){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getDistinctMaterialIdList}`).pipe(map((response: any) => response));
  }

  public getDistinctGradeList(){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.getDistinctGradeList}`).pipe(map((response: any) => response));
  }

  public prodControlStatus(){
    return this.http.get(`${this.baseUrl}${this.ppcEndPoints.prodControlStatus}`).pipe(map((response: any) => response));
  }

//Start Batch Transfer
public getBatchTransferList(wcName, heatNo) {
  return this.http
    .get(
      `${this.baseUrl}${this.ppcEndPoints.getBatchTransferList}/${wcName}/${heatNo}`
    )
    .pipe(map((response: any) => response));
}

public getYardList(wcName) {
  return this.http
    .get(`${this.baseUrl}${this.ppcEndPoints.getYards}/${wcName}`)
    .pipe(map((response: any) => response));
}

public batchTransfer(inTransitId, userId, reqBody) {
  return this.http
    .post(
      `${this.baseUrl}${this.ppcEndPoints.batchTransfer}/${inTransitId}/${userId}`,
      reqBody,
      { responseType: 'text' }
    )
    .pipe(map((response: any) => response));
}


//End Batch Transfer




}
