export class PpcEndPoints{
  // Start of order book Screen API End Points
  public readonly orderBookDetails = '/orderBookDetails';
  public readonly downloadOrderBook = '/downloadOrderBook';
  //End of order book screen API End Points

  //Start of Caster Schedule Screen API End Points
  public readonly getCasterValidations = '/getEnabledValidation';
  public readonly getNewScheduleDetails = '/addNewSchedule';
  public readonly saveScheduleDetails = '/updateCasterSchedule';
  public readonly getCasterSchStatusList = '/smsScheduleStatus';
  public readonly getScheduleList = '/casterScheduleList';
  public readonly getScheduleIdList = '/casterScheduleIDList';
  public readonly getScheduleDetailsById = '/getCasterScheduleById';
  public readonly getSchStatusById = '/getCasterSchdStatusBySchdId';
  public readonly getGradeGroupsCategory = '/casterSchedule/gradeGroupsCategory';
  public readonly insertOrderBookToCasterSch = '/insertOBToCaster';
  public readonly updateCasterScheduleList = '/updateCasterScheduleList';
  public readonly getSMSActionList = '/smsActionMapping?scheduleStatus=';
  public readonly getFilteredCasterOrderBook = '/casterSchedule/getFilteredOrderBook';
  public readonly insertUnplannedOrderBookToCasterSch = '/insertUnPlannedOBToCaster';  //API pending from Rashmi
  public readonly buildHeat = '/heatLines';
  public readonly getHeatLinesBySchId = '/getSmsScheduleHeatLines';
  public readonly getBatchLinesByHeatId = '/heatBatchLines';
  public readonly saveHeatLines = '/updateUnplannedRemark';
  public readonly cancelCasterSchedule = '/cancelCasterSchedule';
  public readonly releaseCasterSch = '/casterSchedule/updateSchduleStatus';
  public readonly deleteCasterSchOrder = '/deleteCasterOrderBook';
  public readonly releaseAbortHeatLines = '/releaseAbortUnplannedHeat';
  //End of order book screen API End Points

  //Start of Long Product Schedule Screen API End Points
  public readonly getLPValidations = '/getLpEnabledValidation';
  public readonly cancelLPSchedule = '/cancelLPSchedule'
  public readonly getAddNewLPSchedule ='/addNewLPSchedule';
  public readonly updateLPSchedule = '/updateLPSchedule';
  public readonly getLPScheduleIdList = '/lpScheduleIDList';
  public readonly getLPScheduleList = '/lpScheduleList';
  public readonly getLpScheduleDetailsById = '/getLPScheduleById';
  public readonly getFilteredOrderBookLpUnplanned = '/lpSchedule/getFilteredOrderBook';
  public readonly insertUnplannedLpOrder = '/insertUnplannedLpOrder';
  public readonly insertLpOrderBook = '/insertLpOrder';
  public readonly lpActionMapping = '/lpActionMapping?scheduleStatus=';
  public readonly lpScheduleStatus = '/lpScheduleStatus';
  public readonly getWCBasedProductSizeList = '/master/getWCBasedProductSizeList';
  public readonly getlpScheduleListById  = '/lpScheduleList';
  public readonly updateLPScheduleOBList = '/updateLPScheduleOBList';
  public readonly releaseLPSchedule = '/releaseLPSchedule';
  public readonly getDensity ='/getDensity'
  public readonly deleteLpOrderBooks = '/deleteLpOrderBooks';
  public readonly getMapping = '/getInputProdByOutputProd'
  //End of Long Product Schedule Screen API End Points

 //Start of Reapplication APIs
 public readonly filteredDetailsOrder = '/filteredDetailsOrder';
 public readonly getFilterInputDetails= '/getFilterInputDetails';
 public readonly getAllDetailsOrder = '/detailsOrder';
 public readonly salesOrderLineNos = '/salesOrderLineNos';
 public readonly reapplyRequiredDetails = '/reapplyRequiredDetails';
 public readonly reapply = '/reapply';
 public readonly reapplyNco = '/reapplyNco';

 //END of Reapplication APIs



  //Start of SO Modification Screen API End Points
  public readonly getLengthValidationFlag = '/getLengthValidationFlag';
  public readonly getSoModFilters = '/getSoModFilters';
  public readonly getSoBasedOrderBookDetails = '/getSoBasedOrderBookDetails';
  //End of SO Modification  Screen API End Points


   //Start of Billet  ACK Screen API End Points
   public readonly getAllBilletBatchesInTransit = '/batch/getAllBatchesInTransit';
   public readonly getAuthorizedWCByUserId = '/batch/getFilterList';
   public readonly getBatchesByUnitAndHeatId = '/batch/getFilteredBatches';
   public readonly batchAcknowlegdement = '/batch/batchAcknowlegdement';
   public readonly getCustomerBasedOrderBookDetails = '/getCustomerBasedOrderBookDetails';
   public readonly updateSOLengthDetails = '/updateSOLengthDetails';
   public readonly getEditHostoryForSoDetails = '/getEditHostoryForSoDetails'
   //End of Billet  ACK Screen API End Points

   //Billet Attach
   public readonly getLPHeaderScheduleList = '/batchAttach/getLPHeaderScheduleList';   //dropdown API
   public readonly getFilteredLpHdrScheduleList = '/batchAttach/getFilteredLPProductionList';  //listing API
   public readonly getHeatNos = '/batchAttach/heatNoList';
   public readonly getStockDetails = '/batchAttach/attachStockDetails';
   public readonly attachStockDetails = '/batchAttach/attachToSchedule';
   public readonly getStockDetailsForUnattach = '/batchAttach/unattachStockDetails';
   public readonly unattachStockDetails = '/batchAttach/unattachFromSchedule';
   //End


  //  Sequence Change Api
  public getScheduleSequence = '/sequenceChange/getScheduleSequence';
  public readonly saveSchduleSequence = '/sequenceChange/saveSchduleSequence'

  // End

  // PO amend API

  public  readonly getProductOrderQuantityList = "/amend/getProductOrderQuantityList"
  public readonly getDistinctMaterialIdList = "/amend/getDistinctMaterialIdList"
  public readonly getDistinctGradeList = "/amend/getDistinctGradeList"
  public readonly prodControlStatus =  "/lovValues/Prod_Control_Status"
  public readonly getFilteredPrductOrderQuantity= "/amend/getFilteredPrductOrderQuantity"
  public readonly amendProductionOrderQty = "/amend/amendProductionOrderQty"



  // Batch transfer screen End Points
  public readonly getBatchTransferList='/batchTransfer/getFilteredBatches';
  public readonly getYards = '/batchTransfer/getYards';
  public readonly batchTransfer='/batchTransfer';
 // End Of Batch Transfer Screen End Points
}

