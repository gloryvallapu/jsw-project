import { FieldType } from 'src/assets/enums/common-enum';

export class CasterScheduleForms {
  public scheduleHeaderForm: Object = {
    formName: 'scheduleHeaderForm',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdID',
        label: 'Schedule Id',
        placeHolder: 'Sales Order',
        value: null,
        name: 'scheduleId',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.text, //'text',
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'casterWC',
        label: 'Schedule Unit',
        placeHolder: 'Schedule Unit',
        value: null,
        name: 'casterWC',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'productId',
        label: 'Product Type',
        placeHolder: 'Product Type',
        value: null,
        name: 'productId',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'sizeCode',
        label: 'Product Size',
        placeHolder: 'Product Size',
        value: null,
        name: 'sizeCode',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdDate',
        label: 'Schedule Date',
        placeHolder: 'Schedule Date',
        value: null,
        name: 'scheduleDate',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.calendar, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'planStartDate',
        label: 'Planned Start Date',
        placeHolder: 'Planned Start Date',
        value: null,
        name: 'plannedStartDate',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.calendar, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'planEndDate',
        label: 'Planned End Date',
        placeHolder: 'Planned End Date',
        value: null,
        name: 'plannedEndDate',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.calendar, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'actualStartDate',
        label: 'Actual Start Date',
        placeHolder: 'Actual Start Date',
        value: null,
        name: 'actualStartDate',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.calendar, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'actualEndDate',
        label: 'Actual End Date',
        placeHolder: 'Actual End Date',
        value: null,
        name: 'actualEndDate',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.calendar, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'setupDuration',
        label: 'Time Gap',
        placeHolder: 'Time Gap',
        value: null,
        name: 'setupDuration',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.number, //'text',
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdStatus',
        label: 'Select Status',
        placeHolder: 'Select Status',
        value: null,
        name: 'status',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.dropdown,
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'incompleteSeqRemark',
        label: 'Sequence Remark',
        placeHolder: '',
        value: null,
        name: 'incompleteSeqRemark',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.text, //'text',
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
    ]
  };

  public viewAndInsertFilterForm: object = {
    formName: 'viewAndInsertFilterForm',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'scheduleId',
        label: 'Schedule Id',
        placeHolder: '',
        value: null,
        name: 'scheduleId',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.text, //'text',
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'productName',
        label: 'Product Type',
        placeHolder: '',
        value: null,
        name: 'productName',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.text, //'text',
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'productSize',
        label: 'Product Size',
        placeHolder: '',
        value: null,
        name: 'productSize',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.text, //'text',
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'gradeGroup',
        label: 'Grade Group',
        placeHolder: '',
        value: null,
        name: 'gradeGroup',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'gradeCategory',
        label: 'Grade Category',
        placeHolder: '',
        value: null,
        name: 'gradeCategory',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      }
    ]
  }
}
export class LongProductScheduleForms {
  public addUnplannedScheduleForm:object ={
    formName: 'Add unplanned heat',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'scheduleId',
        label: 'Select Schedule Id',
        placeHolder: 'Schedule Id',
        value: null,
        name: 'scheduleId',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.text, //'text',
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'grade',
        label: 'Grade',
        placeHolder: 'Grade',
        value: null,
        name: 'grade',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true,
        list: []
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'productId',
        label: 'Product Type',
        placeHolder: 'Product Type',
        value: null,
        name: 'productId',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'productSize',
        label: 'Product Size',
        placeHolder: 'Product Size',
        value: null,
        name: 'producSize',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'orderNo',
        label: 'Order No',
        placeHolder: 'Order No',
        value: null,
        name: 'orderNo',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.number, //'dropdown',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'length',
        label: 'Length',
        placeHolder: 'Length',
        value: null,
        name: 'Length',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.number, //'dropdown',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      }
    ]
  }

  public scheduleHeaderForm: Object = {
    formName: 'ScheduleHeaderForm',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdID',
        label: 'Schedule Id',
        placeHolder: 'Sales Order',
        value: null,
        name: 'scheduleId',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.text, //'text',
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdWC',
        label: 'Schedule Unit',
        placeHolder: 'Schedule Unit',
        value: null,
        name: 'schdWC',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'productId',
        label: 'Product Type',
        placeHolder: 'Product Type',
        value: null,
        name: 'productId',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'sizeCode',
        label: 'Product Size',
        placeHolder: 'Product Size',
        value: null,
        name: 'sizeCode',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdDate',
        label: 'Schedule Date',
        placeHolder: 'Schedule Date',
        value: null,
        name: 'scheduleDate',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.calendar, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'planStartDate',
        label: 'Planned Start Date',
        placeHolder: 'Planned Start Date',
        value: null,
        name: 'plannedStartDate',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.calendar, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'planEndDate',
        label: 'Planned End Date',
        placeHolder: 'Planned End Date',
        value: null,
        name: 'plannedEndDate',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.calendar, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'actualStartDate',
        label: 'Actual Start Date',
        placeHolder: 'Actual Start Date',
        value: null,
        name: 'actualStartDate',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.calendar, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'actualEndDate',
        label: 'Actual End Date',
        placeHolder: 'Actual End Date',
        value: null,
        name: 'actualEndDate',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.calendar, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'duration',
        label: 'Time Gap',
        placeHolder: 'Time Gap',
        value: null,
        name: 'timeGap',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.number, //'text',
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdStatus',
        label: 'Select Status',
        placeHolder: 'Select Status',
        value: null,
        name: 'status',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.dropdown,
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      }
    ]
  };
}


export class ReApplicationScreen{
  public allowAlphaNumeric : RegExp = /^[a-zA-Z0-9]*$/;
  public filterCriteria: Object = {
    formName: 'Filter Criteria',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'productType',
        label: 'Product Type',
        placeHolder: 'Product Type',
        value: null,
        name: 'Product Type',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list:[],
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'heatNo',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list:[],
        changeEvent: null,
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'jswGrade',
        label: 'JSW Grade',
        placeHolder: 'JSW Grade',
        value: null,
        name: 'jswGrade',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'salesOrder',
        label: 'Sales Order',
        placeHolder: 'Sales Order',
        value: null,
        name: 'salesOrder',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.number, //'dropdown',
        pKeyFilter: 'onlyNumber',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'customerName',
        label: 'Customer Name',
        placeHolder: 'Customer Name',
        value: null,
        name: 'Customer Name',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.textWithFilter, //'text',
        pKeyFilter:this.allowAlphaNumeric,
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'productSize',
        label: 'Product Size',
        placeHolder: 'Product Size',
        value: null,
        name: 'productSize',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.number, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'length',
        label: 'Length',
        placeHolder: 'Length',
        value: null,
        name: 'length',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.number, //'calendar',
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      }
    ]
  };
  public reApply: Object = {
    formName: 'ReApply',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'salesOrder',
        label: 'Sales Order',
        placeHolder: 'Sales Order',
        value: null,
        name: 'salesOrder',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown,
        list:[],
        changeEvent: 'dropdownChange',
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'lineNo',
        label: 'Line No ',
        placeHolder: 'Line No',
        value: null,
        name: 'lineNo',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown,
        list:[],
        changeEvent: 'dropdownChange',
        maxLength: '',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'promiseDate',
        label: 'Promise Date',
        placeHolder: 'Promise Date',
        value: null,
        name: 'promiseDate',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.text, //'dropdown',
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'soGrade',
        label: 'SO Grade',
        placeHolder: 'SO Grade',
        value: null,
        name: 'soGrade',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.text, //'dropdown',
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'dimension',
        label: 'Size',
        placeHolder: 'Size',
        value: null,
        name: 'dimension',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.text, //'dropdown',
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'customerName',
        label: 'Customer Name',
        placeHolder: 'Customer Name',
        value: null,
        name: 'customerName',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.text, //'dropdown',
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'btc',
        label: 'BTC',
        placeHolder: 'BTC(Tons)',
        value: null,
        name: 'btc',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.text, //'dropdown',
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'length',
        label: 'Length',
        placeHolder: 'Length',
        value: null,
        name: 'length',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.text, //'dropdown',
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
    ]
  };
}



export class SoModificationFomrs{​​​​​​​​
public filterCriteria:object ={​​​​​​​​
formName:'Filter Criteria',
formFields: [
      {​​​​​​​​
isError:false,
errorMsg:'',
ref_key:'SaleOrderNo',
label:'Sales Order No',
placeHolder:'Sales Order No',
value:null,
name:'saleOrderNo',
isRequired:false,
disabled:false,
fieldType:FieldType.dropdown, //'text',
changeEvent:'dropdownChange',
maxLength:'',
colSize:'col-12 col-sm-4',
isVisible:true,
list: []
      }​​​​​​​​,
      {​​​​​​​​
isError:false,
errorMsg:'',
ref_key:'lineItme',
label:'Line Item',
placeHolder:'Line Item',
value:null,
name:'lineItem',
isRequired:false,
disabled:false,
fieldType:FieldType.dropdown, //'text',
changeEvent:null,
maxLength:'',
colSize:'col-12 col-sm-4',
isVisible:true,
list: []
      }​​​​​​​​,
      {​​​​​​​​
isError:false,
errorMsg:'',
ref_key:'customerName',
label:'Customer Name',
placeHolder:'Customer Name',
value:null,
name:'customerName',
isRequired:false,
disabled:false,
fieldType:FieldType.dropdown, //'text',
changeEvent:null,
maxLength:'',
colSize:'col-12 col-sm-4',
isVisible:true,
list: []
      }​​​​​​​​,

    ]

  }​​​​​​​​

}​​​​​​​​

export class BilletacknowledgementForms{​​​​​​​​
public filterCriteria:object ={​​​​​​​​
formName:'Filter Criteria',
formFields: [
      {​​​​​​​​
isError:false,
errorMsg:'',
ref_key:'workCenter',
label:'Select Yard',
placeHolder:'Select Yard',
value:null,
name:'selectUnit',
isRequired:true,
disabled:false,
fieldType:FieldType.dropdown, //'text',
changeEvent:'dropdownChange',
maxLength:'',
colSize:'col-6',
isVisible:true,
list: []
      }​​​​​​​​,
      {​​​​​​​​
isError:false,
errorMsg:'',
ref_key:'heatNo',
label:'Heat No',
placeHolder:'Enter Heat No',
value:null,
name:'heatNo',
isRequired:true,
disabled:false,
fieldType:FieldType.dropdown, //'text',
changeEvent:null,
maxLength:'',
colSize:'col-6',
isVisible:true
      }​​​​​​​​,

    ]

  }​​​​​​​​

}​​​​​​​​


export class BatchTransferForms {
  public batchTransferFilterForm: Object = {
    formName: 'FilterForm',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Yard',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdID',
        label: 'Schedule No',
        placeHolder: '',
        value: null,
        name: 'schdID',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      }
    ]
  };

  public batchTransfer: object = {
    formName: 'Batch Transfer Production',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Yard',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'heatNo',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: null,
        colSize: 'col-6',
        isVisible: true,
      },
    ]
  };
  public batchTransferTo: object = {
    formName: 'Batch Transfer To',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'yardName',
        label: 'Transfer To',
        placeHolder: 'Transfer To',
        value: null,
        name: 'yardName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-12',
        isVisible: true
      }
    ]
  };
}




//Billet Attach Form object
export class BilletAttachForms {

  public billetFilterCriteriaForms: object ={
    formName: 'billetFilterCriteriaForms',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Select Unit',
        placeHolder: 'Select Unit',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdID',
        label: 'Schedule Id',
        placeHolder: 'Schedule Id',
        value: null,
        name: 'schdID',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },{
        isError: false,
        errorMsg: '',
        ref_key: 'productName',
        label: 'Product Type',
        placeHolder: 'Product Type',
        value: null,
        name: 'productName',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.text,
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },{
        isError: false,
        errorMsg: '',
        ref_key: 'sizeCode',
        label: 'Product Size',
        placeHolder: 'Product Size',
        value: null,
        name: 'sizeCode',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.text,
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },{
        isError: false,
        errorMsg: '',
        ref_key: 'schdStatus',
        label: 'Status',
        placeHolder: 'Status',
        value: null,
        name: 'schdStatus',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.text,
        list: [],
        changeEvent: null,
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      // {
      //   isError: false,
      //   errorMsg: '',
      //   ref_key: 'productGrade',
      //   label: 'Product Grade',
      //   placeHolder: 'Product Grade',
      //   value: null,
      //   name: 'productGrade',
      //   isRequired: true,
      //   disabled: true,
      //   fieldType: FieldType.text,
      //   list: [],
      //   changeEvent: null,
      //   colSize: 'col-2',
      //   isVisible: true
      // }

    ]
  }

}


// PO ammend Form object
export class PoamendForms{
  public poAmendFilterCriteriaForms: object ={
    formName: 'Filter Criteria',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'salseOrder',
        label: 'Sales Order',
        placeHolder: 'Sales Order',
        value: null,
        name: 'salseOrder',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.text,
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'fromDate',
        label: 'From Date',
        placeHolder: 'fromDate',
        value: null,
        name: 'fromDate',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.calendar,
        list: [],
        changeEvent:'startDate',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'toDate',
        label: 'To Date',
        placeHolder: 'toDate',
        value: null,
        name: 'toDate',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.calendar,
        changeEvent:'endDate',
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'gradeGrp',
        label: 'Grade Group',
        placeHolder: 'gradeGrp',
        value: null,
        name: 'gradeGrp',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown,
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'MID',
        label: 'Material ID',
        placeHolder: 'MID',
        value: null,
        name: 'MID',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown,
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
        isVisible: true
      },


    ]
  }

}



//Reapplication Form Object
export class ReapplicationForms{
  public ReapplicationFilterCriteriaForms: object ={
    formName: 'Filter Criteria',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'soId',
        label: 'SO Id',
        placeHolder: 'soId',
        value: null,
        name: 'soId',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: 'dropdownChange',
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdId',
        label: 'Schedule Id',
        placeHolder: 'schdId',
        value: null,
        name: 'schdId',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown,
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },

      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatId',
        label: 'Heat Id',
        placeHolder: 'heatId',
        value: null,
        name: 'heatId',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: 'dropdownChange',
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'batchId',
        label: 'Batch Id',
        placeHolder: 'batchId',
        value: null,
        name: 'batchId',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown,
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },


    ]
  }

}
