import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { MasterEndPoints } from '../form-objects/master-api-endpoints';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MasterService {
  public baseUrl: string;
  public gradeName: any;
  public productsIds: any;
  public psnNo: any;

  constructor(public http: HttpClient, private apiURL: API_Constants, public masterEndPoints: MasterEndPoints, public authService: AuthService) {
    this.baseUrl = `${this.apiURL.baseUrl}${this.apiURL.masterGateWay}`;
  }


  public dbGradeProps = new BehaviorSubject<Object>({});
  public getdbGradeProps = this.dbGradeProps.asObservable();
  public setdbGradeProps(data: any) {
    this.dbGradeProps.next(data);
  }


  //Material Master API's
  // public getLovs(lovCode: any){
  //   return this.http.get(`${this.baseUrl}${this.masterEndPoints.getLovValues}/${lovCode}`).pipe(map((response: any) => response));
  // }

  public getMaterials(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getAllMaterials}`).pipe(map((response: any) => response));
  }

  public createMaterial(data){
    return this.http.post(`${this.baseUrl}${this.masterEndPoints.addNewMaterial}`, data).pipe(map((response: any) => response));
  }

  public updateMaterial(data){
    return this.http.put(`${this.baseUrl}${this.masterEndPoints.updateMaterial}`, data).pipe(map((response: any) => response));
  }
  //End of Material Master API's

  //Work Center Master API's
  public getWorkCenterList(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getWorkCenterList}`).pipe(map((response: any) => response));
  }

  public getWCTypeList(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getWCTypeList}`).pipe(map((response: any) => response));
  }

  public createWorkCenter(data){
    return this.http.post(`${this.baseUrl}${this.masterEndPoints.addNewWorkCenter}`, data).pipe(map((response: any) => response));
  }

  public updateWorkCenter(data){
    return this.http.put(`${this.baseUrl}${this.masterEndPoints.updateWorkCenter}`, data).pipe(map((response: any) => response));
  }
  //End of Work Center Master API's

  //Size master
  public getSizeList(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getAllSizes}`).pipe(map((response: any) => response));
  }

  public createSize(data){
    return this.http.post(`${this.baseUrl}${this.masterEndPoints.addNewProductSize}`, data).pipe(map((response: any) => response));
  }

  public updateSize(data){
    return this.http.put(`${this.baseUrl}${this.masterEndPoints.updateSize}`, data).pipe(map((response: any) => response));
  }
  //End of Size Master APIs

  //WC, Product & Size Mapping
  public getMappingList(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getMappingList}`).pipe(map((response: any) => response));
  }

  public getProductSizeList(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getProductSizes}`).pipe(map((response: any) => response));
  }

  public createMapping(data){
    return this.http.post(`${this.baseUrl}${this.masterEndPoints.addNewMapping}`, data).pipe(map((response: any) => response));
  }

  public updateMapping(data){
    return this.http.put(`${this.baseUrl}${this.masterEndPoints.updateMapping}`, data).pipe(map((response: any) => response));
  }
  //End of WC, Product & Size Mapping

  // Grade Master API's
  public getAllGrades(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getAllGrades}`).pipe(map((response: any) => response));
  }

  public getGradeGroupsCategory(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getGradeGroupsCategory}`).pipe(map((response: any) => response));
  }

  public getAllProductIds(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getAllProductIds}`).pipe(map((response: any) => response));
  }

  public createGrade(gradeDetails){
    return this.http.post(`${this.baseUrl}${this.masterEndPoints.createGrade}`,gradeDetails).pipe(map((response: any) => response));
  }

  public updateGrade(gradeDetails){
    return this.http.put(`${this.baseUrl}${this.masterEndPoints.updateGrade}`,gradeDetails).pipe(map((response: any) => response));
  }

  public deleteGrade(gradeName,userID){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.deleteGrade}/${gradeName}/${userID}`).pipe(map((response: any) => response));
  }


  public updatePropertyValues(props){
    return this.http.put(`${this.baseUrl}${this.masterEndPoints.updatePropertyValues}`,props).pipe(map((response: any) => response));

  }


  public getGradeProperties(gradeDetails){
    return this.http.put(`${this.baseUrl}${this.masterEndPoints.getGradeProps}`,gradeDetails).pipe(map((response: any) => response));
  }

  // End of Grade Master API's


  // start of Property Master API's

  public getAllProperties(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getAllProperties}`).pipe(map((response: any) => response));
  }


  public updateProprety(propName){
    return this.http.put(`${this.baseUrl}${this.masterEndPoints.updateProperty}`,propName).pipe(map((response: any) => response));
  }
  public createProperty(propName){
    return this.http.post(`${this.baseUrl}${this.masterEndPoints.createProperty}`,propName).pipe(map((response: any) => response));
  }

  public deleteProperty(propName:any){
    return this.http.put(`${this.baseUrl}${this.masterEndPoints.deleteProperty}/${propName}`,{ responseType: 'text'}).pipe(map((response: any) => response));
  }
  public getPropertyType( ){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getPropertyType}`).pipe(map((response: any) => response));
  }
  public getAllDistinctLOV( ){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getAlldistinctLOV}`).pipe(map((response: any) => response));
  }
  public getPropertySortSequance( ){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getPropertySortSq}`).pipe(map((response: any) => response));
  }
  //End of property Master API's


  // start of Gradewise density Master API's
  public getAllGradeWiseDensity(){

    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getAllGradeWiseDensity}`).pipe(map((response: any) => response));
  }

  public getWCandPrdDesc(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getWCandPrdDesc}`).pipe(map((response: any) => response));
  }

  public getWCGradeBasedProductSizeList(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getWCGradeBasedProductSizeList}`).pipe(map((response: any) => response));
  }

  public getFilteredGradeDensity(productDesc: any, wcName: any){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getFilteredGradeDensity}/${productDesc}/${wcName}`).pipe(map((response: any) => response));
  }

  public createGradeWiseDensity(wcName)
  {
    return this.http.post(`${this.baseUrl}${this.masterEndPoints.createGradeWiseDensity}`,wcName).pipe(map((response: any) => response));
  }

  public updateGradeWiseDensity(wcName)
  {
    return this.http.put(`${this.baseUrl}${this.masterEndPoints.updateGradeWiseDensity}`,wcName).pipe(map((response: any) => response));
  }

  public getallgrade(){
    // return this.http.get(`${this.baseUrl}${this.masterEndPoints.getallgrade}`).pipe(map((response: any) => response));
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getAllGrades}`).pipe(map((response: any) => response));
  }
//End of Gradewise density Master API's


  // start of LOV Master API's
  public getLOVFilterDetails(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.FilterLOVdata}`).pipe(map((response: any) => response));
  }

  public getFilteredLOVMasterData(reqdetails){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getLOVMasterList}/${reqdetails.lovId}/${reqdetails.masterLovValueId}`).pipe(map((response: any) => response));
  }

  public getDependentLOV(){
    return this.http.get(`${this.baseUrl}${this.masterEndPoints.getdependentLOVList}`).pipe(map((response: any) => response));
  }

  public updateLOVmastervalues(lovdetails){
    return this.http.post(`${this.baseUrl}${this.masterEndPoints.updateLovMasterValues}`,lovdetails ,{ responseType: 'text'}).pipe(map((response: any) => response));
  }
  public updateLOVmasterHeadervalues(lovheaderdetails){
    return this.http.post(`${this.baseUrl}${this.masterEndPoints.updateLovMasterHeaderValues}`,lovheaderdetails ,{ responseType: 'text'}).pipe(map((response: any) => response));
  }
  public updateLOVmasterMappingDetails(mappingDetails){
    return this.http.post(`${this.baseUrl}${this.masterEndPoints.updateLovMasterMappingDetails}`,mappingDetails ,{ responseType: 'text'}).pipe(map((response: any) => response));
  }
  // End of LOV Master API's

    // start of Yard Master API's
    public getallyardlist(){
      return this.http.get(`${this.baseUrl}${this.masterEndPoints.getallyardlist}`).pipe(map((response: any) => response));
    }
    public createYardMaster(userId,reqBody) {
      return this.http.post(`${this.baseUrl}${this.masterEndPoints.createYardMaster}/${userId}`,reqBody,{ responseType: 'text' })
        .pipe(map((response: any) => response));
    }
    public updateYardMaster(userId,reqBody){
      return this.http.put(`${this.baseUrl}${this.masterEndPoints.updateYardMaster}/${userId}`,reqBody,{ responseType: 'text' })
        .pipe(map((response: any) => response));
    }
    // End of Yard Master API's
}



