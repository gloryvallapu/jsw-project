import { Component, OnInit, ViewChild } from '@angular/core';
import { LOVMasterForms } from '../../form-objects/master-form-objects';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswFormComponent } from 'jsw-core';
import { JswCoreService } from 'jsw-core';
import { MasterService } from '../../services/master.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { Table } from 'primeng/table';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-lov-master',
  templateUrl: './lov-master.component.html',
  styleUrls: ['./lov-master.component.css']
})
export class LOVMasterComponent implements OnInit {
  public formObject: any;
  public AddformObject: any;
  public tableType: any;
  public viewType = ComponentType;
  public columns1: ITableHeader[] = [];
  public dependentcolumns: ITableHeader[] = [];
  public columnType = ColumnType;
  public Filteredlovlist:any;
  public unModifiedlovlist: any = [];
  public mappinglist:any=[];
  public statuslist:any=[];
  public selectedLovRecord: any[];
  public shortCodeLovlist: any[];
  public masterValues:any=[];
  public status:string;
  public isnewAdd:boolean=false;
  public isdependent:any=[
    {
      displayName:'Yes',
      modelValue:'Active'
    },
    {
      displayName:'No',
      modelValue:'Inactive'
    },
  ];
  public requestBody = {
    "shortCode": null,
    "masterValue":null
  }
  public reqtBody = {
    "lovId": null,
    "masterLovValueId":null
  }
  public saverequestBody = {
    "shortCode": null,
    "desc": null,
    "status": null,
    "lovId": null,
    "isDependent": null,
    "depLovId":null,
    "userId": null
  }
  public display: boolean=false;
  @ViewChild('dt') table: Table;
  public disableAddBtn: boolean = true;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isLOVAddBtn: boolean = false;
  public isSaveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isRetrieveBtn: boolean = false;
  public isAddvalueBtn: boolean = false;
  public isSaveMappingBtn: boolean = false;
  public isFilterSection: boolean = false;
  public IsValueSection: boolean = false;
  public isDependentSection: boolean = false;
  public clonedProducts: { [s: string]: any; } = {};
  public formRef: NgForm;
  public addFormRef: NgForm;

  //22-11
  public userDetails = this.sharedService.getLoggedInUserDetails();

  constructor( public lovMasterForms: LOVMasterForms,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public masterService: MasterService,
    public commonService: CommonApiService,
    public authService: AuthService
    ) {
      this.columns1 = [
        {
          field: 'lovValueId',
          header: 'Value Id',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'valueShortCode',
          header: 'Short Code',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'valueDescription',
          header: 'Value Description',
          columnType: ColumnType.textField,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'codeValue',
          header: 'Code Value',
          columnType: ColumnType.textField,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'activeStatusYN',
          header: 'Status',
          columnType: ColumnType.dropdown,
          width: '150px',
          sortFieldName: '',
          list: [{
            modelValue: 'N',
            displayName: 'InActive'
          },
          {
            modelValue: 'Y',
            displayName: 'Active'
          }]
        },
        {
          field: '',
          header: 'Action',
          columnType: '',
          width: '150px',
          sortFieldName: '',
        },
      ];

      this.dependentcolumns=[
        {
          field: 'depLovDesc',
          header: 'Description',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'depMappingStatus',
          header: 'Mapping Status',
          columnType: ColumnType.dropdown,
          width: '120px',
          sortFieldName: '',
          list: [{
            modelValue: 'N',
            displayName: 'InActive'
          },
          {
            modelValue: 'Y',
            displayName: 'Active'
          }]
        },
        {
          field: 'activeMaster',
          header: 'ActiveMasterList',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
      ]
    }

  ngOnInit(): void {
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.lovMasterForms.filterLov));
    this.AddformObject = JSON.parse(JSON.stringify(this.lovMasterForms.addLov));
    this.getLovs(LovCodes.status, 'status',this.isnewAdd);
    this.getscreenAccess();
    this.getIsdependent(this.isnewAdd);
    this.getFilterCriteriaForLOV();
    this.getdependentLOVlist(this.isnewAdd);
  }

 getscreenAccess()
 {
  let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Master)[0];
  this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.masterMgmtScreenIds.LOV_Master.id)[0];
  this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.LOVMasterSectionIds.Filter_Criteria.buttons.savelov, menuConstants.LOVMasterSectionIds.Filter_Criteria.id, this.screenStructure);
  this.isLOVAddBtn = this.sharedService.isButtonVisible(true, menuConstants.LOVMasterSectionIds.Filter_Criteria.buttons.addlov, menuConstants.LOVMasterSectionIds.Filter_Criteria.id, this.screenStructure);
  this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.LOVMasterSectionIds.Filter_Criteria.buttons.reset, menuConstants.LOVMasterSectionIds.Filter_Criteria.id, this.screenStructure);
  this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.LOVMasterSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.LOVMasterSectionIds.Filter_Criteria.id, this.screenStructure);
  this.isAddvalueBtn = this.sharedService.isButtonVisible(true, menuConstants.LOVMasterSectionIds.ValueList.buttons.addvalue, menuConstants.LOVMasterSectionIds.ValueList.id, this.screenStructure);
  this.isSaveMappingBtn = this.sharedService.isButtonVisible(true, menuConstants.LOVMasterSectionIds.DepdendentList.buttons.savemapping, menuConstants.LOVMasterSectionIds.DepdendentList.id, this.screenStructure);
  this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.LOVMasterSectionIds.Filter_Criteria.id, this.screenStructure);
  this.IsValueSection = this.sharedService.isSectionVisible(menuConstants.LOVMasterSectionIds.ValueList.id, this.screenStructure);
  this.isDependentSection = this.sharedService.isSectionVisible(menuConstants.LOVMasterSectionIds.DepdendentList.id, this.screenStructure);

 }

  getFilterCriteriaForLOV()
  {
   this.masterService.getLOVFilterDetails().subscribe((data) => {
     this.formObject.formFields.filter(
       (obj: any) => obj.ref_key == 'shortCode'
     )[0].list = data.map((x: any) => {
       return {
         displayName: x.shortCode,
         modelValue: x.lovId,
         shortcodedata:x.shortCodeData,
         masterValue:x.masterValue
       }
     });
   });
  }
  dropdownChangeEvent(event)
  {
  if (event.value !='' && event.value != null && event.ref_key=='status') {
      this.status=event.value;
   }
    else
    {
      let lovlist = this.formObject.formFields.filter(ele => ele.ref_key == 'shortCode')[0].list.filter((x: any) => x.modelValue == event.value)[0]
      if(lovlist.shortcodedata !=null)
      {
        this.sharedService.mapResponseToFormObj(this.formObject.formFields, lovlist.shortcodedata);
        this.formObject.formFields.filter(ele => ele.ref_key == 'masterValue')[0].list = lovlist.masterValue.map((x: any) => {
          return {
            displayName: x.valueDescription,
            modelValue: x.lovValueId
          }
        });
    }
  }
  }
  ShowDialog()
  {
    //this.jswComponent.resetForm(this.formObject);
    this.display = true;
    this.isnewAdd=true;
    this.getLovs(LovCodes.status, 'status',this.isnewAdd);
    this.getIsdependent(this.isnewAdd)
  }
  loaddepndentlist(event)
  {
    let dendentLov = this.AddformObject.formFields.filter((ele) => ele.ref_key == 'depLovId')[0];
    if (event.value !='' && event.value != null && event.value=='Active')
    {
      this.AddformObject.formFields.filter(
        (ele: any) => ele.ref_key == 'depLovId'
      )[0].isVisible = true;
      this.AddformObject.formFields.filter(
        (ele: any) => ele.ref_key == 'depLovId'
      )[0].isRequired = true;
      this.getdependentLOVlist(true);
    }
      else
    {
      this.AddformObject.formFields.filter(
        (ele: any) => ele.ref_key == 'depLovId'
      )[0].isVisible = false;
      this.AddformObject.formFields.filter(
        (ele: any) => ele.ref_key == 'depLovId'
      )[0].isRequired = false;
    }
      //this.AddformObject.formFields.filter((obj: any) => obj.ref_key == 'depLovId')[0].list=[];
  }
  // API call to get dropdown values
  public getLovs(lovCode: any, refKey: any, isnewAdd:boolean){
  this.commonService.getLovs(lovCode).subscribe(
      data => {
      this.statuslist=data;
      if(isnewAdd==true)
      {
      this.AddformObject.formFields.filter((obj: any) => obj.ref_key == refKey)[0].list = data.map((x:any) => {
          return {
            displayName: x.valueDescription, modelValue: x.valueShortCode }
        })
      }
      else
      {
      this.formObject.formFields.filter((obj: any) => obj.ref_key == refKey)[0].list = data.map((x:any) => {
          return { displayName: x.valueDescription, modelValue: x.valueShortCode }
        })
      }
    })
}
  public getIsdependent(isnewAdd:boolean)
  {
   if(isnewAdd==true)
   {
    this.AddformObject.formFields.filter((obj: any) => obj.ref_key == 'isDependent')[0].list = this.isdependent.map((x: any) => {
      return {
        displayName: x.displayName,
        modelValue: x.modelValue,
      }
    });
   }
   else
   {
    this.formObject.formFields.filter((obj: any) => obj.ref_key == 'isDependent')[0].list = this.isdependent.map((x: any) => {
      return {
        displayName: x.displayName,
        modelValue: x.modelValue,
      }
    });
   }
  }

  public getdependentLOVlist(isnewAdd:boolean)
  {
    if(isnewAdd==true)
    {
      this.masterService.getDependentLOV().subscribe(
        data => {
          this.AddformObject.formFields.filter((obj: any) => obj.ref_key == 'depLovId')[0].list = data.map((x:any) => {
            return { displayName: x.depDesc, modelValue: x.depLovId }
          })
        }
      )
    }
    else
    {
      this.masterService.getDependentLOV().subscribe(
        data => {
          this.formObject.formFields.filter((obj: any) => obj.ref_key == 'depLovId')[0].list = data.map((x:any) => {
            return { displayName: x.depDesc, modelValue: x.depLovId }
          })
        }
      )
    }
  }
  public resetFilter() {
    //this.jswComponent.resetForm(this.formObject);
    this.Filteredlovlist =[];
    this.mappinglist=[];
    this.getFilterCriteriaForLOV();
  }
  public FilterData(): any {
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.requestBody);
      this.reqtBody = {
        "lovId": (formObj.shortCode!="")?formObj.shortCode:null,
        "masterLovValueId":(formObj.masterValue!="")?formObj.masterValue:null,
      }
      this.getlistofLovmaterdata();
    }
  }
  getlistofLovmaterdata()
  {
    this.mappinglist=[];
    this.masterService.getFilteredLOVMasterData(this.reqtBody).subscribe((Response) => {
      this.disableAddBtn = false;
      Response.map((rowData: any) => {
        return rowData;
      });
      this.unModifiedlovlist = JSON.parse(JSON.stringify(Response));
      this.Filteredlovlist=Response;
      Response.forEach(element => {
        if(element.mappingDetails !=null)
          this.mappinglist.push(element.mappingDetails);
    });
  });
  }

public updateLOVMasterHeaderdata(): any {
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.saverequestBody);
      if(formObj.shortCode!="")
      {
        this.saverequestBody = {
          "shortCode": (formObj.shortCode!="")?formObj.shortCode:null,
          "desc": (formObj.desc!="")?formObj.desc:null,
          "status": (formObj.status!="")?formObj.status:null,
          "lovId": (formObj.shortCode!="")?formObj.shortCode:null,
          "isDependent": (formObj.isDependent!="")?formObj.isDependent:null,
          "depLovId":(formObj.depLovId!="")?formObj.depLovId:null,
          "userId": this.sharedService.loggedInUserDetails.userId
        }
      }
      
    this.masterService.updateLOVmasterHeadervalues(this.saverequestBody).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `${Response}`,
        }
      );
    })
    this.Filteredlovlist =[];
    this.mappinglist=[];
    this.getFilterCriteriaForLOV();
  }
}
public addLOVMasterHeaderdata(): any {
  this.jswComponent.onSubmit(this.formObject.formName, this.AddformObject.formFields);
  var formData = this.jswService.getFormData();
  if (formData && formData.length) {
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.saverequestBody);
    if(formObj.shortCode!="")
    {
      this.saverequestBody = {
        "shortCode": (formObj.shortCode!="")?formObj.shortCode:null,
        "desc": (formObj.desc!="")?formObj.desc:null,
        "status": (formObj.status!="")?formObj.status:null,
        "lovId": null,
        "isDependent": (formObj.isDependent!="")?formObj.isDependent:null,
        "depLovId":(formObj.depLovId!="")?formObj.depLovId:null,
        "userId": this.sharedService.loggedInUserDetails.userId
      }
    }
  this.masterService.updateLOVmasterHeadervalues(this.saverequestBody).subscribe(Response => {
    this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Success, {
        message: `${Response}`,
      }
    );
    this.mappinglist =[];
    this.Filteredlovlist =[];
    this.getFilterCriteriaForLOV();
    this.closeModal();
  })
}

}

  public updateLOVMaterMappingDetails(mappingDetails): any {
    mappingDetails.userId = this.userDetails.userId;
    this.masterService.updateLOVmasterMappingDetails(mappingDetails).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `${Response}`,
        }
      );
      this.getlistofLovmaterdata();
      this.selectedLovRecord = [];
    })
  }

public onRowEditInit(lovvaluesdetails: any) {
  lovvaluesdetails.submitted = false;
  if(this.unModifiedlovlist.find(m => lovvaluesdetails.lovValueId == m.lovValueId)){
    this.clonedProducts[lovvaluesdetails.lovValueId ] = {...lovvaluesdetails};
  }
}

public onRowEditSave(lovvaluesdetails: any, rowIndex: number) {
  lovvaluesdetails.submitted = true;
  if(!lovvaluesdetails.valueShortCode || !lovvaluesdetails.valueDescription || !lovvaluesdetails.activeStatusYN || !lovvaluesdetails.codeValue){
    this.table.initRowEdit(lovvaluesdetails);
    return;
  }
  let index = this.unModifiedlovlist.findIndex(wc => lovvaluesdetails.lovValueId == wc.lovValueId)
  let reqObj = {
    activeStatusYN: lovvaluesdetails.activeStatusYN,
    codeValue:lovvaluesdetails.codeValue,
    lovId: lovvaluesdetails.lovId,
    lovValueId: lovvaluesdetails.lovValueId,
    mappingDetails: lovvaluesdetails.mappingDetails,
    valueDescription: lovvaluesdetails.valueDescription,
    valueShortCode: lovvaluesdetails.valueShortCode,
    userId: this.userDetails.userId
  }
  if(index == -1){
    this.masterService.updateLOVmastervalues(reqObj).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `${Response}`,
        }
      );
      delete this.clonedProducts[lovvaluesdetails.lovValueId];
      this.getlistofLovmaterdata();
    })
  }else{
    this.masterService.updateLOVmastervalues(reqObj).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `${Response}`,
        }
      );
      delete this.clonedProducts[lovvaluesdetails.lovValueId];
      this.getlistofLovmaterdata();
    })
  }
}

public onRowEditCancel(lovvaluesdetails: any, index: number) {
 lovvaluesdetails.submitted = false;
  let wcIndex = this.unModifiedlovlist.findIndex(wc => lovvaluesdetails.lovValueId == wc.lovValueId)
  if(wcIndex == -1){
    this.Filteredlovlist = this.Filteredlovlist.filter(ele => ele.lovValueId != lovvaluesdetails.lovValueId);
  }else{
    this.Filteredlovlist[index] = this.clonedProducts[lovvaluesdetails.lovValueId];
    delete this.clonedProducts[lovvaluesdetails.lovValueId];
  }
}

public filterTable(event: any){
   this.table.filterGlobal(event.target.value, 'contains');
   if(event.target.value){
     this.disableAddBtn = true;
   }else{
     this.disableAddBtn = false;
   }
}

public newRow() {
  return {
    activeStatusYN: this.statuslist.length ? this.statuslist[0].valueShortCode : '',
    codeValue:"",
    lovId: this.reqtBody.lovId,
    lovValueId: null,
    mappingDetails: null,
    valueDescription: "",
    valueShortCode: ""
  };
}

public enableEditMode(rowData){
  if(!this.table.isRowEditing(rowData)){
    this.table.initRowEdit(rowData);
  }
}
public getFormRef(event){
  this.formRef = event;
}
public closeModal() {
  //this.jswComponent.resetForm(this.AddformObject);
  this.display = false;
  this.AddformObject = JSON.parse(JSON.stringify(this.lovMasterForms.addLov));
}

public getAddFormRef(event){
  this.addFormRef = event;
}

}
