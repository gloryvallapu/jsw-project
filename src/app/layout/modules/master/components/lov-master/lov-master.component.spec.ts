import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LOVMasterComponent } from './lov-master.component';

describe('LOVMasterComponent', () => {
  let component: LOVMasterComponent;
  let fixture: ComponentFixture<LOVMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LOVMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LOVMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
