import { Component, OnInit, ViewChild } from '@angular/core';
import { TableType, LovCodes } from 'src/assets/enums/common-enum';
import { MasterService } from '../../../../services/master.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Table } from 'primeng/table/table';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';
@Component({
  selector: 'app-work-center-master',
  templateUrl: './work-center-master.component.html',
  styleUrls: ['./work-center-master.component.css']
})
export class WorkCenterMasterComponent implements OnInit {
  public workCenterList: any = [];
  public statusList: any = [];
  public workCenterTypeList: any = [];
  public tableType: any;
  public globalFilterArray: any = [];
  public clonedProducts: { [s: string]: any; } = {};
  public unModifiedWorkCenters: any = [];
  @ViewChild('dt') table: Table;
  public disableAddBtn: boolean = false;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isAddBtn: boolean = false;
  public isEditBtn: boolean = false;
  public isSaveBtn: boolean = false;
  public isCloseBtn: boolean = false;
  public disableYard: boolean = false;
  constructor(public masterService: MasterService, public sharedService: SharedService, public commonService: CommonApiService, public authService: AuthService) { }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Master)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.masterMgmtScreenIds.Product_Attribute_Master.id)[0];
    this.isAddBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Work_Center_List.buttons.add, menuConstants.prodAttrMasterSectionIds.Work_Center_List.id, this.screenStructure);
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Work_Center_List.buttons.edit, menuConstants.prodAttrMasterSectionIds.Work_Center_List.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Work_Center_List.buttons.save, menuConstants.prodAttrMasterSectionIds.Work_Center_List.id, this.screenStructure);
    this.isCloseBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Work_Center_List.buttons.close, menuConstants.prodAttrMasterSectionIds.Work_Center_List.id, this.screenStructure);
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getLovs(LovCodes.status);
    this.getWcTypes();
    // this.getWorkCenters();
    this.globalFilterArray = ['wcName','wcShortCode','wcDesc','sapWorkCenter','wcType','wcSubType','status'];
  }

   public getLovs(lovCode){
    this.commonService.getLovs(lovCode).subscribe(data => {
      if(lovCode == LovCodes.status){
        this.statusList = data;
      }
    })
  }

  public getWcTypes(){
    this.masterService.getWCTypeList().subscribe(data => {
      this.workCenterTypeList = data;
      this.getWorkCenters();
    })
  }

  public setSubTypes(workCenter){
    let wcType = this.workCenterTypeList.filter(ele => ele.code == workCenter.wcType)[0];
    workCenter.subTypes = wcType ? wcType.subTypes : [];
  }

  public getWorkCenters(){
    this.masterService.getWorkCenterList().subscribe(data => {
      data.map((ele: any) => {
        ele.submitted = false;
        ele.disabled = true;
        ele.srNo = this.sharedService.generateUniqueId();
        this.setSubTypes(ele);
        return ele;
      });
      data.map(((ele: any) => {
        let yardArr = ele.allocatedYards.map((yards:any) => {
          return yards.yardName;
        })
        ele.yardData = yardArr.join(",");
     }));
      this.unModifiedWorkCenters = JSON.parse(JSON.stringify(data));
      this.workCenterList = data;
    })
  }

  public onRowEditInit(workCenter: any) {
    workCenter.submitted = false;
    if(this.unModifiedWorkCenters.find(m => workCenter.srNo == m.srNo)){
      this.clonedProducts[workCenter.srNo] = {...workCenter};
    }
  }

  public onRowEditSave(workCenter: any, rowIndex: number) {
    workCenter.submitted = true;
    if(!workCenter.wcName || !workCenter.wcShortCode || !workCenter.wcDesc || !workCenter.sapWorkCenter 
      || !workCenter.wcType || !workCenter.wcSubType || !workCenter.status ){
      this.table.initRowEdit(workCenter);
      return;
    }
    let index = this.unModifiedWorkCenters.findIndex(wc => workCenter.srNo == wc.srNo)
    workCenter.allocatedYards.forEach(element => {
      if(element.yardCode == workCenter.defaultYardCode){
        workCenter.defaultYardName = element.yardName;
      }
    });
    let reqObj = {
      isActive: 'Y',
      sapWorkCenter: workCenter.sapWorkCenter,
      status: workCenter.status,
      wcDesc: workCenter.wcDesc,
      wcName: workCenter.wcName,
      wcShortCode: workCenter.wcShortCode,
      wcSubType: workCenter.wcSubType,
      wcType: workCenter.wcType,
      allocatedYards:workCenter.allocatedYards,
      defaultYardCode:workCenter.defaultYardCode,
      defaultYardName:workCenter.defaultYardName, 
    }
    
    if(index == -1){
      //22-11
      let reqObj = {
        isActive: 'Y',
        sapWorkCenter: workCenter.sapWorkCenter,
        status: workCenter.status,
        wcDesc: workCenter.wcDesc,
        wcName: workCenter.wcName,
        wcShortCode: workCenter.wcShortCode,
        wcSubType: workCenter.wcSubType,
        wcType: workCenter.wcType,
        allocatedYards:workCenter.allocatedYards,
        defaultYardCode:workCenter.defaultYardCode,
        defaultYardName:workCenter.defaultYardName, 
        createdBy: this.sharedService.loggedInUserDetails.userId
      }
      //
      this.masterService.createWorkCenter(reqObj).subscribe(data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Work Center created successfully'});
        delete this.clonedProducts[workCenter.srNo];
        this.getWorkCenters();
      })
    }else{
      //22-11
      let reqObj = {
        isActive: 'Y',
        sapWorkCenter: workCenter.sapWorkCenter,
        status: workCenter.status,
        wcDesc: workCenter.wcDesc,
        wcName: workCenter.wcName,
        wcShortCode: workCenter.wcShortCode,
        wcSubType: workCenter.wcSubType,
        wcType: workCenter.wcType,
        allocatedYards:workCenter.allocatedYards,
        defaultYardCode:workCenter.defaultYardCode,
        defaultYardName:workCenter.defaultYardName, 
        modifiedBy: this.sharedService.loggedInUserDetails.userId,
      }
      //
      this.masterService.updateWorkCenter(reqObj).subscribe(data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Work Center updated successfully'});
        delete this.clonedProducts[workCenter.srNo];
        this.getWorkCenters();
      })
    }
  }

  public onRowEditCancel(workCenter: any, index: number) {
    workCenter.submitted = false;
    this.disableYard = false;
    let wcIndex = this.unModifiedWorkCenters.findIndex(wc => workCenter.srNo == wc.srNo)
    if(wcIndex == -1){
      this.workCenterList = this.workCenterList.filter(ele => ele.srNo != workCenter.srNo);
    }else{
      this.workCenterList[index] = this.clonedProducts[workCenter.srNo];
      delete this.clonedProducts[workCenter.srNo];
    }
  }

  public filterTable(event: any){
     this.table.filterGlobal(event.target.value, 'contains');
     if(event.target.value){
       this.disableAddBtn = true;
     }else{
       this.disableAddBtn = false;
     }
  }

  public newRow() {
    return {
      isActive: "Y",
      sapWorkCenter: "",
      status: this.statusList.length ? this.statusList[0].valueShortCode : '',
      wcDesc: "",
      wcName: "",
      wcShortCode: "",
      // subTypes: this.workCenterTypeList.length ? this.workCenterTypeList[0].subTypes : [],
      // wcSubType: this.workCenterTypeList.length ? this.workCenterTypeList[0].subTypes[0].code : '',
      // wcType: this.workCenterTypeList.length ? this.workCenterTypeList[0].code : '',
      subTypes: [],
      wcSubType: '',
      wcType: '',
      srNo: this.sharedService.generateUniqueId(),
      allocatedYards:[],
      defaultYardCode:'',
      defaultYardName:''
    };
  }

  public enableEditMode(rowData){
    if(!this.table.isRowEditing(rowData)){
      this.table.initRowEdit(rowData);
    }
  }

  public addNewRow(){
    this.disableYard = true;
    let row = this.newRow();
    this.table.value.unshift(row);
    // Set the new row in edit mode
    this.table.initRowEdit(row);
    event.preventDefault();
  }
}
