import { Component, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/table/table';
import { MasterService } from '../../../../services/master.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { TableType, LovCodes, ColumnType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';
import { Material } from '../../../../enums/material.enum';

@Component({
  selector: 'app-size-master',
  templateUrl: './size-master.component.html',
  styleUrls: ['./size-master.component.css']
})
export class SizeMasterComponent implements OnInit {
  public sizeList: any = [];
  public statusList: any = [];
  public productList: any = [];
  public tableType: any;
  public globalFilterArray: any = [];
  public clonedProducts: { [s: string]: any; } = {};
  public unModifiedSizes: any = [];
  public columns: ITableHeader[] = [];
  public frozenCols: ITableHeader[] = [];
  @ViewChild('dt') table: Table;
  public disableAddBtn: boolean = false;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isAddBtn: boolean = false;
  public isEditBtn: boolean = false;
  public isSaveBtn: boolean = false;
  public isCloseBtn: boolean = false;
  public materialEnum = Material;
  constructor(public masterService: MasterService, public sharedService: SharedService, public commonService: CommonApiService, public authService: AuthService) { }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Master)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.masterMgmtScreenIds.Product_Attribute_Master.id)[0];
    this.isAddBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Size_Master_List.buttons.add, menuConstants.prodAttrMasterSectionIds.Size_Master_List.id, this.screenStructure);
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Size_Master_List.buttons.edit, menuConstants.prodAttrMasterSectionIds.Size_Master_List.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Size_Master_List.buttons.save, menuConstants.prodAttrMasterSectionIds.Size_Master_List.id, this.screenStructure);
    this.isCloseBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Size_Master_List.buttons.close, menuConstants.prodAttrMasterSectionIds.Size_Master_List.id, this.screenStructure);
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getLovs(LovCodes.status);
    this.getSizes();
    this.getProducts();
    this.frozenCols = [
      {field: 'productName', header: 'Product Name', columnType: ColumnType.dropdown, width: '150px', sortFieldName: ''}
    ]
    this.columns = [
      {field: 'productName', header: 'Product Name', columnType: ColumnType.dropdown, width: '150px', sortFieldName: ''},
      {field: 'sizeCode', header: 'Size Code', columnType: ColumnType.dropdown, width: '150px', sortFieldName: ''},
      {field: 'sizeDesc', header: 'Size Desc', columnType: ColumnType.dropdown, width: '150px', sortFieldName: ''},
      {field: 'dia', header: 'Diameter(mm)', columnType: ColumnType.dropdown, width: '100px', sortFieldName: ''},
      {field: 'thickness', header: 'Thickness(mm)', columnType: ColumnType.dropdown, width: '100px', sortFieldName: ''},
     // {field: 'leg_thickness_a', header: 'Leg Thickness A', columnType: ColumnType.dropdown, width: '130px', sortFieldName: ''},
     // {field: 'leg_thickness_b', header: 'Leg Thickness B', columnType: ColumnType.dropdown, width: '130px', sortFieldName: ''},
      {field: 'width', header: 'Width(mm)', columnType: ColumnType.dropdown, width: '100px', sortFieldName: ''},
     // {field: 'length', header: 'Length', columnType: ColumnType.dropdown, width: '100px', sortFieldName: ''},
     // {field: 'leg_length_a', header: 'Leg Length A', columnType: ColumnType.dropdown, width: '130px', sortFieldName: ''},
     // {field: 'leg_length_b', header: 'Leg Length B', columnType: ColumnType.dropdown, width: '130px', sortFieldName: ''},
     // {field: 'height', header: 'Height', columnType: ColumnType.dropdown, width: '100px', sortFieldName: ''},
      {field: 'status', header: 'Status', columnType: ColumnType.dropdown, width: '100px', sortFieldName: ''},
      {field: 'action', header: 'Action', columnType: ColumnType.action, width: '100px', sortFieldName: ''},
    ]
    this.globalFilterArray = this.columns.map(element => {
      return element.field;
    });
  }

  public getLovs(lovCode){
    this.commonService.getLovs(lovCode).subscribe(data => {
      if(lovCode == LovCodes.status){
        this.statusList = data;
      }
    })
  }

  public getProducts(){
    this.masterService.getMaterials().subscribe(data => {
      this.productList = data.filter(ele => ele.status.toLowerCase() != 'inactive');
    })
  }

  public getSizes(){
    this.masterService.getSizeList().subscribe(data => {
      data.map((ele: any) => {
        ele.submitted = false;
        ele.disabled = true;
        ele.srNo = this.sharedService.generateUniqueId();
        return ele;
      });
      this.unModifiedSizes = JSON.parse(JSON.stringify(data));
      this.sizeList = data;
    })
  }

  public onRowEditInit(size: any) {
    size.submitted = false;
    if(this.unModifiedSizes.find(s => size.srNo == s.srNo)){
      this.clonedProducts[size.srNo] = {...size};
    }
  }

  public onRowEditSave(size: any, rowIndex: number) {
    size.submitted = true;
    if(!size.productName || !size.sizeCode || !size.sizeDesc || !size.status){
      this.table.initRowEdit(size);
      return;
    }
    if(size.productName && ((this.getProductId(size.productName) == this.materialEnum.AsCastRounds || (this.getProductId(size.productName) == this.materialEnum.AsCastRoundsFinal ||
                            this.getProductId(size.productName) == this.materialEnum.Tmt || this.getProductId(size.productName) == this.materialEnum.TmtFinal ||
                            this.getProductId(size.productName) == this.materialEnum.PlainRounds || this.getProductId(size.productName) == this.materialEnum.PlainRoundsFinal)) && !size.dia))
    {
      return;
    }
    if(size.productName && ((this.getProductId(size.productName) == this.materialEnum.Billet || this.getProductId(size.productName) == this.materialEnum.BilletFinal ||
    this.getProductId(size.productName) == this.materialEnum.Bloom || this.getProductId(size.productName) == this.materialEnum.BloomFinal ||
    this.getProductId(size.productName) == this.materialEnum.LpFlats || this.getProductId(size.productName) == this.materialEnum.LpFlatsFinal ||
    this.getProductId(size.productName) == this.materialEnum.Slab || this.getProductId(size.productName) == this.materialEnum.SlabFinal)) && (!size.width || !size.thickness)){ //!size.leg_thickness_a || !size.leg_thickness_b
      return;
    }
    let index = this.unModifiedSizes.findIndex(s => size.srNo == s.srNo);

    let reqObj = {
      created_by: index == -1 ? this.sharedService.loggedInUserDetails.username : size.created_by,
      created_dt: index == -1 ? new Date() : size.created_dt,
      dia: size.dia,
      height: null, //size.height,
      leg_length_a: null, //size.leg_length_a,
      leg_length_b: null, //size.leg_length_b,
      leg_thickness_a: null,//size.leg_thickness_a,
      leg_thickness_b: null,//size.leg_thickness_b,
      length: null, //size.length,
      modified_by: index == -1 ? null : this.sharedService.loggedInUserDetails.username,
      modified_dt: index == -1 ? null : new Date(),
      productId: this.productList.filter(p => p.materialDesc == size.productName)[0].materialId,
      productName: size.productName,
      sizeCode: size.sizeCode,
      sizeDesc: size.sizeDesc,
      status: this.statusList.length ? this.statusList[0].valueShortCode : '',
      thickness: size.thickness,
      width: size.width,
    };
    
    if(index == -1){
      this.masterService.createSize(reqObj).subscribe(data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Size created successfully'});
        delete this.clonedProducts[size.srNo];
        this.getSizes();
      })
    }else{
      this.masterService.updateSize(reqObj).subscribe(data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Size updated successfully'});
        delete this.clonedProducts[size.srNo];
        this.getSizes();
      })
    }
  }

  public onRowEditCancel(size: any, index: number) {
    size.submitted = false;
    let sIndex = this.unModifiedSizes.findIndex(s => size.srNo == s.srNo)
    if(sIndex == -1){
      this.sizeList = this.sizeList.filter(ele => ele.srNo != size.srNo);
    }else{
      this.sizeList[index] = this.clonedProducts[size.srNo];
      delete this.clonedProducts[size.srNo];
    }
  }

  public filterTable(event: any){
     this.table.filterGlobal(event.target.value, 'contains');
     if(event.target.value){
      this.disableAddBtn = true;
     }else{
      this.disableAddBtn = false;
     }
  }

  public newRow() {
    return {
      created_by: null,
      created_dt: null,
      dia: null,
      height: null,
      leg_length_a: null,
      leg_length_b: null,
      leg_thickness_a: null,
      leg_thickness_b: null,
      length: null,
      modified_by: null,
      modified_dt: null,
      productId: null,
      productName: this.productList.length ? this.productList[0].materialDesc : null,
      sizeCode: null,
      sizeDesc: null,
      status: this.statusList.length ? this.statusList[0].valueShortCode : '',
      thickness: null,
      width: null,
      srNo: this.sharedService.generateUniqueId()
    };
  }

  public enableEditMode(rowData){
    if(!this.table.isRowEditing(rowData)){
      this.table.initRowEdit(rowData);
    }
  }

  public updateSizeCode(data){
    if((this.getProductId(data.productName) == this.materialEnum.AsCastRounds || this.getProductId(data.productName) == this.materialEnum.AsCastRoundsFinal
    || this.getProductId(data.productName) == this.materialEnum.Tmt || this.getProductId(data.productName) == this.materialEnum.TmtFinal
    || this.getProductId(data.productName) == this.materialEnum.PlainRounds || this.getProductId(data.productName) == this.materialEnum.PlainRoundsFinal) && data.dia){
      data.sizeCode = data.dia;
    }
    if((this.getProductId(data.productName) == this.materialEnum.Billet || this.getProductId(data.productName) == this.materialEnum.BilletFinal ||
    this.getProductId(data.productName) == this.materialEnum.Bloom || this.getProductId(data.productName) == this.materialEnum.BloomFinal ||
    this.getProductId(data.productName) == this.materialEnum.LpFlats || this.getProductId(data.productName) == this.materialEnum.LpFlatsFinal ||
    this.getProductId(data.productName) == this.materialEnum.Slab || this.getProductId(data.productName) == this.materialEnum.SlabFinal) && data.thickness && data.width){
      data.sizeCode = data.thickness + 'X' + data.width;
    }
  }

  public getProductId(productName){
    let productId = this.productList.filter(p => p.materialDesc == productName)[0].materialId
    return productId;
  }

}
