import { Component, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/table/table';
import { SharedService } from 'src/app/shared/services/shared.service';
import { MasterService } from '../../../../services/master.service';
import { TableType, LovCodes } from 'src/assets/enums/common-enum';
import { threeDigitsAfterDecimal } from 'src/app/shared/constants/app-constants';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';
@Component({
  selector: 'app-wc-size-mapping',
  templateUrl: './wc-size-mapping.component.html',
  styleUrls: ['./wc-size-mapping.component.css']
})
export class WcSizeMappingComponent implements OnInit {
  public mappingList: any = [];
  public statusList: any = [];
  public productList: any = [];
  public workCenterList: any = [];
  public tableType: any;
  public globalFilterArray: any = [];
  public sizeList: any = [];
  public clonedProducts: { [s: string]: any; } = {};
  public unModifiedMappingList: any = [];
  public regEX: any = threeDigitsAfterDecimal;
  @ViewChild('dt') table: Table;
  public isPatternError: boolean = false;
  public disableAddBtn: boolean = false;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isAddBtn: boolean = false;
  public isEditBtn: boolean = false;
  public isSaveBtn: boolean = false;
  public isCloseBtn: boolean = false;

  constructor(public masterService: MasterService, public sharedService: SharedService, public commonService: CommonApiService, public authService: AuthService) { }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Master)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.masterMgmtScreenIds.Product_Attribute_Master.id)[0];
    this.isAddBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.WC_Prod_Size_List.buttons.add, menuConstants.prodAttrMasterSectionIds.WC_Prod_Size_List.id, this.screenStructure);
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.WC_Prod_Size_List.buttons.edit, menuConstants.prodAttrMasterSectionIds.WC_Prod_Size_List.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.WC_Prod_Size_List.buttons.save, menuConstants.prodAttrMasterSectionIds.WC_Prod_Size_List.id, this.screenStructure);
    this.isCloseBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.WC_Prod_Size_List.buttons.close, menuConstants.prodAttrMasterSectionIds.WC_Prod_Size_List.id, this.screenStructure);
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getLovs(LovCodes.status);
    this.getWorkCenters();
    this.getProducts();
    this.globalFilterArray = ['wcName','materialName','sizeCode','castingSpeed','densityKgPerM','status'];
  }

  public getLovs(lovCode){
    this.commonService.getLovs(lovCode).subscribe(data => {
      if(lovCode == LovCodes.status){
        this.statusList = data;
      }
    })
  }

  public getWorkCenters(){
    this.masterService.getWorkCenterList().subscribe(data => {
      this.workCenterList = data;
    })
  }

  public setProductSizes(mappingRecord){
    let product = this.productList.filter(ele => ele.productName == mappingRecord.materialName)[0];
    mappingRecord.materialId = product ? product.productId : null;
    mappingRecord.sizeList = product ? product.sizeList : [];
  }

  public setSizeDesc(mappingRecord){
    let size = mappingRecord.sizeList.filter(ele => ele.sizeCode == mappingRecord.sizeCode)[0];
    mappingRecord.sizeDesc = size ? size.sizeDesc : null;
  }

  public getProducts(){
    this.masterService.getProductSizeList().subscribe( data => {
      this.productList = data;
      this.getMappingList();
    })
  }

  public getMappingList(){
    this.masterService.getMappingList().subscribe(data => {
      data.map((ele: any) => {
        ele.submitted = false;
        ele.disabled = true;
        ele.srNo = this.sharedService.generateUniqueId();
        this.setProductSizes(ele);
        return ele;
      });
      this.unModifiedMappingList = JSON.parse(JSON.stringify(data));
      this.mappingList = data;
    })
  }

  public onRowEditInit(mappingRecord: any) {
    mappingRecord.submitted = false;
    if(this.unModifiedMappingList.find(m => mappingRecord.srNo == m.srNo)){
      this.clonedProducts[mappingRecord.srNo] = {...mappingRecord};
    }
  }

  public onRowEditSave(mappingRecord: any, rowIndex: number) {
    mappingRecord.submitted = true;
    if(!mappingRecord.castingSpeed || !mappingRecord.densityKgPerM || this.isPatternError){
      this.table.initRowEdit(mappingRecord);
      return;
    }
    let mIndex = this.unModifiedMappingList.findIndex(m => mappingRecord.srNo == m.srNo)
    let reqObj = {
      "castingSpeed": mappingRecord.castingSpeed,
      "densityKgPerM": mappingRecord.densityKgPerM,
      "materialId": mappingRecord.materialId,
      "materialName": mappingRecord.materialName,
      "sizeCode": mappingRecord.sizeCode,
      "sizeDescription": mappingRecord.sizeDescription,
      "status": mappingRecord.status,
      "userId": this.sharedService.loggedInUserDetails.userId,
      "validFlag": "Y",
      "wcName": mappingRecord.wcName
    }
    if(mIndex == -1){
      this.masterService.createMapping(reqObj).subscribe(data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Mapping created successfully'});
        delete this.clonedProducts[mappingRecord.srNo];
        this.getMappingList();
      })
    }else{
      this.masterService.updateMapping(reqObj).subscribe(data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Mapping updated successfully'});
        delete this.clonedProducts[mappingRecord.srNo];
        this.getMappingList();
      })
    }
  }

  public onRowEditCancel(mappingRecord: any, index: number) {
    mappingRecord.submitted = false;
    let mIndex = this.unModifiedMappingList.findIndex(m => mappingRecord.srNo == m.srNo)
    if(mIndex == -1){
      this.mappingList = this.mappingList.filter(ele => ele.srNo != mappingRecord.srNo);
    }else{
      this.mappingList[index] = this.clonedProducts[mappingRecord.srNo];
      delete this.clonedProducts[mappingRecord.srNo];
    }
  }

  public filterTable(event: any){
    // this.table.filter(event.target.value, 'country.name', 'contains')
     this.table.filterGlobal(event.target.value, 'contains');
     if(event.target.value){
      this.disableAddBtn = true;
     }else{
      this.disableAddBtn = false;
     }
   }

   public newRow() {
    return {
      wcName: this.workCenterList.length ? this.workCenterList[0].wcName : null,
      materialId: null,
      materialName: null,
      // materialId: this.productList.length ? this.productList[0].productId : null,
      // materialName: this.productList.length ? this.productList[0].productName : null,
      sizeList: [],
      sizeCode: null,
      sizeDescription: null,
      // sizeList: this.productList.length ? this.productList[0].sizeList : [],
      // sizeCode: this.productList.length ? this.productList[0].sizeList[0].sizeCode : null ,
      // sizeDescription: this.productList.length ? this.productList[0].sizeList[0].sizeDesc : null,
      validFlag: "Y",
      densityKgPerM: null,
      castingSpeed: null,
      userId : this.sharedService.loggedInUserDetails.userId,
      status: this.statusList.length ? this.statusList[0].valueShortCode : '',
      submitted: false,
      disabled: false,
      srNo: this.sharedService.generateUniqueId()
    };
  }

  public enableEditMode(rowData){
    if(!this.table.isRowEditing(rowData)){
      this.table.initRowEdit(rowData);
    }
  }

  public setPatternErrStatus(isError: any){
    this.isPatternError = isError ? true : false;
  }

  public addNewRow(){
    let row = this.newRow();
    this.table.value.unshift(row);
    // Set the new row in edit mode
    this.table.initRowEdit(row);
    event.preventDefault();
  }

}
