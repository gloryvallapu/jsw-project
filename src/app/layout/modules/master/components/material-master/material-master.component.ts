import { Component, OnInit, ViewChild } from '@angular/core';
import { TableType, LovCodes } from 'src/assets/enums/common-enum';
import { Table } from 'primeng/table/table';
import { MasterService } from '../../services/master.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { alphaNumericWithSpace } from 'src/app/shared/constants/app-constants';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-material-master',
  templateUrl: './material-master.component.html',
  styleUrls: ['./material-master.component.css']
})
export class MaterialMasterComponent implements OnInit {
  public materialList: any = [];
  public statusList: any = [];
  public materialTypeList: any = [];
  public tableType: any;
  public globalFilterArray: any = [];
  public clonedProducts: { [s: string]: any; } = {};
  public unModifiedMaterials: any = [];
  @ViewChild('dt') table: Table;
  public regEX: any = alphaNumericWithSpace;
  public disableAddBtn: boolean = false;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isAddBtn: boolean = false;
  public isEditBtn: boolean = false;
  public isSaveBtn: boolean = false;
  public isCloseBtn: boolean = false;

  constructor(public masterService: MasterService, public sharedService: SharedService, public commonService: CommonApiService, public authService: AuthService) { }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Master)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.masterMgmtScreenIds.Product_Attribute_Master.id)[0];
    this.isAddBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Product_List.buttons.add, menuConstants.prodAttrMasterSectionIds.Product_List.id, this.screenStructure);
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Product_List.buttons.edit, menuConstants.prodAttrMasterSectionIds.Product_List.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Product_List.buttons.save, menuConstants.prodAttrMasterSectionIds.Product_List.id, this.screenStructure);
    this.isCloseBtn = this.sharedService.isButtonVisible(true, menuConstants.prodAttrMasterSectionIds.Product_List.buttons.close, menuConstants.prodAttrMasterSectionIds.Product_List.id, this.screenStructure);
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getLovs(LovCodes.materialType);
    this.getLovs(LovCodes.status);
    this.getMaterials();
    this.globalFilterArray = ['materialId','materialDesc','materialType','status'];
  }

  public getLovs(lovCode){
    this.commonService.getLovs(lovCode).subscribe(data => {
      if(lovCode == LovCodes.materialType){
        this.materialTypeList = data;
      }
      if(lovCode == LovCodes.status){
        this.statusList = data;
      }
    })
  }

  public getMaterials(){
    this.masterService.getMaterials().subscribe(data => {
      data.map((ele: any, index: number) => {
        ele.submitted = false;
        ele.disabled = true;
        ele.srNo = this.sharedService.generateUniqueId();
        return ele;
      });
      this.unModifiedMaterials = JSON.parse(JSON.stringify(data));
      this.materialList = data;
    })
  }

  public onRowEditInit(material: any) {
    material.submitted = false;
    if(this.unModifiedMaterials.find(m => material.srNo == m.srNo)){
      this.clonedProducts[material.srNo] = {...material};
    }
  }

  public onRowEditSave(material: any, rowIndex: number) {
    material.submitted = true;
    if(!material.materialDesc || !material.materialId){
      this.table.initRowEdit(material);
      return;
    }
    let mIndex = this.unModifiedMaterials.findIndex(m => material.srNo == m.srNo)
    let reqObj = {
      materialDesc: material.materialDesc,
      materialId: material.materialId,
      materialType: material.materialType,
      status: material.status
    }
    if(mIndex == -1){
      this.masterService.createMaterial(reqObj).subscribe(data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Material created successfully'});
        delete this.clonedProducts[material.srNo];
        this.getMaterials();
      })
    }else{
      this.masterService.updateMaterial(reqObj).subscribe(data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Material updated successfully'});
        delete this.clonedProducts[material.srNo];
        this.getMaterials();
      })
    }
  }

  public onRowEditCancel(material: any, index: number) {
    material.submitted = false;
    let mIndex = this.unModifiedMaterials.findIndex(m => material.srNo == m.srNo)
    if(mIndex == -1){
      this.materialList = this.materialList.filter(ele => ele.srNo != material.srNo);
    }else{
      this.materialList[index] = this.clonedProducts[material.srNo];
      delete this.clonedProducts[material.srNo];
    }
  }

  public filterTable(event: any){
     this.table.filterGlobal(event.target.value, 'contains');
     if(event.target.value){
      this.disableAddBtn = true;
     }else{
      this.disableAddBtn = false;
     }
   }

   public newRow() {
    return {
      materialId: '',
      materialDesc: null,
      materialType: this.materialTypeList.length ? this.materialTypeList[0].valueShortCode : '',
      status: this.statusList.length ? this.statusList[0].valueShortCode : '',
      submitted: false,
      disabled: false,
      srNo: this.sharedService.generateUniqueId()
    };
  }

  public enableEditMode(rowData){
    if(!this.table.isRowEditing(rowData)){
      this.table.initRowEdit(rowData);
    }
  }
}
