import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TableType, LovCodes } from 'src/assets/enums/common-enum';
import { Table } from 'primeng/table/table';
import { MasterService } from '../../services/master.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { alphaNumericWithSpace } from 'src/app/shared/constants/app-constants';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { MaterialMasterComponent } from './material-master.component';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { MasterEndPoints } from '../../form-objects/master-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('MaterialMasterComponent', () => {
  let component: MaterialMasterComponent;
  let fixture: ComponentFixture<MaterialMasterComponent>;
  let sharedService: SharedService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let masterService: MasterService
  let commonService: CommonApiService

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaterialMasterComponent ],
      imports :[
       FormsModule,
       ReactiveFormsModule,
       HttpClientModule,
       SharedModule
     ],
     providers:[
       JswCoreService,
       MasterService,
       CommonApiService,
       JswFormComponent,
       MasterEndPoints,
       API_Constants,
       CommonAPIEndPoints,
       { provide: ToastrService, useValue: toastrService }, 
     ]
   })
   .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
