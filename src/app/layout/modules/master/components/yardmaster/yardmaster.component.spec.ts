import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YardmasterComponent } from './yardmaster.component';

describe('YardmasterComponent', () => {
  let component: YardmasterComponent;
  let fixture: ComponentFixture<YardmasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YardmasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YardmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
