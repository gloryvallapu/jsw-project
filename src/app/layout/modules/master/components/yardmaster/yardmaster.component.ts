import { Component, OnInit } from '@angular/core';
import { YardMasterForms } from '../../form-objects/master-form-objects';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswFormComponent } from 'jsw-core';
import { JswCoreService } from 'jsw-core';
import { MasterService } from '../../services/master.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { element } from 'protractor';
import { NgForm } from '@angular/forms';
import { Yard, YardForm } from '../../interfaces/yard-master-interface';

@Component({
  selector: 'app-yardmaster',
  templateUrl: './yardmaster.component.html',
  styleUrls: ['./yardmaster.component.css']
})
export class YardmasterComponent implements OnInit {
  formObject: any;
  tableType: any;
  viewType = ComponentType;
  columns: ITableHeader[] = [];
  yardList: any = [];
  public formRef: NgForm;
  globalFilterArray: string[];
  isEditUser: boolean = false;
  selectedRecordForEdit: any;
  propDataTypeList: any = [];
  yardForm: YardForm;
  public yardlist: any = [];
  public movementyardlist: any = [];
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isEditBtn: boolean = false;
  public isAddBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isUpdateBtn: boolean = false;
  public isCancelBtn: boolean = false;
  public userId:string;
  public YardForm: YardForm;
  public movementyards:any=[];
  public tableRef: any;
  constructor(    
    public yardMasterForms: YardMasterForms,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public masterService: MasterService,
    public commonService: CommonApiService,
    public authService: AuthService) {
      this.yardForm = new YardForm();
     }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Master)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.masterMgmtScreenIds.Yard_Master.id)[0];
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.YardMasterSectionIds.List.buttons.edit, menuConstants.YardMasterSectionIds.List.id, this.screenStructure);
    this.isAddBtn = this.sharedService.isButtonVisible(true, menuConstants.YardMasterSectionIds.Add_New_Record.buttons.add, menuConstants.YardMasterSectionIds.Add_New_Record.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.YardMasterSectionIds.Add_New_Record.buttons.reset, menuConstants.YardMasterSectionIds.Add_New_Record.id, this.screenStructure);
    this.isUpdateBtn = this.sharedService.isButtonVisible(true, menuConstants.YardMasterSectionIds.Edit_Record.buttons.update, menuConstants.YardMasterSectionIds.Edit_Record.id, this.screenStructure);
    this.isCancelBtn = this.sharedService.isButtonVisible(true, menuConstants.YardMasterSectionIds.Edit_Record.buttons.cancel, menuConstants.YardMasterSectionIds.Edit_Record.id, this.screenStructure);
    this.formObject = JSON.parse(JSON.stringify(this.yardMasterForms.filterYard));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.userId=this.sharedService.loggedInUserDetails.userId;
    this.columns = [
      {
        field: 'wcName',
        header: 'Unit',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'yardCode',
        header: 'Yard Code',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'yardName',
        header: 'Yard Name',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'yardData',
        header: 'Movement Yards',
        columnType: ColumnType.ellipsis,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'sapLocCode',
        header: 'SAP Location Code',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'inTransitId',
        header: 'In Transit Id',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'status',
        header: 'Status',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
      },   
      { field: 'action', 
      header: 'Action', 
      columnType: ColumnType.action, 
      width: '80px', 
      sortFieldName: ''} 
    ];
    this.getAllWorkCenterList();
    this.getLovs(LovCodes.status, 'status');
    this.getLovs(LovCodes.sap_loc, 'sapLocCode');
    this.getmovementyardlist();
    this.getAllyardlist();
    this.globalFilterArray = this.columns.map((element) => {
      return element.field;
    });
  }

  public getAllWorkCenterList() {
    this.commonService.getWorkCenterListByUserId().subscribe((data) => {
    this.formObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list = data.map((x: any) => {
        return { displayName: x.wcName, modelValue: x.wcName };
      });
    })
  }
  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
    this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }
  public textBoxEventHandling(formObj){
  let yardcd='In_Transit_'+ formObj.formFields.filter((obj: any) => obj.ref_key == 'yardCode')[0].value;
  this.formObject.formFields.filter((obj: any) => obj.ref_key == 'inTransitId')[0].value=yardcd
}
  
  getAllyardlist()
  {
   this.masterService.getallyardlist().subscribe((data) => {
    data.map(((ele: any) => {
      let yardArr = ele.movementYards.map((yard:any) => {
        return yard.yardName;
      })
      ele.yardData = yardArr.join(",");
      return ele;
   }));
   this.yardlist=data;
   });

  }
  getmovementyardlist()
  {
    this.masterService.getallyardlist().subscribe((data) => {
    this.movementyardlist=data;
    this.formObject.formFields.filter((obj: any) => obj.ref_key == 'movementYards')[0].list = data.map((x:any) => {
      return {
        displayName: x.yardName,
        modelValue: x.yardCode
       }
    })
  });
  }
  public getYardCriteriaDetails(operationType: string) {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
   if(formData && formData.length){
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.yardForm.getyardForm());
    let movementyards: any = [];
    formObj.movementYards.forEach((yardcode: any) => {
      movementyards.push(this.movementyardlist.filter((movementyard:any) => movementyard.yardCode == yardcode)[0]);
     });
   let reqBody : Yard = {
    wcName : formObj.wcName,
    yardCode : formObj.yardCode,
    yardName : formObj.yardName,
    sapLocCode : formObj.sapLocCode,
    inTransitId : formObj.inTransitId,
    status : formObj.status,
    movementYards:movementyards
   };
   if(operationType == 'add'){
    this.createUser(reqBody);
   }else if(operationType == 'update'){
    this.updateUser(reqBody);
   }
 }
 }
 public editRecord(data: any){
  if(!this.sharedService.isSectionVisible(menuConstants.YardMasterSectionIds.Edit_Record.id, this.screenStructure)){
    this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'You are not authorized to access Edit User section. Please contact Admin'});
  }
  this.isEditUser = true;
  this.selectedRecordForEdit = data;

  let movementYards = this.formObject.formFields.filter((ele:any) => ele.ref_key == 'movementYards')[0];
  movementYards.selectedItems = data.movementYards.map((ele:any) => { return ele.yardCode; })
  this.sharedService.mapResponseToFormObj(this.formObject.formFields, data)
}

public createUser(reqBody: any) {
  this.masterService.createYardMaster(this.userId,reqBody).subscribe((data) => {
    this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Success,
      { message: 'Yard created successfully!' }
    );
    this.resetFilter();
    this.getAllyardlist();
  });

}

public updateUser(reqBody: any){
  this.masterService.updateYardMaster(this.userId,reqBody).subscribe(data => {
  this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: "Yard" + (data ? "-" + data.YardName : '') + " updated successfully!"});
  });
  this.resetFilter();
  this.getAllyardlist();
}
 public deleteUser(event)
 {

 }
 public resetFilter(){
  this.jswComponent.resetForm(this.formObject, this.formRef);
  this.isEditUser = false;
  this.yardList=[];
  this.getAllyardlist();
}
  public dropdownChangeEvent(event){
  }
  public getFormRef(event){
    this.formRef = event;
  }

  public getTableRef(event){
    this.tableRef = event;
  }
  public filterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }
}
