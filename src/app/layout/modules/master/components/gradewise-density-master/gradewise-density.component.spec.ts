import { ComponentFixture, TestBed } from '@angular/core/testing';
import {Component,OnInit} from '@angular/core';
import {ComponentType} from 'projects/jsw-core/src/lib/enum/common-enum';
import {GradewiseDensityMasterForms} from '../../form-objects/master-form-objects';
import {JswCoreService,JswFormComponent} from 'jsw-core';
import {MasterService} from '../../services/master.service';
import {SharedService} from 'src/app/shared/services/shared.service';
import {CreateGrade} from '../../interfaces/grade-master-interface';
import {TableType,ColumnType,LovCodes} from 'src/assets/enums/common-enum';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { MasterEndPoints } from '../../form-objects/master-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { GradewiseDensityComponent } from './gradewise-density.component';

describe('GradewiseDensityComponent', () => {
  let component: GradewiseDensityComponent;
  let fixture: ComponentFixture<GradewiseDensityComponent>;
  let sharedService: SharedService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let masterService: MasterService
  let commonService: CommonApiService
  let formObjects : GradewiseDensityMasterForms

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GradewiseDensityComponent ],
       imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        MasterService,
        CommonApiService,
        JswFormComponent,
        MasterEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        GradewiseDensityMasterForms,
        { provide: ToastrService, useValue: toastrService }, 
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GradewiseDensityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
