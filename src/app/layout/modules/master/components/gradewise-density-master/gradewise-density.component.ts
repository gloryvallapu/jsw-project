import { Component, OnInit, ViewChild } from '@angular/core';
import { GradewiseDensityMasterForms } from '../../form-objects/master-form-objects';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { TableType, LovCodes } from 'src/assets/enums/common-enum';
import { JswCoreService } from 'projects/jsw-core/src/public-api';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Table } from 'primeng/table';
import { MasterService } from '../../services/master.service';
import { JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { threeDigitsAfterDecimal } from 'src/app/shared/constants/app-constants';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-gradewise-density',
  templateUrl: './gradewise-density.component.html',
  styleUrls: ['./gradewise-density.component.css'],
})
export class GradewiseDensityComponent implements OnInit {
  formObject: any;
  viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public gradedensityList: any = [];
  public tableType: any;
  public globalFilterArray: any = [];
  public workCenterTypeList: any = [];
  public statusList: any = [];
  isEditUser: boolean = false;
  public unModifiedWorkCenters: any = [];
  public clonedProducts: { [s: string]: any } = {};
  @ViewChild('dt') table: Table;
  public disableAddBtn: boolean = false;
  productTypeList: any = [];
  sizeList: any = [];
  wcNameList: any = [];
  gradeList: any = [];
  public regEX: any = threeDigitsAfterDecimal;
  public isPatternError: boolean = false;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isAddBtn: boolean = false;
  public isEditBtn: boolean = false;
  public isSaveBtn: boolean = false;
  public isCloseBtn: boolean = false;
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public formRef: NgForm;
  constructor(
    public gradeWiseDensityMasterForms: GradewiseDensityMasterForms,
    public sharedService: SharedService,
    public masterService: MasterService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public commonService: CommonApiService,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Master)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.masterMgmtScreenIds.Gradewise_Density_Master.id)[0];
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeWiseDensityMasterSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.gradeWiseDensityMasterSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeWiseDensityMasterSectionIds.Filter_Criteria.buttons.reset, menuConstants.gradeWiseDensityMasterSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isAddBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeWiseDensityMasterSectionIds.List.buttons.add, menuConstants.gradeWiseDensityMasterSectionIds.List.id, this.screenStructure);
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeWiseDensityMasterSectionIds.List.buttons.edit, menuConstants.gradeWiseDensityMasterSectionIds.List.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeWiseDensityMasterSectionIds.List.buttons.save, menuConstants.gradeWiseDensityMasterSectionIds.List.id, this.screenStructure);
    this.isCloseBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeWiseDensityMasterSectionIds.List.buttons.close, menuConstants.gradeWiseDensityMasterSectionIds.List.id, this.screenStructure);
    this.getWCandPrdDesc();
    this.getallgrade();
    this.getAllGradeWiseDensity();
    this.getLovs(LovCodes.status);
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject =JSON.parse(JSON.stringify( this.gradeWiseDensityMasterForms.retrieveGrade));
    this.globalFilterArray = this.columns.map((element) => {
      return element.field;
    });
    this.globalFilterArray = [
      'wcName',
      'materialType',
      'status',
      'gradeName',
      'sizeCode',
      'density',
      'castingSpeed',
      'materialDesc'
    ];
  }

  public newRow() {
    var newWorkcenter; var newProduct;
    let workcenter = this.formObject.formFields.filter((ele) => ele.ref_key == 'workcenter')[0];
    let product = this.formObject.formFields.filter((ele) => ele.ref_key == 'productType')[0];
    if (!workcenter.value && this.workCenterTypeList.length > 0) {//if work center is not selected in filter criteria
      //newWorkcenter = this.workCenterTypeList[0].wcName;
      newWorkcenter = null;
    }else{ //if work center is selected in filter criteria
      newWorkcenter = workcenter.value;
    }

    if (!product.value && this.workCenterTypeList.length > 0) { //if product is not selected in filter criteria
      this.productTypeList = this.workCenterTypeList[0].materialDescription.map((x:any) => {return { displayName: x.materialDesc, modelValue: x.materialDesc}});
      //newProduct = this.workCenterTypeList[0].materialDescription[0].materialDesc;
      newProduct = null;
      this.sizeList = this.workCenterTypeList[0].materialDescription[0].sizeCodes.map((x: any) => {
        return { displayName: x.sizeCode, modelValue: x.sizeCode };
      });
    }else{
      let prodArr = this.workCenterTypeList.filter(wc => wc.wcName == newWorkcenter)[0].materialDescription;
      this.productTypeList = prodArr.map((x:any) => {return { displayName: x.materialDesc, modelValue: x.materialDesc}});
      newProduct = product.value;
      this.sizeList = prodArr.filter(prod => prod.materialDesc == newProduct)[0].sizeCodes.map((x: any) => {
        return { displayName: x.sizeCode, modelValue: x.sizeCode };
      });
    }

    // return {
    //   wcName: this.wcNameList.length ? newWorkcenter : null,
    //   materialDesc: this.productTypeList.length ? newProduct : null,
    //   sizeCode: this.sizeList.length ? this.sizeList[0].displayName : null,
    //   gradeName: this.gradeList.length ? this.gradeList[0].modelValue : null,
    //   density: null,
    //   castingSpeed: null,
    //   status: this.statusList.length ? this.statusList[0].valueShortCode : '',
    //   srNo: this.sharedService.generateUniqueId(),
    //   disabled: false,
    // };

    return {
      wcName: this.wcNameList.length && newWorkcenter ? newWorkcenter : null,
      materialDesc: this.productTypeList.length && newProduct ? newProduct : null,
      sizeCode: null,
      gradeName: null,
      density: null,
      castingSpeed: null,
      status: this.statusList.length ? this.statusList[0].valueShortCode : '',
      srNo: this.sharedService.generateUniqueId(),
      disabled: false,
    };
  }

  public filterTable(event: any) {
    this.table.filterGlobal(event.target.value, 'contains');
    if (event.target.value) {
      this.disableAddBtn = true;
    } else {
      this.disableAddBtn = false;
    }
  }

  public getLovs(lovCode) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      if (lovCode == LovCodes.status) {
        this.statusList = data;
      }
    });
  }

  public getallgrade() {
    this.masterService.getallgrade().subscribe((data) => {
      this.gradeList = data.map((x: any) => {
        return {
          displayName: x.gradeDesc,
          modelValue: x.gradeName,
        };
      });
    });
  }

  public setProductList(workCenter) {
    let wcName = this.workCenterTypeList.filter((ele) => ele.wcName == workCenter.wcName)[0];
    let prodList = wcName ? wcName.materialDescription : [];
    this.productTypeList = prodList.length ? prodList.map((x:any) => {return { displayName: x.materialDesc, modelValue: x.materialDesc}}) : [];
    this.sizeList =  prodList.length ? prodList[0].sizeCodes.map((x: any) => { return { displayName: x.sizeCode, modelValue: x.sizeCode }}) : [];
  }

  public setSizeList(workCenter){
    let wcName = this.workCenterTypeList.filter((ele) => ele.wcName == workCenter.wcName)[0];
    let prodList = wcName ? wcName.materialDescription : [];
    let sizeCodes = prodList.length ? prodList.filter(prod => prod.materialDesc == workCenter.materialDesc)[0].sizeCodes : [];
    this.sizeList = sizeCodes.length ? sizeCodes.map((x: any) => { return { displayName: x.sizeCode, modelValue: x.sizeCode }}) : [];
  }

  public getAllGradeWiseDensity() {
    this.masterService.getAllGradeWiseDensity().subscribe((data) => {
      this.unModifiedWorkCenters = JSON.parse(JSON.stringify(data));
      data.map((ele: any) => {
        ele.submitted = false;
        ele.disabled = true;
        ele.srNo = this.sharedService.generateUniqueId();
        //this.setProductList(ele);
        return ele;
      });
      this.unModifiedWorkCenters = JSON.parse(JSON.stringify(data));
      this.gradedensityList = data;
    });
  }

  public getWCandPrdDesc() {
    this.masterService.getWCandPrdDesc().subscribe((data) => {
      this.workCenterTypeList = data;
      this.wcNameList = this.sharedService.getDropdownData(data, 'wcName', 'wcName');
      this.formObject.formFields.filter(
        (ele) => ele.ref_key == 'workcenter'
      )[0].list = this.wcNameList;
    });
  }

  public getProductByWC(id) {
    let products = this.workCenterTypeList.filter((ele) => ele.wcName == id)[0]
      .materialDescription;
    let prodObj = this.formObject.formFields.filter(
      (ele) => ele.ref_key == 'productType'
    )[0];
    prodObj.list = products.map((x: any) => {
      return {
        displayName: x.materialDesc,
        modelValue: x.materialDesc,
      };
    });
    //this.productTypeList = prodObj.list;
    prodObj.value = prodObj.list[0].modelValue;
  }

  public dropdownChangeEvent(data) {
    if (data.value) {
      this.getProductByWC(data.value);
    }else{
      this.formObject.formFields.filter((ele) => ele.ref_key == 'productType')[0].list = [];
    }
  }

  getFilteredGradewisedensity() {
    this.jswService.setFormData(this.formObject.formFields);
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData[0].isError == false && formData[1].isError == false) {
      let parentScreenId = this.formObject.formFields.filter(
        (ele: any) => ele.ref_key == 'workcenter'
      )[0].value;
      let subScreenId = this.formObject.formFields.filter(
        (ele: any) => ele.ref_key == 'productType'
      )[0].value;
      this.masterService
        .getFilteredGradeDensity(parentScreenId, subScreenId)
        .subscribe((data) => {
          this.unModifiedWorkCenters = JSON.parse(JSON.stringify(data));
          data.map((ele: any) => {
            ele.submitted = false;
            ele.disabled = true;
            ele.srNo = this.sharedService.generateUniqueId();
            return ele;
          });
          this.unModifiedWorkCenters = JSON.parse(JSON.stringify(data));
          this.gradedensityList = data;
          this.gradedensityList.map((x: any) => {
            if (x.wcName == parentScreenId) {
              this.onRowEditInit(x);
              return;
            }
          });
        });
    }
  }

  public resetFilter(retrievescrren) {
    if (retrievescrren == 'resetRetrieveScreen') {
      //this.jswComponent.resetForm(this.formObject);
      this.sharedService.resetForm(this.formObject,this.formRef)
      this.getAllGradeWiseDensity();
    } else {
      this.isEditUser = false;
      this.sharedService.resetForm(this.formObject,this.formRef)
      //this.jswComponent.resetForm(this.formObject);
      this.getAllGradeWiseDensity();
    }
  }

  public enableEditMode(rowData) {
    if (!this.table.isRowEditing(rowData)) {
      this.table.initRowEdit(rowData);
    }
  }

  public onRowEditInit(workCenter: any) {
    workCenter.disabled = true;
    workCenter.submitted = false;
    if (this.unModifiedWorkCenters.find((m) => workCenter.srNo == m.srNo)) {
      this.clonedProducts[workCenter.srNo] = { ...workCenter };
    }
  }

  public onRowEditSave(workCenter: any, rowIndex: number) {
    //let workCenter = this.gradedensityList[rowIndex];
    workCenter.submitted = true;
    if (
      !workCenter.wcName ||
      !workCenter.materialDesc ||
      !workCenter.sizeCode ||
      !workCenter.gradeName ||
      !workCenter.density ||
      !workCenter.castingSpeed ||
      !workCenter.status
    ) {
      this.table.initRowEdit(workCenter);
      return;
    }

    let reqObj = {
      isActive: 'Y',
      wcName: workCenter.wcName,
      materialDesc: workCenter.materialDesc,
      sizeCode: workCenter.sizeCode,
      gradeName: workCenter.gradeName,
      density: workCenter.density,
      castingSpeed: workCenter.castingSpeed,
      status: workCenter.status,
      createdBy: this.sharedService.loggedInUserDetails.userId
    };

    let index = this.unModifiedWorkCenters.findIndex((wc) => workCenter.srNo == wc.srNo);
    if (index == -1) {
      this.masterService.createGradeWiseDensity(reqObj).subscribe((data) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: 'Gradewise Density created successfully' }
        );
        delete this.clonedProducts[workCenter.srNo];
        this.getAllGradeWiseDensity();
      });
    } else {
      this.masterService.updateGradeWiseDensity(reqObj).subscribe((data) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: 'Gradewise Density updated successfully' }
        );
        delete this.clonedProducts[workCenter.srNo];
        this.getAllGradeWiseDensity();
      });
    }
  }

  public onRowEditCancel(workCenter: any, index: number) {
    workCenter.submitted = false;
    let wcIndex = this.unModifiedWorkCenters.findIndex(
      (wc) => workCenter.srNo == wc.srNo
    );
    if (wcIndex == -1) {
      this.gradedensityList = this.gradedensityList.filter(
        (ele) => ele.srNo != workCenter.srNo
      );
    } else {
      this.gradedensityList[index] = this.clonedProducts[workCenter.srNo];
      delete this.clonedProducts[workCenter.srNo];
    }

    this.setProductList(this.workCenterTypeList[0]);
    // this.productTypeList = this.workCenterTypeList[0].materialDescription.list
  }

  public setPatternErrStatus(isError: any) {
    this.isPatternError = isError ? true : false;
  }

  public addNewRow(){
    let row = this.newRow();
    this.table.value.unshift(row);
    // Set the new row in edit mode
    this.table.initRowEdit(row);
    event.preventDefault();
  }

  public getFormRef(event){
    this.formRef = event;
  }

}
