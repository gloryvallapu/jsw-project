import {
  Component,
  OnInit
} from '@angular/core';
import {
  ComponentType
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  GradeMasterForms
} from '../../form-objects/master-form-objects';
import {
  JswCoreService,
  JswFormComponent
} from 'jsw-core';
import {
  MasterService
} from '../../services/master.service';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  CreateGrade
} from '../../interfaces/grade-master-interface';
import {
  TableType,
  ColumnType,
  LovCodes
} from 'src/assets/enums/common-enum';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { SelectItemGroup } from "primeng/api";
import { FilterService } from "primeng/api";
import { retry } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-grade-master',
  templateUrl: './grade-master.component.html',
  styleUrls: ['./grade-master.component.css'],
  providers: [FilterService]
})
export class GradeMasterComponent implements OnInit {
  public dbGradeProps: any;
  public formObject: any;
  public viewType = ComponentType;
  public getGradeGroupsCategoryDetails: any = [];
  public AllGradeDetails: any = [];
  public createGrade: CreateGrade;
  public responseBody: any;
  public columns: any = [];
  public globalFilterArray: any = [];
  public tableType: any;
  public actionAddNewGrade: boolean = true;
  public actionUpgradeGrade: boolean = false;
  public gradeMenuItems: any = [];
  public activeGradeItem: any;
  public selectedGradeProperties: any;
  public isResPonseRecieved: boolean = false;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isEditBtn: boolean = false;
  public isAddBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isUpdateBtn: boolean = false;
  public isCancelBtn: boolean = false;
  public formRef: NgForm;
  public tableRef: any;
  public tabMenu = [{
    label: 'Grade Chemistry',
    sectionId: 2034
  },
  {
    label: 'Mech Property',
    sectionId: 2035
  },
  {
    label: 'Metallurgy Prop',
    sectionId: 2036
  },
  {
    label: 'NDT',
    sectionId: 2037
  },
 ];
  test: any;
  text:any
  filteredCountries: any[];
  constructor(
    private filterService: FilterService,
    public commonApiService:CommonApiService,
    public sharedService: SharedService,
    public gradeMasterForms: GradeMasterForms,
    public masterService: MasterService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public authService: AuthService
  ) {
    this.createGrade = new CreateGrade();
  }

  ngOnInit(): void {
    this.getLovsForEndApplications();
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Master)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.masterMgmtScreenIds.Grade_Master.id)[0];
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeMasterSectionIds.List.buttons.edit, menuConstants.gradeMasterSectionIds.List.id, this.screenStructure);
    this.isAddBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeMasterSectionIds.Add_New_Record.buttons.add, menuConstants.gradeMasterSectionIds.Add_New_Record.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeMasterSectionIds.Add_New_Record.buttons.reset, menuConstants.gradeMasterSectionIds.Add_New_Record.id, this.screenStructure);
    this.isUpdateBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeMasterSectionIds.Edit_Record.buttons.update, menuConstants.gradeMasterSectionIds.Edit_Record.id, this.screenStructure);
    this.isCancelBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeMasterSectionIds.Edit_Record.buttons.cancel, menuConstants.gradeMasterSectionIds.Edit_Record.id, this.screenStructure);
    this.gradeMenuItems.length = 0;
    this.tabMenu.forEach(element => {
      if(this.sharedService.isSectionVisible(element.sectionId, this.screenStructure)){
        this.gradeMenuItems.push(element);
      }
    })
    this.activeGradeItem = this.gradeMenuItems.length ? this.gradeMenuItems[0] : undefined;
    this.formObject = JSON.parse(JSON.stringify(this.gradeMasterForms.addGrade));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getAllGrades();
    this.getGradeGroupsCategory();
    this.getAllProductIds();
    this.columns = [{
        field: 'psnNo',
        header: 'PSN Number',
        columnType: ColumnType.hyperlink,
        width: '160px',
        sortFieldName: 'userId',
      },
      {
        field: 'gradeName',
        header: 'Grade ID',
        columnType: ColumnType.hyperlink,
        width: '150px',
        sortFieldName: 'soId',
      },
      {
        field: 'gradeDesc',
        header: 'Grade Description',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'endApplication',
        header: 'End Application',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'gradeGroup',
        header: 'Grade Group',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'gradeCategory',
        header: 'Grade Category',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'selectedProductIds',
        header: 'Product',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'action',
        header: 'Action',
        columnType: ColumnType.action,
        width: '80px',
        sortFieldName: '',
      },
    ];
    this.globalFilterArray = this.columns.map(element => {
      return element.field;
    });
  }


  public tabMenuChange(value) {
    var index = this.gradeMenuItems.findIndex((menu: any) => menu.label == value.target.innerText);
    this.activeGradeItem = this.gradeMenuItems[index]
  }

  public getAllGrades() {
    this.masterService.getAllGrades().subscribe((Response) => {
      if (Response != null && Response.length > 0) {
        Response.map((grade: any) => {
          grade.gradeCategory = grade.gradeCategorys ?
            grade.gradeCategorys.gradeCategory :
            '';
          return grade;
        });
        Response.map((x: any) => {
          x.selectedProductIds = x.selectedProductIds.map((y: any) => {
            return y.materialDesc
          })
          return x
        })
        this.AllGradeDetails = Response;
      }
    });
  }

  public getLovsForEndApplications(){
    this.commonApiService.getLovs('End_Application').subscribe(Response => {
     if(Response.length > 0){
      this.formObject.formFields.filter(
        (item: any) => item.ref_key == 'endApplication'
      )[0].list  = Response.map((x:any)=> {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        }
      })
     }
    })
  }
  public getGradeGroupsCategory() {
    this.masterService.getGradeGroupsCategory().subscribe((Response) => {
      if (Response != null && Response.length > 0) {
        this.getGradeGroupsCategoryDetails = Response;
        this.formObject.formFields.filter(
          (item: any) => item.ref_key == 'gradeGroup'
        )[0].list = Response.map((gradeGrp: any) => {
          return {
            displayName: gradeGrp.gradeGroup,
            modelValue: gradeGrp.gradeGroup,
          };
        });
      }
    });
  }

  public getAllProductIds() {
    this.masterService.getAllProductIds().subscribe((Response) => {
      if (Response != null && Response.length > 0) {
        this.formObject.formFields.filter(
          (item: any) => item.ref_key == 'selectedProductIds'
        )[0].list = Response.map((material: any) => {
          return {
            displayName: material.valueDescription,
            modelValue: material.valueShortCode,
          };
        });

        this.test =         this.formObject.formFields.filter(
          (item: any) => item.ref_key == 'selectedProductIds'
        )[0].list
      }
    });
  }

  public getGradeGroup(gradeGrp) {
    this.formObject.formFields.filter(
        (item: any) => item.ref_key == 'gradeCategory'
      )[0].list = this.getGradeGroupsCategoryDetails
      .filter(
        (gradeDetails: any) => gradeDetails.gradeGroup == gradeGrp.value
      )[0]
      .gradeCategoryList.map((gradeCategory: any) => {
        return {
          displayName: gradeCategory,
          modelValue: gradeCategory,
        };
      });
  }

  public addGrade() {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData == undefined) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: 'Please Enter Required Fields',
        }
      );
    } else {
      this.createGradeResponseBody(formData);
      this.saveGrade();
      return false;
    }
  }

  public createGradeResponseBody(formData) {
    this.responseBody = this.sharedService.mapFormObjectToReqBody(
      formData,
      this.createGrade.getCreateGradeForm()
    );
    var gradeSpecificDetails = this.AllGradeDetails.filter(
      (ele: any) => ele.gradeGroup == this.responseBody.gradeGroup
    )[0];
    var creatProductList = this.responseBody.selectedProductIds.map((y: any) => {
      return this.formObject.formFields.filter(
        (item: any) => item.ref_key == 'selectedProductIds'
      )[0].list.filter((isd: any) => isd.modelValue == y)[0]
    })
    var selectedProductIds = creatProductList.map((x: any) => {
      return {
        "materialDesc": x.displayName,
        "materialId": x.modelValue,
      }
    })
    this.responseBody.selectedProductIds = selectedProductIds
    this.responseBody.gradeName = this.responseBody.gradeName;
    this.responseBody.materialGrade = gradeSpecificDetails.materialGrade;
    this.responseBody.gradeCategorys = gradeSpecificDetails.gradeCategorys;
    this.responseBody.status = gradeSpecificDetails.status;
    this.responseBody.userId = this.sharedService.loggedInUserDetails.userId;
    this.responseBody.gradeCategorys.gradeCategoryList = this.formObject.formFields
      .filter((item: any) => item.ref_key == 'gradeCategory')[0]
      .list.map((ele: any) => ele.modelValue);
    return this.responseBody;
  }

  public refreshPage() {
    this.getAllGrades();
    // this.getGradeGroupsCategory();
    // this.getAllProductIds();
    this.reset();
  }

  public saveGrade() {

    this.masterService
      .createGrade(this.responseBody)
      .subscribe((Response: any) => {
        if (Response != null) {
          this.sharedService.displayToastrMessage(
            this.sharedService.toastType.Success, {
              message: ` ${Response.gradeDesc} Grade Added successfully`,
            }
          );
          this.refreshPage();
        }
      });
  }

  public reset() {
    //this.jswComponent.resetForm(this.formObject);
    this.actionAddNewGrade = true;
    this.actionUpgradeGrade = false;
    this.formObject.formFields.filter((item: any) => item.ref_key == 'psnNo')[0].disabled = false;
    this.formObject.formFields.filter((item: any) => item.ref_key == 'gradeName')[0].disabled = false;
    this.formObject.formFields.filter((item: any) => item.ref_key == 'gradeDesc')[0].disabled = false;
    this.dbGradeProps = [];
    this.selectedGradeProperties = undefined;
  }

  public updateGrade() {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    var getSelectedProdIds = this.formObject.formFields.filter(
      (item: any) => item.ref_key == 'selectedProductIds'
    )[0].selectedItems;
    var creatProductList = getSelectedProdIds.map((y: any) => {
      return this.formObject.formFields.filter(
        (item: any) => item.ref_key == 'selectedProductIds'
      )[0].list.filter((isd: any) => isd.modelValue == y)[0]
    })
    var selectedProductIds = creatProductList.map((x: any) => {
      return {
        "materialDesc": x.displayName,
        "materialId": x.modelValue,
      }
    })
    formData.selectedProductIds = selectedProductIds
    this.masterService
      .updateGrade(this.createGradeResponseBody(formData))
      .subscribe((Response) => {
        if (Response != null) {
          this.sharedService.displayToastrMessage(
            this.sharedService.toastType.Success, {
              message: ` ${Response.gradeDesc} Grade updated successfully`,
            }
          );
          this.actionAddNewGrade = true;
          this.actionUpgradeGrade = false;
          this.refreshPage();
        }
      });
  }


  public getRowDetails(event) {
    this.selectedGradeProperties = event
    let selectedProducts = this.formObject.formFields.filter(
      (ele: any) => ele.ref_key == 'selectedProductIds'
    )[0];
    let productIds = event.selectedProductIds.map((x: any) => {
      var getID = this.formObject.formFields.filter(
        (ele: any) => ele.ref_key == 'selectedProductIds'
      )[0].list.filter((y: any) => y.displayName == x)[0]
      return getID.modelValue
    });

    this.masterService.gradeName = event.gradeName;
    this.masterService.productsIds = productIds;
    this.masterService.psnNo = event.psnNo;
    this.masterService.getGradeProperties({
      "gradeName": event.gradeName,
      "productsIds": productIds,
      "psnNo": event.psnNo
    }).subscribe(Response => {
      if (Response) {
        this.isResPonseRecieved = true;
        this.activeGradeItem = this.gradeMenuItems[0]
        this.dbGradeProps = Response;
        this.masterService.setdbGradeProps(Response)
      }
    })
  }

  public editRecord(event) {

    this.actionAddNewGrade = false;
    this.actionUpgradeGrade = true;
    this.sharedService.mapResponseToFormObj(this.formObject.formFields, event);
    var defaultgroup = {
      value: event.gradeGroup,
    };
    this.getGradeGroup(defaultgroup);
    this.formObject.formFields.filter(
      (item: any) => item.ref_key == 'gradeCategory'
    )[0].value = event.gradeCategorys.gradeCategory;
    let selectedProducts = this.formObject.formFields.filter(
      (ele: any) => ele.ref_key == 'selectedProductIds'
    )[0];
    selectedProducts.selectedItems = event.selectedProductIds.map((x: any) => {
      var getID = this.formObject.formFields.filter(
        (ele: any) => ele.ref_key == 'selectedProductIds'
      )[0].list.filter((y: any) => y.displayName == x)[0]
      return getID.modelValue
    });
    this.formObject.formFields.filter((item: any) => item.ref_key == 'psnNo')[0].disabled = true;
    this.formObject.formFields.filter((item: any) => item.ref_key == 'gradeName')[0].disabled = true;
    this.formObject.formFields.filter((item: any) => item.ref_key == 'gradeDesc')[0].disabled = true;
  }

  public deleteUser(event) {
    this.masterService
      .deleteGrade(event.gradeName, 'A001')
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success, {
            message: `Grade Deleted successfully`,
          }
        );
        this.getAllGrades();
        this.getGradeGroupsCategory();
        this.getAllProductIds();
      });
  }


  public search(event){
  //   this.mylookupservice.getResults(event.query).then(data => {
  //     this.results = data;
  // });
  }

  public getFormRef(event){
    this.formRef = event;
  }

  public getTableRef(event){
    this.tableRef = event;
  }
  public filterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }


}
