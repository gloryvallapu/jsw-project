import { Component, OnInit, Input } from '@angular/core';
import {
  ComponentType
} from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { MasterService } from '../../../../services/master.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';

@Component({
  selector: 'app-mech-property',
  templateUrl: './mech-property.component.html',
  styleUrls: ['./mech-property.component.css']
})
export class MechPropertyComponent implements OnInit {
  @Input() public dbMechProperties: any;
  @Input() public gradeProperties: any
  public viewType = ComponentType;
  columns: { field: string; header: string; columnType: ColumnType; width: string; sortFieldName: string; }[];
  globalFilterArray: string[];
  tableType: string;
  public mechProperties: any = []
  @Input() public screenStructure: any = [];
  public isSaveBtn: boolean;
  // new changes
  public dummyValues: any = [];
  public frozenCols: any =[];
  public firstRow: any = [{
    field: 'productName',
    header: 'Product Name',
    rowSpan: 1,
    colSpan: 0
  },];
  public secRow: any = [];
  ndtProperties: any[];
  public refResponse: any;
  // end of end changes
  constructor(public sharedService: SharedService, public masterService: MasterService) { }
  public createHeaderName(string) {
    let str = string.split("_");
    let header = (str[0].split('V')[0])
    return header.charAt(0).toUpperCase() + header.slice(1)

  }

  public generateParentCols(obj) {
    this.firstRow = [{
      field: 'productName',
      header: 'Product Name',
      rowSpan: 1,
      colSpan: 0
    },];
    this.secRow = []
    obj.forEach(element => {
      if (element.displayType == 'drp' && element.validationType == 'Equal_to') {
        this.firstRow.push({
          field: element.propDescription,
          header: element.propDescription,
          rowSpan: 1,
          colSpan: 1
        })
      }

      if (element.displayType == 'drp' && element.validationType == 'Range') {
        this.firstRow.push({
          field: element.propDescription,
          header: element.propDescription,
          rowSpan: 1,
          colSpan: 2
        })
      }

      if (element.displayType == 'txt' && element.validationType == 'Equal_to') {
        this.firstRow.push({
          field: element.propDescription,
          header: element.propDescription,
          rowSpan: 1,
          colSpan: 1
        })
      }

      if (element.displayType == 'txt' && element.validationType == 'Range') {
        this.firstRow.push({
          field: element.propDescription,
          header: element.propDescription,
          rowSpan: 1,
          colSpan: 2
        })
      }

    })
  };

  public generateChildCol(obj): any {
    this.secRow = Object.keys(obj).map((x: any) => {
      let drpDwoncheck = x.split(" ");
      if (x != "productName" && drpDwoncheck.length > 0 && drpDwoncheck[1] == 'drp') {
        // let listOfDrp;
        // x.dropDownValList.forEach(element => {
        //   let temp ={
        //     modelValue: element,
        //     displayName: element
        //   }
        //   listOfDrp.push(temp)
        // });
        return {
          field: x,
          header: "Equal to",
          rowSpan: null,
          colSpan: null,
          width: '120px',
          columnType: ColumnType.dropdown,
          list: [],
          isDisabled: true
        }
      } else if (x != "productName" && drpDwoncheck.length > 2 && drpDwoncheck[2] == 'eql') {
        return {
          field: x,
          header: "Equal to",
          rowSpan: null,
          colSpan: null,
          width: '120px',
          columnType: ColumnType.number,
          isDisabled: true
        }
      }
      else if (x == "productName") {
        return {
          field: x,
          header: this.createHeaderName(x),
          rowSpan: null,
          colSpan: null,
          width: '100px',
          columnType: ColumnType.string,
        }
      }
      else {
        return {
          field: x,
          header: this.createHeaderName(x),
          rowSpan: null,
          colSpan: null,
          width: '100px',
          columnType: ColumnType.number,
        }
      }


    })
    this.secRow[0].header = '';
    this.secRow[0].width = '120px';
    this.secRow.forEach(colElement => {
      if (colElement.columnType == 'Dropdown') {
        this.refResponse[0].propertyList.forEach(element => {
          if (colElement.field == (element.propName + ' drp')) {
            element.dropDownValList.forEach(item => {
              let temp = {
                modelValue: item.drpItemLovId,
                displayName: item.drpItemValue
              }
              colElement.list.push(temp)
            });

          }
        });
      }
    });
  }
  public columneGenerator(data) {
    let inputReponse = data[0];
    this.generateParentCols(inputReponse.propertyList);

    data.map((x: any) => {
      var ObjArray = { productName: x.productName };
      x.newData = x.propertyList.map((y: any): any => {
        if (y.displayType == 'drp' && y.validationType == "Equal_to") {
          return {
            [`${y.propName} drp`]: y.valText,

          }
        }

        if (y.displayType == 'drp' && y.validationType == "Range") {
          return {
            [`minVal ${y.propName}`]: y.minVal,
            [`maxVal ${y.propName}`]: y.maxVal,
          }
        }

        if (y.displayType == 'txt' && y.validationType == "Equal_to") {
          return { [`${y.propName}`]: y.maxVal, }
        }

        if (y.displayType == 'txt' && y.validationType == "Range") {
          return {
            [`minVal ${y.propName}`]: y.minVal,
            [`maxVal ${y.propName}`]: y.maxVal,
          }
        }



      });
      x.newData.forEach(element => {
        ObjArray = { ...ObjArray, ...element }

      });
      x.bindindData = ObjArray;
      return x
    });

    let reponseData = [];
    data.forEach(element => {
      reponseData.push(element.bindindData)
    });
    this.dummyValues = reponseData;
    this.generateChildCol(reponseData[0]);
  }

  public generateDynamicTable() {
    this.masterService.getdbGradeProps.subscribe(Response => {
      this.tableType = TableType.gridlines;
      if (Response['Mechanical']?.length > 0) {
        this.refResponse = Response['Mechanical']
        this.columns = [{
          field: 'productName',
          header: 'Product Type',
          columnType: ColumnType.uniqueKey,
          width: '20px',
          sortFieldName: ''
        },
        ];
        this.columneGenerator(Response['Mechanical'])

      } else {
        this.ndtProperties = [];
        this.columns = [];
      }
    })
  }

  ngOnInit(): void {
    this.generateDynamicTable()
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.gradeMasterSectionIds.Grade_Mech_Property.buttons.save, menuConstants.gradeMasterSectionIds.Grade_Mech_Property.id, this.screenStructure);
  }

  public createTableWithNewData(): any {
    this.masterService.getdbGradeProps.subscribe(Response => {
      this.tableType = TableType.normal;
      if (Response['Mechanical']?.length > 0) {
        this.columns = [{
          field: 'productName',
          header: 'Product Type',
          columnType: ColumnType.uniqueKey,
          width: '20px',
          sortFieldName: ''
        },
        ];
        var dummyResponce = Response['Mechanical']
        var creatDynamicCol = dummyResponce.map((x: any) => {
          return Object.keys(x.propertyQuantity)
        })
        var newObject = creatDynamicCol[0].map((neC: any) => {
          return {
            field: neC,
            header: neC,
            columnType: ColumnType.text,
            width: '20px',
            sortFieldName: ''
          }
        })
        this.globalFilterArray = this.columns.map(element => {
          return element.field;
        });

        this.columns = this.columns.concat(newObject);
        var indexOf = this.columns.findIndex((x: any) => x.header == 'productName');
        if (indexOf != -1) {
          this.columns.splice(indexOf, 1);
        }
        var newData = dummyResponce.map((x: any) => {
          x.propertyQuantity.productName = x.productName
          return x.propertyQuantity
        })
        this.mechProperties = newData;

      } else {
        this.columns = [];
        this.mechProperties = []
      }
    })
  }

  // public saveTableData() {
  //   var materialIdList = this.gradeProperties.materialGrade.map((x: any) => {
  //     return x.material
  //   })
  //   var final = this.dummyValues.map((x: any) => {
  //     var id = materialIdList.filter((y: any) => y.materialDesc == x.productName)[0];
  //     x.gradeName = this.gradeProperties.gradeName
  //     x.materialId = id.materialId
  //     var allKeys = Object.keys(x);
  //     let values = ["gradeName", "materialId", "productName"];
  //     allKeys = allKeys.filter(item => !values.includes(item));
  //     let data = allKeys.map((x: any) => {
  //       return x.split("_")[0]
  //     })
  //     let uniqueChars = data.filter((c, index) => {
  //       return data.indexOf(c) === index;
  //     });
  //     x.actulalProps = uniqueChars.map((prop: any) => {
  //       return {
  //         propName: prop,
  //         gradeName: this.gradeProperties.gradeName,
  //         materialId: id.materialId,
  //         minVal: x[`${prop}`],
  //         maxVal: x[`${prop}`],

  //       }
  //     })
  //     return x
  //   })
  //   let ResponceObject = final.map((x: any) => {
  //     return x.actulalProps
  //   })
  //   let checkResponceCalls = ResponceObject.map((x: any) => {
  //    return this.masterService.updatePropertyValues(x).subscribe(Response => {
  //     this.sharedService.displayToastrMessage(
  //       this.sharedService.toastType.Success, {
  //         message: `Mech properties For Grade ID No. ${Response[0].gradeName} and Product ID No. ${Response[0].materialId}  Updated successfully`,
  //       }
  //     );
  //       return Response
  //     })
  //   })

  // }

  public saveTableData() {
    var materialIdList = this.gradeProperties.materialGrade.map((x: any) => {
      return x.material
    })
    var ittArray = []
    var final = [];
    this.dummyValues.map((x: any) => {
      var id = materialIdList.filter((y: any) => y.materialDesc == x.productName)[0];
      var arr = [];
      Object.keys(x).forEach((key) => {
        arr.push(

          {
            propName: x['productName'],
            gradeName: this.gradeProperties.gradeName,
            materialId: id.materialId,
            [key]: x[key],

          });
      });
      ittArray.push(arr)
    })
    this.dummyValues.map((x: any) => {
      var id = materialIdList.filter((y: any) => y.materialDesc == x.productName)[0];
      this.refResponse[0].propertyList.forEach(listItem => {
        Object.keys(x).forEach((key) => {
          let drpDwoncheck = key.split(" ");
          if (listItem.propName == key && listItem.displayType == 'txt') {
            var reqTxtObj = {
              "drpLovVal": '',
              "gradeName": this.gradeProperties.gradeName,
              "materialId": id.materialId,
              "maxVal": x[key],
              "minVal": '',
              "modifiedBy": this.sharedService.loggedInUserDetails.userId,
              "modifiedDate": new Date(),
              "propName": listItem.propName,
              "displayType": listItem.displayType,
              "validationType": listItem.validationType
            }
            final.push(reqTxtObj)
          }
          else if (drpDwoncheck[1] == 'drp' && listItem.displayType == 'drp' && (listItem.propName + ' drp')  == key) {
            // let drpVal = null;
            // if (x[key].length) {
            //   drpVal = x[key][0].drpItemLovId
            // }
            var reqDrpObj = {
              "drpLovVal":  x[key],
              "gradeName": this.gradeProperties.gradeName,
              "materialId": id.materialId,
              "maxVal": '',
              "minVal": '',
              "modifiedBy": this.sharedService.loggedInUserDetails.userId,
              "modifiedDate": new Date(),
              "propName": listItem.propName,
              "displayType": listItem.displayType,
              "validationType": listItem.validationType
            }
            final.push(reqDrpObj)
          }
          else if (drpDwoncheck[0] == 'minVal' && drpDwoncheck[1] == listItem.propName && listItem.displayType == 'txt'
            && listItem.validationType == 'Range') {

            var reqMinObj = {
              "drpLovVal": '',
              "gradeName": this.gradeProperties.gradeName,
              "materialId": id.materialId,
              "maxVal": '',
              "minVal": x[key],
              "modifiedBy": this.sharedService.loggedInUserDetails.userId,
              "modifiedDate": new Date(),
              "propName": listItem.propName,
              "displayType": listItem.displayType,
              "validationType": listItem.validationType
            }
            final.push(reqMinObj)
          }

        })
      })
    })


    final.forEach(fele => {
      this.dummyValues.map((x: any) => {
        var id = materialIdList.filter((y: any) => y.materialDesc == x.productName)[0];
        Object.keys(x).forEach((key) => {
          let drpDwoncheck = key.split(" ");
          if (drpDwoncheck[0] == 'maxVal' && drpDwoncheck[1] == fele.propName && fele.displayType == 'txt'
            && fele.validationType == 'Range' && fele.materialId == id.materialId) {
            fele.maxVal = x[key];
          }

        })
      })
    })
    this.masterService.updatePropertyValues(final).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
        message: `Metallurgy properties For Grade ID No. ${Response[0].gradeName} and Product ID No. ${Response[0].materialId}  Updated successfully`,
      }
      );
      this.setData();

    })
  }
  public setData(){
    this.masterService.getGradeProperties({
      "gradeName": this.masterService.gradeName,
      "productsIds": this.masterService.productsIds,
      "psnNo": this.masterService.psnNo
    }).subscribe(Response => {
      if (Response) {
        this.masterService.setdbGradeProps(Response)
        this.generateDynamicTable();
      }
    })
  }

}
