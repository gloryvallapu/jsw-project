import {
  Component,
  OnInit,
  Input
} from '@angular/core';
import {
  ComponentType
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  TableType,
  ColumnType,
  LovCodes
} from 'src/assets/enums/common-enum';
import {
  MasterService
} from '../../../../services/master.service';

@Component({
  selector: 'app-grade-chemistry',
  templateUrl: './grade-chemistry.component.html',
  styleUrls: ['./grade-chemistry.component.css']
})
export class GradeChemistryComponent implements OnInit {
  public isChemistryDetailsExist:boolean = false;
  @Input() public dbChemProperties: any
  @Input() public gradeProperties: any
  public viewType = ComponentType;
  columns: {
    field: string;header: string;columnType: ColumnType;width: string;sortFieldName: string;
  } [];
  globalFilterArray: string[];
  tableType: string;
  public chemistryDetails: any = []

     // new changes
     public dummyValues:any = [];
     public firstRow: any = [
    //    {
    //    field: 'productName',
    //    header: 'Product Name',
    //    rowSpan: 1,
    //    colSpan: 0
    //  },
    ];
     public secRow:any = [];
    ndtProperties: any[];
     // end of end changes
    constructor(public sharedService:SharedService,public masterService:MasterService) { }
    public createHeaderName(string){
      let str = string.split("_");
      let header = (str[0].split('V')[0])
      return header.charAt(0).toUpperCase() + header.slice(1)

    }

    public generateParentCols(obj){
      this.firstRow = [];
      this.secRow = []
      obj.forEach(element => {
      if(element.displayType == 'drp' && element.validationType =='Equal_to'){
        this.firstRow.push({
          field: element.propName,
          header: element.propName,
          rowSpan: 1,
          colSpan:1
            })
      }

      if(element.displayType == 'drp' && element.validationType =='Range'){
        this.firstRow.push({
          field: element.propName,
          header: element.propName,
          rowSpan: 1,
          colSpan: 2
            })
      }

      if(element.displayType == 'txt' && element.validationType =='Equal_to'){
        this.firstRow.push({
          field: element.propName,
          header: element.propName,
          rowSpan: 1,
          colSpan: 1
            })
      }

      if(element.displayType == 'txt' && element.validationType =='Range'){
        this.firstRow.push({
          field: element.propName,
          header: element.propName,
          rowSpan: 1,
          colSpan: 2
            })
      }

       })
    };

    public generateChildCol(obj):any{
    this.secRow=  Object.keys(obj).map((x:any)=>{
      let drpDwoncheck = x.split(" ");
      if(x != "productName" && drpDwoncheck.length >2 && drpDwoncheck[2]=='drp'){
        return {
          field: x,
          header: "Equal to",
          rowSpan: null,
          colSpan: null,
          width:'120px',
          columnType:ColumnType.dropdown,
          list:[{modelValue:1,displayName:1}],
          isDisabled:true
        }
      } else if(x != "productName" && drpDwoncheck.length >2 && drpDwoncheck[2]=='eql'){
        return {
          field: x,
          header: "Equal to",
          rowSpan: null,
          colSpan: null,
          width:'120px',
          columnType:ColumnType.string,
          isDisabled:true
        }
      }
      // else if(x == "productName"){
      //   return {
      //     field: x,
      //     header: this.createHeaderName(x),
      //     rowSpan: null,
      //     colSpan: null,
      //     width:'100px',
      //     columnType:ColumnType.string,
      //   }
      // }
      else{
        return {
          field: x,
          header: this.createHeaderName(x),
          rowSpan: null,
          colSpan: null,
          width:'100px',
          columnType:ColumnType.string,
        }
      }


     })
    //  this.secRow[0].header = '';
    //  this.secRow[0].width = '120px';
    }
    public columneGenerator(data){
      let inputReponse = data[0];
      this.generateParentCols(inputReponse.propertyList);

      data.map((x:any)=>{
        //var ObjArray= {productName:x.productName};
        var ObjArray= {};
        x.newData = x.propertyList.map((y:any):any=>{
          if(y.displayType == 'drp' && y.validationType == "Equal_to")
          {
           return {[`minVal ${y.propName} drp`] : y.minVal, }
          }

          if(y.displayType == 'drp' && y.validationType == "Range")
          {
            return {
              [`minVal ${y.propName}`] : y.minVal,
              [`maxVal ${y.propName}`] : y.maxVal,
              }
          }

          if(y.displayType == 'txt' && y.validationType == "Equal_to"){
            return {[`maxVal ${y.propName} eql`] : y.minVal, }
          }

          if(y.displayType == 'txt' && y.validationType == "Range"){
            return {
              [`minVal ${y.propName}`] : y.minVal,
              [`maxVal ${y.propName}`] : y.maxVal,
              }
          }


        });
       x.newData.forEach(element => {
        ObjArray = {...ObjArray,...element}

        });
        x.bindindData = ObjArray;
        return x
      });

      let reponseData = [];
      data.forEach(element => {
        reponseData.push(element.bindindData)
      });
      this.dummyValues = reponseData;
      this.generateChildCol(reponseData[0]);
    }

    public generateDynamicTable(){
      this.masterService.getdbGradeProps.subscribe (Response =>{
        this.tableType = TableType.gridlines;
        if(Response['Chemistry']?.length > 0){
          this.columns = [
          //   {
          //   field: 'productName',
          //   header: 'Product Type',
          //   columnType: ColumnType.uniqueKey,
          //   width: '20px',
          //   sortFieldName: ''
          // },
        ];
        this.columneGenerator(Response['Chemistry'])

        }else{
          this.ndtProperties = [];
          this.columns = [];
        }
      })
    }
  public createTableWithNewData():any{
    this.masterService.getdbGradeProps.subscribe(Response =>{
      let ChemistryProps =Response['Chemistry']?.filter((x:any)=> Object.keys(x.propertyQuantity)?.length > 0);
      if (ChemistryProps?.length > 0){
        this.tableType = TableType.normal;
      this.columns = [];
      var dummyResponce = ChemistryProps
      var creatDynamicCol = dummyResponce.map((x: any) => {
        return Object.keys(x.propertyQuantity)
      })
      var newObject = creatDynamicCol[0].map((neC: any) => {
        return {
          field: neC,
          header: neC,
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: ''
        }
      })
      this.globalFilterArray = this.columns.map(element => {
        return element.field;
      });
      this.columns = this.columns.concat(newObject);
      var indexOf = this.columns.findIndex((x:any)=> x.header == 'productName');
      if(indexOf != -1){
        this.columns.splice(indexOf, 1);
      }
      var newData = dummyResponce.map((x: any) => {
        x.propertyQuantity.productName = x.productName
        return x.propertyQuantity
      })
      this.chemistryDetails = newData
      }else{
        this.chemistryDetails = [];
        this.columns = []
      }

    })
  }

  ngOnInit(): void {
    this.generateDynamicTable()

  }



}
