import { ComponentFixture, TestBed } from '@angular/core/testing';
import {Component,OnInit,Input} from '@angular/core';
import {ComponentType} from 'projects/jsw-core/src/lib/enum/common-enum';
import {SharedService} from 'src/app/shared/services/shared.service';
import {TableType,ColumnType,LovCodes} from 'src/assets/enums/common-enum';
import {MasterService} from '../../../../services/master.service';
import { NdtPropertyComponent } from './ndt-property.component';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { GradeMasterForms } from '../../../../form-objects/master-form-objects';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { MasterEndPoints } from '../../../../form-objects/master-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('NdtPropertyComponent', () => {
  let component: NdtPropertyComponent;
  let fixture: ComponentFixture<NdtPropertyComponent>;
  let sharedService: SharedService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let masterService: MasterService
  let commonService: CommonApiService
  let formObjects : GradeMasterForms

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NdtPropertyComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        MasterService,
        CommonApiService,
        JswFormComponent,
        MasterEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        GradeMasterForms,
        { provide: ToastrService, useValue: toastrService }, 
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NdtPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
