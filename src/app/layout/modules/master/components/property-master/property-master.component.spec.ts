import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { PropertyMasterForms } from '../../form-objects/master-form-objects';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswFormComponent } from 'jsw-core';
import { JswCoreService } from 'jsw-core';
import { MasterService } from '../../services/master.service';
import {Property,PropertyForm,} from '../../interfaces/property-master-interface';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { PropertyMasterComponent } from './property-master.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { MasterEndPoints } from '../../form-objects/master-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { IndividualConfig, ToastrService } from 'ngx-toastr';

describe('PropertyMasterComponent', () => {
  let component: PropertyMasterComponent;
  let fixture: ComponentFixture<PropertyMasterComponent>;
  let sharedService: SharedService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let masterService: MasterService
  let commonService: CommonApiService
  let formObjects : PropertyMasterForms

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PropertyMasterComponent ],
      imports :[
       FormsModule,
       ReactiveFormsModule,
       HttpClientModule,
       SharedModule
     ],
     providers:[
       JswCoreService,
       MasterService,
       CommonApiService,
       JswFormComponent,
       MasterEndPoints,
       API_Constants,
       CommonAPIEndPoints,
       PropertyMasterForms,
       { provide: ToastrService, useValue: toastrService }, 
     ]
   })
   .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
