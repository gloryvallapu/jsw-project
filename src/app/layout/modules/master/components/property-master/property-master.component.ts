import { Component, OnInit, ViewChild } from '@angular/core';
import { PropertyMasterForms } from '../../form-objects/master-form-objects';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswFormComponent } from 'jsw-core';
import { JswCoreService } from 'jsw-core';
import { MasterService } from '../../services/master.service';
import {
  Property,
  PropertyForm,
} from '../../interfaces/property-master-interface';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { element } from 'protractor';
import { NgForm } from '@angular/forms';
import { Table } from 'primeng/table';
@Component({
  selector: 'app-property-master',
  templateUrl: './property-master.component.html',
  styleUrls: ['./property-master.component.css'],
})
export class PropertyMasterComponent implements OnInit {
  formObject: any;
  tableType: any;
  viewType = ComponentType;
  columns: ITableHeader[] = [];
  propertyList: any = [];
  lovList: any = [];
  globalFilterArray: string[];
  isEditUser: boolean = false;
  selectedRecordForEdit: any;
  propDataTypeList: any = [];
  propertyForm: PropertyForm;
  globalValidationType: any = [];
  modifiedrecord:any;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isEditBtn: boolean = false;
  public isAddBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isUpdateBtn: boolean = false;
  public isCancelBtn: boolean = false;
  sortSqList: any;
  displaylist: any;
  public formRef: NgForm;
  public tableRef: any;
  public propTypeList: any;
  @ViewChild('dt') table: Table;
  constructor(
    public propertyMasterForms: PropertyMasterForms,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public masterService: MasterService,
    public commonService: CommonApiService,
    public authService: AuthService
  ) {
    this.propertyForm = new PropertyForm();
  }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Master)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.masterMgmtScreenIds.Property_Master.id)[0];
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.propertyMasterSectionIds.List.buttons.edit, menuConstants.propertyMasterSectionIds.List.id, this.screenStructure);
    this.isAddBtn = this.sharedService.isButtonVisible(true, menuConstants.propertyMasterSectionIds.Add_New_Record.buttons.add, menuConstants.propertyMasterSectionIds.Add_New_Record.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.propertyMasterSectionIds.Add_New_Record.buttons.reset, menuConstants.propertyMasterSectionIds.Add_New_Record.id, this.screenStructure);
    this.isUpdateBtn = this.sharedService.isButtonVisible(true, menuConstants.propertyMasterSectionIds.Edit_Record.buttons.update, menuConstants.propertyMasterSectionIds.Edit_Record.id, this.screenStructure);
    this.isCancelBtn = this.sharedService.isButtonVisible(true, menuConstants.propertyMasterSectionIds.Edit_Record.buttons.cancel, menuConstants.propertyMasterSectionIds.Edit_Record.id, this.screenStructure);
    this.getAllProperties();
    this.getPropertyType();
    this.getSortSq();
    this.getLovs(LovCodes.propDataType, 'propDataType');
    this.getLovs(LovCodes.validationType, 'validationType');
    this.getdiplaylist();
    this.formObject = JSON.parse(JSON.stringify(this.propertyMasterForms.addProperty));

    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.columns = [
      {
        field: 'propName',
        header: 'Property Name',
        columnType: ColumnType.uniqueKey,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'propDescription',
        header: 'Property Description',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'propDataType',
        header: 'Data Type',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'propType',
        header: 'Property Type',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'testType',
        header: 'Test Type',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'sortSeq',
        header: 'Sort Seq',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'validationType',
        header: 'Validation Type',
        columnType: ColumnType.number,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'sapCode',
        header: 'SAP Code',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'displayType',
        header: 'Display Type',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'action',
        header: 'Action',
        columnType: ColumnType.action,
        width: '80px',
        sortFieldName: '',
      },
    ];
    this.globalFilterArray = this.columns.map((element) => {
      return element.field;
    });
  }

  public editRecord(data: any){
    //debugger
    this.modifiedrecord=data;
    this.isEditUser = true;
    this.getSortSq();
    this.getdiplaylist();
    this.formObject.formFields.filter((ele: any) => ele.ref_key == 'propName')[0].disabled = true; 
    if(data.validationTypeShortCode=='Range') 
    {
    this.displaylist.forEach(element => {
      if(element.modelValue=='txt')
      {
        this.formObject.formFields.filter((ele: any) => ele.ref_key == 'displayType')[0].list=[element];
      }
    });;
    }
    else
    this.formObject.formFields.filter((ele: any) => ele.ref_key == 'displayType')[0].list=this.displaylist;
    if(data.displayType=='Dropdown')
    {
      this.formObject.formFields.filter( (ele: any) => ele.ref_key == 'lovId')[0].isVisible = true;
      this.getAllLOV();
    }
    else
    {
      this.formObject.formFields.filter( (ele: any) => ele.ref_key == 'lovId')[0].isVisible = false;
    }
    this.modifiedrecord.propType=data.propTypeShortCode;
    this.modifiedrecord.testType=data.testTypeShortCode;
    this.modifiedrecord.propDataType=data.propDataTypeShortCode;
    this.modifiedrecord.validationType=data.validationTypeShortCode;
    this.modifiedrecord.displayType=data.displayTypeShortCode;
    let TestTypeLists = this.formObject.formFields.filter(ele => ele.ref_key == 'propType')[0].list.filter((x: any) => x.modelValue == data.propTypeShortCode)[0]
    this.formObject.formFields.filter(ele => ele.ref_key == 'testType')[0].list = TestTypeLists.TestTypeList.map((x: any) => {
      return {
        displayName: x.propTestDesc,
        modelValue: x.propTestShortCode
      }
    });
    this.sharedService.mapResponseToFormObj(this.formObject.formFields, this.modifiedrecord);
    this.getAllProperties();
  }
  public resetFilter() {
    //this.jswComponent.resetForm(this.formObject);
    this.isEditUser = false;
    this.formObject.formFields.filter( (ele: any) => ele.ref_key == 'propName' )[0].disabled = false;
    this.formObject.formFields.filter((ele: any) => ele.ref_key == 'propName')[0].isVisible = true;
    this.formObject.formFields.filter((ele: any) => ele.ref_key == 'lovId')[0].isVisible = false;
  }

  public getAllProperties() {
    this.masterService.getAllProperties().subscribe((data) => {
      this.propertyList = data;
      this.propTypeList = this.sharedService.getUniqueRecords(data.map((x: any) => {
        return { displayName: x.propType, modelValue: x.propType };
      }));
    })
  }
  public getdiplaylist() {
    this.commonService.getLovs(LovCodes.displayType).subscribe((data) => {
      this.displaylist = data.map((x: any) => {
        return { 
          displayName: x.valueDescription,
          modelValue: x.valueShortCode };
    });
    })
  }
  public getAllLOV() {
    this.masterService.getAllDistinctLOV().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'lovId'
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.lovDescription,
          modelValue: x.lovId,
        };
      });
    });
  }
  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
    this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }

  public getPropertyCriteriaDetails(operationType: string) {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
   if(formData && formData.length){
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.propertyForm.getAddPropertyForm());
   let reqBody : Property = {
    propName : formObj.propName,
    propDataType : formObj.propDataType,
    propDescription : formObj.propDescription,
    propType : formObj.propType,
    sapCode : formObj.sapCode,
    sortSeq : formObj.sortSeq,
    status : 'Active',
    testType : formObj.testType,
    validationType : formObj.validationType,
    displayType:formObj.displayType,
    lovId: formObj.displayType== 'drp'?formObj.lovId: null,
    modifiedBy: this.sharedService.loggedInUserDetails.userId,
   };
   if(operationType == 'add'){
    this.createUser(reqBody);
   }else if(operationType == 'update'){
    this.updateUser(reqBody);
   }
 }
 }
 public dropdownChangeEvent(event){
  if (event.ref_key == 'propType' && event.value != null) {
    let TestTypeLists = this.formObject.formFields.filter(ele => ele.ref_key == 'propType')[0].list.filter((x: any) => x.modelValue == event.value)[0]
    this.formObject.formFields.filter(ele => ele.ref_key == 'testType')[0].list = TestTypeLists.TestTypeList.map((x: any) => {
      return {
        displayName: x.propTestDesc,
        modelValue: x.propTestShortCode
      }
    });
    this.sortSqList.forEach(ele => {
      if(ele.modelValue==event.value)
      {
        this.formObject.formFields.filter(ele => ele.ref_key == 'sortSeq')[0].value=ele.displayName; 
      }
    });
  }
  else if (event.ref_key == 'validationType' && event.value != null) {
      if(event.value=='Range') 
      {
       this.displaylist.forEach(element => {
          if(element.modelValue=='txt')
          {
            this.formObject.formFields.filter(ele => ele.ref_key == 'displayType')[0].list= [element];
          }
        });
        this.formObject.formFields.filter( (ele: any) => ele.ref_key == 'lovId')[0].isVisible = false;
        this.formObject.formFields.filter((ele: any) => ele.ref_key == 'lovId')[0].isRequired = false;
      }
      else
      this.formObject.formFields.filter(ele => ele.ref_key == 'displayType')[0].list=this.displaylist;
  }
 else if (event.ref_key == 'displayType' && event.value != null) {
   if(event.value=='drp')
   {
    this.formObject.formFields.filter((ele: any) => ele.ref_key == 'lovId')[0].isVisible = true;
    this.formObject.formFields.filter( (ele: any) => ele.ref_key == 'lovId')[0].isRequired = true;
    this.getAllLOV();
   }
   else
   {
    this.formObject.formFields.filter( (ele: any) => ele.ref_key == 'lovId' )[0].isVisible = false;
    this.formObject.formFields.filter( (ele: any) => ele.ref_key == 'lovId')[0].isRequired = false;
   }
}
else
{
  var validationFindIndex = this.formObject.formFields.findIndex((item) => item.ref_key == 'validationType');
  if(this.globalValidationType && this.globalValidationType.length==0){
    this.globalValidationType=this.formObject.formFields[validationFindIndex].list;
  }
  if(event.value=="Alphabetic" && this.globalValidationType && this.globalValidationType.length > 0){
    this.formObject.formFields[validationFindIndex].list=[JSON.parse(JSON.stringify( this.globalValidationType[0]))];
    this.formObject.formFields[validationFindIndex].selectedItems=[JSON.parse(JSON.stringify( this.globalValidationType[0]))];
  }
  else
  {
    this.formObject.formFields[validationFindIndex].list=this.globalValidationType;
  }
 }
}

  public createUser(reqBody: any) {
    this.masterService.createProperty(reqBody).subscribe((data) => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        { message: 'Property created successfully!' }
      );
      this.resetFilter();
      this.getAllProperties();
    });
  }

 public updateUser(reqBody: any){
    this.masterService.updateProprety(reqBody).subscribe(data => {
    this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: "Property" + (data ? "-" + data.propName : '') + " updated successfully!"});
      this.resetFilter();
      this.getAllProperties();
    });
  }

  public deleteUser(record: any) {
    this.masterService.deleteProperty(record.propName).subscribe((data) => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        {
          message:
            'Property' +
            (record ? ' - ' + record.propName : '') +
            ' deleted successfully!',
        }
      );
      this.getAllProperties();
    });
  }
  public getPropertyType() {    
    this.masterService.getPropertyType().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'propType'
      )[0].list = data.map((x: any) => {
        return { displayName: x.propTypeDescription, modelValue: x.propTypeShortCode,TestTypeList:x.propTypeTestList };
      });
    });
  }
  getSortSq()
  {
    this.masterService.getPropertySortSequance().subscribe((data) => {
      this.sortSqList=data.map((x: any) => {
        return { displayName: x.nextSortSq, modelValue: x.propShortCode};
      });
    });

  }
  public getFormRef(event){
    this.formRef = event;
  }
  public getTableRef(event){
    this.tableRef = event;
  }
  public filterTable(event){
    this.table.filterGlobal(event.target.value, 'contains');
  }

}
