export enum Material {
  AsCastRounds = 'S_GRNDSW',
  AsCastRoundsFinal = 'S_RNDSF',
  Tmt = 'S_TMTBW',
  TmtFinal = 'S_TMTBF',
  PlainRounds = 'S_PRBW',
  PlainRoundsFinal = 'S_PRBF',
  Billet = 'S_GBLTW',
  BilletFinal = 'S_BLTF',
  Bloom = 'S_GBLMW',
  BloomFinal = 'S_BLMF',
  LpFlats = 'S_LPFW',
  LpFlatsFinal = 'S_LPFF',
  Slab = 'S_GSLBW',
  SlabFinal = 'S_SLBF'
}
