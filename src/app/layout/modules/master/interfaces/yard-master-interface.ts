export interface Yard{
  wcName: string,
  yardCode: string,
  yardName: string,
  sapLocCode: string,
  inTransitId: string,
  status: string,
  movementYards:MovementYards[],
}
export interface MovementYards {
  yardCode: string;
  yardName: string,
}


export class YardForm{
    public formObj = {
      wcName: '',
      yardCode: '',
      yardName: '',
      sapLocCode: '',
      inTransitId: '',
      status: '',
      movementYards:[],
    }
    public getyardForm(){
      return this.formObj;
    }
  }
  
