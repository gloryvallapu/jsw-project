export class PropertyForm{
  public formObj = {
    propDataType: '',
    propDescription: '',
    propName: '',
    propType: '',
    sapCode: '',
    sortSeq: '',
    status: '',
    testType: '',
    validationType: '',
    displayType:'',
    lovId:'',
  }
  public getAddPropertyForm(){
    return this.formObj;
  }
}

export interface Property{
  propDataType: string,
  propDescription: string,
  propName: string,
  propType: string,
  sapCode: string,
  sortSeq: string,
  status: string,
  testType: string,
  validationType: string,
  displayType:string,
  lovId:number,
  modifiedBy: string
}
