export class MasterEndPoints {
    //Material Master Screen API End Points
    public readonly getLovValues = '/lovValues';
    public readonly getAllMaterials = '/material';
    public readonly addNewMaterial = '/material';
    public readonly updateMaterial = '/material/updateMaterial';
    //End of Material Master Screen API End Points

    //Work Center Master API End Points
    public readonly getWorkCenterList = '/workCenters';
    public readonly getWCTypeList = '/workCenters/getWorkCenterTypes';
    public readonly addNewWorkCenter = '/workCenters/createWorkCenter';
    public readonly updateWorkCenter = '/workCenters/updateWorkCenter';
    //End of Work Center Master API End Points

    //Size Master API End Points
     public readonly getAllSizes = '/getAllSizes';
     public readonly addNewProductSize = '/addProductSize';
     public readonly updateSize = '/updateSize';
    //End of Size Master API End Points

    //WC, Product & Size Mapping API End Points
    public readonly getMappingList = '/WcMaterialSize';
    public readonly getProductSizes = '/getAllSizesBasedOnPrd';
    public readonly addNewMapping = '/WcMaterialSize';
    public readonly updateMapping = '/WcMaterialSize';
    //

    // Property Master API End Points


    // End of Screen Master API End Points

    // Grade Master API End Point
    public readonly getAllGrades =  '/grades';
    public readonly getGradeGroupsCategory = '/grades/gradeGroupsCategory';
   // public readonly getAllProductIds = '/material';
    public readonly getAllProductIds ='/lovValues/Output_Product';
    public readonly createGrade = '/grades';
    public readonly updateGrade ='/grades';
    public readonly deleteGrade: '/grades';
    public readonly getGradeProps = '/gradeProp/getGradeProps';
    public readonly updatePropertyValues = '/gradeProp/updatePropertyValues'
    // End of Role Screen Master API Point

    // Start of Prperty Master API points

   public readonly getAllProperties = '/property/getAllProperties';
   public readonly updateProperty ='/property/updateProperty';
   public readonly createProperty = '/property/createProperty';
   public readonly deleteProperty  = '/property/deleteProperty';
   public readonly getPropertyType  = '/property/getPropTypeWiseTests';
   public readonly getAlldistinctLOV='/property/getAllDistinctLovs';
   public readonly getPropertySortSq  = '/property/getPropTypeWiseNextSortSq';

  //End od Property Master API points


  //Start of Gradewise density Master API end Points

   public readonly getAllGradeWiseDensity = '/gradeDensity/getAllGradeWiseDensity';
   public readonly getWCandPrdDesc = '/gradeDensity/getWCandPrdDesc';
   public readonly getWCGradeBasedProductSizeList = '/gradeDensity/getWCGradeBasedProductSizeList';
   public readonly getFilteredGradeDensity = '/gradeDensity/getFilteredGradeDensity';
   public readonly createGradeWiseDensity = '/gradeDensity/createGradeWiseDensity';
   public readonly updateGradeWiseDensity = '/gradeDensity/updateGradeWiseDensity';
   public readonly getallgrade='/gradeDensity/getallgrade';

  //End of Gradewise density Master API end points

  // Start of LOV master API end points
  public readonly FilterLOVdata='/lovScreen/getLovScreenHeaderData'; 
  public readonly getLOVMasterList='/lovScreen/getLovScreenValuesBasedOnLovId'; 
  public readonly getdependentLOVList='/lovScreen/getLovDependentData'; 
  public readonly updateLovMasterValues='/lovScreen/updateLovScreenValues'; 
  public readonly updateLovMasterHeaderValues='/lovScreen/updateLovScreenHeaderData'; 
  public readonly updateLovMasterMappingDetails='/lovScreen/updateLovScreenValueMappings'; 
  // End  of LOV master API end points
  // Start of LOV master API end points
  public readonly getallyardlist='/yardMaster';
  public readonly createYardMaster='/yardMaster/createYardMaster';
  public readonly updateYardMaster='/yardMaster/updateYardMaster';  
  
  // End  of LOV master API end points
 }
