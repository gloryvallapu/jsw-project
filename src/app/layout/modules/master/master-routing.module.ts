import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MasterComponent } from './master.component';
import { GradeMasterComponent } from './components/grade-master/grade-master.component';
import { PropertyMasterComponent } from './components/property-master/property-master.component';
import { MaterialMasterComponent } from './components/material-master/material-master.component';
import { GradewiseDensityComponent } from './components/gradewise-density-master/gradewise-density.component';
import { AuthGuardService } from 'src/app/shared/services/auth-guard.service';
import { moduleIds, masterMgmtScreenIds } from 'src/assets/constants/MENU/menu-list';
import { LOVMasterComponent } from './components/lov-master/lov-master.component';
import { YardmasterComponent } from './components/yardmaster/yardmaster.component';

const routes: Routes = [
  {
    path: '',
    component: MasterComponent,
    children: [
      {
        path: 'materialMaster',
        component: MaterialMasterComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.Master, screenId: masterMgmtScreenIds.Product_Attribute_Master.id }
      },
      {
        path: 'propertyMaster',
        component: PropertyMasterComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.Master, screenId: masterMgmtScreenIds.Property_Master.id }
      },
      {
        path: 'gradeMaster',
        component: GradeMasterComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.Master, screenId: masterMgmtScreenIds.Grade_Master.id }
      },
      {
        path : 'gradewiseDensityMaster',
        component:GradewiseDensityComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.Master, screenId: masterMgmtScreenIds.Gradewise_Density_Master.id }
      },
      {
        path : 'LovMaster',
        component:LOVMasterComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.Master, screenId: masterMgmtScreenIds.LOV_Master.id }
      },
      {
        path : 'YardMaster',
        component:YardmasterComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.Master, screenId: masterMgmtScreenIds.Yard_Master.id }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterRoutingModule {}
