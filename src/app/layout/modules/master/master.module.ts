import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterComponent } from './master.component';
import { MasterRoutingModule } from './master-routing.module';
import { JswCoreModule } from 'jsw-core';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialMasterComponent } from './components/material-master/material-master.component';
import { PropertyMasterComponent } from './components/property-master/property-master.component';
import { GradeMasterComponent } from './components/grade-master/grade-master.component';
import { MasterService } from './services/master.service';
import { MasterEndPoints } from './form-objects/master-api-endpoints';
import { GradeChemistryComponent } from './components/grade-master/components/grade-chemistry/grade-chemistry.component';
import { MechPropertyComponent } from './components/grade-master/components/mech-property/mech-property.component';
import { MetallurgyPropertyComponent } from './components/grade-master/components/metallurgy-property/metallurgy-property.component';
import { NdtPropertyComponent } from './components/grade-master/components/ndt-property/ndt-property.component';
import { GradeMasterForms,GradewiseDensityMasterForms,LOVMasterForms,PropertyMasterForms, YardMasterForms} from './form-objects/master-form-objects';
import { WorkCenterMasterComponent } from './components/material-master/sub-components/work-center-master/work-center-master.component';
import { SizeMasterComponent } from './components/material-master/sub-components/size-master/size-master.component';
import { WcSizeMappingComponent } from './components/material-master/sub-components/wc-size-mapping/wc-size-mapping.component';
import { GradewiseDensityComponent } from './components/gradewise-density-master/gradewise-density.component';
import {LOVMasterComponent } from './components/lov-master/lov-master.component';
import { YardmasterComponent } from './components/yardmaster/yardmaster.component';

@NgModule({
  declarations: [
    MasterComponent,
    MaterialMasterComponent,
    PropertyMasterComponent,
    GradeMasterComponent,
    GradeChemistryComponent,
    MechPropertyComponent,
    MetallurgyPropertyComponent,
    NdtPropertyComponent,
    WorkCenterMasterComponent,
    SizeMasterComponent,
    WcSizeMappingComponent,
    GradewiseDensityComponent,
    LOVMasterComponent,
    YardmasterComponent
  ],
  imports: [
    CommonModule,
   // JswCoreModule,
    MasterRoutingModule,
    SharedModule
  ],
  providers:[GradeMasterForms,MasterService,MasterEndPoints,PropertyMasterForms,GradewiseDensityMasterForms,LOVMasterForms,YardMasterForms]
})
export class MasterModule {}
