

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsComponent } from './reports.component';
import { ShowReportComponent } from './components/show-report/show-report.component';
import { AuthGuardService } from 'src/app/shared/services/auth-guard.service';
import { moduleIds } from 'src/assets/constants/MENU/menu-list';

const routes: Routes = [
    {
      path: '',
      component: ReportsComponent,
      children: [
        { path: '', pathMatch: 'full', redirectTo: 'checkReports' },
        { path: 'checkReports', 
          component: ShowReportComponent,
          canActivate:[AuthGuardService] ,
          data: {
            moduleId: moduleIds.Reports,
            screenId:moduleIds.Reports
          },
        },
      ]
    }
  ];

  @NgModule({
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [ RouterModule ]
  })
  export class ReportsRoutingModule { }
