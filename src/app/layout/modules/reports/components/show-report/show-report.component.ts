import { Component, OnInit, ViewChild, ɵEMPTY_ARRAY } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { TableType, ColumnType, LovCodes, ReportType, WorkCenter, Product, LOV } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ReportsService } from '../../services/reports.service';
import {
  DynamicReportsForm,
} from '../../form-objects/reports-form-objects';
import { saveAs } from 'file-saver';
import * as FileSaver from 'file-saver';
import { MasterService } from '../../../master/services/master.service';
import { BrmQaService } from '../../../brm-qa/services/brm-qa.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';

@Component({
  selector: 'app-show-report',
  templateUrl: './show-report.component.html',
  styleUrls: ['./show-report.component.css'],
})
export class ShowReportComponent implements OnInit {
// LP stockReport Declaration
public isLpSummaryReport:boolean = false;
public dummyValues = [];
public tempObject = {
  field: null,
  header: null,
  rowSpan: null,
  colSpan: null
}
public firstRow: any = [];
public secRow: any = [{
  field: 'grade',
  header: null,
  rowSpan: null,
  colSpan: null,
  width:'70px'
}];
public trialColumnHeads = []
// End Of LP Stock Report Declartion

  public reportType = ReportType;
  public selectReportObject: any;
  public reportList: any = [];
  public grantedreportList: any = [];
  public tableControlActions: any = [];
  public formObject: any;
  public viewType = ComponentType;
  public columnType = ColumnType;
  public columns: ITableHeader[] = [];
  public onDatecolumns: ITableHeader[] = [];
  public screenData: any = [];
  public cummulativeDataTableColumns: ITableHeader[] = [];
  public tableType: any;
  public tableData: any = [];
  public getReportName: any = null;
  public getReportTableName: any = null;
  public unitList: any = [];
  public gradeList: any = [];
  reportsRecordList: any = [];
  globalFilterArray: string[];
  columnList: any;
  columnName: any;
  selectedReport: any;
  public productList: any = [];
  public refProductList: any = [];
  public lpUnitList: any = [];
  public mechTestReportSelected = false;
  public userRights: any;
  public ReportName:string;
  //trial of mechtest report
  public secondColumns: ITableHeader[] = [];
  public requestBody = {
    fromDate: null,
    toDate: null,
    heatId: null,
  };
  public requestBodyBatch = {
    batchId: null,
  };
  public reqBodyGradeSize = {
    fromDate: null,
    toDate: null,
    grade: null,
  };
  public requestBodySo={
    soId:null,
    lineNo:null,
  }
  public reqBodyFurnaceSize = {
    fromDate: null,
    toDate: null,
    unit: null,
  };
  onDateTableData: any = [];
  cummulativeData: any = [];
  ShiftSizeProdData: any;
  shiftWiseProdReprt: boolean = false;
  todayDate: any;
  cummulativeDate: string;
  public screenStructure: any = [];
  public isPDfdwnld: boolean=false;
  public isRetrive: boolean=false;
  public isReset: boolean=false;
  public isExceldwnld: boolean=false;
  public isFilterSection: boolean=false;
  public isListSection: boolean=false;
  menuConstants: any;
  public maxDate = new Date();
  constructor(
    public dynamicReports: DynamicReportsForm,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public reportsService: ReportsService,
    public masterService: MasterService,
    public brmQaService: BrmQaService,
    public authService: AuthService,
  ) { }

  ngOnInit(): void {

    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getReportName = null;
    this.formObject = [];
    this.selectReportObject =JSON.parse(JSON.stringify(this.dynamicReports.createReports.selectReportObject));
    this.reportList = JSON.parse(JSON.stringify(this.dynamicReports.createReports.list));
    this.userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Reports)[0];
    this.userRights.subScreens.forEach(moduleEle => {
            let smIndex = this.dynamicReports.createReports.list.findIndex(ele => ele.ReportId == moduleEle.screenId);
            if(smIndex != -1){
              this.dynamicReports.createReports.list[smIndex].ReportAccess = true;
            }
          });
    this.reportList= this.dynamicReports.createReports.list;
    this.grantedreportList=this.reportList.filter((elex: any) => elex.ReportAccess == true);
    this.selectReportObject.formFields.filter((ele: any) => ele.ref_key == 'report')[0].list = this.grantedreportList;
  }

  // Start Common functionalities//
  public formValidationsResult(): any {
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();
    if (formData == undefined) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: `Please Enter Required Fields `,
      }
      );
    }
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.sharedService.getReqBody(this.formObject.formFields));
    return formObj
  };

  public getReport(event) {
    this.maxDate.setDate(this.maxDate.getDate());
    // this.maxDate.setFullYear(new Date().getFullYear());
    // this.maxDate.setMonth(new Date().getMonth());
    this.mechTestReportSelected = false;
    this.shiftWiseProdReprt = false;
    this.getReportName = event.value;
    // this.formObject = this.dynamicReports.createReports[event.value].filterCriteria;

    if (event.value != null) {
      this.reportsRecordList = [];
      this.getReportName = event.value;
      this.formObject = JSON.parse(JSON.stringify(this.dynamicReports.createReports[event.value].filterCriteria));
      this.formObject.formFields.forEach(element => {
        if(element.ref_key == 'toDate' || element.ref_key == 'fromDate'){
          element.maxDate = this.maxDate
        }
      });

      this.filterData();
      this.switchToSelectedReport(this.getReportName, event);
      this.AccessToSelectedReport(this.getReportName, event);
    } else {
      this.getReportName = null;
      this.formObject = [];
      this.getReportTableName = null;
    }
  };

  public switchToSelectedReport(report, event) {
    this.reportsRecordList = [];
    switch (report) {
      case this.reportType.Billet_Consumption_Report:
        this.generateBilletConspReport(event);
        break;
      
      case this.reportType.Batch_Detail_Report:
          this.generateBatchDetailReport(event);
          break;
      
      case this.reportType.So_Detail_Report:
            this.generateSoDetailReport(event);
            break;

      case this.reportType.Furnace_Residence_Report:
        this.genRatefurnaceResidenceReport(event);
        break;

      case this.reportType.SizeWise_Monthly_Report:
        this.genrateSizeWiseMonthlyReport();
        break;
      case this.reportType.HeatWise_Production_Report:
        this.generateHeatwiseProdReport(event);
        break;
      case this.reportType.LP_Stock_Report:
        this.genRateLpStockReport(event);
        break;
      case this.reportType.LP_Production_SummaryReport:
        this.generateLPProdSummaryReport(event);
        break;
      case this.reportType.Hot_Out_Pending_Report:
        this.generatHotOutPendingdReport(event);
        break;
      case this.reportType.LP_Production_SummaryReport:
        this.generateLPProdSummaryReport(event);
        break;
      case this.reportType.Lp_Production_Details_Report:
        this.generateLpProductionDetailsReport(event);
        break;
      case this.reportType.Mech_Test_Report:
        this.generateMechTestReport(event);
        break;
      case this.reportType.Lp_Rolling_Schedule:
        this.reportsRecordList = []
        this.genRateRollingScheduleReport(event);
        break;
      case this.reportType.Shift_Wise_Prod_Report:
        this.shiftWiseProdReprt = true;
        this.getShiftWseProdRptOnLoad();
        this.selectedReport = this.getReportName;
        this.columns = [];
        this.columns = this.dynamicReports.createReports[event.value].onDatecolumns;
        this.onDatecolumns = this.dynamicReports.createReports[event.value].onDatecolumns;
        this.cummulativeDataTableColumns = this.dynamicReports.createReports[event.value].cummulativeDataTableColumns;
        this.reportsRecordList = []
        break;
        case this.reportType.Billet_Stock_Report:
        this.genRateBilletStockReport(event);
        break;
      default: break;
    }

  };

  public getFilterReport() {
    this.columns = [];
    switch (this.selectedReport) {
      case this.reportType.Billet_Consumption_Report:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns;
        this.getBilletConsumptionReport();
        break;
      case this.reportType.Furnace_Residence_Report:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns;
        this.getFurnaceResidenceTimeReportDetails();
        break;
      case this.reportType.SizeWise_Monthly_Report:
        this.getGradeSizeWiseReportData();
        break;
      case this.reportType.HeatWise_Production_Report:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns;
        this.getheatWiseProductionReport();
        break;
      case this.reportType.Hot_Out_Pending_Report:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns;
        this.getHotOutPendingFilterWiseReport();
        break;
      case this.reportType.LP_Production_SummaryReport:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns;
        this.getLPProductionSummaryReport();
        break;
      case this.reportType.LP_Stock_Report:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns;
        this.getLPStockReport();
        break;
      case this.reportType.Lp_Production_Details_Report:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns
        this.getLpProductionReport();
        break;
      case this.reportType.Lp_Rolling_Schedule:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns
        this.getLPRollingReport();
        break;
      case this.reportType.Shift_Wise_Prod_Report:
        this.columns = this.dynamicReports.createReports[this.selectedReport].onDatecolumns;
        this.getShiftSeProdReport();
        break;
      case this.reportType.Mech_Test_Report:
        //this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns
        this.getMechTestReport();
        break;
        case this.reportType.Billet_Stock_Report:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns;
        this.getBilletStockReport();
        break;
        case this.reportType.Batch_Detail_Report:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns;
        this.getBatchDetailReport();
        break; 
        case this.reportType.So_Detail_Report:
        this.columns = this.dynamicReports.createReports[this.selectedReport].tableColumns;
        this.getSoDetailReport();
        break; 
      default: break;
    }

  }
  public AccessToSelectedReport(report, event) {

    switch (report) {
      case this.reportType.Billet_Consumption_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.BatchConsumption_Report);
        break;
      
      case this.reportType.Batch_Detail_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.BatchDetail_Report);
        break;

      case this.reportType.So_Detail_Report:
          this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.SoDetail_Report);
          break;

      case this.reportType.Furnace_Residence_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.FurnanceResidence_Report);
        break;

      case this.reportType.SizeWise_Monthly_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.SizeWise_Report);
        break;
      case this.reportType.HeatWise_Production_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.HeatWise_Report);
        break;
      case this.reportType.LP_Stock_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.LPStock_Report);
        break;
      case this.reportType.LP_Production_SummaryReport:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.LPProdSummary_Report);
        break;
      case this.reportType.Hot_Out_Pending_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.HotOut_Report);
        break;
      case this.reportType.Lp_Production_Details_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.LPProdDetails_Report);
        break;
      case this.reportType.Mech_Test_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.MechTest_Report);
        break;
      case this.reportType.Lp_Rolling_Schedule:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.LPRolling_Report);
        break;
      case this.reportType.Shift_Wise_Prod_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.ShiftWiseProd_Report);
        break;
        case this.reportType.Billet_Stock_Report:
        this.setScreenAccess(this.dynamicReports.createReports.ReportsIDs.billetStock_Report);
        break;
      default: break;
    }

  };

  public dropDownChangeEvent(event){
    if(event.value !=null && event.value != '' && event.value == this.reportType.LPStock_Summary_Report ){
      this.formObject.formFields.filter((obj:any)=>obj.ref_key == "productType")[0].isVisible = false;
    }

    if(event.value !=null && event.value != '' && event.value == this.reportType.LP_Stock_Report ){
      this.formObject.formFields.filter((obj:any)=>obj.ref_key == "productType")[0].isVisible = true;
    }
  }


  public setScreenAccess(ReportScreensIds:any)
  {
    this.isRetrive=false; this.isRetrive=false; this.isReset=false; this.isPDfdwnld=false; this.isExceldwnld=false;  this.isFilterSection=false; this.isListSection=false;
    this.screenStructure = this.userRights.subScreens.filter(screen => screen.screenId == ReportScreensIds.Id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, ReportScreensIds.SectionIds.buttons.retrieve, ReportScreensIds.SectionIds.FiletrCriteriaId, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, ReportScreensIds.SectionIds.buttons.reset, ReportScreensIds.SectionIds.FiletrCriteriaId, this.screenStructure);
    this.isPDfdwnld = this.sharedService.isButtonVisible(true, ReportScreensIds.SectionIds.buttons.pdf, ReportScreensIds.SectionIds.RecordListId, this.screenStructure);
    this.isExceldwnld = this.sharedService.isButtonVisible(true, ReportScreensIds.SectionIds.buttons.excel, ReportScreensIds.SectionIds.RecordListId, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(ReportScreensIds.SectionIds.FiletrCriteriaId, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(ReportScreensIds.SectionIds.RecordListId, this.screenStructure);
  }
  public filterData(): any {
    this.getReportTableName = this.reportList.filter(
      (report: any) => report.modelValue == this.getReportName
    )[0].displayName;
  };

  public resetForm() {
    this.isLpSummaryReport = false;
    this.jswComponent.resetForm(this.formObject);
    //this.jswComponent.resetForm(this.selectReportObject);
    this.reportsRecordList = [];
    if (this.shiftWiseProdReprt) {
      this.getShiftWseProdRptOnLoad();
    }
  };
  public download(event) {
    switch (this.selectedReport) {
      case this.reportType.Billet_Consumption_Report:
        this.downloadFile(event);
        break;

      case this.reportType.Furnace_Residence_Report:
        this.downloadFurnaceResidenceTimeReport(event);
        break;

      case this.reportType.SizeWise_Monthly_Report:
        this.getGradeSizeWiseReportDownload(event);
        break;

      case this.reportType.HeatWise_Production_Report:
        this.downloadHeatWiseProductionReport(event);
        break;

      case this.reportType.Hot_Out_Pending_Report:
        this.downloadHotOutPendingDetailsReport(event)
        break;
      case this.reportType.LP_Production_SummaryReport:
        this.downloadLPProductionSummaryReport(event);
        break;
      case this.reportType.LP_Stock_Report:
        this.getLPStockReportDownload(event)
        break;
      case this.reportType.Lp_Production_Details_Report:
        this.downloadLpProductionDetlReport(event);
        break;
      case this.reportType.Lp_Rolling_Schedule:
        this.downloadRollingReport(event);
        break;
      case this.reportType.Shift_Wise_Prod_Report:
        this.downloadSWPReport(event);
        break;
        case this.reportType.Mech_Test_Report:
        this.downloadMechTestReport(event);
        break;
        case this.reportType.Billet_Stock_Report:
        this.getBilletStockReportDownload(event)
        break;
      default: break;
    }

  }
  public getWorkCenterList() {
    this.brmQaService.getWorkCenterList().subscribe((data) => {
      this.unitList = data;
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'unit'
      )[0].list = this.unitList.map((x: any) => {
        return { displayName: x.wcName, modelValue: x.wcName };
      });
    });
  }
  public getAllWorkCenterList() {
    this.commonService.getWorkCenterListByUserId().subscribe((data) => {
      this.unitList = data;
      this.formObject.formFields.filter((obj: any) => obj.ref_key == 'unit')[0].list = this.unitList.map((x: any) => {
        return { displayName: x.wcName, modelValue: x.wcName };
      });
    })
  }

  // public getWorkCenterList() {
  //   this.brmQaService.getWorkCenterList().subscribe((data) => {
  //     this.unitList = data;
  //     this.filterFormObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list = this.unitList.map((x: any) => {
  //       return { displayName: x.wcName, modelValue: x.wcName };
  //     });
  //   })
  // }
  // End Common Functionalistis


  // Start Billet Report//
  public generateBilletConspReport(event) {
    this.selectedReport = this.getReportName;
    this.columns = [];
    this.columns = this.dynamicReports.createReports[event.value].tableColumns
    this.getBilletConsumptionReport();
  }
  public downloadFile(event) {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.BatchConsumption_Report.Name;
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(
        formData,
        this.requestBody
      );
      this.requestBody = {
        fromDate: formObj.fromDate != '' ? formObj.fromDate : null,
        toDate: formObj.toDate != '' ? formObj.toDate : null,
        heatId: formObj.heatId != '' ? formObj.heatId : null,
      };
      if (event == 'pdf') {
        this.reportsService
          .downloadbilletConsumptionReport('PDF', this.requestBody)
          .subscribe((Response) => {
            this.sharedService.generateReport(event, Response,this.ReportName)
            // let file = new Blob([Response], { type: 'application/pdf' });
            // var fileURL = URL.createObjectURL(file);
            // window.open(fileURL);
          });
      } else if (event == 'excel') {
        this.reportsService
          .downloadbilletConsumptionReport('EXCEL', this.requestBody)
          .subscribe((Response) => {
            this.sharedService.generateReport(event, Response,this.ReportName)
            // let file = new Blob([Response], {
            //   type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8',
            // });
            // var fileURL = URL.createObjectURL(file);
            // window.open(fileURL);
          });
      }
    }
  }
  public getBilletConsumptionReport(): any {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(
        formData,
        this.requestBody
      );
      this.requestBody = {
        fromDate: formObj.fromDate != '' ? formObj.fromDate : null,
        toDate: formObj.toDate != '' ? formObj.toDate : null,
        heatId: formObj.heatId != '' ? formObj.heatId : null,
      };
      this.reportsService
        .getBilletConsumptionReport(this.requestBody)
        .subscribe((Response) => {
          Response.map((rowData: any) => {
            return rowData;
          });
          this.reportsRecordList = Response;
        });
    }
  }
  // public getLPStockReport1(): any {
  //   this.jswComponent.onSubmit(
  //     this.formObject.formName,
  //     this.formObject.formFields
  //   );
  //   var formData = this.jswService.getFormData();
  //   if (formData && formData.length) {
  //     let formObj = this.sharedService.mapFormObjectToReqBody(
  //       formData,
  //       this.reqBodyLpStock
  //     );
  //     // this.reqBodyLpStock = {



  //     //   unit: formObj.unit != '' ? formObj.unit : null,
  //     // };
  //     this.reportsService
  //       .getLPStockReport(this.reqBodyLpStock)
  //       .subscribe((Response) => {
  //         Response.map((rowData: any) => {
  //           return rowData;
  //         });
  //         this.reportsRecordList = Response;
  //       });
  //   }
  // }
 //Start of Batch Detail Report
 public generateBatchDetailReport(event) {
  this.selectedReport = this.getReportName;
  this.columns = [];
  this.columns = this.dynamicReports.createReports[event.value].tableColumns
  this.getBatchDetailReport();
}
 public downloadFileBatch(event) {
  this.jswComponent.onSubmit(
    this.formObject.formName,
    this.formObject.formFields
  );
  var formData = this.jswService.getFormData();
  this.ReportName=this.dynamicReports.createReports.ReportsIDs.BatchDetail_Report.Name;
  if (formData && formData.length) {
    let formObj = this.sharedService.mapFormObjectToReqBody(
      formData,
      this.requestBodyBatch
    );
    this.requestBodyBatch = {

      batchId: formObj.batchId != '' ? formObj.batchId : null,
    };
    if (event == 'pdf') {
      this.reportsService
        .downloadbatchDetailReport('PDF', this.requestBodyBatch)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    } else if (event == 'excel') {
      this.reportsService
        .downloadbatchDetailReport('EXCEL', this.requestBodyBatch)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    }
  }
} 
public getBatchDetailReport(): any {
  this.jswComponent.onSubmit(
    this.formObject.formName,
    this.formObject.formFields
  );
  var formData = this.jswService.getFormData();
  if (formData && formData.length) {
    let formObj = this.sharedService.mapFormObjectToReqBody(
      formData,
      this.requestBodyBatch
    );
    this.requestBodyBatch = {
      batchId: formObj.batchId != '' ? formObj.batchId : null,
    };
    this.reportsService
      .getBatchDetailReport(this.requestBodyBatch)
      .subscribe((Response) => {
        Response.map((rowData: any) => {
          return rowData;
        });
        this.reportsRecordList = Response;
      });
  }
} 

//end of batch detail report 
//start of so detail report 
public generateSoDetailReport(event) {
  this.selectedReport = this.getReportName;
  this.columns = [];
  this.columns = this.dynamicReports.createReports[event.value].tableColumns
  this.getSoDetailReport();
}
 public downloadFileSo(event) {
  this.jswComponent.onSubmit(
    this.formObject.formName,
    this.formObject.formFields
  );
  var formData = this.jswService.getFormData();
  this.ReportName=this.dynamicReports.createReports.ReportsIDs.SoDetail_Report.Name;
  if (formData && formData.length) {
    let formObj = this.sharedService.mapFormObjectToReqBody(
      formData,
      this.requestBodySo
    );
    this.requestBodySo = {

      soId: formObj.soId != '' ? formObj.soId : null,
      lineNo:formObj.lineNo != '' ? formObj.lineNo : null,
    };
    if (event == 'pdf') {
      this.reportsService
        .downloadbatchDetailReport('PDF', this.requestBodySo)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    } else if (event == 'excel') {
      this.reportsService
        .downloadbatchDetailReport('EXCEL', this.requestBodySo)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    }
  }
} 
public getSoDetailReport(): any {
  this.jswComponent.onSubmit(
    this.formObject.formName,
    this.formObject.formFields
  );
  var formData = this.jswService.getFormData();
  if (formData && formData.length) {
    let formObj = this.sharedService.mapFormObjectToReqBody(
      formData,
      this.requestBodySo
    );
    this.requestBodySo = {
      soId: formObj.soId != '' ? formObj.soId : null,
      lineNo:formObj.lineNo != '' ? formObj.lineNo : null,
    };
    this.reportsService
      .getSoDetailReport(this.requestBodySo)
      .subscribe((Response) => {
        Response.map((rowData: any) => {
          return rowData;
        });
        this.reportsRecordList = Response;
      });
  }
}
//end of so detail report 
  public getLPStockReportDownload(event) {
    let formObj = this.formValidationsResult();
    if (event == 'pdf') {
      this.reportsService
        .getLPStockReportDownload('PDF', formObj)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.selectedReport)
        });
    } else if (event == 'excel') {
      this.reportsService.getLPStockReportDownload('EXCEL', formObj).subscribe((Response) => {
        this.sharedService.generateReport(event, Response,this.selectedReport)
      });
    }
  };

  // End Billet Report//

  //16-11-2021

  public getBilletStockReportDownload(event) {
    let formObj = this.formValidationsResult();
    if (event == 'pdf') {
      this.reportsService
        .getBilletStockReportDownload('PDF', formObj)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.selectedReport)
        });
    } else if (event == 'excel') {
      this.reportsService.getBilletStockReportDownload('EXCEL', formObj).subscribe((Response) => {
        this.sharedService.generateReport(event, Response,this.selectedReport)
      });
    }
  };
  //


  // Start FurnaceResidence Report//

  public genRatefurnaceResidenceReport(event) {
    this.getWorkCenterList();
    this.selectedReport = this.getReportName;
    this.columns = [];
    this.columns = this.dynamicReports.createReports[event.value].tableColumns
    this.getFurnaceResidenceTimeReportDetails();

  }
  public genRateLpStockReport(event) {
    this.getWorkCenterList();
    this.getLovsStockReport1(LovCodes.Output_Product, 'productType');
    this.getLovsStockReport(LovCodes.mat_c, 'materialClass');

    this.selectedReport = this.getReportName;
    this.columns = [];
    this.columns = this.dynamicReports.createReports[event.value].tableColumns
    this.getLPStockReport();

  }

  //16-11-2021
  public genRateBilletStockReport(event) {
    this.getSmsWorkCenterList();
    this.getLovsStockReport1(LovCodes.input_Product, 'productType');
    this.getLovsStockReport(LovCodes.mat_c, 'materialClass');

    this.selectedReport = this.getReportName;
    this.columns = [];
    this.columns = this.dynamicReports.createReports[event.value].tableColumns;
    this.reportsRecordList= [];
    //this.getBilletStockReport();

  }

  public getSmsWorkCenterList()
    {
      this.reportsService.getBilletStockReportUnits(this.sharedService.loggedInUserDetails.userId).subscribe((response: any)=>{
        if(response){
              this.formObject.formFields.filter(
            (obj: any) => obj.ref_key == 'unit'
          )[0].list = response.map((x: any) => {
            return { displayName: x, modelValue: x };
          });
        }
      })
      // this.commonService.getWorkCenterListByUserId().subscribe((data) => {
      //   if(data){
      //    this.unitList = data.filter(
      //       (ele: any) => ele.wcType == 'SMS'
      //     );
      //     this.formObject.formFields.filter(
      //       (obj: any) => obj.ref_key == 'unit'
      //     )[0].list = this.unitList.map((x: any) => {
      //       return { displayName: x.wcName, modelValue: x.wcName };
      //     });
      //   }

      // })
    }
  //
  public getLovsStockReport(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.lovValueId,
        };
      });
    });
  }

  public getLovsStockReport1(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }
  public getFurnaceResidenceTimeReportDetails() {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(
        formData,
        this.reqBodyFurnaceSize
      );
      this.reqBodyFurnaceSize = {
        fromDate: formObj.fromDate != '' ? formObj.fromDate : null,
        toDate: formObj.toDate != '' ? formObj.toDate : null,
        unit: formObj.unit != '' ? formObj.unit : null,
      };
      this.reportsService
        .getFurnaceResidenceTimeReportDetails(this.reqBodyFurnaceSize)
        .subscribe((Response) => {
          Response.map((rowData: any) => {
            return rowData;
          });
          this.reportsRecordList = Response;
        });
    }
  }
  public downloadFurnaceResidenceTimeReport(event) {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.FurnanceResidence_Report.Name;
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(
        formData,
        this.reqBodyFurnaceSize
      );
      this.reqBodyFurnaceSize = {
        fromDate: formObj.fromDate != '' ? formObj.fromDate : null,
        toDate: formObj.toDate != '' ? formObj.toDate : null,
        unit: formObj.unit != '' ? formObj.unit : null,
      };
    }
    if (event == 'pdf') {
      this.reportsService
        .downloadFurnaceResidenceTimeReport('PDF', this.reqBodyFurnaceSize)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
          // let file = new Blob([Response], { type: 'application/pdf' });
          // var fileURL = URL.createObjectURL(file);
          // window.open(fileURL);
        });
    } else if (event == 'excel') {
      this.reportsService
        .downloadFurnaceResidenceTimeReport('EXCEL', this.reqBodyFurnaceSize)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
          // let file = new Blob([Response], {
          //   type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8',
          // });
          // var fileURL = URL.createObjectURL(file);
          // window.open(fileURL);
        });
    }
  }

  // End FurnaceResidence Report//

  // Start GradeSize Report//

  public createHeaderName(header) {
    let string = header.replace("_", " ");
    return string.charAt(0).toUpperCase() + string.slice(1)
  }

  public getGradeSizeWiseReportData(): any {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(
        formData,
        this.reqBodyGradeSize
      );
      this.reqBodyGradeSize = {
        fromDate: formObj.fromDate != '' ? formObj.fromDate : null,
        toDate: formObj.toDate != '' ? formObj.toDate : null,
        grade: formObj.grade != '' ? formObj.grade : null,
      };
      this.reportsService
        .getGradeSizeWiseReport(this.reqBodyGradeSize)
        .subscribe((Response) => {
          Response.map((rowData: any) => {
            return rowData;
          });
          this.reportsRecordList = Response;
          this.setGradeSizeWiseTable(Response);
        });
    }
  };

  public setGradeSizeWiseTable(gradeDiaList) {
    this.columns[0] = {
      field: 'grade',
      header: 'Grade',
      columnType: ColumnType.string,
      width: '20px',
      sortFieldName: '',
    };
    // var indexOf = this.columns.findIndex((x: any) => x.header == 'grade');
    // if (indexOf != -1) {
    //   this.columns.splice(indexOf, 1);
    // }
    let objectArray = [];
    var newData = gradeDiaList.map((x: any) => {
      x.diaAndCountList.grade = x.grade;
      x.diaAndCountList.forEach((element) => {
        objectArray.push({
          [`${element.diaName}`]: element.diaCount,
          gradeName: x.grade,
        });
      });
      return objectArray;
    });
    this.reportsRecordList = newData;
    let newObjectData = gradeDiaList.map((obj: any) => {
      let columeObject = [];
      let latestGradeWiseObjt = newData[0]
        .filter((y: any) => y.gradeName == obj.grade)
        .map((element) => {
          let keyList = Object.keys(element);
          return { [`${keyList[0]}`]: element[`${keyList[0]}`] };
        });
      latestGradeWiseObjt.push({ grade: obj.grade });
      return {
        grade: obj.grade,
        list: latestGradeWiseObjt,
      };
    });
    newObjectData[0].list.forEach((element) => {
      let keyList = Object.keys(element);
      if (
        this.columns.push({
          field: `${keyList[0]}`,
          header: this.createHeaderName(`${keyList[0]}`),
          columnType: ColumnType.string,
          width: '20px',
          sortFieldName: '',
        })
      ) {
      }
    });
    let Data = newObjectData.map((x: any) => {
      return x.list.reduce(function (result, current) {
        return Object.assign(result, current);
      }, {});
    });
    this.reportsRecordList = Data;
    let index = this.columns.length - 1;
    this.columns.splice(index, 1); //adjusted
  }
  public getGradeSizeWiseReportDownload(event) {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.SizeWise_Report.Name;
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(
        formData,
        this.reqBodyGradeSize
      );
      this.reqBodyGradeSize = {
        fromDate: formObj.fromDate != '' ? formObj.fromDate : null,
        toDate: formObj.toDate != '' ? formObj.toDate : null,
        grade: formObj.grade != '' ? formObj.grade : null,
      };
      if (event == 'pdf') {
        this.reportsService
          .getGradeSizeWiseReportDownload('PDF', this.reqBodyGradeSize)
          .subscribe((Response) => {
            this.sharedService.generateReport(event, Response,this.ReportName)
            // let file = new Blob([Response], { type: 'application/pdf' });
            // var fileURL = URL.createObjectURL(file);
            // window.open(fileURL);
          });
      } else if (event == 'excel') {
        this.reportsService
          .getGradeSizeWiseReportDownload('EXCEL', this.reqBodyGradeSize)
          .subscribe((Response) => {
            this.sharedService.generateReport(event, Response,this.ReportName)
            // let file = new Blob([Response], {
            //   type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8',
            // });
            // var fileURL = URL.createObjectURL(file);
            // window.open(fileURL);
          });
      }
    }
  }
  public getallgrade() {
    this.masterService.getallgrade().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'grade'
      )[0].list = data.map((x: any) => {
        return { displayName: x.gradeDesc, modelValue: x.gradeName };
      });
    });
  }
  public genrateSizeWiseMonthlyReport() {
    this.getallgrade();
    this.columns = [];
    this.selectedReport = this.getReportName;
    this.getGradeSizeWiseReportData();

  }
  // End GradeSize Report//

  // Start HeatWise Report//
  public generateHeatwiseProdReport(event) {
    this.getAllWorkCenterList();
    this.selectedReport = this.getReportName;
    this.columns = [];
    this.columns = this.dynamicReports.createReports[event.value].tableColumns;
  }

  public getheatWiseProductionReport(): any {
    let formObj = this.formValidationsResult();
    var valueExists = false;
    Object.keys(formObj).forEach(function (key) {
      if (formObj[key] != null) {
        valueExists = true;
      }
    });
    if (valueExists) {
      this.reportsService
        .getheatWiseProductionReport(formObj)
        .subscribe((Response) => {
          Response.map((rowData: any) => {
            return rowData;
          });
          this.reportsRecordList = Response;
        });
    }
    else {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: `Please Select a Fields `,
      }
      );
    }
  };
  public downloadHeatWiseProductionReport(event) {
    let formObj = this.formValidationsResult();
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.HeatWise_Report.Name;
    if (event == 'pdf') {
      this.reportsService
        .downloadheatWiseProductionReport('PDF', formObj)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    } else if (event == 'excel') {
      this.reportsService.downloadheatWiseProductionReport('EXCEL', formObj).subscribe((Response) => {
        this.sharedService.generateReport(event, Response,this.ReportName)
      });
    }
  };
  // End GradeSize Report//


  // Start Hot Out Pending Report//
  public generatHotOutPendingdReport(event) {
    this.selectedReport = this.getReportName;
    this.columns = [];
    this.columns = this.dynamicReports.createReports[event.value].tableColumns
  };

  public getHotOutPendingFilterWiseReport(): any {
    let formObj = this.formValidationsResult();
    this.reportsService.getHotOutPendingDetails(formObj).subscribe((Response) => {
      Response.map((rowData: any) => {
        return rowData;
      });
      this.reportsRecordList = Response;
    });
  };


  public downloadHotOutPendingDetailsReport(event) {
    let formObj = this.formValidationsResult();
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.HotOut_Report.Name;
    if (event == 'pdf') {
      this.reportsService
        .downloadHotOutPendingDetailsReport('PDF', formObj)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    } else if (event == 'excel') {
      this.reportsService.downloadHotOutPendingDetailsReport('EXCEL', formObj).subscribe((Response) => {
        this.sharedService.generateReport(event, Response,this.ReportName)
      });
    }
  };

  // End Hot Out Pending Report//


  // Start LP Production Summary Report//
  public generateLPProdSummaryReport(event) {
    this.getWorkCenterList();
    this.selectedReport = this.getReportName;
    this.columns = [];
    this.columns = this.dynamicReports.createReports[event.value].tableColumns;
    //this.getLPProductionSummaryReport();
  }
  public getLPProductionSummaryReport(): any {
    let formObj = this.formValidationsResult();
    var valueExists = false;
    Object.keys(formObj).forEach(function (key) {
      if (formObj[key] != null) {
        valueExists = true;
      }
    });
    if (valueExists) {
      this.reportsService
        .getLPProductionSummaryReport(formObj)
        .subscribe((Response) => {
          Response.map((rowData: any) => {
            return rowData;
          });
          this.reportsRecordList = Response;
        });
    }
    else {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: `Please Select a Fields `,
      }
      );
    }
  };

  public downloadLPProductionSummaryReport(event) {
    let formObj = this.formValidationsResult();
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.LPProdSummary_Report.Name;
    if (event == 'pdf') {
      this.reportsService
        .downloadLPProductionSummaryReport('PDF', formObj)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    } else if (event == 'excel') {
      this.reportsService.downloadLPProductionSummaryReport('EXCEL', formObj).subscribe((Response) => {
        this.sharedService.generateReport(event, Response,this.ReportName)
      });
    }
  };
  // End LP Production Summary Report//


  public getLPStockReport(): any {
    let formObj = this.formValidationsResult();
    if (formObj.materialClass.length <= 0) {
      formObj.materialClass = null;
    };
    if(formObj.reportType == this.reportType.LP_Stock_Report){
      this.isLpSummaryReport = false
      this.getLPstockReportDetails(formObj);
    };

    if(formObj.reportType == this.reportType.LPStock_Summary_Report){
      this.isLpSummaryReport = true;
      this.getLPStockSummaryDetails(formObj);
    }

  };

  //16-11-2021
  public getBilletStockReport(): any {
    let formObj = this.formValidationsResult();
    if (formObj.materialClass.length <= 0) {
      formObj.materialClass = null;
    };
    formObj.reportType = null;
    //this.isLpSummaryReport = false
    this.getBilletstockReportDetails(formObj);
  };
  public getBilletstockReportDetails(requestBody){
    this.reportsService
     .getBilletStockReport(requestBody)
     .subscribe((Response) => {
       this.getReportTableName = 'Billet Stock Report'
       Response.map((rowData: any) => {
         return rowData;
       });
       this.reportsRecordList = Response;
     });

 };
//
  public getLPstockReportDetails(requestBody){
     this.reportsService
      .getLPStockReport(requestBody)
      .subscribe((Response) => {
        this.getReportTableName = 'LP Stock Report'
        Response.map((rowData: any) => {
          return rowData;
        });
        this.reportsRecordList = Response;
      });

  };


  // Special Method To render LP Stock Summary Report

  public getLPStockSummaryDetails(reqBody){
    this.dummyValues = [];
    this.secRow = [];
    this.reportsService.getLPStockSummaryReport(reqBody).subscribe (Response =>{
      if(Response.length > 0){
        this.secRow = [{
          field: 'grade',
          header: null,
          rowSpan: null,
          colSpan: null,
          width:'70px'
        }];
        this.trialColumnHeads = Response;
        let newObject = this.trialColumnHeads;
        let profuctList = [];
       let gradeWiseData= newObject.map((x:any)=>{
          x.prodWiseDiaAndWeightList.push({grade:x.grade})
          return x.prodWiseDiaAndWeightList
        });
        let listobject = [];
        let mergeObject = {}
        gradeWiseData.map((x:any)=>{
          x.map((y:any)=>{
            mergeObject = {...mergeObject,...y};
          });
          listobject.push(mergeObject)
        })
        let firstRowKeys = Object.keys(listobject[0]);
        this.firstRow = firstRowKeys.map((x:any)=>{
          return {
            field: x,
            header: x=='grade'?'Grade':x,
            rowSpan: 1,
            colSpan: x=='grade'?0:listobject[0][x].diaWeightInfo.length + 1,
            width:x=='grade'?'65px':null
          }
        });
        this.firstRow.unshift({
          field: 'grade',
          header: 'Grade',
          rowSpan: 1,
          colSpan: 0,
          width:'65px'});
          this.firstRow.pop();
        let TmtValue = [];
        listobject.forEach((element:any)=>{
          let currentGrade = null
          let totalMaterialWeightTMT = null;
          let totalMaterialWeightPr= null;
          let TMTData = [];
          let plainRoundData = [];
          Object.keys(element).forEach((x:any) =>{

            currentGrade = element.grade;
            if(x != 'grade'){
              if(x == 'TMT'){

                totalMaterialWeightTMT = element[x].totalMaterialWeight
                TMTData = element[x].diaWeightInfo.map((x:any)=>{
                    return {
                        [`${x.diaName}`]:x.diaWeight
                      }
                    })
              TMTData.push({totalMaterialWeightTMT:totalMaterialWeightTMT})
              }
              if(x == 'Plain Rounds' ){
                totalMaterialWeightPr  =element[x].totalMaterialWeightPr

                plainRoundData = element[x].diaWeightInfo.map((x:any)=>{
                    return {
                        [`${x.diaName}pr`]:x.diaWeight
                      }
                    });
                    plainRoundData.push({totalMaterialWeightPr:element[x].totalMaterialWeight})

              }

            };
          })
          let dumData =plainRoundData.concat(TMTData);
          let mergeDummyobject  = {grade:currentGrade}
          dumData.forEach((x:any)=>{
              mergeDummyobject = {...mergeDummyobject,...x}
            });
          this.dummyValues.push(mergeDummyobject);
        });

        let tmtRows =listobject[0].TMT.diaWeightInfo.map((x:any)=>{
          return {
            value :x.diaWeight,
            field: x.diaName,
            header: this.createHeaderName(x.diaName),
            rowSpan: null,
            colSpan: null,
            width:'70px'
          }
        })
        tmtRows.push({
          value :null,
          field: `totalMaterialWeightTMT`,
          header: `Total Material Weight`,
          rowSpan: null,
          colSpan: null,
          width:'200px'

        });
        let prSecRow = listobject[0]['Plain Rounds'].diaWeightInfo.map((x:any)=>{
          return {
            value :x.diaWeight,
            field: `${x.diaName}pr`,
            header:this.createHeaderName(x.diaName),
            rowSpan: null,
            colSpan: null,
            width:'65px'
          }
        });
        prSecRow.push({
          value :null,
          field: "totalMaterialWeightPr",
          header: `Total Material Weight`,
          rowSpan: null,
          colSpan: null,
          width:'200px'

        });

        let netRows = tmtRows.concat(prSecRow);


        this.secRow = this.secRow.concat(netRows);


      }else{
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning, {
          message: `No Data Available`,
        }
        );
      }


    });
  }


  public downloadFilesLpsReport(event){
    let formObj = this.formValidationsResult();
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.LPStock_Report.Name;
    this.reportsService.downLoadReport(formObj,event).subscribe((Response => {
      this.sharedService.generateReport(event, Response,this.ReportName)
    }))
  }


    // End Special Method To render LP Stock Summary Report




  // End Hot Out Pending Report//
  /// lp production report
  public generateLpProductionDetailsReport(event) {
    this.getWorkCenterList();
    this.getLovs('Output_Product');
    this.selectedReport = this.getReportName;
    this.columns = [];
    this.columns = this.dynamicReports.createReports[event.value].tableColumns
    //this.getLpProductionReport();
  }
  public getLpProductionReport(): any {
    let formObj = this.formValidationsResult();
    var valueExists = false;
    Object.keys(formObj).forEach(function (key) {
      if (formObj[key] != null) {
        valueExists = true;
      }
    });
    if (valueExists) {
      this.reportsService
        .getLpProdctnDetlsReport(formObj)
        .subscribe((Response) => {
          Response.map((rowData: any) => {
            return rowData;
          });
          this.reportsRecordList = Response;
        });
    }
    else {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: `Please Select a Fields `,
      }
      );
    }
  }

  public dropdownChangeEvent(event: any) {}

  public onItemSelect(event:any){
    if(event.ref_key == 'materialClass' && event.value != null && this.getReportName == 'LPStockReport'){
      if(this.formObject.formFields[1].isError)
      {
        this.formObject.formFields[1].value=''
      }
      this.formObject.formFields[1].isError=false

   }

  }
  public getLovs(lovCode) {
    this.commonService.getLovs(lovCode).subscribe((data) => {

      this.productList = data.map((ele: any) => {
        return {
          displayName: ele.valueDescription,
          modelValue: ele.valueShortCode,
        };
      });
      this.refProductList = this.productList;
      this.formObject.formFields.filter(
        (ele) => ele.ref_key == 'productType'
      )[0].list = this.productList;
    });

  };

  public downloadLpProductionDetlReport(event) {
    let formObj = this.formValidationsResult();
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.LPProdDetails_Report.Name;
    if (event == 'pdf') {
      this.reportsService
        .downloadLpProdctnDetlsReport('PDF', formObj)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    } else if (event == 'excel') {
      this.reportsService.downloadLpProdctnDetlsReport('EXCEL', formObj).subscribe((Response) => {
        this.sharedService.generateReport(event, Response,this.ReportName)
      });
    }
  };

  ///end of lp production report

  /// mech Test report
  public generateMechTestReport(event) {
    this.selectedReport = this.getReportName;
    this.columns = [];
    this.columns = JSON.parse(JSON.stringify(this.dynamicReports.createReports[event.value].tableColumns)) ;
    this.secondColumns = this.dynamicReports.createReports[event.value].tableSecondColumns;
    //this.mechTestReportSelected = true;
    //this.getMechTestReport();
  };

  public getMechTestReport(): any {
    let formObj = this.formValidationsResult();

    var valueExists = false;
    Object.keys(formObj).forEach(function (key) {
      if (formObj[key] != null) {
        valueExists = true;
      }
    });
    if (valueExists) {
      this.reportsService
        .getMechTestReport(formObj)
        .subscribe((Response) => {
          Response.map((rowData: any) => {
            return rowData;
          });
          this.reportsRecordList = Response;
          this.setMechTableData(Response)
        });
    }
    else {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: `Please Select a Fields `,
      }
      );
    }

  };

  public setMechTableData(dynamicList) {
    this.columns = [];
    this.columns = JSON.parse(JSON.stringify(this.dynamicReports.createReports['mechTestReport'].tableColumns)) ;
    if(dynamicList[0] && dynamicList[0].propertyList.length){
      dynamicList[0].propertyList.forEach((element) => {
        if (
          this.columns.push({
            field: element.propertyName,
            header: this.createHeaderName(element.propertyName),
            columnType: ColumnType.string,
            width: '150px',
            sortFieldName: '',
          })
        ) {
        }
      });
      var newDyArr = [];
    dynamicList.forEach(element => {
      element.propertyList.map((x: any) =>{
        this.columns.forEach(item =>{
        if(item.field == x.propertyName){
         var obj = {
            [item.field]: x.propertyValue
        };
        element = {...element, ...obj};
        }
        })
      })
      newDyArr.push(element)

    });
    this.reportsRecordList = newDyArr;
    }


  }
  public downloadMechTestReport(event) {
    let formObj = this.formValidationsResult();
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.MechTest_Report.Name;
    if (event == 'pdf') {
      this.reportsService
        .downloadMechTestReport('PDF', formObj)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    } else if (event == 'excel') {
      this.reportsService.downloadMechTestReport('EXCEL', formObj).subscribe((Response) => {
        this.sharedService.generateReport(event, Response,this.ReportName)
      });
    }
  };
  /// end of mech Test report

  //Lp rolling schedule start
  public genRateRollingScheduleReport(event) {
    this.getWorkCenterList();
    // this.getLovsStockReport(LovCodes.Output_Product, 'ProductType');
    //this.getLovsStockReport(LovCodes.mat_c, 'materialClass');

    this.selectedReport = this.getReportName;
    this.columns = [];
    this.columns = this.dynamicReports.createReports[event.value].tableColumns
    //this.getLPRollingReport();

  }

  public getLPRollingReport(): any {
    let formObj = this.formValidationsResult();
    if (formObj.unit != null && formObj.scheduleId != null) {
      this.reportsService
        .getLPRollingScheduleReport(formObj)
        .subscribe((Response) => {
          Response.map((rowData: any) => {
            return rowData;
          });
          this.reportsRecordList = Response;
        });
      // }
    }
  };

  public downloadRollingReport(event) {
    let formObj = this.formValidationsResult();
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.LPRolling_Report.Name;
    if (event == 'pdf') {
      this.reportsService
        .getLPRollingReportDownload('PDF', formObj)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    } else if (event == 'excel') {
      this.reportsService.getLPRollingReportDownload('EXCEL', formObj).subscribe((Response) => {
        this.sharedService.generateReport(event, Response,this.ReportName)
      });
    }
  };

  //Lp rolling schedule end


  // Shift wise production Report

  public downloadSWPReport(event) {
    let formObj = this.formValidationsResult();
    this.ReportName=this.dynamicReports.createReports.ReportsIDs.ShiftWiseProd_Report.Name;
    if (event == 'pdf') {
      this.reportsService
        .downloadShiftWseProdRprt('PDF', formObj)
        .subscribe((Response) => {
          this.sharedService.generateReport(event, Response,this.ReportName)
        });
    } else if (event == 'excel') {
      this.reportsService.downloadShiftWseProdRprt('EXCEL', formObj).subscribe((Response) => {
        this.sharedService.generateReport(event, Response,this.ReportName)
      });
    }

  };

public getShiftWseProdRptOnLoad(){
  this.reportsService.getShiftWseProdRptOnLoad().subscribe(Response =>{

    this.screenData = Response
    this.todayDate = this.sharedService.get_MM_DD_YYYY_Date(Response.onDateData.today)
    this.ShiftSizeProdData = Response;
    this.onDateTableData = Response.onDateData.onDate;
    this.cummulativeData = Response.cummulativeData != null ? Response.cummulativeData.cummulativeData:[]
    this.cummulativeDate = null
  })
};


public getShiftSeProdReport(): any {
  this.screenData = undefined;
  let formObj = this.formValidationsResult();
    this.reportsService.getShiftWseProdRprt(formObj)
      .subscribe((Response) => {
        this.screenData = Response
        this.todayDate = this.sharedService.get_MM_DD_YYYY_Date(Response.onDateData.today)
        this.onDateTableData = Response.onDateData.onDate;
        this.cummulativeData = Response.cummulativeData != null ? Response.cummulativeData.cummulativeData:[]
        this.reportsRecordList = this.cummulativeData;
        this.cummulativeDate = `${this.sharedService.get_MM_DD_YYYY_Date(Response.cummulativeData.fromDate)} To ${this.sharedService.get_MM_DD_YYYY_Date(Response.cummulativeData.toDate)}`
      })
    // }
  };
  // End Shift wise production Report




}
