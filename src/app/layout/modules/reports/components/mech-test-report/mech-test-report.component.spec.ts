import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MechTestReportComponent } from './mech-test-report.component';

describe('MechTestReportComponent', () => {
  let component: MechTestReportComponent;
  let fixture: ComponentFixture<MechTestReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MechTestReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MechTestReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
