import { Component, Input, OnInit, ViewChild, ɵEMPTY_ARRAY } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { TableType, ColumnType, LovCodes, ReportType, WorkCenter, Product, LOV } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ReportsService } from '../../services/reports.service';
import {
  DynamicReportsForm,
} from '../../form-objects/reports-form-objects';
import { saveAs } from 'file-saver';
import * as FileSaver from 'file-saver';
import { MasterService } from '../../../master/services/master.service';
import { BrmQaService } from '../../../brm-qa/services/brm-qa.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';

@Component({
  selector: 'app-mech-test-report',
  templateUrl: './mech-test-report.component.html',
  styleUrls: ['./mech-test-report.component.css']
})
export class MechTestReportComponent implements OnInit {

  public dummyValues = [];
  public tableType: any;
  public tempObject = {
    field: null,
    header: null,
    rowSpan: null,
    colSpan: null
  }
  public firstRow: any = [];
  public secRow: any = [{
    field: 'grade',
    header: null,
    rowSpan: null,
    colSpan: null
  }];
  public trialColumnHeads = []

  constructor(    public dynamicReports: DynamicReportsForm,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public reportsService: ReportsService,
    public masterService: MasterService,
    public brmQaService: BrmQaService,
    public authService: AuthService,) {
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
  };


  public getLPStockSummaryDetails(){
    let reqBody = 
    {
      "materialClass": ["P","C"],
      "productType": null,
      "reportType": "string",
      "unit": "LP1"
    }
    
    this.reportsService.getLPStockSummaryReport(reqBody).subscribe (Response =>{
      this.trialColumnHeads = Response;
      let newObject = this.trialColumnHeads;
      let profuctList = [];
     let gradeWiseData= newObject.map((x:any)=>{
        x.prodWiseDiaAndWeightList.push({grade:x.grade})
        return x.prodWiseDiaAndWeightList
      });
      let listobject = [];
      let mergeObject = {}
      gradeWiseData.map((x:any)=>{
        x.map((y:any)=>{
          mergeObject = {...mergeObject,...y};
        });
        listobject.push(mergeObject)
      })
      let firstRowKeys = Object.keys(listobject[0]);
      this.firstRow = firstRowKeys.map((x:any)=>{
        return {
          field: x,
          header: x=='grade'?'Grade':x,
          rowSpan: 1,
          colSpan: x=='grade'?0:listobject[0][x].diaWeightInfo.length + 1
        }
      });
      this.firstRow.unshift({
        field: 'grade',
        header: 'Grade',
        rowSpan: 1,
        colSpan: 0});
        this.firstRow.pop();


      let TmtValue = [];
      listobject.forEach((element:any)=>{
        let currentGrade = null
        let totalMaterialWeightTMT = null;
        let totalMaterialWeightPr= null;
        let TMTData = [];
        let plainRoundData = [];
        Object.keys(element).forEach((x:any) =>{
          
          currentGrade = element.grade;
          if(x != 'grade'){
            if(x == 'TMT'){
        
              totalMaterialWeightTMT = element[x].totalMaterialWeight
              TMTData = element[x].diaWeightInfo.map((x:any)=>{
                  return {
                      [`${x.diaName}`]:x.diaWeight
                    }
                  })
            TMTData.push({totalMaterialWeightTMT:totalMaterialWeightTMT})
            }
            if(x == 'Plain Rounds' ){
              totalMaterialWeightPr  =element[x].totalMaterialWeightPr

              plainRoundData = element[x].diaWeightInfo.map((x:any)=>{
                  return {
                      [`${x.diaName}pr`]:x.diaWeight
                    }
                  });
                  plainRoundData.push({totalMaterialWeightPr:element[x].totalMaterialWeight})
               
            }
        
          };
        })
        let dumData =plainRoundData.concat(TMTData);
        let mergeDummyobject  = {grade:currentGrade}
        dumData.forEach((x:any)=>{
            mergeDummyobject = {...mergeDummyobject,...x}
          });
        this.dummyValues.push(mergeDummyobject)
      });

      

             
      let tmtRows =listobject[0].TMT.diaWeightInfo.map((x:any)=>{
        return {
          value :x.diaWeight,
          field: x.diaName,
          header: this.createHeaderName(x.diaName),
          rowSpan: null,
          colSpan: null
        }
      })
      tmtRows.push({
        value :null,
        field: `totalMaterialWeightTMT`,
        header: `Total Material Weight(tons)`,
        rowSpan: null,
        colSpan: null

      });


      let prSecRow = listobject[0]['Plain Rounds'].diaWeightInfo.map((x:any)=>{
        return {
          value :x.diaWeight,
          field: `${x.diaName}pr`,
          header:this.createHeaderName(x.diaName),
          rowSpan: null,
          colSpan: null
        }
      });
      prSecRow.push({
        value :null,
        field: "totalMaterialWeightPr",
        header: `Total Material Weight(tons)`,
        rowSpan: null,
        colSpan: null

      });

      let netRows = tmtRows.concat(prSecRow);

      this.secRow = this.secRow.concat(netRows);      
     
    
   
    });
  }

  ngOnInit(): void {
    this.getLPStockSummaryDetails();
    this.dummyValues = [];

  };


  public createHeaderName(header) {
    let string = header.replace("_", " ");
    return string.charAt(0).toUpperCase() + string.slice(1)
  }

}
