import {
  Injectable
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  API_Constants
} from 'src/app/shared/constants/api-constants';
import {
  map
} from 'rxjs/operators';
import {
  AuthService
} from 'src/app/core/services/auth.service';
import {
  CommonAPIEndPoints
} from 'src/app/shared/constants/common-api-end-points';
import {
  BehaviorSubject
} from 'rxjs';
import {
  ReportsEndPoints
} from '../form-objects/reports-api-endpoints';


@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  public baseUrl: string;
  public masterBaseUrl: string;


  constructor(public http: HttpClient,
    private apiURL: API_Constants,
    public commonEndPoints: CommonAPIEndPoints,
    public reportsEndPoints: ReportsEndPoints,
    public authService: AuthService) {
    this.baseUrl = `${this.apiURL.baseUrl}${this.apiURL.reportsGateway}`;
    this.masterBaseUrl = `${this.apiURL.baseUrl}${this.apiURL.masterGateWay}`;
  }
  //start of billet consumption report
  public getBilletConsumptionReport(requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getBilletConsumptionReport}`, requestBody).pipe(map((response: any) => response));
  }

  public downloadbilletConsumptionReport(fileFormat, requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadbilletConsumptionReport}/${fileFormat}`, requestBody, { responseType: 'blob' }).pipe(map((response: any) => response));
  }
  //end of billet consumption report

  //start of batch detail report
  public getBatchDetailReport(requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getBatchDetailReport}`, requestBody).pipe(map((response: any) => response));
  }
  public downloadbatchDetailReport(fileFormat, requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadbatchDetailReport}/${fileFormat}`, requestBody, { responseType: 'blob' }).pipe(map((response: any) => response));
  }
  //end of batch detail report

  //Start So detail report
 public getSoDetailReport(requestBody){
  return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getSoDetailReport}`, requestBody).pipe(map((response: any) => response));
 }
 public downloadSoDetailReport(fileFormat, requestBody) {
  return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadbatchDetailReport}/${fileFormat}`, requestBody, { responseType: 'blob' }).pipe(map((response: any) => response));
 }
 //End of So Detail Report

  //start of grade size wise report
  public getGradeSizeWiseReport(requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getGradeSizeWiseReport}`, requestBody).pipe(map((response: any) => response));
  }

  public getGradeSizeWiseReportDownload(fileFormat, requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getGradeSizeWiseReportDownload}/${fileFormat}`, requestBody, { responseType: 'blob' }).pipe(map((response: any) => response));
  }
  //end of grade size wise report

  //Start of furnace residence report
  public getFurnaceResidenceTimeReportDetails(requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getFurnaceResidenceTimeReportDetails}`, requestBody).pipe(map((response: any) => response));
    //End of furnace residence report
  }

  public downloadFurnaceResidenceTimeReport(fileFormat, requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadFurnaceResidenceTimeReport}/${fileFormat}`, requestBody, { responseType: 'blob' }).pipe(map((response: any) => response));
  }

  //start of heat Wise production report
  public getheatWiseProductionReport(requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getHeatWiseProductionReport}`, requestBody).pipe(map((response: any) => response));
  }

  public downloadheatWiseProductionReport(fileFormat, requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadHeatWiseProductionReport}/${fileFormat}`, requestBody, { responseType: 'blob' }).pipe(map((response: any) => response));
  }


  //end of heat Wise production report


  //start of LP production summary report
  public getLPProductionSummaryReport(requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getLPProductionSummaryReport}`, requestBody).pipe(map((response: any) => response));
  }

  public downloadLPProductionSummaryReport(fileFormat, requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadLPProductionSummaryReport}/${fileFormat}`, requestBody, { responseType: 'blob' }).pipe(map((response: any) => response));
  }
  //end of LP production summary report

  //start of Hot Out Pending report
  public getHotOutPendingDetails(requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getHotOutPendingDetails}`, requestBody).pipe(map((response: any) => response));
  };

  public downloadHotOutPendingDetailsReport(fileFormat, requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadHotOutPendingDetailsReport}/${fileFormat}`, requestBody, { responseType: 'blob' }).pipe(map((response: any) => response));
  }

  //start of Hot Out Pending report

  // start of LP production details report
  public getLpProdctnDetlsReport(requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getLpProdctnDetlsReport}`, requestBody).pipe(map((response: any) => response));
  }

  public downloadLpProdctnDetlsReport(fileFormat, requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadLpProdctnDetlsReport}/${fileFormat}`, requestBody, { responseType: 'blob' }).pipe(map((response: any) => response));
  }
  // end of lpprdction report
   //start of LP stock report
 public getLPStockReport(requestBody){
  return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getLPStockReport}`, requestBody).pipe(map((response: any) => response));

 }
 public getLPStockReportDownload(fileFormat,requestBody){
  return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getLPStockReportDownload}/${fileFormat}`, requestBody,{ responseType: 'blob'}).pipe(map((response: any) => response));
 }
 //End of LP stock report

 //start of LP Rolling Schedule
 public getLPRollingScheduleReport(requestBody){
  return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getLPRollingScheduleReport}`, requestBody).pipe(map((response: any) => response));

 }
 public getLPRollingReportDownload(fileFormat,requestBody){
  return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getLPRollingReportDownload}/${fileFormat}`, requestBody,{ responseType: 'blob'}).pipe(map((response: any) => response));
 }
 //End of LP Rolling Schedule

  //start Shift Wise Production report
  public getShiftWseProdRptOnLoad(){
    return this.http.get(`${this.baseUrl}${this.reportsEndPoints.getShiftWseProdRptOnLoad}`,).pipe(map((response: any) => response));  
   }

  public downloadShiftWseProdRprt(fileFormat,requestBody){
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadShiftWseProdRprt}/${fileFormat}`, requestBody,{ responseType: 'blob'}).pipe(map((response: any) => response));
  }

  public getShiftWseProdRprt(requestBody){
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getShiftWseProdRprt}`, requestBody).pipe(map((response: any) => response));
  }
 
   //End of Shift Wise Production report

   // start of Mech test report
  public getMechTestReport(requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getMechTestReport}`, requestBody).pipe(map((response: any) => response));
  }

  public downloadMechTestReport(fileFormat, requestBody) {
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadMechTestReport}/${fileFormat}`, requestBody, { responseType: 'blob' }).pipe(map((response: any) => response));
  }
  // end of lpprdction report

  // Start Of LP Stock summary Report
  public getLPStockSummaryReport(requestBody){
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getLPStockSummaryReport}`,requestBody).pipe(map((response: any) => response));
  }

  public downLoadReport(requestBody,fileType){
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.downloadFilesLps}/${fileType}`,requestBody,{ responseType: 'blob' }).pipe(map((response: any) => response));
  }

  // End Of LP Stock summary Report

  // billet stock report 16-11-20201
  public getBilletStockReport(requestBody){
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getBilletStockReport}`, requestBody).pipe(map((response: any) => response));
   }
   public getBilletStockReportDownload(fileFormat,requestBody){
    return this.http.put(`${this.baseUrl}${this.reportsEndPoints.getBilletStockReportDownload}/${fileFormat}`, requestBody,{ responseType: 'blob'}).pipe(map((response: any) => response));
   }
   public getBilletStockReportUnits(userId){
    return this.http.get(`${this.baseUrl}${this.reportsEndPoints.getBilletStockReportUnits}/${userId}`).pipe(map((response: any) => response));
   }
}
