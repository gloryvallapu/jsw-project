import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';

import { ReportsRoutingModule } from './reports-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReportsService } from './services/reports.service';
import { DynamicReportsForm } from './form-objects/reports-form-objects';
import { ShowReportComponent } from './components/show-report/show-report.component';
import { ReportsEndPoints } from './form-objects/reports-api-endpoints';
import { MasterService } from '../master/services/master.service';
import { MasterEndPoints } from '../master/form-objects/master-api-endpoints';
import { BrmqaEndPoints } from '../brm-qa/form-objects/brm-qa-api-endpoints';
import { BrmQaService } from '../brm-qa/services/brm-qa.service';
import { MechTestReportComponent } from './components/mech-test-report/mech-test-report.component';



@NgModule({
  declarations: [
    ReportsComponent,
    ShowReportComponent,
    MechTestReportComponent,
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule
  ],
  providers: [
    ReportsService,DynamicReportsForm,ReportsEndPoints,MasterService,MasterEndPoints,BrmqaEndPoints,BrmQaService
  ],
})
export class ReportsModule { }
