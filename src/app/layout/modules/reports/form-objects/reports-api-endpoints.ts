export class ReportsEndPoints{
//start of billet consumption api end points
public readonly getBilletConsumptionReport= '/getBilletConsumptionReport';
public readonly downloadbilletConsumptionReport='/billetConsumptionReport/downloadReport';
//end of billet consumption api end points

//start of batch detail report
public readonly getBatchDetailReport='/batchDetail/getBatchDetail';
public readonly downloadbatchDetailReport='/batchDetail/downloadBatchDetailReport';
//end of batch detail report

//start of So detail report
public readonly getSoDetailReport='/soDetail/getSoDetail';
public readonly getSoDetailReportDownload='/soDetail/downloadBatchDetailReport';
//end of So detail report

//start of grade wise size reports
public readonly getGradeSizeWiseReport='/getGradeSizeWiseReport';
public readonly getGradeSizeWiseReportDownload='/getGradeSizeWiseReport/downloadReport';
//end of grade wise size reports

//start of furnace reports
public readonly getFurnaceResidenceTimeReportDetails='/furnace/getFurnaceResidenceTimeReportDetails';
public readonly downloadFurnaceResidenceTimeReport='/furnace/downloadFurnaceResidenceTimeReport';
//end of furnace reports

//start of heat wise production api end points
public readonly getHeatWiseProductionReport= '/getHeatWiseProductionReport';
public readonly downloadHeatWiseProductionReport='/heatWiseProductionReport/downloadReport';
//end of heat wise production api end points


//start of LP production summary api end points
public readonly getLPProductionSummaryReport= '/lpProdSummary/getLpProdSummaryReportDetails';
public readonly downloadLPProductionSummaryReport='/lpProdSummary/downloadLpProdSummaryReportDetails';
//end of LP production summary api end points


//start of Hot Out Pending api end points
public readonly getHotOutPendingDetails= '/hotOutPending/getHotOutPendingDetails';
public readonly downloadHotOutPendingDetailsReport='/hotOutPending/downloadHotOutPendingDetailsReport';
//end of Hot Out Pending api end points
//start of Lp stock report
public readonly getLPStockReport='/getLPStockReport';
public readonly getLPStockReportDownload='/getLPStockReport/downloadReport';
//end of  Lp stock report

//start of LP production details report api end points
public readonly getLpProdctnDetlsReport= '/lpProductionDetail/getlpProductionDetails';
public readonly downloadLpProdctnDetlsReport='/lpProductionDetail/downloadLpProductionDetailReport';
//end of LP production details report api end points

//start of LP rolling schedule report api end points
public readonly getLPRollingScheduleReport='/getLPRollingScheduleReport';
public readonly getLPRollingReportDownload = '/getLPRollingReport/downloadReport';

//End of LP rolling schedule report api end points

//start of Shift Wise Production report api end points
public readonly getShiftWseProdRptOnLoad='/shiftWiseProd/getShiftWiseProductionReportDetailsOnLoad';
public readonly downloadShiftWseProdRprt = '/shiftWiseProd/downloadShiftWiseProductionReport';
public readonly getShiftWseProdRprt = '/shiftWiseProd/getShiftWiseProductionReportDetails';

//End of  Shift Wise Productio report api end points

//start of Mech Test report api end points
public readonly getMechTestReport= '/mechanicalTest/getMechanicalTestDetails';
public readonly downloadMechTestReport='/mechanicalTest/downloadMechanicalTestReport';
//end of LP production details report api end points

//start of Lp stock  summary report api end points
public readonly getLPStockSummaryReport= '/getLPStockSummaryReport';
public readonly downloadFilesLps  ='/getLPStockSummary/downloadReport';
//end of Lp stock  summary report api end points

//start of Billet stock report
public readonly getBilletStockReport='/getSmsStockReport';
public readonly getBilletStockReportDownload='/getSmsStockReport/downloadReport';
public readonly getBilletStockReportUnits='/getSmsWorkCenters';
//end of  Billet stock report
}
