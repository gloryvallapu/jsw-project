import { FieldType } from 'src/assets/enums/common-enum';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';

export class BilletConsumptionReportForm {
  public filterCriteria: Object =
  {
    formName: 'Filter Criteria',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heat',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'Heat No',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.text, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'fromDate',
        label: 'From Date',
        placeHolder: 'fromDate',
        value: null,
        name: 'fromDate',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.calendar,
        list: [],
        changeEvent:'startDate',
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'toDate',
        label: 'To Date',
        placeHolder: 'toDate',
        value: null,
        name: 'toDate',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.calendar,
        changeEvent:'endDate',
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },

    ]
  }
}


export class ReportsList{
  // public reports:any = [
  //   {
  //     displayName:'BILLET CONSUMPTION REPORT',
  //     modelValue:"billetConsumptionReport"
  //   },
  //   {
  //     displayName:'FURNACE RESIDENCE REPORT',
  //     modelValue:"furnaceResidenceReport"
  //   },
  //   {
  //     displayName:'SIZE WISE MONTHLY REPORT',
  //     modelValue:"sizeWiseMonthlyReport"
  //   },
  //   {
  //     displayName:'HEAT WISE PRODUCTION REPORT',
  //     modelValue:"heatWiseProductionReport"
  //   },
  //   {
  //     displayName:'LP PRODUCTION SUMMARY REPORT',
  //     modelValue:"lpProductionSummaryReport"
  //   }

  // ]
}


export class DynamicReportsForm{
  public createReports :any ={
    reportFormateType :{
      excel:'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8',
      pdf:'application/pdf'
    },

    selectReportObject:{
      formName: 'Select Report',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'report',
            label: 'Select Report',
            placeHolder: 'Select Report',
            value: null,
            name: 'report',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
            isVisible: true
          },
        ]

    },

    list:[
      {
        displayName:'Batch Consumption Report',
        modelValue:"billetConsumptionReport",
        ReportAccess:false,
        ReportId:801,
      },
      {
        displayName:'Furnace Residence Report',
        modelValue:"furnaceResidenceReport",
        ReportAccess:false,
        ReportId:802,
      },
      {
        displayName:'Size Wise Monthly Report',
        modelValue:"sizeWiseMonthlyReport",
        ReportAccess:false,
        ReportId:803,
      },
      {
        displayName:'LP Production Details Report',
        modelValue:"lpProductionDetailsReport",
        ReportAccess:false,
        ReportId:804,
      },{
        displayName:'Heat Wise Production Report',
        modelValue:"heatWiseProductionReport",
        ReportAccess:false,
        ReportId:805,
      },
      {
        displayName:'Hot out Pending Report',
        modelValue:"hotOutPendingReport",
        ReportAccess:false,
        ReportId:806,
      },
      {
        displayName:'LP Production Summary Report',
        modelValue:"lpProductionSummaryReport",
        ReportAccess:false,
        ReportId:807,
      },

      {
        displayName:'LP Stock Report',
        modelValue:"LPStockReport",
        ReportAccess:false,
        ReportId:808,
      },
      {
        displayName:'Mechanical Test Report',
        modelValue:"mechTestReport",
        ReportAccess:false,
        ReportId:809,
      },
      {
        displayName:'LP Rolling Schedule Report',
        modelValue:"LpRollingSchduleReport",
        ReportAccess:false,
        ReportId:810,
      },
      {
        displayName:'Shift Wise Production Report',
        modelValue:"shiftWiseProductionReport",
        ReportAccess:false,
        ReportId:811,
      },
      {
        displayName:'Billet Stock Report',
        modelValue:"billetStockReport",
        ReportAccess:true,
        ReportId:812,
      },
      {
        displayName:'Batch Detail Report',
        modelValue:"batchDetailReport",
        ReportAccess:false,
        ReportId:813,
      },
      {
        displayName:'So Detail Report',
        modelValue:"soDetailReport",
        ReportAccess:false,
        ReportId:814,
      },
    ],
    


    shiftWiseProductionReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'fromDate',
            label: 'From Date',
            placeHolder: 'fromDate',
            value: null,
            name: 'fromDate',
            isRequired: true,
            disabled: false,
            fieldType: FieldType.calendar,
            list: [],
            changeEvent:'startDate',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'toDate',
            label: 'To Date',
            placeHolder: 'toDate',
            value: null,
            name: 'toDate',
            isRequired: true,
            disabled: false,
            fieldType: FieldType.calendar,
            changeEvent:'endDate',
            list: [],
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
        ],

      },
      cummulativeDataTableColumns:[

        {
          field: 'shiftAQty',
          header: 'Shift A Quantity',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'shiftBQty',
          header: 'Shift B Quantity',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'shiftCQty',
          header: 'Shift C Quantity',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'total',
          header: 'Total',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },

      ],

      onDatecolumns:[
        {
          field: 'statusDesc',
          header: 'Status Type',
          columnType: ColumnType.string,
          width: '160px',
          sortFieldName: '',
        },
        {
          field: 'shiftAQty',
          header: 'A',
          columnType: ColumnType.string,
          width: '100px',
          sortFieldName: '',
        },
        {
          field: 'shiftBQty',
          header: 'B',
          columnType: ColumnType.string,
          width: '100px',
          sortFieldName: '',
        },
        {
          field: 'shiftCQty',
          header: 'C',
          columnType: ColumnType.string,
          width: '100px',
          sortFieldName: '',
        },
        {
          field: 'total',
          header: 'Total',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },

      ]

    },

    billetConsumptionReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'heatId',
            label: 'Heat No',
            placeHolder: 'Heat No',
            value: null,
            name: 'Heat No',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.text, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'fromDate',
            label: 'From Date',
            placeHolder: 'fromDate',
            value: null,
            name: 'fromDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            list: [],
            changeEvent:'startDate',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'toDate',
            label: 'To Date',
            placeHolder: 'toDate',
            value: null,
            name: 'toDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            changeEvent:'endDate',
            list: [],
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },

        ]
      },
      tableColumns:[
        {
          field: 'batchId',
          header: 'Batch ID',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'heatId',
          header: 'Heat ID',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'dia',
          header: 'Diameter(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },


        {
          field: 'length',
          header: 'Length(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'width',
          header: 'Width(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'smsBilletWgt',
          header: 'SMS Billet Weight(tons)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'rollingBilletWgt',
          header: 'Rolling Billet Weight(tons)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'difWt',
          header: 'Weight Difference(tons)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'custName',
          header: 'Customer Name',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'prodDate',
          header: 'Production Date',
          columnType: ColumnType.date,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'prodDateTime',
          header: 'Production Time',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'planOrd',
          header: 'Plan Order',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'salesOrd',
          header: 'Sales Order ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'idOrd',
          header: 'Order ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'shape',
          header: 'Size',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'shift',
          header: 'Shift',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'idSchd',
          header: 'Schedule ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'schdSeqNo',
          header: 'Schd Seq No',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'unit',
          header: 'Unit',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'wbWeight',
          header: 'WB Weight(tons)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'grade',
          header: 'Grade',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
      ]
    },
    batchDetailReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'batchId',
            label: 'Batch No',
            placeHolder: 'Batch No',
            value: null,
            name: 'Batch No',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.text, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },

        ]
      },
      tableColumns:[
        {
          field: 'batchId',
          header: 'Batch ID',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'prodDt',
          header: 'Prod Date',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'materialId',
          header: 'Material',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },


        {
          field: 'shiftId',
          header: 'Shift',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'prodWc',
          header: 'Prod WC',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'batchStatus',
          header: 'Batch Status',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'storageLoc',
          header: 'Storage Location',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'jswGrade',
          header: 'JSW Grade',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'heatId',
          header: 'Heat ID',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'chemCtr',
          header: 'Chem CTR',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'dia',
          header: 'Dia(mm)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'thickness',
          header: 'Thickness(mm)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'width',
          header: 'Width(mm)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'length',
          header: 'Length(mm)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'pcsInBundle',
          header: 'PCS in Bundle',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'weight',
          header: 'Weight(Tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'orderId',
          header: 'Order ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'scheduleId',
          header: 'Schedule ID',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'scheduleOrderSeq',
          header: 'Schedule Order Seq',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'stackerId',
          header: 'Stacker ID',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'noOfStrap',
          header: 'No Of Straps',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'prevOrderId',
          header: 'Prev Order ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'productionRemark',
          header: 'Production Remark',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'holdRemark',
          header: 'Hold Remark',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'qualityRemark',
          header: 'Quality Remark',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'nextWc',
          header: 'Next WC',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'origOrderId',
          header: 'Orig Order ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'prevBatchStatus',
          header: 'Prev Baytch Status',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'curpPoId',
          header: 'Curr Po ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'origPoId',
          header: 'Orig Po ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'sizeCode',
          header: 'Size Code',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'prevLoc',
          header: 'Previous Location',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'ncoReason',
          header: 'NCO Reason',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'planHeatId',
          header: 'Plan Heat ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
      ]
    },
    furnaceResidenceReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'unit',
            label: 'Select Unit',
            placeHolder: 'Select Unit',
            value: null,
            name: 'unit',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'fromDate',
            label: 'From Date',
            placeHolder: 'fromDate',
            value: null,
            name: 'fromDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            list: [],
            changeEvent:'startDate',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'toDate',
            label: 'To Date',
            placeHolder: 'toDate',
            value: null,
            name: 'toDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            changeEvent:'endDate',
            list: [],
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },

        ]
      },
      tableColumns :[
        {
          field: 'scheduleId',
          header: 'Schedule ID',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'heatId',
          header: 'Heat ID',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'batchId',
          header: 'Batch ID',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'scheduleOrderSeq',
          header: 'Schd Seq No',
          columnType: ColumnType.string,
          width: '260px',
          sortFieldName: '',
        },
        {
          field: 'chargeDate',
          header: 'Batch Charge Time',
          columnType: ColumnType.date,
          width: '160px',
          sortFieldName: '',
        },
        {
          field: 'dischargeDt',
          header: 'Batch Discharge Time',
          columnType: ColumnType.date,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'totalHours',
          header: 'Total Hours',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'totalMinutes',
          header: 'Total Mins',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
       {
          field: 'shape',
          header: 'Size',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
      ]
    },
    sizeWiseMonthlyReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'grade',
            label: 'Select Grade',
            placeHolder: 'Select Grade',
            value: null,
            name: 'grade',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'fromDate',
            label: 'From Date',
            placeHolder: 'fromDate',
            value: null,
            name: 'fromDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            list: [],
            changeEvent:'startDate',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'toDate',
            label: 'To Date',
            placeHolder: 'toDate',
            value: null,
            name: 'toDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            changeEvent:'endDate',
            list: [],
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },

        ]
      }
    },
    LPRollingSchedule:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'MaterialShape',
            label: 'Material Shape',
            placeHolder: 'Material Shape',
            value: null,
            name: 'MaterialShape',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.text, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-2',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'scheduleNo',
            label: 'Schedule ID',
            placeHolder: 'Schedule ID',
            value: null,
            name: 'scheduleNo',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.text,
            isVisible: true
          },
        ]
      },
      tableColumns:[
        {
          field: 'prodDate',
          header: 'Prod Date',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'billDispatch',
          header: 'Billet Dispatch',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'billDispatchWgt',
          header: 'Billet Dispatch Weight',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'hotOutCount',
          header: 'Hot Out Count',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'batchtId',
          header: 'Batch ID',
          columnType: ColumnType.string,
          width: '100px',
          sortFieldName: '',
        },
        {
          field: 'chopLoss',
          header: 'Chop Loss',
          columnType: ColumnType.string,
          width: '90px',
          sortFieldName: '',
        },
        {
          field: 'cobbCnt',
          header: 'Cobal Count',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'cobbPer',
          header: 'Cobal Percentage',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'cobbWgt',
          header: 'Cobal Weight(tons)',
          columnType: ColumnType.string,
          width: '100px',
          sortFieldName: '',
        },
        {
          field: 'flatCnt',
          header: 'Flat Count',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'flatWgt',
          header: 'Flat Weight(tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'hotOutCnt',
          header: 'Hot Out Count',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'hotOutWgt',
          header: 'Hot Out Weight(tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'planeRoundCnt',
          header: 'Plain Round Count',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'planeRoundWgt',
          header: 'Plain Round Weight(tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'qtyPcs',
          header: 'Quantity Pieces',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'rolledCnt',
          header: 'Rolled Count',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'rolledWgt',
          header: 'Rolled Weight(tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'scaleLoss',
          header: 'Scale Loss',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'tmtCnt',
          header: 'TMT Count',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'tmtWgt',
          header: 'TMT Weight(tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'yeildPer',
          header: 'Yield Percentage',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
      ]
    },
    heatWiseProductionReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'unit',
            label: 'Unit',
            placeHolder: 'Unit',
            value: null,
            name: 'Unit',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'heatId',
            label: 'Heat No',
            placeHolder: 'Heat No',
            value: null,
            name: 'Heat No',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.text, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'fromDate',
            label: 'From Date',
            placeHolder: 'fromDate',
            value: null,
            name: 'fromDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            list: [],
            changeEvent:'startDate',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'toDate',
            label: 'To Date',
            placeHolder: 'toDate',
            value: null,
            name: 'toDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            changeEvent:'endDate',
            list: [],
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },

        ]
      },
      tableColumns:[
        {
          field: 'batchtId',
          header: 'Batch ID',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'dimDia',
          header: 'Diameter(mm)',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'cdeSpecStmk',
          header: 'Grade',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'dateProd',
          header: 'Production Date',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'unit',
          header: 'Unit',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'cdeShift',
          header: 'Shift',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'schdNumber',
          header: 'Schedule ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'schdSeqNo',
          header: 'Schd Seq No',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'heatId',
          header: 'Heat ID',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'billetSection',
          header: 'Batch Size',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'billetLength',
          header: 'Batch Length(mm)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'billetWgt',
          header: 'Batch Weight(tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'coilingRemarks',
          header: 'Coiling Remarks',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'prodRemarks',
          header: 'Production Remarks',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
      ]
    },
    lpProductionSummaryReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'unit',
            label: 'Unit',
            placeHolder: 'Unit',
            value: null,
            name: 'Unit',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'schdId',
            label: 'Schedule ID',
            placeHolder: 'Schedule ID',
            value: null,
            name: 'Schedule ID',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.text, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },

          {
            isError: false,
            errorMsg: '',
            ref_key: 'fromDate',
            label: 'From Date',
            placeHolder: 'fromDate',
            value: null,
            name: 'fromDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            list: [],
            changeEvent:'startDate',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'toDate',
            label: 'To Date',
            placeHolder: 'toDate',
            value: null,
            name: 'toDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            changeEvent:'endDate',
            list: [],
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },

        ]
      },
      tableColumns:[
        {
          field: 'schdId',
          header: 'Schd ID',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'schdSts',
          header: 'Schd Sts',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'ordLineId',
          header: 'Ord Line ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'cdeSpecStmk',
          header: 'Cde Spec Stmk',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'equivSpec',
          header: 'Equiv Spec',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'dimOrdDia',
          header: 'Dim Ord Dia(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'diaTolPlus',
          header: 'Dia Tol Plus(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'diaTolMinus',
          header: 'Dia Tol Minus(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'dimOrdWidth',
          header: 'Dim Ord Width(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'widthTolPlus',
          header: 'Width Tol Plus(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'widthTolMinus',
          header: 'Width Tol Minus(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'dimOrdThik',
          header: 'Dim Ord Thik',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'thikTolPlus',
          header: 'Thik Tol Plus',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'thikTolMinus',
          header: 'Thik Tol Minus',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'billetSection',
          header: 'Batch Section',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'noBilletsPlanned',
          header: 'No Batch Planned',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'schdDate',
          header: 'Schd Date',
          columnType: ColumnType.date,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'name_cust_abb',
          header: 'Name Cust Abb',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'cdeShape',
          header: 'Cde Shape',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'salesOrder',
          header: 'Sales Order',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'sapPoOrder',
          header: 'Sales PO Order',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'ipBatchPlanned_tons',
          header: 'IP Batch Planned',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
      ]
    },
    LPStockReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'reportType',
            label: 'Report Type',
            placeHolder: '',
            value: null,
            name: 'role',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown,
            list: [
              {
                displayName:'LP Stock Report',
                modelValue:'LPStockReport'
              },
              {
                displayName:'LP Stock Summary Report',
                modelValue:'LPStockSummaryReport'
              },
            ],
            selectedItems: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true,
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'unit',
            label: 'Unit',
            placeHolder: 'Unit',
            value: null,
            name: 'Unit',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'productType',
            label: 'Product Type',
            placeHolder: 'Product Type',
            value: null,
            name: 'Product Type',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'materialClass',
            label: 'Material Class',
            placeHolder: '',
            value: null,
            name: 'role',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.multiselect,
            list: [],
            selectedItems: [],
            changeEvent: 'multiSelectchange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true,
          },


        ]
      },
      tableColumns:[
        {
          field: 'bundleId',
          header: 'Batch ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'cdeSchdLine',
          header: 'Schedule ID',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'salesOrder',
          header: 'Sales Ord',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'sapPoOrder',
          header: 'SAP PO Order',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'shape',
          header: 'Shape',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'cdeStsMaterial',
          header: 'Batch Status',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'dimDiaAct',
          header: 'Diameter Act(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'dimwidthAct',
          header: 'Width Act(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'dimThickAct',
          header: 'Thick Act',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'wgtMatlActMtrc',
          header: 'Actual Weight(tons)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'nameCustAbb',
          header: 'Customer Name',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'grade',
          header: 'Grade',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'cdeMatlCls',
          header: 'Material Class',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'tmstpCrea',
          header: 'Tmstp Crea',
          columnType: ColumnType.date,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'prodDate',
          header: 'Prod Date',
          columnType: ColumnType.date,
          width: '150px',
          sortFieldName: '',
  },
        {
          field: 'shift',
          header: 'Shift',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'daysOld',
          header: 'Days Old',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'reason',
          header: 'Reason',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'length',
          header: 'Length(mm)',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'inpRemarks',
          header: 'Inp Remarks',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'heatId',
          header: 'Heat ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'c',
          header: 'C',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'mn',
          header: 'Mn',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
 {
          field: 's',
          header: 'S',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'p',
          header: 'P',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'si',
          header: 'Si',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'al',
          header: 'Al',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'cu',
          header: 'Cu',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'cr',
          header: 'Cr',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'ni',
          header: 'Ni',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'yts',
          header: 'Yts',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'uts',
          header: 'Uts',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'el',
          header: 'El',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'plnDia',
          header: 'Plan Diameter(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'plnWidth',
          header: 'Plan Width(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'plnThik',
          header: 'Plan Thickness',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'plnGrade',
          header: 'Plan Grade',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'ordMinWgt',
          header: 'Order Min Weight',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'ordMaxWgt',
          header: 'Order Max Weight(tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'idMatlMstr',
          header: 'ID Matl Mstr',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'custBranch',
          header: 'Cust Branch',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
       {
          field: 'equivSpec',
          header: 'Equiv Spec',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'fgnDom',
          header: 'Fgn Dom',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
   ]
    },
    
    //16-11-2021
    billetStockReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'unit',
            label: 'Unit',
            placeHolder: 'Unit',
            value: null,
            name: 'Unit',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'productType',
            label: 'Product Type',
            placeHolder: 'Product Type',
            value: null,
            name: 'Product Type',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'materialClass',
            label: 'Material Class',
            placeHolder: '',
            value: null,
            name: 'role',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.multiselect,
            list: [],
            selectedItems: [],
            changeEvent: 'multiSelectchange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true,
          },


        ]
      },
      tableColumns:[
        {
          field: 'bundleId',
          header: 'Batch ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'cdeSchdLine',
          header: 'Schedule ID',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'salesOrder',
          header: 'Sales Ord',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'sapPoOrder',
          header: 'SAP PO Order',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'shape',
          header: 'Shape',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'cdeStsMaterial',
          header: 'Batch Status',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'dimDiaAct',
          header: 'Diameter Act(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'dimwidthAct',
          header: 'Width Act(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'dimThickAct',
          header: 'Thick Act',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'wgtMatlActMtrc',
          header: 'Actual Weight(tons)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'nameCustAbb',
          header: 'Customer Name',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'grade',
          header: 'Grade',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'cdeMatlCls',
          header: 'Material Class',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'tmstpCrea',
          header: 'Tmstp Crea',
          columnType: ColumnType.date,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'prodDate',
          header: 'Prod Date',
          columnType: ColumnType.date,
          width: '150px',
          sortFieldName: '',
  },
        {
          field: 'shift',
          header: 'Shift',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'daysOld',
          header: 'Days Old',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'reason',
          header: 'Reason',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'length',
          header: 'Length(mm)',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'inpRemarks',
          header: 'Inp Remarks',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'heatId',
          header: 'Heat ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'c',
          header: 'C',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'mn',
          header: 'Mn',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
 {
          field: 's',
          header: 'S',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'p',
          header: 'P',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'si',
          header: 'Si',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'al',
          header: 'Al',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'cu',
          header: 'Cu',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'cr',
          header: 'Cr',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'ni',
          header: 'Ni',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'yts',
          header: 'Yts',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'uts',
          header: 'Uts',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'el',
          header: 'El',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'plnDia',
          header: 'Plan Diameter(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'plnWidth',
          header: 'Plan Width(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'plnThik',
          header: 'Plan Thickness',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'plnGrade',
          header: 'Plan Grade',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'ordMinWgt',
          header: 'Order Min Weight',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'ordMaxWgt',
          header: 'Order Max Weight(tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'idMatlMstr',
          header: 'ID Matl Mstr',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'custBranch',
          header: 'Cust Branch',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
       {
          field: 'equivSpec',
          header: 'Equiv Spec',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'fgnDom',
          header: 'Fgn Dom',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
   ]
    },

    

    hotOutPendingReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'fromDate',
            label: 'From Date',
            placeHolder: 'fromDate',
            value: null,
            name: 'fromDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            list: [],
            changeEvent:'startDate',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'toDate',
            label: 'To Date',
            placeHolder: 'toDate',
            value: null,
            name: 'toDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            changeEvent:'endDate',
            list: [],
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },

        ]
      },
      tableColumns:[
        {
          field: 'prodDate',
          header: 'Production Date',
          columnType: ColumnType.string,
          width: '270px',
          sortFieldName: '',

        },
        {
          field: 'shift',
          header: 'Shift',
          columnType: ColumnType.string,
          width: '100px',
          sortFieldName: '',

        },
        {
          field: 'billetId',
          header: 'Batch ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'heatId',
          header: 'Heat ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'billetWgt',
          header: 'Batch Weight(tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'billetThk',
          header: 'Batch Thickness(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },

        {
          field: 'billetGrade',
          header: 'Batch Grade',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'schdSeqNo',
          header: 'Schedule Seq. No',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'idSchd',
          header: 'Schedule ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        
        {
          field: 'dischargeTime',
          header: 'Discharge Time',
          columnType: ColumnType.string,
          width: '270px',
          sortFieldName: '',

        },
        {
          field: 'plannedSection',
          header: 'Planned Section',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'unit',
          header: 'Unit',
          columnType: ColumnType.string,
          width: '90px',
          sortFieldName: '',

        },
        {
          field: 'cdeStsMatl',
          header: 'Material Status',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        }, 
        {
          field: 'matlCls',
          header: 'Material Class',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },              
        {
          field: 'planOrd',
          header: 'Plan Order',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'salesOrd',
          header: 'Sales Order',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'idOrd',
          header: 'Order ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
      
      ],

    },

    lpProductionDetailsReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'unit',
            label: 'Unit',
            placeHolder: 'Unit',
            value: null,
            name: 'Unit',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'productType',
            label: 'Product Type',
            placeHolder: 'Product Type',
            value: null,
            name: 'Product Type',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'fromDate',
            label: 'From Date',
            placeHolder: 'fromDate',
            value: null,
            name: 'fromDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            list: [],
            changeEvent:'startDate',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'toDate',
            label: 'To Date',
            placeHolder: 'toDate',
            value: null,
            name: 'toDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            changeEvent:'endDate',
            list: [],
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },

        ]
      },
      tableColumns:[
        {
          field: 'idMatl',
          header: 'Batch ID',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'salesOrd',
          header: 'Sales Order',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'sapPoOrder',
          header: 'SAP PO Order',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'cdeShape',
          header: 'Shape',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'custName',
          header: 'Customer Name',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'idHeat',
          header: 'Heat ID',
          columnType: ColumnType.string,
          width: '90px',
          sortFieldName: '',
        },
        {
          field: 'cdeMatlCls',
          header: 'Material Class',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'cdeQualityReason',
          header: 'Cde Quality Reason',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'cdeStsMatl',
          header: 'Material Status',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'cdeSchd',
          header: 'Schedule ID',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'cdeSpecStlmke',
          header: 'Grade',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'eqvGrade',
          header: 'Equivalent Specification',
          columnType: ColumnType.string,
          width: '250px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'plannedGrade',
          header: 'Planned Grade',
          columnType: ColumnType.string,
          width: '250px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'plainRoundProduction',
          header: 'Plain Round Production',
          columnType: ColumnType.string,
          width: '250px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'dimThickAct',
          header: 'Thickness Actual(mm)',
          columnType: ColumnType.string,
          width: '250px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'dimDia',
          header: 'Diameter(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'dimWidth',
          header: 'Actual Width(mm)',
          columnType: ColumnType.string,
          width: '250px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'wgtMatlActMtrc',
          header: 'Actual Weight(tons)',
          columnType: ColumnType.string,
          width: '250px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'tmstpCrea',
          header: 'Tmstp_Crea',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'tmstpConsm',
          header: 'Tmstp_Consm',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'tmstpHold',
          header: 'Tmstp_Hold',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'qtyPcs',
          header: 'Qty Pcs',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'theoreticalWgtMatl',
          header: 'Theoretical Weight(tons)',
          columnType: ColumnType.string,
          width: '250px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'invoiceWtCat',
          header: 'Invoice Weight(tons)',
          columnType: ColumnType.string,
          width: '250px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'lengthActual',
          header: 'Length Actual(mm)',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
          dateType: 'short',
        },
        {
          field: 'salesOrganization',
          header: 'Sales Organization',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short',
        }
      ]
    },

    mechTestReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'heatId',
            label: 'Heat No',
            placeHolder: 'Heat No',
            value: null,
            name: 'Heat No',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.text, //'text',
            list: [],
            changeEvent: '',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'fromDate',
            label: 'From Date',
            placeHolder: 'fromDate',
            value: null,
            name: 'fromDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            list: [],
            changeEvent:'startDate',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'toDate',
            label: 'To Date',
            placeHolder: 'toDate',
            value: null,
            name: 'toDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            changeEvent:'endDate',
            list: [],
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },

        ]
      },
      tableColumns:[
        {
          field: 'dateOfTesting',
          header: 'Date of Testing',
          columnType: ColumnType.string,
          sortFieldName: '',
          width: '150px',
          dateType: 'short'
        },
        {
          field: 'dateOfRolling',
          header: 'Date of Rolling',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
          dateType: 'short'
        },
        {
          field: 'bundleNo',
          header: 'Bundle No',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: ''
        },
        {
          field: 'heatNo',
          header: 'Heat No',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: ''
        },
        {
          field: 'sampleId',
          header: 'Sample ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: ''
        },
        {
          field: 'grade',
          header: 'Grade',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: ''
        }
      ],
      tableSecondColumns:[
        {
          field: 'Izod',
          header: 'Izod',
          columnType: ColumnType.string,

          sortFieldName: '',
          dateType: 'short',
          rowSpan:'2'
        },
        {
          field: 'Charpy',
          header: 'Charpy',
          columnType: ColumnType.string,

          sortFieldName: '',
          dateType: 'short',
          rowSpan:'2'
        },
        {
          field: 'YS',
          header: 'YS (Mpa)',
          columnType: ColumnType.string,

          sortFieldName: '',
          dateType: 'short',
          rowSpan:'2'
        },
        {
          field: 'TS',
          header: 'TS (Mpa)',
          columnType: ColumnType.string,

          sortFieldName: '',
          dateType: 'short',
          rowSpan:'2'
        },
        {
          field: 'Elongation',
          header: 'Elongation (%)',
          columnType: ColumnType.string,

          sortFieldName: '',
          dateType: 'short',
          rowSpan:'2'
        },
        {
          field: 'Reduction',
          header: 'Reduction in area (%)',
          columnType: ColumnType.string,

          sortFieldName: '',
          dateType: 'short',
          rowSpan:'2'
        }
      ]
    },
    LpRollingSchduleReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'unit',
            label: 'Unit',
            placeHolder: 'Unit',
            value: null,
            name: 'Unit',
            isRequired: true,
            disabled: false,
            fieldType: FieldType.dropdown, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'scheduleId',
            label: 'Schedule ID',
            placeHolder: 'Schedule ID',
            value: null,
            name: 'Schedule ID',
            isRequired: true,
            disabled: false,
            fieldType: FieldType.text, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'fromDate',
            label: 'From Date',
            placeHolder: 'fromDate',
            value: null,
            name: 'fromDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            list: [],
            changeEvent:'startDate',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'toDate',
            label: 'To Date',
            placeHolder: 'toDate',
            value: null,
            name: 'toDate',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.calendar,
            changeEvent:'endDate',
            list: [],
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },


        ]
      },
      tableColumns:[
        {
          field: 'prodDate',
          header: 'Prod Date',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'billDispatch',
          header: 'IP Batch Cnt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'billDispatchWgt',
          header: 'IP Batch Wgt(tons)',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'hotOutCnt',
          header: 'Hot Out Cnt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'hotOutWgt',
          header: 'Hot Out Wgt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'rolledCnt',
          header: 'Rolled Cnt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'rolledWgt',
          header: 'Rolled Wgt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'tmtCnt',
          header: 'TMT Cnt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'tmtWgt',
          header: 'TMT Wgt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'flatCnt',
          header: 'Flat Cnt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'flatWgt',
          header: 'Flat Wgt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'planeRoundCnt',
          header: 'Plain Round Cnt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'planeRoundWgt',
          header: 'Plain Round Wgt',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'cobbCnt',
          header: 'Cobb Cnt',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'cobbWgt',
          header: 'Cobb Wgt',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'cobbPer',
          header: 'Cobb Per',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'yeildPer',
          header: 'Yield Per',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'scaleLoss',
          header: 'Scale Loss',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
 },
        {
          field: 'qtyPcs',
          header: 'Qty Pcs',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'chopLoss',
          header: 'Chop Loss',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
             ]
    },
    soDetailReport:{
      filterCriteria:{
        formName: 'Filter Criteria',
        formFields: [
          {
            isError: false,
            errorMsg: '',
            ref_key: 'soId',
            label: 'So ID',
            placeHolder: 'So ID',
            value: null,
            name: 'So ID',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.text, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
          {
            isError: false,
            errorMsg: '',
            ref_key: 'lineNo',
            label: '  Line No',
            placeHolder: 'Line No',
            value: null,
            name: 'Line No',
            isRequired: false,
            disabled: false,
            fieldType: FieldType.text, //'text',
            list: [],
            changeEvent: 'dropdownChange',
            colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
            isVisible: true
          },
        ]
      },

      tableColumns:[
        {
          field: 'soId',
          header: 'So ID',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'purOrdNo',
          header: 'Purchase Order No',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'salesOrg',
          header: 'Sales Org',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'branch',
          header: 'Branch',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'disChannel',
          header: 'Dischannel',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'soldToParty',
          header: 'Sold To Party',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',
        },
        {
          field: 'lineNo',
          header: 'Line No',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'orderID',
          header: 'Order ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'materialID',
          header: 'Material ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'routeIndicator',
          header: 'Route Indicator',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'shipToParty',
          header: 'Ship To Party',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'destinationCity',
          header: 'Destination City',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'documentType',
          header: 'Document Type',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'subConFlag',
          header: 'SubConFlag',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',

        },
        {
          field: 'eqSpec',
          header: 'EQ Spec',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'eqSpecGroup',
          header: 'EQ Spec Group',
          columnType: ColumnType.string,
          width: '200px',
          sortFieldName: '',

        },
        {
          field: 'eqSpecSubGroup',
          header: 'EQ Sub Spec',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'orderClass',
          header: 'Order Class',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'edgeCondition',
          header: 'Edge Condition',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'endApllication',
          header: 'End Application',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'orderCategory',
          header: 'Order Category',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'modeOfTransport',
          header: 'Mode Of Transport',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'requireDt',
          header: 'Required Date',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'promiseDt',
          header: 'Promise Date',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'dueDt',
          header: 'Due Date',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'soReleaseDt',
          header: 'So Release Date',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'orderQty',
          header: 'Order Qty',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'orderQtyUom',
          header: 'Order Qty Uom',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'shippingLeewayMin',
          header: 'Shipping Leeway Min',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'shippingLeewayMax',
          header: 'Shipping Leeway Max',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'shippingLeewayUom',
          header: 'Shipping Leeway Uom',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'packingInstruction',
          header: 'Packing Instruction',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'markingInstruction',
          header: 'Marking Instruction',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'LoadingInstruction',
          header: 'Loading Instruction',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'tpInspFlag',
          header: 'TP Insp Flag',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'tpInspAgent',
          header: 'TP Insp Agent',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'custTdcRef',
          header: 'Cust Td Ref',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'priority',
          header: 'Priority',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'ackDt',
          header: 'Ack Date',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'ackBy',
          header: 'Ack By',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'planningType',
          header: 'Planning Type',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'custNameRefer',
          header: 'Cust Name Refer',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'vcFreeText',
          header: 'VC Free Text',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'invoicingWtCategory',
          header: 'Invoicing Wt Category',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'soSource',
          header: 'So Source',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'typeOfLogoSticker',
          header: 'Type Of Logo Sticker',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'testCertificateType',
          header: 'Test Certificate Type',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'unloadingPoint',
          header: 'Unloading Point',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'receivingPoint',
          header: 'Receiving Point',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'routeTree',
          header: 'Route Tree',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'processPath',
          header: 'Process Path',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'materialTree',
          header: 'Material Tree',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'yieldString',
          header: 'Yield String',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'productType',
          header: 'Product Type',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'orderStatus',
          header: 'Order Status',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },
        {
          field: 'prodControlStatus',
          header: 'Prod Control Status',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',

        },

      ]
    },

    ReportsIDs:{
      BatchConsumption_Report: {
        Name: 'BatchConsumption',
        Id:'801',
        SectionIds:
          {
            FiletrCriteriaId: '8011',
            RecordListId: '8012',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      FurnanceResidence_Report:{
        Name: 'FurnaceResidence',
        Id:'802',
        SectionIds:
          {
            FiletrCriteriaId: '8021',
            RecordListId: '8022',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      SizeWise_Report: {
        Name: 'SizeWise',
        Id:'803',
        buttons:null,
        SectionIds:
          {
            FiletrCriteriaId: '8031',
            RecordListId: '8032',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      LPProdDetails_Report: {
        Name: 'LPProdDetails',
        Id:'804',
        SectionIds:
          {
            FiletrCriteriaId: '8041',
            RecordListId: '8042',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      HeatWise_Report: {
        Name: 'HeatWise',
        Id:'805',
        SectionIds:
          {
            FiletrCriteriaId: '8051',
            RecordListId: '8052',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      HotOut_Report: {
        Name: 'HotOut',
        Id:'806',
        SectionIds:
          {
            FiletrCriteriaId: '8061',
            RecordListId: '8062',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      LPProdSummary_Report: {
        Name: 'LPProdSummary',
        Id:'807',
        SectionIds:
          {
            FiletrCriteriaId: '8071',
            RecordListId: '8072',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      LPStock_Report: {
        Name: 'LPStock',
        Id:'808',
        SectionIds:
          {
            FiletrCriteriaId: '8081',
            RecordListId: '8082',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      billetStock_Report: {
        Name: 'BilletStock',
        Id:'812',
        SectionIds:
          {
            FiletrCriteriaId: '8121',
            RecordListId: '8122',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      MechTest_Report: {
        Name: 'MechTest',
        Id:'809',
        SectionIds:
          {
            FiletrCriteriaId: '8091',
            RecordListId: '8092',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      LPRolling_Report: {
        Name: 'LPRolling',
        Id:'810',
        SectionIds:
          {
            FiletrCriteriaId: '8101',
            RecordListId: '8102',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      ShiftWiseProd_Report: {
        Name: 'ShiftWiseProd',
        Id:'811',
        SectionIds:
          {
            FiletrCriteriaId: '8111',
            RecordListId: '8112',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      BatchDetail_Report: {
        Name: 'BatchDetail',
        Id:'812',
        SectionIds:
          {
            FiletrCriteriaId: '8121',
            RecordListId: '8122',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      },
      SoDetail_Report: {
        Name: 'SoDetail',
        Id:'814',
        SectionIds:
          {
            FiletrCriteriaId: '8141',
            RecordListId: '8142',
            buttons: {
              retrieve: "RETRIEVE",
              reset: "RESET",
              pdf:"PDF",
              excel:"EXCEL"
            }
          }
      }
    }
  }

}

