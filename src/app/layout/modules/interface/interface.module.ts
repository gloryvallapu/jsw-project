import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InterfaceRoutingModule } from './interface-routing.module';
import { InterfaceComponent } from './interface.component';
import { LPProductionConfirmationComponent } from './components/lp-production-confirmation/lp-production-confirmation.component';
import { SoModificationComponent } from './components/so-modification/so-modification.component';
import { BatchQualityClearanceComponent } from './components/batch-quality-clearance/batch-quality-clearance.component';
import { BatchConditioningComponent } from './components/batch-conditioning/batch-conditioning.component';
import { LocationChangeComponent } from './components/location-change/location-change.component';
import { HeatChemistryInterfaceComponent } from './components/heat-chemistry-interface/heat-chemistry-interface.component';
import { BatchQualityClearance, InterfaceForms, SOModificationFilterForms, ReceiveSalesOrder, BatchDispatchForms,BatchUpdateFilterForms, HeatchemistryForms, ProcureBatchForms, LptoSMSForms, LPtoQAForms } from './form-objects/interface-form-objects';
import { SharedModule } from 'src/app/shared/shared.module';
import { InterfaceService } from './services/interface.service';
import { ReceiveSalesOrderComponent } from './components/receive-sales-order/receive-sales-order.component';
import { BatchDispatchComponent } from './components/batch-dispatch/batch-dispatch.component';
import { BatchSwappingComponent } from './components/batch-swapping/batch-swapping.component';
import { InterfaceEndpoints } from './form-objects/interface-api-endpoints';
import { BatchUpdateComponent } from './components/batch-update/batch-update.component';
import { ProcureBatchComponent } from './components/procure-batch/procure-batch.component';
import { LpSmsComponent } from './components/lp-sms/lp-sms.component';
import { SmsProdComponent } from './components/sms-prod/sms-prod.component';
import { LpQaComponent } from './components/lp-qa/lp-qa.component';
import { SmsToLpComponent } from './components/sms-to-lp/sms-to-lp.component';




@NgModule({
  declarations: [InterfaceComponent, LPProductionConfirmationComponent, SoModificationComponent, BatchQualityClearanceComponent, BatchConditioningComponent, LocationChangeComponent, HeatChemistryInterfaceComponent, ReceiveSalesOrderComponent, BatchUpdateComponent, BatchSwappingComponent,BatchDispatchComponent, ProcureBatchComponent,LpSmsComponent, SmsProdComponent, LpQaComponent, SmsToLpComponent],
  imports: [
    CommonModule,
    InterfaceRoutingModule,
    SharedModule,
    ],

  providers:[InterfaceService,
    InterfaceForms,
    SOModificationFilterForms,
    BatchQualityClearance,
    ReceiveSalesOrder,
    BatchDispatchForms,
    BatchUpdateFilterForms,
    InterfaceEndpoints,
    HeatchemistryForms,
    ProcureBatchForms,
    LptoSMSForms,
    LPtoQAForms]

})
export class InterfaceModule { }
