import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ReceiveSalesOrder } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { Subject } from 'rxjs';
import { InterfaceService } from '../../services/interface.service';
@Component({
  selector: 'app-receive-sales-order',
  templateUrl: './receive-sales-order.component.html',
  styleUrls: ['./receive-sales-order.component.css'],
})
export class ReceiveSalesOrderComponent implements OnInit {
  public formObject: any;
  public viewType = ComponentType;
  public columnType = ColumnType;
  public columns: ITableHeader[] = [];
  public columns1: ITableHeader[] = [];
  public columns2: ITableHeader[] = [];
  public itemAtrribute: any;
  public tableType: any;
  globalFilterArray: any = [];
  public globalFilterMainArray: any = [];
  public globalFilterSubArray1: any = [];
  public globalFilterSubArray2: any = [];
  public tableRef:any
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public screenStructure: any = [];
  public isReset:boolean= false;
  public isRetrive:boolean= false;
  public isTransfer:boolean = false;
  public isAttriSave:boolean =true;
  public isMechSave:boolean = true;
  public isReceiveSalesFilterSection:boolean = false;
  public isHeatFilterSection:boolean = false;
  public isAttriSection:boolean = false;
  public isMechSection:boolean = false;
  griddisplay:string='col-6';
  public materialIdList = [];
  public selectedMaterialId=[];
  public salesList: any;
  public ChemMechDetails: any;
  public isRunInterface:boolean = false;
  public isDownload:boolean =false;
  public subFirstTableRef: any;
  public subSecondTableRef: any;
  public requestBody = {
    "fromDate": null,
    "toDate": null,
    "salesOrder": null,
    "lineItemNo": null,
    "interfaceStatus": null
  }
  constructor(
    public receiveSalesOrder: ReceiveSalesOrder,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService: InterfaceService,
    public authService: AuthService
  ) {


  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.formObject =JSON.parse(JSON.stringify( this.receiveSalesOrder.receiveFilterCriteria));
    this.columns = this.receiveSalesOrder.receiveFilterCriteria.mainTableColumns;
    this.columns1 = this.receiveSalesOrder.receiveFilterCriteria.subTable1Columns;
    this.columns2 = this.receiveSalesOrder.receiveFilterCriteria.subTable2Columns;
    this.assignGlobalFilters();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.itemAtrribute = [];
    this.getLovs(LovCodes.int_stat, 'interfaceStatus');
    this.getSalesDropdown();
  }

  public assignGlobalFilters(){
    this.globalFilterMainArray = this.columns.map((element) => {
      return element.field;
    });

    this.globalFilterSubArray1 = this.columns1.map((element) => {
      return element.field;
    });

    this.globalFilterSubArray2 = this.columns2.map((element) => {
      return element.field;
    });
  }

  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }
  public formValidationsResult(): any {
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();
    if (formData == undefined) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: `Please Enter Required Fields `,
      }
      );
    }
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.sharedService.getReqBody(this.formObject.formFields));
    return formObj
  };
  public getSalesDropdown(){
      this.interfaceService.getSalesOrderLineItemDropdown().subscribe((response: any)=>{
        if(response){
          this.formObject.formFields.filter(
            (obj: any) => obj.ref_key == 'salesOrder'
          )[0].list = response.map((x: any) => {
            return { displayName: x.salesOrder, modelValue: x.salesOrder, lineItemList: x.lineItemNoList };
          });
        }
      })
  }
  public dropdownChangeEvent(event) {
    if (event.ref_key == 'salesOrder' && event.value != null) {
      let heatIdListByUnit = this.formObject.formFields
        .filter((ele) => ele.ref_key == 'salesOrder')[0]
        .list.filter((x: any) => x.modelValue == event.value)[0];

      this.formObject.formFields.filter(
        (ele) => ele.ref_key == 'lineItemNo'
      )[0].list = heatIdListByUnit.lineItemList.map((x: any) => {
        return {
          displayName: x,
          modelValue: x,
        };
      });
    }
    else if(event.ref_key == 'salesOrder' && event.value == null){
      this.jswComponent.resetForm(this.formObject);
      this.formObject.formFields.filter(
        (ele) => ele.ref_key == 'lineItemNo'
      )[0].list = [];
    }
  }
  public getTableRef(event){
    this.tableRef = event;
  }

  public getSubFirstTableRef(event){
    this.subFirstTableRef = event;
  }

  public getSubSecondTableRef(event){
    this.subSecondTableRef = event;
  }

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.ReceiveSalesOrder.id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, menuConstants.ReceiveSalesSectionIds.ReceiveSalesFilter_Criteria.buttons.retrieve, menuConstants.ReceiveSalesSectionIds.ReceiveSalesFilter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.ReceiveSalesSectionIds.ReceiveSalesFilter_Criteria.buttons.reset, menuConstants.ReceiveSalesSectionIds.ReceiveSalesFilter_Criteria.id, this.screenStructure);
    //this.isTransfer = this.sharedService.isButtonVisible(true, menuConstants.ReceiveSalesSectionIds.HeatFilter_Criteria.buttons.transfer, menuConstants.ReceiveSalesSectionIds.HeatFilter_Criteria.id, this.screenStructure);
    this.isAttriSave = this.sharedService.isButtonVisible(true, menuConstants.ReceiveSalesSectionIds.Attri_save.buttons.save, menuConstants.ReceiveSalesSectionIds.Attri_save.id, this.screenStructure);
    this.isMechSave = this.sharedService.isButtonVisible(true, menuConstants.ReceiveSalesSectionIds.Mech_Save.buttons.save, menuConstants.ReceiveSalesSectionIds.Mech_Save.id, this.screenStructure);
    this.isReceiveSalesFilterSection = this.sharedService.isSectionVisible(menuConstants.ReceiveSalesSectionIds.ReceiveSalesFilter_Criteria.id, this.screenStructure);
    this.isHeatFilterSection = this.sharedService.isSectionVisible(menuConstants.ReceiveSalesSectionIds.HeatFilter_Criteria.id, this.screenStructure);
    //this.isAttriSection = this.sharedService.isSectionVisible(menuConstants.ReceiveSalesSectionIds.Attri_save.id, this.screenStructure);
    //this.isMechSection = this.sharedService.isSectionVisible(menuConstants.ReceiveSalesSectionIds.Mech_Save.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.ReceiveSalesSectionIds.HeatFilter_Criteria.buttons.runInterface, menuConstants.ReceiveSalesSectionIds.HeatFilter_Criteria.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.ReceiveSalesSectionIds.HeatFilter_Criteria.buttons.download, menuConstants.ReceiveSalesSectionIds.HeatFilter_Criteria.id, this.screenStructure);
    if(this.isAttriSection==true && this.isMechSection==false)
    {
       this.griddisplay='col-12'
    }
    else if(this.isMechSection==true && this.isAttriSection==false)
    {
      this.griddisplay='col-12'
    }
    else if(this.isMechSection==false && this.isAttriSection==false)
    {
      this.griddisplay='col-6'
    }
  }

  public filterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }

  public filterItemAttrTable(event){
    this.subFirstTableRef.filterGlobal(event.target.value, 'contains');
  }

  public filterChemMechTable(event){
    this.subSecondTableRef.filterGlobal(event.target.value, 'contains');
  }

  public retrieveSalesList(){
    let formObj = this.formValidationsResult();
    var valueExists = false;
    Object.keys(formObj).forEach(function (key) {
      if (formObj[key] != null) {
        valueExists = true;
      }
    });
    if(valueExists){
      this.salesList = [];
      this.itemAtrribute = [];
      this.ChemMechDetails = [];
      this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
      var formData = this.jswService.getFormData();
      if (formData && formData.length) {
        let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.requestBody);
        this.requestBody = {
          "fromDate": (formObj.fromDate!="")?formObj.fromDate:null,
          "toDate": (formObj.toDate!="")?formObj.toDate:null,
          "salesOrder": (formObj.salesOrder!="")?formObj.salesOrder:null,
          "lineItemNo": (formObj.lineItemNo!="")?formObj.lineItemNo:null,
          "interfaceStatus": (formObj.interfaceStatus!="")?formObj.interfaceStatus:null
        }
        this.interfaceService.getSalesOrderFilterDetails(this.requestBody).subscribe(Response => {
          Response.map((rowData: any) => {
            return rowData;
          })
          this.salesList = Response
        })
      }
    }
    else {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: `Please Select a Fields `,
      }
      );
    }
  }


  public resetForm() {
    this.salesList = [];
    this.itemAtrribute = [];
    this.ChemMechDetails = [];
    this.jswComponent.resetForm(this.formObject);
    this.formObject.formFields.filter(
      (ele) => ele.ref_key == 'lineItemNo'
    )[0].list = [];
  }

  public selectedSalesOrder(event: any){
    this.itemAtrribute = [];
    this.ChemMechDetails = [];
    if(event){
      this.interfaceService.getSalesOrderDetailsByOrderNumber(event.salesOrder,event.soItem).subscribe((data: any)=>{
        if(data.length){
          this.itemAtrribute = data[0].salesOrderPropertyBoList;
          this.ChemMechDetails = data[1].salesOrderPropertyBoList;
        }
      })
    }

  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele:any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.salesList, keyArr);
      this.sharedService.downloadCSV(data, 'Recieve_Sales_Order', tableHeader);
    }
  }

  public runInterface() {
    this.interfaceService.runReceiveSalesOrderInterface()
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          {
            message: Response,
          }
        );

      });
  }

}
