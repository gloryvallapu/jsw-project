import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiveSalesOrderComponent } from './receive-sales-order.component';

describe('ReceiveSalesOrderComponent', () => {
  let component: ReceiveSalesOrderComponent;
  let fixture: ComponentFixture<ReceiveSalesOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceiveSalesOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiveSalesOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
