import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LpSmsComponent } from './lp-sms.component';

describe('LpSmsComponent', () => {
  let component: LpSmsComponent;
  let fixture: ComponentFixture<LpSmsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LpSmsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LpSmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
