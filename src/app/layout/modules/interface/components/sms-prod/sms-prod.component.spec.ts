import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsProdComponent } from './sms-prod.component';

describe('SmsProdComponent', () => {
  let component: SmsProdComponent;
  let fixture: ComponentFixture<SmsProdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmsProdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsProdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
