import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { LptoSMSForms} from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';

@Component({
  selector: 'app-sms-prod',
  templateUrl: './sms-prod.component.html',
  styleUrls: ['./sms-prod.component.css']
})
export class SmsProdComponent implements OnInit {
  public formObject: any;
  public columnType = ColumnType;
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public productionList: any = [];
  statusFlag: any;
  public requestBody = {
    "fromDate": null,
    "toDate": null,
    "statusFlag": null,
    "eventName":null
  }
  public screenStructure: any = [];
  public isReset:boolean= false;
  public isRetrieve:boolean= false;
  public isRunInterface:boolean = false;
  public isDownload:boolean =false;
  public isFilterSection:boolean = false;
  public isListSection:boolean =false;

  constructor(public smsProdForms: LptoSMSForms,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService: InterfaceService,
    public authService: AuthService) {
      this.columns = [
        {
          field: 'msgId',
          header: 'Message ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'schId',
          header: 'Schedule ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'plannedHeatId',
          header: 'Planned Heat ID',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'eventCode',
          header: 'Event Code',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'interfaceStatus',
          header: 'Interface Status',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'errorMsg',
          header: 'Error Message',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'createdBy',
          header: 'Created By',
          columnType: ColumnType.string,
          width: '230px',
          sortFieldName: '',
        },
        {
          field: 'createdDate',
          header: 'Created Date',
          columnType: ColumnType.date,
          width: '230px',
          sortFieldName: '',
        },
        {
          field: 'modifiedBy',
          header: 'Modified By',
          columnType: ColumnType.string,
          width: '180px',
          sortFieldName: '',
        },
        {
          field: 'modifiedDate',
          header: 'Modified Date',
          columnType: ColumnType.date,
          width: '230px',
          sortFieldName: '',
        },

      ];
    }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.smsProdForms.LptoSMSFilterCriteria));
     this.filterData();
     this.getLovs(LovCodes.int_stat, 'statusFlag');
     this.getLovs(LovCodes.lp_sms, 'eventName');
  }
  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.SMS_Prod.id)[0];
    this.isRetrieve = this.sharedService.isButtonVisible(true, menuConstants.SMS_ProdSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.SMS_ProdSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.SMS_ProdSectionIds.Filter_Criteria.buttons.reset, menuConstants.SMS_ProdSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.SMS_ProdSectionIds.SMSProd_List.buttons.runInterface, menuConstants.SMS_ProdSectionIds.SMSProd_List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.SMS_ProdSectionIds.SMSProd_List.buttons.download, menuConstants.SMS_ProdSectionIds.SMSProd_List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.SMS_ProdSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.SMS_ProdSectionIds.SMSProd_List.id, this.screenStructure);
  }
 public filterData(){

}

  public resetFilter() {
    this.jswComponent.resetForm(this.formObject);
    this.productionList=[];
  }

   public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }

}
