import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { InterfaceForms } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-batch-conditioning',
  templateUrl: './batch-conditioning.component.html',
  styleUrls: ['./batch-conditioning.component.css']
})
export class BatchConditioningComponent implements OnInit {
  public openRepushInterfaceModel: boolean = false;
  public formObject: any;
  public columnType = ColumnType;
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public batchConditioningList: any;
  public requestBody = {
    "fromDate": null,
    "toDate": null,
    "batchId": null,
    "heatId": null,
    "statusFlag": null
  }
  public setBatchId = {
    "setBatchId": null,
  }
  public screenStructure: any = [];
  public isReset: boolean = false;
  public isRetrive: boolean = false;
  public isRunInterface: boolean = false;
  public isDownload: boolean = false;
  public isRepushInterface: boolean = false;
  public selectedRecord: any;
  public isFilterSection: boolean = false;
  public isListSection: boolean = false;

  constructor(
    public batchCondFilterForms: InterfaceForms,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService: InterfaceService,
    public authService: AuthService
  ) {
    this.columns = [
      {
        field: 'radio',
        header: '',
        columnType: ColumnType.radio,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'salesOrder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'heat',
        header: 'Heat Number',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'setBatchId',
        header: 'Set Batch ID',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'counter',
        header: 'Counter',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'materialDesc',
        header: 'Material',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'parameterType',
        header: 'Parameter Type',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchWt',
        header: 'Batch WT(tons)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'salesOrderItem',
        header: 'Sales Order Item',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'workCenter',
        header: 'Work Center',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'productionDate',
        header: 'Production Date',
        columnType: ColumnType.date,
        width: '190px',
        sortFieldName: '',
      },
      {
        field: 'shift',
        header: 'Shift',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'dia',
        header: 'Batch Diameter(mm)',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'thickness',
        header: 'Batch Thickness(mm)',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'width',
        header: 'Batch Width(mm)',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Batch Length(mm)',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'jswGrade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'PLANT',
        header: 'PSN No',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'productionRemark',
        header: 'Production Remark',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'qualityRemark',
        header: 'Quality Remark',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'eqSpec',
        header: 'EQ Spec',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'eqSubSpec',
        header: 'EQ Sub Spec',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'ncoDeclared',
        header: 'NCO Declared',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'ncoReason',
        header: 'NCO Reason',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchLocation',
        header: 'Batch Location',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'nextWorkCenter',
        header: 'Next WorkCenter',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'statusDesc',
        header: 'Status Flag',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'errorDescription',
        header: 'Error Description',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'createdDate',
        header: 'Date Created',
        columnType: ColumnType.date,
        width: '190px',
        sortFieldName: '',
      },
      {
        field: 'modifiedDate',
        header: 'Date Modified',
        columnType: ColumnType.date,
        width: '190px',
        sortFieldName: '',
      },
    ];
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    // this.formObject = this.batchCondFilterForms.BatchCondFilterCriteria;
    this.formObject = JSON.parse(JSON.stringify(this.batchCondFilterForms.filterCriteria));
    //this.getConditioningDetails();
    this.getLovs(LovCodes.int_stat, 'statusFlag');
  }
  public screenRightsCheck() {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.BatchConditioning.id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, menuConstants.BtachConditioningSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.BtachConditioningSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.BtachConditioningSectionIds.Filter_Criteria.buttons.reset, menuConstants.BtachConditioningSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.BtachConditioningSectionIds.BatchConditioning_List.buttons.runInterface, menuConstants.BtachConditioningSectionIds.BatchConditioning_List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.BtachConditioningSectionIds.BatchConditioning_List.buttons.download, menuConstants.BtachConditioningSectionIds.BatchConditioning_List.id, this.screenStructure);
    this.isRepushInterface = this.sharedService.isButtonVisible(true, menuConstants.BtachConditioningSectionIds.BatchConditioning_List.buttons.repushInterface, menuConstants.BtachConditioningSectionIds.BatchConditioning_List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.BtachConditioningSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.BtachConditioningSectionIds.BatchConditioning_List.id, this.screenStructure);
  }


  public getSelectedRecord(rowData) {
    this.selectedRecord = rowData;
    this.isRepushInterface = true;
  }

  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }

  public getConditioningDetails(): any {
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.requestBody);
      this.requestBody = {
        "fromDate": (formObj.fromDate != "") ? formObj.fromDate : null,
        "toDate": (formObj.toDate != "") ? formObj.toDate : null,
        "batchId": (formObj.batchId != "") ? formObj.batchId : null,
        "heatId": (formObj.heatId != "") ? formObj.heatId : null,
        "statusFlag": (formObj.statusFlag != "") ? formObj.statusFlag : null
      }
      this.interfaceService.getConditioningDetails(this.requestBody).subscribe(Response => {
        Response.map((rowData: any) => {
          return rowData;
        })
        this.batchConditioningList = Response
      })
    }
  }

  public textBoxEventHandling(formObj) {
    for (let i = 0; i < formObj.formFields.length; i++) { // unit textbox change event
      if (formObj.formFields[i].isError) {
        formObj.formFields[i].value = ''
      }
      formObj.formFields[i].isError = false
    }
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele: any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.batchConditioningList, keyArr);
      this.sharedService.downloadCSV(data, 'Batch_Conditioning', tableHeader);
    }
  }

  public resetFilter() {
    this.jswComponent.resetForm(this.formObject);
    this.clearSelection.next(true);
    this.selectedRecord = [];
    this.isRepushInterface = false;
    this.batchConditioningList = [];
  }

  public closeBatchConditioningInterfcePopup() {
    this.openRepushInterfaceModel = false;
  }

  public BatchConditioningInterfcePopup() {
    this.openRepushInterfaceModel = true;
    this.setBatchId = {
      "setBatchId": this.selectedRecord.setBatchId
    }
  }

  //Vaibhav
  public BatchConditioningInterfce() {
    this.openRepushInterfaceModel = false;
    this.setBatchId = {
      "setBatchId": this.selectedRecord.setBatchId
    }
    this.interfaceService.pushBackConditioningDetailsToSap(this.setBatchId).subscribe(Response => {
      this.clearSelection.next(true);
      this.selectedRecord = [];
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        {
          message: `Push back conditioning details to SAP Successfuly.`
        }
      );
    })
  }

}
