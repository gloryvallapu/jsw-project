import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchConditioningComponent } from './batch-conditioning.component';

describe('BatchConditioningComponent', () => {
  let component: BatchConditioningComponent;
  let fixture: ComponentFixture<BatchConditioningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BatchConditioningComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchConditioningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
