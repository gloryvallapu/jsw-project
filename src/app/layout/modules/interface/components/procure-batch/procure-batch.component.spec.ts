import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcureBatchComponent } from './procure-batch.component';

describe('ProcureBatchComponent', () => {
  let component: ProcureBatchComponent;
  let fixture: ComponentFixture<ProcureBatchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcureBatchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcureBatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
