import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ProcureBatchForms, ReceiveSalesOrder } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { Subject } from 'rxjs';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-procure-batch',
  templateUrl: './procure-batch.component.html',
  styleUrls: ['./procure-batch.component.css']
})
export class ProcureBatchComponent implements OnInit {
  public formObject: any;
  public viewType = ComponentType;
  public columnType = ColumnType;
  public columns: ITableHeader[] = [];
  public batchDetails: any = [];
  public itemAtrribute: any;
  public tableType: any;
  public globalFilterArray: any = [];
  public tableRef:any
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public screenStructure: any = [];
  public isReset:boolean= false;
  public isRetrieve:boolean= false;
  public isRunInterface:boolean = false;
  public isDownload:boolean =false;
  public isFilterSection:boolean = false;
  public isListSection:boolean =false;
  constructor(
    public procureBatchForms: ProcureBatchForms,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService: InterfaceService,
    public authService: AuthService
  ) {
    this.columns = [
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch Id',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'material',
        header: 'Material',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'jswHeatNo',
        header: 'Heat No.',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchWt',
        header: 'Batch Weight(tons)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'coiling_Temp',
        header: 'Coiling Temp',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },

      {
        field: 'finishingTemp',
        header: 'Finishing Temp',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'purchaseOrderItem',
        header: 'Purchase Order Item',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'purchaseOrderNumber',
        header: 'Prchase Order Number',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },


      {
        field: 'purchasedCoilFlag',
        header: 'Purchased Coil Flag',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'receivingDate',
        header: 'Receiving Date',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'saluminiumPct',
        header: 'Saluminium Pct',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'sarsenicPct',
        header: 'S Arsenic Pct',
        columnType: ColumnType.string,
        width: '170px',
        sortFieldName: '',
      },
      {
        field: 'hrProductionDate',
        header: 'Hr Production Date',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'Sbend',
        header: 'S bend',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'sboronPct',
        header: 'S Boron Pct',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'scalciumPpm',
        header: 'S Calcium Ppm',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },

      {
        field: 'scarbonEqLong',
        header: 'Scar Bon Eq Long',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'scarbonEqShort',
        header: 'Scar Bon Eq Short',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'scarbonPct',
        header: 'Scar Bon Pct',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'schromiumPct',
        header: 'S Chromium Pct',
        columnType: ColumnType.string,
        width: '190px',
        sortFieldName: '',
      },
      {
        field: 'scoilIdMm',
        header: 'S CoilId Mm',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'scoilOdMm',
        header: 'S CoilOd Mm',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'scopperPct',
        header: 'S Copper Pct',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'sel',
        header: 'Sel',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'seqSpec',
        header: 'Seq Spec',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'seqSpecGrp',
        header: 'Seq Spec Grp',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'seqSubSpec',
        header: 'Seq Sub Spec',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'shrb',
        header: 'Shrb',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'sjswGrade',
        header: 'S Jsw Grade',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'slengthMm',
        header: 'S length Mm',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'smanganesePct',
        header: 'S Manganese Pct',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'smolybdenumPct',
        header: 'S Molybdenum Pct',
        columnType: ColumnType.string,
        width: '190px',
        sortFieldName: '',
      },
      {
        field: 'snickelPct',
        header: 'S Nickel Pct',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'sniobiumPct',
        header: 'S Niobium Pct',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'snitrogenPct',
        header: 'S Nitrogen Pct',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'snoOfPcs',
        header: 'S No Of Pcs',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'soxygenPpm',
        header: 'S Oxygen Ppm',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'sphosphorusPct',
        header: 'S Phosphorus Pct',
        columnType: ColumnType.string,
        width: '170px',
        sortFieldName: '',
      },
      {
        field: 'ssiliconPct',
        header: 'S Silicon Pct',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'ssulphurPct',
        header: 'S Sulphur Pct',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'statusDesc',
        header: 'Status Description',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'statusFlag',
        header: 'Status Flag',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'sthicknessMm',
        header: 'S Thickness Mm',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'stinPct',
        header: 'Stin Pct',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'stitaniumPct',
        header: 'S Titanium Pct',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'stsMpa',
        header: 'S Ts Mpa',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'stungstenPct',
        header: 'Stung Sten Pct',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'sutsYs',
        header: 'S uts Ys',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'svanadiumPct',
        header: 'S Vanadium Pct',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'swidthMm',
        header: 'S Width Mm',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'sysMpa',
        header: 'S Ys Mpa',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'sysutr',
        header: 'S Ysu Tr',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'szirconiumPct',
        header: 'S zirconium Pct',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 't1',
        header: 'T1',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'vendBatchId',
        header: 'Vendor Batch Id',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'vendor',
        header: 'Vendor',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'vendorGrade',
        header: 'Vendor Grade',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'vendorHeatNo',
        header: 'Vendor Heat No',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'dateCreated',
        header: 'Date Created',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'dateModified',
        header: 'Date Modified',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
    ];
    this.globalFilterArray = this.columns.map((element) => {
      return element.field;
    });
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.getAllBatchProcureDetails()
    this.formObject = JSON.parse(JSON.stringify(this.procureBatchForms.procureBatchFilterCriteria));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.itemAtrribute = [
      {
        plant: '1',
        SetBatchID: '2',
        MATERIAL: '3',
      },
      {
        plant: '2',
        SetBatchID: 'test',
        MATERIAL: '3',
      },
      {
        plant: '3',
        SetBatchID: '2',
        MATERIAL: '3',
      },
    ];
  };

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.ProcureBatch.id)[0];
    this.isRetrieve = this.sharedService.isButtonVisible(true, menuConstants.ProcureBatchSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.ProcureBatchSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.ProcureBatchSectionIds.Filter_Criteria.buttons.reset, menuConstants.ProcureBatchSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.ProcureBatchSectionIds.ProcureBatch_List.buttons.runInterface, menuConstants.ProcureBatchSectionIds.ProcureBatch_List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.ProcureBatchSectionIds.ProcureBatch_List.buttons.download, menuConstants.ProcureBatchSectionIds.ProcureBatch_List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.ProcureBatchSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.ProcureBatchSectionIds.ProcureBatch_List.id, this.screenStructure);
  }
  public runBatchHeaderChemOBInterface(){
    this.interfaceService.runBatchHeaderChemOBInterface(this.sharedService.loggedInUserDetails.userId).subscribe(Response =>{
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        {
          message: Response,
        }
      );
      this.getAllBatchProcureDetails();
    })
  }

  public resetForm() {
    this.jswComponent.resetForm(this.formObject);
    this.getAllBatchProcureDetails()
  }

  public filterData(){
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    let requestBody = {
      "fromDate": null,
      "toDate": null,
      "heatId": null
    }
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, requestBody);
    this.interfaceService.getFilteredBatchProcureDetails(formObj).subscribe(Response =>{
      Response.map((obj:any)=>{
        obj.dateCreated =  this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.dateCreated)
        obj.dateModified =  this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.dateModified)
        obj.receivingDate =  this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.receivingDate)
        obj.hrProductionDate =  this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.hrProductionDate)
      })
      this.batchDetails = Response
    })
  }

  public getAllBatchProcureDetails(){
    let requestBody = {
      "fromDate": null,
      "toDate": null,
      "heatId": null
    }
    this.interfaceService.getFilteredBatchProcureDetails(requestBody).subscribe(Response =>{
      Response.map((obj:any)=>{
        obj.dateCreated =  this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.dateCreated)
        obj.dateModified =  this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.dateModified)
        obj.receivingDate =  this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.receivingDate)
        obj.hrProductionDate =  this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.hrProductionDate)
      })
      this.batchDetails = Response
    })
  }
  public getTableRef(event){
    this.tableRef = event;
  }

  public filterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele:any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.batchDetails, keyArr); 
      this.sharedService.downloadCSV(data, 'Procure_Batch', tableHeader);
    }
  }

}
