import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BatchSwappingComponent } from './batch-swapping.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { InterfaceForms, SOModificationFilterForms } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { InterfaceService } from '../../services/interface.service';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { InterfaceEndpoints } from '../../form-objects/interface-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('BatchSwappingComponent', () => {
  let component: BatchSwappingComponent;
  let fixture: ComponentFixture<BatchSwappingComponent>;
  let soModificationFilterForms: SOModificationFilterForms;
  let jswService: JswCoreService;
  let jswComponent: JswFormComponent;
  let sharedService: SharedService;
  let commonService: CommonApiService;
  let interfaceService: InterfaceService;
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BatchSwappingComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        InterfaceService,
       CommonApiService,
        JswFormComponent,
        InterfaceEndpoints,
        API_Constants,
        CommonAPIEndPoints,
        InterfaceForms,
        SOModificationFilterForms,

        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchSwappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
