import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SOModificationFilterForms } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';

@Component({
  selector: 'app-batch-swapping',
  templateUrl: './batch-swapping.component.html',
  styleUrls: ['./batch-swapping.component.css']
})
export class BatchSwappingComponent implements OnInit {
  public formObject: any;
  public columnType = ColumnType;
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public BatchList: any = [];
  statusFlag: any;
  public requestBody = {
    "fromDate": null,
    "toDate": null,
    "salesOrder": null,
    "lineItemNo": null,
    "statusFlag": null
  }
  public screenStructure: any = [];
  public isReset:boolean= false;
  public isRetrive:boolean= false;
  public isRunInterface:boolean = false;
  public isDownload:boolean =false;
  public isFilterSection:boolean = true;
  public isListSection:boolean =true;
  constructor(
    public authService: AuthService,
    public batchSwapFilterForms: SOModificationFilterForms,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService: InterfaceService,
  ) {
    this.columns = [
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'material',
        header: 'Material',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'salesOrderNo',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'salesOrderItem',
        header: 'Sales Order item',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'counter',
        header: 'Counter',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'statusDescription',
        header: 'Status flag',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'errorDescription',
        header: 'Error Description',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'createdDate',
        header: 'Date Created',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'modifiedDate',
        header: 'Date Modified',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },

    ];
  }

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.FGBatchSwap.id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, menuConstants.FGBatchSwapInterfaceSectionsIds.Filter_Criteria.buttons.retrieve, menuConstants.FGBatchSwapInterfaceSectionsIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.FGBatchSwapInterfaceSectionsIds.Filter_Criteria.buttons.reset, menuConstants.FGBatchSwapInterfaceSectionsIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.FGBatchSwapInterfaceSectionsIds.List.buttons.runInterface, menuConstants.FGBatchSwapInterfaceSectionsIds.List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.FGBatchSwapInterfaceSectionsIds.List.buttons.download, menuConstants.FGBatchSwapInterfaceSectionsIds.List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.FGBatchSwapInterfaceSectionsIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.FGBatchSwapInterfaceSectionsIds.List.id, this.screenStructure);
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.getBatchSwapInterfaceFilterCriteriaDetails();
    this.getBatchSwapInterfaceBatchDetailsOnLoad();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject =JSON.parse(JSON.stringify(this.batchSwapFilterForms.soFilterCriteria));
    this.getLovs(LovCodes.int_stat, 'statusFlag');
  }

  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }
  public getBatchSwapInterfaceFilterCriteriaDetails() {
    this.interfaceService.getBatchSwapInterfaceFilterCriteriaDetails().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'salesOrder'
      )[0].list = data.map((x: any) => {
        return { displayName: x.salesOrderNo, modelValue: x.salesOrderNo, lineItemList: x.salesOrderItemList };
      });
    });
  }
 
  public getBatchSwapInterfaceBatchDetailsOnLoad() {
    this.interfaceService.getBatchSwapInterfaceBatchDetailsOnLoad().subscribe(Response => {
      this.BatchList = Response;
    })
  }

  public getBtachSwapInterfaceBatchDetails(): any {
    debugger
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.requestBody);
      this.requestBody = {
        "fromDate": (formObj.fromDate!="")?formObj.fromDate:null,
        "toDate": (formObj.toDate!="")?formObj.toDate:null,
        "salesOrder": (formObj.salesOrder!="")?formObj.salesOrder:null,
        "lineItemNo": (formObj.lineItemNo!="")?formObj.lineItemNo:null,
        "statusFlag": (formObj.statusFlag!="")?formObj.statusFlag:null
      }
    this.interfaceService.getBtachSwapInterfaceBatchDetails(this.requestBody).subscribe(Response => {
      Response.map((rowData: any) => {
        return rowData;
      })
      this.BatchList = Response
    })
  }
}
  public runBatchSwapInterface() {
    this.interfaceService.runBatchSwapInterface()
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          {
            message: Response,
          }
        );

      });
  }
  public dropdownChangeEvent(event) {
    if (event.ref_key == 'salesOrder') {
    }
    if (event.ref_key == 'salesOrder' && event.value != null) {
      let heatIdListByUnit = this.formObject.formFields
        .filter((ele) => ele.ref_key == 'salesOrder')[0]
        .list.filter((x: any) => x.modelValue == event.value)[0];
      this.formObject.formFields.filter(
        (ele) => ele.ref_key == 'lineItemNo'
      )[0].list = heatIdListByUnit.lineItemList.map((x: any) => {
        return {
          displayName: x,
          modelValue: x,
        };
      });
    }
  }
  public resetForm() {
    this.jswComponent.resetForm(this.formObject);
    this.formObject.formFields.filter(
      (ele) => ele.ref_key == 'lineItemNo'
    )[0].list = [];
   this.BatchList=[];
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele:any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.BatchList, keyArr); 
      this.sharedService.downloadCSV(data, 'Batch_Swapping', tableHeader);
    }
  }
}
