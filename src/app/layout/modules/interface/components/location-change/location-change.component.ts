import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { InterfaceForms } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { locationChangeBo } from '../../../user-management/interfaces/user-master-interface';
@Component({
  selector: 'app-location-change',
  templateUrl: './location-change.component.html',
  styleUrls: ['./location-change.component.css']
})
export class LocationChangeComponent implements OnInit {
  public formObject: any;
  public columnType = ColumnType;
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public batchList: any;
  public statusFlag: any = null;
  public requestBody = {
    "fromDate": null,
    "toDate": null,
    "batchId": null,
     "heatId": null,
    "statusFlag": null
  }
  public batchIds: any;
  public locationStorageParams: locationChangeBo = new locationChangeBo();
  public openRepushInterfaceModel: boolean = false;
  public isRepushInterface: boolean = false;
  public selectedRecord: any;
  public screenStructure: any = [];
  public isReset:boolean= false;
  public isRetrive:boolean= false;
  public isRunInterface:boolean = false;
  public isDownload:boolean =false;
  public isFilterSection:boolean = true;
  public isListSection:boolean =true;
  constructor(
    public authService: AuthService,
    public locationChangeFilterForms: InterfaceForms,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public interfaceService: InterfaceService,
    public commonService: CommonApiService
  ) {
    this.columns = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'material',
        header: 'Material',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'counter',
        header: 'Counter',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'receivedSloc',
        header: 'Received Location',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'quantity',
        header: 'QTY',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'unitOfMeasurement',
        header: 'UOM',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'statusFlag',
        header: 'Status Flag',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'errorDesp',
        header: 'Error Description',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'dateCreated',
        header: 'Date Created',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
        dateType: 'short'
      },
      {
        field: 'dateModified',
        header: 'Date Modified',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
        dateType: 'short'
      },

    ];
  }

    public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.LocationChange.id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, menuConstants.LocationChangeSectionsIds.Filter_Criteria.buttons.retrieve, menuConstants.LocationChangeSectionsIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.LocationChangeSectionsIds.Filter_Criteria.buttons.reset, menuConstants.LocationChangeSectionsIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.LocationChangeSectionsIds.List.buttons.runInterface, menuConstants.LocationChangeSectionsIds.List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.LocationChangeSectionsIds.List.buttons.download, menuConstants.LocationChangeSectionsIds.List.id, this.screenStructure);
    this.isRepushInterface = this.sharedService.isButtonVisible(true, menuConstants.LocationChangeSectionsIds.List.buttons.repushInterface, menuConstants.LocationChangeSectionsIds.List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.LocationChangeSectionsIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.LocationChangeSectionsIds.List.id, this.screenStructure);
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    // this.formObject = this.locationChangeFilterForms.filterCriteria;
    this.formObject = JSON.parse(JSON.stringify(this.locationChangeFilterForms.filterCriteria));
    this.getLocationChangedBatches();
    this.getLovs(LovCodes.int_stat, 'statusFlag');
  }

  public getSelectedRecord(rowData) {
    this.selectedRecord = rowData;
    this.isRepushInterface = true;
    console.log(this.selectedRecord);
  }

  public resetFilter() {
    this.jswComponent.resetForm(this.formObject);
    this.clearSelection.next(true);
    this.selectedRecord = [];
    this.isRepushInterface = false;
    this.batchList=[];
  }

  public getLocationChangedBatches(): any {
   this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();

    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData,this.requestBody);
      this.requestBody = {
        "fromDate": (formObj.fromDate!="")?formObj.fromDate:null,
        "toDate": (formObj.toDate!="")?formObj.toDate:null,
        "batchId": (formObj.batchId!="")?formObj.batchId:null,
        "heatId": (formObj.heatId!="")?formObj.heatId:null,
        "statusFlag": (formObj.statusFlag!="")?formObj.statusFlag:null
      }
      this.interfaceService.getLocationChangedBatches(this.requestBody).subscribe(Response => {
        Response.map((rowData: any) => {
          return rowData;
        })
        this.batchList = Response
      })
    }
}

  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele:any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.batchList, keyArr); 
      this.sharedService.downloadCSV(data, 'Location_Change', tableHeader);
    }
  }

  public textBoxEventHandling(formObj){
    for (let i = 0; i < formObj.formFields.length; i++) { // unit textbox change event
      if(formObj.formFields[i].isError)
      {
        formObj.formFields[i].value=''
      } 
      formObj.formFields[i].isError=false
    }
  }

  public closeStorageLocationInterfcePopup() {
    this.openRepushInterfaceModel = false;
  }

  public locationChangeInterfcePopup() {
    this.openRepushInterfaceModel = true;
    this.batchIds = this.selectedRecord.map((x: any) => {
      return x.batchId;
    });
  }

 //vaibhav
public pushBackStorageLocation()
{
  this.openRepushInterfaceModel = false;
  this.locationStorageParams.storageLocationIbRePushFilterCriteriaDataBo = this.selectedRecord.map((x: any) => {
    return {
      "batchId": x.batchId,
      "counter": x.counter
    }
  })
  this.interfaceService.pushBackStorageLocationDataToSap(this.locationStorageParams).subscribe(Response =>{
    this.clearSelection.next(true);
    this.selectedRecord = [];
    this.isRepushInterface = false;
    return this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Success,
      {
        message: `Push back location details to SAP Successfuly`
      }
    );
  })
}
}
