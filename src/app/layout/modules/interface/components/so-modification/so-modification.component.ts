import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SOModificationFilterForms } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-so-modification',
  templateUrl: './so-modification.component.html',
  styleUrls: ['./so-modification.component.css'],
})
export class SoModificationComponent implements OnInit {
  public formObject: any;
  public columnType = ColumnType;
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public productionList: any = [];
  statusFlag: any;
  public requestBody = {
    "fromDate": null,
    "toDate": null,
    "salesOrder": null,
    "lineItemNo": null,
    "statusFlag": null
  }
  public screenStructure: any = [];
  public isReset:boolean= false;
  public isRetrive:boolean= false;
  public isRunInterface:boolean = false;
  public isDownload:boolean =false;
  public isFilterSection:boolean = false;
  public isListSection:boolean =false;
  constructor(
    public soModificationFilterForms: SOModificationFilterForms,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService: InterfaceService,
    public authService: AuthService
  ) {
    this.columns = [
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'salesOrder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'soItem',
        header: 'SO Line Item',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'counter',
        header: 'Counter',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'quantity',
        header: 'Quantity',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'unitOfMeasure',
        header: 'Unit Of Measure',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'soOverDeliveryTol',
        header: 'SO Overdelivery Tolarance',
        columnType: ColumnType.string,
        width: '230px',
        sortFieldName: '',
      },
      {
        field: 'soUnderDeliveryTol',
        header: 'SO Underdelivery Tolarance',
        columnType: ColumnType.string,
        width: '230px',
        sortFieldName: '',
      },
      {
        field: 'soStatus',
        header: 'SO Status',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'reasonForRejection',
        header: 'Reason For Rejection',
        columnType: ColumnType.string,
        width: '230px',
        sortFieldName: '',
      },
      {
        field: 'statusDescription',
        header: 'Status Flag',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'errorDescription',
        header: 'Error Description',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'piProcessStatus',
        header: 'PI Process Status',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
    ];
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.getSoModOBInterfaceFilterSODetails();
    this.getSoModOBInterfaceFilterDetailsOnLoad();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.soModificationFilterForms.soFilterCriteria));
    this.getLovs(LovCodes.int_stat, 'statusFlag');
  }
  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.SO_modification.id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, menuConstants.SOModificationSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.SOModificationSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.SOModificationSectionIds.Filter_Criteria.buttons.reset, menuConstants.SOModificationSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.SOModificationSectionIds.SOModification_List.buttons.runInterface, menuConstants.SOModificationSectionIds.SOModification_List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.SOModificationSectionIds.SOModification_List.buttons.download, menuConstants.SOModificationSectionIds.SOModification_List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.SOModificationSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.SOModificationSectionIds.SOModification_List.id, this.screenStructure);
  }
  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }

  public getSoModOBInterfaceFilterDetailsOnLoad() {
    this.interfaceService.getSoModOBInterfaceFilterDetailsOnLoad().subscribe(Response => {
      this.productionList = Response;
    })
  }

  public getSoModOBInterfaceFilterSODetails() {
    this.interfaceService.getSoModOBInterfaceFilterSODetails().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'salesOrder'
      )[0].list = data.map((x: any) => {
        return { displayName: x.salesOrder, modelValue: x.salesOrder, lineItemList: x.lineItemList };
      });
    });
  }

  public dropdownChangeEvent(event) {
    if (event.ref_key == 'salesOrder' && event.value != null) {
      let heatIdListByUnit = this.formObject.formFields
        .filter((ele) => ele.ref_key == 'salesOrder')[0]
        .list.filter((x: any) => x.modelValue == event.value)[0];
      this.formObject.formFields.filter(
        (ele) => ele.ref_key == 'lineItemNo'
      )[0].list = heatIdListByUnit.lineItemList.map((x: any) => {
        return {
          displayName: x,
          modelValue: x,
        };
      });
    }
    else if(event.ref_key == 'salesOrder' && event.value == null){
      this.jswComponent.resetForm(this.formObject);
      this.formObject.formFields.filter(
        (ele) => ele.ref_key == 'lineItemNo'
      )[0].list = [];
    }
  }

  public runSoModOBInterface() {
    this.interfaceService.runSoModOBInterface()
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          {
            message: Response,
          }
        );

      });
  }

  public filterData(): any {
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.requestBody);
      this.requestBody = {
        "fromDate": (formObj.fromDate!="")?formObj.fromDate:null,
        "toDate": (formObj.toDate!="")?formObj.toDate:null,
        "salesOrder": (formObj.salesOrder!="")?formObj.salesOrder:null,
        "lineItemNo": (formObj.lineItemNo!="")?formObj.lineItemNo:null,
        "statusFlag": (formObj.statusFlag!="")?formObj.statusFlag:null
      }
      this.interfaceService.getSoModOBInterfaceFilterDetails(this.requestBody).subscribe(Response => {
        Response.map((rowData: any) => {
          return rowData;
        })
        this.productionList = Response
      })
    }
  }

  public textBoxEventHandling(formObj){
    for (let i = 0; i < formObj.formFields.length; i++) { // unit textbox change event
      if(formObj.formFields[i].isError)
      {
        formObj.formFields[i].value=''
      } 
      formObj.formFields[i].isError=false
    }
  }

  public resetForm() {
    this.jswComponent.resetForm(this.formObject);
    this.formObject.formFields.filter(
      (ele) => ele.ref_key == 'lineItemNo'
    )[0].list = [];
    this.productionList=[];
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele:any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.productionList, keyArr); 
      this.sharedService.downloadCSV(data, 'SO_Modification', tableHeader);
    }
  }
}
