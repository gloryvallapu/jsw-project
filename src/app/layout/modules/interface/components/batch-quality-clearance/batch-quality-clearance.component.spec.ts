import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchQualityClearanceComponent } from './batch-quality-clearance.component';

describe('BatchQualityClearanceComponent', () => {
  let component: BatchQualityClearanceComponent;
  let fixture: ComponentFixture<BatchQualityClearanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BatchQualityClearanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchQualityClearanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
