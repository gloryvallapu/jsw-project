import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { BatchQualityClearance } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { Subject } from 'rxjs';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { smsQaBo } from '../../../user-management/interfaces/user-master-interface';

@Component({
  selector: 'app-batch-quality-clearance',
  templateUrl: './batch-quality-clearance.component.html',
  styleUrls: ['./batch-quality-clearance.component.css']
})
export class BatchQualityClearanceComponent implements OnInit {
  public selectedBatchId:any;
  public tableControlActions:any = []
  public formObject: any;
  public viewType = ComponentType;
  public columnType = ColumnType;
  public columns: ITableHeader[] = []
  public tableType: any;
  public batchDetails:any = []
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public screenStructure: any = [];
  public isReset:boolean= false;
  public isRetrive:boolean= false;
  public isRunInterface:boolean = false;
  public smsQaList: any;
  public inputBatchIds: any;
  public selectedRecord: any;
  public isRepushInterface: boolean = false;
  public openRepushInterfaceModel: boolean = false;
  public smsQAparams: smsQaBo = new smsQaBo();
  public isDownload:boolean =false;
  public isFilterSection:boolean = false;
  public isListSection:boolean =false;
  constructor(
    public batchQualityClearance: BatchQualityClearance,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService:InterfaceService,
    public authService: AuthService
    ) {
    this.columns = [
      // {
      //   field: '',
      //   header: '',
      //   columnType: ColumnType.radio,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'heatNumber',
        header: 'Heat Number',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'inputMaterial',
        header: 'Input Material',
        columnType: ColumnType.string,
        width: '170px',
        sortFieldName: '',
      },
      {
        field: 'inputBatch Id',
        header: 'Input Batch Id',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'parameterType',
        header: 'Parameter Type',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'counter',
        header: 'Counter',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'inputWt',
        header: 'Input Weight(tons)',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },

      {
        field: 'outputMaterial',
        header: 'Output Material',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'outputBatch',
        header: 'Output Batch',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'outputWt',
        header: 'Output Weight(tons)',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'salesOrder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'salesOrderItem',
        header: 'Sales Order Item',
        columnType: ColumnType.string,
        width: '190px',
        sortFieldName: '',
      },
      {
        field: 'workCenter',
        header: 'Work Center',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'productionDate',
        header: 'Production Date',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'shift',
        header: 'Shift',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchDia',
        header: 'Batch Diameter(mm)',
        columnType: ColumnType.string,
        width: '170px',
        sortFieldName: '',
      },
      {

        field: 'batchThickness',
        header: 'Batch Thickness(mm)',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'batchWidth',
        header: 'Batch Width(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchLength',
        header: 'Batch Length(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'eqSpec',
        header: 'Eq Spec',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'eqSubSpec',
        header: 'Eq Sub Spec',
        columnType: ColumnType.string,
        width: '170px',
        sortFieldName: '',
      },
      {
        field: 'eqSubSpecGrp',
        header: 'Eq Sub SpecGrp',
        columnType: ColumnType.string,
        width: '170px',
        sortFieldName: '',
      },


      {
        field: 'ncoDeclared',
        header: 'NCO Declared',
        columnType: ColumnType.string,
        width: '170px',
        sortFieldName: '',
      },
      {
        field: 'ncoReason',
        header: 'NCO Reason',
        columnType: ColumnType.string,
        width: '170px',
        sortFieldName: '',
      },
      {
        field: 'batchLocation',
        header: 'Batch Location',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'nextWorkcenter',
        header: 'Next Work Center',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'jswGrade',
        header: 'Jsw Grade',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'qualityRemark',
        header: 'Quality Remark',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'productionRemark',
        header: 'Production Remark',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'statusFlag',
        header: 'Status Flag',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'errorDescription',
        header: 'Error Description',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'dateCreated',
        header: 'Date Create',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'dateModified',
        header: 'Date Mofied',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
    ]
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.tableControlActions = ["Run Interface", "Download"]
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.batchQualityClearance.batchFilterCriteria));
    this.getLovs(LovCodes.int_stat, 'statusFlag');
    this.getAllQualityAndWToFDetails()
  }
  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.BtachQualityClearance.id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, menuConstants.BtachQualitySectionIds.Filter_Criteria.buttons.retrieve, menuConstants.BtachQualitySectionIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.BtachQualitySectionIds.Filter_Criteria.buttons.reset, menuConstants.BtachQualitySectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.BtachQualitySectionIds.BatchQqality_List.buttons.runInterface, menuConstants.BtachQualitySectionIds.BatchQqality_List.id, this.screenStructure);
    this.isRepushInterface = this.sharedService.isButtonVisible(true, menuConstants.BtachQualitySectionIds.BatchQqality_List.buttons.repushInterface, menuConstants.BtachQualitySectionIds.BatchQqality_List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.BtachQualitySectionIds.BatchQqality_List.buttons.download, menuConstants.BtachQualitySectionIds.BatchQqality_List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.BtachQualitySectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.BtachQualitySectionIds.BatchQqality_List.id, this.screenStructure);
  }
  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }
  public resetForm() {
    this.jswComponent.resetForm(this.formObject);
    this.getAllQualityAndWToFDetails()

    this.clearSelection.next(true);
    this.selectedRecord = [];
    this.smsQaList = [];
  }

  public getSelectedRecord(rowData) {
    this.selectedRecord = rowData;
    this.isRepushInterface = true;
  }

  public getRowDetails(event){
    this.selectedBatchId = event.inputBatchId
  }

public getAllQualityAndWToFDetails(){
  let requestBody = {
    "fromDate": null,
    "toDate": null,
    "statusFlag": null,
    "batchId":null
  }
  this.interfaceService.getQualityAndWToFDetails(requestBody).subscribe(Response =>{
    Response.map((obj:any)=>{
      obj.dateCreated = this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.dateCreated)
      obj.productionDate = this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.productionDate)
    })
    this.batchDetails = Response
  })
}

  public filterData(){
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    let requestBody = {
      "fromDate": null,
      "toDate": null,
      "statusFlag": null,
      "batchId":null
    }
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, requestBody);
    this.interfaceService.getQualityAndWToFDetails(formObj).subscribe(Response =>{
      Response.map((obj:any)=>{
        obj.dateCreated = this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.dateCreated)
        obj.productionDate = this.sharedService.getDate_MM_DD_YYYY_With_Time(obj.productionDate)
      })
      this.batchDetails = Response
    })
  }



  public runInterface(){
    this.interfaceService.pushConditioningDetailsToSap(this.batchDetails).subscribe(Response =>{
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        {
          message: Response,
        }
      );
    })

  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele:any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.batchDetails, keyArr); 
      this.sharedService.downloadCSV(data, 'Batch_Quality_Clearance', tableHeader);
    }
  }


  public closeSmsQaInterfcePopup() {
    this.openRepushInterfaceModel = false;
  }

  public SmsQaInterfcePopup() {
    this.openRepushInterfaceModel = true;
    this.inputBatchIds = this.selectedRecord.map((x: any) => {
      return x.inputBatchId;
    });
  }

//Vaibhav
public BatchQualityClearanceWToFIb()
{
  this.openRepushInterfaceModel = false;
  this.smsQAparams.batchQuaClearWToFIbRePushFilterCriteriaDataBo = this.selectedRecord.map((x: any) => {
    return {
      "inputBatchId": x.inputBatchId,
      "counter": x.counter,
      "inputMaterial":x.inputMaterial
    }
  })

  this.interfaceService.pushBackQualityAndWToFDetailsToSap(this.smsQAparams).subscribe(Response =>{
    this.clearSelection.next(true);
    this.selectedRecord = [];
    this.isRepushInterface = false;
    return this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Success,
      {
        message: `Push back Quality And WToF details to SAP Successfuly.`
      }
    );
  })
}

}
