import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { HeatchemistryForms } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';

import { heatChemistryBo } from '../../../../modules/user-management/interfaces/user-master-interface';


@Component({
  selector: 'app-heat-chemistry-interface',
  templateUrl: './heat-chemistry-interface.component.html',
  styleUrls: ['./heat-chemistry-interface.component.css']
})

export class HeatChemistryInterfaceComponent implements OnInit {
  public heatIds: any;
  public formObject: any;
  public columnType = ColumnType;
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public heatChemistryList: any;
  public screenStructure: any = [];
  public isReset: boolean = false;
  public isRetrive: boolean = false;
  public isRunInterface: boolean = false;
  public isDownload: boolean = false;
  public selectedRecord: any;
  public isRepushInterface: boolean = false;
  public openRepushInterfaceModel: boolean = false;
  public isFilterSection: boolean = true;
  public isListSection: boolean = true;
  public heatChemistryparams: heatChemistryBo = new heatChemistryBo();
  constructor(
    public authService: AuthService,
    public heatchemistryForms: HeatchemistryForms,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService: InterfaceService,
  ) {
    this.columns = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'heat',
        header: 'Heat',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'counter',
        header: 'Counter',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'intnlGrade',
        header: 'International Grade',
        columnType: ColumnType.string,
        width: '190px',
        sortFieldName: '',
      },
      {
        field: 'jswGrade',
        header: 'JSW Grade',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pcarbonPct',
        header: 'P Carbon PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pmanganesePct',
        header: 'P Manganese PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'psulphurPct',
        header: 'P Sulphur PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'pphosphorusPct',
        header: 'P Phosphorus PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'psiliconPct',
        header: 'P Silicon PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'paluminiumPct',
        header: 'P Aluminium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'paluminiumSolPct',
        header: 'P Aluminium Sol PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'saluminiumInsoPct',
        header: 'S Aluminium Inso PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'pnitrogenPct',
        header: 'P Nitrogen PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pnitrogenPpm',
        header: 'P Nitrogen PPM',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pnickelPct',
        header: 'P Nickel PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pcopperPct',
        header: 'P_Copper PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pchromiumPct',
        header: 'P Chromium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pmolybdenumPct',
        header: 'P Molybdenum PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'pvanadiumlPct',
        header: 'P Vanadium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pniobiumPct',
        header: 'P Niobium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'ptinPct',
        header: 'P Tin PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'ptitaniumPct',
        header: 'P Titanium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pzirconiumPct',
        header: 'P Zirconium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pboronPct',
        header: 'P Boron PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pboronPpm',
        header: 'P Boron PPM',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pcalciumPct',
        header: 'P Calcium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pcalciumPpm',
        header: 'P Calcium PPM',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'poxygenPpm',
        header: 'P Oxygen PPM',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'phudrogenPpm',
        header: 'P Hydrogen PPM',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pzincPct',
        header: 'P Zinc PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'parsenicPct',
        header: 'P Arsenic PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'ptungstenPct',
        header: 'P Tungsten PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pleadPct',
        header: 'P Lead PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pantimonyPct',
        header: 'P Antimony PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'scoboltPct',
        header: 'S Cobolt PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'sseleniumPct',
        header: 'S Selenium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pbismuthPct',
        header: 'P Bismuth PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pcadmiumPct',
        header: 'P Cadmium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'ptelluriumPct',
        header: 'P Tellurium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'smagnesiumPct',
        header: 'S Magnesium PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'palNRatio',
        header: 'P ALN Ratio',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pnbVTiPct',
        header: 'P_NB_V_TI_PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pnbVTiBPct',
        header: 'P_NB_V_TI_PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'pcrNiMoPct',
        header: 'P_CR_NI_MO_PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pcuNiMoPct',
        header: 'P_CU_NI_PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pnbVTiMoCrPct',
        header: 'P_NB_V_TI_MO_CR_PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'pnbVTiCuMoPct',
        header: 'P_NB_V_TI_CU_MO_PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'pcuCrNiPct',
        header: 'P_CU_CR_NI_PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pcuCrNiMoPct',
        header: 'P_CU_CR_NI_MO_PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'psi25PPct',
        header: 'P_SI_25_P_PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pcuNiMoPct',
        header: 'P_CU_NI_MO_PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'pcarbonEqShort',
        header: 'P Carbon EQ Short',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'pcarbonEqLong',
        header: 'P_Carbon_EQ_Long',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'ppcm',
        header: 'P_PCM',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'scuNiCrMoVPct',
        header: 'S_CU_NI_CR_MO_V_PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'scrMoPct',
        header: 'S_CR_MO_PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'sniCrPct',
        header: 'S_NI_CR_PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'sniCrMoVPct',
        header: 'S_NI_CR_MO_V_PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },

      {
        field: 's4PSnPct',
        header: 'S_4P_SN_PCT',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'stiAlZrPct',
        header: 'S_TI_AL_ZR_PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'scu10SnPct',
        header: 'S_CU_10_SN_PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'scu8SnPct',
        header: 'S_CU_8_SN_PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'ssnAsSbPct',
        header: 'S_SN_AS_SB_PCT',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'smnSRatio',
        header: 'S_MN_S_RATIO',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'smnSiRatio',
        header: 'S_MN_SI_RATIO',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'stiNRatio',
        header: 'S_TI_N_RATIO',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'sniCuRatio',
        header: 'S_NI_CU_RATIO',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'smanganeseEq',
        header: 'S_MANGANESE_EQ',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'entryDate',
        header: 'Entry Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'entryTime',
        header: 'Entry Time',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'flag',
        header: 'Flag',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'statusDesc',
        header: 'Status Flag',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'errorDescritpion',
        header: 'Error Description',
        columnType: ColumnType.string,
        width: '220px',
        sortFieldName: '',
      },
      {
        field: 'dateCreated',
        header: 'Date Created',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'dateModified',
        header: 'Date Modified',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
    ];
  }

  public screenRightsCheck() {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.HeatChemistry.id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, menuConstants.HeatChemInterfaceSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.HeatChemInterfaceSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.HeatChemInterfaceSectionIds.Filter_Criteria.buttons.reset, menuConstants.HeatChemInterfaceSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.HeatChemInterfaceSectionIds.List.buttons.runInterface, menuConstants.HeatChemInterfaceSectionIds.List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.HeatChemInterfaceSectionIds.List.buttons.download, menuConstants.HeatChemInterfaceSectionIds.List.id, this.screenStructure);
    this.isRepushInterface = this.sharedService.isButtonVisible(true, menuConstants.HeatChemInterfaceSectionIds.List.buttons.repushInterface, menuConstants.HeatChemInterfaceSectionIds.List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.HeatChemInterfaceSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.HeatChemInterfaceSectionIds.List.id, this.screenStructure);
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.heatchemistryForms.heatChemistryFilterCriteria));
    this.getHeatChemistryIbDetails();
    this.getLovs(LovCodes.int_stat, 'statusFlag');
  }

  public getSelectedRecord(rowData) {
    this.selectedRecord = rowData;
    this.isRepushInterface = true;
  }

  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }

  public getHeatChemistryIbDetails(): any {
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();
    let requestBody = {
      "fromDate": null,
      "toDate": null,
      "heatId": null,
      "statusFlag": null
    }
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, requestBody);

      requestBody = {
        "fromDate": (formObj.fromDate != "") ? formObj.fromDate : null,
        "toDate": (formObj.toDate != "") ? formObj.toDate : null,
        "heatId": (formObj.heatId != "") ? formObj.heatId : null,
        "statusFlag": (formObj.statusFlag != "") ? formObj.statusFlag : null
      }
      this.interfaceService.getHeatChemistryIbDetails(requestBody).subscribe(Response => {
        Response.map((rowData: any) => {
          return rowData;
        })
        this.heatChemistryList = Response
      })
    }
  }


  public textBoxEventHandling(formObj) {
    for (let i = 0; i < formObj.formFields.length; i++) { // unit textbox change event
      if (formObj.formFields[i].isError) {
        formObj.formFields[i].value = ''
      }
      formObj.formFields[i].isError = false
    }
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele: any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.heatChemistryList, keyArr);
      this.sharedService.downloadCSV(data, 'Heat_Chemistry', tableHeader);
    }
  }

  public resetFilter() {
    this.jswComponent.resetForm(this.formObject);
    this.clearSelection.next(true);
    this.selectedRecord = [];
    this.heatChemistryList = [];
  }

  public closeHeatChemistryInterfcePopup() {
    this.openRepushInterfaceModel = false;
  }

  public HeatChemistryInterfcePopup() {
    this.openRepushInterfaceModel = true;
    this.heatIds = this.selectedRecord.map((x: any) => {
      return x.heat;
    });
  }

  //Vaibhav
  public HeatChemistry() {
    this.openRepushInterfaceModel = false;
    this.heatChemistryparams.heatChemistryIbRePushFilterCriteriaDataBo = this.selectedRecord.map((x: any) => {
      return {
        "heat": x.heat,
        "counter": x.counter
      }
    })
    this.interfaceService.pushBackChemistryDetailsToSap(this.heatChemistryparams).subscribe(Response => {
      this.clearSelection.next(true);
      this.selectedRecord = [];
      this.isRepushInterface = false;
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        {
          message: `Push back Batch changed chemistry to SAP Successfuly.`
        }
      );
    })
  }


}
