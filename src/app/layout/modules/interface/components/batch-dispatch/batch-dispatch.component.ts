import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { BatchDispatchForms } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-batch-dispatch',
  templateUrl: './batch-dispatch.component.html',
  styleUrls: ['./batch-dispatch.component.css']
})
export class BatchDispatchComponent implements OnInit {
  public formObject: any;
  public columnType = ColumnType;
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  batchList: any;
  public requestBody = {
    "fromDate": null,
    "toDate": null,
    "batchId": null,
    "statusFlag": null
  }
  public screenStructure: any = [];
  public isReset:boolean= false;
  public isRetrive:boolean= false;
  public isRunInterface:boolean = false;
  public isDownload:boolean =false;
  public isFilterSection:boolean = true;
  public isListSection:boolean =true;
  constructor(
    public authService: AuthService,
    public jswService: JswCoreService,
    public batchDispatchForms: BatchDispatchForms,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService: InterfaceService,) {
    this.columns = [
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'salesOrder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'salesOrderItem',
        header: 'Sales Order Item',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'orderType',
        header: 'Order Type',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'purchaseOrder',
        header: 'Purchase Order',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'salesOfficeDescrip',
        header: 'Sales Office Description',
        columnType: ColumnType.string,
        width: '190px',
        sortFieldName: '',
      },
      {
        field: 'salesDistrict',
        header: 'Sales District',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'material',
        header: 'Material',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'materialDescription',
        header: 'Material Description',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'jswGrade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'eqSubGrade',
        header: 'EQ Sub Grade',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchThick',
        header: 'Batch Thick(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchWidth',
        header: 'Batch Width(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchLength',
        header: 'Batch Length(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'qualityLevel',
        header: 'Quality Level',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchDia',
        header: 'Batch Diameter(mm)',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'noOfSheets',
        header: 'No Of Sheets',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'netWeight',
        header: 'Net Weight(tons)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'grossWeight',
        header: 'Gross Weight(tons)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'uom',
        header: 'UOM',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'truckNo',
        header: 'Truck No',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'storageLocation',
        header: 'Storage Location',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'modeOfTransport',
        header: 'Mode Of Transport',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'exciseInvoiceNo',
        header: 'Excise Invoice No',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'exciseInvoiceDate',
        header: 'Excise Invoice Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'statusDescription',
        header: 'Status Flag',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'errorDescription',
        header: 'Error Description',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'salesReturnFlag',
        header: 'Sales Return Flag',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'materialDocument',
        header: 'Material Document',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'cancelFflag',
        header: 'Cancel Flag',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'createdDate',
        header: 'Created Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'modifiedDate',
        header: 'Modified Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },

    ];
  }

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.BatchDispatch.id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, menuConstants.BatchDispatchInterfaceSectionsIds.Filter_Criteria.buttons.retrieve, menuConstants.BatchDispatchInterfaceSectionsIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.BatchDispatchInterfaceSectionsIds.Filter_Criteria.buttons.reset, menuConstants.BatchDispatchInterfaceSectionsIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.BatchDispatchInterfaceSectionsIds.List.buttons.runInterface, menuConstants.BatchDispatchInterfaceSectionsIds.List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.BatchDispatchInterfaceSectionsIds.List.buttons.download, menuConstants.BatchDispatchInterfaceSectionsIds.List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.BatchDispatchInterfaceSectionsIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.BatchDispatchInterfaceSectionsIds.List.id, this.screenStructure);
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.getBatchDispatchOBInterfaceFilterDetailsOnLoad();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.batchDispatchForms.receiveFilterCriteria));
    this.getLovs(LovCodes.int_stat, 'statusFlag');
  }
  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }

  public filterData(): any {
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.requestBody);
      this.requestBody = {
        "fromDate": formObj.fromDate,
        "toDate": formObj.toDate,
        "batchId": formObj.batchId,
        "statusFlag": formObj.statusFlag
      }
      this.interfaceService.getBatchDispatchOBFilterDetails(this.requestBody).subscribe(Response => {
        Response.map((rowData: any) => {
          return rowData;
        })
        this.batchList = Response
      })
    }
  }

 public textBoxEventHandling(formObj){
  for (let i = 0; i < formObj.formFields.length; i++) { // unit textbox change event
    if(formObj.formFields[i].isError)
    {
      formObj.formFields[i].value=''
    }
    formObj.formFields[i].isError=false
  }
}

  public resetForm() {
    this.jswComponent.resetForm(this.formObject);
    this.batchList=[];
  }

  public runBatchDispatchOBInterface() {
    this.interfaceService.runBatchDispatchOBInterface()
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          {
            message: Response,
          }
        );
      });
  }

  public getBatchDispatchOBInterfaceFilterDetailsOnLoad() {
    this.interfaceService.getBatchDispatchOBInterfaceFilterDetailsOnLoad().subscribe((Response) => {
      this.batchList = Response;
    })
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele:any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.batchList, keyArr);  
      this.sharedService.downloadCSV(data, 'Batch_Dispatch', tableHeader);
    }
  }
}
