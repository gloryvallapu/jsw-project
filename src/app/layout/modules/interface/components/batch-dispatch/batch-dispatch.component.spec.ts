import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { BatchDispatchForms} from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { InterfaceService } from '../../services/interface.service';

import { BatchDispatchComponent } from './batch-dispatch.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { SharedModule } from 'src/app/shared/shared.module';
import { InterfaceEndpoints } from '../../form-objects/interface-api-endpoints';

describe('BatchDispatchComponent', () => {
  let component: BatchDispatchComponent;
  let fixture: ComponentFixture<BatchDispatchComponent>;
  let jswService: JswCoreService
  let batchDispatchForms: BatchDispatchForms;
  let jswComponent: JswFormComponent;
  let sharedService: SharedService;
  let commonService: CommonApiService;
  let interfaceService: InterfaceService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BatchDispatchComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        InterfaceService,
       CommonApiService,
        JswFormComponent,
        InterfaceEndpoints,
        API_Constants,
        CommonAPIEndPoints,
        BatchDispatchForms,

     { provide: ToastrService, useValue: ToastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchDispatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
