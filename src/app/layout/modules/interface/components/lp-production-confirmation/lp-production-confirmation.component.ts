import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { LPtoQAForms } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { Subject } from 'rxjs';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-lp-production-confirmation',
  templateUrl: './lp-production-confirmation.component.html',
  styleUrls: ['./lp-production-confirmation.component.css']
})

export class LPProductionConfirmationComponent implements OnInit {
  public openRepushInterfaceModel: boolean = false;
  public formObject: any;
  public viewType = ComponentType;
  public columnType = ColumnType;
  public columns: ITableHeader[] = []
  public tableType: any;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public productionList: any = [];
  public interfaceStatus: any;
  public requestBody = {
    "fromDate": null,
    "toDate": null,
    "batchId": null,
    "material": null,
    "statusFlag": null
  }
  public setBatchId = {
    "setBatchId": null,
  }
  public screenStructure: any = [];
  public isReset: boolean = false;
  public isRetrive: boolean = false;
  public isRunInterface: boolean = false;
  public isRepushInterface: boolean = false;
  public selectedRecord: any;
  public isDownload: boolean = false;
  public isFilterSection: boolean = false;
  public isListSection: boolean = false;
  constructor(
    public authService: AuthService,
    public lpConfirmationForm: LPtoQAForms,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService: InterfaceService,

  ) {
    this.columns = [
      {
        field: 'radio',
        header: '',
        columnType: ColumnType.radio,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'setBatchId',
        header: 'Set Batch ID',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'material',
        header: 'Material',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'parameterType',
        header: 'Parameter Type',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'counter',
        header: 'Counter',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchWeight',
        header: 'Batch Weight(tons)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'heatNo',
        header: 'Heat No.',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'salesOrder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'salesOrderItem',
        header: 'Sales Order Item',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'productionOrder',
        header: 'Production Order',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'workCenter',
        header: 'Work Centre',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'productionDate',
        header: 'Production Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'productionTime',
        header: 'Production Time',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'shift',
        header: 'Shift',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {

        field: 'processTime',
        header: 'Process Time',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },

      {
        field: 'batchDiameter',
        header: 'Batch Diameter(mm)',
        columnType: ColumnType.string,
        width: '240px',
        sortFieldName: '',
      },
      {

        field: 'batchThickness',
        header: 'Batch Thickness(mm)',
        columnType: ColumnType.string,
        width: '240px',
        sortFieldName: '',
      },
      {
        field: 'batchWidth',
        header: 'Batch Width(mm)',
        columnType: ColumnType.string,
        width: '240px',
        sortFieldName: '',
      },
      {
        field: 'batchLength',
        header: 'Batch Length(mm)',
        columnType: ColumnType.string,
        width: '240px',
        sortFieldName: '',
      },
      {
        field: 'legThicknessA',
        header: 'Leg Thickness A(mm)',
        columnType: ColumnType.string,
        width: '240px',
        sortFieldName: '',
      },
      {
        field: 'legThicknessB',
        header: 'Leg Thickness B(mm)',
        columnType: ColumnType.string,
        width: '240px',
        sortFieldName: '',
      },
      {
        field: 'legLengthA',
        header: 'Leg Length A(mm)',
        columnType: ColumnType.string,
        width: '240px',
        sortFieldName: '',
      },
      {
        field: 'legLengthB',
        header: 'Leg Length B(mm)',
        columnType: ColumnType.string,
        width: '240px',
        sortFieldName: '',
      },
      {
        field: 'theoreticalWgtMatl',
        header: 'Theoretical Wgt Matl',
        columnType: ColumnType.string,
        width: '240px',
        sortFieldName: '',
      },
      {
        field: 'soInvoicingWtCat',
        header: 'SO Invoicing Wt Cat',
        columnType: ColumnType.string,
        width: '240px',
        sortFieldName: '',
      },
      {
        field: 'jswGrade',
        header: 'JSW Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'isGrade',
        header: 'IS Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'billetSource',
        header: 'Billtet Source',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'scheduleNum',
        header: 'Schedule Num',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'scheduleLineNum',
        header: 'Schedule Line Num',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'scheduleCode',
        header: 'Schedule Code',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'productionRemark',
        header: 'Production Remark',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'noOfPcs',
        header: 'NO OF PCS',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'statusFlag',
        header: 'Status Flag',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'errorDescription',
        header: 'Error Description',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'dateCreated',
        header: 'Date Created',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'dateModified',
        header: 'Date Modified',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
    ]
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.lpConfirmationForm.LPConfirmationFilterCriteria));
    this.getLovs(LovCodes.int_stat, 'statusFlag');
    this.getLPProducts();
    //  this.getLPProdConfirmationDetails(true);
  }

  public screenRightsCheck() {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.LP_Confirmation.id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, menuConstants.LPConfirmationSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.LPConfirmationSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.LPConfirmationSectionIds.Filter_Criteria.buttons.reset, menuConstants.LPConfirmationSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.LPConfirmationSectionIds.LPConfirmation_List.buttons.runInterface, menuConstants.LPConfirmationSectionIds.LPConfirmation_List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.LPConfirmationSectionIds.LPConfirmation_List.buttons.download, menuConstants.LPConfirmationSectionIds.LPConfirmation_List.id, this.screenStructure);
    this.isRepushInterface = this.sharedService.isButtonVisible(true, menuConstants.LPConfirmationSectionIds.LPConfirmation_List.buttons.repushInterface, menuConstants.LPConfirmationSectionIds.LPConfirmation_List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.LPConfirmationSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.LPConfirmationSectionIds.LPConfirmation_List.id, this.screenStructure);
  }

  public getSelectedRecord(rowData) {
    this.selectedRecord = rowData;
    this.isRepushInterface = true;
  }

  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }

  public getLPProducts() {
    this.interfaceService.getLpProductDetails().subscribe(
      data => {
        this.formObject.formFields.filter(
          (obj: any) => obj.ref_key == 'material'
        )[0].list = data.map((x: any) => {
          return {
            displayName: x.materialDesc,
            modelValue: x.materialId,
          };
        });
      }
    )
  }

  public getLPProdConfirmationDetails(isOnLoad): any {
    if (!isOnLoad && (this.formObject.formFields.filter((ele: any) => { if (!ele.value) return ele; }).length == this.formObject.formFields.length)) {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "Please select at least one of the filter options" });
      return;
    }
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();

    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.requestBody);
      this.requestBody = {
        "fromDate": (formObj.fromDate != "") ? formObj.fromDate : null,
        "toDate": (formObj.toDate != "") ? formObj.toDate : null,
        "batchId": (formObj.batchId != "") ? formObj.batchId : null,
        "material": formObj.material ? formObj.material : null,
        "statusFlag": (formObj.statusFlag != "") ? formObj.statusFlag : null
      }
      this.interfaceService.getLPProdConfirmationDetails(this.requestBody).subscribe(Response => {
        Response.map((rowData: any) => {
          return rowData;
        })
        this.productionList = Response
      })
    }
  }

  public textBoxEventHandling(formObj) {
    for (let i = 0; i < formObj.formFields.length; i++) { // unit textbox change event
      if (formObj.formFields[i].isError) {
        formObj.formFields[i].value = ''
      }
      formObj.formFields[i].isError = false
    }
  }

  public runLpProdConfirmationInterface() {
    this.interfaceService.runLpProdConfirmationInterface()
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          {
            message: Response,
          }
        );

      });
  }
  public resetForm() {
    this.jswComponent.resetForm(this.formObject);
    this.formObject.formFields.filter(
      (ele) => ele.ref_key == 'lineItem'
    )[0].list = [];
    this.clearSelection.next(true);
    this.selectedRecord = [];
    this.isRepushInterface = false;
    this.productionList = [];
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele: any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.productionList, keyArr);
      this.sharedService.downloadCSV(data, 'LP_Prod_Confirmation', tableHeader);
    }
  }

  public closeLPConfirmationInterfcePopup() {
    this.openRepushInterfaceModel = false;
  }

  public LPConfirmationInterfcePopup() {
    this.openRepushInterfaceModel = true;
    this.setBatchId = {
      "setBatchId": this.selectedRecord.setBatchId
    }
  }

  //vaibhav
  public pushBackProductionConfirmationDetails() {
    this.openRepushInterfaceModel = false;
    this.setBatchId = {
      "setBatchId": this.selectedRecord.setBatchId
    }
    this.interfaceService.pushBackProductionConfirmation(this.setBatchId).subscribe(Response => {
      this.clearSelection.next(true);
      this.selectedRecord = [];
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        {
          message: `Push back Production Confirmation details to SAP Successfuly.`
        }
      );
    })
  }





}
