import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LpQaComponent } from './lp-qa.component';

describe('LpQaComponent', () => {
  let component: LpQaComponent;
  let fixture: ComponentFixture<LpQaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LpQaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LpQaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
