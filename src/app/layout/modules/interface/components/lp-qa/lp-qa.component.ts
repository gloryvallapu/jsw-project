import { Component, OnInit } from '@angular/core';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { LPtoQAForms } from '../../form-objects/interface-form-objects';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { Subject } from 'rxjs';
// import { lpQABo } from '../../../user-management/interfaces/user-master-interface';

@Component({
  selector: 'app-lp-qa',
  templateUrl: './lp-qa.component.html',
  styleUrls: ['./lp-qa.component.css']
})
export class LpQaComponent implements OnInit {
  public openRepushInterfaceModel: boolean = false;
  public formObject: any;
  public columnType = ColumnType;
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public productionList: any = [];
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  statusFlag: any;
  public requestBody = {
    "fromDate": null,
    "toDate": null,
    "material": null,
    "batchId": null,
    "statusFlag": null
  }
  // public lpQAParams: lpQABo = new lpQABo();
  public batchId = {
    "batchId": null,
    "counter": null,
  }
  public screenStructure: any = [];
  public isReset: boolean = false;
  public isRetrieve: boolean = false;
  public isRepushInterface: boolean = false;
  public selectedRecord: any;
  public isDownload: boolean = false;
  public isFilterSection: boolean = false;
  public isListSection: boolean = false;

  constructor(public lptoQAForms: LPtoQAForms,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService: InterfaceService,
    public authService: AuthService) { }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.lptoQAForms.LPtoQAFilterCriteria));
    this.getLovs(LovCodes.int_stat, 'statusFlag');
    this.getLPProducts();
    this.getColumns();
    this.filterData(true);
  }

  public screenRightsCheck() {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.LP_QA.id)[0];
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.LP_QA_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRetrieve = this.sharedService.isButtonVisible(true, menuConstants.LP_QA_SectionIds.Filter_Criteria.buttons.retrieve, menuConstants.LP_QA_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.LP_QA_SectionIds.Filter_Criteria.buttons.reset, menuConstants.LP_QA_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.LP_QA_SectionIds.List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.LP_QA_SectionIds.List.buttons.download, menuConstants.LP_QA_SectionIds.List.id, this.screenStructure);
    this.isRepushInterface = this.sharedService.isButtonVisible(true, menuConstants.LP_QA_SectionIds.List.buttons.repushInterface, menuConstants.LP_QA_SectionIds.List.id, this.screenStructure);
  }

  public getSelectedRecord(rowData) {
    this.selectedRecord = rowData;
    this.isRepushInterface = true;
  }

  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }

  public getLPProducts() {
    this.interfaceService.getLpProductDetails().subscribe(
      data => {
        this.formObject.formFields.filter(
          (obj: any) => obj.ref_key == 'material'
        )[0].list = data.map((x: any) => {
          return {
            displayName: x.materialDesc,
            modelValue: x.materialId,
          };
        });
      }
    )
  }

  public getColumns() {
    this.columns = [
      {
        field: 'radio',
        header: '',
        columnType: ColumnType.radio,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'materialDesc',
        header: 'Product Type',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch Id',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'operationNo',
        header: 'Operation No',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'inspCharCode',
        header: 'Insp Char Code',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'counter',
        header: 'Counter',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'actualValue',
        header: 'Actual Value',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'reworkWorkcenter',
        header: 'Rework Work Center',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'reInsp',
        header: 'Re Insp',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'udCode',
        header: 'UD Code',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'errorFlag',
        header: 'Error Flag',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'errorDescription',
        header: 'Error Desc',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'statusDesc',
        header: 'Status Flag',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'createdDate',
        header: 'Created Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'modifiedDate',
        header: 'Modified Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      }
    ]
  }

  public filterData(isOnLoad): any {
    if (!isOnLoad && (this.formObject.formFields.filter((ele: any) => { if (!ele.value) return ele; }).length == this.formObject.formFields.length)) {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "Please select at least one of the filter options" });
      return;
    }
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.requestBody);
      this.requestBody = {
        fromDate: formObj.fromDate ? formObj.fromDate : null,
        toDate: formObj.toDate ? formObj.toDate : null,
        material: formObj.material ? formObj.material : null,
        batchId: formObj.batchId ? formObj.batchId : null,
        statusFlag: formObj.statusFlag ? formObj.statusFlag : null
      }
      this.interfaceService.getLpToQaDetails(this.requestBody).subscribe(Response => {
        this.productionList = Response.map((rowData: any) => {
          rowData.modifiedDate = rowData.modifiedDate ? new Date(rowData.modifiedDate) : null;
          rowData.createdDate = rowData.createdDate ? new Date(rowData.createdDate) : null;
          return rowData;
        })

      })
    }
  }

  public resetFilter() {
    this.jswComponent.resetForm(this.formObject);
    this.clearSelection.next(true);
    this.selectedRecord = [];
    this.isRepushInterface = false;
    this.productionList = [];
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele: any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.productionList, keyArr);
      this.sharedService.downloadCSV(data, 'LP_QA_Details', tableHeader);
    }



  }

  public closeLPQAInterfcePopup() {
    this.openRepushInterfaceModel = false;
  }

  public LPQAInterfcePopup() {
    this.openRepushInterfaceModel = true;
    this.batchId = {
      "batchId": this.selectedRecord.batchId,
      "counter": this.selectedRecord.counter
    }
  }

  // public runInterface() {
  //   let batchId = this.formObject.formFields.filter((obj: any) => obj.ref_key == 'batchId')[0].value;
  //   this.interfaceService.runLPQAInterface(batchId)
  //     .subscribe((Response) => {
  //       this.sharedService.displayToastrMessage(
  //         this.sharedService.toastType.Success,
  //         {
  //           message: Response,
  //         }
  //       );
  //   });
  // }

  //Vaibhav
  public pushBackLpQaDetailsToSap() {
    this.openRepushInterfaceModel = false;
    // this.lpQAParams.storageLocationIbRePushFilterCriteriaDataBo = this.selectedRecord.map((x: any) => {
    //   return {
    //     "batchId": x.batchId,
    //     "counter": x.counter
    //   }
    // })
    this.batchId = {
      "batchId": this.selectedRecord.batchId,
      "counter": this.selectedRecord.counter
    }
    this.interfaceService.pushBackLpQaDetails(this.batchId).subscribe(Response => {
      this.clearSelection.next(true);
      this.selectedRecord = [];
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        {
          message: `Push back LP QA details to SAP Successfuly.`
        }
      );
    })
  }



}
