import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsToLpComponent } from './sms-to-lp.component';

describe('SmsToLpComponent', () => {
  let component: SmsToLpComponent;
  let fixture: ComponentFixture<SmsToLpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmsToLpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsToLpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
