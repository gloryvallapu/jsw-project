import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { BatchQualityClearance } from '../../form-objects/interface-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { Subject } from 'rxjs';
import { InterfaceService } from '../../services/interface.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-batch-update',
  templateUrl: './batch-update.component.html',
  styleUrls: ['./batch-update.component.css']
})
export class BatchUpdateComponent implements OnInit {
  public formObject: any;
  public viewType = ComponentType;
  public columnType = ColumnType;
  public columns: ITableHeader[] = []
  public tableType: any;
  public batchDetails:any = []
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public screenStructure: any = [];
  public isReset:boolean= false;
  public isRetrive:boolean= false;
  public isRunInterface:boolean = false;
  public isDownload:boolean =false;
  public isFilterSection:boolean = true;
  public isListSection:boolean =true;
  constructor(
    public authService: AuthService,
    public batchUpdateFilterForms: BatchQualityClearance,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public interfaceService:InterfaceService
  ) {
    this.columns = [
      {
        field: 'plant',
        header: 'Plant',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'materialDesc',
        header: 'Material',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'attributeName',
        header: 'Attribute Name',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'counter',
        header: 'Counter',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'attributeValue',
        header: 'Attribute Value',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'statusDesc',
        header: 'Status flag',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'errorDescription',
        header: 'Error Description',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'createdDate',
        header: 'Date Created',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'modifiedDate',
        header: 'Date Modified',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },

    ];
  }

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.Interface)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.InterfaceScreenIds.BatchUpdate.id)[0];
    this.isRetrive = this.sharedService.isButtonVisible(true, menuConstants.BatchUpdateInterfaceSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.BatchUpdateInterfaceSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isReset = this.sharedService.isButtonVisible(true, menuConstants.BatchUpdateInterfaceSectionIds.Filter_Criteria.buttons.reset, menuConstants.BatchUpdateInterfaceSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isRunInterface = this.sharedService.isButtonVisible(true, menuConstants.BatchUpdateInterfaceSectionIds.List.buttons.runInterface, menuConstants.BatchUpdateInterfaceSectionIds.List.id, this.screenStructure);
    this.isDownload = this.sharedService.isButtonVisible(true, menuConstants.BatchUpdateInterfaceSectionIds.List.buttons.download, menuConstants.BatchUpdateInterfaceSectionIds.List.id, this.screenStructure);
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.BatchUpdateInterfaceSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isListSection = this.sharedService.isSectionVisible(menuConstants.BatchUpdateInterfaceSectionIds.List.id, this.screenStructure);
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = this.formObject =JSON.parse(JSON.stringify(this.batchUpdateFilterForms.batchFilterCriteria));
    this.getLovs(LovCodes.int_stat, 'statusFlag');
    this.filterData();
  }
  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
    });
  }
 
  public filterData(){
    //debugger
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    let requestBody = {
      "fromDate": null,
      "toDate": null,
      "statusFlag": null,
      "batchId":null
    }
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, requestBody);
    this.interfaceService.getBatchUpdateInterfaceDetails(formObj).subscribe(Response =>{
      Response.map((rowData:any)=>{
        return rowData;
      })
      this.batchDetails = Response
    })
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.columns.map(ele => ele.header)];
      let keyArr = this.columns.map((ele:any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.batchDetails, keyArr); 
      this.sharedService.downloadCSV(data, 'Batch_Update', tableHeader);
    }
  }
  public resetFilter() {
    this.jswComponent.resetForm(this.formObject);
    this.clearSelection.next(true);
    this.batchDetails=[];
  }

  //vaibhav
  public BatchUpdate()
{
  this.interfaceService.pushBackBatchUpdateDetailsToSap().subscribe(Response =>{
    return this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Success,
      {
        message: `Push back Batch Update details to SAP Successfuly.`
      }
    );
  })
}

}
