import {
  Injectable
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  API_Constants
} from 'src/app/shared/constants/api-constants';
import {
  map
} from 'rxjs/operators';
import {
  AuthService
} from 'src/app/core/services/auth.service';
import {
  CommonAPIEndPoints
} from 'src/app/shared/constants/common-api-end-points';
import {
  BehaviorSubject
} from 'rxjs';
import {
  InterfaceEndpoints
} from '../form-objects/interface-api-endpoints';

@Injectable({
  providedIn: 'root'
})
export class InterfaceService {
  public baseUrl: string;
  public masterBaseUrl: string;

  constructor(
    public http: HttpClient,
    private apiURL: API_Constants,
    public commonEndPoints: CommonAPIEndPoints,
    public interfaceEndpoints: InterfaceEndpoints,
    public authService: AuthService
  ) {
    this.baseUrl = `${this.apiURL.baseUrl}${this.apiURL.interfaceGateway}`;
    this.masterBaseUrl = `${this.apiURL.baseUrl}${this.apiURL.masterGateWay}`;
  }
  //Start of so modification api's
  public getSoModOBInterfaceFilterDetailsOnLoad() {
    return this.http
      .get(`${this.baseUrl}${this.interfaceEndpoints.getSoModOBInterfaceFilterDetailsOnLoad}`)
      .pipe(map((response: any) => response));
  }

  public getSoModOBInterfaceFilterDetails(reqBody) {
    return this.http
      .put(`${this.baseUrl}${this.interfaceEndpoints.getSoModOBInterfaceFilterDetails}`, reqBody)
      .pipe(map((response: any) => response));
  }

  public getSoModOBInterfaceFilterSODetails() {
    return this.http.get(`${this.baseUrl}${this.interfaceEndpoints.getSoModOBInterfaceFilterSODetails}`).pipe(map((response: any) => response));
  }

  public runSoModOBInterface() {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.runSoModOBInterface}`, null, {
      responseType: 'text'
    }).pipe(map((response: any) => response));
  }
  //End of so modification api's

  // Start of batch dispatch api's
  public getBatchDispatchOBInterfaceFilterDetailsOnLoad() {
    return this.http
      .get(`${this.baseUrl}${this.interfaceEndpoints.getBatchDispatchOBInterfaceFilterDetailsOnLoad}`)
      .pipe(map((response: any) => response));
  }

  public getBatchDispatchOBFilterDetails(reqBody) {
    return this.http
      .put(`${this.baseUrl}${this.interfaceEndpoints.getBatchDispatchOBFilterDetails}`, reqBody)
      .pipe(map((response: any) => response));
  }

  public runBatchDispatchOBInterface() {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.runBatchDispatchOBInterface}`, null, {
      responseType: 'text'
    }).pipe(map((response: any) => response));
  }
  //End of batch dispatch api's

  //Start of Batch Swapping Interface api's

  public getBatchSwapInterfaceFilterCriteriaDetails() {
    return this.http.get(`${this.baseUrl}${this.interfaceEndpoints.getBatchSwapInterfaceFilterCriteriaDetails}`).pipe(map((response: any) => response));
  }

  public getBatchSwapInterfaceBatchDetailsOnLoad() {
    return this.http
      .get(`${this.baseUrl}${this.interfaceEndpoints.getBatchSwapInterfaceBatchDetailsOnLoad}`)
      .pipe(map((response: any) => response));
  }

  public getBtachSwapInterfaceBatchDetails(reqBody) {
    return this.http
      .put(`${this.baseUrl}${this.interfaceEndpoints.getBatchSwapInterfaceBatchDetails}`, reqBody)
      .pipe(map((response: any) => response));
  }

  public runBatchSwapInterface() {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.runBatchSwapInterface}`, null, {
      responseType: 'text'
    }).pipe(map((response: any) => response));
  }
  //End of Batch Swapping Interface api's

  // Start Location Changed
  public getLocationChangedBatches(requestBody) {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.getStorageLocationData}`, requestBody).pipe(map((response: any) => response));
  }
  // End Location Changed

  //Start of Batch clearance api's

  public getAllQualityAndWToFDetails() {
    return this.http.get(`${this.baseUrl}${this.interfaceEndpoints.getAllQualityAndWToFDetails}`).pipe(map((response: any) => response));
  }

  public getQualityAndWToFDetails(requestBody) {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.getQualityAndWToFDetails}`, requestBody).pipe(map((response: any) => response));
  }

  public pushConditioningDetailsToSap(batchDetails) {
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.pushConditioningDetailsToSap}`, batchDetails,{ headers, responseType: 'text' }).pipe(map((response: any) => response));
  }
  //End of Batch clearance api's

  //Start of Batch conditioning api's

  public getConditioningDetails(requestBody) {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.getConditioningDetails}`, requestBody).pipe(map((response: any) => response));
  }

  //Start of LP Prod Confirmation api's

  public getLPProdConfirmationDetails(reqBody) {
    return this.http
      .put(`${this.baseUrl}${this.interfaceEndpoints.getLPprodConfimationInterfaceDetails}`, reqBody)
      .pipe(map((response: any) => response));
  }

  public runLpProdConfirmationInterface() {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.runLpProdConfirmationInterface}`, null, {
      responseType: 'text'
    }).pipe(map((response: any) => response));
  }
  //End of LP Prod Confirmation api's
  //End of Batch conditiong api's

  //Start of Heat chemistry api's
  public getHeatChemistryIbDetails(requestBody) {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.getHeatChemistryIbDetails}`, requestBody).pipe(map((response: any) => response));

  }
  //End of Heat chemisty api's


  //Start of Procure Batch api's
  public getAllBatchProcureDetails() {
    return this.http.get(`${this.baseUrl}${this.interfaceEndpoints.getAllBatchProcureDetails}`).pipe(map((response: any) => response));
  }

  public getFilteredBatchProcureDetails(filterContent) {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.getFilteredBatchProcureDetails}`,filterContent).pipe(map((response: any) => response));
  }

  public runBatchHeaderChemOBInterface(userId){
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.runBatchHeaderChemOBInterface}/${userId}`, null, {
      responseType: 'text'
    }).pipe(map((response: any) => response));
  }
  //End of Procure Batch api's


  //Start of Batch updatecapi's
  public getBatchUpdateInterfaceDetails(requestBody) {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.getBatchUpdateInterfaceDetails}`, requestBody).pipe(map((response: any) => response));
  }
    //Start of Batch  update api's

    //Start of lp_sms interface
    public getLpToSmsDetails(requestBody){
      return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.getLpToSmsDetails}`, requestBody).pipe(map((response: any) => response));
    }
  //End of lp_sms interface

   //Start of lp_sms interface
   public getSmsToLPDetails(requestBody){
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.getSmsToLpDetails}`, requestBody).pipe(map((response: any) => response));
  }
  public runSmsToLpInterface() {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.runSmsToLpInterface}`, null, {
      responseType: 'text'
    }).pipe(map((response: any) => response));
  }
//End of lp_sms interface

  //Start of LP to QA Interface APIs
  public getLPProducts() {
    return this.http.get(`${this.baseUrl}${this.interfaceEndpoints.getLPProducts}`).pipe(map((response: any) => response));
  }

  public getLpToQaDetails(requestBody){
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.getLPQaDetails}`, requestBody).pipe(map((response: any) => response));
  }

  public getLpProductDetails(){
    return this.http.get(`${this.baseUrl}${this.interfaceEndpoints.getLpProductDetails}`).pipe(map((response: any) => response));
  }

  // public runLPQAInterface(batchId){
  //   return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.runLPQAInterface}` + `?batchId=${batchId}`, null, {
  //     responseType: 'text'
  //   }).pipe(map((response: any) => response));
  // }

  //start of receive sales order
  public getSalesOrderLineItemDropdown() {
    return this.http.get(`${this.baseUrl}${this.interfaceEndpoints.getSalesOrderDropdown}`).pipe(map((response: any) => response));
  }
  public getSalesOrderFilterDetails(reqBody) {
    return this.http
      .put(`${this.baseUrl}${this.interfaceEndpoints.getSalesOrderFilterData}`, reqBody)
      .pipe(map((response: any) => response));
  }

  public getSalesOrderDetailsByOrderNumber(salesOrder: any, soItemNo: any) {
    return this.http.get(`${this.baseUrl}${this.interfaceEndpoints.getSalesOrderDetailsByOrderNumber}/${salesOrder}/${soItemNo}`).pipe(map((response: any) => response));
  }
  public runReceiveSalesOrderInterface() {
    return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.runSalesOrderInterface}`, null, {
      responseType: 'text'
    }).pipe(map((response: any) => response));
  }

//vaibhav
public pushBackLpQaDetails(requestBody) {
  return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.lpQaInterface}`, requestBody, {
    responseType: 'text'
  }).pipe(map((response: any) => response));
}

//vaibhav
public pushBackProductionConfirmation(requestBody) {
  return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.lpConfirmation}`, requestBody, {
    responseType: 'text'
  }).pipe(map((response: any) => response));
}

//vaibhav
public pushBackStorageLocationDataToSap(requestBody) {
  return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.locationChange}`, requestBody, {
    responseType: 'text'
  }).pipe(map((response: any) => response));
}

//vaibhav
public pushBackChemistryDetailsToSap(requestBody) {
  return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.heatChemistry}`, requestBody, {
    responseType: 'text'
  }).pipe(map((response: any) => response));
}

//vaibhav
public pushBackBatchUpdateDetailsToSap() {
  return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.batchUpdate}`, null, {
    responseType: 'text'
  }).pipe(map((response: any) => response));
}

//vaibhav
public pushBackQualityAndWToFDetailsToSap(requestBody) {
  return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.BatchQualityClearanceWToFIbController}`, requestBody, {
    responseType: 'text'
  }).pipe(map((response: any) => response));
}

//vaibhav
public pushBackConditioningDetailsToSap(requestBody) {
  return this.http.put(`${this.baseUrl}${this.interfaceEndpoints.BatchConditioningInterfce}`, requestBody, {
    responseType: 'text'
  }).pipe(map((response: any) => response));
}

}
