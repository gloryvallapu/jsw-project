export class InterfaceEndpoints{
// Start of so modification api endpoints
  public readonly getSoModOBInterfaceFilterDetailsOnLoad = '/soModOBInterface/getSoModOBInterfaceFilterDetailsOnLoad';
  public readonly getSoModOBInterfaceFilterSODetails = '/soModOBInterface/getSoModOBInterfaceFilterSODetails';
  public readonly runSoModOBInterface='/soModOBInterface/runSoModOBInterface';
  public readonly getSoModOBInterfaceFilterDetails='/soModOBInterface/getSoModOBInterfaceFilterDetails';
//End of so modification api endpoints

//Start of batch dispatch api end points
public readonly getBatchDispatchOBInterfaceFilterDetailsOnLoad='/batchDispatchOB/getBatchDispatchOBInterfaceFilterDetailsOnLoad';
public readonly getBatchDispatchOBFilterDetails = '/batchDispatchOB/getBatchDispatchOBFilterDetails';
public readonly runBatchDispatchOBInterface = '/batchDispatchOB/runBatchDispatchOBInterface';
//End of batch dispatch api end points


// Start of Batch Swapping Interface api endpoints
public readonly getBatchSwapInterfaceFilterCriteriaDetails = '/batchSwapOB/getBatchSwappingOBData';
public readonly getBatchSwapInterfaceBatchDetailsOnLoad = '/batchSwapOB/getBatchSwappingOBOnLoadData';
public readonly getBatchSwapInterfaceBatchDetails='/batchSwapOB/getSoBatchSwapOBFilterDetails';
public readonly runBatchSwapInterface='/batchSwapOB/runBatchSwapOBInterface';

//End of Batch Swapping Interface api endpoints


// Start Location Changed
public readonly getStorageLocationData= '/storageLocationIb/getStorageLocationData';
public readonly pushStorageLocationDataToSap= '/storageLocationIb/pushStorageLocationDataToSap';
// End Location Changed

//Start of batch clearance api end points
public readonly getAllQualityAndWToFDetails = '/getAllQualityAndWToFDetails';
public readonly getQualityAndWToFDetails='/getQualityAndWToFDetails';
public readonly pushConditioningDetailsToSap = '/pushConditioningDetailsToSap';
//End of batch clearance api end points

//Start of batch conditioning api end points
public readonly getConditioningDetails='/getConditioningDetails';
//End of batch conditioning api end points

//Start of LP prod Confimation api end points
public readonly getLPprodConfimationInterfaceDetails='/lpProdConfIb/getLpConfirmDetails';
public readonly runLpProdConfirmationInterface='/lpProdConfIb/pushProductionConfirmationDetailsToSap';
//End of LP prod Confimation api end points

//start of heat chemisty api end points
public readonly getHeatChemistryIbDetails='/getHeatChemistryIbDetails';
//End of heat chemistry api end points

//start of Procure Batch  api end points
public readonly getAllBatchProcureDetails='/getAllBatchProcureDetails';
public readonly getFilteredBatchProcureDetails = '/getFilteredBatchProcureDetails'
public readonly runBatchHeaderChemOBInterface = '/runBatchHeaderChemOBInterface'
//End of heat Procure Batch api end points

//Start of batch update api end points
public readonly getBatchUpdateInterfaceDetails='/getBatchUpdateDetails';
//End of batch update api end points

//Start of lp to sms api end points
public readonly getLpToSmsDetails='/getLpToSmsDetails';
//End of lp to sms api end points

//Start of sms to lp api end points
public readonly getSmsToLpDetails='/getSmsToLpDetails';
public readonly runSmsToLpInterface = '/runInterface'
//End of lp to sms api end points

//Start of LP to QA API ENd Points
public readonly getLPProducts = '/getLpProducts';
public readonly getLPQaDetails = '/getLpQaDetails';
public readonly getLpProductDetails = '/lpProdConfIb/getLpProductDetails';

//public readonly runLPQAInterface = '/pushLpQaDetailsToSap';

//start receive sales order
public readonly getSalesOrderDropdown = '/getSalesOrderLineItemNos';
public readonly getSalesOrderFilterData ='/getReceiveSalesOrder';
public readonly getSalesOrderDetailsByOrderNumber = '/getSalesOrderDetails';
public readonly runSalesOrderInterface = '/runSapReceiveSoInterface'


//vaibhav
public readonly BatchConditioningInterfce= '/pushBackConditioningDetailsToSap'
public readonly BatchQualityClearanceWToFIbController= '/pushBackQualityAndWToFDetailsToSap'
public readonly batchUpdate = '/pushBackBatchUpdateDetailsToSap'
public readonly heatChemistry = '/pushBackChemistryDetailsToSap'
public readonly locationChange = '/storageLocationIb/pushBackStorageLocationDataToSap'
public readonly lpConfirmation = '/lpProdConfIb/pushBackProductionConfirmationDetailsToSap'
public readonly lpQaInterface= '/pushBackLpQaDetailsToSap'

}

