import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BatchConditioningComponent } from './components/batch-conditioning/batch-conditioning.component';
import { BatchDispatchComponent } from './components/batch-dispatch/batch-dispatch.component';
import { BatchQualityClearanceComponent } from './components/batch-quality-clearance/batch-quality-clearance.component';
import { HeatChemistryInterfaceComponent } from './components/heat-chemistry-interface/heat-chemistry-interface.component';
import { LocationChangeComponent } from './components/location-change/location-change.component';
import { LPProductionConfirmationComponent } from './components/lp-production-confirmation/lp-production-confirmation.component';
import { ReceiveSalesOrderComponent } from './components/receive-sales-order/receive-sales-order.component';
import { SoModificationComponent } from './components/so-modification/so-modification.component';
import { InterfaceComponent } from './interface.component';
import { BatchUpdateComponent } from './components/batch-update/batch-update.component';
import { BatchSwappingComponent } from './components/batch-swapping/batch-swapping.component';
import { ProcureBatchComponent } from './components/procure-batch/procure-batch.component';
import { LpSmsComponent } from './components/lp-sms/lp-sms.component';
import { SmsProdComponent } from './components/sms-prod/sms-prod.component';
import { AuthGuardService } from 'src/app/shared/services/auth-guard.service';
import { InterfaceScreenIds, moduleIds } from 'src/assets/constants/MENU/menu-list';
import { BatchQualityClearance } from './form-objects/interface-form-objects';
import { LpQaComponent } from './components/lp-qa/lp-qa.component';
import { SmsToLpComponent } from './components/sms-to-lp/sms-to-lp.component';

const routes: Routes = [
  {
    path: '',
    component: InterfaceComponent,
    children: [
      {
        path: 'lpProdConfirmation',
        component: LPProductionConfirmationComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.LP_Confirmation.id,
        },
      },

      {
        path: 'receiveSalesOrder',
        component: ReceiveSalesOrderComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.ReceiveSalesOrder.id,
        },
      },
      {
        path: 'soModification',
        component: SoModificationComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.SO_modification.id,
        },
      },
      {
        path: 'batchQualityClearance',
        component: BatchQualityClearanceComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.BtachQualityClearance.id,
        },
      },
      {
        path: 'batchConditioning',
        component: BatchConditioningComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.BatchConditioning.id,
        },
      },
      {
        path: 'locationChange',
        component: LocationChangeComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.LocationChange.id,
        },
      },
      {
        path: 'heatChemistry',
        component: HeatChemistryInterfaceComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.HeatChemistry.id,
        },
      },
      {
        path: 'batchUpdate',
        component: BatchUpdateComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.BatchUpdate.id,
        },
      },
      {
        path: 'batchSwapping',
        component: BatchSwappingComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.FGBatchSwap.id,
        },
      },
      {
        path: 'BatchDispatch',
        component: BatchDispatchComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.BatchDispatch.id,
        },
      },
      {
        path: 'procurBatch',
        component: ProcureBatchComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.ProcureBatch.id,
        },
      },
      {
        path: 'LpSms',
        component: LpSmsComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.LP_SMS.id,
        },
      },
      //new component
      {
        path: 'SmsLp',
        component: SmsToLpComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.SMS_LP.id,
        },
      },
      {
        path: 'SmsProd',
        component: SmsProdComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.SMS_Prod.id,
        },
      },
      {
        path: 'lpQA',
        component: LpQaComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.Interface,
          screenId: InterfaceScreenIds.LP_QA.id,
        },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterfaceRoutingModule { }
