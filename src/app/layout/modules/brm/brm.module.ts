import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrmRoutingModule } from './brm.routing.module';
import { ChargingScreenComponent } from './components/charging-screen/charging-screen.component';
import { DischargingScreenComponent } from './components/discharging-screen/discharging-screen.component';
import { BundleProductionComponent } from './components/bundle-production/bundle-production.component';
import { RollingScreenComponent } from './components/rolling-screen/rolling-screen.component';
import { ProductionConfirmationComponent } from './components/production-confirmation/production-confirmation.component';
import { BatchTransferComponent } from './components/batch-transfer/batch-transfer.component';
import { AdditionalBundleProductionComponent } from './components/additional-bundle-production/additional-bundle-production.component';
import { ScheduleConsumptionComponent } from './components/schedule-consumption/schedule-consumption.component';
import { BrmComponent } from './brm.component';
import { JswCoreModule } from 'jsw-core';
import { RadioButtonModule } from 'primeng/radiobutton';
import {CalendarModule } from 'primeng/calendar';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrmEndpoints } from './form-objects/brm-api-endpoints';
import { FilterCriteriaComponent } from './components/common-components/filter-criteria/filter-criteria.component';
import { AdditionalBundleProdForms, ChargingScreenForms, ScheduleConsumptionForms,BatchTransferForms} from './form-objects/brm-form-objects';
import { ConfirmationService } from 'primeng/api';
import { BrmService } from './services/brm.service';
import { TooltipModule } from 'primeng/tooltip';
import { SmallBatchProductionComponent } from './components/small-batch-production/small-batch-production.component';
import { NgxPrintModule } from 'ngx-print';

@NgModule({
  declarations: [
    BrmComponent,
    ChargingScreenComponent,
    DischargingScreenComponent,
    RollingScreenComponent,
    BundleProductionComponent,
    ProductionConfirmationComponent,
    BatchTransferComponent,
    AdditionalBundleProductionComponent,
    ScheduleConsumptionComponent,
    FilterCriteriaComponent,
    SmallBatchProductionComponent
  ],
  imports: [
    CommonModule,
    BrmRoutingModule,
   // JswCoreModule,
    RadioButtonModule,
    CalendarModule,
    SharedModule,
    TooltipModule,
    NgxPrintModule
  ],
  providers: [
    BrmEndpoints,
    ChargingScreenForms,
    ConfirmationService,
    BrmService,
    AdditionalBundleProdForms,
    ScheduleConsumptionForms,
    BatchTransferForms
  ]
})
export class BrmModule { }
