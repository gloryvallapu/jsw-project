import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrmComponent } from './brm.component';
import { AdditionalBundleProductionComponent } from './components/additional-bundle-production/additional-bundle-production.component';
import { BatchTransferComponent } from './components/batch-transfer/batch-transfer.component';
import { BundleProductionComponent } from './components/bundle-production/bundle-production.component';
import { ChargingScreenComponent } from './components/charging-screen/charging-screen.component';
import { DischargingScreenComponent } from './components/discharging-screen/discharging-screen.component';
import { ProductionConfirmationComponent } from './components/production-confirmation/production-confirmation.component';
import { RollingScreenComponent } from './components/rolling-screen/rolling-screen.component';
import { ScheduleConsumptionComponent } from './components/schedule-consumption/schedule-consumption.component';
import { moduleIds, masterMgmtScreenIds, ChargingScreenSectionIds, BRMScreenIds } from 'src/assets/constants/MENU/menu-list';
import { AuthGuardService } from 'src/app/shared/services/auth-guard.service';
import { SmallBatchProductionComponent } from './components/small-batch-production/small-batch-production.component';

const routes: Routes = [
  {
    path: '',
    component: BrmComponent,
    children: [
      {
        path: 'chargingScreen',
        component: ChargingScreenComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM,
          screenId: BRMScreenIds.Charging_Screen.id,
        },
      },
      {
        path: 'dischargingScreen',
        component: DischargingScreenComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM,
          screenId: BRMScreenIds.DisCharging_Screen.id,
        },
      },
      {
        path: 'rollingScreen',
        component: RollingScreenComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM,
          screenId: BRMScreenIds.Rolling_Screen.id,
        },
      },
      {
        path: 'bundleProduction',
        component: BundleProductionComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM,
          screenId: BRMScreenIds.Bundle_Production.id,
        },
      },
      {
        path: 'productionConfirmation',
        component: ProductionConfirmationComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM,
          screenId: BRMScreenIds.Production_confirmation.id,
        },
      },
      {
        path: 'batchTransfer',
        component: BatchTransferComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM,
          screenId: BRMScreenIds.Batch_transfer.id,
        },
      },
      {
        path: 'addiBundleProduction',
        component: AdditionalBundleProductionComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM,
          screenId: BRMScreenIds.Additional_Production.id,
        },
      },
      {
        path: 'scheduleConsumption',
        component: ScheduleConsumptionComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM,
          screenId: BRMScreenIds.Schedule_Consumption.id,
        },
        
      },
      {
        path: 'smallBatchProduction',
        component: SmallBatchProductionComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM,
          screenId: BRMScreenIds.Small_Batch_Production_Screen.id,
        },
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})
export class BrmRoutingModule { }
