import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrmComponent } from './brm.component';

describe('BrmComponent', () => {
  let component: BrmComponent;
  let fixture: ComponentFixture<BrmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
