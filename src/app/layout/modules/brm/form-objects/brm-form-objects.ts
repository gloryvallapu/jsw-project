import { FieldType } from 'src/assets/enums/common-enum';

export class ChargingScreenForms {
  public chargingFilterForm: Object = {
    formName: 'chargingFilterForm',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdID',
        label: 'Schedule ID',
        placeHolder: '',
        value: null,
        name: 'schdID',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      }
    ]
  };

  public bundleProdForm: Object = {
    formName: 'generateBundleForm',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'length',
        label: 'Length',
        placeHolder: '',
        value: null,
        name: 'length',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.number, //'text',
        colSize: 'col-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'qtyPcs',
        label: 'Qty PCS',
        placeHolder: '',
        value: null,
        name: 'qtyPcs',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.number, //'text',
        colSize: 'col-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'bundleWt',
        label: 'Bundle Wt(tons)',
        placeHolder: '',
        value: null,
        name: 'bundleWt',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.number, //'text',
        colSize: 'col-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'strap',
        label: 'Straps',
        placeHolder: '',
        value: null,
        name: 'strap',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: '',
        colSize: 'col-2',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'noOfStraps',
        label: 'No of Straps',
        placeHolder: '',
        value: null,
        name: 'noOfStraps',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: '',
        colSize: 'col-2',
        isVisible: true
      }
    ]
  };
}


export class AdditionalBundleProdForms {

  public addBundFilterCriteria: object = {
    formName: 'Additional Bundle Production',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdId',
        label: 'Schedule ID',
        placeHolder: 'Schedule ID',
        value: null,
        name: 'schdId',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
    ]
  }
}
export class ScheduleConsumptionForms {

  public addScheduleFilter: object = {
    formName: 'Schedule Filter',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdId',
        label: 'Schedule ID',
        placeHolder: 'Schedule ID',
        value: null,
        name: 'schdId',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
    ]
  }
}


export class BatchTransferForms {
  public batchTransferFilterForm: Object = {
    formName: 'FilterForm',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Yard',
        placeHolder: '',
        value: null,
        name: 'Yard',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdID',
        label: 'Schedule No',
        placeHolder: '',
        value: null,
        name: 'schdID',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      }
    ]
  };

  public batchTransfer: object = {
    formName: 'Batch Transfer Production',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Yard',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'heatNo',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: null,
        colSize: 'col-6',
        isVisible: true,
      },
    ]
  };
  public batchTransferTo: object = {
    formName: 'Batch Transfer To',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'yardName',
        label: 'Transfer To',
        placeHolder: 'Transfer To',
        value: null,
        name: 'yardName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-12',
        isVisible: true
      }
    ]
  };
  
}

export class SmallBatchProductionForm {
  public smallBatch: object = {
    formName: 'Small Batch Production',
    formFields: [

      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: 'Unit',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true,
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: null,
        colSize: 'col-6',
        isVisible: true,
      },
    ],
  };
  public qaHold: object = {
    formName: 'QA hold',
    formFields: [

      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'heatNo',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: null,
        colSize: 'col-6',
        isVisible: true,
      },
    ],
  };
}

