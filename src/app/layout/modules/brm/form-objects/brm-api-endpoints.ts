export class BrmEndpoints{
  // Common API End Points
  public readonly getShiftDetails = '/shift/shiftWiseDetails';
  // End of common API End Points

  // Charging screen End Points
  public readonly getUnitsForCharging = '/charging/unitAndSchedules';
  public readonly getLPSchDetails = '/charging/scheduleDetails';
  public readonly getChargingDetails = '/charging/chargingDetails';
  public readonly getChargingSeq = '/charging/chargingSequence';
  public readonly getDummyBatches = '/charging/dummyBillets';
  public readonly startCharging = '/charging/startCharging';
  public readonly addDummyBatch = '/charging/addDummyBillet';
  public readonly deleteBatch = '/charging/deleteAttachedBatch';
  // End Of charging Screen End Points

  // Discharging screen End Points
  public readonly getUnitsForDischarging = '/discharging/dischargeFiltering';
  public readonly getDischargingDetails = '/discharging/dischargingDetails';
  public readonly getBatchStatus = '/discharging/getDisChargingBatchStatusFilter';
  public readonly dischargeBatch = '/discharging/updateToDischarge';
  // End Of Discharging Screen End Points

  // Rolling screen End Points
  public readonly getAllRollingDetails ='/rolling/RollingDetails';
  public readonly saveRollingDetails='/rolling/SaveRollingDetails';
  public readonly getUnitsForRolling ='/rolling/unitAndSchedules';
  public readonly closeSchId = '/rolling/updateScheduleCompleteStatus'
  // End Of Rolling Screen End Points

  // Bundle Production screen End Points
  public readonly getBundleProdValidations = '/bundle/getEnabledValidation';
  public readonly getUnitsForBundleProd ='/bundle/bundleFiltering';
  public readonly getBundleDetails = '/bundle/bundleDetails';
  public readonly generateBundle = '/bundle/generateBundleFromBatches';
  public readonly getMinMaxLen = '/bundle/schdOrderCutLength';
  public readonly generateAddBundle = '/bundle/generateAdditionBundle';
  public readonly consumeBatches = '/bundle/consumeBatches';
  public readonly getStackerListByWc = '/bundle/getStackerList';
  // End Of Bundle Production Screen End Points

  // Production Confirmation screen End Points
  public readonly getProdConfirmationDetails='/prodConfirmation/getProdConfirmationDetails';
  public readonly updateProdConfirmationDetails='/prodConfirmation/updateProdConfirmationDetails';
  // End Of Production Confirmation Screen End Points

   // Batch transfer screen End Points
   public readonly getBatchTransferList='/batchTransfer/getFilteredBatches';
   public readonly getYards = '/batchTransfer/getYards';
   public readonly batchTransfer='/batchTransfer';
  // End Of Batch Transfer Screen End Points

   // Additional Bundle Production screen End Points
   public readonly getAddBundleFilterDetails='/additionalBundle/getAddBundleFilterDetails';
   public readonly getAddBundleDetails='/additionalBundle/getAddBundleDetails';
   public readonly generateAdditionalBundle='/additionalBundle/generateAdditionalBundle';
   public readonly updateGeneratedAdditionalBundle='/additionalBundle/updateGeneratedAdditionalBundle';
  // End Of Additional Bundle Production Screen End Points

   // Schedule Consumption screen End Points
   public readonly getSchdConsumptionFilterDetails ='/schdConsumption/getSchdConsumptionFilterDetails';
   public readonly getSchdConsumptionDetails='/schdConsumption/getSchdConsumptionDetails';
   public readonly updateSchdConsumptionDetails='/schdConsumption/updateSchdConsumptionDetails';
  // End Of Schedule Consumption Screen End Points

  // small batch Filter screen Apis
  public readonly smallBatchProdFilter = '/smallBatchProd/smallBatchProdFilter';
  public readonly getBatchProductionDetails = '/smallBatchProd/getBatchProductionDetails';
  //public readonly updateCreatedSmallBatches = '/smallBatchProd/updateCreatedSmallBatches';
  public readonly updateCreatedSmallBatches = '/smallBatchProd/updatesCreatedSmallBatches';
  public readonly getValidationFlag='/smallBatchProd/getValidationFlag';
  //  End of small batch screen Apis

  //start of Sticker print
  public readonly getStickerPrintDetails = '/stickerPrint/getStickerPrintingDetails'
}
