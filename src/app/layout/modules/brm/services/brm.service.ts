import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { BrmEndpoints } from '../form-objects/brm-api-endpoints';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { BehaviorSubject } from 'rxjs';
import { SharedService } from 'src/app/shared/services/shared.service';

@Injectable({
  providedIn: 'root',
})
export class BrmService {
  public baseUrl: string;
  public brmqaBaseUrl: string;
  public masterBaseUrl: string;
  public lpSchList: BehaviorSubject<any> = new BehaviorSubject([]);
  public getlpSch = this.lpSchList.asObservable();
  public setLPSch(data: any) {
    this.lpSchList.next(data);
  }
  public smsUrl: string;
  constructor(
    public http: HttpClient,
    private apiURL: API_Constants,
    public commonEndPoints: CommonAPIEndPoints,
    public brmEndPoints: BrmEndpoints,
    public authService: AuthService,
    public sharedService: SharedService
  ) {
    this.baseUrl = `${this.apiURL.baseUrl}${this.apiURL.brmGateway}`;
    this.smsUrl = `${this.apiURL.baseUrl}${this.apiURL.smsGateway}`;
    this.masterBaseUrl = `${this.apiURL.baseUrl}${this.apiURL.masterGateWay}`;

    //sticker
    this.brmqaBaseUrl = `${this.apiURL.baseUrl}${this.apiURL.brmQaGateway}`;
  }

  //Common API Calls
  public getShiftDetails() {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getShiftDetails}`)
      .pipe(map((response: any) => response));
  }

  //Charging Screen API calls
  public getUnitsForCharging() {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getUnitsForCharging}/${this.sharedService.loggedInUserDetails.userId}`)
      .pipe(map((response: any) => response));
  }

  public getLPSchList(schdId) {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getLPSchDetails}/${schdId}`)
      .pipe(map((response: any) => response));
  }

  public getChargingDetails(schdId, orderSeqNo, heatNo) {
    //293, 1
    return this.http
      .get(
        `${this.baseUrl}${this.brmEndPoints.getChargingDetails}/${schdId}/${orderSeqNo}/${heatNo}`
      )
      .pipe(map((response: any) => response));
  }

  public getChargingSequence() {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getChargingSeq}`)
      .pipe(map((response: any) => response));
  }

  public getDummyBatches() {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getDummyBatches}`)
      .pipe(map((response: any) => response));
  }

  public startCharging(userId, reqBody) {
    return this.http
      .post(
        `${this.baseUrl}${this.brmEndPoints.startCharging}/${userId}`,
        reqBody,
        { responseType: 'text' }
      )
      .pipe(map((response: any) => response));
  }

  public addDummyBatch(userId, reqBody) {
    return this.http
      .post(
        `${this.baseUrl}${this.brmEndPoints.addDummyBatch}/${userId}`,
        reqBody,
        { responseType: 'text' }
      )
      .pipe(map((response: any) => response));
  }

  public deleteBatch(userId, reqBody) {
    return this.http
      .put(
        `${this.baseUrl}${this.brmEndPoints.deleteBatch}/${userId}`,
        reqBody,
        { responseType: 'text' }
      )
      .pipe(map((response: any) => response));
  }
  //End of Charging Screen API calls

  //Start of Discharge Screen API Calls
  public getUnitsForDischarging() {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getUnitsForDischarging}/${this.sharedService.loggedInUserDetails.userId}`)
      .pipe(map((response: any) => response));
  }

  public getDischargingDetails(schdId) {
    return this.http
      .get(
        `${this.baseUrl}${this.brmEndPoints.getDischargingDetails}/${schdId}`
      )
      .pipe(map((response: any) => response));
  }

  public getBatchStatus() {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getBatchStatus}`)
      .pipe(map((response: any) => response));
  }

  public dischargeBatch(userId, reqBody) {
    return this.http
      .put(
        `${this.baseUrl}${this.brmEndPoints.dischargeBatch}/${userId}`,
        reqBody,
        { responseType: 'text' }
      )
      .pipe(map((response: any) => response));
  }
  //End of Discharge Screen API calls

  //Start Of Rolling Screen API Calls
  public getUnitsForRolling() {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getUnitsForRolling}/${this.sharedService.loggedInUserDetails.userId}`)
      .pipe(map((response: any) => response));
  }

  public getAllRollingDetails(wcName, schdId) {
    return this.http
      .get(
        `${this.baseUrl}${this.brmEndPoints.getAllRollingDetails}/${wcName}/${schdId}`
      )
      .pipe(map((response: any) => response));
  }

  public saveRollingDetails(userId, reqBody) {
    return this.http
      .post(
        `${this.baseUrl}${this.brmEndPoints.saveRollingDetails}/${userId}`,
        reqBody,
        { responseType: 'text' }
      )
      .pipe(map((response: any) => response));
  }

  public closeSchdDetails(schdId,userId) {
    return this.http
      .put(
        `${this.baseUrl}${this.brmEndPoints.closeSchId}/${schdId}/${userId}`, null,{
          responseType: 'text'
        } )
      .pipe(map((response: any) => response));
  }
  //End Of Rolling Screen API Calls

  //Start of Bundle Production Screen APIs
  public getBundleProdValidations() {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getBundleProdValidations}`)
      .pipe(map((response: any) => response));
  }

  public getUnitsForBundleProd() {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getUnitsForBundleProd}/${this.sharedService.loggedInUserDetails.userId}`)
      .pipe(map((response: any) => response));
  }

  public getBundleDetails(schdId) {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getBundleDetails}/${schdId}`)
      .pipe(map((response: any) => response));
  }

  //get all bundle details
  public getAllBundleDetails(schdId, showAll) {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getBundleDetails}/${schdId}/${showAll}`)
      .pipe(map((response: any) => response));
  }

  public generateBundle(reqBody) {
    return this.http
      .post(`${this.baseUrl}${this.brmEndPoints.generateBundle}`, reqBody)
      .pipe(map((response: any) => response));
  }

  public getMinMaxLen(schdId) {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getMinMaxLen}/${schdId}`)
      .pipe(map((response: any) => response));
  }

  public generateAddBundle(reqBody, userId) {
    return this.http
      .post(`${this.baseUrl}${this.brmEndPoints.generateAddBundle}/${userId}`, reqBody, {
        responseType: 'text',
      })
      .pipe(map((response: any) => response));
  }

  public cousumeBatches(reqBody, userId) {
    return this.http
      .post(`${this.baseUrl}${this.brmEndPoints.consumeBatches}/${userId}`, reqBody, {
        responseType: 'text',
      })
      .pipe(map((response: any) => response));
  }

  public getStackerListByWc(wcName){
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getStackerListByWc}/${wcName}`)
      .pipe(map((response: any) => response));
  }

  //stricker in bundle gen
  public getStickerDetails(bundleId){
    return this.http.get(`${this.brmqaBaseUrl}${this.brmEndPoints.getStickerPrintDetails}/${bundleId}`).pipe(map((response: any) => response));
  }

  //End of Bundle Production

  //Start Batch Transfer
  public getBatchTransferList(wcName, heatNo) {
    return this.http
      .get(
        `${this.baseUrl}${this.brmEndPoints.getBatchTransferList}/${wcName}/${heatNo}`
      )
      .pipe(map((response: any) => response));
  }

  public getYardList(wcName) {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getYards}/${wcName}`)
      .pipe(map((response: any) => response));
  }

  public batchTransfer(inTransitId, userId, reqBody) {
    return this.http
      .post(
        `${this.baseUrl}${this.brmEndPoints.batchTransfer}/${inTransitId}/${userId}`,
        reqBody,
        { responseType: 'text' }
      )
      .pipe(map((response: any) => response));
  }
  //End Batch Transfer

  //Start production confirmation details
  public getAllProdConfDetails() {
    return this.http
      .get(`${this.baseUrl}${this.brmEndPoints.getProdConfirmationDetails}`)
      .pipe(map((response: any) => response));
  }

  public updateProdConfirmationDetails(userId, reqBody) {
    return this.http
      .put(
        `${this.baseUrl}${this.brmEndPoints.updateProdConfirmationDetails}/${userId}`,
        reqBody
      )
      .pipe(map((response: any) => response));
  }

  //End production confirmation details

  // Start Additional Bundle Generation

  public getAddBundleFilterDetails() {
    return this.http
      .get(
        `${this.baseUrl}${this.brmEndPoints.getAddBundleFilterDetails}/${this.sharedService.loggedInUserDetails.userId}`
      )
      .pipe(map((response: any) => response));
  }

  public getAddBundleDetails(wcName, schdId) {
    return this.http
      .get(
        `${this.baseUrl}${this.brmEndPoints.getAddBundleDetails}/${wcName}/${schdId}`
      )
      .pipe(map((response: any) => response));
  }

  public generateAdditionalBundle(reqBody) {
    return this.http.post(`${this.baseUrl}${this.brmEndPoints.generateAdditionalBundle}`,reqBody)
      .pipe(map((response: any) => response));
  }

  public saveAdditionalBundle(userId, reqBody) {
    return this.http.put(`${this.baseUrl}${this.brmEndPoints.updateGeneratedAdditionalBundle}/${userId}`, reqBody).pipe(map((response: any) => response));
  }
  // End Additional Bundle Generation

  // Start Schedule consumption Api's

  public getSchdConsumptionFilterDetails() {
    return this.http
      .get(
        `${this.baseUrl}${this.brmEndPoints.getSchdConsumptionFilterDetails}/${this.sharedService.loggedInUserDetails.userId}`
      )
      .pipe(map((response: any) => response));
  }

  public getSchdConsumptionDetails(wcName, schdId) {
    return this.http
      .get(
        `${this.baseUrl}${this.brmEndPoints.getSchdConsumptionDetails}/${wcName}/${schdId}`
      )
      .pipe(map((response: any) => response));
  }

  public updateSchdConsumptionDetails(statusType, userId, reqBody) {
    return this.http
      .put(
        `${this.baseUrl}${this.brmEndPoints.updateSchdConsumptionDetails}/${statusType}/${userId}`,
        reqBody,
        { responseType: 'text' }
      )
      .pipe(map((response: any) => response));
  }
  // End Schedule consumption Api's
  // Start SMS Small Batch Production Filter Apis


public smallBatchProdFilter(){
  return this.http.get(`${this.smsUrl}${this.brmEndPoints.smallBatchProdFilter}/${this.sharedService.loggedInUserDetails.userId}`).pipe(map((response: any) => response));
}

public getBatchProductionDetails(wcName,headId){
  return this.http.get(`${this.smsUrl}${this.brmEndPoints.getBatchProductionDetails}/${wcName}/${headId}`).pipe(map((response: any) => response));
}

// public updateCreatedSmallBatches(userId,reqBody){
//   return this.http.put(`${this.smsUrl}${this.brmEndPoints.updateCreatedSmallBatches}/${userId}`,reqBody).pipe(map((response: any) => response));
// }
public updateCreatedSmallBatches(userId,reqBody){
  // const headers = new HttpHeaders({
  //   'content-type': 'application/json',
  //   'Access-Control-Allow-Origin': '*'
  // });
  return this.http.put(`${this.smsUrl}${this.brmEndPoints.updateCreatedSmallBatches}/${userId}`,reqBody).pipe(map((response: any) => response));
}

public getValidationFlag(){
  return this.http.get(`${this.smsUrl}${this.brmEndPoints.getValidationFlag}`).pipe(map((response: any) => response));
}
// End SMS Small Batch Production
}
