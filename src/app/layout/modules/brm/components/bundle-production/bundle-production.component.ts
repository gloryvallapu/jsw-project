import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { BrmService } from '../../services/brm.service';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ChargingScreenForms } from '../../form-objects/brm-form-objects';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { NgForm } from '@angular/forms';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-bundle-production',
  templateUrl: './bundle-production.component.html',
  styleUrls: ['./bundle-production.component.css']
})
export class BundleProductionComponent implements OnInit {
  public refreshShiftDetails: Subject<boolean> = new Subject<boolean>();
  public columnType: any;
  public tableType: any;
  public globalFilterArray: any = [];
  public viewType = ComponentType;
  public selectedRecord: any;
  public batchColumns: ITableHeader[] = [];
  public batchList: any = [];
  public scheduleDetails: any;
  public formObject: any;
  public bundleData: any = {
    bundleLength: null,
    qtyPcs: null,
    bundleWt: 0,
    selectedStrap: null,
    selectedStrapNo: null,
    wcName: null,
    stackerId: null
  }
  public strapList: any = [];
  public strapNoList: any = [];
  public stackerList: any = [];
  public minLength: any;
  public maxLength: any;
  public orderId: any;
  public display: boolean = false;
  public popupMsg: any;
  public selectedAction: string;
  public generateBundleResponse: any;
  public remainingWeight: number = 0;
  public onePcWt: number = 0;
  @ViewChild('bundleForm') public bundleForm: NgForm;
  public tableRef: any;
  public screenStructure: any = [];
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isGnrtBtn: boolean = false;
  public isfilterCriteriaScreen:boolean=false;
  public isPlan_shiftScreen:boolean=false;
  public isbundledetailsScreen:boolean=false;
  public menuConstants = menuConstants;
  public validationDetails: any;
  //sticcker print
  public barCode: any;
  public bundleId: any;
  public stickerParms: any;
  public qrCode: any;
  public printDisplay: boolean = false;
  public isPrint: boolean = false;
  constructor(public sharedService: SharedService,
    public brmService: BrmService, public chargingScreenForms : ChargingScreenForms, public commonService: CommonApiService,
    public authService: AuthService,private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.BRMScreenIds.Bundle_Production.id)[0];
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.BundleProdSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.BundleProdSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.BundleProdSectionIds.Filter_Criteria.buttons.reset, menuConstants.BundleProdSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isGnrtBtn = this.sharedService.isButtonVisible(true, menuConstants.BundleProdSectionIds.Bundle_List.buttons.generate, menuConstants.BundleProdSectionIds.Bundle_List.id, this.screenStructure);
    this.isfilterCriteriaScreen=this.sharedService.isSectionVisible(menuConstants.BundleProdSectionIds.Filter_Criteria.id,this.screenStructure);
    this.isPlan_shiftScreen=this.sharedService.isSectionVisible(menuConstants.BundleProdSectionIds.PlantandShiftwise_List.id,this.screenStructure);
    this.isbundledetailsScreen=this.sharedService.isSectionVisible(menuConstants.BundleProdSectionIds.Bundle_List.id,this.screenStructure);

    this.formObject = JSON.parse(JSON.stringify(this.chargingScreenForms.bundleProdForm));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.batchColumns = [
      {
        field: 'orderLine',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'scheduleSeqNo',
        header: 'Batch Seq. No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      
      {
        field: 'totalYieldWeight',
        header: 'Tot Yield Wt(tons)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'totalWeightBundled',
        header: 'Tot Wt Bundled(tons)',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'balancedWeight',
        header: 'Balance Wt(tons)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'actDiameter',
        header: 'Act Size(mm)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: ''
      },
      {
        field: 'planDiameter',
        header: 'Plan Size',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'scheduleNo',
        header: 'Schd No',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
    ];
    this.getValidations();
    this.getLovs('strap_t');
    this.getLovs('no_strap');
    //this.getLovs('stack');
    this.getLovs('load_c_w');
    this.globalFilterArray = ['orderLine', 'scheduleNo', 'heatId', 'batchId'];
  }

  public getValidations(){
    this.brmService.getBundleProdValidations().subscribe( data => {
      this.validationDetails = data;
      this.validationDetails.bundleWeightEnabled = data.bundleWeightEnabled.toLowerCase() == 'y' ? true : false;
    })
  }

  public getLovs(lovCode) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      let arr = data.map((ele: any) => {
        return {
          displayName: ele.valueDescription,
          modelValue: ele.codeValue,
        };
      });
      if (lovCode == 'strap_t') {
        this.strapList = arr;
      }
      if (lovCode == 'no_strap') {
        this.strapNoList = arr;
      }
      // if (lovCode == 'stack') {
      //   this.stackerList = arr;
      // }
      if(lovCode == 'load_c_w'){
        this.onePcWt = data && data.length ? parseFloat(data[0].valueShortCode) : 0;
      }
    });
  }

  public getStackerList(wcName){
    this.brmService.getStackerListByWc(wcName).subscribe(data => {
      this.stackerList = data.map((ele: any) => {
        return {
          displayName: ele.stackerName,
          modelValue: ele.stackerId,
        };
      });
      this.bundleData.stackerId = this.stackerList.length && this.stackerList.length == 1 ? this.stackerList[0].modelValue : null;
    })
    this.bundleData.selectedStrap = this.strapList.length  ? this.strapList[0].modelValue : null;
    this.bundleData.selectedStrapNo = this.strapNoList.length  ? this.strapNoList[0].modelValue : null;
  }

  public getMinAndMaxLen(data){
    this.brmService.getMinMaxLen(data.schdID).subscribe((data) => {
      this.minLength = data.lengthMin;
      this.maxLength = data.lengthMax;
      this.orderId = data.orderLine;
      this.bundleData.bundleLength = data.defaultLength;
    });
  }

  public setBundleWt(){
    this.bundleData.bundleWt = 0;
    if(this.bundleData.qtyPcs){
      this.bundleData.bundleWt = this.onePcWt * this.bundleData.qtyPcs;
    }
  }


  // New change
  public setBundlePcs(){
    this.bundleData.qtyPcs = 0;
    if(this.bundleData.bundleWt){
      this.bundleData.qtyPcs = parseInt(Math.floor(this.bundleData.bundleWt / this.onePcWt).toFixed(3));
    }
  }

  public refreshShiftData(refreshData) {
    this.refreshShiftDetails.next(refreshData);
  }

  public getBundleDetails(data){
    // this.brmService.getBundleDetails(data.schdID).subscribe((data) => {
    //   this.batchList = data;
    // });
    this.brmService.getAllBundleDetails(data.schdID, false).subscribe((data) => {
      this.batchList = data;
    });
  }

  public setBundleDetails(data) {
    this.getBundleDetails(data);
    this.scheduleDetails = data;
    this.getMinAndMaxLen(data);
    this.getStackerList(data.wcName);
  }

  //new
  public setAllBundleDetails(data){
    this.getAllBundleDetailsfun(data);
    this.scheduleDetails = data;
    this.getMinAndMaxLen(data);
    this.getStackerList(data.wcName);
  }
  
  public getAllBundleDetailsfun(data){
    this.brmService.getAllBundleDetails(data.schdID, true).subscribe((data) => {
      this.batchList = data;
    });
  }

  public generateBundle(){
    if(this.isInvalidBundleWt() || this.isInvalidPcs()){
      return;
    }
    let reqBody = {
      bundleWeight: this.bundleData.bundleWt,
      noOfStrap: this.bundleData.selectedStrapNo,
      prodWc: this.scheduleDetails.wcName,
      qtyPcs: this.bundleData.qtyPcs,
      strap: this.bundleData.selectedStrap,
      userId: this.sharedService.loggedInUserDetails.userId,
      length: this.bundleData.bundleLength,
      scheduleId: this.scheduleDetails.schdID,
      stackerId: this.bundleData.stackerId,
      orderId: this.orderId,
      isExtraBundleFlag: false,
      isScrapBatchFlag: false,
    }
    this.brmService.generateBundle(reqBody).subscribe(data => {
      this.generateBundleResponse = data;
      if(data && data.changeType){
        this.getBundleDetails(this.scheduleDetails);
        this.openModal(data);
      }
      else if(data.bundleId != null){
        
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Bundle is generated successfully'});
        //sticker print
        this.printDisplay = true;
        this.getStickerDetails(data.bundleId);
        
        this.getBundleDetails(this.scheduleDetails);
        this.refreshShiftData(true);
        this.getMinAndMaxLen(this.scheduleDetails);
      }
      else if(data.bundleId == null){
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: 'Bundle not generated'});
        this.getBundleDetails(this.scheduleDetails);
        this.refreshShiftData(true);
        this.getMinAndMaxLen(this.scheduleDetails);
      }
    })
  }

  public confirm(){
    if(!this.selectedAction){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please select action to continue'});
      return;
    }
    if(this.selectedAction == 'additionalBundle'){//additionalBundle
      // this.brmService.generateAddBundle(this.generateBundleResponse, this.sharedService.loggedInUserDetails.userId).subscribe(
      //   data => {
      //      this.closeModal();
      //      this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Bundle is generated successfully'});
      //      this.getBundleDetails(this.scheduleDetails);
      //      this.refreshShiftData(true);
      //      this.getMinAndMaxLen(this.scheduleDetails);
      //      this.bundleForm.resetForm();
      //   }
      // )
      this.generateAdditionalBundle();
    }else if(this.selectedAction == 'consume'){// consume
      this.generateConsumeBundle();
      // this.brmService.cousumeBatches(this.generateBundleResponse, this.sharedService.loggedInUserDetails.userId).subscribe(
      //   data => {
      //        this.closeModal();
      //        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Bundle is generated successfully'});
      //        this.getBundleDetails(this.scheduleDetails);
      //        this.refreshShiftData(true);
      //        this.getMinAndMaxLen(this.scheduleDetails);
      //        this.bundleForm.resetForm();
      //   }
      // )
    }else{
      this.closeModal();
    }
  }

  public openModal(data){
    this.popupMsg = `Remaining weight is ${data.bundleWeight}. Please select action to proceed further.`;
    this.remainingWeight = this.getRemainingWt(data.gradeBatchHeaderList);
    this.display = true;
  }

  public getRemainingWt(data){
    let initialValue = 0;
    if(data && data.length > 0){
       return data.reduce((total, item) => {
          return Number(total) + Number(item.weight);}, initialValue).toFixed(3);
    }else{
      return 0;
    }
  };

  public closeModal() {
    this.display = false;
    this.getBundleDetails(this.scheduleDetails);
  }

  public getTableRef(event){
    this.tableRef = event;
  }

  public filterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }

  public isInvalidBundleWt(){
    if(this.validationDetails && this.validationDetails.bundleWeightEnabled){
      let minWt = this.validationDetails.bundleWeightMin/1000;
      let maxWt = this.validationDetails.bundleWeightMax/1000;
      if(this.bundleData.bundleWt && ((this.bundleData.bundleWt < minWt) || (this.bundleData.bundleWt > maxWt))){
          return true;
      }else{
          return false;
      }
    }else{
      return false;
    }
  }

  public isInvalidPcs(){
    if(this.validationDetails && this.validationDetails.pcQtyEnabled && this.bundleData.qtyPcs && this.bundleData.qtyPcs > this.validationDetails.pcQty){
      return true;
    }else{
      return false;
    }
  }

  public ResetFilter()
  {
    this.batchList=[];
    this.bundleForm.resetForm();
  }

  public generateAdditionalBundle(){
    if(this.isInvalidBundleWt() || this.isInvalidPcs()){
      return;
    }
    this.closeModal();
    let reqBody = {
      bundleWeight: this.bundleData.bundleWt,
      noOfStrap: this.bundleData.selectedStrapNo,
      prodWc: this.scheduleDetails.wcName,
      qtyPcs: this.bundleData.qtyPcs,
      strap: this.bundleData.selectedStrap,
      userId: this.sharedService.loggedInUserDetails.userId,
      length: this.bundleData.bundleLength,
      scheduleId: this.scheduleDetails.schdID,
      stackerId: this.bundleData.stackerId,
      orderId: this.orderId,
      isExtraBundleFlag: true,
      isScrapBatchFlag: false,
    }
    this.brmService.generateBundle(reqBody).subscribe(data => {
      this.generateBundleResponse = data;
      if(data && data.changeType){
        //this.getBundleDetails(this.scheduleDetails);
       // this.openModal(data);
      }else{
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Bundle is generated successfully'});
        this.printDisplay = true;
        this.getStickerDetails(data.bundleId);
        this.getBundleDetails(this.scheduleDetails);
        this.refreshShiftData(true);
        this.getMinAndMaxLen(this.scheduleDetails);
      }
    })
  }
  public generateConsumeBundle(){
    if(this.isInvalidBundleWt() || this.isInvalidPcs()){
      return;
    }
    this.closeModal();
    let reqBody = {
      bundleWeight: this.bundleData.bundleWt,
      noOfStrap: this.bundleData.selectedStrapNo,
      prodWc: this.scheduleDetails.wcName,
      qtyPcs: this.bundleData.qtyPcs,
      strap: this.bundleData.selectedStrap,
      userId: this.sharedService.loggedInUserDetails.userId,
      length: this.bundleData.bundleLength,
      scheduleId: this.scheduleDetails.schdID,
      stackerId: this.bundleData.stackerId,
      orderId: this.orderId,
      isExtraBundleFlag: false,
      isScrapBatchFlag: true,
    }
    this.brmService.generateBundle(reqBody).subscribe(data => {
      this.generateBundleResponse = data;
      if(data && data.changeType){
        //this.getBundleDetails(this.scheduleDetails);
        //this.openModal(data);
      }else{
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Bundle is generated successfully'});
        this.printDisplay = true;
        this.getStickerDetails(data.bundleId);
        this.getBundleDetails(this.scheduleDetails);
        this.refreshShiftData(true);
        this.getMinAndMaxLen(this.scheduleDetails);
      }
    })
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      let tableHeader = [this.batchColumns.map(ele => ele.header)];
      let keyArr = this.batchColumns.map((ele:any) => { return ele.field });
      let data = this.sharedService.getDataForExcelAsPerColKeys(this.batchList, keyArr);
      this.sharedService.downloadCSV(data, 'Bundle_Production', tableHeader);
    }
  }

  //printing sticker

  public getStickerDetails(bunId: any){
    this.brmService.getStickerDetails(bunId).subscribe((response: any)=>{
      if(response){
      let barCodeURL = 'data:image/png;base64,' + response.barCodeByteArray;
      this.barCode = this.sanitizer.bypassSecurityTrustUrl(barCodeURL);
      let qrCodeURL = 'data:image/png;base64,' + response.qrCodeByteArray;
      this.qrCode = this.sanitizer.bypassSecurityTrustUrl(qrCodeURL);
      this.stickerParms = response.stickerParametersBo;
      }
    })
  }
  public print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <style>
          // height: 150mm;
          //  width: 100mm;
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()" >${printContents}</body>
      </html>`
    );
    popupWin.document.close();
}

public printClose(){
  this.printDisplay = false;
}
}
