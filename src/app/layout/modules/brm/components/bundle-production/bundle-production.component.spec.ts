import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { BrmService } from '../../services/brm.service';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ChargingScreenForms } from '../../form-objects/brm-form-objects';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { FormsModule, NgForm, ReactiveFormsModule } from '@angular/forms';
import { BundleProductionComponent } from './bundle-production.component';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConfirmationService } from 'primeng/api';
import { BrmEndpoints } from '../../form-objects/brm-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('BundleProductionComponent', () => {
  let component: BundleProductionComponent;
  let fixture: ComponentFixture<BundleProductionComponent>;
  let sharedService: SharedService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let brmService: BrmService
  let commonService: CommonApiService
  
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BundleProductionComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        BrmService,
        CommonApiService,
        JswFormComponent,
        ConfirmationService,
        BrmEndpoints,
        API_Constants,
        CommonAPIEndPoints,
        ChargingScreenForms,
        { provide: ToastrService, useValue: toastrService }, 
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BundleProductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
