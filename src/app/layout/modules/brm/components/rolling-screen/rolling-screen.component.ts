import { Component, OnInit } from '@angular/core';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { BrmService } from '../../services/brm.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-rolling-screen',
  templateUrl: './rolling-screen.component.html',
  styleUrls: ['./rolling-screen.component.css'],
})
export class RollingScreenComponent implements OnInit {
  public selectedRecords: any = [];
  public viewType = ComponentType;
  public tableType: any;
  public rollingcolumns: ITableHeader[] = [];
  public rollingList: any = [];
  public columnType = ColumnType;
  public shapeTypeList: any = [];
  public refreshShiftDetails: Subject<boolean> = new Subject<boolean>();
  public scheduleDetails: any;
  public chargingSequence: any = [];
  public requestBody: any;
  public screenStructure: any = [];
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isSaveBtn: boolean = false;
  public isfilterCriteriaScreen:boolean=false;
  public isPlan_shiftScreen:boolean=false;
  public isRollingdetailsScreen:boolean=false;
  public menuConstants = menuConstants;
  public isRollchargeScreen: boolean=false;
  public chargediv: string='content-charging';
  constructor(
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public brmService: BrmService,
    public commonService: CommonApiService,
    public authService: AuthService
  ) {
    this.rollingcolumns = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.radio,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'ipBatchId',
        header: 'Batch ID',
        columnType:  ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'orderId',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'billetSeqNo',
        header: 'Batch Seq No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },

      {
        field: 'rolledShape',
        header: 'Rolled State',
        columnType: ColumnType.dropdown,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'rolledRemarks',
        header: 'Rolling Remark',
        columnType: ColumnType.textWithValidations,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'remarks',
        header: 'Charging Remark',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },{
        field: 'dischargeRemarks',
        header: 'Discharge Remark',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'schdId',
        header: 'Schd No',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'orderSeq',
        header: 'Schd Seq No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'endOfHeat',
        header: 'End Of Heat',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'ipBatchGrade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'planDia',
        header: 'Size(mm)',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'planLength',
        header: 'Billet Len(mm)',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'billetWeight',
        header: 'Batch Wgt(tons)',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'actualDia',
        header: 'Dia Actual (mm)',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },

      {
        field: 'rolledWt',
        header: 'Actual Wgt FG(tons)',
        columnType: ColumnType.text,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'scrapWt',
        header: 'Actual Wgt Cobble(tons)',
        columnType: ColumnType.textField,
        width: '130px',
        sortFieldName: '',
      },

      {
        field: 'chargeDate',
        header: 'Time Of Charging',
        columnType: ColumnType.date,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'purchaseHeatNo',
        header: 'Pur Heat No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.ellipsis,
        width: '140px',
        sortFieldName: '',
      }
    ];
  }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.BRMScreenIds.Rolling_Screen.id)[0];
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.RollingScreenSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.RollingScreenSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.RollingScreenSectionIds.Filter_Criteria.buttons.reset, menuConstants.RollingScreenSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.RollingScreenSectionIds.Rolling_List.buttons.save, menuConstants.RollingScreenSectionIds.Rolling_List.id, this.screenStructure);
    this.isfilterCriteriaScreen=this.sharedService.isSectionVisible(menuConstants.RollingScreenSectionIds.Filter_Criteria.id,this.screenStructure);
    this.isPlan_shiftScreen=this.sharedService.isSectionVisible(menuConstants.RollingScreenSectionIds.PlantandShiftwise_List.id,this.screenStructure);
    this.isRollingdetailsScreen=this.sharedService.isSectionVisible(menuConstants.RollingScreenSectionIds.Rolling_List.id,this.screenStructure);
    this.isRollchargeScreen=this.sharedService.isSectionVisible(menuConstants.RollingScreenSectionIds.RollingCharge.id,this.screenStructure);
    if(this.isRollchargeScreen==true)
    {
      this.chargediv='content-charging'
    }
    else
    {
      this.chargediv='content'
    }
    this.getLovs(LovCodes.rolledShape);
    this.getChargingSeq();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
  }

  public getAllRollingDetails(data) {
    this.rollingList.length = 0;
    this.brmService
      .getAllRollingDetails(data.wcName, data.schdID)
      .subscribe((Response) => {
        Response.map((rowData: any) => {
          rowData.disabledFG = true;
          rowData.disabledCob = true;
          rowData.rolledWt = rowData.billetWeight;
          rowData.scrapWt = null;
          rowData.rolledShape = this.shapeTypeList.length ? this.shapeTypeList.filter(ele => ele.modelValue == 'Bund')[0].modelValue : null;
          return rowData;
        });
        this.rollingList = Response;
      });
  }

  public getChargingSeq() {
    this.brmService.getChargingSequence().subscribe((data) => {
      this.chargingSequence = data;
    });
  }

  public updateRollingWeight(): any {
    if (this.selectedRecords.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please select SO Order to Save`,
        }
      );
    } else {
      if(this.selectedRecords.filter(ele => !ele.rolledShape).length){
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          {
            message: `Please select rolled shape`,
          }
        );
      }
      else if(
        this.selectedRecords.filter((rowData:any) => (parseInt(rowData.billetWeight) != (parseInt(rowData.rolledWt) + parseInt(rowData.scrapWt))) && rowData.rolledShape=="Mix").length) {
          return this.sharedService.displayToastrMessage(
            this.sharedService.toastType.Warning,
            {
              message: `The sum of FG weight and Cobble weight should be equals to Batch weight `
            }
          );
        }
        else if(
          this.selectedRecords.filter((rowData:any) => (rowData.rolledRemarks==null || rowData.rolledRemarks=="") && rowData.rolledShape=="Mix").length) {
            return this.sharedService.displayToastrMessage(
              this.sharedService.toastType.Warning,
              {
                message: `If rolled shape is Mixed then remark should not be null`
              }
            );
          }
      else
      {
      this.requestBody = this.selectedRecords.map((x: any) => {
        return {
          actualDia: x.actualDia,
          billetWeight: x.billetWeight,
          bundleQty: x.bundleQty,
          chargeDate: x.chargeDate,
          chargeSeq: x.chargeSeq,
          cobbleQty: x.cobbleQty,
          completeStatus: x.completeStatus,
          endOfHeat: x.endOfHeat,
          heatId: x.heatId,
          ipBatchGrade: x.ipBatchGrade,
          ipBatchId: x.ipBatchId,
          ipBatchStatus: x.ipBatchStatus,
          mixedQty: x.mixedQty,
          orderSeq: x.orderSeq,
          planDia: x.planDia,
          planLength: x.planLength,
          poId: x.poId,
          purchaseHeatNo: x.purchaseHeatNo,
          remarks: x.remarks,
          rolledWt: x.rolledWt,
          schdId: x.schdId,
          scrapWt: x.scrapWt,
          rolledShape: x.rolledShape,
          rolledRemarks: x.rolledRemarks,
          dischargeRemarks:x.dischargeRemarks
        };
      });

      this.brmService
        .saveRollingDetails(
          this.sharedService.loggedInUserDetails.userId,
          this.requestBody
        )
        .subscribe((Response) => {
          this.selectedRecords = [];
          this.sharedService.displayToastrMessage(
            this.sharedService.toastType.Success, { message: Response }
          );
          this.getAllRollingDetails(this.scheduleDetails);
          this.refreshShiftData(true);
          this.getChargingSeq();
        });
      }
    }
  }

  public getLovs(lovCode) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      if (lovCode == LovCodes.rolledShape) {
        this.shapeTypeList = data.map((ele: any) => {
          return {
            displayName: ele.valueDescription,
            modelValue: ele.valueShortCode,
          };
        });
      }
    });
  }

  public checkShapeStatus(data, rowData) {
    if (rowData.rolledShape == 'Mix') {
      rowData.disabledFG = false;
      rowData.disabledCob = false;

      if (rowData.rolledWt == null) {
        rowData.actualFGError = true;
        rowData.actualFGWeightMsg="FG weight should be greater than 0"
      }

      if (rowData.scrapWt == null) {
        rowData.actualCobbalError = true;
        rowData.actualCobWeightMsg="Cobbal weight should be greater than 0"
      }

    } else if (rowData.rolledShape == 'Cob') {
      rowData.scrapWt = rowData.billetWeight;
      rowData.rolledWt = null;
      rowData.disabledFG = true;
      rowData.disabledCob = true;
      rowData.actualFGError = false;
      rowData.actualCobbalError = false;
    } else if (rowData.rolledShape == 'Bund') {
      rowData.rolledWt = rowData.billetWeight;
      rowData.scrapWt = null;
      rowData.disabledFG = true;
      rowData.disabledCob = true;
      rowData.actualFGError = false;
      rowData.actualCobbalError = false;
    } else if (rowData.rolledShape == undefined) {
      rowData.disabledFG = true;
      rowData.disabledCob = true;
      rowData.actualFGError = false;
      rowData.actualCobbalError = false;
    }
    else {
      rowData.actualFGError = false;
      rowData.actualCobbalError = false;
    }
  }

  public validateWeight(event,rowData,colField): any {
    //this.selectedRecords = rowData;
    rowData.actualFGError = false;
    rowData.actualCobbalError = false;
    //new code
    //rowData.rolledWt =parseFloat(rowData.rolledWt)
    //rowData.scrapWt =parseFloat(rowData.scrapWt)

    if (rowData.rolledShape == 'Mix') {
      if(rowData.rolledWt == rowData.billetWeight || rowData.scrapWt==rowData.billetWeight)
      {
      if (rowData.rolledWt == null || rowData.rolledWt==0) {
        rowData.actualFGError = true;
        rowData.actualFGWeightMsg="FG weight should be greater than 0"
      }

      if (rowData.scrapWt == null || rowData.scrapWt == 0) {
        rowData.actualCobbalError = true;
        rowData.actualCobWeightMsg="Cobbal weight should be greater than 0"
      }
    }

        if(colField == 'rolledWt')
        {
          if (rowData.scrapWt == null) {
             rowData.scrapWt = 0
          }
          if (rowData.rolledWt == null) {
            rowData.rolledWt =0
          }
          if ((parseFloat(rowData.billetWeight).toFixed(3)) > (parseFloat(rowData.rolledWt).toFixed(3))+ (parseFloat(rowData.scrapWt).toFixed(3))) {
              rowData.scrapWt=parseFloat(rowData.billetWeight)-parseFloat(rowData.rolledWt)
              rowData.scrapWt = (parseFloat(rowData.scrapWt).toFixed(3))
          }
        //   if (rowData.billetWeight > rowData.rolledWt + rowData.scrapWt) {
        //     rowData.scrapWt=rowData.billetWeight-rowData.rolledWt
        // }
        }
        else if(colField == 'scrapWt'){
          if (rowData.scrapWt == null) {
            rowData.scrapWt = 0
         }
         if (rowData.rolledWt == null) {
           rowData.rolledWt =0
         }
      //    if ((parseFloat(rowData.billetWeight).toFixed(3)) > (parseFloat(rowData.rolledWt).toFixed(3))+ (parseFloat(rowData.scrapWt).toFixed(3))) {
      //     rowData.rolledWt=parseFloat(rowData.billetWeight)-parseFloat(rowData.rolledWt)
      //     rowData.rolledWt = (parseFloat(rowData.rolledWt).toFixed(3))
      // }
        //  if (parseFloat(rowData.billetWeight) > parseFloat(rowData.rolledWt) + parseFloat(rowData.scrapWt)) {
        //   rowData.rolledWt=parseFloat(rowData.billetWeight)-parseFloat(rowData.scrapWt)
        //  }
        // if (rowData.billetWeight > rowData.rolledWt + rowData.scrapWt) {
        //   rowData.rolledWt=rowData.billetWeight-rowData.scrapWt
        //  }
        if ((parseFloat(rowData.billetWeight).toFixed(3)) > (parseFloat(rowData.scrapWt).toFixed(3))) {
          rowData.rolledWt=parseFloat(rowData.billetWeight)-parseFloat(rowData.scrapWt)
          rowData.rolledWt = (parseFloat(rowData.rolledWt).toFixed(3))
      }
        }

      //  if (parseFloat(rowData.billetWeight) != parseFloat(rowData.rolledWt) + parseFloat(rowData.scrapWt)) {
        //new approach
        let total = (parseFloat(rowData.rolledWt) + parseFloat(rowData.scrapWt)).toFixed(3)
        let btwgt = parseFloat(rowData.billetWeight).toFixed(3)
      if (btwgt != total) {
        rowData.actualFGError = true;
        rowData.actualCobbalError = true;
        rowData.actualCobWeightMsg="Please Enter Correct Cobbal weight "
        rowData.actualFGWeightMsg="Please Enter Correct FG weight "

        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          {
            message: `The sum of FG weight and Cobble weight should be equals to Batch weight `+rowData.billetWeight
          }
        );
      }

      if (
        (rowData.rolledRemarks == null || rowData.rolledRemarks == '') &&
        rowData.rolledShape == 'Mix'
      ) {
        rowData.rollingRemarkError = true;
      } else {
        rowData.rollingRemarkError = false;
      }
    }
  }

  public setRollingDetails(data) {
    this.getAllRollingDetails(data);
    this.scheduleDetails = data;
  }

  public refreshShiftData(refreshData) {
    this.refreshShiftDetails.next(refreshData);
  }
  public ResetFilter()
  {
    this.rollingList=[];
  }

  //new changes
  public getRowOnclick(data: any){
    this.selectedRecords = [];
    this.selectedRecords.push(data);
  }

  public closeSchd(event: any){
    this.brmService.closeSchdDetails(event.schdID,this.sharedService.loggedInUserDetails.userId).subscribe((response: any)=>{
      if(response){
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: response});
      }
      else{
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: response});
      }
    })
  }
}
