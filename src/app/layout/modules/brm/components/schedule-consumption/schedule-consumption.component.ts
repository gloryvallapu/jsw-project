import { Component, OnInit } from '@angular/core';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ScheduleConsumptionForms } from '../../form-objects/brm-form-objects';
import { BrmService } from '../../services/brm.service';
import { Subject } from 'rxjs';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-schedule-consumption',
  templateUrl: './schedule-consumption.component.html',
  styleUrls: ['./schedule-consumption.component.css'],
})
export class ScheduleConsumptionComponent implements OnInit {
  formObject: any;
  viewType = ComponentType;
  tableType: any;
  columns: ITableHeader[] = [];
  BatchStockDetailsList: any = [];
  public requestBody: any;
  public selectedBatches: any[];
  public globalFilterArray: any = [];
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public screenStructure: any = [];
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isSentTostockBtn: boolean = true;
  public isSentToSAPBtn: boolean = true;
  public isfilterCriteriaSection:boolean=false;
  public isbatchstockSection:boolean=false;
  public menuConstants = menuConstants;
  public tableRef: any;
  public tdClass ="height-max-content pt-0 pb-0";
  constructor(
    public scheduleConsumptionForms: ScheduleConsumptionForms,
    public jswComponent: JswFormComponent,
    public jswService: JswCoreService,
    public brmService: BrmService,
    public sharedService: SharedService,
    public authService: AuthService
  ) {
    this.columns = [
      {
        field: 'checkbox',
        header: 'Dummy',
        columnType: ColumnType.checkbox,
        width: '20px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Product Type',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'sizeCode',
        header: 'Product Size',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Length(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Balance Weight(tons)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'orgBatchWeight',
        header: 'Batch Weight(tons)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'jswGrade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '70px',
        sortFieldName: '',
      },
      {
        field: 'status',
        header: 'Status',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
    ];
    this.globalFilterArray = this.columns.map((element) => {
      return element.field;
    });
  }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.BRMScreenIds.Schedule_Consumption.id)[0];
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.ScheduleConsumptionSectionIds.filter_Criteria.buttons.retrieve, menuConstants.ScheduleConsumptionSectionIds.filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.ScheduleConsumptionSectionIds.filter_Criteria.buttons.reset, menuConstants.ScheduleConsumptionSectionIds.filter_Criteria.id, this.screenStructure);
    this.isSentTostockBtn = this.sharedService.isButtonVisible(true, menuConstants.ScheduleConsumptionSectionIds.BatchStock_List.buttons.senttostock, menuConstants.ScheduleConsumptionSectionIds.BatchStock_List.id, this.screenStructure);
    this.isSentToSAPBtn = this.sharedService.isButtonVisible(true, menuConstants.ScheduleConsumptionSectionIds.BatchStock_List.buttons.sentosap, menuConstants.ScheduleConsumptionSectionIds.BatchStock_List.id, this.screenStructure);
    this.isfilterCriteriaSection=this.sharedService.isSectionVisible(menuConstants.ScheduleConsumptionSectionIds.filter_Criteria.id,this.screenStructure);
    this.isbatchstockSection=this.sharedService.isSectionVisible(menuConstants.ScheduleConsumptionSectionIds.BatchStock_List.id,this.screenStructure);
    this.getUnitList();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.scheduleConsumptionForms.addScheduleFilter));
    this.selectedBatches = [];
  }

  public resetForm() {
    this.jswComponent.resetForm(this.formObject);
    this.clearSelection.next(true);
    this.BatchStockDetailsList=[];
  }

  public getRowDetails(event) {
    this.selectedBatches = event;
  }

  public sentToClick(event): any {
    if (this.selectedBatches.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: 'Please Select Batch',
        }
      );
    }
    this.requestBody = this.selectedBatches.map((x: any) => {
      return {
        batchId: x.batchId,
        jswGrade: x.jswGrade,
        length: x.lengths,
        productType: x.productType,
        sizeCode: x.sizeCode,
        weight: x.weight,
      };
    });
    this.brmService
      .updateSchdConsumptionDetails(
        'toStock',
        this.sharedService.loggedInUserDetails.userId,
        this.requestBody
      )
      .subscribe((Response) => {
        this.selectedBatches = [];
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: Response }
        );
        this.getSchdConsumptionDetails();
        this.selectedBatches = [];
        this.clearSelection.next(true);
      });
  }

  public SAPToClick(event): any {
    if (this.selectedBatches.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: 'Please Select Batch',
        }
      );
    }
    this.requestBody = this.selectedBatches.map((x: any) => {
      return {
        batchId: x.batchId,
        jswGrade: x.jswGrade,
        length: x.lengths,
        productType: x.productType,
        sizeCode: x.sizeCode,
        weight: x.weight,
      };
    });
    this.brmService
      .updateSchdConsumptionDetails(
        'toSAP',
        this.sharedService.loggedInUserDetails.userId,
        this.requestBody
      )
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: Response }
        );
        this.getSchdConsumptionDetails();
        this.selectedBatches = [];
        this.clearSelection.next(true);
      });
  }

  public getSchdConsumptionDetails() {
    let wcName = this.formObject.formFields.filter(
      (ele: any) => ele.ref_key == 'wcName'
    )[0].value;
    let schdId = this.formObject.formFields.filter(
      (ele: any) => ele.ref_key == 'schdId'
    )[0].value;
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formObject = this.jswService.getFormData();
    this.brmService
      .getSchdConsumptionDetails(wcName, schdId ? schdId : 0)
      .subscribe((Response) => {
        Response.map((rowData: any) => {
          return rowData;
        });
        this.BatchStockDetailsList = Response;
      });
  }

  public dropdownChangeEvent(event) {
    if (event.ref_key == 'wcName') {
    }
    if (event.ref_key == 'wcName' && event.value != null) {
      let heatIdListByUnit = this.formObject.formFields
        .filter((ele) => ele.ref_key == 'wcName')[0]
        .list.filter((x: any) => x.modelValue == event.value)[0];
      this.formObject.formFields.filter(
        (ele) => ele.ref_key == 'schdId'
      )[0].list = heatIdListByUnit.schdIdList.map((x: any) => {
        return {
          displayName: x,
          modelValue: x,
        };
      });
    }
  }

  public getUnitList() {
    this.brmService
      .getSchdConsumptionFilterDetails()
      .subscribe((data) => {
        this.formObject.formFields.filter(
          (obj: any) => obj.ref_key == 'wcName'
        )[0].list = data.map((x: any) => {
          return {
            displayName: x.wcName,
            modelValue: x.wcName,
            schdIdList: x.schdIdList,
          };
        });
      });
  }

  public getTableRef(event){
    this.tableRef = event;
  }
  public filterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }
}

