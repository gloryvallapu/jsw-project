import { Component, OnInit } from '@angular/core';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { AdditionalBundleProdForms, ChargingScreenForms, ScheduleConsumptionForms } from '../../form-objects/brm-form-objects';
import { BrmService } from '../../services/brm.service';
import { of, Subject } from 'rxjs';
import { ScheduleConsumptionComponent } from './schedule-consumption.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrmEndpoints } from '../../form-objects/brm-api-endpoints';
import { ConfirmationService } from 'primeng/api';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('ScheduleConsumptionComponent', () => {
  let component: ScheduleConsumptionComponent;
  let fixture: ComponentFixture<ScheduleConsumptionComponent>;
  let scheduleConsumptionForms: ScheduleConsumptionForms;
  let sharedService;
  let brmService: BrmService;
  let jswService: JswCoreService;
  let jswComponent: JswFormComponent;
  let formObjects : ScheduleConsumptionForms
  const wCresp = [{
    wcName : "LP1",
    lpScheduleIdList:[305, 489, 508, 384]
  }]
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleConsumptionComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule,
   ],
   providers:[
    JswCoreService,
    BrmEndpoints,
    ChargingScreenForms,
    ConfirmationService,
    BrmService,
    ScheduleConsumptionForms,
    CommonApiService,
    API_Constants,
    CommonAPIEndPoints,
    JswFormComponent,
    { provide: ToastrService, useValue: toastrService },
]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleConsumptionComponent);
    component = fixture.componentInstance;
    brmService = fixture.debugElement.injector.get(BrmService);
    sharedService = fixture.debugElement.injector.get(SharedService);
    formObjects = fixture.debugElement.injector.get(ScheduleConsumptionForms);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
