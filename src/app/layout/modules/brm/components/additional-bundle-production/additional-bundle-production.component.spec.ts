import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { AdditionalBundleProdForms } from '../../form-objects/brm-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { BrmService } from '../../services/brm.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { of, Subject } from 'rxjs';
import { AdditionalBundleProductionComponent } from './additional-bundle-production.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrmEndpoints } from '../../form-objects/brm-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { IndividualConfig, ToastrService } from 'ngx-toastr';

describe('AdditionalBundleProductionComponent', () => {
  let component: AdditionalBundleProductionComponent;
  let fixture: ComponentFixture<AdditionalBundleProductionComponent>;
  let sharedService: SharedService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let brmService: BrmService
  let commonService: CommonApiService
  let formObjects : AdditionalBundleProdForms

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  const wCresp = [{
    wcName : "LP1",
    lpScheduleIdList:[305, 489, 508, 384]
  }]

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdditionalBundleProductionComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        BrmService,
        AdditionalBundleProdForms,
        CommonApiService,
        JswFormComponent,
        BrmEndpoints,
        API_Constants,
        CommonAPIEndPoints,
        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalBundleProductionComponent);
    component = fixture.componentInstance;
    brmService = fixture.debugElement.injector.get(BrmService);
    sharedService = fixture.debugElement.injector.get(SharedService);
    formObjects = fixture.debugElement.injector.get(AdditionalBundleProdForms);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get Work Center List',() => {
    brmService.getAddBundleFilterDetails();
    spyOn(brmService, 'getAddBundleFilterDetails').and.returnValues(of(wCresp));
     component.getUnitList();
     expect(component.filterFormObject.formFields.filter((x: any) => x.ref_key == 'wcName')[0].list.length).toBeGreaterThan(0);
   });

});
