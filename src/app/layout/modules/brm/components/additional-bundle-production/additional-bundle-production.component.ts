import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { AdditionalBundleProdForms } from '../../form-objects/brm-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { BrmService } from '../../services/brm.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { Subject } from 'rxjs';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';
@Component({
  selector: 'app-additional-bundle-production',
  templateUrl: './additional-bundle-production.component.html',
  styleUrls: ['./additional-bundle-production.component.css']
})
export class AdditionalBundleProductionComponent implements OnInit {

  public viewType = ComponentType;
  public columnType = ColumnType;
  public heatColumns: ITableHeader[] = [];
  public batchColumns: ITableHeader[] = [];
  public tableType: any;
  public filterFormObject: any = [];
  public heatDetails: any = [];
  public responseData: any = [];
  public batchDetails: any = [];
  public addBundFilterCriteria: any;
  public data: any;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public loadCellWeight: any;
  public balanceWeight: any;
  public bundWtValidateFailed: boolean;
  public generatedBundleList: any;
  public selectedWcName: any;
  public selectedSchdId: any;
  public isSaveBtnDisable: boolean;
  public screenStructure: any = [];
  public isRetriveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isHeatSaveBtn: boolean = true;
  public isBatchSaveBtn: boolean = true;
  public isBatchDetailsSection:boolean=false;
  public isHeatDetailsSection:boolean=false;
  public isfilterSection:boolean=false;
  public menuConstants = menuConstants;
  public selectedList: any = {
    "batchIdList": [],
    "scheduleId": 0,
    "heatId": [],
    "grade": [],
    "sizeCode": [],
    "noOfBundle": 0,
    "weight": 0,
    "length": [],
    "productType": "",
    "materialId": ""
  };

  public radioSelected: any =[];

  constructor(
    public additionalBundleProdForms: AdditionalBundleProdForms,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public brmService: BrmService,
    public commonService: CommonApiService,
    public authService: AuthService) {
    this.heatColumns = [
      {
        field: 'radio',
        header: '',
        columnType: ColumnType.radio,
        width: '40px',
        sortFieldName: '',
      },
      {
        field: 'scheduleId',
        header: 'Schedule ID',
        columnType: ColumnType.number,
        width: '70px',
        sortFieldName: '',
      },
      // {
      //   field: 'schSeqNo',
      //   header: 'Schedule Seq No',
      //   columnType: ColumnType.string,
      //   width: '80px',
      //   sortFieldName: '',
      // },
      {
        field: 'orderId',
        header: 'Order ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Prod. Type',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'sizeCode',
        header: 'Prod. Size',
        columnType: ColumnType.string,
        width: '70px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Len.(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '70px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Balance Weight (tons)',
        columnType: ColumnType.number,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'generationSrc',
        header: 'Generation Src',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'noOfBundle',
        header: 'No of Bundles',
        columnType: ColumnType.numericValidationField,
        width: '90px',
        sortFieldName: '',
      }
    ];
    this.batchColumns = [
      {
        field: 'batchId',
        header: 'Bundle ID',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'scheduleId',
        header: 'Schedule ID',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Length(mm)',
        columnType: ColumnType.numericValidationField,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'pcsInBundle',
        header: 'No of Pieces',
        columnType: ColumnType.numericValidationField,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'batchStatus',
        header: 'Batch Status',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'batchClass',
        header: 'Batch Class',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'orderId',
        header: 'Order ID',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Product Type',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'sizeCode',
        header: 'Product Size',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },

      {
        field: 'weight',
        header: 'Weight (tons)',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'jswGrade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },

      {
        field: 'prodDate',
        header: 'Prod Date/Time',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
        dateType: 'short'
      },
      {
        field: 'sapSalesOrder',
        header: 'SAP Sales Order',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      }
    ];
  }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.BRMScreenIds.Additional_Production.id)[0];
    this.isRetriveBtn = this.sharedService.isButtonVisible(true, menuConstants.AdditionalProdSectionIds.filter_Criteria.buttons.retrieve, menuConstants.AdditionalProdSectionIds.filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.AdditionalProdSectionIds.filter_Criteria.buttons.reset, menuConstants.AdditionalProdSectionIds.filter_Criteria.id, this.screenStructure);
    this.isBatchSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.AdditionalProdSectionIds.Batch_List.buttons.save, menuConstants.AdditionalProdSectionIds.Batch_List.id, this.screenStructure);
    this.isHeatSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.AdditionalProdSectionIds.Heat_List.buttons.save, menuConstants.AdditionalProdSectionIds.Heat_List.id, this.screenStructure);
    this.isfilterSection=this.sharedService.isSectionVisible(menuConstants.AdditionalProdSectionIds.filter_Criteria.id,this.screenStructure);
    this.isBatchDetailsSection=this.sharedService.isSectionVisible(menuConstants.AdditionalProdSectionIds.Batch_List.id,this.screenStructure);
    this.isHeatDetailsSection=this.sharedService.isSectionVisible(menuConstants.AdditionalProdSectionIds.Heat_List.id,this.screenStructure);
    this.getUnitList();
    this.getLovs(LovCodes.loadCellWeight);
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.filterFormObject = JSON.parse(JSON.stringify(this.additionalBundleProdForms.addBundFilterCriteria));
  }

  //To fetch the load cell value
  public getLovs(lovCode) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      if (lovCode == LovCodes.loadCellWeight) {
        this.loadCellWeight = data.map((ele: any) => {
          return {
            displayName: ele.valueDescription,
            modelValue: ele.valueShortCode,
          };
        });
      }
    });
  }

  // Fetch heat Id by work center
  public dropdownChangeEvent(event) {
    if (event.ref_key == 'wcName' && event.value != null) {
      let schdIdListByUnit = this.filterFormObject.formFields.filter(ele => ele.ref_key == 'wcName')[0].list.filter((x: any) => x.modelValue == event.value)[0]
      this.filterFormObject.formFields.filter(ele => ele.ref_key == 'schdId')[0].list = schdIdListByUnit.schdIdList.map((x: any) => {
        return {
          displayName: x,
          modelValue: x
        }
      });
    }
  }

  // Get work center list for drop down
  public getUnitList() {
    this.brmService.getAddBundleFilterDetails().subscribe((data) => {
      this.filterFormObject.formFields.filter(
        (obj: any) => obj.ref_key == 'wcName'
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.wcName,
          modelValue: x.wcName,
          schdIdList: x.schdIdList
        };
      });
    });
  }

  // Get heat details
  public getAddBundHeatDetails() {
    this.jswComponent.onSubmit(this.filterFormObject.formName, this.filterFormObject.formFields);
    var filterFormObject = this.jswService.getFormData();
    this.selectedWcName = this.filterFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'wcName'
    )[0].value;
    this.selectedSchdId = this.filterFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'schdId'
    )[0].value;
    this.fetchHeatTableDetails(this.selectedWcName, this.selectedSchdId)
    if (this.selectedWcName != null && this.selectedSchdId != null) {
      this.brmService.getAddBundleDetails(this.selectedWcName, this.selectedSchdId).subscribe(Response => {
        if (Response) {
          this.responseData[0] = Response
          //this.setToolTipData(this.responseData);
          this.balanceWeight = Response.weight
          this.heatDetails = Response;
        }
      })
    }
  }

  //Set tool tip data
  public setToolTipData(data) {
    data.map((ele: any) => {
      let heatIdArr = ele.heatId.map((heatids: any) => {
        return heatids;
      });
      ele.heatId = heatIdArr.join(',');
      let sizeCodeArr = ele.sizeCode.map((sizeCodes: any) => {
        return sizeCodes;
      });
      ele.sizeCode = sizeCodeArr.join(',');
      let gradeArr = ele.grade.map((grades: any) => {
        return grades;
      });
      ele.grade = gradeArr.join(',');
      let lengthArr = ele.length.map((lengths: any) => {
        return lengths;
      });
      let ordArr = ele.orderId.map((id: any) => {
        return id;
      });
      ele.orderId = ordArr.join(',');
      ele.length = lengthArr.join(',');
      ele.weight = ele.weight.toFixed(4)
      return ele;
    });
    this.heatDetails = data;
  }

  //Get selected record by radio button
  public fetchHeatTableDetails(wcName, schdId): any {
    if (wcName != null && schdId != null) {
      this.brmService.getAddBundleDetails(wcName, schdId).subscribe(Response => {
        if (Response) {
          this.selectedList = Response
          this.balanceWeight = Response.weight
        }
      })
    }
  }

  // Save button for generate bundle
  public sendTableData(event) {
   // this.generateBundle()
   //new change
   this.generateNewBundle();
    this.isSaveBtnDisable = false
  }

  public generateBundle(): any {
    let colObj = this.heatColumns.filter(ele => ele.field == 'noOfBundle')[0];
    colObj.rowIndex = 0;

    if (this.selectedList.noOfBundle == null || this.selectedList.noOfBundle == 0) {
      colObj.isError = true;
      colObj.errorMsg = 'Enter number of bundles';
    }
    else {
      this.brmService.generateAdditionalBundle(this.selectedList)
        .subscribe((Response) => {
          Response.map((rowData: any) => {
            return rowData;
          })
          this.batchDetails = Response
        });
      colObj.isError = false;
      colObj.errorMsg = '';

      var index = 0
      for (index = 0; index < this.batchDetails.length; index++) {
        let colObjNoOfPcs = this.batchColumns.filter(ele => ele.field == 'pcsInBundle')[0];
        colObjNoOfPcs.rowIndex = index;
        colObjNoOfPcs.disableCol = false

        let colObjLength = this.batchColumns.filter(ele => ele.field == 'length')[0];
        colObjLength.rowIndex = index;
        colObjLength.disableCol = false
      }
    }
    this.isSaveBtnDisable = false
  }

  public validateBundleWeight(event) {
    if (event.colField == 'noOfBundle') {
      let colObj = this.heatColumns.filter(ele => ele.field == 'noOfBundle')[0];
      colObj.rowIndex = event.rowIndex;
      this.selectedList.noOfBundle = event.rowData.noOfBundle
      if (event.rowData.noOfBundle <= 0 || event.rowData.noOfBundle == null) {
        colObj.isError = true;
        colObj.errorMsg = 'Enter number of bundles';
      } else {
        colObj.isError = false;
        colObj.errorMsg = '';
      }
    }

  }

  // Calculate the weight by given pieces
  public getWeightAndLength(event) {
    if (event.colField == 'pcsInBundle') {
      let colObj = this.batchColumns.filter(ele => ele.field == 'pcsInBundle')[0];
      colObj.rowIndex = event.rowIndex;
      this.selectedList.pcsInBundle = event.rowData.pcsInBundle
      if (event.rowData.pcsInBundle <= 0 || event.rowData.pcsInBundle == null) {
        colObj.isError = true;
        colObj.errorMsg = 'No of Pieces should not be empty or 0';
      } else {
        let colObj = this.batchColumns.filter(ele => ele.field == 'pcsInBundle')[0];
        colObj.rowIndex = event.rowIndex;
        //var bundleWeight = Number(this.loadCellWeight[0].modelValue * event.rowData.pcsInBundle)*1000
        var bundleWeight = Number(this.loadCellWeight[0].modelValue * event.rowData.pcsInBundle)
        var index = 0, totalWeight = 0.0000
        for (index = 0; index < event.tableData.length; index++) {
          var rowWeight = event.tableData[index].weight != null ? (event.tableData[index].weight) : 0.0000
          totalWeight = Number(totalWeight) + Number(rowWeight)
        }
        totalWeight = totalWeight - Number(event.rowData.weight ? event.rowData.weight : 0.0000) + Number(bundleWeight)
        if (totalWeight > this.balanceWeight) {
          this.bundWtValidateFailed = true
          colObj.isError = true;
          colObj.errorMsg = 'The balance Cast weight exceeded';
        } else {
          this.bundWtValidateFailed = false
          event.rowData.weight = bundleWeight.toFixed(4)
          colObj.isError = false;
          colObj.errorMsg = '';
        }
      }
    }
    else if (event.colField == 'length') {
      let colObj = this.batchColumns.filter(ele => ele.field == 'length')[0];
      colObj.rowIndex = event.rowIndex;
      this.selectedList.length = event.rowData.length
      if (event.rowData.length <= 0 || event.rowData.length == null) {
        colObj.isError = true;
        colObj.errorMsg = 'length should not be empty or 0';
      } else {
        colObj.isError = false;
        colObj.errorMsg = '';
      }
    }
  }

  public saveBundleDetails(event): any {
    this.generatedBundleList = event
    var index = 0
    var isNoOfPcsEmpty: boolean = false
    var isLengthEmpty: boolean = false
    for (index = 0; index < this.generatedBundleList.length; index++) {
      if (this.generatedBundleList[index].pcsInBundle == null || this.generatedBundleList[index].pcsInBundle <= 0) {
        isNoOfPcsEmpty = true
        let colObj = this.batchColumns.filter(ele => ele.field == 'pcsInBundle')[0];
        colObj.rowIndex = index;
        colObj.isError = true;
        colObj.errorMsg = 'No of Pieces should not be empty or 0';
      }
      if (this.generatedBundleList[index].length == null || this.generatedBundleList[index].length <= 0) {
        isLengthEmpty = true
        let colObj = this.batchColumns.filter(ele => ele.field == 'length')[0];
        colObj.rowIndex = index;
        colObj.isError = true;
        colObj.errorMsg = 'length should not be empty or 0';
      }
    }
    if (isNoOfPcsEmpty) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `No of Pieces should not be empty or 0`
        }
      );
    }
    else if(isLengthEmpty){
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Length should not be empty or 0`
        }
      );
    }
    else if (this.bundWtValidateFailed || this.bundWtValidateFailed == null) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please provide the correct bundle weight`,
        }
      );
    }
    else {
      var requestBody = this.generatedBundleList.map((x: any) => {
        return {
          "batchId": x.batchId,
          "batchIdList": this.selectedList.batchIdList,
          "heatId": x.heatId,
          "jswGrade": null,
          "length": x.length,
          "materialId": x.materialId,
          "orderId": x.orderId,
          "pcsInBundle": x.pcsInBundle,
          "prodDate": "2021-04-22T15:37:44.661Z",
          "productType": x.productType,
          "scheduleId": x.scheduleId,
          "sizeCode": "string",
          "weight": Number(x.weight),
          "prodWc": this.selectedWcName,
          "scheduleOrdSeq": x.scheduleOrdSeq
        }
      })
      this.brmService.saveAdditionalBundle(this.sharedService.loggedInUserDetails.userId, requestBody)
        .subscribe((Response) => {
          if (Response) {
            this.batchDetails = Response
          }
          return this.sharedService.displayToastrMessage(
            this.sharedService.toastType.Success,
            {
              message: `Records are saved successfully`
            }
          );
        });
      for (index = 0; index < this.generatedBundleList.length; index++) {
        let colObjNoOfPcs = this.batchColumns.filter(ele => ele.field == 'pcsInBundle')[0];
        colObjNoOfPcs.rowIndex = index;
        colObjNoOfPcs.isError = false;
        colObjNoOfPcs.disableCol = true
        let colObjLength = this.batchColumns.filter(ele => ele.field == 'length')[0];
        colObjLength.rowIndex = index;
        colObjLength.isError = false;
        colObjLength.disableCol = true
      }
      this.getAddBundHeatDetails()
      this.isSaveBtnDisable = true
      this.balanceWeight = 0
      this.bundWtValidateFailed = false
      this.generatedBundleList = null;
      this.selectedWcName = null;
      this.selectedSchdId = null;
    }
  }

  //Reset the form data
  public resetFilter() {
    this.jswComponent.resetForm(this.filterFormObject);
    this.clearSelection.next(true);
    this.heatDetails=[];
    this.batchDetails=[];
  }
  public editRecord(event: any){
    this.radioSelected = event;
    // this.radioSelected = [];
    // this.radioSelected.push(event);
  }
  public generateNewBundle(): any {
    let colObj = this.heatColumns.filter(ele => ele.field == 'noOfBundle')[0];
    colObj.rowIndex = 0;

    if (this.radioSelected.noOfBundle == null || this.radioSelected.noOfBundle == 0) {
      colObj.isError = true;
      colObj.errorMsg = 'Enter number of bundles';
    }
    else {
      this.brmService.generateAdditionalBundle(this.radioSelected)
        .subscribe((Response) => {
          Response.map((rowData: any) => {
            return rowData;
          })
          this.batchDetails = Response
        });
      colObj.isError = false;
      colObj.errorMsg = '';

      var index = 0
      for (index = 0; index < this.batchDetails.length; index++) {
        let colObjNoOfPcs = this.batchColumns.filter(ele => ele.field == 'pcsInBundle')[0];
        colObjNoOfPcs.rowIndex = index;
        colObjNoOfPcs.disableCol = false

        let colObjLength = this.batchColumns.filter(ele => ele.field == 'length')[0];
        colObjLength.rowIndex = index;
        colObjLength.disableCol = false
      }
    }
    this.isSaveBtnDisable = false
  }

}
