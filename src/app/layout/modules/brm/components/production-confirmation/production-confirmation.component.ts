import { Component, OnInit, ViewChild } from '@angular/core';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { BrmService } from '../../services/brm.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Table } from 'primeng/table';
import { Subject } from 'rxjs';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-production-confirmation',
  templateUrl: './production-confirmation.component.html',
  styleUrls: ['./production-confirmation.component.css']
})
export class ProductionConfirmationComponent implements OnInit {

  public selectedRecords: any = []
  public data: any;
  public viewType = ComponentType;
  public tableType: any;
  public columns: ITableHeader[] = [];
  public productionList: any = [];
  public columnType = ColumnType;
  public scheduleDetails: any;
  public requestBody: any;
  public globalFilterArray: any = [];
  @ViewChild('dt') table: Table;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public screenStructure: any = [];
  public isRefreshBtn: boolean = false;
  public isReleaseBtn: boolean = false;
  public isProdSection:boolean=false;
  public menuConstants = menuConstants;
  public tableRef: any;
  constructor(
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public brmService: BrmService,
    public commonService: CommonApiService,
    public authService: AuthService
  ) {
    this.columns = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Bundle ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'orderId',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'bunHeatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      
      {
        field: 'productType',
        header: 'Prod. Type',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'batchIdList',
        header: 'Input Batch',
        columnType: ColumnType.ellipsis,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'size',
        header: 'Size (mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Len (mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'pcsInBundle',
        header: 'Qty Pcs',
        columnType: ColumnType.string,
        width: '70px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Wgt(tons)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'batchClass',
        header: 'Class',
        columnType: ColumnType.string,
        width: '60px',
        sortFieldName: '',
      },
      {
        field: 'stickerId',
        header: 'Stacker ID',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.ellipsis,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'shiftId',
        header: 'Shift',
        columnType: ColumnType.string,
        width: '60px',
        sortFieldName: '',
      },
      {
        field: 'prodDate',
        header: 'Production Date',
        columnType: ColumnType.date,
        width: '120px',
        sortFieldName: '',
        dateType:'short'
      },
      
      {
        field: 'origPoId',
        header: 'PO ID',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      }
      
    ];
    this.globalFilterArray = this.columns.map((element) => {
      return element.field;
    });
  }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.BRMScreenIds.Production_confirmation.id)[0];
    this.isRefreshBtn = this.sharedService.isButtonVisible(true, menuConstants.ProdConformationSectionIds.ProductType_List.buttons.refresh, menuConstants.ProdConformationSectionIds.ProductType_List.id, this.screenStructure);
    this.isReleaseBtn = this.sharedService.isButtonVisible(true, menuConstants.ProdConformationSectionIds.ProductType_List.buttons.release, menuConstants.ProdConformationSectionIds.ProductType_List.id, this.screenStructure);
    this.isProdSection=this.sharedService.isSectionVisible(menuConstants.ProdConformationSectionIds.ProductType_List.id,this.screenStructure);
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getAllProdConfDetails();
  }

  public selectedBatchRecord(rowData) {
    this.selectedRecords = rowData;
  }

  // public btnClicked(event) {
  //   if(event.target.name=='release')
  //   {
  //     this.updateProdConfirmationDetails();
  //   }
  //   else if(event.target.name=='refresh')
  //   {
  //     this.getAllProdConfDetails();
  //     this.clearSelection.next(true);
  //   }
  // }

  //changed as btn moved to blue bar
  public btnClicked(name) {
    if(name=='release')
    {
      this.updateProdConfirmationDetails();
    }
    else if(name=='refresh')
    {
      this.getAllProdConfDetails();
      this.clearSelection.next(true);
    }
  }

  public filterTable(event: any) {
    this.table.filterGlobal(event.target.value, 'contains');
  }

  // Get production confirmation details
  public getAllProdConfDetails() {
    this.brmService.getAllProdConfDetails().subscribe(Response => {
      // Response.map((rowData: any) => {
      //   if (Response) {
      //     this.setToolTipData(Response);
      //   }
      // })
      this.productionList = Response;
      if(this.productionList){ this.setToolTipData(Response); }
    })
  }

    //Set tool tip data
  public setToolTipData(data) {
    data.map((ele: any) => {
      let heatIdArr = ele.batchIdList.map((heatids: any) => {
        return heatids;
      });
      ele.batchIdList = heatIdArr.join(',');
    })
    this.productionList = data;
  }

  // Release the batch for confirmation
  public updateProdConfirmationDetails(): any {
    if (this.selectedRecords.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: `Please select bundle to release`,
      }
      );
    }
    else {
      this.requestBody = this.selectedRecords.map((x: any) => {
        return {
          "batchId": x.batchId,
          "materialId": x.materialId,
          "length": x.length,
          "productType": x.productType,
          "weight": x.weight,
          "prodDate": x.prodDate,
          "shiftId": x.shiftId,
          "batchClass": x.batchClass,
          "stickerId": x.stickerId,
          "origPoId": x.origPoId,
          "pcsInBundle": x.pcsInBundle,
          "orderId": x.orderId,
          "thickness": x.thickness,
          "customerName": x.customerName,
        }
      })

      this.brmService.updateProdConfirmationDetails(this.sharedService.loggedInUserDetails.userId, this.requestBody).subscribe(Response => {
        this.selectedRecords = []
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success, {
          message: "Batch production confirmation is successfully done",
        }
        );
        this.getAllProdConfDetails();
        this.clearSelection.next(true);
      })
    }
  }
  public getTableRef(event){
    this.tableRef = event;
  }
  public newfilterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }
}

