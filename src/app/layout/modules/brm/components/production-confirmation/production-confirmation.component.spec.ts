import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { BrmService } from '../../services/brm.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Table } from 'primeng/table';
import { Subject } from 'rxjs';
import { ProductionConfirmationComponent } from './production-confirmation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { BatchTransferForms } from '../../form-objects/brm-form-objects';
import { BrmEndpoints } from '../../form-objects/brm-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { IndividualConfig, ToastrService } from 'ngx-toastr';

describe('ProductionConfirmationComponent', () => {
  let component: ProductionConfirmationComponent;
  let fixture: ComponentFixture<ProductionConfirmationComponent>;
  let sharedService: SharedService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let brmService: BrmService
  let commonService: CommonApiService

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductionConfirmationComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        BrmService,
        CommonApiService,
        JswFormComponent,
        BrmEndpoints,
        API_Constants,
        CommonAPIEndPoints,
        { provide: ToastrService, useValue: toastrService }, 
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
