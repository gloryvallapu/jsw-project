import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ChargingScreenForms } from '../../../form-objects/brm-form-objects';
import {BatchTransferForms} from '../../../form-objects/brm-form-objects';
import { TableType, ColumnType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { BrmService } from '../../../services/brm.service';
import { JswCoreService} from 'jsw-core';
import { JswFormComponent } from 'jsw-core';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Observable, of, Subscription } from 'rxjs';
import { FilterCriteriaComponent } from './filter-criteria.component';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { BrmEndpoints } from '../../../form-objects/brm-api-endpoints';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { Data } from '@angular/router';


describe('FilterCriteriaComponent', () => {
  let component: FilterCriteriaComponent;
  let fixture: ComponentFixture<FilterCriteriaComponent>;
  let sharedService: SharedService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let brmService: BrmService


  
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };
  
  class SomeService {
    refreshShiftDataEvent(): Observable<boolean>{
      return of(false);
    }
  }
  let mockSomeService = {
    refreshShiftDataEvent: () => {}
  }
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterCriteriaComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        BrmService,
        CommonApiService,
        JswFormComponent,
        ChargingScreenForms,
        BrmEndpoints,
        API_Constants,
        CommonAPIEndPoints,
        { provide: ToastrService, useValue: toastrService }, 
        { provide: SomeService, useValue: mockSomeService }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterCriteriaComponent);
    component = fixture.componentInstance;
    // spyOn(mockSomeService, 'refreshShiftDataEvent').and.returnValues({ subscribe: () => {} })
    
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
