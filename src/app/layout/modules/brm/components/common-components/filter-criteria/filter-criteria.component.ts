import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ChargingScreenForms } from '../../../form-objects/brm-form-objects';
import { TableType, ColumnType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { BrmService } from '../../../services/brm.service';
import { JswCoreService} from 'jsw-core';
import { JswFormComponent } from 'jsw-core';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Observable, Subscription } from 'rxjs';
@Component({
  selector: 'app-filter-criteria',
  templateUrl: './filter-criteria.component.html',
  styleUrls: ['./filter-criteria.component.css']
})
export class FilterCriteriaComponent implements OnInit {
  public viewType = ComponentType;
  public formObject: any;
  public tableType: any;
  public lpSchColumns: ITableHeader[] = [];
  public lpSchList: any = [];
  public shiftColumns: ITableHeader[] = [];
  public shiftData: any = [];
  public unitSchList: any = [];
  @Input() screen: string;
  @Input() refreshShiftDataEvent: Observable<boolean>;
  @Input() Resetbtn:boolean;
  @Input() Retrivebtn:boolean;
  @Input() filterCriteriaDisplay:boolean;
  @Input() PlanShiftDisplay:boolean;
  @Output() public setChargingDetails = new EventEmitter<Object>();
  @Output() public setRollingDetails = new EventEmitter<Object>();
  @Output() public setDischargingDetails = new EventEmitter<Object>();
  @Output() public setBundleDetails = new EventEmitter<Object>();
  @Output() public ResetChargingDetails = new EventEmitter<Object>();
  @Output() public ResetRollingDetails = new EventEmitter<Object>();
  @Output() public ResetDischargingDetails = new EventEmitter<Object>();
  @Output() public ResetBundleDetails = new EventEmitter<Object>();

  @Output() public setAllBundleDetails = new EventEmitter<Object>();

  @Output() public setCloseSchDetails = new EventEmitter<Object>();
  private eventSubscription: Subscription;
  public totalAttachedNos: number = 0;
  public totalAttachedWt: number = 0;
  public totalChargedNos: number = 0;
  public totalChargedWt: number = 0;
  public totalDischargedNos: number = 0;
  public totalDischargedWt: number = 0;
  public totalRolledNos: number = 0;
  public totalRolledWt: number = 0;
  public totalCobbledNos: number = 0;
  public totalCobbledWt: number = 0;
  public totalMixedNos: number = 0;
  public totalMixedWt: number = 0;
  public totalHotoutNos: number = 0;
  public totalHotoutWt: number = 0;
  public totalBundleNos: number = 0;
  public totalBundleWt: number = 0;
  public selectedSchedule: any;
  public chargingDetails: any = [];
  public currentShiftName: any;
  public filterRetrieved = false;
  public displayPopup = false;
  constructor(public chargingScreenForms : ChargingScreenForms, public brmService: BrmService, public jswService:JswCoreService,
    public jswComponent:JswFormComponent, public sharedService: SharedService) { }

  ngOnInit(): void {
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.chargingScreenForms.chargingFilterForm));
    this.lpSchColumns = [
      
      {
        field: 'salesOrderNo',
        header: 'Sales Order',
        columnType: ColumnType.number,
        width: '150px',
        sortFieldName: '',
      },
      // {
      //   field: 'lineNo',
      //   header: 'Order Line No',
      //   columnType: ColumnType.number,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      {
        field: 'batchGrade',
        header: 'Batch Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'heatNo',
        header: 'Heat No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Heat No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchAttached',
        header: 'Heat No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'scheduleId',
        header: 'Schedule ID',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      }
    ]

    this.eventSubscription = this.refreshShiftDataEvent.subscribe(() => {
      this.getShiftDetails()
      this.retrieveLPSchedules();
    }
    );

    if(this.screen == 'charging'){
      this.getUnitSchDetails('charging');
    }
    if(this.screen == 'discharging'){
      this.getUnitSchDetails('discharging');
    }
    if(this.screen == 'rolling'){
      this.getUnitSchDetails('rolling');
    }
    if(this.screen == 'bundleProd'){
      this.getUnitSchDetails('bundleProd');
    }
    this.getShiftDetails();
  }

  ngOnDestroy() {
    this.eventSubscription.unsubscribe();
  }

  public getShiftDetails(){
    this.brmService.getShiftDetails().subscribe(data => {
      this.currentShiftName = data.currentShiftName;
      this.shiftData = data.shiftWiseData;
    })
  }

  public getUnitSchDetails(screenName){
    this.unitSchList = [];
    if(screenName == 'charging'){
      this.brmService.getUnitsForCharging().subscribe(data => {
        this.unitSchList = data;
        this.formObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list = this.unitSchList.map((x: any) => {
          return { displayName: x.wcName, modelValue: x.wcName };
        });
      })
    }

    if(screenName == 'discharging'){
      this.brmService.getUnitsForDischarging().subscribe(data => {
        this.unitSchList = data;
        this.formObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list = this.unitSchList.map((x: any) => {
          return { displayName: x.wcName, modelValue: x.wcName };
        });
      })
    }

    if(screenName == 'rolling'){
      this.brmService.getUnitsForRolling().subscribe(data => {
        this.unitSchList = data;
        this.formObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list = this.unitSchList.map((x: any) => {
          return { displayName: x.wcName, modelValue: x.wcName };
        });
      })
    }

    if(screenName == 'bundleProd'){
      this.brmService.getUnitsForBundleProd().subscribe(data => {
        this.unitSchList = data;
        this.formObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list = this.unitSchList.map((x: any) => {
          return { displayName: x.wcName, modelValue: x.wcName };
        });
      })
    }
  }

  public dropdownChangeEvent(event){
    if (event.ref_key == 'wcName') { // unit dropdown change event
      this.getSchdByWC(event.value);
    }
  }

  public getSchdByWC(id) {
    let schArr = this.unitSchList.filter((ele) => ele.wcName == id)[0].lpScheduleIdList; // get the scheduleInfo array for the selected unit
    let schObj = this.formObject.formFields.filter((ele) => ele.ref_key == 'schdID')[0]; // get the schObj from formArray
    schObj.list = schArr.map((x: any) => { // assign schedule data to dropdown list
      return {
        displayName: x,
        modelValue: x,
      };
    });
    //schObj.value = schObj.list[0].modelValue; // assign first value of array to the schId dropdown (will be displayed as 1st selected value)

  }

  public resetFilter(){
    this.jswComponent.resetForm(this.formObject);
    this.lpSchList=[];
    this.filterRetrieved = false;
    if(this.screen == 'charging'){
      this.ResetChargingDetails.emit();
    }
    if(this.screen == 'discharging'){
      this.ResetDischargingDetails.emit();
    }
    if(this.screen == 'rolling'){
      this.ResetRollingDetails.emit();
    }
    if(this.screen == 'bundleProd'){
      this.ResetBundleDetails.emit();
    }
  }

  //new
  public retrieveAllBunProd(){
    this.lpSchList = [];
    this.selectedSchedule = {};
    this.filterRetrieved = true;
   this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
   var formData = this.jswService.getFormData();
   if(formData && formData.length){
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, {wcName: null, schdID: null});
    this.setAllBundleDetails.emit(formObj);
   }
   
  }

  public retrieveLPSchedules(){
    this.lpSchList = [];
    this.selectedSchedule = {};
    this.filterRetrieved = true;
   this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
   var formData = this.jswService.getFormData();

   if(formData && formData.length){
     let formObj = this.sharedService.mapFormObjectToReqBody(formData, {wcName: null, schdID: null});
     if (this.screen == 'rolling') {
      this.setRollingDetails.emit(formObj);
     }
     if (this.screen == 'discharging') {
      this.setDischargingDetails.emit(formObj);
     }
     if (this.screen == 'bundleProd') {
      this.setBundleDetails.emit(formObj);
     }
     this.brmService.getLPSchList(formObj.schdID).subscribe(
       data => {
         data.map(((ele: any) => {
          ele.heatNo = ele.heatIdList.join(",");
          return ele;
        }));
        this.lpSchList = data;
        if (this.screen == 'charging') {
          this.brmService.setLPSch(this.lpSchList);
        }
         this.totalAttachedNos = this.getTotalNo(this.lpSchList, 'attached');
         this.totalAttachedWt = this.getTotalWt(this.lpSchList, 'attachedWt');
         this.totalChargedNos = this.getTotalNo(this.lpSchList, 'charged');
         this.totalChargedWt = this.getTotalWt(this.lpSchList, 'chargedWt');
         this.totalDischargedNos = this.getTotalNo(this.lpSchList, 'discharged');
         this.totalDischargedWt = this.getTotalWt(this.lpSchList, 'dischargedWt');
         this.totalRolledNos = this.getTotalNo(this.lpSchList, 'rolled');
         this.totalRolledWt = this.getTotalWt(this.lpSchList, 'rolledWt');
         this.totalCobbledNos = this.getTotalNo(this.lpSchList, 'cobbled');
         this.totalCobbledWt = this.getTotalWt(this.lpSchList, 'cobbledWt');
         this.totalMixedNos = this.getTotalNo(this.lpSchList, 'mixed');
         this.totalMixedWt = this.getTotalWt(this.lpSchList, 'mixedWt');
         this.totalHotoutNos = this.getTotalNo(this.lpSchList, 'hotout');
         this.totalHotoutWt = this.getTotalWt(this.lpSchList, 'hotoutWt');
         this.totalBundleNos = this.getTotalNo(this.lpSchList, 'bundle');
         this.totalBundleWt = this.getTotalWt(this.lpSchList, 'bundleWt');
       }
     )
   }
  }

  public getTotalNo(data: any, field: string){
    let initialValue = 0; let totalNos = 0;
    if(data && data.length > 0){
      totalNos = data.reduce((total: any, item: any) => {
          return Number(total) + Number(item[field])}, initialValue);
    }
    return totalNos;
  }

  public getTotalWt(data: any, field: string){
    let initialValue = 0; let totalWt = 0;
    if(data && data.length > 0){
      totalWt = data.reduce((total: any, item: any) => {
          return Number(total) + Number(item[field])}, initialValue).toFixed(2);
    }
    return totalWt;
  }

  onRowSelect(event) {
    if(this.screen == 'charging'){
      this.setChargingDetails.emit(event.data);
    }
  }

  onRowUnselect(event) {

  }

  public numSequence(n: number): Array<number> {
    return Array(n);
  }

  public ShowDialog()
  {
    this.displayPopup = true;
  }

  public closeSchedules(){
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
   var formData = this.jswService.getFormData();

   if(formData && formData.length){
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, {wcName: null, schdID: null});
    this.setCloseSchDetails.emit(formObj);
  }
  }
}
