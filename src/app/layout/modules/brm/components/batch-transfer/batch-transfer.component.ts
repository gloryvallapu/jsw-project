import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { BatchTransferForms} from '../../form-objects/brm-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { BrmService } from '../../services/brm.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Subject } from 'rxjs';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-batch-transfer',
  templateUrl: './batch-transfer.component.html',
  styleUrls: ['./batch-transfer.component.css']
})
export class BatchTransferComponent implements OnInit {
  public formObject: any;
  public formTransferTo:any;

  public viewType = ComponentType;
  public columns: any=[];

  public selectedRecords:any =[]
  public data: any;
  public tableType: any;
  public columnType = ColumnType;
  public unitList: any = [];
  public requestBody:any;
  public batchList: any;
  public selectedUnit: any;
  public selectedYard: any;
  public yardList: any;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public screenStructure: any = [];
  public isRetriveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public istransferBtn: boolean = false;
  public isBatchDetailsSection:boolean=false;
  public isfilterSection:boolean=false;
  public menuConstants = menuConstants;
  public formRef: NgForm;

  constructor(public batchTransferForms : BatchTransferForms,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public brmService: BrmService,
    public commonService: CommonApiService,
    public authService: AuthService) {
      this.columns = [
        {
          field: 'checkbox',
          header: '',
          columnType: ColumnType.checkbox,
          width: '50px',
          sortFieldName: '',
        },
        {
          field: 'batchId',
          header: 'Batch ID',
          columnType: ColumnType.string,
          width: '100px',
          sortFieldName: '',
        },
        {
          field: 'orderId',
          header: 'Sales Order',
          columnType: ColumnType.string,
          width: '100px',
          sortFieldName: '',
        },
        {
          field: 'heatId',
          header: 'Heat ID',
          columnType: ColumnType.string,
          width: '80px',
          sortFieldName: '',
        },
        {
          field: 'jswGrade',
          header: 'Grade',
          columnType: ColumnType.string,
          width: '80px',
          sortFieldName: '',
        },
        {
          field: 'batchStatusDesc',
          header: 'Status',
          columnType: ColumnType.string,
          width: '100px',
          sortFieldName: '',
        },
        {
          field: 'shape',
          header: 'Product Type',
          columnType: ColumnType.string,
          width: '140px',
          sortFieldName: '',
        },
        {
          field: 'sizeCode',
          header: 'Product Size',
          columnType: ColumnType.string,
          width: '140px',
          sortFieldName: '',
        },
        {
          field: 'length',
          header: 'Length(mm)',
          columnType: ColumnType.string,
          width: '120px',
          sortFieldName: '',
        },
        {
          field: 'weight',
          header: 'Weight(tons)',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        },
        {
          field: 'yardName',
          header: 'Current Location',
          columnType: ColumnType.string,
          width: '140px',
          sortFieldName: '',
        },
        {
          field: 'nextWc',
          header: 'Next Unit',
          columnType: ColumnType.string,
          width: '100px',
          sortFieldName: '',
        },
        {
          field: 'productionRemark',
          header: 'Production Remark',
          columnType: ColumnType.string,
          width: '150px',
          sortFieldName: '',
        }
      ];
    }

    ngOnInit(): void {
      let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.SMS_Operation)[0];
      this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.smsOperationScreenIds.Batch_transfer.id)[0];
      this.isRetriveBtn = this.sharedService.isButtonVisible(true, menuConstants.BatchTransferSectionIds.filter_Criteria.buttons.retrieve, menuConstants.BatchTransferSectionIds.filter_Criteria.id, this.screenStructure);
      this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.BatchTransferSectionIds.filter_Criteria.buttons.reset, menuConstants.BatchTransferSectionIds.filter_Criteria.id, this.screenStructure);
      this.istransferBtn = this.sharedService.isButtonVisible(true, menuConstants.BatchTransferSectionIds.Batch_List.buttons.transfer, menuConstants.BatchTransferSectionIds.Batch_List.id, this.screenStructure);
      this.isfilterSection=this.sharedService.isSectionVisible(menuConstants.BatchTransferSectionIds.filter_Criteria.id,this.screenStructure);
      this.isBatchDetailsSection=this.sharedService.isSectionVisible(menuConstants.BatchTransferSectionIds.Batch_List.id,this.screenStructure);
      this.tableType = TableType.gridlines + ' ' + TableType.normal;
      this.formObject = JSON.parse(JSON.stringify(this.batchTransferForms.batchTransfer));
      this.formTransferTo =JSON.parse(JSON.stringify(this.batchTransferForms.batchTransferTo));
      // this.getWorkCenterList();
      this.getFilterList();
    }

    public resetFilter(){
      this.jswComponent.resetForm(this.formTransferTo);
      this.jswComponent.resetForm(this.formObject);
      //this.getBatchTransferList();
      this.selectedYard=null;
      this.clearSelection.next(true);
      this.selectedRecords = [];
      this.batchList=[];
    }

    public selectedBatchRecord(selectedData)
    {
      this.selectedRecords =selectedData;
    }

    public wcDropdownChangeEvent(event){
      if (event.ref_key == 'wcName') { // unit dropdown change event
        this.selectedUnit= event.value;
        let wclist = this.formObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list
        let heatList = wclist.filter((x:any) => x.modelValue == event.value)[0].heatList.map((y:any)=>{
          return { displayName: y, modelValue: y };
        })
        this.formObject.formFields.filter((obj: any) => obj.ref_key == 'heatNo')[0].list = heatList
      }
    }

    public heatIdTextBoxEvent(formObj){
      if (formObj.formFields[1].ref_key == "heatNo") { // unit textbox change event
        if(formObj.formFields[1].isError)
        {
          formObj.formFields[1].value=''
        }
        formObj.formFields[1].isError=false
      }
    }

    public yardDropdownChangeEvent(event){
      if (event.ref_key == 'yardName') { // yard name dropdown change event
        this.selectedYard= event.value;
      }
    }


    public getFilterList(){
      this.commonService.getFilterList(this.sharedService.loggedInUserDetails.userId).subscribe(Response =>{
        this.formObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list = Response.map((x:any)=>{
          return { displayName: x.yardName, modelValue: x.yardCode ,heatList:x.headIdList}
        })
      })

    }

    public getWorkCenterList()
    {
      this.commonService.getWorkCenterListByUserId().subscribe((data) => {
        this.unitList = data;
        this.formObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list = this.unitList.map((x: any) => {
          return { displayName: x.wcName, modelValue: x.wcName };
        });
      })
    }

    public getYardList(wcName)
    {
      this.commonService.getYardList(wcName).subscribe(data => {
        this.yardList = data;
        this.formTransferTo.formFields.filter((obj: any) => obj.ref_key == 'yardName')[0].list = this.yardList.map((x: any) => {
          return { displayName: x.yardName, modelValue: x.inTransitId };
        });
      })
    }

    public getBatchTransferList() {
      this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
      var formData = this.jswService.getFormData();
      let wcName = this.formObject.formFields.filter(
        (ele: any) => ele.ref_key == 'wcName'
      )[0].value;
      let heatNo = this.formObject.formFields.filter(
        (ele: any) => ele.ref_key == 'heatNo'
      )[0].value;
      if (wcName != null) {
        this.commonService.getBatchTransferList(wcName, heatNo ? heatNo : 0).subscribe(Response => {
          Response.map((rowData: any) => {
            return rowData;
          })
          //added to show error msg for inactive yards
          if(Response.length){
            if(Response[0].batchTransferMsg != null){
              this.sharedService.displayToastrMessage(
                this.sharedService.toastType.Warning, {
                  message: Response[0].batchTransferMsg,
                }
              );
            }

          }
          this.batchList = Response
        })
      }
      if (wcName != null)
      {
        this.getYardList(wcName);
      }
    }

    public batchTransfer():any{
      if(this.selectedRecords.length == 0){
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning, {
            message: `Please select the batch to transfer`,
          }
        );
      }
      else  if(this.selectedYard == null || this.selectedYard == ""){
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning, {
            message: `Please select the yard location`,
          }
        );
      }
      else{
        var requestBody = this.selectedRecords.map((x:any)=>{
          return {
            "batchId": x.batchId,
            "sizeCode":x.sizeCode,
            "shape": x.shape,
            "yardName":x.yardName,
            "batchStatusDesc": x.batchStatusDesc,
            "heatId": x.heatId,
            "prevLoc": x.yardName,
            "jswGrade": x.jswGrade,
            "orderId": x.orderId,
            "nextWc": x.nextWc,
            "thickness": x.thickness,
            "width": x.width,
            "weight": x.weight,
            "productionRemark": x.productionRemark,
            "storageLoc": x.storageLoc
          }
        })

        this.commonService.batchTransfer(this.selectedYard,this.sharedService.loggedInUserDetails.userId, requestBody).subscribe(Response => {
          // if(Response.length){
          //   Response.forEach(element => {
          //     let messageShow = element.batchId.concat(element.batchTransferMsg)
          //     this.sharedService.displayToastrMessage(
          //       this.sharedService.toastType.Success, {
          //       message: messageShow,
          //     }
          //     );
          //   });
          // }
          this.sharedService.displayToastrMessage(
            this.sharedService.toastType.Success, {
            message: 'Batch Transfered Successfully',
          }
          );
          this.getBatchTransferList();
          // this.jswComponent.resetForm(this.formTransferTo);
          // this.jswComponent.resetForm(this.formObject);
          this.sharedService.resetForm(this.formTransferTo,this.formRef);
          this.sharedService.resetForm(this.formObject,this.formRef);
          this.selectedYard=null
          this.selectedRecords = [];
          this.clearSelection.next(true);
        })
      }
    }
    public getFormRef(event){
      this.formRef = event;
    }
  }
