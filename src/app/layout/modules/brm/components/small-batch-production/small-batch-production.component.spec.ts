import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SmallBatchProductionForm} from '../../form-objects/sms-form-objects';
import { ColumnType, LovCodes, TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { SmsService } from '../../services/sms.service';
import {
  JswCoreService,
  JswFormComponent,
} from 'projects/jsw-core/src/public-api';
import { Table } from 'primeng/table';
import { SmallBatchProductionComponent } from './small-batch-production.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { SmsEndPoints } from '../../form-objects/sms-api-endpoint';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { of } from 'rxjs';

describe('SmallBatchProductionComponent', () => {
  let component: SmallBatchProductionComponent;
  let fixture: ComponentFixture<SmallBatchProductionComponent>;
  let smallBatchProductionForm: SmallBatchProductionForm;
    let sharedService;
    let smsService: SmsService;
    let jswService: JswCoreService;
    let jswComponent: JswFormComponent
    let formObjects : SmallBatchProductionForm
    const wCresp = [{
      wcName : "LP1",
      lpScheduleIdList:[305, 489, 508, 384]
    }]

    const toastrService = {
      success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
      error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
    };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SmallBatchProductionComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        SmsService,
        CommonApiService,
        JswFormComponent,
        SmsEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        SmallBatchProductionForm,
        { provide: ToastrService, useValue: toastrService },
 ]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallBatchProductionComponent);
    component = fixture.componentInstance;
    smsService = fixture.debugElement.injector.get(SmsService);
    sharedService = fixture.debugElement.injector.get(SharedService);
    formObjects = fixture.debugElement.injector.get(SmallBatchProductionForm);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get Work Center List',() => {
    smsService.smallBatchProdFilter();
    spyOn(smsService, 'smallBatchProdFilter').and.returnValues(of(wCresp));
     component.smallBatchProdFilter();
     expect(component.formObject.formFields.filter((x: any) => x.ref_key == 'wcName')[0].list.length).toBeGreaterThan(0);
   });

});
