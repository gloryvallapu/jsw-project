import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { TableType, ColumnType, LOV } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { Subject } from 'rxjs';
import { BrmService } from '../../services/brm.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-discharging-screen',
  templateUrl: './discharging-screen.component.html',
  styleUrls: ['./discharging-screen.component.css'],
})
export class DischargingScreenComponent implements OnInit {
  public refreshShiftDetails: Subject<boolean> = new Subject<boolean>();
  public tableType: any;
  public globalFilterArray: any = [];
  public viewType = ComponentType;
  public selectedRecord: any;
  public batchColumns: ITableHeader[] = [];
  public batchList: any = [];
  public scheduleDetails: any;
  public chargingSequence: any = [];
  public screenStructure: any = [];
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isSaveBtn: boolean = false;
  public isfilterCriteriaScreen:boolean=false;
  public isPlan_shiftScreen:boolean=false;
  public isdisChargingdetailsScreen:boolean=false;
  public menuConstants = menuConstants;
  public isDischargeScreen: boolean=false;
  public chargediv: string='content-charging';
  public tdClass ="height-max-content pt-0 pb-0";
  constructor(
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public brmService: BrmService,
    public jswComponent: JswFormComponent,
    public authService: AuthService
  ) {
    this.batchColumns = [
      {
        field: 'radio',
        header: '',
        columnType: ColumnType.radio,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch Id',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'salesOrderNo',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'billetSeqNo',
        header: 'Batch Seq No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'product',
        header: 'Prod Type',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'batchSize',
        header: 'Batch Size',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'batchGrade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchWt',
        header: 'Batch Wgt(tons)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'batchStatus',
        header: 'Discharge Status',
        columnType: ColumnType.dropdown,
        width: '130px',
        sortFieldName: '',
        list: [],
        disableCol: false
      },
      {
        field: 'limeCoatFlag',
        header: 'Lime Coat',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
        list: [],
        disableCol: false
      },
      {
        field: 'orderGrade',
        header: 'SO Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'orderDia',
        header: 'Ord Diameter(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'dummyBilletYN',
        header: 'Dummy Batch',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      
      {
        field: 'chargeRemark',
        header: 'Charge Remark',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'hotoutRemarks',
        header: 'Discharge Remark',
        columnType: ColumnType.textField,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'vendorHeatId',
        header: 'Purchased Heat No',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'customer',
        header: 'Customer Name',
        columnType: ColumnType.ellipsis,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'scheduleId',
        header: 'Schd ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
    ];
  }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.BRMScreenIds.DisCharging_Screen.id)[0];
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.DisChargingScreenSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.DisChargingScreenSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.DisChargingScreenSectionIds.Filter_Criteria.buttons.reset, menuConstants.DisChargingScreenSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.DisChargingScreenSectionIds.DisCharging_List.buttons.save, menuConstants.DisChargingScreenSectionIds.DisCharging_List.id, this.screenStructure);
    this.isfilterCriteriaScreen=this.sharedService.isSectionVisible(menuConstants.DisChargingScreenSectionIds.Filter_Criteria.id,this.screenStructure);
    this.isPlan_shiftScreen=this.sharedService.isSectionVisible(menuConstants.DisChargingScreenSectionIds.PlantandShiftwise_List.id,this.screenStructure);
    this.isdisChargingdetailsScreen=this.sharedService.isSectionVisible(menuConstants.DisChargingScreenSectionIds.DisCharging_List.id,this.screenStructure);
    this.isDischargeScreen=this.sharedService.isSectionVisible(menuConstants.DisChargingScreenSectionIds.Discharge.id,this.screenStructure);
    if(this.isDischargeScreen==true)
    {
      this.chargediv='content-charging'
    }
    else
    {
      this.chargediv='content'
    }
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getChargingSeq();
    this.getDischargeBatchStatus();
  }

  public getDischargeBatchStatus(){
    this.brmService.getBatchStatus().subscribe(data => {
      this.batchColumns.filter(col => col.field == 'batchStatus')[0].list = this.sharedService.getDropdownData(
        data,
        'valueDescription',
        'lovValueId'
      );
    })
  }

  public setDischargingDetails(data) {
    this.getDischargingDetails(data);
    this.scheduleDetails = data;
  }

  public getDischargingDetails(data) {
    this.batchList.length = 0;
    this.brmService.getDischargingDetails(data.schdID).subscribe((Response) => {
      let statusArr = this.batchColumns.filter(col => col.field == 'batchStatus')[0].list;
      Response.map((rowData:any) => {
          rowData.batchStatus = rowData.dummyBilletYN == 'Y' ? statusArr.filter(ele => ele.modelValue == 52)[0].modelValue : statusArr.filter(ele => ele.modelValue == 53)[0].modelValue;
          return rowData;
      })
      this.batchList = Response.map((x:any)=>{
        x.disabled = true;
        return x
      });
      this.batchList[0].disabled = false;
    });
  }

  public getChargingSeq(){
    this.brmService.getChargingSequence().subscribe(
      data => {
        this.chargingSequence = data;
      }
    )
  }

  public updateToDischarge() {
    this.brmService
      .dischargeBatch(
        this.sharedService.loggedInUserDetails.userId,
        this.selectedRecord
      )
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: Response});
        this.selectedRecord = null;
        this.getDischargingDetails(this.scheduleDetails);
        this.refreshShiftData(true);
        this.getChargingSeq();
      });
  }

  public selectedBatchRecord(event) {
    this.selectedRecord = event;
  }

  public refreshShiftData(refreshData) {
    this.refreshShiftDetails.next(refreshData);
  }

  public dropdownChangeEvent(data){
    //confirm with Ankur if change remark is mandatory
  }
  public ResetFilter()
  {
    this.batchList=[];
  }
}
