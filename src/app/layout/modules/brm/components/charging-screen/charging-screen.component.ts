import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { TableType, ColumnType } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ConfirmationService } from 'primeng/api';
import { BrmService } from '../../services/brm.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { Subject, Subscription } from 'rxjs';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';
@Component({
  selector: 'app-charging-screen',
  templateUrl: './charging-screen.component.html',
  styleUrls: ['./charging-screen.component.css'],
})

export class ChargingScreenComponent implements OnInit {
  public batchColumns: ITableHeader[] = [];
  public dummyBatchColumns: ITableHeader[] = [];
  public batchList: any = [];
  public dummyBatchList: any = [];
  public tableType: any;
  public viewType = ComponentType;
  public columnType = ColumnType;
  public selectedBatch: any;
  public selectedDummyBatch: any;
  public chargingSequence: any = [];
  public selectedSchedule: any;
  public refreshShiftDetails: Subject<boolean> = new Subject<boolean>();
  recordToBeEdited: any;
  public screenStructure: any = [];
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isChargeBtn: boolean = false;
  public isDeleteBtn: boolean = false;
  public isdummyAddBtn: boolean = false;
  public isfilterCriteriaScreen:boolean=false;
  public isPlan_shiftScreen:boolean=false;
  public isChargingdetailsScreen:boolean=false;
  public isDummydetailsScreen:boolean=false;
  public isChargeScreen:boolean=false;
  public menuConstants = menuConstants;
  public chargediv:string='content-charging';
  public lpSchList: any = [];
  private eventSubscription: Subscription;
  public currentSOForCharging: any;
  public tdClass ="height-max-content pt-0 pb-0";

  constructor(
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    private confirmationService: ConfirmationService,
    public brmService: BrmService,
    public authService: AuthService
  ) {
    this.batchColumns = [
      {
        field: 'radio',
        header: '',
        columnType: ColumnType.radio,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'product',
        header: 'Prod Type',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchSize',
        header: 'Batch Size',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchGrade',
        header: 'Batch Grade',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'batchWt',
        header: 'Batch Wt(tons)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'chargeRemark',
        header: 'Charge Remark',
        columnType: ColumnType.textField,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'limeCoatFlag',
        header: 'Lime Coat Flag',
        columnType: ColumnType.dropdown,
        width: '130px',
        sortFieldName: '',
        list: [{
          modelValue: 'N',
          displayName: 'No'
        },
        {
          modelValue: 'Y',
          displayName: 'Yes'
        }]
      },
      {
        field: 'salesOrderNo',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'vendorHeatId',
        header: 'Pur Heat No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'scheduleOrderSeqNo', // confirmation is pending from Ankur
        header: 'Schd Seq No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      
      {
        field: 'orderGrade',
        header: 'Prod Grade',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'soLength',
        header: 'Ord Len(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Len(mm)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      
      {
        field: 'customer',
        header: 'Customer Name',
        columnType: ColumnType.ellipsis,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'scheduleId',
        header: 'Schedule ID',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      }
    ];
    this.dummyBatchColumns = [
      {
        field: 'radio',
        header: '',
        columnType: ColumnType.radio,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat No',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'batchSize',
        header: 'Batch Size',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'batchGrade',
        header: 'Batch Grade',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'product',
        header: 'Product Type',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      }
    ];
  }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.BRMScreenIds.Charging_Screen.id)[0];
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.ChargingScreenSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.ChargingScreenSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.ChargingScreenSectionIds.Filter_Criteria.buttons.reset, menuConstants.ChargingScreenSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isChargeBtn = this.sharedService.isButtonVisible(true, menuConstants.ChargingScreenSectionIds.Charging_List.buttons.charge, menuConstants.ChargingScreenSectionIds.Charging_List.id, this.screenStructure);
    this.isDeleteBtn = this.sharedService.isButtonVisible(true, menuConstants.ChargingScreenSectionIds.Charging_List.buttons.delete, menuConstants.ChargingScreenSectionIds.Charging_List.id, this.screenStructure);
    this.isdummyAddBtn = this.sharedService.isButtonVisible(true, menuConstants.ChargingScreenSectionIds.dummyBatch_List.buttons.add, menuConstants.ChargingScreenSectionIds.dummyBatch_List.id, this.screenStructure);
    this.isfilterCriteriaScreen = this.sharedService.isSectionVisible(menuConstants.ChargingScreenSectionIds.Filter_Criteria.id,this.screenStructure);
    this.isPlan_shiftScreen = this.sharedService.isSectionVisible(menuConstants.ChargingScreenSectionIds.PlantandShiftwise_List.id,this.screenStructure);
    this.isChargingdetailsScreen = this.sharedService.isSectionVisible(menuConstants.ChargingScreenSectionIds.Charging_List.id,this.screenStructure);
    this.isDummydetailsScreen = this.sharedService.isSectionVisible(menuConstants.ChargingScreenSectionIds.dummyBatch_List.id,this.screenStructure);
    this.isChargeScreen = this.sharedService.isSectionVisible(menuConstants.ChargingScreenSectionIds.Charge.id,this.screenStructure);
    if(this.isChargeScreen == true)
    {
      this.chargediv='content-charging'
    }
    else
    {
      this.chargediv='content'
    }
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getDummyBatches();
    this.getChargingSeq();
    this.eventSubscription = this.brmService.getlpSch.subscribe( data => {
      this.lpSchList = JSON.parse(JSON.stringify(data.map((ele: any) => {
        // var curDate = new Date();
        // curDate.setMinutes(curDate.getMinutes() +  Math.floor(Math.random() * 10)); // timestamp
        // curDate = new Date(curDate); // Date object
        ele.latestChargeDt = ele.latestChargeDt ? new Date(ele.latestChargeDt) : null; //curDate;
        return ele;
      })))
      let sortedData = this.sortData(this.lpSchList, 'latestChargeDt');
      let salesOrderNo;
      if(sortedData.filter(ele => ele.latestChargeDt == null).length == sortedData.length){//charging has not started yet
        salesOrderNo = sortedData && sortedData.length ? sortedData[0].salesOrderNo : null;
      }else{
        salesOrderNo = sortedData && sortedData.length ? sortedData[sortedData.length - 1].salesOrderNo : null;
      }
      this.currentSOForCharging = this.lpSchList.filter(ele => ele.salesOrderNo == salesOrderNo)[0];
    })
  }

  public getTime(date?: Date) {
    return date != null ? date.getTime() : 0;
  }


  public sortData(data, sortBy){
    if(data && data.length){
      data.sort((a, b) => {
        return ((a ? this.getTime(new Date(a[sortBy])) : 0) - (b ? this.getTime(new Date(b[sortBy])) : 0));
      });
    }
    return data;
  }

  ngOnDestroy(){
    this.eventSubscription.unsubscribe();
  }

  public getChargingDetails(selectedSch) {
    //heatNo
    this.selectedSchedule = selectedSch;
    this.batchList.length = 0;
    this.selectedBatch = null;
    this.brmService.getChargingDetails(selectedSch.scheduleId, selectedSch.scheduleOrderSeqNo,selectedSch.heatNo).subscribe(
      data => {
        data.map((rowData:any) => {
          let limeCoatFlagArr = this.batchColumns.filter(col => col.field == 'limeCoatFlag')[0].list;
          rowData.limeCoatFlag = limeCoatFlagArr.filter(ele => ele.modelValue == 'N')[0].modelValue;
          return rowData;
        })
        this.batchList = data;
      }
    )
  }

  public getChargingSeq(){
    this.brmService.getChargingSequence().subscribe(
      data => {
        this.chargingSequence = data;
      }
    )
  }

  public getDummyBatches(){
    this.brmService.getDummyBatches().subscribe(
      data => {
        this.dummyBatchList = data;
      }
    )
  }

  public chargeBatch(){
    if(this.lpSchList.filter(ele => ele.latestChargeDt == null).length == this.lpSchList.length || this.selectedBatch.salesOrderNo == this.currentSOForCharging.salesOrderNo ||
      (this.selectedBatch.salesOrderNo != this.currentSOForCharging.salesOrderNo && this.currentSOForCharging.charged == this.currentSOForCharging.attached)){
        // going to charge first batch in the schedule OR continuing charging of batches from same Sales Order
        // OR charging of one of the sales order is completed and starting the charging from another SO
      this.proceedForCharging();
    }else if(this.selectedBatch.salesOrderNo != this.currentSOForCharging.salesOrderNo && this.currentSOForCharging.attached > 0 && this.currentSOForCharging.charged < this.currentSOForCharging.attached){
      this.confirmationService.confirm({
        message: `<span class="font-16 text-grey">All the batches are not charged from Sales Order - ${this.currentSOForCharging.salesOrderNo}, do you still want to proceed?</span>`,
        header: 'Charge Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
          this.proceedForCharging();
        },
        reject: () => {
          //this.selectedBatch = null;
        },
      });
    }
  }

  public proceedForCharging(){
    this.brmService.startCharging(this.sharedService.loggedInUserDetails.userId, this.selectedBatch).subscribe(
      data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success,
            { message: `Batch ${this.selectedBatch.batchId} from Sales Order ${this.selectedBatch.salesOrderNo} is sent for charging.`});
        this.getChargingDetails(this.selectedSchedule);
        this.getChargingSeq();
        this.refreshShiftData(true);
      }
    )
  }

  public addDummyBatch(){
    if(!this.selectedSchedule){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please select schedule from LP Schedule Plan Details table'})
    }
    this.selectedDummyBatch.scheduleId = this.selectedSchedule.scheduleId;
    this.selectedDummyBatch.scheduleOrderSeqNo = this.selectedSchedule.scheduleOrderSeqNo;
    this.brmService.addDummyBatch(this.sharedService.loggedInUserDetails.userId, this.selectedDummyBatch).subscribe(
      data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: data});
        this.getDummyBatches();
        this.getChargingSeq();
        this.refreshShiftData(true);
        //call API to refresh shift details
      }
    )
  }

  public deleteBatch() {
    this.confirmationService.confirm({
      message: '<span class="font-16 text-grey">Do you want to delete - ' + this.selectedBatch.batchId + ' batch ?</span>',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.deleteSelectedBatch();
      },
      reject: () => {
        this.selectedBatch = null;
      },
    });
  }

  public deleteSelectedBatch(){
    this.brmService.deleteBatch(this.sharedService.loggedInUserDetails.userId, this.selectedBatch).subscribe(
      data => {
        this.getChargingDetails(this.selectedSchedule);
        this.getChargingSeq();
        this.refreshShiftData(true);
      }
    )
  }

  public deleteDummyBatch() {
    this.confirmationService.confirm({
      message:
        '<span class="font-16 text-grey">Do you want to delete selected record?</span>',
      header: 'Delete Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        // this.deleteMultipleEvent.emit(data);
      },
      reject: () => {
        // this.selectedRecords.length = 0;
      },
    });
  }

  public selectedBatchRecord(rowData) {
    this.selectedBatch = rowData;
  }

  public selectedDummyBatchRecord(rowData) {
    this.selectedDummyBatch = rowData;
  }

  public refreshShiftData(refreshData){
    this.refreshShiftDetails.next(refreshData);
  }

  ResetFilter()
  {
  this.batchList=[];
  }
}
