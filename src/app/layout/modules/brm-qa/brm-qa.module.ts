import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrmQaComponent } from './brm-qa.component';
import { TpiHoldReleaseComponent } from './components/tpi-hold-release/tpi-hold-release.component';
import { StickerPrintingComponent } from './components/sticker-printing/sticker-printing.component';
import { SampleMechPropertyComponent } from './components/sample-mech-property/sample-mech-property.component';
import { BundleQualityClearanceComponent } from './components/bundle-quality-clearance/bundle-quality-clearance.component';
import { FinalMaterialBatchUpdateComponent } from './components/final-material-batch-update/final-material-batch-update.component';
import { BrmQaService } from './services/brm-qa.service';
import { BRMqaRoutingModule } from './brm-qa-routing.module';
import { BrmqaEndPoints } from './form-objects/brm-qa-api-endpoints';
import { TpiHoldReleaseForm, NDTForms } from './form-objects/brm-qa-form-objects';
import { FinalMaterialBatchForms } from './form-objects/brm-qa-form-objects';
import { SampleMechProForms } from './form-objects/brm-qa-form-objects';
import { NdtComponent } from './components/ndt/ndt.component';
import { NgxPrintModule } from 'ngx-print';

@NgModule({
  declarations: [
    BrmQaComponent,
    TpiHoldReleaseComponent,
    StickerPrintingComponent,
    SampleMechPropertyComponent,
    BundleQualityClearanceComponent,
    FinalMaterialBatchUpdateComponent,
    NdtComponent
  ],
  imports: [
    BRMqaRoutingModule,
    CommonModule,
    SharedModule,
    NgxPrintModule,
  ],
  providers: [BrmQaService, BrmqaEndPoints, FinalMaterialBatchForms, TpiHoldReleaseForm, SampleMechProForms, NDTForms]
})
export class BrmQaModule { }
