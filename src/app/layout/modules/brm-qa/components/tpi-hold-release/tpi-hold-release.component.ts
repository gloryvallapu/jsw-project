import {
  Component,
  OnInit
} from '@angular/core';
import {
  ColumnType,
  ComponentType
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  TableType
} from 'src/assets/enums/common-enum';
import {
  ITableHeader
} from 'src/assets/interfaces/table-headers';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  BrmQaService
} from '../../services/brm-qa.service';
import {
  CommonApiService
} from 'src/app/shared/services/common-api.service';
import {
  TpiHoldReleaseForm
} from '../../form-objects/brm-qa-form-objects';
import {
  JswCoreService,
  JswFormComponent
} from 'jsw-core';
import {
  Subject
} from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';


@Component({
  selector: 'app-tpi-hold-release',
  templateUrl: './tpi-hold-release.component.html',
  styleUrls: ['./tpi-hold-release.component.css']
})
export class TpiHoldReleaseComponent implements OnInit {
  public scheduleId: any;
  public batchDetails: any = [];
  public viewType = ComponentType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public selectedBatches: any = [];
  public productDetails: any = [];
  public filterCriteriaFrom: any = [];
  public tpiCommonDetails: any = [];
  public clearSelection: Subject < boolean > = new Subject < boolean > ();
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isFlCrViewOnly:boolean= false;
  public isFlCrRtrv:boolean= false;
  public isFlCrRst:boolean = false;
  public isBatchDetails:boolean =false;
  public isSave:boolean = false;
  public isRelease:boolean= false;
  constructor(
    public authService: AuthService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent, public tpiHoldReleaseForm: TpiHoldReleaseForm, public brmQaService: BrmQaService, public sharedService: SharedService, public commonApiService: CommonApiService) {}

    public screenRightsCheck(){
      let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM_QA)[0];
      this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.brmQAScreenIds.TPI_HOLD_RELEASE_SCREEN.id)[0];
      this.isFlCrViewOnly = this.sharedService.isSectionVisible(menuConstants.TPI_HOLD_RELEASE_SCREEN_SectionIds.Filter_Criteria.id, this.screenStructure);
      this.isFlCrRtrv = this.sharedService.isButtonVisible(true, menuConstants.TPI_HOLD_RELEASE_SCREEN_SectionIds.Filter_Criteria.buttons.retrive, menuConstants.TPI_HOLD_RELEASE_SCREEN_SectionIds.Filter_Criteria.id, this.screenStructure);
      this.isFlCrRst = this.sharedService.isButtonVisible(true, menuConstants.TPI_HOLD_RELEASE_SCREEN_SectionIds.Filter_Criteria.buttons.reset, menuConstants.TPI_HOLD_RELEASE_SCREEN_SectionIds.Filter_Criteria.id, this.screenStructure);
      this.isBatchDetails = this.sharedService.isSectionVisible(menuConstants.TPI_HOLD_RELEASE_SCREEN_SectionIds.List.id, this.screenStructure);
      this.isSave = this.sharedService.isButtonVisible(true, menuConstants.TPI_HOLD_RELEASE_SCREEN_SectionIds.List.buttons.save, menuConstants.TPI_HOLD_RELEASE_SCREEN_SectionIds.List.id, this.screenStructure);
      this.isRelease = this.sharedService.isButtonVisible(true, menuConstants.TPI_HOLD_RELEASE_SCREEN_SectionIds.List.buttons.release, menuConstants.TPI_HOLD_RELEASE_SCREEN_SectionIds.List.id, this.screenStructure);
  }
  
    ngOnInit(): void {
      this.screenRightsCheck(); 
    this.tpiCommonDetails = JSON.parse(JSON.stringify(this.tpiHoldReleaseForm.tpiDetails));
    this.filterCriteriaFrom =JSON.parse(JSON.stringify( this.tpiHoldReleaseForm.filterCriteria));
    this.filterCriteria();
    this.getProductSizeList()
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.brmQaService.gettpiSelectedRecords.subscribe(Response => {
      this.selectedBatches = Response;
    })
    this.columns = [{
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'saledOrderId',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID ',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: 'heatId',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: 'heatId',
      },
      {
        field: 'product',
        header: 'Type',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'size',
        header: 'Prod Size',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      
      {
        field: 'agency',
        header: 'TP Agency ',
        columnType: ColumnType.textField,
        width: '120px',
        sortFieldName: '',
        errorMsg: "Required",
      },
      {
        field: 'agentName',
        header: 'Agent Name',
        columnType: ColumnType.textField,
        width: '150px',
        sortFieldName: '',
        errorMsg: "Required"
      },
      {
        field: 'approvalDate',
        header: 'Approval Date',
        columnType: ColumnType.calender,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'approvalRemark',
        header: 'Approval Remark ',
        columnType: ColumnType.textField,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Len(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Wgt(tons)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.ellipsis,
        width: '150px',
        sortFieldName: '',
      }
    ];
  };

  public reset() {
    this.scheduleId = undefined;
    this.selectedBatches = [];
    this.batchDetails = [];
    this.brmQaService.settpiSelectedRecords([])
    this.jswComponent.resetForm(this.filterCriteriaFrom);
  };

  public getProductSizeList() {
    this.commonApiService.getProductSizeList().subscribe(Response => {
      this.productDetails = Response.map((x: any) => {
        return {
          displayName: x.productName,
          modelValue: x.productId
        }
      })
    })
  };

  public filterCriteria() {
    this.brmQaService.filterCriteria().subscribe(Response => {
      this.filterCriteriaFrom.formFields.filter((x: any) => x.ref_key == 'wcName')[0].list = Response.map((y: any) => {
        return {
          displayName: y.wcName,
          modelValue: y.wcName,
          scheduleList: y.lpScheduleIdList
        }
      })
    })
  };

  public dropDownChangeEvent(event) {
    this.batchDetails = [];
    this.brmQaService.settpiSelectedRecords([])
    if (event.value != null || event.value != '') {
      let getScheduleList = this.filterCriteriaFrom.formFields.filter((x: any) => x.ref_key == 'wcName')[0].list
      let schuleIdlist = getScheduleList.filter((x: any) => x.modelValue == event.value)[0].scheduleList
      this.filterCriteriaFrom.formFields.filter((x: any) => x.ref_key == 'scheduleID')[0].list = schuleIdlist.map((y: any) => {
        return {
          displayName: y,
          modelValue: y,
        }
      })
    }
  };

  public getTpiHoldDetails() {
    this.brmQaService.getTpiHoldDetails(this.scheduleId).subscribe(Response => {
      Response.map((x: any) => {
        x.approvalDate = x.approvalDate == null ? null : new Date(x.approvalDate)
        x.productName = this.productDetails.filter((y: any) => y.modelValue == x.materialId)[0].displayName
        return x
      })
      this.batchDetails = Response
    })
  };

  public filterData(): any {
    this.brmQaService.settpiSelectedRecords([])
    this.jswComponent.onSubmit(
      this.filterCriteriaFrom.formName,
      this.filterCriteriaFrom.formFields
    );
    var formData = this.jswService.getFormData();
    let obj = {
      "scheduleID": null,
      "wcName": null
    }
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, obj);
    this.scheduleId = formObj.scheduleID
    if (this.scheduleId == undefined || this.scheduleId == '') {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter Schedule Id `,
        }
      );
    };
    this.getTpiHoldDetails()
  };

  public getSelectedRow(event) {
    this.brmQaService.settpiSelectedRecords(event)
  }

  public saveTpiDetailsApi(formObject) {
    formObject.tpiRelaseBoList = this.selectedBatches
    this.brmQaService.save(this.sharedService.loggedInUserDetails.userId, formObject).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `${Response}`,
        }
      );
      this.brmQaService.settpiSelectedRecords([]);
      this.clearSelection.next(true);
      this.jswComponent.resetForm(this.tpiCommonDetails);
      this.filterData();
    })
  };

  public saveDetails(): any {
    this.jswComponent.onSubmit(
      this.tpiCommonDetails.formName,
      this.tpiCommonDetails.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData == undefined) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter TP Details`,
        }
      );
    };
    let obj = {
      "agency": null,
      "agentName": null,
      "approvalDate": null,
      "approvalRemark": null,
    }
    let formObj = this.sharedService.mapFormObjectToReqBody(formData, obj);
    this.selectedBatches.map((x: any) => {
      x.agency = formObj.agency
      x.agentName = formObj.agentName
      x.approvalDate = formObj.approvalDate
      x.approvalRemark = formObj.approvalRemark
      return x
    })

    if (this.selectedBatches.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Select Material Id(s) `,
        }
      );
    };
    let tpAgencyValidation = this.selectedBatches.filter((x: any) => x.agency == null || x.agency == '')
    let agentNameValidation = this.selectedBatches.filter((x: any) => x.agentName == null || x.agentName == '')
    let approvalDateValidation = this.selectedBatches.filter((x: any) => x.approvalDate == null || x.approvalDate == '')
    let approvalRemarkValidation = this.selectedBatches.filter((x: any) => x.approvalRemark == null || x.approvalRemark == '')

    if (tpAgencyValidation.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter TP Agency Details For Selected Batch(s). `,
        }
      );
    };
    if (agentNameValidation.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter Agent Name For Selected Batch(s). `,
        }
      );
    };

    if (approvalDateValidation.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter Approval Date For Selected Batch(s). `,
        }
      );
    };

    if (approvalRemarkValidation.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter Approval Remark For Selected Batch(s). `,
        }
      );
    };
    this.saveTpiDetailsApi(formObj);

  };

  public releaseTpiDetailsApi() {
    this.brmQaService.release(this.sharedService.loggedInUserDetails.userId, this.selectedBatches).subscribe(Response => {
      this.clearSelection.next(true);
      this.filterData();
      this.brmQaService.settpiSelectedRecords([])
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `${Response}`,
        }
      );
    })
  };

  public release(): any {
    if (this.selectedBatches.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Select Material Id(s) `,
        }
      );
    };
    let tpAgencyValidation = this.selectedBatches.filter((x: any) => x.agency == null || x.agency == '')
    let agentNameValidation = this.selectedBatches.filter((x: any) => x.agentName == null || x.agentName == '')
    let approvalDateValidation = this.selectedBatches.filter((x: any) => x.approvalDate == null || x.approvalDate == '')
    let approvalRemarkValidation = this.selectedBatches.filter((x: any) => x.approvalRemark == null || x.approvalRemark == '');

    if (tpAgencyValidation.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter TP Agency Details For Selected Batch(s). `,
        }
      );
    };
    if (agentNameValidation.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter Agent Name For Selected Batch(s). `,
        }
      );
    };

    if (approvalDateValidation.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter Approval Date For Selected Batch(s). `,
        }
      );
    };

    if (approvalRemarkValidation.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter Approval Remark For Selected Batch(s). `,
        }
      );
    };
    this.releaseTpiDetailsApi();
  }

}
