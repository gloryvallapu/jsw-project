import {
  ComponentFixture,
  TestBed
} from '@angular/core/testing';
import {
  TpiHoldReleaseComponent
} from './tpi-hold-release.component';
import {
  ColumnType,
  ComponentType
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  TableType
} from 'src/assets/enums/common-enum';
import {
  ITableHeader
} from 'src/assets/interfaces/table-headers';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  BrmQaService
} from '../../services/brm-qa.service';
import {
  CommonApiService
} from 'src/app/shared/services/common-api.service';
import {
  TpiHoldReleaseForm
} from '../../form-objects/brm-qa-form-objects';
import {
  JswCoreService,
  JswFormComponent
} from 'jsw-core';
import {
  Subject,
  Subscriber
} from 'rxjs';
import {
  FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import {
  HttpClientModule
} from '@angular/common/http';
import {
  SharedModule
} from 'src/app/shared/shared.module';
import {
  BrmqaEndPoints
} from '../../form-objects/brm-qa-api-endpoints';
import {
  API_Constants
} from 'src/app/shared/constants/api-constants';
import {
  CommonAPIEndPoints
} from 'src/app/shared/constants/common-api-end-points';
import {
  IndividualConfig,
  ToastrService
} from 'ngx-toastr';
import {
  BrmService
} from '../../../brm/services/brm.service';
import {
  of ,
  Observable
} from 'rxjs';

describe('TpiHoldReleaseComponent', () => {
  let component: TpiHoldReleaseComponent;
  let fixture: ComponentFixture < TpiHoldReleaseComponent > ;
  let sharedService;
  let jswServices: JswCoreService;
  let brmQaService: BrmQaService;
  let formObjects: TpiHoldReleaseForm
  const wCresp = [{
    wcName: "LP1",
    lpScheduleIdList: [305, 489, 508, 384]
  }]

  const toastrService = {
    success: (message ? : string, title ? : string, override ? : Partial < IndividualConfig > ) => {},
    error: (message ? : string, title ? : string, override ? : Partial < IndividualConfig > ) => {}
  };


  beforeEach(async () => {
    await TestBed.configureTestingModule({
        declarations: [TpiHoldReleaseComponent],
        imports: [
          FormsModule,
          ReactiveFormsModule,
          HttpClientModule,
          SharedModule
        ],
        providers: [
          JswCoreService,
          BrmQaService,
          TpiHoldReleaseForm,
          CommonApiService,
          JswFormComponent,
          BrmqaEndPoints,
          API_Constants,
          CommonAPIEndPoints,
          {
            provide: ToastrService,
            useValue: toastrService
          },
        ]
      })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TpiHoldReleaseComponent);
    component = fixture.componentInstance;
    brmQaService = fixture.debugElement.injector.get(BrmQaService);
    sharedService = fixture.debugElement.injector.get(SharedService);
    formObjects = fixture.debugElement.injector.get(TpiHoldReleaseForm);


    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get Work Center List', () => {
    brmQaService.filterCriteria();
    spyOn(brmQaService, 'filterCriteria').and.returnValues( of (wCresp));
    component.filterCriteria();
    component.filterCriteriaFrom.formFields.filter((x: any) => x.ref_key == 'wcName')[0].list
    expect(component.filterCriteriaFrom.formFields.filter((x: any) => x.ref_key == 'wcName')[0].list.length).toBeGreaterThan(0);
  });



});
