import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { Subject } from 'rxjs';
import { BrmQaService } from '../../services/brm-qa.service';
import { FinalMaterialBatchForms } from '../../form-objects/brm-qa-form-objects';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';

@Component({
  selector: 'app-final-material-batch-update',
  templateUrl: './final-material-batch-update.component.html',
  styleUrls: ['./final-material-batch-update.component.css']
})
export class FinalMaterialBatchUpdateComponent implements OnInit {
  public viewType = ComponentType;
  public columnType = ColumnType;
  public heatColumns: ITableHeader[] = [];
  public tableType: any;
  public filterFormObject: any;
  public heatDetails: any = [];
  public formObject: any;
  public columns: any = [];
  public selectedRecords: any = []
  public data: any;
  public unitList: any = [];
  public requestBody: any;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isFlCrViewOnly:boolean= false;
  public isFlCrRtrv:boolean= false;
  public isFlCrRst:boolean = false;
  public isBatchDetails:boolean =false;
  public isSave:boolean = false;
  public isRelease:boolean= false;
  constructor(
    public authService: AuthService,
    public finalMaterialBatchForms: FinalMaterialBatchForms,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public brmQaService: BrmQaService,
    public commonService: CommonApiService) {

    this.heatColumns = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'bundleId',
        header: 'Bundle ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName:'',
      },
      {
        field: 'salesOrder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'scheduleNo',
        header: 'Schd ID',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Bundle Type',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'productSize',
        header: 'Bundle Size',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'dia',
        header: 'Dia (mm)',
        columnType: ColumnType.numericValidationField,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Len (mm)',
        columnType: ColumnType.numericValidationField,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'qtyPcs',
        header: 'Qty Pcs',
        columnType: ColumnType.numericValidationField,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Wgt(tons)',
        columnType: ColumnType.numericValidationField,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'batchStatus',
        header: 'Batch Status',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'scheduleSeqNo',
        header: 'Schd Line',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'shiftId',
        header: 'Shift',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'prodDate',
        header: 'Prod Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
        dateType: 'short'
      },
      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.ellipsis,
        width: '140px',
        sortFieldName: '',
      },
    ];
  }

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM_QA)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.brmQAScreenIds.FINAL_MATERIAL_BATCH_UPDATE_SCREEN.id)[0];
    this.isFlCrRtrv = this.sharedService.isButtonVisible(true, menuConstants.FINAL_MATERIAL_BATCH_UPDATE_SCREEN_SectionIds.Filter_Criteria.buttons.retrive, menuConstants.FINAL_MATERIAL_BATCH_UPDATE_SCREEN_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRst = this.sharedService.isButtonVisible(true, menuConstants.FINAL_MATERIAL_BATCH_UPDATE_SCREEN_SectionIds.Filter_Criteria.buttons.reset, menuConstants.FINAL_MATERIAL_BATCH_UPDATE_SCREEN_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isSave = this.sharedService.isButtonVisible(true, menuConstants.FINAL_MATERIAL_BATCH_UPDATE_SCREEN_SectionIds.List.buttons.save, menuConstants.FINAL_MATERIAL_BATCH_UPDATE_SCREEN_SectionIds.List.id, this.screenStructure);
    this.isFlCrViewOnly = this.sharedService.isSectionVisible(menuConstants.FINAL_MATERIAL_BATCH_UPDATE_SCREEN_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isBatchDetails = this.sharedService.isSectionVisible(menuConstants.FINAL_MATERIAL_BATCH_UPDATE_SCREEN_SectionIds.List.id, this.screenStructure);
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.filterFormObject = JSON.parse(JSON.stringify(this.finalMaterialBatchForms.finalMaterialFilterCriteria));
    this.getWorkCenterList();
  }

  public resetFilter() {
    this.jswComponent.resetForm(this.filterFormObject);
    this.getMaterialBatchDetails();
    this.clearSelection.next(true);
    this.selectedRecords = [];
    this.heatDetails=[];
  }

  public getRowDetails(selectedData) {
    this.selectedRecords = selectedData;
  }

  public bundleIdTextBoxEvent(formObj) {
    if (formObj.formFields[1].ref_key == "bundleId") { // unit textbox change event
      if (formObj.formFields[1].isError) {
        formObj.formFields[1].value = ''
      }
      formObj.formFields[1].isError = false
    }
  }

  public getWorkCenterList() {
    this.brmQaService.getWorkCenterList().subscribe((data) => {
      this.unitList = data;
      this.filterFormObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list = this.unitList.map((x: any) => {
        return { displayName: x.wcName, modelValue: x.wcName };
      });
    })
  }

  public getMaterialBatchDetails() {
    this.jswComponent.onSubmit(this.filterFormObject.formName, this.filterFormObject.formFields);
    var formData = this.jswService.getFormData();
    let wcName = this.filterFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'wcName'
    )[0].value;
    let bundleId = this.filterFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'bundleId'
    )[0].value;
    if (wcName != null && bundleId == null) {
      this.brmQaService.getMaterialBatchByUnit(wcName).subscribe(Response => {
        Response.map((rowData: any) => {
          return rowData;
        })
        this.heatDetails = Response
      })
    }
    else if (wcName != null && bundleId != null) {
      this.brmQaService.getMaterialBatchByUnitAndBundId(wcName, bundleId).subscribe(Response => {
        Response.map((rowData: any) => {
          return rowData;
        })
        this.heatDetails = Response
      })
    }
  }


  public fieldValidation(event): any {

    var index = 0
      if (!event.rowData.dia && event.rowData.dia!=0) {
        let colObj = this.heatColumns.filter(ele => ele.field == 'dia')[0];
        colObj.rowIndex = index;
        colObj.isError = true;
        colObj.errorMsg = 'Diameter should not be empty';
      }
      else {
        let colObj = this.heatColumns.filter(ele => ele.field == 'dia')[0];
        colObj.rowIndex = index;
        colObj.isError = false;
        colObj.errorMsg = '';
      }

      if (event.rowData.length == null || event.rowData.length == "" || event.rowData.length <= 0) {
        let colObj = this.heatColumns.filter(ele => ele.field == 'length')[0];
        colObj.rowIndex = index;
        colObj.isError = true;
        colObj.errorMsg = 'Length should not be empty or 0';
      }
      else {
        let colObj = this.heatColumns.filter(ele => ele.field == 'length')[0];
        colObj.isError = false;
        colObj.errorMsg = '';
      }

      if (event.rowData.qtyPcs == null || event.rowData.qtyPcs == "" || event.rowData.qtyPcs <= 0){
        let colObj = this.heatColumns.filter(ele => ele.field == 'qtyPcs')[0];
        colObj.rowIndex = index;
        colObj.isError = true;
        colObj.errorMsg = 'No of pieces should not be empty';
      }
      else {
        let colObj = this.heatColumns.filter(ele => ele.field == 'qtyPcs')[0];
        colObj.rowIndex = index;
        colObj.isError = false;
        colObj.errorMsg = '';
      }

      if (event.rowData.weight == null || event.rowData.weight == "" || event.rowData.weight <= 0) {
        let colObj = this.heatColumns.filter(ele => ele.field == 'weight')[0];
        colObj.rowIndex = index;
        colObj.isError = true;
        colObj.errorMsg = 'Weight should not be empty or 0';
      }
      else {
        let colObj = this.heatColumns.filter(ele => ele.field == 'weight')[0];
        colObj.rowIndex = index;
        colObj.isError = false;
        colObj.errorMsg = '';
      }
  }

  public sendTableData(event): any {

    var index = 0
    var isNoOfPcsEmpty: boolean = false,isDiaEmpty: boolean = false,isWeightEmpty: boolean = false ,isLengthEmpty: boolean = false
    for (index = 0; index < this.selectedRecords.length; index++) {
      if (!this.selectedRecords[index].dia && this.selectedRecords[index].dia !=0) {
        isDiaEmpty = true
        let colObj = this.heatColumns.filter(ele => ele.field == 'dia')[0];
        colObj.rowIndex = index;
        colObj.isError = true;
        colObj.errorMsg = 'Diameter should not be empty';
      }
      if (this.selectedRecords[index].length == null || this.selectedRecords[index].length == "" || this.selectedRecords[index].length <= 0) {
        isLengthEmpty = true
        let colObj = this.heatColumns.filter(ele => ele.field == 'length')[0];
        colObj.rowIndex = index;
        colObj.isError = true;
        colObj.errorMsg = 'Length should not be empty or 0';
      }
      if (this.selectedRecords[index].qtyPcs == null || this.selectedRecords[index].qtyPcs == "" || this.selectedRecords[index].qtyPcs <= 0){
        isNoOfPcsEmpty = true
        let colObj = this.heatColumns.filter(ele => ele.field == 'qtyPcs')[0];
        colObj.rowIndex = index;
        colObj.isError = true;
        colObj.errorMsg = 'No of pieces should not be empty';
      }
      if (this.selectedRecords[index].weight == null || this.selectedRecords[index].weight == "" || this.selectedRecords[index].weight <= 0) {
        isWeightEmpty = true
        let colObj = this.heatColumns.filter(ele => ele.field == 'weight')[0];
        colObj.rowIndex = index;
        colObj.isError = true;
        colObj.errorMsg = 'Weight should not be empty or 0';
      }
  }
    if (this.selectedRecords.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: `Please select the final batch`,
      }
      );
    }
    else if(isDiaEmpty || isNoOfPcsEmpty || isWeightEmpty ||isLengthEmpty)
    {
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          {
            message: `Fields should not be empty`
          }
        );
    }
    else {
      var requestBody = this.selectedRecords.map((x: any) => {
        return {
          "batchStatus": x.batchStatus,
          "bundleId": x.bundleId,
          "customerName": x.customerName,
          "dia": x.dia,
          "grade": x.grade,
          "heatId": x.heatId,
          "length": x.length,
          "prodDate": x.prodDate,
          "productSize": x.productSize,
          "productType": x.productType,
          "qtyPcs": x.qtyPcs,
          "salesOrder": x.salesOrder,
          "scheduleNo": x.scheduleNo,
          "scheduleSeqNo": x.scheduleSeqNo,
          "shiftId": x.shiftId,
          "weight": x.weight
        }
      })
      this.brmQaService.saveFinalMaterialBatchDetails(this.sharedService.loggedInUserDetails.userId, requestBody).subscribe(Response => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success, {
          message: Response,
        }
        );
        this.getMaterialBatchDetails();
        this.selectedRecords = [];
        this.clearSelection.next(true);
      })
    }
  }
}
