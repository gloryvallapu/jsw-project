import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalMaterialBatchUpdateComponent } from './final-material-batch-update.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { Subject } from 'rxjs';
import { BrmQaService } from '../../services/brm-qa.service';
import { FinalMaterialBatchForms } from '../../form-objects/brm-qa-form-objects';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrmqaEndPoints } from '../../form-objects/brm-qa-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { IndividualConfig, ToastrService } from 'ngx-toastr';

describe('FinalMaterialBatchUpdateComponent', () => {
  let sharedService: SharedService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let brmQaService: BrmQaService
  let commonService: CommonApiService
  let formObjects : FinalMaterialBatchForms
  let component: FinalMaterialBatchUpdateComponent;
  let fixture: ComponentFixture<FinalMaterialBatchUpdateComponent>;

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinalMaterialBatchUpdateComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        BrmQaService,
        FinalMaterialBatchForms,
        CommonApiService,
        JswFormComponent,
        BrmqaEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalMaterialBatchUpdateComponent);
    component = fixture.componentInstance;
    brmQaService = fixture.debugElement.injector.get(BrmQaService);
    sharedService = fixture.debugElement.injector.get(SharedService);
    formObjects = fixture.debugElement.injector.get(FinalMaterialBatchForms);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should sum', () => {
    expect(component).toBeTruthy();
  });
});
