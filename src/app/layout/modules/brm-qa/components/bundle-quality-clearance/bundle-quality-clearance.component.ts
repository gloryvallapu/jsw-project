import { Component, OnInit } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { BrmQaService } from '../../services/brm-qa.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { threeDigitsAfterDecimal } from 'src/app/shared/constants/app-constants';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-bundle-quality-clearance',
  templateUrl: './bundle-quality-clearance.component.html',
  styleUrls: ['./bundle-quality-clearance.component.css']
})
export class BundleQualityClearanceComponent implements OnInit {
  public clearSelection: Subject < boolean > = new Subject < boolean > ();

  viewType = ComponentType;
  columns: ITableHeader[] = [];
  public columnType = ColumnType;
  BundleQualityList: any = [];
  tableType: any;
  public checkedRecords: any = [];
  public sampleCheck:boolean = false;
  public materialClassList: any = []
  public selectedRecords: any = [];
  public shapeTypeList: any = [];
  public requestBody: any;
  public isColumnValidated: boolean = false;
  public regEX: any = threeDigitsAfterDecimal;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isViewOnly:boolean= false;
  public isFlCrRtrv:boolean= false;
  public isFlCrRst:boolean = false;
  public isBatchDetails:boolean =false;
  public isMechProps:boolean = false;
  public isSave:boolean = false;
  public mapMaterial:boolean= false;
  public isMapMaterialView:boolean= false;
  public sampleResultColmns:ITableHeader[] = [];
  public faildSampleList:any = [];
  validationFlag: boolean;
  constructor(
    public authService: AuthService,
    public jswComponent: JswFormComponent, public jswService: JswCoreService, public brmQaService: BrmQaService, public sharedService: SharedService,) {
    this.sampleResultColmns = [
      {
        field: 'releaseToNco',
        header: 'Release To NCO',
        columnType: ColumnType.radio,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'releaseToSo',
        header: 'Release To SO',
        columnType: ColumnType.radio,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Bundle ID',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'bundleType',
        header: 'Product Type',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'bundleSize',
        header: 'Size',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'dia',
        header: 'Diameter(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'reasonForMaterialClassChange',
        header: 'Reason',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'productionDate',
        header: 'Production Date',
        columnType: ColumnType.date,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'sampleStatus',
        header: 'Sample Status',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'sampleResult',
        header: 'Sample Result',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
    ];
      this.columns = [
      {
        field: 'checkbox',
        header: 'Dummy',
        columnType: ColumnType.checkbox,
        width: '40px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Bundle ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'salesOrder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'scheId',
        header: 'Schd ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'batchGrade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'bundleType',
        header: 'Type',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'bundleSize',
        header: 'Size',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Wgt(tons)',
        columnType: ColumnType.textField,
        width: '80px',
        sortFieldName: '',

      },
      {
        field: 'qtyPcs',
        header: 'Qty Pcs',
        columnType: ColumnType.textField,
        width: '80px',
        sortFieldName: '',

      },
      {
        field: 'materialClass',
        header: 'Material Class',
        columnType: ColumnType.dropdown,
        width: '120px',
        sortFieldName: '',
      },

      {
        field: 'reasonForMaterialClassChange',
        header: 'Reason',
        columnType: ColumnType.textWithValidations,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'dia',
        header: 'Dia(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
        disableCol: false
      },
      {
        field: 'thickness',
        header: 'Thk(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
        disableCol: false
      },
      {
        field: 'width',
        header: 'Wdt(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
        disableCol: false
      },
      {
        field: 'length',
        header: 'Len(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
        disableCol: false
      },
      {
        field: 'materialStatus',
        header: 'Material Status',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'scheduleLine',
        header: 'Schd Line',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'shift',
        header: 'Shift',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.ellipsis,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'productionDate',
        header: 'Production Date',
        columnType: ColumnType.date,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'sampleStatus',
        header: 'Sample Status',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'sampleResult',
        header: 'Sample Result',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
    ];
  }

  public scheIdList: any;
  public heatIdList: any;
  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM_QA)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.brmQAScreenIds.BUNDLE_QUALITY_CLEARANCE_SCREEN.id)[0];
    this.isSave = this.sharedService.isButtonVisible(true, menuConstants.BUNDLE_QUALITY_CLEARANCE_SCREEN_SectionIds.List.buttons.save, menuConstants.BUNDLE_QUALITY_CLEARANCE_SCREEN_SectionIds.List.id, this.screenStructure);
    this.isViewOnly = this.sharedService.isSectionVisible(menuConstants.BUNDLE_QUALITY_CLEARANCE_SCREEN_SectionIds.List.id, this.screenStructure);

  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.materialClassList1()
    this.bundleQualityClearance();
    this.getValidationStatus();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
  }

  public getValidationStatus(){
    this.brmQaService.validationFlag().subscribe(Response =>{
      this.validationFlag = Response == 'Y'?true:false;
    })
  }

  public saveBundleQuality(event): any {
    if (this.selectedRecords.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please select Bundle`,
        }
      );
    }
    var isColumnValidated: boolean = false;
    let index = 0;
    for (index = 0; index < this.selectedRecords.length; index++) {
      if (this.selectedRecords[index].dia == null || this.selectedRecords[index].dia == 0) {
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          {
            message: `Please enter Diameter`,
          }
        );
        this.isColumnValidated = true;
      }
      else if (this.selectedRecords[index].length == null || this.selectedRecords[index].length == 0) {
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          {
            message: `Please enter Length`,
          }
        );
        isColumnValidated = true;
      } else if (this.selectedRecords[index].materialClass != 'P' && (this.selectedRecords[index].reasonForMaterialClassChange == null || this.selectedRecords[index].reasonForMaterialClassChange == 0)) {
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          {
            message: `Please enter Reason`,
          }
        );
        isColumnValidated = true;
      }
    }
    this.requestBody = this.selectedRecords.map((x: any) => {
      return {
        batchGrade: x.batchGrade,
        batchId: x.batchId,
        bundleSize: x.bundleSize,
        bundleType: x.bundleType,
        customerName: x.customerName,
        dia: x.dia,
        heatId: x.heatId,
        length: x.length,
        materialClass: x.materialClass,
        materialStatus: x.materialStatus,
        productionDate: x.date,
        qtyPcs: x.qtyPcs,
        reasonForMaterialClassChange: x.reasonForMaterialClassChange,
        salesOrder: x.salesOrder,
        scheId: x.scheduleId,
        scheduleLine: x.scheduleLine,
        shift: x.shift,
        weight: x.weight
      }
    })
    if (!isColumnValidated) {
      this.brmQaService.saveBundleQuality(this.sharedService.loggedInUserDetails.userId,
        this.requestBody).subscribe((Response) => {
          this.selectedRecords = [];
          this.sharedService.displayToastrMessage(
            this.sharedService.toastType.Success,
            { message: Response }
          );
          this.bundleQualityClearance();
        })
    }
  }


  public gerRowOnclick(fieldName, columeName, rowData){
    if(fieldName == "releaseToNco"){
      rowData.releaseToSo = false;
    }
    if(fieldName == "releaseToSo"){
      rowData.releaseToNco = false;
    }

  }

  public releaseBundleQuality(): any {
    if (this.selectedRecords.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please select Bundle`,
        }
      );
    }
    var isColumnValidated: boolean = false;
    let index = 0;
    for (index = 0; index < this.selectedRecords.length; index++) {
      if (this.selectedRecords[index].materialId == 'S_PRBW' && (this.selectedRecords[index].dia == null || this.selectedRecords[index].dia == 0)) {
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning, { message: `Please enter Diameter` }
        );
        this.isColumnValidated = true;
      }
      if (this.selectedRecords[index].materialId == 'S_LPFW' && (!this.selectedRecords[index].width || !this.selectedRecords[index].thickness)) {
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          {
            message: `Please check the values of thickness and width`,
          }
        );
        this.isColumnValidated = true;
      }
      else if (this.selectedRecords[index].length == null || this.selectedRecords[index].length == 0) {
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          {
            message: `Please enter Length`,
          }
        );
        isColumnValidated = true;
      } else if (this.selectedRecords[index].materialClass != 'Prime' && (this.selectedRecords[index].reasonForMaterialClassChange == null || this.selectedRecords[index].reasonForMaterialClassChange == 0)) {
        return this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          {
            message: `Please enter Reason`,
          }
        );
        isColumnValidated = true;
      }
    };
if(this.validationFlag){
  this.faildSampleList = this.selectedRecords.filter((obj:any)=>obj.sampleResult == 'Fail' || obj.sampleResult == null);
  this.sampleCheck = this.faildSampleList.length > 0;
  if(this.faildSampleList.length > 0){
    return this.faildSampleList.length > 0
  }
}

    this.releaseSample(this.selectedRecords,isColumnValidated);
  };


  public releaseSample(selectedRecords,isColumnValidated){
      this.requestBody = selectedRecords.map((x: any) => {
      return {
        batchGrade: x.batchGrade,
        batchId: x.batchId,
        bundleSize: x.bundleSize,
        bundleType: x.bundleType,
        customerName: x.customerName,
        dia: x.dia,
        heatId: x.heatId,
        length: x.length,
        materialClass: x.materialClass,
        materialStatus: x.materialStatus,
        productionDate: x.date,
        qtyPcs: x.qtyPcs,
        reasonForMaterialClassChange: x.reasonForMaterialClassChange,
        salesOrder: x.salesOrder,
        scheId: x.scheduleId,
        scheduleLine: x.scheduleLine,
        shift: x.shift,
        weight: x.weight,
        releaseAsNco:x.releaseToSo? 'N':'Y'
      }
    })
    if (!isColumnValidated) {
      this.brmQaService.releaseQuality(this.sharedService.loggedInUserDetails.userId,
        this.requestBody).subscribe((Response) => {
          this.closeDialog();
          this.clearSelection.next(true);
            this.selectedRecords = [];
            this.sharedService.displayToastrMessage(
            this.sharedService.toastType.Success,
            { message: Response }
          );
          this.bundleQualityClearance();
        })
    }

  }


  public releaseFailedSamples(){
    let releaseSpecialCaseSamples = this.selectedRecords.filter((obj:any)=>obj.sampleResult == "Pass").concat(this.faildSampleList);
    this.releaseSample(releaseSpecialCaseSamples,false);
  }

  public closeDialog(){
    this.sampleCheck = false;
  }

  public getSelectedRecord(event){

  }

  public bundleQualityClearance() {
    this.brmQaService.bundleQualityClearance(this.sharedService.loggedInUserDetails.userId).subscribe((
      data => {
        this.BundleQualityList = data.map((rowData: any) => {
          //rowData.materialClass = this.shapeTypeList.length ? this.shapeTypeList.filter(ele => ele.modelValue == 'C')[0].modelValue : null;
          if(rowData.bundleType == 'TMT'){
            rowData.tmtCheck = true;
          }
          else if(rowData.bundleType == 'FLAT'){
            rowData.flatCheck = true;
          }
          return rowData;
        });
        this.scheIdList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.scheId, modelValue: x.scheId };
        }));
        this.heatIdList = this.sharedService.getUniqueRecords(data.map((x: any) => {
          return { displayName: x.heatId, modelValue: x.heatId };
        }));
       // this.BundleQualityList = data;

      }))
  }

  public materialClassList1() {
    this.brmQaService.materialClassList().subscribe((data) => {
      this.shapeTypeList = data.map((ele: any) => {
        return {
          displayName: ele.valueDescription,
          modelValue: ele.valueShortCode,
        };
      });
    });


  }

  public validateWeight(event, rowData, colField): any {
    rowData.actualFGError = false;
    rowData.actualCobbalError = false;
    if (colField == 'qtyPcs') {
      if (rowData.qtyPcs != 0) {
        // var calcwt = Number(rowData.qtyPcs) * 0.4
        // rowData.weight = calcwt.toFixed(4);
      }
      else {
        rowData.actualCobbalError = true;
        rowData.actualCobWeightMsg = "FG weight should be greater than 0"
      }
    }
    if (colField == 'materialClass' || colField == 'reasonForMaterialClassChange') {
      if (rowData.materialClass != "P") {
        if (rowData.reasonForMaterialClassChange == null || rowData.reasonForMaterialClassChange == "") {
          rowData.rollingRemarkError = true;
        }
        else {
          rowData.rollingRemarkError = false;
        }
      }
      else {
        rowData.rollingRemarkError = false;
      }
    }
  }

  public checkShapeStatus(event, rowData) {
    if (event.value != "P") {
      if (rowData.reasonForMaterialClassChange == null || rowData.reasonForMaterialClassChange == "") {
        rowData.rollingRemarkError = true;
      }
    }
    else {
      rowData.rollingRemarkError = false;
    }
  }
}
