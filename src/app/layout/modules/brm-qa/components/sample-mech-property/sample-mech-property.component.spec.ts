import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SampleMechPropertyComponent } from './sample-mech-property.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { SampleMechProForms } from '../../form-objects/brm-qa-form-objects';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { Subject } from 'rxjs';
import { BrmQaService } from '../../services/brm-qa.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { BrmqaEndPoints } from '../../form-objects/brm-qa-api-endpoints';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

fdescribe('SampleMechPropertyComponent', () => {
  let component: SampleMechPropertyComponent;
  let fixture: ComponentFixture<SampleMechPropertyComponent>;
  let sharedService: SharedService
  let brmQaService: BrmQaService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let commonService: CommonApiService

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SampleMechPropertyComponent],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        BrmQaService,
        CommonApiService,
        JswFormComponent,
        BrmqaEndPoints,
        API_Constants,
        CommonAPIEndPoints,SampleMechProForms,
        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleMechPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
