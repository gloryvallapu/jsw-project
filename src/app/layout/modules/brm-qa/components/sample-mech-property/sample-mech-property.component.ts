import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { SampleMechProForms } from '../../form-objects/brm-qa-form-objects';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { Subject } from 'rxjs';
import { BrmQaService } from '../../services/brm-qa.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { threeDigitsAfterDecimal } from 'src/app/shared/constants/app-constants';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';

@Component({
  selector: 'app-sample-mech-property',
  templateUrl: './sample-mech-property.component.html',
  styleUrls: ['./sample-mech-property.component.css'],
})
export class SampleMechPropertyComponent implements OnInit {
  public formObject: any;
  public viewType = ComponentType;
  public tableType: any;
  public columns: ITableHeader[] = [];
  public BatchIdcolumns: ITableHeader[] = [];
  public columnType = ColumnType;
  public sampleMechDetailsList: any = [];
  public batchIDList: any = [];
  public mechdata: any = [];
  public metaldata: any = [];
  public mechProplist: any = [];
  public metalProplist: any = [];
  public b_testList: any = [];
  public rb_testList: any = [];
  public surf_condList: any = [];
  public requestBody: any;
  public sampleId:string='';
  public showmechprop:boolean=false;
  public gridexpand = '';
  public regEX: any = threeDigitsAfterDecimal;
  public display:boolean;
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isFlCrViewOnly:boolean= false;
  public isFlCrRtrv:boolean= false;
  public isFlCrRst:boolean = false;
  public isBatchDetails:boolean =false;
  public isMechProps:boolean = false;
  public isSave:boolean = false;
  public mapMaterial:boolean= false;
  public isMapMaterialView:boolean= false;
  public userId:string;
  public clmtxt:string='txt';
  public clmdrp:string='drp';
  //for vaidation of txt and range
  public clmRange:string='Range';
  public clmEql:string='Equal_to';
  public maxDate = new Date();

  constructor(
    public authService: AuthService,
    public SampleMechProForm: SampleMechProForms,
    public sharedService: SharedService,
    public brmQaService: BrmQaService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public commonService: CommonApiService
  ) {}

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM_QA)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.brmQAScreenIds.SAMPLE_MECH_PROPERTY_SCREEN.id)[0];
    this.isFlCrViewOnly = this.sharedService.isSectionVisible(menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRtrv = this.sharedService.isButtonVisible(true, menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.Filter_Criteria.buttons.retrive, menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRst = this.sharedService.isButtonVisible(true, menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.Filter_Criteria.buttons.reset, menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isBatchDetails = this.sharedService.isSectionVisible(menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.List.id, this.screenStructure);
    this.isMechProps = this.sharedService.isSectionVisible(menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.MechProperties.id, this.screenStructure);
    this.isSave = this.sharedService.isButtonVisible(true, menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.MechProperties.buttons.save, menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.MechProperties.id, this.screenStructure);
    this.mapMaterial = this.sharedService.isButtonVisible(true, menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.MechProperties.buttons.mapMaterial, menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.MechProperties.id, this.screenStructure);
    this.isMapMaterialView = this.sharedService.isSectionVisible(menuConstants.SAMPLE_MECH_PROPERTY_SCREEN_SectionIds.MechProperties.id, this.screenStructure);
}

  ngOnInit(): void {
    this.screenRightsCheck();
    this.userId=this.sharedService.loggedInUserDetails.userId;
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.SampleMechProForm.SampleMaterialProFilterCriteria));
    this.maxDate.setDate(this.maxDate.getDate());
    this.formObject.formFields.forEach(element => {
      if(element.ref_key == 'toDate' || element.ref_key == 'fromDate'){
        element.maxDate = this.maxDate
      }
    });
    this.getSampleMechProductList();
    this.getSampleMechStatusList();
    this.filterSampleMechDetails();
    this.columns = [
      {
        field: 'sampleId',
        header: 'Sample ID',
        columnType: ColumnType.hyperlink,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'orderId',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'jswGrade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'bundleDia',
        header: 'Bun Size',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },


      {
        field: 'bundleLength',
        header: 'Bun Len(mm) ',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'bundleShape',
        header: 'Bun Shape',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },

      {
        field: 'eqSpecGroup', // add new column
        header: 'Eq Spec Group',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },

      {
        field: 'sampleStatus',
        header: 'Sample Status',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.ellipsis,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'createdDate',
        header: 'Created Date',
        columnType: ColumnType.date,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'scheduleId',
        header: 'Schd ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      }
    ];
    this.BatchIdcolumns = [
      {
        field: 'bundleId',
        header: 'Bundle ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'sampleCount',
        header: 'Sample Count',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Weight(tons)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      }
    ];
  }

  public getSampleMechProductList() {
    this.brmQaService.getSampleMechProductList().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'productName'
      )[0].list = data.map((x: any) => {
        return { displayName: x.productName, modelValue: x.materialId };
      });
    });
  }

  public getSampleMechStatusList() {
    this.brmQaService.getSampleMechStatusList().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'status'
      )[0].list = data.map((x: any) => {
        return { displayName: x.status, modelValue: x.status };
      });
    });
  }

  public getsampleMechPropDetails(event) {
    if(!this.isMechProps){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access Mech Properties section. Please contact Admin."});
      return;
    }
    this.sampleId = event.sampleId;
    this.brmQaService.getMechDetails(this.sampleId).subscribe((data) => {
      this.mechdata = data;
      this.mechProplist = (this.mechdata as any).mechPropList;
      this.mechProplist = this.mechProplist.map((x:any) => {
        x.selectedValue = x.dropDownValues ? x.value: null;
        x.dropDownValues = x.dropDownValues ? x.dropDownValues.map(ele => { return { displayName: ele.drpItemValue, modelValue: ele.drpItemLovId}}) : [];
        if(x.dropDownValues.length){
          x.dropDownValues.forEach(element => {
            if(x.selectedValue == element.modelValue){
              x.selectedName = element.displayName
            }

          });
        }
        return x;
      });
    })
    this.brmQaService.getMetalPropDetails(this.sampleId).subscribe((data) => {
      this.metalProplist = data.map((x:any) => {
        // x.min = x.value;
        // x.dropDownValues = x.dropDownValues ? x.dropDownValues.map(ele => { return { displayName: ele.drpItemValue, modelValue: ele.drpItemLovId}}) : [];
        x.selectedValue = x.dropDownValues ? x.value: null;
        x.dropDownValues = x.dropDownValues ? x.dropDownValues.map(ele => { return { displayName: ele.drpItemValue, modelValue: ele.drpItemLovId}}) : [];
        if(x.dropDownValues.length){
          x.dropDownValues.forEach(element => {
            if(x.selectedValue == element.modelValue){
              x.selectedName = element.displayName
            }

          });
        }
        return x;
      });
    });
    this.showmechprop = true;
    this.gridexpand = '';
  }
  public getLovs(lovCode) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      let arr = data.map((ele: any) => {
        return {
          displayName: ele.valueDescription,
          modelValue: ele.lovValueId,
        };
      });
      // if (lovCode == 'b_test') {
      //   this.b_testList = arr;
      // }
      // if (lovCode == 'rb_test') {
      //   this.rb_testList = arr;
      // }
      // if (lovCode == 'surf_cond') {
      //   this.surf_condList = arr;
      // }
    });
  }
  public saveMechPropDetails(mechdata): any {
    this.mechdata = mechdata;
    this.requestBody = this.mechdata.mechPropList.map((x: any) => {
      return {
        propName: x.propName,
        value: x.value,
      };
    });
    this.brmQaService
      .saveMechPropDetails(this.sampleId,this.userId, this.requestBody)
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: Response }
        );
      });
  }
  public saveMetalPropDetails(metaldata): any {
    this.metaldata = metaldata;
    this.requestBody = this.metaldata.map((x: any) => {
      return {
        propName: x.propName,
        value: x.value,
      };
    });
    var mechRequestBody = this.mechdata.mechPropList.map((x: any) => {
      return {
        propName: x.propName,
        value: x.value,
      };
    });
    // this.brmQaService
    //   .saveMetalPropDetails(this.sampleId,this.userId, this.requestBody)
    //   .subscribe((Response) => {
    //     this.sharedService.displayToastrMessage(
    //       this.sharedService.toastType.Success,
    //       { message: Response }
    //     );
    //   });
    //var newRequestBody =  {...this.mechdata,...this.metaldata};
    var newReq = {
      mechPropBoList: null,
      metalPropBoList: null
    };
    newReq.mechPropBoList = this.mechdata.mechPropList;
    newReq.metalPropBoList = this.metaldata;
    this.brmQaService
      .newSaveMechMetalPropDetails(this.sampleId,this.userId, newReq)
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: Response }
        );
      });
  }
  public filterSampleMechDetails() {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let reqBody = {
        heatId: this.formObject.formFields.filter(
          (ele) => ele.ref_key == 'heatId'
        )[0].value,
        productName: this.formObject.formFields.filter(
          (ele) => ele.ref_key == 'productName'
        )[0].value,
        status: this.formObject.formFields.filter(
          (ele) => ele.ref_key == 'status'
        )[0].value,
        fromDate: this.formObject.formFields.filter(
          (ele) => ele.ref_key == 'fromDate'
        )[0].value,
        toDate: this.formObject.formFields.filter(
          (ele) => ele.ref_key == 'toDate'
        )[0].value,
      };
      this.brmQaService.getSampleMechDetails(reqBody).subscribe((Response) => {
        this.sampleMechDetailsList = Response;
      });
    }
  }

  public resetForm() {
    this.jswComponent.resetForm(this.formObject);
    this.filterSampleMechDetails();
    this.showmechprop=false;
  }

  public showDialog() {
    this.getMechDetails();
    this.display = true;
  }

  public getMechDetails() {
    this.brmQaService.getSampleMapping(this.sampleId).subscribe((Response) => {
      this.batchIDList = Response;
    });
  }

  public editRecord(data)
  {

  }
}
