import { Component, OnInit } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { AuthService } from 'src/app/core/services/auth.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { NDTForms } from '../../form-objects/brm-qa-form-objects';
import { BrmQaService } from '../../services/brm-qa.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { TableType, ColumnType } from 'src/assets/enums/common-enum';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
@Component({
  selector: 'app-ndt',
  templateUrl: './ndt.component.html',
  styleUrls: ['./ndt.component.css']
})
export class NdtComponent implements OnInit {
  public viewType = ComponentType;
  public filterCriteriaForm: any;
  public ndtDetails: any = [];
  public columns: ITableHeader[] = [];
  public sampleTestColumns: ITableHeader[] = [];
  public tableType: any;
  public selectedRecords: any = [];
  public columnType = ColumnType;
  public testDetails: any = [];
  public selectedSample: any;
  public submitted: boolean = false;
  // User Auth Variables
  public screenStructure: any = [];
  public isFilterSection : boolean = false;
  public isFlCrRtrvBtn: boolean = false;
  public isFlCrRstBtn: boolean = false;
  public isNDTDetails: boolean = false;
  public isTestDetails: boolean = false;
  public isSaveBtn: boolean = false;

  //22-11
  public userDetails = this.sharedService.getLoggedInUserDetails();
  //

  constructor(
    public authService: AuthService, public jswService: JswCoreService, public jswComponent: JswFormComponent,
    public ndtForm: NDTForms, public brmQaService: BrmQaService, public sharedService: SharedService, public commonApiService: CommonApiService) {}

  ngOnInit(): void {
    this.screenRightsCheck();
    this.filterCriteriaForm = JSON.parse(JSON.stringify(this.ndtForm.filterCriteria));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getColumns();
    this.getSampleTestColumns();
    this.getScheduleIds();
  }

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM_QA)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.brmQAScreenIds.NDT.id)[0];
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.NDT_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRtrvBtn = this.sharedService.isButtonVisible(true, menuConstants.NDT_SectionIds.Filter_Criteria.buttons.retrieve, menuConstants.NDT_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRstBtn = this.sharedService.isButtonVisible(true, menuConstants.NDT_SectionIds.Filter_Criteria.buttons.reset, menuConstants.NDT_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isNDTDetails = this.sharedService.isSectionVisible(menuConstants.NDT_SectionIds.NDT_Details.id, this.screenStructure);
    this.isTestDetails = this.sharedService.isSectionVisible(menuConstants.NDT_SectionIds.NDT_Test_Details.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.NDT_SectionIds.NDT_Test_Details.buttons.save, menuConstants.NDT_SectionIds.NDT_Test_Details.id, this.screenStructure);
  }

  public getColumns(){
    this.columns = [
      {
        field: 'bundleId',
        header: 'Bundle ID',
        columnType: ColumnType.hyperlink,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'sampleId',
        header: 'Sample ID',
        columnType: ColumnType.hyperlink,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'productTypeDesc',
        header: 'Product',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'sampleStatus',
        header: 'Status',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      }
    ];
  }

  public getSampleTestColumns(){
    this.sampleTestColumns = [
      {
        field: 'testTypeName',
        header: 'Test Type',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'selectedSubType',
        header: 'Sub Test Type',
        columnType: ColumnType.dropdown,
        width: '160px',
        sortFieldName: '',
      },
      // selectedSubTest selectedTestResult{
      //   field: 'soCharName',
      //   header: 'SO Char Name',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      {
        field: 'value',
        header: 'Test Result',
        columnType: ColumnType.dropdown,
        width: '130px',
        sortFieldName: '',
      },
    ]
  }

  public getScheduleIds(){
    // API call to get schedule Ids
    this.brmQaService.getScheduleIds().subscribe(data => {
      this.filterCriteriaForm.formFields.filter(ele => ele.ref_key == 'scheduleID')[0].list = data.map((x: any) => {
        return { displayName: x, modelValue: x };
      });
    })
  }

  public dropDownChangeEvent(event) {
    if(event.ref_key == 'scheduleID'){
      this.getHeatIds(event.value);
    }
    // if(event.ref_key == 'heatID'){
    //   this.getBundleIds(event.value);
    // }
  }

  public getHeatIds(scheduleID){
    // API call to get heat Ids
    this.brmQaService.getHeatIds(scheduleID).subscribe(data => {
      this.filterCriteriaForm.formFields.filter(ele => ele.ref_key == 'heatID')[0].list = data.map((x: any) => {
        return { displayName: x, modelValue: x };
      });
    })
  }

  // public getBundleIds(heatID){
  //   // API call to get bundle Ids based on heat Id
  //   this.brmQaService.getBundleIds(heatID).subscribe(data => {
  //     this.filterCriteriaForm.formFields.filter(ele => ele.ref_key == 'bundleID')[0].list = data.map((x: any) => {
  //       return { displayName: x, modelValue: x };
  //     });
  //   })
  // }

  public getSampleTestDetails(event){
    this.selectedSample = event;
    //this.brmQaService.getTestTypeList(this.selectedSample.productTypeCode).subscribe(data => {
      //new code as api changed and BO changed
      this.brmQaService.getTestDetailsBySampleId(this.selectedSample.sampleId).subscribe(data => {
      this.testDetails = data.ndtPropList.map((x:any) => {
       // x.selectedSubTest = null;
        x.subTestType = x.subTypesDrpDownValues ? x.subTypesDrpDownValues.map(ele => { return { displayName: ele.drpItemValue, modelValue: ele.drpItemLovId}}) : [];
        //x.selectedTestResult = null;
        //x.testResult = x.testResult ? x.testResult.map(ele => { return { displayName: ele, modelValue: ele}}) : [];
        //new code as BO changed to dynamic set value
        x.testTypeName =  x.propDescription
        x.testResultBoList = x.dropDownValues ? x.dropDownValues.map(ele => { return { displayName: ele.drpItemValue, modelValue: ele.drpItemLovId}}) : [];
        return x;
      });
    })
  }

  public filterData(){
    this.jswComponent.onSubmit(this.filterCriteriaForm.formName, this.filterCriteriaForm.formFields);
    var formData = this.jswService.getFormData();
     if(formData && formData.length){
       //API call for filter details
       let formObj = this.sharedService.mapFormObjectToReqBody(formData, {scheduleID: '', heatID: ''});
       this.brmQaService.getNDTBundleDetails(formObj.heatID).subscribe(data => {
         this.ndtDetails = data;
       })
     }
  }

  public reset(){
    this.jswComponent.resetForm(this.filterCriteriaForm);
    this.ndtDetails=[];
    this.selectedSample=false;
  }

  public saveDetails(){
    this.submitted = true;
    if(this.testDetails && this.testDetails.length){
      let tempArr = this.testDetails.filter(ele => (ele.testTypeCode == 'grd_mx' && !ele.selectedSubType) || !ele.value);
      if(tempArr.length){
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please enter required fields'});
        return;
      }
      let reqBody = this.testDetails.map((x:any) => {
        return {
          "sampleId": this.selectedSample.sampleId,
          "testResult": x.selectedTestResult,
          "testType": x.testTypeCode
        }
      })
      //this.testDetails.selectedSubType = this.testDetails.selectedSubTest;
     
      this.brmQaService.newSaveNDTDetails(this.selectedSample.sampleId,this.sharedService.loggedInUserDetails.userId,this.testDetails).subscribe(
        data => {
          if(data == 'Pass'){
            this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: data});
          this.filterData();
          }
          else{
            this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: data});
            //this.filterData();
          }
          // this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: data});
          // this.filterData();
        }
      )
    }else{
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'No records available to save'});
      return;
    }
  }

}
