import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StickerPrintingComponent } from './sticker-printing.component';

describe('StickerPrintingComponent', () => {
  let component: StickerPrintingComponent;
  let fixture: ComponentFixture<StickerPrintingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StickerPrintingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StickerPrintingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
