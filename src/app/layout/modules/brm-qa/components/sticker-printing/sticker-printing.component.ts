import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared/services/shared.service';
import { BrmQaService } from '../../services/brm-qa.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';

@Component({
  selector: 'app-sticker-printing',
  templateUrl: './sticker-printing.component.html',
  styleUrls: ['./sticker-printing.component.css']
})
export class StickerPrintingComponent implements OnInit {
  public barCode: any;
  public bundleId: any;
  public stickerParms: any;
  public qrCode: any;
  public screenStructure: any = [];
  public isFilterSection : boolean = false;
  public isFlCrRtrvBtn: boolean = false;
  public isFlCrRstBtn: boolean = false;
  public isPrint: boolean = false;
  constructor(public brmQaService: BrmQaService, public authService: AuthService,
    public sharedService: SharedService,private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.getImg();
  }

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.BRM_QA)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.brmQAScreenIds.STICKER_PRINTING_SCRREN.id)[0];
    this.isFilterSection = this.sharedService.isSectionVisible(menuConstants.STICKER_PRINTING_SCRREN_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRtrvBtn = this.sharedService.isButtonVisible(true, menuConstants.STICKER_PRINTING_SCRREN_SectionIds.Filter_Criteria.buttons.retrive, menuConstants.STICKER_PRINTING_SCRREN_SectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRstBtn = this.sharedService.isButtonVisible(true, menuConstants.STICKER_PRINTING_SCRREN_SectionIds.List.buttons.close, menuConstants.STICKER_PRINTING_SCRREN_SectionIds.List.id, this.screenStructure);
    this.isPrint = this.sharedService.isButtonVisible(true, menuConstants.STICKER_PRINTING_SCRREN_SectionIds.List.buttons.print, menuConstants.STICKER_PRINTING_SCRREN_SectionIds.List.id, this.screenStructure);
  }
  public getImg(){
    this.brmQaService.getLocalImg().subscribe((response: any)=>{
      let barCodeURL = 'data:image/png;base64,' + response.barCodeByteArray;
      this.barCode = this.sanitizer.bypassSecurityTrustUrl(barCodeURL);
      let qrCodeURL = 'data:image/png;base64,' + response.qrCodeByteArray;
      this.qrCode = this.sanitizer.bypassSecurityTrustUrl(qrCodeURL);
      this.stickerParms = response.stickerParametersBo;
     // this.sharedService.generateReport('png',this.imgCode,'trailImg'); 'M003030B11'
    })
  }

  public getStickerDetails(){
    // this.brmQaService.getLocalImg().subscribe((response: any)=>{
    //   let barCodeURL = 'data:image/png;base64,' + response.barCodeByteArray;
    //   this.barCode = this.sanitizer.bypassSecurityTrustUrl(barCodeURL);
    //   let qrCodeURL = 'data:image/png;base64,' + response.qrCodeByteArray;
    //   this.qrCode = this.sanitizer.bypassSecurityTrustUrl(qrCodeURL);
    //   this.stickerParms = response.stickerParametersBo;
    //  // this.sharedService.generateReport('png',this.imgCode,'trailImg'); 'M003030B11'
    // })
    this.brmQaService.getStickerDetails(this.bundleId).subscribe((response: any)=>{
      if(response){
      let barCodeURL = 'data:image/png;base64,' + response.barCodeByteArray;
      this.barCode = this.sanitizer.bypassSecurityTrustUrl(barCodeURL);
      let qrCodeURL = 'data:image/png;base64,' + response.qrCodeByteArray;
      this.qrCode = this.sanitizer.bypassSecurityTrustUrl(qrCodeURL);
      this.stickerParms = response.stickerParametersBo;
      }
    })
  }
  public print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <style>
          // height: 150mm;
          //  width: 100mm;
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()" >${printContents}</body>
      </html>`
    );
    popupWin.document.close();
}


}
