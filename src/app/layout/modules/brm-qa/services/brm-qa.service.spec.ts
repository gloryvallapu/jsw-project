import { TestBed } from '@angular/core/testing';

import { BrmQaService } from './brm-qa.service';

describe('BrmQaService', () => {
  let service: BrmQaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BrmQaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
