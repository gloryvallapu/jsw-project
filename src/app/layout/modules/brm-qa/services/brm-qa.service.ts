import {
  Injectable
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  API_Constants
} from 'src/app/shared/constants/api-constants';
import {
  map
} from 'rxjs/operators';
import {
  AuthService
} from 'src/app/core/services/auth.service';
import {
  BrmqaEndPoints
} from '../form-objects/brm-qa-api-endpoints';
import {
  CommonAPIEndPoints
} from 'src/app/shared/constants/common-api-end-points';
import {
  BehaviorSubject
} from 'rxjs';
import { SharedService } from 'src/app/shared/services/shared.service';

@Injectable({
  providedIn: 'root'
})
export class BrmQaService {

  public baseUrl: string;
  public masterBaseUrl: string;
  ///10-11
  public localBaseUrl = 'assets/mock-json';
  constructor(public brmQaEndPoints:BrmqaEndPoints,public http: HttpClient, private apiURL: API_Constants, public commonEndPoints: CommonAPIEndPoints,
              public authService: AuthService, public sharedService: SharedService) {
    this.baseUrl = `${this.apiURL.baseUrl}${this.apiURL.brmQaGateway}`;
    this.masterBaseUrl = `${this.apiURL.baseUrl}${this.apiURL.masterGateWay}`;
  }

  // TPI HOLD RELEASE screen Apis
  public tpiSelectedRecords = new BehaviorSubject<Object>([]);
  public gettpiSelectedRecords = this.tpiSelectedRecords.asObservable();
  public settpiSelectedRecords(data: any) {
    this.tpiSelectedRecords.next(data);
  }
   public filterCriteria(){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.filterCriteria}/${this.sharedService.loggedInUserDetails.userId}`).pipe(map((response: any) => response));

   }
    public getTpiHoldDetails(scheduleId){
      return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.tpiHoldDetails}/${scheduleId}`).pipe(map((response: any) => response));
    }

    public save(userId,batches){
      const headers = new HttpHeaders({
        'content-type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      });
      return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.save}/${userId}`,batches,{ headers, responseType: 'text' }).pipe(map((response: any) => response));
    }

    public release(userId,batches){
      const headers = new HttpHeaders({
        'content-type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      });
      return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.release}/${userId}`,batches,{ headers, responseType: 'text' }).pipe(map((response: any) => response));
    }
  // end TPI HOLD RELEASE screen Apis

  // Start final material batch update

  public getWorkCenterList() {
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getWorkCenterList}/${this.sharedService.loggedInUserDetails.userId}`).pipe(map((response: any) => response));
   }

   public getMaterialBatchByUnitAndBundId(wcName,bundleId) {
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getMaterialBatchDetails}/${wcName}/${bundleId}`).pipe(map((response: any) => response));
   }

   public getMaterialBatchByUnit(wcName) {
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getMaterialBatchDetails}/${wcName}`).pipe(map((response: any) => response));
   }

   public saveFinalMaterialBatchDetails(userId, reqBody) {
    return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.saveFinalMaterialBatchDetails}/${userId}`,reqBody,{ responseType: 'text' })
      .pipe(map((response: any) => response));
  }
  // End final material batch update

  //start Bundle Quality clearance

  public bundleQualityClearance(userId){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.bundleQualityClearance}/${userId}`).pipe(map((response: any) => response));
  }

  public materialClassList(){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.materialClassList}`).pipe(map((response: any) => response));
  }

  public saveBundleQuality(userId,reqBody){
    return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.saveBundleQuality}/${userId}`,reqBody, { responseType: 'text' }).pipe(map((response: any) => response));
 }

 public releaseQuality(userId,reqBody){
    return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.releaseQuality}/${userId}`,reqBody, { responseType: 'text' }).pipe(map((response: any) => response));
  }

public validationFlag(){
  const headers = new HttpHeaders({
    'content-type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  });
  return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.validationFlag}`,{ headers, responseType: 'text' }).pipe(map((response: any) => response));

}
  //End Bundle Quality clearance


  //start Sample Mech Properties Apis

  public getSampleMechProductList(){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.sampleMechProductList}`).pipe(map((response: any) => response));
  }
  public getSampleMechStatusList(){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.sampleMechStatusList}`).pipe(map((response: any) => response));
  }
  public getMechDetails(sampleId){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.sampleMechDetails}/${sampleId}`).pipe(map((response: any) => response));
  }
  public saveMechPropDetails(sampleId,userId,reqBody){
    return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.saveMechPropDetails}/${sampleId}/${userId}`,reqBody, { responseType: 'text' }).pipe(map((response: any) => response));
 }
 public getMetalPropDetails(sampleId){
  return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.sampleMetalPropDetails}/${sampleId}`).pipe(map((response: any) => response));
}
public saveMetalPropDetails(sampleId,userId,reqBody){
  return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.saveMetalPropDetails}/${sampleId}/${userId}`,reqBody, { responseType: 'text' }).pipe(map((response: any) => response));
}
 public getSampleMechDetails(reqBody){
  return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.getSampleMechDetails}`,reqBody).pipe(map((response: any) => response));
}
public getSampleMapping(sampleId){
  return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getSampleMapping}/${sampleId}`).pipe(map((response: any) => response));
}

public newSaveMechMetalPropDetails(sampleId,userId,reqBody){
  return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.newSaveMechMetalProp}/${sampleId}/${userId}`,reqBody, { responseType: 'text' }).pipe(map((response: any) => response));
} 
  //End Sample Mech Properties Apis

  //Start of NDT Screen APIs
  public getScheduleIds(){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getScheduleIdList}`).pipe(map((response: any) => response));
  }

  public getHeatIds(scheduleId){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getHeatIdList}/${scheduleId}`).pipe(map((response: any) => response));
  }

  public getBundleIds(heatId){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getBundleIdList}/${heatId}`).pipe(map((response: any) => response));
  }

  public getNDTBundleDetails(heatId){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getNdtBundleDetails}/${heatId}`).pipe(map((response: any) => response));
  }

  public getTestTypeList(productType){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getTestTypeList}/${productType}`).pipe(map((response: any) => response));
  }
  
  //new api for dynamic NDT details on sampleId

  public getTestDetailsBySampleId(sampleId){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getTestDetailsBySampleId}/${sampleId}`).pipe(map((response: any) => response));
  }

  public saveNDTDetails(userId,reqBody){
    return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.saveNDTDetails}/${userId}`,reqBody, { responseType: 'text' }).pipe(map((response: any) => response));
  }

  public newSaveNDTDetails(sampleId,userId,reqBody){
    return this.http.put(`${this.baseUrl}${this.brmQaEndPoints.newSaveNDTDetails}/${sampleId}/${userId}`,reqBody, { responseType: 'text' }).pipe(map((response: any) => response));
  }
  //End of NDT Screen APIs

  //trial of sticker printing
  public getLocalImg(){
    return this.http.get(`${this.localBaseUrl}/imgResponse.json`);
  }

  public getStickerDetails(bundleId){
    return this.http.get(`${this.baseUrl}${this.brmQaEndPoints.getStickerPrintDetails}/${bundleId}`).pipe(map((response: any) => response));
  }

}
