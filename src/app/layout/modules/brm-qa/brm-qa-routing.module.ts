import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/shared/services/auth-guard.service';
import { BrmQaComponent } from './brm-qa.component';
import { BundleQualityClearanceComponent } from './components/bundle-quality-clearance/bundle-quality-clearance.component';
import { FinalMaterialBatchUpdateComponent } from './components/final-material-batch-update/final-material-batch-update.component';
import { SampleMechPropertyComponent } from './components/sample-mech-property/sample-mech-property.component';
import { StickerPrintingComponent } from './components/sticker-printing/sticker-printing.component';
import { TpiHoldReleaseComponent } from './components/tpi-hold-release/tpi-hold-release.component';
import { moduleIds, brmQAScreenIds } from 'src/assets/constants/MENU/menu-list';
import { NdtComponent } from './components/ndt/ndt.component';

const routes: Routes = [
  {
    path: '',
    component: BrmQaComponent,
    children: [
      {
        path: 'stickerPrinting',
        component: StickerPrintingComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM_QA,
          screenId: brmQAScreenIds.STICKER_PRINTING_SCRREN.id,
        },
      },
      {
        path: 'sampleMechProperty',
        component: SampleMechPropertyComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM_QA,
          screenId: brmQAScreenIds.SAMPLE_MECH_PROPERTY_SCREEN.id,
        },
      },
      {
        path: 'bundleQualityClearance',
        component: BundleQualityClearanceComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM_QA,
          screenId: brmQAScreenIds.BUNDLE_QUALITY_CLEARANCE_SCREEN.id,
        },
      },
      {
        path: 'tpiHoldRelease',
        component: TpiHoldReleaseComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM_QA,
          screenId: brmQAScreenIds.TPI_HOLD_RELEASE_SCREEN.id,
        },
      },
      {
        path: 'finalMaterialBatchUpdate',
        component: FinalMaterialBatchUpdateComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM_QA,
          screenId: brmQAScreenIds.FINAL_MATERIAL_BATCH_UPDATE_SCREEN.id,
        },
      },
      {
        path: 'ndt',
        component: NdtComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.BRM_QA,
          screenId: brmQAScreenIds.NDT.id,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BRMqaRoutingModule {}
