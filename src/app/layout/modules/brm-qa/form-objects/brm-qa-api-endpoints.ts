export class BrmqaEndPoints {
    // TPI HOLD RELEASE screen Apis
    public readonly filterCriteria = '/tpiRelease/filterCriteria'
    public readonly tpiHoldDetails = '/tpiRelease/tpiHoldDetails';
    public readonly save  = '/tpiRelease/save';
    public readonly release = '/tpiRelease/release'

     // end TPI HOLD RELEASE screen Apis

     // Start final material batch update
     public readonly getWorkCenterList ='/finalMaterialBatch/getMaterialUnit';
     public readonly getMaterialBatchDetails ='/finalMaterialBatch/getMaterialBatchDetails';
     public readonly saveFinalMaterialBatchDetails ='/finalMaterialBatch/saveFinalMaterialBatchDetails';
     // End final material batch update

     //Start Bundle quality clearance Api's endpoints
     public readonly bundleQualityClearance ='/bundleQualityClearance/bundleQualityClearance';
     public readonly materialClassList = '/bundleQualityClearance/materialClassList';
     public readonly saveBundleQuality = '/bundleQualityClearance/save';
     public readonly releaseQuality='/bundleQualityClearance/release';
     public readonly validationFlag = '/bundleQualityClearance/getValidationFlag';
    //End Bundle quality clearance Api's endpoints

    //Start Sample Mech Properties Api's endpoints
    public readonly sampleMechProductList ='/sampleMechProperty/getSampleMechProductList';
    public readonly sampleMechStatusList ='/sampleMechProperty/getSampleMechStatusList';
    public readonly sampleMechDetails ='/sampleMechProperty/getMechDetails';
    public readonly sampleMetalPropDetails ='/sampleMechProperty/getMetalPropDetails';
    public readonly saveMechPropDetails = '/sampleMechProperty/saveMechPropDetails';
    public readonly saveMetalPropDetails = '/sampleMechProperty/saveMetalPropDetails';
    public readonly getSampleMechDetails ='/sampleMechProperty/getSampleMechDetails';
    public readonly getSampleMapping = '/sampleMechProperty/getSampleMapping';
    public readonly newSaveMechMetalProp = '/sampleMechProperty/saveAndValidateAllPropDetails'
    //End Sample Mech Properties Api's endpoints

    //Start of NDT Screen APIs
    public readonly getScheduleIdList = '/getScheduleIdList';
    public readonly getHeatIdList = '/getHeatIdList';
    public readonly getBundleIdList = '/getBundleIdList';
    public readonly getNdtBundleDetails = '/getNdtBundleDetails';
    public readonly getTestTypeList = '/getTestTypeList';
    public readonly saveNDTDetails = '/saveNdtBundleDetails';
    public readonly getTestDetailsBySampleId = '/getNdtTestDetails';
    public readonly newSaveNDTDetails = '/saveAndValidateNdtPropDetails';
    //End of NDT Screen APIs

    //start of Sticker print
    public readonly getStickerPrintDetails = '/stickerPrint/getStickerPrintingDetails'
}
