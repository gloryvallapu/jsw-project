import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrmQaComponent } from './brm-qa.component';

describe('BrmQaComponent', () => {
  let component: BrmQaComponent;
  let fixture: ComponentFixture<BrmQaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrmQaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrmQaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
