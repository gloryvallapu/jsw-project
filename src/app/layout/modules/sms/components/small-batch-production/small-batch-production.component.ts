import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SmallBatchProductionForm } from '../../form-objects/sms-form-objects';
import { ColumnType, LovCodes, TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { SmsService } from '../../services/sms.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import {
  JswCoreService,
  JswFormComponent,
} from 'projects/jsw-core/src/public-api';
import { Table } from 'primeng/table';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-small-batch-production',
  templateUrl: './small-batch-production.component.html',
  styleUrls: ['./small-batch-production.component.css'],
})
export class SmallBatchProductionComponent implements OnInit {
  formObject: any;
  viewType = ComponentType;
  tableType: any;
  columns: ITableHeader[] = [];
  columns1: ITableHeader[] = [];
  BatchStockDetailsList: any = [];
  public smallBatchList: any = [];
  public selectedGradeProperties: any;
  public columnType = ColumnType;
  public unModifiedSizes: any = [];
  public clonedProducts: { [s: string]: any } = {};
  wcName: any;
  headId: any;
  selectedUnit: any;
  userEnteredLength: any = [];
  public cnt: any;
  @ViewChild('dt') table: Table;
  public unModifiedWorkCenters: any = [];
  totalLength: any;
  oncePieceWeight: any;
  comefromNewRow: boolean = false;
  public requestBody: any;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isFlCrViewOnly: boolean= false;
  public isFlCrRtrv: boolean= false;
  public isFlCrRst: boolean = false;
  public batchStkDetails: boolean = false;
  public isSmalProdDetails: boolean = false;
  public isSmalProdDetailsSave: boolean = false;
  public isSmalProdDetailsAdd: boolean = false;
  public smallBatchLimit:boolean=false;
  public deleteFlag: boolean=false;
  public validations:any={
    "lenWtValidationFlag": "string",
    "maxSmallBatchValidationFlag": "string"
  }
  public formRef: NgForm;
  public tableRef: any;
  public tdClass ="height-max-content pt-0 pb-0";
  public totalCount: any;
  public generatedBatchIds=[];

  public weightDistrubtion: any;

  constructor(
    public smallBatchProductionForm: SmallBatchProductionForm,
    public sharedService: SharedService,
    public smsService: SmsService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,public authService: AuthService
  ) {}

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.SMS_Operation)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.smsOperationScreenIds.Small_Batch_Production_Screen.id)[0];
    this.isFlCrRtrv = this.sharedService.isButtonVisible(true, menuConstants.smallBatchProductionSectionIds.Filter_Criteria.buttons.retrive, menuConstants.smallBatchProductionSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRst = this.sharedService.isButtonVisible(true, menuConstants.smallBatchProductionSectionIds.Filter_Criteria.buttons.reset, menuConstants.smallBatchProductionSectionIds.Filter_Criteria.id, this.screenStructure);
    this.batchStkDetails = this.sharedService.isSectionVisible(menuConstants.smallBatchProductionSectionIds.batchStock_Details.id, this.screenStructure);
    this.isSmalProdDetailsSave = this.sharedService.isButtonVisible(true, menuConstants.smallBatchProductionSectionIds.small_batch_prod.buttons.save, menuConstants.smallBatchProductionSectionIds.small_batch_prod.id, this.screenStructure);
    this.isSmalProdDetailsAdd = this.sharedService.isButtonVisible(true, menuConstants.smallBatchProductionSectionIds.small_batch_prod.buttons.add, menuConstants.smallBatchProductionSectionIds.small_batch_prod.id, this.screenStructure);
    this.isFlCrViewOnly = this.sharedService.isSectionVisible(menuConstants.smallBatchProductionSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isSmalProdDetails = this.sharedService.isSectionVisible(menuConstants.smallBatchProductionSectionIds.small_batch_prod.id, this.screenStructure);

  }

  ngOnInit(): void {
    this.getValidationFlag();
    this.screenRightsCheck()
    this.smallBatchProdFilter();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject =JSON.parse(JSON.stringify( this.smallBatchProductionForm.smallBatch));
    this.columns = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.radio,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'salesOrder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Product Type',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'productSize',
        header: 'Product Size ',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      // {
      //   field: 'thickness',
      //   header: 'Thickness',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'width',
      //   header: 'Width ',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      {
        field: 'length',
        header: 'Length(mm)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Weight(tons)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
    ];
    this.columns1 = [
      {
        field: 'batchId',
        header: 'Small Batch ID',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'parentBatch',
        header: 'Parent Batch',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Product Type',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'productSize',
        header: 'Product Size',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'heatNo',
        header: 'Heat No',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'lengths',
        header: 'Length(mm)',
        columnType: ColumnType.number,
        width: '100px',
        sortFieldName: '',
      },
      // {
      //   field: 'width ',
      //   header: 'Width ',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'thickness',
      //   header: 'Thickness',
      //   columnType: ColumnType.string,
      //   width: '120px',
      //   sortFieldName: '',
      // },
      {
        field: 'weight',
        header: 'Weight(tons)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'SAPSendStatus',
        header: 'SAP Send Status',
        columnType: ColumnType.string,
        width: '140px',
        sortFieldName: '',
      },
      // {
      //   field: 'action',
      //   header: 'Action',
      //   columnType: ColumnType.action,
      //   width: '120px',
      //   sortFieldName: '',
      // },
    ];
  }

  public smallBatchProdFilter() {
    this.smsService.smallBatchProdFilter().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'wcName'
      )[0].list = data.map((x: any) => {
        return { displayName: x.wcName, modelValue: x.wcName , heatList:x.headIdList};
      });
    });
  }

  public getValidationFlag(){
    this.smsService.getValidationFlag().subscribe((data)=>{
      this.validations = data;
    })
  }

  public dropdownChangeEvent(event) {
    if (event.ref_key == 'wcName') {
      this.selectedUnit = event.value;
      let heatList =  this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'wcName'
      )[0].list.filter((y:any)=> y.modelValue == event.value)[0].heatList.map((x:any)=>{
        return {
          displayName: x, modelValue: x
        }
      })
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'heatNo'
      )[0].list = heatList
    }
  }

  public filterDataByUnit() {
    this.totalCount = null;
    this.smallBatchList = [];
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    this.smsService
      .getBatchProductionDetails(formData[0].value, formData[1].value)
      .subscribe((Response) => {
        Response.map((ele: any) => {
          ele.submitted = false;
          //ele.disabled = false;
          ele.srNo = this.sharedService.generateUniqueId();
          ele.parentBatch = ele.batchId;
          return ele;
        });
        this.unModifiedWorkCenters = JSON.parse(JSON.stringify(Response));
        this.BatchStockDetailsList = Response;
      });
      //22-11
    this.smsService.getParentChildBatchDetails(formData[0].value, formData[1].value).subscribe((response: any)=>{
      if(response){
        this.totalCount = response;
      }
    })
    //
  }

  public resetFilter() {
    //this.jswComponent.resetForm(this.formObject);
    this.smallBatchList=[];
    this.BatchStockDetailsList=[];
  }

  public getRowDetails(event) {
    this.deleteFlag=false;
    //10-11
    // this.smsService.getParentChildBatchDetails(event.prodWc,event.heatId).subscribe((response: any)=>{
    //   if(response){
    //     this.totalCount = response;
    //   }
    // })
    //
    this.smallBatchList = [];
    this.selectedGradeProperties = event;
    this.selectedGradeProperties.length=(this.selectedGradeProperties.length)
    // ?(this.selectedGradeProperties.length)/1000:0;
    this.selectedGradeProperties.density=(this.selectedGradeProperties.density)
    // ?(this.selectedGradeProperties.density)/1000:0;
    this.totalLength = this.selectedGradeProperties.length;
    this.cnt = 0;
    this.oncePieceWeight = Number(
      (this.selectedGradeProperties.length *
        this.selectedGradeProperties.density) /
        1000
    );
    //22-11
    this.weightDistrubtion = this.selectedGradeProperties.weight / this.selectedGradeProperties.length;
    //
    this.smsService.getSmallBatchIds(this.selectedGradeProperties.heatId, 2).subscribe((response: any)=>{
      if(response){
        this.generatedBatchIds = response;
        if (this.selectedGradeProperties.length % 2 == 0 && this.generatedBatchIds) {
          for (var i = 0; i < 2; i++) {
            this.cnt = i + 1;
            this.smallBatchList.push({
              //batchId: this.selectedGradeProperties.batchId + '_' + this.cnt,
              batchId: this.generatedBatchIds[i],
              parentBatch: this.selectedGradeProperties.batchId,
              srNo: this.sharedService.generateUniqueId(),
              productType: this.selectedGradeProperties.productType,
              productSize : this.selectedGradeProperties.productSize,
              heatNo: this.selectedGradeProperties.heatId,
              grade: this.selectedGradeProperties.grade,
              lengths: this.selectedGradeProperties.length / 2,
              width: this.selectedGradeProperties.width,
              thickness: this.selectedGradeProperties.thickness,
              // weight:(((this.selectedGradeProperties.length/2)/1000) *
              // (this.selectedGradeProperties.density /1000)),
              weight:((this.selectedGradeProperties.length/2) * this.weightDistrubtion),
              //weight:((this.selectedGradeProperties.length / 2) * this.selectedGradeProperties.density) /1000,
              SAPSendStatus: this.selectedGradeProperties.sapSendStatus,
                     });
          }
        }
      }
    })

    // if (this.selectedGradeProperties.length % 2 == 0 && this.generatedBatchIds) {
    //   for (var i = 0; i < 2; i++) {
    //     this.cnt = i + 1;
    //     this.smallBatchList.push({
    //       //batchId: this.selectedGradeProperties.batchId + '_' + this.cnt,
    //       batchId: this.generatedBatchIds[i],
    //       parentBatch: this.selectedGradeProperties.batchId,
    //       srNo: this.sharedService.generateUniqueId(),
    //       productType: this.selectedGradeProperties.productType,
    //       productSize : this.selectedGradeProperties.productSize,
    //       heatNo: this.selectedGradeProperties.heatId,
    //       grade: this.selectedGradeProperties.grade,
    //       lengths: this.selectedGradeProperties.length / 2,
    //       width: this.selectedGradeProperties.width,
    //       thickness: this.selectedGradeProperties.thickness,
    //       weight:((((this.selectedGradeProperties.length/2)/1000) *
    //       this.selectedGradeProperties.density) /
    //     1000)/2,
    //       //weight:((this.selectedGradeProperties.length / 2) * this.selectedGradeProperties.density) /1000,
    //       SAPSendStatus: this.selectedGradeProperties.sapSendStatus,
    //              });
    //   }
    // }
  }

public deleteRecord(){
  let latestRowIndex=this.smallBatchList.length
   if (latestRowIndex > 2) {
    this.deleteFlag=false;
  this.smallBatchList.splice(0, 1)
  }
  else{
    this.deleteFlag=true;
  }
  var remainingSum = Number(this.totalLength) / Number(this.smallBatchList.length);
for (var i = 0; i < this.smallBatchList.length; i++) {
  this.smallBatchList[i].lengths = remainingSum;
  // this.smallBatchList[i].weight =
  //   (((this.smallBatchList[i].lengths/this.smallBatchList.length)/1000) *
  //     this.selectedGradeProperties.density) /1000;

  //22-11
      this.smallBatchList[i].weight = (this.weightDistrubtion * this.smallBatchList[i].lengths);
}
}

//10-11
public generateNewIds(){
  this.smsService.getSmallBatchIds(this.selectedGradeProperties.heatId,this.smallBatchList.length).subscribe((response: any)=>{
    this.generatedBatchIds = response;
    this.generatedBatchIds.forEach((element, index) => {
      this.smallBatchList[index].batchId = response[index]
    });
  })
}
//

  public newRow() {
 if(this.validations.maxSmallBatchValidationFlag=="Y"){
    if(this.smallBatchList.length>=9){
      this.smallBatchLimit=true;
    }
    else{
      this.smallBatchLimit=false;
    }
  }else{
    this.smallBatchLimit=false;
  }
    this.comefromNewRow = true;
    this.deleteFlag=false;
    var lengthCount = Number(this.smallBatchList.length);
    return {
      srNo: this.sharedService.generateUniqueId(),
      batchId: this.generatedBatchIds[Number(lengthCount)],
       // this.selectedGradeProperties.batchId + '_' + Number(lengthCount + 1),
      parentBatch: this.selectedGradeProperties.batchId,
      productType: this.selectedGradeProperties.productType,
      productSize: this.selectedGradeProperties.productSize,
      heatNo: this.selectedGradeProperties.heatId,
      grade: this.selectedGradeProperties.grade,
      lengths: '',
      density: this.selectedGradeProperties.density,
      width: this.selectedGradeProperties.width,
      thickness: this.selectedGradeProperties.thickness,
      //weight: this.selectedGradeProperties.weight,
      weight:0,
      SAPSendStatus: this.selectedGradeProperties.sapSendStatus,
    };
  }


  public onRowEditInit(columns1: any) {
    columns1.submitted = false;
    if (this.unModifiedSizes.find((s) => columns1.srNo == s.srNo)) {
      this.clonedProducts[columns1.srNo] = { ...columns1 };
    }
  }

  public onRowEditSave(columns1: any, rowIndex: number) {
    columns1.submitted = true;
    this.userEnteredLength = Number(columns1.lengths);
    for (var i = 0; i < this.smallBatchList.length; i++) {
      if (rowIndex != i) {
        this.userEnteredLength += this.smallBatchList[i].lengths;
      }
    }
    if (
      !columns1.lengths ||
      !columns1.sizeCode ||
      !columns1.sizeDesc ||
      !columns1.status
    ) {
      this.table.initRowEdit(columns1);
      return;
    }
  }

  public onRowEditCancel(columns1: any, index: number) {
    columns1.submitted = false;
    let sIndex = this.unModifiedSizes.findIndex((s) => columns1.srNo == s.srNo);
    if (sIndex == -1) {
      this.smallBatchList = this.smallBatchList.filter(
        (ele) => ele.srNo != columns1.srNo
      );
      // var remainingSum =
      //   Number(this.totalLength) / Number(this.smallBatchList.length);
      // for (var i = 0; i < this.smallBatchList.length; i++) {
      //   this.smallBatchList[i].lengths = remainingSum;
      //   this.smallBatchList[i].weight =
      //     (this.smallBatchList[i].lengths *
      //       this.selectedGradeProperties.density) /
      //     1000;
      // }
     } else {
    //   this.smallBatchList[index] = this.clonedProducts[columns1.srNo];
    //   delete this.clonedProducts[columns1.srNo];
    // }
  }
}

  public enableEditMode(rowData) {
    if (!this.table.isRowEditing(rowData)) {
      this.table.initRowEdit(rowData);
    }
  }

  public validateWeight(columns1, index) {
    //columns1.weight = columns1.lengths * this.oncePieceWeight;
    columns1.weight = 0;
    let i = 0;
    let sum = 0;
    for (i = 0; i < this.smallBatchList.length; i++) {
      sum = sum + Number(this.smallBatchList[i].lengths);
    }
    if (columns1.lengths > this.totalLength) {
      columns1.actualFGError = true;
    } else {
      var remainingSum;
      this.smallBatchList[index].lengths = columns1.lengths;
      remainingSum =
        Number(this.totalLength - columns1.lengths) /
        Number(this.smallBatchList.length - 1);
      for (i = 0; i < this.smallBatchList.length; i++) {
        if (i != index) {
          this.smallBatchList[i].lengths = remainingSum;
        }
        // this.smallBatchList[i].weight = (((this.selectedGradeProperties.weight/this.smallBatchList.length)/1000) * this.selectedGradeProperties.density) /1000;
        this.smallBatchList[i].weight = (this.weightDistrubtion * this.smallBatchList[i].lengths);
        this.smallBatchList[i].weight = parseFloat(this.smallBatchList[i].weight).toFixed(4);
      }
    }
  }

  public smallBatchSave(): any {
    this.requestBody = this.smallBatchList.map((x: any) => {
      return {
        batchId: x.batchId,
        parentBatch: x.parentBatch,
        productType: x.productType,
        productSize : x.productSize,
        heatId: x.heatId,
        grade: x.grade,
        length: x.lengths,
        density: x.density,
        width: x.width,
        thickness: x.thickness,
        weight: x.weight,
        SAPSendStatus: x.sapSendStatus,
      };
    });
    this.smsService
      .updateCreatedSmallBatches(
        this.sharedService.loggedInUserDetails.userId,
        this.requestBody
      )
      .subscribe((Response) => {
        this.smallBatchList = [];
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          { message: 'Small Batch Production Successful' }
        );
        this.filterDataByUnit();
      });
  }

  public getFormRef(event){
    this.formRef = event;
  }
  public getTableRef(event){
    this.tableRef = event;
  }
  public filterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }
}
