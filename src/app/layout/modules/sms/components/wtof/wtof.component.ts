import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { WtoFScreenForm } from '../../form-objects/sms-form-objects';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { SmsService } from '../../services/sms.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import {
  JswCoreService,
  JswFormComponent,
} from 'projects/jsw-core/src/public-api';
import { Subject } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-wtof',
  templateUrl: './wtof.component.html',
  styleUrls: ['./wtof.component.css']
})
export class WtofComponent  implements OnInit {
  public formObject: any;
  public viewType = ComponentType;
  public tableType: any;
  public columns: ITableHeader[] = [];
  public BatchDetailsList: any = [];
  public columnType = ColumnType;
  public selectedBatches: any = [];
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isFlCrViewOnly:boolean= false;
  public isFlCrRtrv:boolean= false;
  public isFlCrRst:boolean = false;
  public batchDetails:boolean = false;
  public batchDetailsdispatch:boolean = false;
  public formRef: NgForm;
  
  constructor(
    public WtoFForm: WtoFScreenForm,
    public sharedService: SharedService,
    public smsService: SmsService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public authService: AuthService
  ) {}

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.SMS_Operation)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.smsOperationScreenIds.WtoF_Screen.id)[0];
    this.isFlCrRtrv = this.sharedService.isButtonVisible(true, menuConstants.wTofSectionIds.Filter_Criteria.buttons.retrive, menuConstants.wTofSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRst = this.sharedService.isButtonVisible(true, menuConstants.wTofSectionIds.Filter_Criteria.buttons.reset, menuConstants.wTofSectionIds.Filter_Criteria.id, this.screenStructure);
    this.batchDetailsdispatch = this.sharedService.isButtonVisible(true, menuConstants.wTofSectionIds.BatchDetails.buttons.dispatch, menuConstants.wTofSectionIds.BatchDetails.id, this.screenStructure);
    this.batchDetails = this.sharedService.isSectionVisible(menuConstants.wTofSectionIds.BatchDetails.id, this.screenStructure);
    this.isFlCrViewOnly = this.sharedService.isSectionVisible(menuConstants.wTofSectionIds.Filter_Criteria.id, this.screenStructure);
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.wipToFinalFilter();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject =JSON.parse(JSON.stringify( this.WtoFForm.WtoFFilterForm));
    this.columns = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '20px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '40px',
        sortFieldName: '',
      },
      {
        field: 'salesOrder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '60px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '54px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '40px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Prod Type',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'productSize', // add new column
        header: 'Prod Size',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Len(mm)',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Wgt(tons)',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'batchStatus',
        header: 'Status',
        columnType: ColumnType.string,
        width: '70px',
        sortFieldName: '',
      },
      // {
      //   field: 'dia',
      //   header: 'Diameter',
      //   columnType: ColumnType.string,
      //   width: '50px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'thickness',
      //   header: 'Thickness',
      //   columnType: ColumnType.string,
      //   width: '50px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'width',
      //   header: 'Width ',
      //   columnType: ColumnType.string,
      //   width: '50px',
      //   sortFieldName: '',
      // },

      {
        field: 'customerName',
        header: 'Customer Name',
        columnType: ColumnType.string,
        width: '70px',
        sortFieldName: '',
      },
      {
        field: 'productionRemark',
        header: 'Production Remark',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      }
    ];
  }

  public wipToFinalFilter() {
    this.smsService.wipToFinalFilter().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'wcName'
      )[0].list = data.map((x: any) => {
        return { displayName: x.wcName, modelValue: x.wcName,
          heatIdList:x.headIdList };
      });
    });
  }

  public dropdownChangeEvent(event) {
    if (event.ref_key == 'wcName' && event.value != null) {
      let heatIdListByUnit = this.formObject.formFields.filter(ele => ele.ref_key == 'wcName')[0].list.filter((x: any) => x.modelValue == event.value)[0]
      this.formObject.formFields.filter(ele => ele.ref_key == 'heatNo')[0].list = heatIdListByUnit.heatIdList.map((x: any) => {
        return {
          displayName: x,
          modelValue: x
        }
      });
    }
  }

  public filterDataByUnit() {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    if(formData && formData.length){
      this.smsService
      .getBatchDetailsList(
        this.formObject.formFields.filter(ele => ele.ref_key == 'wcName')[0].value,
        this.formObject.formFields.filter(ele => ele.ref_key == 'heatNo')[0].value
      ).subscribe((Response) => {
        this.BatchDetailsList = Response;
      });
    }
  }

  public dispatch(){
    if(!this.selectedBatches.length){
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'Please select batches to dispatch'});
      return;
    }
    let batchIds = this.selectedBatches.map(element => {
      return element.batchId;
    });
    this.smsService.dispatchBatches(this.sharedService.loggedInUserDetails.userId, batchIds).subscribe(
      data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Selected batches dispatched successfully'});
        this.filterDataByUnit();
        this.selectedBatches = [];
        this.clearSelection.next(true);
      }
    )
  }

  public resetFilter() {
    //this.jswComponent.resetForm(this.formObject);
    this.BatchDetailsList=[];
  }

  public getSelectedRecords(data){
    this.selectedBatches = data;
  }

  public getFormRef(event){
    this.formRef = event;
  }


}
