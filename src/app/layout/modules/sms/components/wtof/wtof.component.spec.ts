import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { WtoFScreenForm } from '../../form-objects/sms-form-objects';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { SmsService } from '../../services/sms.service';
import {JswCoreService,JswFormComponent,} from 'projects/jsw-core/src/public-api';
import { Subject } from 'rxjs';
import { WtofComponent } from './wtof.component';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { SmsEndPoints } from '../../form-objects/sms-api-endpoint';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { API_Constants } from 'src/app/shared/constants/api-constants';

describe('WtofComponent', () => {
  let component: WtofComponent;
  let fixture: ComponentFixture<WtofComponent>;
  let sharedService: SharedService
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let smsService: SmsService
  let commonService: CommonApiService

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WtofComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        SmsService,
        CommonApiService,
        JswFormComponent,
        SmsEndPoints,
        API_Constants,
        CommonAPIEndPoints,WtoFScreenForm,
        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WtofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
