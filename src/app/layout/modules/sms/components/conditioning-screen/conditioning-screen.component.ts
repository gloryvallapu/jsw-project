import { Component, OnInit } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ConditioningScreenForms } from '../../form-objects/sms-form-objects';
import { SmsService } from '../../services/sms.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-conditioning-screen',
  templateUrl: './conditioning-screen.component.html',
  styleUrls: ['./conditioning-screen.component.css'],
})
export class ConditioningScreenComponent implements OnInit {
  public noOfRows: any = 3;
  public columnType = ColumnType;
  public filterCriteria: any = [];
  public viewType = ComponentType;
  public originalProdcolumns: ITableHeader[] = [];
  public planDetailscolumns: ITableHeader[] = [];
  public conditioningDetailscolumns: ITableHeader[] = [];
  public balanceDetailsCols: ITableHeader[] = [];
  public conditionigHistoryCols: ITableHeader[] = [];
  public tableType: any;
  public originalProdList: any = [];
  public planProdList: any = [];
  public filterDetails: any = [];
  public selectedBatch: any = [];
  public conditioningTypes: any = [];
  public conditioningList: any = [];
  public conditioningHistoryList: any = [];
  public balanceDetails: any = [];
  public conditioningProcessedList: any = [];
  public holdCtr: number;
  public retriveDetails: any;
  public paginator: boolean = false;
  public finalizedConditioning: boolean = false;
  public sequenceCounter: number = 0;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isFlCrViewOnly: boolean = false;
  public isFlCrRtrv: boolean = false;
  public isFlCrRst: boolean = false;
  public isOrignlPlanDetails: boolean = false;
  public addConditioning: boolean = false;
  public isCondDetails: boolean = false;
  public isInsert: boolean = false;
  public isReset: boolean = false;
  public isSave: boolean = false;
  public isFinalize: boolean = false;
  public isBalanceDetails: boolean = false;
  public isPlanDetails: boolean = false;
  public isHistoryDetails: boolean = false;
  public formRef: NgForm;
  public openstartConditioningmodel: boolean = false;
  public minHeight: '200px';
  public finalized: boolean = false;
  public disableInsertBtn: boolean = false;

  constructor(
    public conditioningScreenForms: ConditioningScreenForms,
    public smsServices: SmsService,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.screenRightsCheck();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.filterCriteria = JSON.parse(
      JSON.stringify(this.conditioningScreenForms.filterCriteriaForms)
    );
    this.originalProdcolumns = this.createOriginalProdcolumns();
    this.planDetailscolumns = this.createplanDetailscolumns();
    this.conditioningDetailscolumns = this.createconditioningDetailscolumns();
    this.balanceDetailsCols = this.createBalanceDetailsCols();
    this.conditionigHistoryCols = this.createConditionigHistoryCols();
    this.getFilterCriteriaDropDownLists(); //fetch dropDwonDetails
    this.getConditioningTypes(); // fetch conditioningTypes
  }

  public screenRightsCheck() {
    let userRights = this.authService
      .getUserRights()
      .filter(
        (ele) => ele.screenId == menuConstants.moduleIds.SMS_Operation
      )[0];
    this.screenStructure = userRights.subScreens.filter(
      (screen) =>
        screen.screenId ==
        menuConstants.smsOperationScreenIds.Conditioning_Screen.id
    )[0];
    this.isFlCrViewOnly = this.sharedService.isSectionVisible(
      menuConstants.conditioningSectionIds.Filter_Criteria.id,
      this.screenStructure
    );
    this.isFlCrRtrv = this.sharedService.isButtonVisible(
      true,
      menuConstants.conditioningSectionIds.Filter_Criteria.buttons.retrive,
      menuConstants.conditioningSectionIds.Filter_Criteria.id,
      this.screenStructure
    );
    this.isFlCrRst = this.sharedService.isButtonVisible(
      true,
      menuConstants.conditioningSectionIds.Filter_Criteria.buttons.reset,
      menuConstants.conditioningSectionIds.Filter_Criteria.id,
      this.screenStructure
    );
    this.isOrignlPlanDetails = this.sharedService.isSectionVisible(
      menuConstants.conditioningSectionIds.Original_ProductDetails.id,
      this.screenStructure
    );
    this.addConditioning = this.sharedService.isButtonVisible(
      true,
      menuConstants.conditioningSectionIds.Original_ProductDetails.buttons
        .addConditioning,
      menuConstants.conditioningSectionIds.Original_ProductDetails.id,
      this.screenStructure
    );
    this.isCondDetails = this.sharedService.isSectionVisible(
      menuConstants.conditioningSectionIds.conditioning_Details.id,
      this.screenStructure
    );
    this.isInsert = this.sharedService.isButtonVisible(
      true,
      menuConstants.conditioningSectionIds.conditioning_Details.buttons.insert,
      menuConstants.conditioningSectionIds.conditioning_Details.id,
      this.screenStructure
    );
    this.isReset = this.sharedService.isButtonVisible(
      true,
      menuConstants.conditioningSectionIds.conditioning_Details.buttons.reset,
      menuConstants.conditioningSectionIds.conditioning_Details.id,
      this.screenStructure
    );
    this.isFinalize = this.sharedService.isButtonVisible(
      true,
      menuConstants.conditioningSectionIds.conditioning_Details.buttons
        .finalize,
      menuConstants.conditioningSectionIds.conditioning_Details.id,
      this.screenStructure
    );
    this.isSave = this.sharedService.isButtonVisible(
      true,
      menuConstants.conditioningSectionIds.conditioning_Details.buttons.save,
      menuConstants.conditioningSectionIds.conditioning_Details.id,
      this.screenStructure
    );
    this.isBalanceDetails = this.sharedService.isSectionVisible(
      menuConstants.conditioningSectionIds.Balance_Details.id,
      this.screenStructure
    );
    this.isPlanDetails = this.sharedService.isSectionVisible(
      menuConstants.conditioningSectionIds.Plan_Details.id,
      this.screenStructure
    );
    this.isHistoryDetails = this.sharedService.isSectionVisible(
      menuConstants.conditioningSectionIds.conditioning_History.id,
      this.screenStructure
    );
  }

  public reset() {
    //this.jswComponent.resetForm(this.filterCriteria);
    this.planProdList = [];
    this.originalProdList = [];
    this.conditioningList = [];
    this.conditioningHistoryList = [];
    this.balanceDetails = [];
  }

  public createOriginalProdcolumns(): any {
    return [
      {
        field: 'radio',
        header: 'Select ',
        columnType: ColumnType.radio,
        width: '60px',
        sortFieldName: '',
      },
      {
        field: 'productId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'soId',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Len(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Wgt(tons)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'sizeCode',
        header: 'Size',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      // {
      //   field: 'width',
      //   header: 'Width',
      //   columnType: ColumnType.string,
      //   width: '90px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'thickness',
      //   header: 'Thickness',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      {
        field: 'holdRemark',
        header: 'Hold Remark',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
    ];
  }

  public createplanDetailscolumns(): any {
    return [
      // {
      //   field: 'srNo',
      //   header: 'Sr No',
      //   columnType: ColumnType.string,
      //   width: '80px',
      //   sortFieldName: '',
      // },
      {
        field: 'dueDate',
        header: 'Due Date',
        columnType: ColumnType.date,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Plan Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'size',
        header: 'Dimension/Size',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'rollUnit',
        header: 'Roll Unit',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'lengthMin',
        header: 'Min Length(mm)',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'lengthMax',
        header: 'Max Length(mm)',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'batchQty',
        header: 'Batch Qty',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
    ];
  }

  public createconditioningDetailscolumns(): any {
    return [
      {
        field: 'checkbox',
        header: ' ID',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },

      {
        field: 'conditioningType',
        header: 'Conditioning Type',
        columnType: ColumnType.dropdown,
        width: '130px',
        list: this.conditioningTypes,
        sortFieldName: '',
      },
      {
        field: 'conditioningName',
        header: 'Conditioning Name',
        columnType: ColumnType.dropdown,
        width: '140px',
        list: [],
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Len(mm)',
        columnType: ColumnType.numericValidationField,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'width',
        header: 'Wdh(mm)',
        columnType: ColumnType.numericValidationField,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'thickness',
        header: 'Thik(mm)',
        columnType: ColumnType.numericValidationField,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'defaultWeight',
        header: 'Wgt(tons)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },

      {
        field: 'condRemark',
        header: 'Cond Remark',
        columnType: ColumnType.text,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'productId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'sizeCode',
        header: 'Size',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
    ];
  }

  public createConditionigHistoryCols(): any {
    // return [
    //   {
    //     field: 'productId',
    //     header: 'Product ID',
    //     columnType: ColumnType.string,
    //     width: '100px',
    //     sortFieldName: '',
    //   },
    //   {
    //     field: 'condName',
    //     header: 'Conditioning Name',
    //     columnType: ColumnType.string,
    //     width: '150px',
    //     sortFieldName: '',
    //   },
    //   {
    //     field: 'defaultLength',
    //     header: 'Length Loss(mm)',
    //     columnType: ColumnType.string,
    //     width: '180px',
    //     sortFieldName: '',
    //   },
    //   {
    //     field: 'defaultWidth',
    //     header: 'Width Loss(mm)',
    //     columnType: ColumnType.string,
    //     width: '180px',
    //     sortFieldName: '',
    //   },
    //   {
    //     field: 'defaultThickness',
    //     header: 'Thickness Loss(mm)',
    //     columnType: ColumnType.string,
    //     width: '200px',
    //     sortFieldName: '',
    //   },
    //   {
    //     field: 'weight',
    //     header: 'Weight Loss',
    //     columnType: ColumnType.string,
    //     width: '120px',
    //     sortFieldName: '',
    //   },
    //   {
    //     field: 'condRemark',
    //     header: 'Cond Remark',
    //     columnType: ColumnType.string,
    //     width: '130px',
    //     sortFieldName: '',
    //   },
    // ]
    return [
      // {
      //   field: 'conditioningType',
      //   header: 'Conditioning Type',
      //   columnType: ColumnType.string,
      //   width: '130px',
      //   list: this.conditioningTypes,
      //   sortFieldName: '',
      // },
      {
        field: 'condName',
        header: 'Conditioning Name',
        columnType: ColumnType.string,
        width: '140px',
        list: [],
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Len(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'width',
        header: 'Wdh(mm)',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'thickness',
        header: 'Thik(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'defaultWeight',
        header: 'Wgt(tons)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },

      {
        field: 'condRemark',
        header: 'Cond Remark',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'productId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'sizeCode',
        header: 'Size',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
    ];
  }

  public createBalanceDetailsCols(): any {
    return [
      {
        field: 'length',
        header: 'Length(mm)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'width',
        header: 'Width(mm)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },

      {
        field: 'thickness',
        header: 'Thickness(mm)',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },

      {
        field: 'defaultWeight',
        header: 'Weight(tons)',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
    ];
  }

  public getFilterCriteriaDropDownLists() {
    this.smsServices.getFilterCriteriaDropDownLists().subscribe((Response) => {
      this.filterDetails = Response.map((x: any) => {
        return {
          displayName: x.wcName,
          modelValue: x.wcName,
          smsProductBoList: x.smsProductBoList,
        };
      });
      this.filterCriteria.formFields.filter(
        (ele) => ele.ref_key == 'unitId'
      )[0].list = this.filterDetails;
    });
  }

  public dropdownChangeEvent(event) {
    if (event.ref_key == 'unitId' && event.value != null) {
      this.filterCriteria.formFields.filter(
        (ele) => ele.ref_key == 'productType'
      )[0].list = this.filterDetails
        .filter((x: any) => x.modelValue == event.value)[0]
        .smsProductBoList.map((y: any) => {
          return {
            displayName: y.productName,
            modelValue: y.productId,
            smsHeatIdList: y.smsHeatIdList == null ? [] : y.smsHeatIdList,
          };
        });
    }

    if (event.ref_key == 'productType' && event.value != null) {
      this.filterCriteria.formFields.filter(
        (ele) => ele.ref_key == 'heatId'
      )[0].list = event.list
        .filter((x: any) => x.modelValue == event.value)[0]
        .smsHeatIdList.map((y: any) => {
          return {
            displayName: y,
            modelValue: y,
          };
        });
    }
  }

  //new trial 9-11
  public assignDropDownData(data: any) {
    this.conditioningList.forEach((item) => {
      if (item.conNamelist == undefined || item.conNamelist.length == 0) {
        item.conNamelist = [];
        data.forEach((element) => {
          if (element.condType == item.condType) {
            let temp = {
              displayName: element.condName,
              modelValue: element.condName,
            };
            item.conNamelist.push(temp);
          }
        });
      }
    });
  }

  public fetchConditioningName(conditioningType, batchId) {
    this.smsServices
      .getconditioningName(conditioningType, batchId)
      .subscribe((Response): any => {
        this.assignDropDownData(Response);
        // setTimeout(() => {
        //   this.assignDropDownData(Response);
        // }, 1000);

        //   this.conditioningList.forEach(item => {
        //     item.conNamelist = []
        //     Response.forEach(element => {
        //       if(element.condType == item.condType ){
        //         let temp = {
        //             "displayName":element.condName,
        //             "modelValue":element.condName
        //         }
        //         item.conNamelist.push(temp)
        //       }

        //     });

        // });

        // this.conditioningList[0].conNamelist = Response.map((y: any) => {
        //   y.displayName = y.condName
        //   y.modelValue = y.condName
        //   return y
        // })
        // this.conditioningDetailscolumns.filter((x: any) => x.field == 'conditioningName')[0].list = Response.map((y: any) => {
        //   y.displayName = y.condName
        //   y.modelValue = y.condName
        //   return y
        // })
      });
  }

  public getPlanDetails(): any {
    this.disableInsertBtn = true;
    this.holdCtr = 0;
    this.finalizedConditioning = false;
    this.originalProdList = [];
    this.planProdList = [];
    this.balanceDetails = [];
    this.conditioningList = [];
    this.conditioningHistoryList = [];
    this.jswComponent.onSubmit(
      this.filterCriteria.formName,
      this.filterCriteria.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData == undefined) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please select required fields`,
        }
      );
    }

    this.smsServices
      .getPlanDetails(
        this.filterCriteria.formFields.filter(
          (ele) => ele.ref_key == 'heatId'
        )[0].value
      )
      .subscribe((Response) => {
        this.retriveDetails = Response;
        this.originalProdList = Response.smsProductOriginalDetailsList.map(
          (x: any) => {
            // x.changeColor = x.finalizedConditioning == true ? x.finalizedConditioning: false;
            //new change
            if (
              x.finalizedConditioning == false &&
              x.holdCtr != 0 &&
              x.qaHoldAfterFinalized == false
            ) {
              //x.changeColor = (x.finalizedConditioning == false && x.holdCtr !=0) ? true:false;
              x.changeColor = true;
              x.color = 'SkyBlue';
            }
            //9-11
            //  else if(x.finalizedConditioning == true){
            //   //x.changeColor = (x.finalizedConditioning == false && x.holdCtr !=0) ? true:false;
            //   x.changeColor = true;
            //   x.color = 'lightgreen';
            //  }
            else if (x.holdCtr == 0 && x.qaHoldAfterFinalized == true) {
              x.changeColor = true;
              x.color = 'lightgreen';
            } else if (x.holdCtr == 0 && x.qaHoldAfterFinalized == false) {
              x.changeColor = false;
              x.color = '';
            }

            // x.changeColor = (x.finalizedConditioning == true && x.holdCtr ==0) ? true:false;
            // x.color = 'SkyBlue';
            return x;
          }
        );
        // this.holdCtr = Response.holdCtr
        // this.finalizedConditioning=Response.finalizedConditioning
        // this.originalProdList.push(this.retriveDetails.smsProductOriginalDetails);
        // Response.smsProductPlanDetails.srNo = 1
        // Response.smsProductPlanDetails.dueDate = new Date(Response.smsProductPlanDetails.dueDate)
        Response.smsProductPlanDetails.batchQty =
          Response.smsProductOriginalDetailsList.length;
        this.planProdList.push(Response.smsProductPlanDetails);
        // this.originalProdList[0].disabledThickness = true;
        // this.originalProdList[0].disabledLength = true;
        // this.originalProdList[0].disabledWidth = true;
        // this.originalProdList[0].conditioningType = null
        // this.originalProdList[0].conditioningName = null
        // // this.conditioningList.push(this.originalProdList[0]);
        //this.getSavedConditioningDetails();
      });
  }

  public getConditioningTypes() {
    this.smsServices.getConditioningTypes().subscribe((Response) => {
      this.conditioningTypes = Response;
      this.conditioningDetailscolumns.filter(
        (x: any) => x.field == 'conditioningType'
      )[0].list = Response.map((y: any) => {
        return {
          displayName: y.valueDescription,
          modelValue: y.lovValueId,
        };
      });
    });
  }

  public getRowDetails(event): any {
    this.disableInsertBtn = false;
    this.selectedBatch = event;
    this.holdCtr = event.holdCtr;
    this.getSavedConditioningDetails(); // confirm the operation
    this.getConditioningHistoryDetails(event.batchId);

    // commenting below block as no need to show popup --> 15th Feb 22
    /* if(event.qaHoldAfterFinalized == true){
      //9-11
      this.disableInsertBtn = true;
      this.getHistoryConditioningDetails();
      this.openstartConditioningmodel = true
      return true
    }
    else{
     // this.getPlanDetails();
      this.getSavedConditioningDetails();
    }
    */

    // no need to check hold counter --> 14th Feb 22
    // if(this.holdCtr == 0){
    //   this.conditioningList = [];
    //   this.conditioningHistoryList = [];
    //   this.addConditioningToBatch(event);
    // }
    //this.addConditioningToBatch(event);
  }

  public closeGradeChangePopup() {
    this.openstartConditioningmodel = false;
  }

  public startConditioning() {
    this.disableInsertBtn = false;
    this.addConditioningToBatch(this.selectedBatch);
  }

  public addConditioningToBatch(event): any {
    if (event.batchId == null) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please fetch plan details`,
        }
      );
    }

    this.smsServices
      .addConditioning(
        event.batchId,
        this.sharedService.loggedInUserDetails.userId
      )
      .subscribe((Response) => {
        this.holdCtr = Response;
        this.finalizedConditioning = false;
        this.openstartConditioningmodel = false;
        let index = this.originalProdList.findIndex(
          (x: any) => x.batchId == event.batchId
        );
        this.originalProdList[index].changeColor = true;
        this.originalProdList[index].color = 'SkyBlue';
      });
  }

  public getConditioningRow(event) {
    this.conditioningProcessedList = event;
  }

  public saveConditioningDetails(): any {
    if (this.conditioningList.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please select conditioning details item(s)`,
        }
      );
    }
    let conditioningTypeValidations = this.conditioningList.filter(
      (x: any) => x.conditioningType == null
    );
    let conditioningNameValidations = this.conditioningList.filter(
      (x: any) => x.conditioningName == null
    );
    let thicknessValidations = this.conditioningList.filter(
      (x: any) => x.thicknessError == true
    );
    let widthValidations = this.conditioningList.filter(
      (x: any) => x.widthError == true
    );

    if (widthValidations.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please enter valid width `,
        }
      );
    }
    if (thicknessValidations.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please enter valid thickness value`,
        }
      );
    }
    if (conditioningTypeValidations.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please select Conditioning Type for Unselected fields`,
        }
      );
    }

    if (conditioningNameValidations.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please select Conditioning Name for Unselected fields`,
        }
      );
    }

    let saveConditioningList = this.conditioningList.filter(
      (x: any) => (x.isChanged = 'updated')
    );
    let requestBody = saveConditioningList.map((x: any) => {
      let lengthCheckFlag;
      if (x.lengthEditFlag == 'Y') {
        lengthCheckFlag = 'Y';
      } else {
        lengthCheckFlag = 'N';
      }
      return {
        batchId: this.selectedBatch.batchId,
        condCtr: x.condCtr,
        condDesc: x.condDesc,
        condName: x.conditioningName,
        condRemark: x.condRemark,
        condType: x.conditioningType,
        defaultLength: x.length,
        defaultThickness: x.thickness,
        defaultWidth: x.width,
        defaultWeight: x.defaultWeight,
        dia: x.dia,
        holdCtr: x.holdCtr,
        length: x.length,
        lengthEditFlag: x.lengthEditFlag,
        thickness: x.thickness,
        thicknessEditFlag: x.thicknessEditFlag,
        weight: x.weight,
        width: x.width,
        widthEditFlag: x.widthEditFlag,
        weightFlag: x.weightFlag,
        //added as requested by rajashree
        thicknessFlag: x.thicknessFlag,
        widthFlag: x.widthFlag,
        lengthFlag: lengthCheckFlag,
      };
    });

    this.smsServices
      .saveConditioningItmes(
        this.sharedService.loggedInUserDetails.userId,
        requestBody
      )
      .subscribe((Response) => {
        this.conditioningProcessedList = [];
        this.getSavedConditioningDetails();
        this.getConditioningHistoryDetails(this.selectedBatch.batchId);
      });
  }

  public gerRowOnclick(value, field): any {
    if (this.conditioningProcessedList.length > 1) {
      // this.conditioningProcessedList.splice(this.conditioningProcessedList.length -2, 1);
      this.conditioningProcessedList = [];
      return this.warningAlertMessage(
        'More Than One can not perfom Delete Action'
      );
    }
  }

  public deleteSelectedCondi(): any {
    if (this.conditioningProcessedList.length == 0) {
      return this.warningAlertMessage(
        'Please Select Conditioning which want to be delete'
      );
    }
    this.smsServices
      .deleteSelectedCondi(this.conditioningProcessedList[0])
      .subscribe((Response) => {
        this.successAlertMessage(Response);
        this.conditioningProcessedList = [];
        this.getSavedConditioningDetails();
      });
  }

  public warningAlertMessage(msg) {
    this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Warning,
      {
        message: `${msg}`,
      }
    );
  }

  public successAlertMessage(msg) {
    this.sharedService.displayToastrMessage(
      this.sharedService.toastType.Success,
      {
        message: `${msg}`,
      }
    );
  }

  public delete(): any {
    this.deleteSelectedCondi();
    // let index = this.conditioningList.length -1;
    // this.conditioningList.splice(index, 1)
  }

  public getConditioningHistoryDetails(batchId: any) {
    this.smsServices
      .getConditioningHistoryDetails(batchId)
      .subscribe((Response) => {
        this.conditioningHistoryList = Response;
      });
  }

  public finalize(): any {
    if (this.conditioningList.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please fetch Conditioning details before Finalize`,
        }
      );
    }
    let saveValidations = this.conditioningList.filter(
      (x: any) => x.condCtr == 0
    );
    if (saveValidations.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please save ongoing conditioning details before Finalize`,
        }
      );
    }
    this.smsServices
      .finalizeConditioning(
        this.sharedService.loggedInUserDetails.userId,
        this.selectedBatch.batchId
      )
      .subscribe((Response) => {
        this.finalizedConditioning = true;
        this.getPlanDetails();
        // this.getConditioningHistoryDetails(this.selectedBatch.batchId); // confirm with Mangesh if we have to remove this call
        // this.getFiinalizezedConditioningDetails();
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          {
            message: `${Response}`,
          }
        );
      });
  }

  public getBalanceDetails(): any {
    this.balanceDetails = [];
    this.smsServices.getBalanceDetails().subscribe((Response) => {
      this.balanceDetails.push(Response);
    });
  }

  public onTableDropdownChange(col, condType, index): any {
    if (col.field == 'conditioningType') {
      this.conditioningList[index].conNamelist = [];
      this.smsServices
        .getconditioningName(
          condType.conditioningType,
          this.selectedBatch.batchId
        )
        .subscribe((Response): any => {
          if (Response.length == 0) {
            condType.conditioningName = null;
            this.conditioningDetailscolumns.filter(
              (x: any) => x.field == 'conditioningName'
            )[0].list = [];
            return this.sharedService.displayToastrMessage(
              this.sharedService.toastType.Warning,
              {
                message: `Selected Conditioning type does not have Conditioning Name Records`,
              }
            );
          }
          // this.conditioningDetailscolumns.filter((x: any) => x.field == 'conditioningName')[0].list
          // = Response.map((y: any) => {
          //   y.displayName = y.condName
          //   y.modelValue = y.condName
          //   return y
          // })

          this.conditioningList[index].conNamelist = Response.map((y: any) => {
            y.displayName = y.condName;
            y.modelValue = y.condName;
            return y;
          });
        });
    }

    if (col.field == 'conditioningName') {
      // let getconditioningdetails = col.list.filter((x: any) => x.condName == condType.conditioningName)[0]
      let getconditioningdetails = this.conditioningList[
        index
      ].conNamelist.filter(
        (x: any) => x.condName == condType.conditioningName
      )[0];
      condType.widthEditFlag = getconditioningdetails.widthEditFlag;
      condType.thicknessEditFlag = getconditioningdetails.thicknessEditFlag;
      condType.lengthEditFlag = getconditioningdetails.lengthEditFlag;
      condType.defaultThickness = getconditioningdetails.defaultThickness;
      condType.defaultWidth = getconditioningdetails.defaultWidth;
      condType.defaultLength = getconditioningdetails.defaultLength;
      condType.condDesc = getconditioningdetails.condDesc;
      condType.weightFlag = getconditioningdetails.weightFlag;
      condType.disabledWidth =
        getconditioningdetails.widthEditFlag == 'Y' ? false : true;
      condType.disabledThickness =
        getconditioningdetails.thicknessEditFlag == 'Y' ? false : true;
      condType.disabledLength =
        getconditioningdetails.lengthEditFlag == 'Y' ? false : true;
      //added as requested by rajashree
      condType.thicknessFlag = getconditioningdetails.thicknessFlag;
      condType.widthFlag = getconditioningdetails.widthFlag;

      if (getconditioningdetails.thicknessEditFlag == 'Y') {
        this.isthicknessEditFlag(getconditioningdetails, condType);
      }
      if (getconditioningdetails.thicknessEditFlag == 'N') {
        condType.length = getconditioningdetails.defaultLength;
        condType.width = getconditioningdetails.defaultWidth;
        condType.thickness = getconditioningdetails.defaultThickness;
        condType.originalthickness = this.balanceDetails[0].thickness;
        condType.defaultThickness = getconditioningdetails.defaultThickness;
        condType.balanceThickness = this.balanceDetails[0].thickness;
        condType.balanceLength = this.balanceDetails[0].length;
        condType.balanceWidth = this.balanceDetails[0].width;
      }

      if (getconditioningdetails.widthEditFlag == 'Y') {
        this.iswidthEditFlag(getconditioningdetails, condType);
      }

      if (getconditioningdetails.lengthEditFlag == 'Y') {
        this.islengthEditFlag(getconditioningdetails, condType);
      }
    }
  }

  public updateWeightByWidth(rowData): any {
    rowData.balanceWidth = rowData.originalWidth;
    if (rowData.width > rowData.balanceWidth) {
      rowData.widthError = true;
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Thickness value should be less than Balance Thickness`,
        }
      );
    }

    rowData.widthError = false;
    rowData.balanceWidth =
      parseFloat(rowData.originalWidth) - parseFloat(rowData.width);
    rowData.balanceLength = this.balanceDetails[0].length;
    rowData.balanceThickness = this.balanceDetails[0].thickness;
    rowData.balanceWidth =
      parseFloat(rowData.originalWidth) - parseFloat(rowData.width);
    //rowData.weight = (parseFloat(rowData.thickness) * parseFloat(rowData.length) * parseFloat(rowData.width))
  }

  public updateWeightByThickness(rowData): any {
    rowData.balanceThickness = rowData.originalthickness;
    if (rowData.thickness > rowData.balanceThickness) {
      rowData.thicknessError = true;

      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Thickness value should be less than Balance Thickness`,
        }
      );
    }
    rowData.thicknessError = false;
    rowData.balanceThickness =
      parseFloat(rowData.originalthickness) - parseFloat(rowData.thickness);
    rowData.balanceLength = this.balanceDetails[0].length;
    rowData.balanceWidth = this.balanceDetails[0].width;
    rowData.balanceThickness =
      parseFloat(rowData.originalthickness) - parseFloat(rowData.thickness);
    rowData.defaultWeight =
      ((parseFloat(rowData.thickness) * parseFloat(rowData.length)) / 1000) *
      parseFloat(rowData.width);
  }

  public isthicknessEditFlag(conditioningName, rowData) {
    rowData.disabledThickness =
      conditioningName.thicknessEditFlag == 'Y' ? false : true;
    rowData.length = conditioningName.defaultLength;
    rowData.width = conditioningName.defaultWidth;
    rowData.thickness = conditioningName.defaultThickness;
    rowData.originalthickness = this.balanceDetails[0].thickness;
    rowData.defaultThickness = conditioningName.defaultThickness;
    rowData.defaultWeight =
      parseFloat(rowData.defaultThickness) *
      parseFloat(rowData.length) *
      parseFloat(rowData.width);
    rowData.balanceThickness =
      parseFloat(this.balanceDetails[0].thickness) -
      parseFloat(rowData.defaultThickness);
    rowData.balanceLength = this.balanceDetails[0].length;
    rowData.balanceWidth = this.balanceDetails[0].width;
  }

  public iswidthEditFlag(conditioningName, rowData) {
    rowData.disabledWidth =
      conditioningName.widthEditFlag == 'Y' ? false : true;
    rowData.length = conditioningName.defaultLength;
    rowData.width = conditioningName.defaultWidth;
    rowData.thickness = conditioningName.defaultThickness;
    rowData.originalWidth = this.balanceDetails[0].width;
    rowData.defaultWidth = conditioningName.defaultWidth;
    rowData.balanceWidth =
      parseFloat(this.balanceDetails[0].width) -
      parseFloat(rowData.defaultWidth);
    rowData.defaultWeight =
      parseFloat(rowData.thickness) *
      parseFloat(rowData.length) *
      parseFloat(rowData.defaultWidth);
    rowData.balanceLength = this.balanceDetails[0].length;
    rowData.balanceThickness = this.balanceDetails[0].thickness;
  }

  public islengthEditFlag(conditioningName, rowData) {
    rowData.disabledLength =
      conditioningName.lengthEditFlag == 'Y' ? false : true;
    rowData.originalLength = this.balanceDetails[0].length;
    rowData.length = conditioningName.defaultLength;
    rowData.balanceLength =
      parseFloat(rowData.defaultLength) - parseFloat(rowData.length);
  }

  public validateNumericField(rowData, field, ri) {}

  public insertConditioning(): any {
    let conditioningValidations = this.conditioningList.filter(
      (x: any) => x.condCtr == 0
    );
    if (conditioningValidations.length > 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please save previous conditioning`,
        }
      );
    }
    if (this.conditioningList.length > 0) {
      this.balanceDetails = [];
      let index = this.conditioningList.length - 1;
      //23-11
      let totalConsumedWeight = 0;
      let balanceRemWeight;
      this.conditioningList.forEach((element) => {
        totalConsumedWeight += element.defaultWeight;
      });
      balanceRemWeight = this.selectedBatch.weight - totalConsumedWeight;
      balanceRemWeight = parseFloat(balanceRemWeight).toFixed(3);
      //
      let updateBalanceDetails = {
        length: this.conditioningList[index].balanceLength,
        width: this.conditioningList[index].balanceWidth,
        thickness: this.conditioningList[index].balanceThickness,
        defaultWeight: balanceRemWeight,
      };

      this.balanceDetails.push(updateBalanceDetails);
    } else {
      this.balanceDetails = [];
      let updateBalanceDetails = {
        length: this.selectedBatch.length,
        width: this.selectedBatch.width,
        thickness: this.selectedBatch.thickness,
        weight: this.selectedBatch.weight,
      };
      this.balanceDetails.push(updateBalanceDetails);
    }

    if (this.holdCtr === undefined) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `Please Add Conditioning`,
        }
      );
    }
    let inserData = {
      disabledThickness: true,
      disabledLength: true,
      disabledWidth: true,
      batchId: this.selectedBatch.batchId,
      condCtr: 0,
      condDesc: null,
      condRemark: null,
      productId: this.selectedBatch.productId,
      sizeCode: this.selectedBatch.sizeCode,
      condType: this.selectedBatch.productId,
      condName: this.selectedBatch.conditioningName,
      dim1: 0,
      dim2: 0,
      dim3: 0,
      dim4: 0,
      holdCtr: this.holdCtr,
      length: this.selectedBatch.length,
      thickness: this.selectedBatch.thickness,
      weight: this.selectedBatch.weight,
      width: this.selectedBatch.width,
      sequence: this.sequenceCounter,
    };
    this.conditioningList.push(inserData);
    this.finalized = false;
    this.sequenceCounter = this.sequenceCounter + 1;
  }

  public getFiinalizezedConditioningDetails() {
    this.balanceDetails = [];
    this.smsServices
      .getSavedConditioningDetails(this.selectedBatch.batchId)
      .subscribe((Response) => {
        this.balanceDetails = [];
        this.conditioningList = [];
        if (Response.length > 0) {
          this.finalized = true;
          Response.map((x: any) => {
            x.isReadOnly = true;
            x.disabledLength = true;
            x.disabledWidth = true;
            x.disabledThickness = true;
            x.conditioningName = x.condName;
            x.conditioningType = parseInt(x.condType);
            x.productId = this.selectedBatch.productId;
            x.sizeCode = this.selectedBatch.sizeCode;
            this.fetchConditioningName(x.condType, x.batchId);
            // x.weight = (x.length * x.width * x.thickness)
          });
          let index = Response.length - 1;
          //23-11
          let totalConsumedWeight = 0;
          let balanceRemWeight;
          Response.forEach((element) => {
            totalConsumedWeight = +element.defaultWeight;
          });
          balanceRemWeight = Response[0].weight - totalConsumedWeight;
          balanceRemWeight = parseFloat(balanceRemWeight).toFixed(3);
          //
          let updateBalanceDetails = {
            length: Response[index].length,
            width: Response[index].width,
            thickness: Response[index].thickness,
          };
          this.balanceDetails.push(updateBalanceDetails);
          this.conditioningHistoryList = Response;
          // this.conditioningList = Response.map((x:any)=>{
          //   x.balanceThickness = x.thickness,
          //   x.balanceLength = x.length,
          //   x.balanceWidth = x.width,
          //   x.thickness = x.defaultThickness,
          //   x.length = x.defaultLength,
          //   x.width = x.defaultWidth
          //   return x
          // })
        } else {
          this.conditioningHistoryList = Response;
          this.balanceDetails.push(this.selectedBatch);
        }
      });
    this.getPlanDetails();
  }

  public getSavedConditioningDetails() {
    this.balanceDetails = [];
    // this.conditioningHistoryList=[]
    this.smsServices
      .getSavedConditioningDetails(this.selectedBatch.batchId)
      .subscribe((Response) => {
        this.balanceDetails = [];
        this.conditioningList = [];
        if (Response.length > 0) {
          Response.map((x: any) => {
            // x.isReadOnly = true;
            x.disabledLength = x.lengthEditFlag == 'N' ? true : false;
            x.disabledWidth = x.widthEditFlag == 'N' ? true : false;
            x.disabledThickness = x.thicknessEditFlag == 'N' ? true : false;
            x.conditioningName = x.condName;
            x.conditioningType = parseInt(x.condType);
            x.productId = this.selectedBatch.productId;
            x.sizeCode = this.selectedBatch.sizeCode;
            this.fetchConditioningName(x.condType, x.batchId);
            // x.weight = (x.length * x.width * x.thickness)
          });
          let index = Response.length - 1;
          //23-11
          let totalConsumedWeight = 0;
          let balanceRemWeight;
          Response.forEach((element) => {
            totalConsumedWeight += element.defaultWeight;
          });
          balanceRemWeight = this.selectedBatch.weight - totalConsumedWeight;
          // balanceRemWeight = Response[index].weight   ... commented this line
          balanceRemWeight = parseFloat(balanceRemWeight).toFixed(3);
          //
          let updateBalanceDetails = {
            length: Response[index].length,
            width: Response[index].width,
            thickness: Response[index].thickness,
            defaultWeight: balanceRemWeight,
          };
          this.balanceDetails.push(updateBalanceDetails);
          //this.conditioningHistoryList =JSON.parse(JSON.stringify(Response))
          this.conditioningList = Response.map((x: any) => {
            (x.balanceThickness = x.thickness),
              (x.balanceLength = x.length),
              (x.balanceWidth = x.width),
              (x.thickness = x.defaultThickness),
              (x.length = x.defaultLength),
              (x.width = x.defaultWidth);
            return x;
          });
        } else {
          //this.conditioningHistoryList = Response
          let updateBalancesDetails = {
            length: this.selectedBatch.length,
            width: this.selectedBatch.width,
            thickness: this.selectedBatch.thickness,
            defaultWeight: this.selectedBatch.weight,
          };
          this.balanceDetails.push(updateBalancesDetails);
        }
      });
  }

  //23-11
  public getHistoryConditioningDetails() {
    this.balanceDetails = [];
    this.smsServices
      .getSavedConditioningDetails(this.selectedBatch.batchId)
      .subscribe((Response) => {
        this.balanceDetails = [];
        this.conditioningList = [];
        if (Response.length > 0) {
          Response.map((x: any) => {
            this.conditioningHistoryList = JSON.parse(JSON.stringify(Response));
          });
          let index = Response.length - 1;
          let updateBalanceDetails = {
            length: Response[index].length,
            width: Response[index].width,
            thickness: Response[index].thickness,
            defaultWeight: Response[index].weight,
          };
          this.balanceDetails.push(updateBalanceDetails);
        } else {
          this.conditioningHistoryList = Response;
          this.balanceDetails.push(this.selectedBatch);
        }
      });
  }
  //
  public getFormRef(event) {
    this.formRef = event;
  }
}
