import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { JswCoreService,  JswFormComponent} from 'jsw-core';
import { SharedService } from 'src/app/shared/services/shared.service';
import { PrimeNGConfig, SharedModule } from 'primeng/api';
import { ConditioningScreenForms, DummyBatch } from '../../form-objects/sms-form-objects';
import { SmsService } from '../../services/sms.service';
import { reduce } from 'rxjs/operators';
import { ConditioningScreenComponent } from './conditioning-screen.component';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { SmsEndPoints } from '../../form-objects/sms-api-endpoint';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('ConditioningScreenComponent', () => {
  let component: ConditioningScreenComponent;
  let fixture: ComponentFixture<ConditioningScreenComponent>;
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let smsService: SmsService
  let commonService: CommonApiService

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConditioningScreenComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers: [
        JswCoreService,
        SmsService,
        CommonApiService,
        JswFormComponent,
        SmsEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        ConditioningScreenForms,
        { provide: ToastrService, useValue: toastrService },
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditioningScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
