import { Component, OnInit } from '@angular/core';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { ColumnType, ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SmallBatchProductionForm } from '../../form-objects/sms-form-objects';
import { SmsService } from '../../services/sms.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-qa-hold',
  templateUrl: './qa-hold.component.html',
  styleUrls: ['./qa-hold.component.css']
})
export class QaHoldComponent implements OnInit {
  formObject: any;
  viewType = ComponentType;
  columns: ITableHeader[] = [];
  tableType: any;
  QAholdDetails:any=[];
  public selectedHeat: any[] = [];
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isFlCrViewOnly:boolean= false;
  public isFlCrRtrv:boolean= false;
  public isFlCrRst:boolean = false;
  public batchDetails:boolean = false;
  public batchDetailsHold:boolean = false;
  public batchDetailsRlse:boolean = false;
  public formRef: NgForm;
  public clearSelection: Subject<boolean> = new Subject<boolean>();

  constructor( public smallBatchProductionForm:SmallBatchProductionForm,public smsService: SmsService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public sharedService: SharedService,public authService: AuthService) { }

    public screenRightsCheck(){
      let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.SMS_Operation)[0];
      this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.smsOperationScreenIds.Qa_Hold_Screen.id)[0];
      this.isFlCrRtrv = this.sharedService.isButtonVisible(true, menuConstants.qAholdSectionIds.Filter_Criteria.buttons.retrive, menuConstants.qAholdSectionIds.Filter_Criteria.id, this.screenStructure);
      this.isFlCrRst = this.sharedService.isButtonVisible(true, menuConstants.qAholdSectionIds.Filter_Criteria.buttons.reset, menuConstants.qAholdSectionIds.Filter_Criteria.id, this.screenStructure);
      this.batchDetailsHold = this.sharedService.isButtonVisible(true, menuConstants.qAholdSectionIds.BatchDetails_hold.buttons.hold, menuConstants.qAholdSectionIds.BatchDetails_hold.id, this.screenStructure);
      this.batchDetailsRlse = this.sharedService.isButtonVisible(true, menuConstants.qAholdSectionIds.BatchDetails_hold.buttons.release, menuConstants.qAholdSectionIds.BatchDetails_hold.id, this.screenStructure);
      this.isFlCrViewOnly = this.sharedService.isSectionVisible(menuConstants.qAholdSectionIds.Filter_Criteria.id, this.screenStructure);
      this.batchDetails = this.sharedService.isSectionVisible(menuConstants.qAholdSectionIds.BatchDetails_hold.id, this.screenStructure);
    }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.qaHoldFilter();
    this.formObject = JSON.parse(JSON.stringify(this.smallBatchProductionForm.qaHold));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.columns = [
      {
        field: 'checkbox',
        header: '',
        columnType: ColumnType.checkbox,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '90px',
        sortFieldName: '',
      },
      {
        field: 'soId',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '130px',
        sortFieldName: '',
      },
      {
        field: 'lineNo',
        header: 'Line No',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Product Type',
        columnType: ColumnType.string,
        width: '110px',
        sortFieldName: '',
      },
      {
        field: 'productSize',
        header: 'Product Size ',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Length(mm)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Weight(tons)',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'statusDesc',
        header: 'Status',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      // {
      //   field: 'thickness',
      //   header: 'Thickness',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'diameter ',
      //   header: 'Diameter ',
      //   columnType: ColumnType.string,
      //   width: '50px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'width',
      //   header: 'Width ',
      //   columnType: ColumnType.string,
      //   width: '100px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'soId',
      //   header: 'Sales Order',
      //   columnType: ColumnType.string,
      //   width: '130px',
      //   sortFieldName: '',
      // },
      {
        field: 'poId',
        header: 'PO ID',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
    ];
  }


  public dropdownChangeEvent(event){
    this.formObject.formFields.filter((obj: any) => obj.ref_key == 'heatNo')[0].value = null
    if(event.value != null && event.value != ''){
      let UnitDetails =this.formObject.formFields.filter((obj: any) => obj.ref_key == 'wcName')[0].list;
      let heatList = UnitDetails.filter((x:any)=> x.modelValue == event.value)[0].heatList.map((x:any)=>{
        return {
          displayName: x, modelValue: x
        }
      });
      this.formObject.formFields.filter((obj: any) => obj.ref_key == 'heatNo')[0].list = heatList
    }
  }

  public qaHoldFilter(){
    this.smsService.qaHoldFilter().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'wcName'
      )[0].list = data.map((x: any) => {
        return { displayName: x.wcName, modelValue: x.wcName ,
        heatList:x.headIdList};
      });
    });
  }

  public filterDataByUnit() {
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    this.smsService
      .getBatchDetails(formData[0].value, formData[1].value,formData[2].value)
      .subscribe((Response) => {
        Response.map((ele: any) => {
          ele.submitted = false;
          //ele.disabled = true;
          ele.parentBatch = ele.batchId;
          return ele;
        });
        this.QAholdDetails = Response;
      });
  }

  public getSelectedRow(event){
    this.selectedHeat = event;
  }

  public createHold():any{
    if(!this.selectedHeat.length){
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please select heat Id `,
        }
      );
    }

    //new changes akhilesh suggested
    let newRequestBody= [];
    this.selectedHeat.forEach(element => {
      let temp = {
        "batchId": element.batchId,
        "batchState": element.batchState,
        "dia": element.dia,
        "grade": element.grade,
        "heatId": element.heatId,
        "length": element.length,
        "poId": element.poId,
        "productSize": element.productSize,
        "productType":element.productType,
        "soId": element.soId,
        "status": element.status,
        "thickness": element.thickness,
        "weight": element.weight,
        "width": element.width
      }
      newRequestBody.push(temp);
    });
    //end of new change
  // let requestBody = {
  //   "batchId": this.selectedHeat.batchId,
  //   "batchState": this.selectedHeat.batchState,
  //   "dia": this.selectedHeat.dia,
  //   "grade": this.selectedHeat.grade,
  //   "heatId": this.selectedHeat.heatId,
  //   "length": this.selectedHeat.length,
  //   "poId": this.selectedHeat.poId,
  //   "productSize": this.selectedHeat.productSize,
  //   "productType":this.selectedHeat.productType,
  //   "soId": this.selectedHeat.soId,
  //   "status": this.selectedHeat.status,
  //   "thickness": this.selectedHeat.thickness,
  //   "weight": this.selectedHeat.weight,
  //   "width": this.selectedHeat.width
  // }

    this.smsService.createQaHold(newRequestBody, this.sharedService.loggedInUserDetails.userId, this.formObject.formFields[2].value).subscribe(Response =>{
      this.selectedHeat = [];
      this.clearSelection.next(true);
      this.filterDataByUnit();
      let batchIdArr = Response.map(item => item.batchId);
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `Hold Action added for selected batches - ${batchIdArr.join(', ')}`, // ${Response.batchId}
        }
      );
    })
  }

  public reset(){
    this.QAholdDetails = [];
    this.selectedHeat = [];
    this.clearSelection.next(true);
    //this.jswComponent.resetForm(this.formObject);
  }

  public releaseQaHold():any{
    if(!this.selectedHeat.length){
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please select Batch Id `,
        }
      );
    }
    this.smsService.releaseQaHold(this.selectedHeat[0].batchId,this.sharedService.loggedInUserDetails.userId).subscribe(Response =>{
      this.selectedHeat = [];
      this.filterDataByUnit();
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `${Response}`,
        }
      );
    })
  }

  public getFormRef(event){
    this.formRef = event;
  }


}
