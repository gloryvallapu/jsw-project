import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { ColumnType, ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SmallBatchProductionForm } from '../../form-objects/sms-form-objects';
import { SmsService } from '../../services/sms.service';
import { QaHoldComponent } from './qa-hold.component';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { SmsEndPoints } from '../../form-objects/sms-api-endpoint';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('QaHoldComponent', () => {
  let component: QaHoldComponent;
  let fixture: ComponentFixture<QaHoldComponent>;
  let jswService: JswCoreService
  let jswComponent: JswFormComponent
  let smsService: SmsService
  let commonService: CommonApiService

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QaHoldComponent ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers: [
        JswCoreService,
        SmsService,
        CommonApiService,
        JswFormComponent,
        SmsEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        SmallBatchProductionForm,
        { provide: ToastrService, useValue: toastrService },
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QaHoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
