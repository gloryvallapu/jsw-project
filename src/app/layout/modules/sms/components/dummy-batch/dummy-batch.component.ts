import { Component, OnInit, ViewChild } from '@angular/core';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ColumnType, TableType } from 'src/assets/enums/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { DummyBatch } from '../../form-objects/sms-form-objects';
import { SmsService } from '../../services/sms.service';
import { Subject } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { NgForm } from '@angular/forms';
import { element } from 'protractor';

@Component({
  selector: 'app-dummy-batch',
  templateUrl: './dummy-batch.component.html',
  styleUrls: ['./dummy-batch.component.css'],
})
export class DummyBatchComponent implements OnInit {
  tableType: string;
  viewType = ComponentType;
  DummyBatch: any;
  formObject: any;
  columns: ITableHeader[] = [];
  public dummyDetails: any = [];
  public requestBody: any;
  public selectedBatches: any[];
  public clearSelection: Subject<boolean> = new Subject<boolean>();
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isFlCrViewOnly:boolean= false;
  public isFlCrRtrv:boolean= false;
  public isFlCrRst:boolean = false;
  public batchDetails:boolean = false;
  public batchDetailsSave:boolean = false;
  public formRef: NgForm;
  public dummyMinLength = 0;

  constructor(
    public dummyBatch: DummyBatch,
    public jswComponent: JswFormComponent,
    public smsService: SmsService,
    public sharedService: SharedService,
    public commonService: CommonApiService,
    public jswService: JswCoreService,
    public authService: AuthService
  ) {}

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.SMS_Operation)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.smsOperationScreenIds.Dummy_Batch_Screen.id)[0];
    this.isFlCrRtrv = this.sharedService.isButtonVisible(true, menuConstants.dummyBatchSectionIds.Filter_Criteria.buttons.retrive, menuConstants.dummyBatchSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRst = this.sharedService.isButtonVisible(true, menuConstants.dummyBatchSectionIds.Filter_Criteria.buttons.reset, menuConstants.dummyBatchSectionIds.Filter_Criteria.id, this.screenStructure);
    this.batchDetailsSave = this.sharedService.isButtonVisible(true, menuConstants.dummyBatchSectionIds.BatchDetails.buttons.save, menuConstants.dummyBatchSectionIds.BatchDetails.id, this.screenStructure);
    this.batchDetails = this.sharedService.isSectionVisible(menuConstants.dummyBatchSectionIds.BatchDetails.id, this.screenStructure);
    this.isFlCrViewOnly = this.sharedService.isSectionVisible(menuConstants.dummyBatchSectionIds.Filter_Criteria.id, this.screenStructure);
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.getUnitList();
    this.getLov();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.formObject = JSON.parse(JSON.stringify(this.dummyBatch.dummyBatchFilter));
    this.columns = [
      {
        field: 'checkbox',
        header: 'Dummy',
        columnType: ColumnType.checkbox,
        width: '20px',
        sortFieldName: '',
      },
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '50px',
        sortFieldName: '',
      },
      {
        field: 'grade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '70px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Product Type',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'productSize',
        header: 'Product Size ',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      // {
      //   field: 'customerName',
      //   header: 'Customer Name',
      //   columnType: ColumnType.string,
      //   width: '50px',
      //   sortFieldName: '',
      // },
      {
        field: 'length',
        header: 'Length(mm)',
        columnType: ColumnType.string,
        width: '60px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Weight(tons)',
        columnType: ColumnType.string,
        width: '60px',
        sortFieldName: '',
      },
      // {
      //   field: 'poId',
      //   header: 'PO No',
      //   columnType: ColumnType.string,
      //   width: '50px',
      //   sortFieldName: '',
      // },
      {
        field: 'soId',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
    ];
    this.selectedBatches = [];
  }

  //new code
  public getLov(){
    this.commonService.getLovs('val_len_dum').subscribe((response: any)=>{
      if(response){
        this.dummyMinLength = parseInt(response[0].codeValue)
      }
    })
  }

  public getRowDetails(event) {
    this.selectedBatches = event;
  }

  public dropdownChangeEvent(event) {
    if (event.ref_key == 'wcName') {
    }
    if (event.ref_key == 'wcName' && event.value != null) {
      let heatIdListByUnit = this.formObject.formFields
        .filter((ele) => ele.ref_key == 'wcName')[0]
        .list.filter((x: any) => x.modelValue == event.value)[0];
      this.formObject.formFields.filter(
        (ele) => ele.ref_key == 'heatNo'
      )[0].list = heatIdListByUnit.heatIdList.map((x: any) => {
        return {
          displayName: x,
          modelValue: x,
        };
      });
    }
  }

  public getUnitList() {
    this.smsService.dummyBatchFilter().subscribe((data) => {
      this.formObject.formFields.filter(
        (obj: any) => obj.ref_key == 'wcName'
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.wcName,
          modelValue: x.wcName,
          heatIdList: x.headIdList,
        };
      });
    });
  }

  public getDummyBatchDetails() {
    let wcName = this.formObject.formFields.filter(
      (ele: any) => ele.ref_key == 'wcName'
    )[0].value;
    let heatNo = this.formObject.formFields.filter(
      (ele: any) => ele.ref_key == 'heatNo'
    )[0].value;
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formObject = this.jswService.getFormData();
    this.smsService
      .getDummyBatchDetails(wcName, heatNo ? heatNo : 0)
      .subscribe((Response) => {
        Response.map((rowData: any) => {
          return rowData;
        });
        this.dummyDetails = Response;
      });
  }

  public resetForm() {
    //this.jswComponent.resetForm(this.formObject);
    this.clearSelection.next(true);
    this.dummyDetails=[];
  }

  public dummySave(): any {
    if (this.selectedBatches.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: 'Please Select Dummy Batch',
        }
      );
    }
    // if (this.selectedBatches.length) {
    //   this.selectedBatches.forEach((element)=>{
    //     if(element.length < this.dummyMinLength){
    //       return this.sharedService.displayToastrMessage(
    //         this.sharedService.toastType.Warning,
    //         {
    //           message: `BatchID ${element.batchId} length is less than Min Length` ,
    //         }
    //       );
    //     }
    //   })
    // }

    //this.dummyMinLength = 15000;
    var errorMsg = this.selectedBatches.map((batch: any) => {
      if(batch.length < this.dummyMinLength){
         return batch;
      }
    });
      if(errorMsg){
        return  this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Warning,
          {
            message: `BatchID ${errorMsg[0].batchId} length is less than Min Length` ,
          }
        );
      }
    let dummyBatchIdList = this.selectedBatches.map((batch: any) => {
        return batch.batchId;
    });
    this.smsService
      .saveDummyBatchDetails(
        this.sharedService.loggedInUserDetails.userId,
        dummyBatchIdList
      )
      .subscribe((Response) => {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success,
          {
            message: Response,
          }
        );
        this.getDummyBatchDetails();
        this.selectedBatches = [];
        this.clearSelection.next(true);
      });
  }

  public getFormRef(event){
    this.formRef = event;
  }

}
