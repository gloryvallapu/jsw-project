import { Component, OnInit, ViewChild } from '@angular/core';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Observable, Subject } from 'rxjs';
import { Table } from 'primeng/table';
import { SmsService } from '../../services/sms.service';
import { QaReleaseForms, QAReleaseScreenForm, SmallBatchProductionForm } from '../../form-objects/sms-form-objects';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-qa-release',
  templateUrl: './qa-release.component.html',
  styleUrls: ['./qa-release.component.css']
})
export class QaReleaseComponent implements OnInit {

  public viewType = ComponentType;
  public columnType = ColumnType;
  public columns: ITableHeader[] = [];
  public tableType: any;
  public filterFormObject: any = [];
  public compatableGrade: any = [];
  public QAReleaseDetails: any = [];
  public selectedRecords: any = []
  public selectedUnit: any;
  public globalFilterArray: any = [];
  public openGradeChangeModel: boolean = false;
  public chemicalobject: any = []
  public heatDetails: any = [];
  public openNcoDeclaration: boolean = false;
  public filterCriteria: any = []
  public selectedBatch: any;
  public currentGradeDetails: any;
  public menuConstants = menuConstants;
  public screenStructure: any = [];
  public isFlCrViewOnly:boolean= false;
  public isFlCrRtrv:boolean= false;
  public isFlCrRst:boolean = false;
  public batchDetails:boolean = false;
  public batchDetailsSave:boolean = false;
  public batchDetailsRlse:boolean = false;
  public formRef: NgForm;
  public saveResponse: any =[];
  public dummyMinLength = 0;

  constructor(public smallBatchProductionForm: QAReleaseScreenForm,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public jswComponent: JswFormComponent,
    public smsService: SmsService,
    public commonService: CommonApiService,
    public qaReleaseForms: QaReleaseForms,public authService: AuthService) {
    this.columns = [
      {
        field: 'batchId',
        header: 'Batch ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'heatId',
        header: 'Heat ID',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'jswGrade',
        header: 'Grade',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'productType',
        header: 'Prod. Type',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      {
        field: 'sizeCode',
        header: 'Prod. Size',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'length',
        header: 'Len(mm)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'weight',
        header: 'Wgt(tons)',
        columnType: ColumnType.string,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'release',
        header: 'Release',
        columnType: ColumnType.radio,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'gradeChange',
        header: 'Grade Change',
        columnType: ColumnType.radio,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'nco',
        header: 'NCO',
        columnType: ColumnType.radio,
        width: '60px',
        sortFieldName: '',
      },
      {
        field: 'notFitForRolling',
        header: 'Not Fit For Rolling',
        columnType: ColumnType.radio,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'dummy',
        header: 'Dummy',
        columnType: ColumnType.radio,
        width: '80px',
        sortFieldName: '',
      },
      {
        field: 'nextWc',
        header: 'Next Unit',
        columnType: ColumnType.string,
        width: '100px',
        sortFieldName: '',
      },
      // {
      //   field: 'dia',
      //   header: 'Diameter',
      //   columnType: ColumnType.string,
      //   width: '90px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'thickness',
      //   header: 'Thickness',
      //   columnType: ColumnType.string,
      //   width: '90px',
      //   sortFieldName: '',
      // },
      // {
      //   field: 'width',
      //   header: 'Width',
      //   columnType: ColumnType.string,
      //   width: '80px',
      //   sortFieldName: '',
      // },
      {
        field: 'salesOrder',
        header: 'Sales Order',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'customer',
        header: 'Customer',
        columnType: ColumnType.ellipsis,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'poId',
        header: 'PO ID',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'message',
        header: 'Remark',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
    ];

  }

  public screenRightsCheck(){
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.SMS_Operation)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.smsOperationScreenIds.Qa_Release_Screen.id)[0];
    this.isFlCrRtrv = this.sharedService.isButtonVisible(true, menuConstants.qAReleaseSectionIds.Filter_Criteria.buttons.retrive, menuConstants.qAReleaseSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isFlCrRst = this.sharedService.isButtonVisible(true, menuConstants.qAReleaseSectionIds.Filter_Criteria.buttons.reset, menuConstants.qAReleaseSectionIds.Filter_Criteria.id, this.screenStructure);
    this.batchDetailsSave = this.sharedService.isButtonVisible(true, menuConstants.qAReleaseSectionIds.BatchDetails_Release.buttons.save, menuConstants.qAReleaseSectionIds.BatchDetails_Release.id, this.screenStructure);
    this.batchDetails = this.sharedService.isSectionVisible(menuConstants.qAReleaseSectionIds.BatchDetails_Release.id, this.screenStructure);
    this.isFlCrViewOnly = this.sharedService.isSectionVisible(menuConstants.qAReleaseSectionIds.Filter_Criteria.id, this.screenStructure);
  }

  ngOnInit(): void {
    this.screenRightsCheck();
    this.getUnitList();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.filterFormObject =JSON.parse(JSON.stringify( this.smallBatchProductionForm.qaReleaseFilterForm));
    this.filterCriteria = JSON.parse(JSON.stringify(this.qaReleaseForms.filterCriteria));
  };

  public updatecompatableGrade() {

  };

  public getGradehemicalDetailsByHeat(rowData) {
    this.smsService.getGradeChemicalDetailsByHeat(rowData.heatId, rowData.batchId, rowData.jswGrade).subscribe(Response => {
      this.currentGradeDetails = Response;
      this.chemicalobject = Response.gradeChangeElementList //Object.entries(Response.gradeChangeElementList)
    })
  };

  public reset() {

  };

  public isRadioButtonChecked(fieldName, columeName, rowData) {
    rowData.isChanged = true;
    this.selectedBatch = rowData
    if (fieldName == "nco") {
      rowData.gradeChange = false;
      rowData.notFitForRolling = false;
      rowData.release = false;
      rowData.dummy = false;
    }
    if (fieldName == "release") {
      rowData.gradeChange = false;
      rowData.notFitForRolling = false;
      rowData.nco = false;
      rowData.dummy = false;
    }
    if (fieldName == "notFitForRolling") {
      rowData.gradeChange = false;
      rowData.release = false;
      rowData.nco = false;
      rowData.dummy = false;
    }
    if (fieldName == "gradeChange") {
      rowData.notFitForRolling = false;
      rowData.release = false;
      rowData.nco = false;
      rowData.dummy = false;
    }
    if (fieldName == "dummy") {
      rowData.notFitForRolling = false;
      rowData.release = false;
      rowData.nco = false;
      rowData.gradeChange = false;
    }
    if (columeName == 'Grade Change') {
      rowData.dummy = false;
      this.openGradeChangeModel = true;
      this.bindValuesToForm(rowData)
      this.getGradehemicalDetailsByHeat(rowData);
    }
  };

  public bindValuesToForm(rowData) {
    this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'heatId')[0].value = rowData.heatId;
    this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'batchId')[0].value = rowData.batchId;
    this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'curentGrade')[0].value = rowData.jswGrade
  };

  public gerRowOnclick(rowData) {
  };

  public dropdownChangeEvent(event) {
    if (event.ref_key == 'wcName') {
      this.selectedUnit = event.value;
    }

    if (event.ref_key == 'wcName' && event.value != null) {
      let heatIdListByUnit = this.filterFormObject.formFields.filter(ele => ele.ref_key == 'wcName')[0].list.filter((x: any) => x.modelValue == event.value)[0]
      this.filterFormObject.formFields.filter(ele => ele.ref_key == 'heatNo')[0].list = heatIdListByUnit.heatIdList.map((x: any) => {
        return {
          displayName: x,
          modelValue: x
        }
      });
    }
  }


  public getUnitList() {
    this.smsService.qaReleaseFilter().subscribe((data) => {
      this.filterFormObject.formFields.filter(
        (obj: any) => obj.ref_key == 'wcName'
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.wcName,
          modelValue: x.wcName,
          heatIdList: x.headIdList
        };
      });
    });
  };


  public getQAReleaseDetails() {
    this.jswComponent.onSubmit(this.filterFormObject.formName, this.filterFormObject.formFields);
    var filterFormObject = this.jswService.getFormData();
    this.smsService.getQAReleaseDetails(filterFormObject[0].value, filterFormObject[1].value).subscribe(Response => {
      Response.map((rowData: any) => {
        return rowData;
      })
      Response.map((x:any)=>{
        let index = this.saveResponse.filter((y:any)=>y.batchId == x.batchId)[0];
        if(index != undefined){
          x.message = index.message;
        }
        return x
      })
      this.QAReleaseDetails = Response
    })
  }

   //new code
   public getLov(){
    this.commonService.getLovs('val_len_dum').subscribe((response: any)=>{
      if(response){
        this.dummyMinLength = parseInt(response[0].codeValue)
      }
    })
  }

  public saveBatchDetails() {
    let reqBody  = this.QAReleaseDetails.filter((x:any)=> x.isChanged == true)
    if(reqBody[0].length < this.dummyMinLength){
       this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning,
        {
          message: `BatchID ${reqBody[0].batchId} length is less than Min Length` ,
        }
      );
    }
    else{
      this.smsService.saveQaReleaseBatchDetails(this.sharedService.loggedInUserDetails.userId, reqBody).subscribe(Response => {
        this.saveResponse = JSON.parse(Response);
        this.saveResponse.forEach(element => {
          if(element.message == '' || element.message == null){
            this.getQAReleaseDetails();

            this.sharedService.displayToastrMessage(
              this.sharedService.toastType.Success, {
                message: `Records are saved successfully`,
              }
            );
          }
          else{
            this.getQAReleaseDetails();
            //this.saveResponse = JSON.parse(Response);
            this.sharedService.displayToastrMessage(
              this.sharedService.toastType.Warning, {
                message: element.message,
              }
            );
          }
        });


        // this.sharedService.displayToastrMessage(
        //   this.sharedService.toastType.Success, {
        //     message: `${Response} `,
        //   }
        // );
      })
    }

  };

  public resetFilter() {
    //this.jswComponent.resetForm(this.filterFormObject);
    this.selectedBatch = [];
    this.QAReleaseDetails=[];
  };

  public closeGradeChangePopup(clossingStatus) {
    if(clossingStatus == 'manualClose'){
      let batchId = this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'batchId')[0].value
      let index = this.QAReleaseDetails.findIndex(x => x.batchId == batchId);
      let rowData = this.QAReleaseDetails[index]
      rowData.gradeChange = false;
    rowData.release = false;
    rowData.nco = false;
    rowData.dummy = false;
    this.currentGradeDetails = null;
    this.chemicalobject = []
    }
    this.openGradeChangeModel = false;

  };

  public validateGradeValues() {
    this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'newGrade')[0].list = [];
    this.smsService.validateNewGrade(this.currentGradeDetails).subscribe(Response => {
      if (Response.length > 0) {
        this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'newGrade')[0].list = Response.map((x: any) => {
          return {
            displayName: x,
            modelValue: x
          }
        })
      }

    })
  };

  public saveQaReleaseNewGradeChange():any{
    let userId = this.sharedService.loggedInUserDetails.userId;
    let batchId = this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'batchId')[0].value;
    if (this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'newGrade')[0].list.length == 0) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Validate Grade and Chemical combinations`,
        }
      );
    };
    if (this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'newGrade')[0].value == null) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Select New grade From Grade List`,
        }
      );
    }

    if (this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'reason')[0].value == null || this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'reason')[0].value == '') {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
          message: `Please Enter Reason`,
        }
      );
    }
    this.currentGradeDetails.gradeName = this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'newGrade')[0].value
    this.currentGradeDetails.reason = this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'reason')[0].value;
    this.smsService.saveQaReleaseNewGradeChange(userId, batchId, this.currentGradeDetails).subscribe(Response => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success, {
          message: `${Response}`,
        }
      );
      this.closeGradeChangePopup('autoClose');
      this.currentGradeDetails.gradeName = this.filterCriteria.formFields.filter((x: any) => x.ref_key == 'newGrade')[0].list = []
    })
  };

  public getFormRef(event){
    this.formRef = event;
  }

}

