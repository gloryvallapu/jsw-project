import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { SharedService } from 'src/app/shared/services/shared.service';
import { JswCoreService, JswFormComponent } from 'jsw-core';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { Observable, Subject } from 'rxjs';
import { Table } from 'primeng/table';
import { SmsService } from '../../services/sms.service';
import { QaReleaseForms, QAReleaseScreenForm } from '../../form-objects/sms-form-objects';
import { QaReleaseComponent } from './qa-release.component';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { SmsEndPoints } from '../../form-objects/sms-api-endpoint';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';

describe('QaReleaseComponent', () => {
  let component: QaReleaseComponent;
  let fixture: ComponentFixture<QaReleaseComponent>;
  let sharedService;
  let smsService: SmsService;
  let jswService: JswCoreService;
  let jswComponent: JswFormComponent;
  let qaReleaseForms: QaReleaseForms;
  let qaReleaseScreenForm: QAReleaseScreenForm;

  const wCresp = [{
    wcName: "LP1",
    lpScheduleIdList: [305, 489, 508, 384]
  }]

  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [QaReleaseComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers: [
        JswCoreService,
        SmsService,
        CommonApiService,
        JswFormComponent,
        SmsEndPoints,
        API_Constants,
        CommonAPIEndPoints,
        QaReleaseForms,
        QAReleaseScreenForm,
        { provide: ToastrService, useValue: toastrService },
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QaReleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
