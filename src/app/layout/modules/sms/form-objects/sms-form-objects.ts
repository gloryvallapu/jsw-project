import { FieldType } from 'src/assets/enums/common-enum';
export class QaReleaseForms {
  public filterCriteria: object = {
    formName: 'fgradeDetails',
    formFields: [

      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatId',
        label: 'Heat ID',
        placeHolder: 'Heat Id',
        value: null,
        name: 'heatId',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.text,
        colSize: 'col-4',
        isVisible: true,
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'batchId',
        label: 'Batch ID',
        placeHolder: 'batchId',
        value: null,
        name: 'batchId',
        isRequired: true,
        disabled: true,
        fieldType: FieldType.text,
        colSize: 'col-4',
        isVisible: true,
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'curentGrade',
        label: 'Current Grade',
        placeHolder: 'current Grade',
        value: null,
        name: 'currentGrade',
        isRequired: false,
        disabled: true,
        fieldType: FieldType.text,
        colSize: 'col-4',
        isVisible: true,
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'newGrade',
        label: 'New Grade',
        placeHolder: 'new Grade',
        value: null,
        name: 'newGrade',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        list:[],
        colSize: 'col-4',
        isVisible: true,
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'reason',
        label: 'Reason',
        placeHolder: 'reason',
        value: null,
        name: 'reason',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.text,
        colSize: 'col-8',
        isVisible: true,
      },
    ],
  };
  public qaHold: object = {
    formName: 'QA hold',
    formFields: [

      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'heatNo',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.text,
        changeEvent: null,
        colSize: 'col-6',
        isVisible: true,
      },
    ],
  };
}

export class SmallBatchProductionForm {
  public smallBatch: object = {
    formName: 'Small Batch Production',
    formFields: [

      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: 'Unit',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true,
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: null,
        colSize: 'col-6',
        isVisible: true,
      },
    ],
  };
  public qaHold: object = {
    formName: 'QA hold',
    formFields: [

      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-4',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'heatNo',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: null,
        colSize: 'col-4',
        isVisible: true,
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'holdType',
        label: 'Hold Type',
        placeHolder: 'Hold Type',
        value: null,
        name: 'holdType',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: null,
        list: [
          {
            displayName: 'Cond Hold',
            modelValue: 'COND'
          },
          {
            displayName: 'QA Hold',
            modelValue: 'QA'
          }
        ],
        colSize: 'col-4',
        isVisible: true,
      },
    ],
  };
}
export class ConditioningScreenForms{
  public filterCriteriaForms: object ={
    formName: 'Filter Criteria',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'unitId',
        label: 'Unit',
        placeHolder: 'Unit',
        value: null,
        name: 'unit',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: 'dropdownChange',
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'productType',
        label: 'Product Type',
        placeHolder: 'Product type',
        value: null,
        name: 'Product type',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: 'dropdownChange',
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatId',
        label: 'Heat ID',
        placeHolder: 'Heat ID',
        value: null,
        name: 'Heat ID',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: 'dropdownChange',
        list: [],
        colSize: 'col-6 col-md-4 col-lg-3 col-xl-3',
        isVisible: true
      },
 


    ]
  }

}

export class QAReleaseScreenForm {
  public qaReleaseFilterForm: Object = {
    formName: 'Filter Criteria',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'heatNo',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
    ]
 }
}

export class DummyBatch {
  public dummyBatchFilter: Object = {
    formName: 'Filter Criteria',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'heatNo',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true,

      },
    ]
 }
}

export class WtoFScreenForm {
  public WtoFFilterForm: Object = {
    formName: 'Filter Criteria',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'heatNo',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: '',
        colSize: 'col-6',
        isVisible: true
      },
    ]
 }
}

export class BatchTransferForms {
  public batchTransferFilterForm: Object = {
    formName: 'FilterForm',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Unit',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'schdID',
        label: 'Schedule No',
        placeHolder: '',
        value: null,
        name: 'schdID',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'dropdown',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      }
    ]
  };

  public batchTransfer: object = {
    formName: 'Batch Transfer Production',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'wcName',
        label: 'Yard',
        placeHolder: '',
        value: null,
        name: 'wcName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-6',
        isVisible: true
      },
      {
        isError: false,
        errorMsg: '',
        ref_key: 'heatNo',
        label: 'Heat No',
        placeHolder: 'Heat No',
        value: null,
        name: 'heatNo',
        isRequired: false,
        disabled: false,
        fieldType: FieldType.dropdown,
        changeEvent: null,
        colSize: 'col-6',
        isVisible: true,
      },
    ]
  };
  public batchTransferTo: object = {
    formName: 'Batch Transfer To',
    formFields: [
      {
        isError: false,
        errorMsg: '',
        ref_key: 'yardName',
        label: 'Transfer To',
        placeHolder: 'Transfer To',
        value: null,
        name: 'yardName',
        isRequired: true,
        disabled: false,
        fieldType: FieldType.dropdown, //'text',
        list: [],
        changeEvent: 'dropdownChange',
        colSize: 'col-12',
        isVisible: true
      }
    ]
  };
}
