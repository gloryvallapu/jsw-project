export class SmsEndPoints {
  // Conditioning screen Apis
  public readonly getfilterCriteriaDropDownLists = '/conditioning/filterCriteria';
  public readonly getPlanDetails = '/conditioning/batchDetails/';
  public readonly getConditioningTypes = '/conditioning/conditioningTypes';
  public readonly getconditioningName = '/conditioning/conditioningDetails';
  public readonly addConditioning = '/conditioning/addConditioning/';
  public readonly finalizeConditioning = '/conditioning/finalizeConditioning';
  public readonly saveConditioningItmes = '/conditioning/saveConditioning';
  public readonly getConditiongHistory = '/getConditiongHistory';
  public readonly getBalanceDetails = '/getBalanceDetails';
  public readonly getSavedConditioningDetails = '/conditioning/retrieveConditiong';
  public readonly deleteConditioningDetails = '/conditioning/deleteConditioning';
  public readonly getConditioningHistoryDetails = '/conditioning/retrieveConditiongHistory';
  // End of Conditioning Apis

  // small batch Filter screen Apis
  public readonly smallBatchProdFilter = '/smallBatchProd/smallBatchProdFilter';
  public readonly getBatchProductionDetails = '/smallBatchProd/getBatchProductionDetails';
  //public readonly updateCreatedSmallBatches = '/smallBatchProd/updateCreatedSmallBatches';
  public readonly updateCreatedSmallBatches = '/smallBatchProd/updatesCreatedSmallBatches';
  public readonly getValidationFlag='/smallBatchProd/getValidationFlag';
  public readonly getParentChildBatchDetail = '/smallBatchProd/getParentChildBatchDetails';
  public readonly getGeneratedBatchId='/smallBatchProd/generateSmallBatchIds';
  //  End of small batch screen Apis

  //qa hold screen APis
  public readonly qaHoldFilter = '/qahold/qaHoldFilter';
  public readonly getBatchDetails = '/qahold/getBatchDetails';
  public readonly createQaHold = '/qahold/createQaHold'
  public readonly releaseQaHold = '/qahold/releaseQaHold'
  // End of qa hold screen Apis

  // Start QA Release
  public readonly qaReleaseFilter = '/qaRelease/qaReleaseFilter';
  public readonly getQAReleaseBatchDetails = '/qaRelease/BatchDetails';
  public readonly getGradeChemicalDetailsByHeat = '/qaRelease/getGradeChangeData';
  public readonly saveQaReleaseBatchDetails = '/qaRelease/saveQaReleaseBatchDetails';
  public readonly validateNewGrade = '/qaRelease/validateNewGrade';
  public readonly saveQaReleaseNewGradeChange = '/qaRelease/saveNewGradeData'


  // End QA Release


  //Start dummy api end points
public readonly dummyBatchFilter= '/dummyBatch/dummyBatchFilter';
public readonly getDummyBatchDetails= '/dummyBatch/getDummyBatchDetails';
public readonly saveDummyBatchDetails = '/dummyBatch/saveDummyBatchDetails';
  //End dummy api end points
  // W to F Screen APIs
  public readonly wipToFinalFilter = '/wiptofinal/wiptofinalfilter';
  public readonly getBatchDetailsList = '/wiptofinal/getDispatchedBatchDetails';
  public readonly dispatchBatches = '/wiptofinal/dispatchBatchDetail';
  // End of W to F Screen APIs

  // Batch transfer screen End Points
  public readonly getBatchTransferList='/batchTransfer/getFilteredBatches';
  public readonly getYards = '/batchTransfer/getYards';
  public readonly batchTransfer='/batchTransfer';
  public readonly getFilterList = '/batchTransfer/getFilterList'
 // End Of Batch Transfer Screen End Points
}
