import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/app/shared/services/auth-guard.service';
import { smsOperationScreenIds, moduleIds } from 'src/assets/constants/MENU/menu-list';
import { BatchTransferComponent } from './components/batch-transfer/batch-transfer.component';
import { ConditioningScreenComponent } from './components/conditioning-screen/conditioning-screen.component';
import { DummyBatchComponent } from './components/dummy-batch/dummy-batch.component';
import { QaHoldComponent } from './components/qa-hold/qa-hold.component';
import { QaReleaseComponent } from './components/qa-release/qa-release.component';
import { SmallBatchProductionComponent } from './components/small-batch-production/small-batch-production.component';
import { WtofComponent} from './components/wtof/wtof.component';
import { SmsComponent } from './sms.component';

const routes: Routes = [
  {
    path: '',
    component: SmsComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'conditioningScreen',
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.SMS_Operation,
          screenId: smsOperationScreenIds.Conditioning_Screen.id,
        },
      },
      {
        path: 'conditioningScreen',
        component: ConditioningScreenComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.SMS_Operation,
          screenId: smsOperationScreenIds.Conditioning_Screen.id,
        },
      },
      {
        path: 'smallBatchProduction',
        component: SmallBatchProductionComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.SMS_Operation,
          screenId: smsOperationScreenIds.Small_Batch_Production_Screen.id,
        },
      },
      {
        path: 'QAHold',
        component: QaHoldComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.SMS_Operation,
          screenId: smsOperationScreenIds.Qa_Hold_Screen.id,
        },
      },
      {
        path: 'QARelease',
        component: QaReleaseComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.SMS_Operation,
          screenId: smsOperationScreenIds.Qa_Release_Screen.id,
        },
      },
      {
        path: 'dummyBatch',
        component: DummyBatchComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.SMS_Operation,
          screenId: smsOperationScreenIds.Dummy_Batch_Screen.id,
        },
      },
      {
        path: 'wtof',
        component: WtofComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.SMS_Operation,
          screenId: smsOperationScreenIds.WtoF_Screen.id,
        },
        
      },
      {
        path: 'batchTransfer',
        component: BatchTransferComponent,
        canActivate: [AuthGuardService],
        data: {
          moduleId: moduleIds.SMS_Operation,
          screenId: smsOperationScreenIds.Batch_transfer.id,
        },
        
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SmsRoutingModule {}
