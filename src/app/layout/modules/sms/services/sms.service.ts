import {
  Injectable
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';
import {
  API_Constants
} from 'src/app/shared/constants/api-constants';
import {
  map
} from 'rxjs/operators';
import {
  AuthService
} from 'src/app/core/services/auth.service';
import {
  SmsEndPoints
} from '../form-objects/sms-api-endpoint';
import {
  CommonAPIEndPoints
} from 'src/app/shared/constants/common-api-end-points';
import {
  BehaviorSubject
} from 'rxjs';
import { SharedService } from 'src/app/shared/services/shared.service';

@Injectable({
  providedIn: 'root'
})
export class SmsService {
  public baseUrl: string;
  public masterBaseUrl: string;
  constructor(public smsEndPoints:SmsEndPoints,public http: HttpClient, private apiURL: API_Constants, public commonEndPoints: CommonAPIEndPoints,
              public authService: AuthService, public sharedService: SharedService) {
    this.baseUrl = `${this.apiURL.baseUrl}${this.apiURL.smsGateway}`;
    this.masterBaseUrl = `${this.apiURL.baseUrl}${this.apiURL.masterGateWay}`;
  }

// Start Conditioning Screen Api
  public getFilterCriteriaDropDownLists() {
    return this.http.get(`${this.baseUrl}${this.smsEndPoints.getfilterCriteriaDropDownLists}/${this.sharedService.loggedInUserDetails.userId}`).pipe(map((response: any) => response));
  }

  public getPlanDetails(batchId){
    return this.http.get(`${this.baseUrl}${this.smsEndPoints.getPlanDetails}${batchId}`).pipe(map((response: any) => response));
  }

  public getConditioningTypes(){
    return this.http.get(`${this.baseUrl}${this.smsEndPoints.getConditioningTypes}`).pipe(map((response: any) => response));
  }


  public getconditioningName(condType,batchId){
    return this.http.get(`${this.baseUrl}${this.smsEndPoints.getconditioningName}/${condType}/${batchId}`).pipe(map((response: any) => response));
  }

  public addConditioning(batchId,userId){
    return this.http.post(`${this.baseUrl}${this.smsEndPoints.addConditioning}${batchId}/${userId}`,'').pipe(map((response: any) => response));
  }

  public getConditiongHistory(){
    return this.http.get(`${this.baseUrl}${this.smsEndPoints.getConditiongHistory}`).pipe(map((response: any) => response));
  }

  public getConditioningHistoryDetails(batchId: any) {
    return this.http.get(`${this.baseUrl}${this.smsEndPoints.getConditioningHistoryDetails}/${batchId}`).pipe(map((response: any) => response));
  }

  public saveConditioningItmes(userId,batch){
    return this.http.post(`${this.baseUrl}${this.smsEndPoints.saveConditioningItmes}/${userId}`,batch).pipe(map((response: any) => response));
  }

  public deleteSelectedCondi(details){
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });
    return this.http.put(`${this.baseUrl}${this.smsEndPoints.deleteConditioningDetails}/${details.batchId}/${details.holdCtr}/${details.condCtr}/${details.condName}`,'',{ headers, responseType: 'text' }).pipe(map((response: any) => response));
  }

  public finalizeConditioning(userId,batchId){
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });
    return this.http.put(`${this.baseUrl}${this.smsEndPoints.finalizeConditioning}/${batchId}/${userId}`,'',{ headers, responseType: 'text' }).pipe(map((response: any) => response));
  }

  public getBalanceDetails(){
    return this.http.get(`${this.baseUrl}${this.smsEndPoints.getBalanceDetails}`).pipe(map((response: any) => response));
  }

  public getSavedConditioningDetails(batchId){
    return this.http.get(`${this.baseUrl}${this.smsEndPoints.getSavedConditioningDetails}/${batchId}`).pipe(map((response: any) => response));

  }

// End Conditioning Screen Api


// Start SMS Small Batch Production Filter Apis


public smallBatchProdFilter(){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.smallBatchProdFilter}/${this.sharedService.loggedInUserDetails.userId}`).pipe(map((response: any) => response));
}

public getBatchProductionDetails(wcName,headId){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.getBatchProductionDetails}/${wcName}/${headId}`).pipe(map((response: any) => response));
}

// public updateCreatedSmallBatches(userId,reqBody){
//   return this.http.put(`${this.baseUrl}${this.smsEndPoints.updateCreatedSmallBatches}/${userId}`,reqBody).pipe(map((response: any) => response));
// }
public updateCreatedSmallBatches(userId,reqBody){
  // const headers = new HttpHeaders({
  //   'content-type': 'application/json',
  //   'Access-Control-Allow-Origin': '*'
  // });
  return this.http.put(`${this.baseUrl}${this.smsEndPoints.updateCreatedSmallBatches}/${userId}`,reqBody).pipe(map((response: any) => response));
}

public getValidationFlag(){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.getValidationFlag}`).pipe(map((response: any) => response));
}

public getParentChildBatchDetails(wcName, heatId){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.getParentChildBatchDetail}/${wcName}/${heatId}`).pipe(map((response: any) => response));
}

public getSmallBatchIds(heatId, count){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.getGeneratedBatchId}/${heatId}/${count}`).pipe(map((response: any) => response));
}
// End SMS Small Batch Production

//qa hold screen APis
public releaseQaHold(batchId,userId){
  const headers = new HttpHeaders({
    'content-type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  });
  return this.http.put(`${this.baseUrl}${this.smsEndPoints.releaseQaHold}/${userId}/${batchId}`,'',{ headers, responseType: 'text' }).pipe(map((response: any) => response));
}

public qaHoldFilter(){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.qaHoldFilter}/${this.sharedService.loggedInUserDetails.userId}`).pipe(map((response: any) => response));
}

// public getBatchDetails(wcName,headId){
//   return this.http.get(`${this.baseUrl}${this.smsEndPoints.getBatchDetails}/${wcName}/${headId}`).pipe(map((response: any) => response));
// }

//adding condition type new requirement 24-01-2022
public getBatchDetails(wcName,headId, condType){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.getBatchDetails}/${wcName}/${headId}/${condType}`).pipe(map((response: any) => response));
}

public createQaHold(batch, userId, holdType:any){
  return this.http.post(`${this.baseUrl}${this.smsEndPoints.createQaHold}/${userId}/${holdType}`,batch).pipe(map((response: any) => response));

}
// End of qa hold screen Apis

// Start QA Release
public qaReleaseFilter(){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.qaReleaseFilter}/${this.sharedService.loggedInUserDetails.userId }`).pipe(map((response: any) => response));
}

public saveQaReleaseNewGradeChange(userId,batchId,gradeChange){
  const headers = new HttpHeaders({
    'content-type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  });
  return this.http.post(`${this.baseUrl}${this.smsEndPoints.saveQaReleaseNewGradeChange}/${userId}/${batchId}`,gradeChange,{ headers, responseType: 'text' }).pipe(map((response: any) => response));

}

public validateNewGrade(gradeChanges){

  return this.http.put(`${this.baseUrl}${this.smsEndPoints.validateNewGrade}`,gradeChanges).pipe(map((response: any) => response));
};

public getQAReleaseDetails(wcName, heatNo) {
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.getQAReleaseBatchDetails}/${wcName}/${heatNo}`).pipe(map((response: any) => response));
};

public saveQaReleaseBatchDetails(userId,batch){
  const headers = new HttpHeaders({
    'content-type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  });
  return this.http.post(`${this.baseUrl}${this.smsEndPoints.saveQaReleaseBatchDetails}/${userId}`,batch,{ headers, responseType: 'text' }).pipe(map((response: any) => response));
};

public getGradeChemicalDetailsByHeat(heatId,batchId,currentGrade){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.getGradeChemicalDetailsByHeat}/${heatId}/${batchId}/${currentGrade}`).pipe(map((response: any) => response));
}
// End QA Release


//Start Dummy batch Api's

public dummyBatchFilter(){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.dummyBatchFilter}/${this.sharedService.loggedInUserDetails.userId}`).pipe(map((response: any) => response));

}

public getDummyBatchDetails(wcName, heatNo){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.getDummyBatchDetails}/${wcName}/${heatNo}`).pipe(map((response: any) => response));
}

public saveDummyBatchDetails(userId,dummyBatchIdList)
{
  return this.http.put(`${this.baseUrl}${this.smsEndPoints.saveDummyBatchDetails}/${userId}`,dummyBatchIdList, {responseType: 'text' }).pipe(map((response: any) => response));
}

//End of Dummy batch Api's

// Start W to F Screen Apis
public wipToFinalFilter(){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.wipToFinalFilter}/${this.sharedService.loggedInUserDetails.userId}`).pipe(map((response: any) => response));
}

public getBatchDetailsList(wcName,headId){
  return this.http.get(`${this.baseUrl}${this.smsEndPoints.getBatchDetailsList}/${wcName}/${headId}`).pipe(map((response: any) => response));
}

public dispatchBatches(userId, reqBody){
  return this.http.post(`${this.baseUrl}${this.smsEndPoints.dispatchBatches}/${userId}`, reqBody, { responseType: 'text' }).pipe(map((response: any) => response));
}
// End of W to F Screen Apis

//Start Batch Transfer
public getBatchTransferList(wcName, heatNo) {
  return this.http
    .get(
      `${this.baseUrl}${this.smsEndPoints.getBatchTransferList}/${wcName}/${heatNo}`
    )
    .pipe(map((response: any) => response));
}

public getYardList(wcName) {
  return this.http
    .get(`${this.baseUrl}${this.smsEndPoints.getYards}/${wcName}`)
    .pipe(map((response: any) => response));
}

public batchTransfer(inTransitId, userId, reqBody) {
  return this.http
    .post(
      `${this.baseUrl}${this.smsEndPoints.batchTransfer}/${inTransitId}/${userId}`,
      reqBody,
      { responseType: 'text' }
    )
    .pipe(map((response: any) => response));
}


public getFilterList(userId) {
  return this.http
    .get(`${this.baseUrl}${this.smsEndPoints.getFilterList}/${userId}`)
    .pipe(map((response: any) => response));
}


//End Batch Transfer
}
