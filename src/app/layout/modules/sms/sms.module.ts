import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConditioningScreenComponent } from './components/conditioning-screen/conditioning-screen.component';
import { SmsRoutingModule } from './sms-routing.module';
import { SmsComponent } from './sms.component';
import { SmallBatchProductionComponent } from './components/small-batch-production/small-batch-production.component';
import { QaHoldComponent } from './components/qa-hold/qa-hold.component';
import { QaReleaseComponent } from './components/qa-release/qa-release.component';
import { DummyBatchComponent } from './components/dummy-batch/dummy-batch.component';
import { JswFormComponent } from 'projects/jsw-core/src/public-api';
import { SmallBatchProductionForm,ConditioningScreenForms, QAReleaseScreenForm,WtoFScreenForm,DummyBatch, QaReleaseForms, BatchTransferForms } from './form-objects/sms-form-objects';
import { SmsService } from './services/sms.service';
import { SmsEndPoints } from './form-objects/sms-api-endpoint';
import { WtofComponent } from './components/wtof/wtof.component';
import { BatchTransferComponent } from './components/batch-transfer/batch-transfer.component';


@NgModule({
  declarations: [
    SmsComponent,
    ConditioningScreenComponent,
    SmallBatchProductionComponent,
    QaHoldComponent,
    QaReleaseComponent,
    DummyBatchComponent,
    WtofComponent,
    BatchTransferComponent,
  ],
  imports: [CommonModule, SmsRoutingModule, SharedModule],
  providers: [SmallBatchProductionForm,QaReleaseForms,SmsService,SmsEndPoints,
    JswFormComponent,ConditioningScreenForms,
    QAReleaseScreenForm,WtoFScreenForm,DummyBatch,
    BatchTransferForms],
})
export class SmsModule {}
