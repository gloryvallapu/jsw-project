import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { UserEndPoints } from '../form-objects/user-api-endpoints';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { TreeNode } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public baseUrl: string;

  constructor(public http: HttpClient, private apiURL: API_Constants, public userEndPoints: UserEndPoints, public authService: AuthService) {
    this.baseUrl = `${this.apiURL.baseUrl}${this.apiURL.masterGateWay}`;
  }

  // User Master APIs
  // public getLovs(lovCode: any){
  //   return this.http.get(`${this.baseUrl}${this.userEndPoints.getLovValues}/${lovCode}`).pipe(map((response: any) => response));
  // }

  public getRoles(){
    return this.http.get(`${this.baseUrl}${this.userEndPoints.getRoles}`).pipe(map((response:any) => response));
  }

  // public getUnits(){
  //   return this.http.get(`${this.baseUrl}${this.userEndPoints.getUnits}`).pipe(map((response:any) => response));
  // }

  public getUserList(){
    return this.http.get(`${this.baseUrl}${this.userEndPoints.getUserList}`).pipe(map((response:any) => response));
  }

  public createUser(userData: any){
    let httpOptions = new HttpHeaders().set('Authorization', 'Bearer ' + this.authService.getToken());
    // return this.restApi.get(`${this.baseUrl}/getUserRights`, { headers: httpOptions }).pipe(map((response: any) => response));
    return this.http.post(`${this.baseUrl}${this.userEndPoints.createUser}`, userData, { headers: httpOptions }).pipe(map((response: any) => response));
  }

  public updateUser(userData: any){
    return this.http.put(`${this.baseUrl}${this.userEndPoints.updateUser}/${userData.userId}`, userData).pipe(map((response: any) => response));
  }

  public deleteUser(userId: any){
    return this.http.delete(`${this.baseUrl}${this.userEndPoints.deleteUser}/${userId}`, { responseType: 'text'}).pipe(map((response: any) => response));
  }

  public deleteMultipleUsers(userIds: any){
    return this.http.put(`${this.baseUrl}${this.userEndPoints.deleteMultipleUsers}`, userIds, { responseType: 'text'}).pipe(map((response: any) => response));
  }
  // End of User Master APIs

  // Screen Master APIs

  public getAllScreen(){
    return this.http.get(`${this.baseUrl}${this.userEndPoints.getAllScreen}`).pipe(map((response:any) => response));
  }

  public getDistinctParentScreen(){
    return this.http.get(`${this.baseUrl}${this.userEndPoints.getDistinctParentScreen}`).pipe(map((response:any) => response));
  }

  public getSubScreensByParentId(parentId:any){
    return this.http.get(`${this.baseUrl}${this.userEndPoints.getSubScreensByParentId}/${parentId}`).pipe(map((response: any) => response));
  }

  public getFilteredScreenList(parentId: any, subScreenId: any){
    return this.http.get(`${this.baseUrl}${this.userEndPoints.getFilteredScreenList}/${parentId}/${subScreenId}`).pipe(map((response: any) => response));
  }
  public updateScreen(userData: any){
    return this.http.put(`${this.baseUrl}${this.userEndPoints.updateScreen}`, userData).pipe(map((response: any) => response));
  }
  public deleteScreen(screenId: any){
    return this.http.put(`${this.baseUrl}${this.userEndPoints.deleteScreen}/${screenId}`, { responseType: 'text'}).pipe(map((response: any) => response));
  }
  public deleteMulScreens(userIds: any){
    return this.http.put(`${this.baseUrl}${this.userEndPoints.deleteMultipleUsers}`, userIds, { responseType: 'text'}).pipe(map((response: any) => response));
  }

  // End of Screen Master APIs

  // Role Screen Mapping APIs

  public getAllRolesScreen(){
    return this.http.get(`${this.baseUrl}${this.userEndPoints.getAllRolesScreen}`).pipe(map((response:any) => response));
  }

  public getAllScreenDetails(){
    return this.http.get(`${this.baseUrl}${this.userEndPoints.getAllScreen}`).pipe(map((response:any)=> response))
  }

  public createNewRole(roleDetails:any){
    return this.http.post(`${this.baseUrl}${this.userEndPoints.createNewRole}`,roleDetails).pipe(map((response:any)=> response))

  }

  public deleteRole(userID,roleId){
    return this.http.delete(`${this.baseUrl}${this.userEndPoints.deleteRole}${userID}`,roleId)
  }



  public editRole(roleId){
    return this.http.get(`${this.baseUrl}${this.userEndPoints.editRoleDetails}${roleId}`).pipe(map((response:any)=> response))
  }


  public updateRoleMapping(roleDetails){
    return this.http.put(`${this.baseUrl}${this.userEndPoints.updateRoleMapping}`,roleDetails)
    .pipe(map((response:any)=> response))
  }


  public getFiles() {
    return this.http.get<any>('assets/files.json').pipe(map((response:any)=> response))
  }



  // End of Role Screen Mapping APIs

}
