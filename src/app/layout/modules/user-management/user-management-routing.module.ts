import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserManagementComponent } from './user-management.component';
import { UserMasterComponent } from './components/user-master/user-master.component';
import { ScreenMasterComponent } from './components/screen-master/screen-master.component';
import { RoleScreenMappingComponent } from './components/role-screen-mapping/role-screen-mapping.component';
import { AuthGuardService } from 'src/app/shared/services/auth-guard.service';
import { moduleIds, userMgmtScreenIds } from 'src/assets/constants/MENU/menu-list';

const routes: Routes = [
  {
    path: '',
    component: UserManagementComponent,
    children: [
      {
        path: 'userMaster',
        component: UserMasterComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.User_Management, screenId: userMgmtScreenIds.User_Master.id }
      },
      {
        path: 'screenMaster',
        component: ScreenMasterComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.User_Management, screenId: userMgmtScreenIds.Screen_Master.id }
      },
      {
        path: 'roleScreenMapping',
        component: RoleScreenMappingComponent,
        canActivate: [AuthGuardService],
        data: { moduleId: moduleIds.User_Management, screenId: userMgmtScreenIds.Role_Screen_Mapping.id }
      }
    ]
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})
export class UserManagementRoutingModule { }
