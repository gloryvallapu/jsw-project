import { Component, OnInit, ViewChild } from '@angular/core';
import { JswFormComponent } from 'jsw-core';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { RoleMappingForms, ScreenMasterForms, UserMasterForms } from '../../form-objects/user-mgmt-form-objects';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { JswCoreService } from 'projects/jsw-core/src/public-api';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ColumnType, LovCodes, TableType } from 'src/assets/enums/common-enum';
import { UserService } from '../../services/user.service';
import { ScreenMasterComponent } from './screen-master.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConfirmationService } from 'primeng/api';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { UserEndPoints } from '../../form-objects/user-api-endpoints';

describe('ScreenMasterComponent', () => {
  let component: ScreenMasterComponent;
  let fixture: ComponentFixture<ScreenMasterComponent>;
  let screenMasterForms: ScreenMasterForms;
    let jswComponent: JswFormComponent;
    let userService: UserService;
    let sharedService: SharedService;
    let jswService: JswCoreService;
    let commonService: CommonApiService;
    let formObjects : ScreenMasterForms
    const toastrService = {
      success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
      error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
    };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScreenMasterComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        ConfirmationService,
        CommonApiService,
        API_Constants,
        CommonAPIEndPoints,
        JswFormComponent,
        UserMasterForms,
        ScreenMasterForms,
        RoleMappingForms,
        UserEndPoints,
        UserService ,
        { provide: ToastrService, useValue: toastrService },
      ]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenMasterComponent);
    component = fixture.componentInstance;
    userService = fixture.debugElement.injector.get(UserService);
    sharedService = fixture.debugElement.injector.get(SharedService);
    formObjects = fixture.debugElement.injector.get(ScreenMasterForms);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
