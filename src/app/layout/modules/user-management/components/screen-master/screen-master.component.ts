import { Component, OnInit } from '@angular/core';
import { JswFormComponent } from 'jsw-core';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { ScreenMasterForms } from '../../form-objects/user-mgmt-form-objects';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { JswCoreService } from 'projects/jsw-core/src/public-api';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ColumnType, LovCodes, TableType } from 'src/assets/enums/common-enum';
import { UserService } from '../../services/user.service';
import {
  ScreenForm,
  Screen,
  ScreenWorkCenterBO,
  WorkCenterBoList,
} from '../../interfaces/screen-master-interface';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-screen-master',
  templateUrl: './screen-master.component.html',
  styleUrls: ['./screen-master.component.css'],
})
export class ScreenMasterComponent implements OnInit {
  screenList: any = [];
  filterFormObject: any;
  editScreenFormObject: any;
  tableType: any;
  columns: ITableHeader[] = [];
  viewType = ComponentType;
  showScreenForm: boolean = false;
  isEditUser: boolean = false;
  userList: any = [];
  editScreenMaster: any[];
  units: any = [];
  globalFilterArray: any = [];
  screenForm: ScreenForm;
  recordToBeEdited: any;
  public screenStructure: any = [];
  public isRetrieveBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isEditBtn: boolean = false;
  public isUpdateBtn: boolean = false;
  public isCancelBtn: boolean = false;
  public menuConstants = menuConstants;
  public formRef: NgForm;
  public editForm: NgForm;
  public tableRef: any;
  constructor(
    public screenMasterForms: ScreenMasterForms,
    public jswComponent: JswFormComponent,
    public userService: UserService,
    public sharedService: SharedService,
    public jswService: JswCoreService,
    public commonService: CommonApiService,
    public authService: AuthService
  ) {
    this.screenForm = new ScreenForm();
  }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.User_Management)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.userMgmtScreenIds.Screen_Master.id)[0];
    this.isRetrieveBtn = this.sharedService.isButtonVisible(true, menuConstants.screenMasterSectionIds.Filter_Criteria.buttons.retrieve, menuConstants.screenMasterSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.screenMasterSectionIds.Filter_Criteria.buttons.reset, menuConstants.screenMasterSectionIds.Filter_Criteria.id, this.screenStructure);
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.screenMasterSectionIds.List.buttons.edit, menuConstants.screenMasterSectionIds.List.id, this.screenStructure);
    this.isUpdateBtn = this.sharedService.isButtonVisible(true, menuConstants.screenMasterSectionIds.Edit_Record.buttons.update, menuConstants.screenMasterSectionIds.Edit_Record.id, this.screenStructure);
    this.isCancelBtn = this.sharedService.isButtonVisible(true, menuConstants.screenMasterSectionIds.Edit_Record.buttons.cancel, menuConstants.screenMasterSectionIds.Edit_Record.id, this.screenStructure);
    this.filterFormObject =JSON.parse(JSON.stringify( this.screenMasterForms.addScreenForm));
    this.editScreenFormObject =JSON.parse(JSON.stringify( this.screenMasterForms.editScreenMasterForm));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getAllScreen();
    this.getDistinctParentScreen();
    this.getUnits();
    this.getLovs(LovCodes.status, 'status');
    this.getLovs(LovCodes.screen_type, 'screenType');

    this.columns = [
      // {
      //   field: 'checkbox',
      //   header: '',
      //   columnType: ColumnType.checkbox,
      //   width: '20px',
      //   sortFieldName: '',
      // },
      {
        field: 'screenId',
        header: 'Screen ID',
        columnType: ColumnType.uniqueKey,
        width: '120px',
        sortFieldName: 'screenID',
      },
      {
        field: 'screenName',
        header: 'Screen Name',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'parentScreenName',
        header: 'Parent Screen Name',
        columnType: ColumnType.string,
        width: '180px',
        sortFieldName: '',
      },
      {
        field: 'screenType',
        header: 'Screen Type Name',
        columnType: ColumnType.string,
        width: '160px',
        sortFieldName: '',
      },
      {
        field: 'roleData',
        header: 'Buttons',
        columnType: ColumnType.tooltip,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'unitData',
        header: 'Units',
        columnType: ColumnType.string,
        width: '200px',
        sortFieldName: '',
      },
      {
        field: 'status',
        header: 'Status',
        columnType: ColumnType.string,
        width: '120px',
        sortFieldName: '',
      },
      {
        field: 'action',
        header: 'Action',
        columnType: ColumnType.action,
        width: '120px',
        sortFieldName: '',
      },
    ];
    this.globalFilterArray = this.columns.map((element) => {
      return element.field;
    });
  }

  getFilteredScreenData() {
    this.jswService.setFormData(this.filterFormObject.formFields);
    this.jswComponent.onSubmit(
      this.filterFormObject.formName,
      this.filterFormObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData[0].isError == false && formData[1].isError == false) {
      let parentScreenId = this.filterFormObject.formFields.filter(
        (ele: any) => ele.ref_key == 'parentScreenName'
      )[0].value;
      let subScreenId = this.filterFormObject.formFields.filter(
        (ele: any) => ele.ref_key == 'subScreen'
      )[0].value;
      this.userService
        .getFilteredScreenList(parentScreenId, subScreenId)
        .subscribe((data) => {
          this.setScreenData(data);
          this.screenList = data;
        });
    }
  }

  public getFilterCriteriaDetails() {
    this.jswService.setFormData(this.editScreenFormObject.formFields); // To set formData I used this line of code
    this.jswComponent.onSubmit(
      this.filterFormObject.formName,
      this.filterFormObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(
        formData,
        this.screenForm.getEditScreenForm()
      );

      // let workCenters = [];
      var screenWorkCenterBO: ScreenWorkCenterBO = {
        screenId: this.recordToBeEdited.screenId,
        workCenterBoList: [],
      };
      formObj.unit.forEach((unitId: any) => {
        let unit = this.units.filter(
          (unit: any) => unit.wcShortCode == unitId
        )[0];
        if (unit) {
          let wcBO: WorkCenterBoList = {
            wcName: unit.wcName,
            wcShortCode: unit.wcShortCode,
          };
          screenWorkCenterBO.workCenterBoList.push(wcBO);
        }
      });

      let reqBody: Screen = {
        parentId: this.recordToBeEdited.parentId,
        screenId: this.recordToBeEdited.screenId,
        screenName: formObj.screenName,
        screenType: formObj.screenType,
        screenWorkCenterBoList: [screenWorkCenterBO],
        status: formObj.status,
        userId: this.sharedService.loggedInUserDetails.userId,
        moduleName: this.recordToBeEdited.moduleNameId,
        screenObjective: this.recordToBeEdited.screenObjective,
        createdDate: this.recordToBeEdited.createdDate,
        createdBy: this.recordToBeEdited.createdBy
      };
      this.updateScreen(reqBody);
    }
  }

  public deleteScreen(record: any) {
    this.userService.deleteScreen(record.screenId).subscribe((data) => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        {
          message:
            'screen' +
            (record ? ' - ' + record.screenId : '') +
            ' deleted successfully!',
        }
      );
      this.getAllScreen();
    });
  }

  public deleteMultipleRecords(data: any) {
    let userIds = data.map((ele: any) => {
      return ele.screenId;
    });
    this.userService.deleteMulScreens(userIds).subscribe((data) => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        { message: 'Selected records deleted successfully!' }
      );
      this.getAllScreen();
    });
  }

  public editRecord(data: any) {
    //this.resetFilter(this.editForm)
    this.toggleForm();
    if(!this.showScreenForm){
      this.sharedService.resetForm(this.editScreenFormObject,this.editForm)
    }
    this.recordToBeEdited = data;
    this.isEditUser = true;
    this.showScreenForm = true;
    this.editScreenFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'screenId'
    )[0].disabled = true;
    this.editScreenFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'screenName'
    )[0].disabled = false;
    this.editScreenFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'parentScreenName'
    )[0].disabled = true;
    this.editScreenFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'screenType'
    )[0].disabled = false;
    this.editScreenFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'screenObjectBoList'
    )[0].disabled = true;
    this.sharedService.mapResponseToFormObj(
      this.editScreenFormObject.formFields,
      data
    );
    let unit = this.editScreenFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'unit'
    )[0];
    unit.selectedItems = data.screenWorkCenterBoList.map((ele: any) => {
      return ele.wcName;
    });
    let btnArray = data.screenObjectBoList.map((btnObj: any) => {
      return btnObj.objectName;
    });
    this.editScreenFormObject.formFields.filter(
      (ele: any) => ele.ref_key == 'screenObjectBoList'
    )[0].value = btnArray.join(',');
    
  }

  public getAllScreen() {
    this.userService.getAllScreen().subscribe((data) => {
      this.setScreenData(data);
      this.screenList = data;
    });
  }

  public setScreenData(data) {
    data.map((ele: any) => {
      let roleArr = ele.screenObjectBoList.map((role: any) => {
        return role.objectName;
      });
      ele.roleData = roleArr.join(',');
      let unitArr = ele.screenWorkCenterBoList.map((unit: any) => {
        return unit.wcName;
      });
      ele.unitData = unitArr.join(',');
      return ele;
    });
    this.screenList = data;
  }

  public getDistinctParentScreen() {
    this.userService.getDistinctParentScreen().subscribe((data) => {
      this.filterFormObject.formFields.filter(
        (obj: any) => obj.ref_key == 'parentScreenName'
      )[0].list = data.map((x: any) => {
        return { displayName: x.parentScreenName, modelValue: x.parentId };
      });
    });
  }

  public getSubScreensByParentId(parentId: any) {
    this.userService.getSubScreensByParentId(parentId).subscribe((data) => {
      this.filterFormObject.formFields.filter(
        (obj: any) => obj.ref_key == 'subScreen'
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.screenName,
          modelValue: x.screenId,
        };
      });
    });
  }

  public updateScreen(reqBody: any) {
    this.userService.updateScreen(reqBody).subscribe((data) => {
      this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Success,
        {
          message:
            'Screen' +
            (data ? '-' + data.screenName : '') +
            ' updated successfully!',
        }
      );
      this.resetFilter('resetEditscreen');
    });
  }

  public getUnits() {
    this.commonService.getWorkCenterList().subscribe((data) => {
      this.units = data;
      this.editScreenFormObject.formFields.filter(
        (obj: any) => obj.ref_key == 'unit'
      )[0].list = data.map((x: any) => {
        return {
          displayName: x.wcDesc,
          modelValue: x.wcShortCode,
        };
      });
    });
  }

  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe((data) => {
      let lovData = data.map((x: any) => {
        return {
          displayName: x.valueDescription,
          modelValue: x.valueShortCode,
        };
      });
      this.editScreenFormObject.formFields.filter(
        (obj: any) => obj.ref_key == refKey
      )[0].list = refKey == 'screenType' ? lovData.filter(ele => {return ele.modelValue != 'Button' && ele.modelValue != 'Field'}) : lovData;
    });
  }

  public changeEvent(data) {
    if (data.value) {
      this.getSubScreensByParentId(data.value);
    }
  }

  public resetFilter(retrievescrren) {
    if (retrievescrren == 'resetRetrieveScreen') {
      this.sharedService.resetForm(this.filterFormObject,this.formRef);
      this.sharedService.resetForm(this.editScreenFormObject,this.editForm);
      //this.jswComponent.resetForm(this.filterFormObject);
    } else {
      this.isEditUser = false;
      this.sharedService.resetForm(this.editScreenFormObject,this.formRef);
      //this.jswComponent.resetForm(this.editScreenFormObject);
    }
    this.getAllScreen();
  }

  toggleForm() {
    this.showScreenForm = !this.showScreenForm;
  }

  public navigateToMainScreen() {
    this.toggleForm();
    this.resetFilter('resetRetrieveScreen');
    this.getAllScreen();
  }
  public getFormRef(event){
    this.formRef = event;
  }

  public getEditFormRef(event){
    this.editForm = event;
  }

  public getTableRef(event){
    this.tableRef = event;
  }
  public filterTable(event){
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }
}
