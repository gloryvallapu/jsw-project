import {
  Component,
  OnInit,
} from '@angular/core';
import {
  JswCoreService
} from 'jsw-core';
import {
  JswFormComponent
} from 'jsw-core';
import {
  ComponentType,
  FieldType,
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  ITableHeader
} from 'src/assets/interfaces/table-headers';
import {
  TableType,
  ColumnType,
  LovCodes
} from 'src/assets/enums/common-enum';
import {
  UserService
} from '../../services/user.service';
import {
  RoleMappingForms
} from '../../form-objects/user-mgmt-form-objects';
import {
  ToastrService
} from 'ngx-toastr';
import {
  CommonApiService
} from 'src/app/shared/services/common-api.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { AuthService } from 'src/app/core/services/auth.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-role-screen-mapping',
  templateUrl: './role-screen-mapping.component.html',
  styleUrls: ['./role-screen-mapping.component.css'],
})
export class RoleScreenMappingComponent implements OnInit {
  public formObject: any;
  public tableType: any;
  public columns: ITableHeader[] = [];
  public viewType = ComponentType;
  public roleList: any = [];
  public display: boolean = false;
  public modules: any = [];
  public activeModule: any;
  public screenMap: any;
  public moduleDetails: any;
  public selectedFiles2: any = []
  public selectedFiles2Hold: any;
  public moduleList: any;
  public screenDetails: any;
  public roleDetails: any = [];
  public createRole: boolean = true;
  public saveAction: boolean = true;
  public updateAction: boolean = false;
  public roleId: any;
  subMenuItems: any;
  public holdAlreadySelectedData: any;
  public onholdData: any = [];
  hiarchyObject: any;
  objectTree: any;
  public screenStructure: any = [];
  public isCreateBtn: boolean = false;
  public isEditBtn: boolean = false;
  public isSaveBtn: boolean = false;
  public isCloseCreateModalBtn: boolean = false;
  public isUpdateBtn: boolean = false;
  public isCloseUpdateModalBtn: boolean = false;
  public menuConstants = menuConstants;
  public formRef: NgForm;
  public tableRef: any;
  constructor(
    public toastr: ToastrService,
    public roleMappingForms: RoleMappingForms,
    public jswService: JswCoreService,
    public sharedService: SharedService,
    public userService: UserService,
    public jswComponent: JswFormComponent,
    public commonService: CommonApiService,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.User_Management)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.userMgmtScreenIds.Role_Screen_Mapping.id)[0];
    this.isCreateBtn = this.sharedService.isButtonVisible(true, menuConstants.roleScreenMappingSectionIds.List.buttons.createRole, menuConstants.roleScreenMappingSectionIds.List.id, this.screenStructure);
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.roleScreenMappingSectionIds.List.buttons.edit, menuConstants.roleScreenMappingSectionIds.List.id, this.screenStructure);
    this.isSaveBtn = this.sharedService.isButtonVisible(true, menuConstants.roleScreenMappingSectionIds.Add_New_Record.buttons.save, menuConstants.roleScreenMappingSectionIds.Add_New_Record.id, this.screenStructure);
    this.isCloseCreateModalBtn = this.sharedService.isButtonVisible(true, menuConstants.roleScreenMappingSectionIds.Add_New_Record.buttons.close, menuConstants.roleScreenMappingSectionIds.Add_New_Record.id, this.screenStructure);
    this.isUpdateBtn = this.sharedService.isButtonVisible(true, menuConstants.roleScreenMappingSectionIds.Edit_Record.buttons.update, menuConstants.roleScreenMappingSectionIds.Edit_Record.id, this.screenStructure);
    this.isCloseUpdateModalBtn = this.sharedService.isButtonVisible(true, menuConstants.roleScreenMappingSectionIds.Edit_Record.buttons.close, menuConstants.roleScreenMappingSectionIds.Edit_Record.id, this.screenStructure);
    this.formObject = JSON.parse(JSON.stringify(this.roleMappingForms.createRole));
    this.getAllRolesScreen();
    this.getAllScreens();
    this.getStatusDetails();
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.columns = [
      // {
      //   field: 'checkbox',
      //   header: '',
      //   columnType: ColumnType.checkbox,
      //   width: '30px',
      //   sortFieldName: '',
      // },
      {
        field: 'roleDescription',
        header: 'Role(s)',
        columnType: ColumnType.string,
        width: '250px',
        sortFieldName: '',
      },
      {
        field: 'screenData',
        header: 'Pages/Modules',
        columnType: ColumnType.tooltip,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'status',
        header: 'Status',
        columnType: ColumnType.string,
        width: '150px',
        sortFieldName: '',
      },
      {
        field: 'action',
        header: 'Action',
        columnType: ColumnType.action,
        width: '150px',
        sortFieldName: '',
      },
    ];
  }

  public getAllRolesScreen() {
    this.userService.getAllRolesScreen().subscribe((data) => {
      this.setScreenData(data);
    });
  }

  public setScreenData(data) {
    data.map((ele: any) => {
      ele.screenData = ele.screenList.join(',');
      return ele;
    });
    this.roleList = data;
  }

  public showDialog(event) {
    this.display = event;
  }

  public getAllScreens() {
    this.userService.getAllScreenDetails().subscribe((Response) => {
      if (Response != null && Response.length > 0) {
        this.screenDetails = Response;
        this.formObject.formFields.filter(
          (x: any) => x.ref_key == 'selectModule'
        )[0].list = Response.filter(
          (x: any) => x.screenType == 'Main_Menu'
        ).map((ele: any) => {
          return {
            displayName: ele.screenName,
            modelValue: ele.screenName,
          };
        });
      }
      this.createTreeStructure()
    });
  };

  public getStatusDetails() {
    this.commonService.getLovs('status').subscribe((Response) => {
      this.formObject.formFields.filter(
        (x: any) => x.ref_key == 'status'
      )[0].list = Response.map((ele: any) => {
        return {
          displayName: ele.valueDescription,
          modelValue: ele.valueShortCode,
        };
      });
    });
  };

  public mapEditedDataToscreeen(data) {
    var temp = [];
    (data.map((x: any) => {
      var a = this.objectTree.filter((y: any) => y.screenName == x.moduleName)
      //if module value 0 it will break here.... verify response if nothing is selected in tree
      var submenulist = a[0].children.filter((p: any) => p.screenName == x.parentScreenName)
      var object = a[0].children.filter((p: any) => p.screenName == x.parentScreenName)
      if (object.length > 0) {
        temp.push(a[0].children.filter((p: any) => p.screenName == x.parentScreenName)[0]);
      }
      var b = submenulist.map((l: any) => {
        let action = l.children.filter((m: any) => m.screenName == x.screenName)
        temp.push(action[0])
        action[0].children.filter(button => {
          (x.roleScreenObjectBoList.forEach(element => {
            if (element.objectName == button.objectName) {
              temp.push(button)
            }
          }))
        });
        return action[0].children
      })
      return a[0].children
    }))
    this.selectedFiles2 = this.selectedFiles2.concat(temp)
  };

  public editRecord(event) {
    if (!this.sharedService.isSectionVisible(menuConstants.roleScreenMappingSectionIds.Edit_Record.id, this.screenStructure)) {
      this.display = false;
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'You are not authorized to access Edit Role section. Please contact Admin' });
      return;
    }
    this.userService.editRole(event.roleId).subscribe((Response) => {
      if (Response != null) {
        this.formObject.formFields.filter(
          (ele: any) => ele.ref_key == 'roleDescription'
        )[0].disabled = true;
        this.sharedService.mapResponseToFormObj(
          this.formObject.formFields,
          Response
        );
        let selectModule = this.formObject.formFields.filter(
          (ele: any) => ele.ref_key == 'selectModule'
        )[0];
        selectModule.selectedItems = event.screenList;
        this.modules = event.screenList.map((module: any) => {
          return {
            label: module,
          };
        });
        this.modules = this.modules.filter((mod: any) => mod.label != null)
        this.activeModule = this.modules[0];
        this.display = true;
        this.roleId = event.roleId;
        this.updateAction = true;
        this.saveAction = false;
        let value = {
          target: {
            innerText: this.activeModule.label,
          },
        };
        this.tabMenuChange(value);
        this.mapEditedDataToscreeen(Response.roleScreenBoList)
      }
    });
  };

  public getMultiSelectedData(data) {
    this.modules = [];
    this.modules = data.selectedItems.map((x: any) => {
      return {
        label: x,
      };
    });
    this.activeModule = this.modules[this.modules.length - 1];
    if (this.modules.length > 0) {
      let value = {
        target: {
          innerText: this.modules[this.modules.length - 1].label,
        },
      };
      this.tabMenuChange(value);
    }
  };

  public createTreeStructure() {
    let mainMenu = this.screenDetails.filter((x: any) => x.screenType == 'Main_Menu');
    let screens = mainMenu.map((x: any) => {
      x.screenObjectBoList = this.screenDetails.filter((y: any) => y.screenType == 'Sub_Menu' && y.moduleName == x.screenName)
      return x
    })
    this.hiarchyObject = screens.map((x: any) => {
      x.screenObjectBoList.map((y: any) => {
        y.screenObjectBoList = this.screenDetails.filter((z: any) => z.parentScreenName == y.screenName && z.screenType == 'Screen')
      })
      return x
    })
    this.objectTree = this.createTreeObjects()
  };

  public modifyButtonLevelObject(details, moduleName) {
    return details.map((x: any) => {
      x.label = x.objectName,
        x.moduleName = moduleName,
        x.buttonlevel = true,
        x.isactionLevel = false,
        x.isSubMenuLevel = false,
        x.data = 'Work Folder',
        x.expandedIcon = 'pi pi-folder-open',
        x.collapsedIcon = 'pi pi-folder',
        x.highlightState = 1,
        x.partialSelected = false,
        x.parentId = x.parentId
      return x
    })
  };

  public modifyActionLevelObject(details, moduleName) {
    return details.map((x: any) => {
      x.label = x.screenName,
        x.moduleName = moduleName,
        x.buttonlevel = false,
        x.isactionLevel = true,
        x.isSubMenuLevel = false,
        x.data = 'Work Folder',
        x.expandedIcon = 'pi pi-folder-open',
        x.collapsedIcon = 'pi pi-folder',
        x.highlightState = 1,
        x.partialSelected = false,
        x.parentId = x.parentId,
        x.children = this.modifyButtonLevelObject(x.screenObjectBoList, moduleName)
      return x
    })
  };

  public modifySubMenuCgild(details, moduleName) {
    return details.map((x: any) => {
      x.label = x.screenName,
        x.moduleName = moduleName,
        x.buttonlevel = false,
        x.isactionLevel = false,
        x.isSubMenuLevel = true,
        x.data = 'Work Folder',
        x.expandedIcon = 'pi pi-folder-open',
        x.collapsedIcon = 'pi pi-folder',
        x.highlightState = 1,
        x.partialSelected = false,
        x.parentId = x.parentId,
        x.children = x.screenObjectBoList && x.screenObjectBoList.length > 0 ? this.modifyActionLevelObject(x.screenObjectBoList, moduleName) : []
      return x
    })
  };

  public createTreeObjects() {
    return this.hiarchyObject.map((x: any) => {
      x.label = x.screenName,
        x.data = 'Work Folder',
        x.expandedIcon = 'pi pi-folder-open',
        x.collapsedIcon = 'pi pi-folder',
        x.highlightState = 1,
        x.partialSelected = false,
        x.parentId = x.parentId,
        x.children = this.modifySubMenuCgild(x.screenObjectBoList, x.label)
      return x
    })

  };

  public tabMenuChange(value) {
    this.screenMap = this.objectTree.filter((x: any) => x.label == value.target.innerText)[0].children
  };

  public nodeSelect(event) {

  }

  public nodeUnselect(event) {

  }

  public initObjWithoutChild() {
    return {
      screenId: null,
      parentId: null,
      parentScreenName: null,
      moduleName: null,
      screenName: null,
      screenType: null,
      status: null,
      roleScreenObjectBoList: [],
      modifiedBy: this.sharedService.loggedInUserDetails.userId,
      modifiedDt: new Date(),
      moduleNameId: null,
      validFlagYN: 'Y'
    }
  }


  public buildObjectWithChildBody(childObject) {
    return childObject.map((element: any) => {
      return {
        screenId: element.screenId,
        objectName: element.objectName,
        objectType: element.objectType,
        validFlagYN: 'Y',
        status: element.status,
        selected: true
      }
    })
  };

  public createRequestBody(): any {
    return this.selectedFiles2.map((element: any): any => {
      if (element.isSubMenuLevel == true && element.children && element.children.length > 0) {
        var reqQbject = this.initObjWithoutChild();
        reqQbject.screenId = element.screenId
        reqQbject.parentId = element.parentId
        reqQbject.parentScreenName = element.parentScreenName,
          reqQbject.moduleName = element.moduleName,
          reqQbject.screenName = element.screenName,
          reqQbject.screenType = element.screenType,
          reqQbject.status = element.status,
          reqQbject.moduleNameId = element.moduleNameId,
          reqQbject.roleScreenObjectBoList = []
        return reqQbject
      };

      if (element.isactionLevel == true && element.children && element.children.length == 0) {
        var reqQbject = this.initObjWithoutChild();
        reqQbject.screenId = element.screenId
        reqQbject.parentId = element.parentId
        reqQbject.parentScreenName = element.parentScreenName,
          reqQbject.moduleName = element.moduleName,
          reqQbject.screenName = element.screenName,
          reqQbject.screenType = element.screenType,
          reqQbject.status = element.status,
          reqQbject.moduleNameId = element.moduleNameId
        reqQbject.roleScreenObjectBoList = []
        return reqQbject
      };

      if (element.isactionLevel == true && element.children && element.children.length > 0) {
        var reqQbject = this.initObjWithoutChild();
        reqQbject.screenId = element.screenId
        reqQbject.parentId = element.parentId
        reqQbject.parentScreenName = element.parentScreenName,
          reqQbject.moduleName = element.moduleName,
          reqQbject.screenName = element.screenName,
          reqQbject.screenType = element.screenType,
          reqQbject.status = element.status,
          reqQbject.moduleNameId = element.moduleNameId
        reqQbject.roleScreenObjectBoList = this.buildObjectWithChildBody(element.children)
        return reqQbject
      }
      if (element.buttonlevel == true && element.parent) {
        var reqQbject = this.initObjWithoutChild();
        var itsParentDetails = element.parent;
        reqQbject = this.buildReqObjectWithoutChild(reqQbject, itsParentDetails, element)
        return reqQbject
      }
    });
  };

  public buildReqObjectWithoutChild(reqQbject, itsParentDetails, element) {
    reqQbject.screenId = itsParentDetails.screenId
    reqQbject.parentId = itsParentDetails.parentId
    reqQbject.parentScreenName = itsParentDetails.parentScreenName,
      reqQbject.moduleName = itsParentDetails.moduleName,
      reqQbject.screenName = itsParentDetails.screenName,
      reqQbject.screenType = itsParentDetails.screenType,
      reqQbject.status = itsParentDetails.status,
      reqQbject.moduleNameId = element.parent.parent.moduleNameId
    reqQbject.roleScreenObjectBoList = [{
      screenId: element.screenId,
      objectName: element.objectName,
      objectType: element.objectType,
      validFlagYN: 'Y',
      status: element.status,
      selected: true
    }];
    return reqQbject
  };

  public save(): any {
    this.roleDetails = this.selectedFiles2
    this.jswComponent.onSubmit(
      this.formObject.formName,
      this.formObject.formFields
    );
    var formData = this.jswService.getFormData();
    if (formData == undefined) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: 'Please Enter Required Fields',
      }
      );
    };
    var newObject = {
      roleDescription: formData.filter(
        (roleDetails: any) => roleDetails.ref_key == 'roleDescription'
      )[0].value,
      roleId: 0,
      roleScreenBoList: this.createRequestBody().filter((x: any) => x != null),
      userId: this.sharedService.loggedInUserDetails.userId,
    };
    var reqBodyObject = [];
    (this.modules.map((y: any) => {
      newObject.roleScreenBoList.filter((v: any) => v.moduleName == y.label).map((x: any) => {
        reqBodyObject.push(x)
      })
      return newObject.roleScreenBoList.filter((v: any) => v.moduleName == y.label)
    }))
    newObject.roleScreenBoList = reqBodyObject
    this.userService.createNewRole(newObject).subscribe((Response) => {
      if (Response != null) {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success, {
          message: 'New Role created successfully!',
        }
        );
        this.getAllRolesScreen();
        this.getAllScreens();
        this.closeModal();
      }
    });
  }

  public update(): any {
    this.selectedFiles2.map((x: any) => {
      return x.label;
    })
    this.selectedFiles2.map((x: any) => {
      x.partialSelected = 'Y';
      return x;
    });
    this.roleDetails = this.createRequestBody().filter((x: any) => x != null),
      this.jswComponent.onSubmit(
        this.formObject.formName,
        this.formObject.formFields
      );
    var formData = this.jswService.getFormData();
    if (formData == undefined) {
      return this.sharedService.displayToastrMessage(
        this.sharedService.toastType.Warning, {
        message: 'Please Enter Required Fields',
      }
      );
    };
    var newObject = {
      status: formData.filter(
        (roleDetails: any) => roleDetails.ref_key == 'status'
      )[0].value,
      roleDescription: formData.filter(
        (roleDetails: any) => roleDetails.ref_key == 'roleDescription'
      )[0].value,
      roleId: this.roleId,
      roleScreenBoList: this.roleDetails,
      userId: this.sharedService.loggedInUserDetails.userId,
    };
    var reqBodyObject = [];
    (this.modules.map((y: any) => {
      newObject.roleScreenBoList.filter((v: any) => v.moduleName == y.label).map((x: any) => {
        reqBodyObject.push(x)
      })
      return newObject.roleScreenBoList.filter((v: any) => v.moduleName == y.label)
    }))
    newObject.roleScreenBoList = reqBodyObject
    this.userService.updateRoleMapping(newObject).subscribe((Response) => {
      if (Response != null) {
        this.sharedService.displayToastrMessage(
          this.sharedService.toastType.Success, {
          message: 'Role updated successfully!',
        }
        );
        this.getAllRolesScreen();
        this.getAllScreens();
        this.closeModal();
      }
    });

  }

  public closeModal() {
    this.updateAction = false;
    this.saveAction = true;
    this.display = false;
    //this.sharedService.resetForm(this.formObject,this.formRef)
    //this.jswComponent.resetForm(this.formObject);
    this.modules = [];
    this.selectedFiles2 = [];
    this.formObject.formFields.filter(
      (ele: any) => ele.ref_key == 'roleDescription'
    )[0].disabled = false;
  }
  public getFormRef(event) {
    this.formRef = event;
  }
  public getTableRef(event) {
    this.tableRef = event;
  }
  public filterTable(event) {
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }


}
