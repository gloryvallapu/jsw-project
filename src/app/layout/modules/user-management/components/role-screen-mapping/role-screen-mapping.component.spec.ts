import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  JswCoreService
} from 'jsw-core';
import {
  JswFormComponent
} from 'jsw-core';
import {
  ComponentType,
  FieldType,
} from 'projects/jsw-core/src/lib/enum/common-enum';
import {
  SharedService
} from 'src/app/shared/services/shared.service';
import {
  ITableHeader
} from 'src/assets/interfaces/table-headers';
import {
  TableType,
  ColumnType,
  LovCodes
} from 'src/assets/enums/common-enum';
import {
  UserService
} from '../../services/user.service';
import {
  RoleMappingForms, ScreenMasterForms, UserMasterForms
} from '../../form-objects/user-mgmt-form-objects';
import {
  IndividualConfig,
  ToastrService
} from 'ngx-toastr';
import {
  CommonApiService
} from 'src/app/shared/services/common-api.service';
import { RoleScreenMappingComponent } from './role-screen-mapping.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { ConfirmationService } from 'primeng/api';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { UserEndPoints } from '../../form-objects/user-api-endpoints';
describe('RoleScreenMappingComponent', () => {
  let component: RoleScreenMappingComponent;
  let fixture: ComponentFixture<RoleScreenMappingComponent>;
  let toastr: ToastrService;
    let roleMappingForms: RoleMappingForms;
    let jswService: JswCoreService;
    let sharedService: SharedService;
    let userService: UserService;
    let jswComponent: JswFormComponent;
    let commonService: CommonApiService;
    let formObjects : RoleMappingForms
    const toastrService = {
      success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
      error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
    };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoleScreenMappingComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        ConfirmationService,
        CommonApiService,
        API_Constants,
        CommonAPIEndPoints,
        JswFormComponent,
        UserMasterForms,
        ScreenMasterForms,
        RoleMappingForms,
        UserEndPoints,
        UserService ,
        { provide: ToastrService, useValue: toastrService },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleScreenMappingComponent);
    component = fixture.componentInstance;
    userService = fixture.debugElement.injector.get(UserService);
    sharedService = fixture.debugElement.injector.get(SharedService);
    formObjects = fixture.debugElement.injector.get(RoleMappingForms);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
