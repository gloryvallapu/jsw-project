import { Component, OnInit, ViewChild } from '@angular/core';
import { JswCoreService } from 'jsw-core';
import { JswFormComponent } from 'jsw-core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { UserMasterForms } from '../../form-objects/user-mgmt-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { UserService } from '../../services/user.service';
import { User, UserForm } from '../../interfaces/user-master-interface';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { AuthService } from 'src/app/core/services/auth.service';
import * as menuConstants from 'src/assets/constants/MENU/menu-list';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-user-master',
  templateUrl: './user-master.component.html',
  styleUrls: ['./user-master.component.css']
})
export class UserMasterComponent implements OnInit {
  formObject: any;
  tableType: any;
  columns: ITableHeader[] = [];
  viewType = ComponentType;
  userList: any = [];
  roles: any = [];
  units: any = [];
  userForm: UserForm;
  isEditUser: boolean = false;
  selectedRecordForEdit: any;
  globalFilterArray: any = [];
  public screenStructure: any = [];
  public isEditBtn: boolean = false;
  public isPDFBtn: boolean = false;
  public isExcelBtn: boolean = false;
  public isAddBtn: boolean = false;
  public isResetBtn: boolean = false;
  public isUpdateBtn: boolean = false;
  public isCancelBtn: boolean = false;
  public menuConstants = menuConstants;
  public formRef: NgForm;
  public frozenCols: any = [];
  public frznWidth = '400px'
  public tableRef: any;

  constructor(public userMasterForm: UserMasterForms,
    public sharedService: SharedService,
    public jswService: JswCoreService, public commonService: CommonApiService,
    public jswComponent: JswFormComponent,
    public userService: UserService, public authService: AuthService) {
    this.userForm = new UserForm();
  }

  ngOnInit(): void {
    let userRights = this.authService.getUserRights().filter(ele => ele.screenId == menuConstants.moduleIds.User_Management)[0];
    this.screenStructure = userRights.subScreens.filter(screen => screen.screenId == menuConstants.userMgmtScreenIds.User_Master.id)[0];
    this.isAddBtn = this.sharedService.isButtonVisible(true, menuConstants.userMasterSectionIds.Add_New_Record.buttons.add, menuConstants.userMasterSectionIds.Add_New_Record.id, this.screenStructure);
    this.isResetBtn = this.sharedService.isButtonVisible(true, menuConstants.userMasterSectionIds.Add_New_Record.buttons.reset, menuConstants.userMasterSectionIds.Add_New_Record.id, this.screenStructure);
    this.isUpdateBtn = this.sharedService.isButtonVisible(true, menuConstants.userMasterSectionIds.Edit_Record.buttons.update, menuConstants.userMasterSectionIds.Edit_Record.id, this.screenStructure);
    this.isCancelBtn = this.sharedService.isButtonVisible(true, menuConstants.userMasterSectionIds.Edit_Record.buttons.cancel, menuConstants.userMasterSectionIds.Edit_Record.id, this.screenStructure);;
    this.isEditBtn = this.sharedService.isButtonVisible(true, menuConstants.userMasterSectionIds.List.buttons.edit, menuConstants.userMasterSectionIds.List.id, this.screenStructure);
    this.isPDFBtn = this.sharedService.isButtonVisible(true, menuConstants.userMasterSectionIds.List.buttons.pdf, menuConstants.userMasterSectionIds.List.id, this.screenStructure);
    this.isExcelBtn = this.sharedService.isButtonVisible(true, menuConstants.userMasterSectionIds.List.buttons.excel, menuConstants.userMasterSectionIds.List.id, this.screenStructure);
    this.formObject = JSON.parse(JSON.stringify(this.userMasterForm.addUserForm));
    this.tableType = TableType.gridlines + ' ' + TableType.normal;
    this.getLovs(LovCodes.userType, 'userType');
    this.getLovs(LovCodes.status, 'status');
    this.getLovs(LovCodes.designation, 'designation');
    this.getLovs(LovCodes.userAccountLocked, 'accountLockFlagYN');
    this.getRoles();
    this.getUnits();
    this.getUserList();
    // this.getEmpId();
    this.frozenCols = [
      { field: 'userId', header: 'User ID', columnType: ColumnType.uniqueKey, width: '110px', sortFieldName: 'userId' },
      { field: 'userName', header: 'Name', columnType: ColumnType.string, width: '150px', sortFieldName: 'soId' },
      { field: 'userType', header: 'User Type', columnType: ColumnType.string, width: '120px', sortFieldName: '' },
    ]
    this.columns = [
      // { field: 'userId', header: 'User ID', columnType: ColumnType.uniqueKey, width: '110px', sortFieldName: 'userId'},
      // { field: 'userName', header: 'Name', columnType: ColumnType.string, width: '150px', sortFieldName: 'soId' },
      // { field: 'userType', header: 'User Type', columnType: ColumnType.string, width: '120px', sortFieldName: ''},
      { field: 'designation', header: 'Designation', columnType: ColumnType.string, width: '120px', sortFieldName: '' },
      { field: 'roleData', header: 'Role(s)', columnType: ColumnType.tooltip, width: '150px', sortFieldName: '' },
      { field: 'unitData', header: 'Unit', columnType: ColumnType.tooltip, width: '150px', sortFieldName: '' },
      { field: 'userMobileNo', header: 'Phone(Pri)', columnType: ColumnType.number, width: '120px', sortFieldName: '' },
      { field: 'userExtNo', header: 'User Ext. No.', columnType: ColumnType.number, width: '150px', sortFieldName: '' },
      { field: 'aadhaarNo', header: 'Aadhar No.', columnType: ColumnType.number, width: '150px', sortFieldName: '' },
      { field: 'lastPwChangedOn', header: 'Last Pwd Changed', columnType: ColumnType.date, width: '150px', sortFieldName: '' },
      { field: 'officialEmailId', header: 'Email ID', columnType: ColumnType.string, width: '250px', sortFieldName: '' },
      { field: 'accountLockFlagYN', header: 'Ac Locked', columnType: ColumnType.string, width: '100px', sortFieldName: '' },
      { field: 'status', header: 'Status', columnType: ColumnType.string, width: '90px', sortFieldName: '' },
      { field: 'action', header: 'Action', columnType: ColumnType.action, width: '80px', sortFieldName: '' }
    ]
    // this.globalFilterArray = this.columns.map(element => {
    //   return element.field;
    // });

    // to search the particular feilds in the table
    this.globalFilterArray = ['userId', 'userName', 'officialEmailId','userMobileNo'];

  }

  // API call to get dropdown values
  public getLovs(lovCode: any, refKey: any) {
    this.commonService.getLovs(lovCode).subscribe(
      data => {
        this.formObject.formFields.filter((obj: any) => obj.ref_key == refKey)[0].list = data.map((x: any) => {
          return { displayName: x.valueDescription, modelValue: x.valueShortCode }
        })
      }
    )
  }

  //  //API call to get EmpId
  //  public getEmpId(){
  //   this.formObject.formFields.filter((obj:any) => obj.ref_key == 'empId')[0].value = this.sharedService.getLoggedInUserDetails().userId;
  //  }

  //API call to get Roles
  public getRoles() {
    this.userService.getRoles().subscribe(data => {
      this.roles = data;
      this.formObject.formFields.filter((obj: any) => obj.ref_key == 'role')[0].list = data.map((x: any) => {
        return { displayName: x.roleDescription, modelValue: x.roleId }
      })
    })
  }

  //API call to get Units
  public getUnits() {
    this.commonService.getWorkCenterList().subscribe(data => {
      this.units = data;
      this.formObject.formFields.filter((obj: any) => obj.ref_key == 'unit')[0].list = data.map((x: any) => {
        return {
          displayName: x.wcDesc,
          modelValue: x.wcShortCode
        }
      })
    })
  }

  // API call to get user list
  public getUserList() {
    this.userService.getUserList().subscribe(data => {
      data.map(((ele: any) => {
        let roleArr = ele.roleBoList.map((role: any) => {
          return role.roleDescription;
        })
        ele.roleData = roleArr.join(",");

        let unitArr = ele.workCenterBoList.map((unit: any) => {
          return unit.wcDesc;
        })
        ele.unitData = unitArr.join(",");
        return ele;
      }));
      this.userList = data;
    })
  }

  public resetFilter() {
    //this.formRef.resetForm();
    //this.jswComponent.resetForm(this.formObject, this.formRef);
    this.sharedService.resetForm(this.formObject, this.formRef)
    this.isEditUser = false;
    this.formObject.formFields.filter((ele: any) => ele.ref_key == 'userId')[0].isVisible = false;
    this.formObject.formFields.filter((ele: any) => ele.ref_key == 'empId')[0].disabled = false;
  }

  public getFilterCriteriaDetails(operationType: string) {
    this.jswComponent.onSubmit(this.formObject.formName, this.formObject.formFields);
    var formData = this.jswService.getFormData();

    if (formData && formData.length) {
      let formObj = this.sharedService.mapFormObjectToReqBody(formData, this.userForm.getAddUserForm());
      let roles: any = []; let units: any = [];
      formObj.role.forEach((roleId: any) => {
        roles.push(this.roles.filter((role: any) => role.roleId == roleId)[0]);
      });
      formObj.unit.forEach((unitId: any) => {
        units.push(this.units.filter((unit: any) => unit.wcShortCode == unitId)[0]);
      });

      let reqBody: User = {
        aadhaarNo: formObj.aadhaarNo,
        accountLockFlagYN: formObj.accountLockFlagYN,
        designation: formObj.designation,
        // empId: operationType == 'update' ? this.selectedRecordForEdit.empId : '' ,
        // empId: this.formObject.formFields.filter((obj:any) => obj.ref_key == 'empId')[0].value,
        encryptedPassword: operationType == 'update' ? this.selectedRecordForEdit.encryptedPassword : '',
        lastLoginDt: operationType == 'update' ? this.selectedRecordForEdit.lastLoginDt : '',  // confirm with Rashmi and Rajashree
        lastPwChangedOn: operationType == 'update' ? this.selectedRecordForEdit.lastPwChangedOn : '',
        loggedInUserId: this.sharedService.loggedInUserDetails.userId, // update later with actual value
        officialEmailId: formObj.officialEmailId,
        personalEmailId: operationType == 'update' ? this.selectedRecordForEdit.personalEmailId : '',
        roleBoList: roles,
        status: formObj.status,
        userExtNo: formObj.userExtNo,
        // userId: operationType == 'add' ? this.sharedService.loggedInUserDetails.userId : formObj.userId,
        userId: operationType == 'add' ? '' : formObj.userId,
        empId: formData.filter((obj: any) => obj.ref_key == 'empId')[0].value,
        userMobileNo: formObj.userMobileNo,
        userName: formObj.userName,
        userType: formObj.userType,
        workCenterBoList: units
      };

      if (operationType == 'add') {
        this.createUser(reqBody);
      } else if (operationType == 'update') {
        this.updateUser(reqBody);
      }
    }
  }

  public createUser(reqBody: any) {
    this.userService.createUser(reqBody).subscribe(data => {

      if (data.status === "Inactive") {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: data.statusMsg });
      }
      else {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: "User created successfully!" });
      }
      this.resetFilter();
      this.getUserList();
    })
  }

  public updateUser(reqBody: any) {
    this.userService.updateUser(reqBody).subscribe(data => {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: "User" + (data ? "-" + data.userName : '') + " updated successfully!" });
      this.resetFilter();
      this.getUserList();
    })
  }

  public editRecord(data: any) {
    this.resetFilter();
    if (!this.sharedService.isSectionVisible(menuConstants.userMasterSectionIds.Edit_Record.id, this.screenStructure)) {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'You are not authorized to access Edit User section. Please contact Admin' });
    }
    this.isEditUser = true;
    this.selectedRecordForEdit = data;
    this.formObject.formFields.filter((ele: any) => ele.ref_key == 'userId')[0].isVisible = true;
    this.formObject.formFields.filter((ele: any) => ele.ref_key == 'empId')[0].disabled = true;

    let unit = this.formObject.formFields.filter((ele: any) => ele.ref_key == 'unit')[0];
    unit.selectedItems = data.workCenterBoList.map((ele: any) => { return ele.wcShortCode; })
    let role = this.formObject.formFields.filter((ele: any) => ele.ref_key == 'role')[0];
    role.selectedItems = data.roleBoList.map((ele: any) => { return ele.roleId; })
    this.sharedService.mapResponseToFormObj(this.formObject.formFields, data)
  }

  public deleteUser(record: any) {
    this.userService.deleteUser(record.userId).subscribe(data => {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: "User" + (record ? " - " + record.userId : '') + " deleted successfully!" });
      this.getUserList();
    })
  }

  public deleteMultipleRecords(data: any) {
    let userIds = data.map((ele: any) => { return ele.userId });
    this.userService.deleteMultipleUsers(userIds).subscribe(data => {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: "Selected records deleted successfully!" });
      this.getUserList();
    })
  }

  public downloadFile(fileType: string) {
    if (fileType == 'csv') {
      //let tableHeader = [this.columns.map(ele => ele.header)];
      let tableHeader = [this.columns.map((ele: any) => { if (ele.field != 'action') return ele.header; })];
      let keyArr = this.columns.map((ele: any) => { if (ele.field != 'action') return ele.field });
      let userData = this.sharedService.getDataForExcelAsPerColKeys(this.userList, keyArr);
      this.sharedService.downloadCSV(userData, 'Users', tableHeader);
    }
    if (fileType == 'pdf') {

    }
  }

  public getFormRef(event) {
    this.formRef = event;
  }
  public getTableRef(event) {
    this.tableRef = event;
  }
  public filterTable(event) {
    this.tableRef.filterGlobal(event.target.value, 'contains');
  }

}
