import { Component, OnInit, ViewChild } from '@angular/core';
import { JswCoreService} from 'jsw-core';
import { JswFormComponent } from 'jsw-core';
import { ComponentType } from 'projects/jsw-core/src/lib/enum/common-enum';
import { SharedService } from 'src/app/shared/services/shared.service';
import { ITableHeader } from 'src/assets/interfaces/table-headers';
import { RoleMappingForms, ScreenMasterForms, UserMasterForms } from '../../form-objects/user-mgmt-form-objects';
import { TableType, ColumnType, LovCodes } from 'src/assets/enums/common-enum';
import { UserService } from '../../services/user.service';
import { User, UserForm } from '../../interfaces/user-master-interface';
import { CommonApiService } from 'src/app/shared/services/common-api.service';
import { UserMasterComponent } from './user-master.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { API_Constants } from 'src/app/shared/constants/api-constants';
import { CommonAPIEndPoints } from 'src/app/shared/constants/common-api-end-points';
import { ConfirmationService } from 'primeng/api';
import { UserEndPoints } from '../../form-objects/user-api-endpoints';

describe('UserMasterComponent', () => {
  let component: UserMasterComponent;
  let fixture: ComponentFixture<UserMasterComponent>;
  let userMasterForm: UserMasterForms;
  let sharedService: SharedService;
  let jswService:JswCoreService;
  let commonService: CommonApiService;
  let jswComponent:JswFormComponent;
  let userService: UserService
  let formObjects : UserMasterForms
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserMasterComponent ],
      imports :[
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        SharedModule
      ],
      providers:[
        JswCoreService,
        ConfirmationService,
        CommonApiService,
        API_Constants,
        CommonAPIEndPoints,
        JswFormComponent,
        UserMasterForms,
        ScreenMasterForms,
        RoleMappingForms,
        UserEndPoints,
        UserService ,

        { provide: ToastrService, useValue: toastrService },
    ]

    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMasterComponent);
    component = fixture.componentInstance;
    userService = fixture.debugElement.injector.get(UserService);
    sharedService = fixture.debugElement.injector.get(SharedService);
    formObjects = fixture.debugElement.injector.get(UserMasterForms);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
