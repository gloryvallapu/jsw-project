export interface Role {
  roleDescription: string;
  roleId: number,
  roleSelected: boolean,
  status: string;
}

export interface Unit {
  isActive: string;
  sapWorkCenter: string;
  status: string;
  wcDesc: string;
  wcName: string;
  wcShortCode: string;
  wcSubType: string;
  wcType: string;
}

export interface User {
  aadhaarNo: string;
  accountLockFlagYN: string;
  designation: string;
  empId?: string;
  encryptedPassword?: string;
  lastLoginDt?: string;
  lastPwChangedOn?: string,
  loggedInUserId: string;
  officialEmailId: string;
  personalEmailId?: string;
  registrationToken?: string;
  roleBoList: Role[];
  status: string;
  userExtNo: string;
  userId: string;
  userMobileNo: string;
  userName: string;
  userType: string;
  workCenterBoList: Unit[];
}

export class SMSQA {
  inputBatchId: string;
  counter: number;
  inputMaterial: string;
}

export class smsQaBo{
  batchQuaClearWToFIbRePushFilterCriteriaDataBo:SMSQA;
}

export class HeatChemistry {
  heat: string;
  counter: number
}

export class heatChemistryBo{
  heatChemistryIbRePushFilterCriteriaDataBo:HeatChemistry;
}

export class LocationChange {
  batchId: string;
  counter: number
}

export class locationChangeBo{
  storageLocationIbRePushFilterCriteriaDataBo:LocationChange;
}

// export class LPQA {
//   batchId: string;
//   counter: number
// }

// export class lpQABo{
//   storageLocationIbRePushFilterCriteriaDataBo:LPQA;
// }

export class UserForm {
  public formObj = {
    aadhaarNo: '',
    accountLockFlagYN: '',
    designation: '',
    officialEmailId: '',
    status: '',
    userExtNo: '',
    userMobileNo: '',
    userId: '',
    userName: '',
    userType: '',
    role: [],
    unit: []
  }

  public getAddUserForm(){
    return this.formObj;
  }
}



