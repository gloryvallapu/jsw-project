export interface Screen {
  parentId:string;
  screenId: string;
  screenName: string;
  screenType: string;
  screenWorkCenterBoList: ScreenWorkCenterBO[];
  status: string;
  userId: string;
  moduleName: string;
  screenObjective: string;
  createdDate: string;
  createdBy: string;
}
export interface ScreenWorkCenterBO {
  screenId: string,
  workCenterBoList: WorkCenterBoList[]
}
export interface WorkCenterBoList{
  wcName: string,
  wcShortCode: string;
}
export class ScreenForm {
  public formObj = {
    screenName: '',
    screenType: '',
    unit: [],
    status: '',
  };

  public getEditScreenForm() {
    return this.formObj;
  }
}
