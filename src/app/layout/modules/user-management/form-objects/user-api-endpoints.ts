export class UserEndPoints {
  // User Master Screen API End Points
  public readonly getLovValues = '/lovValues';
  public readonly getUserList = '/users';
  public readonly getRoles = '/roles';
  public readonly getUnits = '/workCenters';
  public readonly createUser = '/users';
  public readonly updateUser = '/users';
  public readonly deleteUser = '/users';
  public readonly deleteMultipleUsers = '/users/deleteMul';
  // End of User Master Screen API End Points

  // Screen Master API End Points
  public readonly getAllScreen =  '/screen/getAllScreen';
  public readonly getDistinctParentScreen = '/screen/getDistinctParentScreen';
  public readonly getSubScreensByParentId = '/screen/getSubScreensByParentId';
  public readonly getFilteredScreenList = '/screen/getFilteredScreenList';
  public readonly updateScreen = '/screen/updateScreen';
  public readonly deleteScreen = '/screen/deleteScreen';
  public readonly deleteMulScreens = '/screen/deleteMulScreens';
  // End of Screen Master API End Points

  // Role Master API End Point
  public readonly createNewRole = '/roleScreen/mapping/';
  public readonly deleteRole = '/roleScreen/delete/';
  public readonly getAllRolesScreen = '/roles/getAllRolesScreen';
  public readonly editRoleDetails = '/roleScreen/getRoleScreenButton/';
  public readonly updateRoleMapping = '/roleScreen/mapping/';
  // End of Role Screen Master API Point




}
