import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserManagementRoutingModule } from './user-management-routing.module';
import { UserManagementComponent } from './user-management.component';
import { UserMasterComponent } from './components/user-master/user-master.component';
import { ScreenMasterComponent } from './components/screen-master/screen-master.component';
import { RoleScreenMappingComponent } from './components/role-screen-mapping/role-screen-mapping.component';
import { ScreenMasterForms, UserMasterForms,RoleMappingForms } from './form-objects/user-mgmt-form-objects';
import { JswCoreModule } from 'projects/jsw-core/src/public-api';
import { UserEndPoints } from './form-objects/user-api-endpoints';
import { UserService } from './services/user.service';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    UserManagementComponent,
    UserMasterComponent,
    ScreenMasterComponent,
    RoleScreenMappingComponent
  ],
  imports: [
    CommonModule,
    //JswCoreModule,
    UserManagementRoutingModule,
    SharedModule
  ],
  providers: [UserMasterForms,ScreenMasterForms,RoleMappingForms, UserEndPoints, UserService]
})
export class UserManagementModule { }
