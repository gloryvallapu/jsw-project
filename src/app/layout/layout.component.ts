import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Router } from '@angular/router';
import { MENU } from 'src/assets/constants/MENU/menu-list';
import { AuthService } from '../core/services/auth.service';
import { UserDetails } from '../shared/interfaces/common-interfaces';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
  // add to hide menu on outside click
  host: {
    "(window:click)": "onClick()"
  }
})
export class LayoutComponent implements OnInit {
  expandMenu: boolean = false;
  userDetails: UserDetails;
  userRoles: any = [];
  currentUserRole: any = '';
  menuList: any = [];
  selectedSM: any;
  menuItemSelected = false;
  sideMenuState: boolean = false;
  isInterface:boolean=false;
  public menuData = new MENU();
  FROMSAPList: any = [];
  TOSAPList: any = [];
  interfaceheader:any=[];
  menuSelectedName: any;
  constructor(public sharedService: SharedService, public route: Router, public authService: AuthService) { }

  ngOnInit(): void {
    let menuItems;
    this.sharedService.getMenuList.subscribe(menuL => {
      menuItems = menuL;
    })
    // this.menuList = JSON.parse(JSON.stringify(this.menuData.getMenuList()));
    this.userRoles = this.authService.roleList;
    this.sideMenuState = false;
    this.userDetails = this.sharedService.getLoggedInUserDetails();
    this.currentUserRole = this.userDetails.currentRole;
    //let menuItems = this._constants._getConstantsData('menu').menuList;
    //let menuItems = this.sharedService.getMenuList();
    let fromsaplist; let tosaplist; let interfaceheader;
    this.menuList = menuItems.filter(function(menu){
      if(menu.permission == true){
        if(menu.name=='Interface')
        {
          fromsaplist= menu.subMenuItems ? menu.subMenuItems.filter(screenEle => screenEle.permission == true && screenEle.type=='FROMSAP') : [];
          tosaplist= menu.subMenuItems ? menu.subMenuItems.filter(screenEle => screenEle.permission == true && screenEle.type=='TOSAP') : [];
          interfaceheader=[menu.subMenuItems[0],menu.subMenuItems[1]]
        }
        else
        menu.subMenuItems = menu.subMenuItems ? menu.subMenuItems.filter(screenEle => screenEle.permission == true) : [];
        return menu;
      }
    })
    this.FROMSAPList=fromsaplist;
    this.TOSAPList=tosaplist;
    this.interfaceheader=interfaceheader;
  }

  ngAfterContentInit() {
    if (this.route.url) {
      //this.seletedMenuItem = this.menuList.filter((ml: any) => ml.path.includes(this.route.url))[0];
      var routeURL = this.route.url.split('/');
      this.menuList.forEach((menu: any) => {
        menu.active = false;
        if (menu.path.includes(routeURL[2])) {
          menu.active = true;
          menu.subMenuItems.forEach((submenu: any) => {
            submenu.active = false;
            if (submenu.path.includes(this.route.url)) {
              submenu.active = true;
              this.selectedSM = submenu;
            }
          })
        }
      });
    }
  }

  switchUser(role: any){
    //this.authService.switchedRole = role;
    this.route.navigate(['/user-auth']);
  }

  toggleSideBar() {
    this.expandMenu = !this.expandMenu;
    //this.sharedService.menuBarToggle(this.expandMenu);
    if(this.expandMenu){
      if((this.menuList.filter((menu: any) => menu.active)).length > 0){
        var element: any = document.getElementById("submenu");
        setTimeout(() => {
          if(element) { element.classList.add("show"); }
        }, 250);

      }
    }else{
     // if((this.menuList.filter(menu => menu.active)).length > 0){
        var element: any = document.getElementById("submenu");
        if(element) { element.classList.remove("show"); }
     // }
    }
  }

  onMenuSelect(menu: any, m_Index: number){
    this.menuList.forEach((menu: any) => {
      menu.active = false;
      this.selectedSM = null;
      if(menu.subMenuItems && menu.subMenuItems.length > 0){
      menu.subMenuItems.forEach((submenu: any) => {
          submenu.active = false;
        })
      }
    });
    this.menuList[m_Index].active = !this.menuList[m_Index].active;
    this.menuSelectedName= this.menuList[m_Index].name;
    if(this.menuList[m_Index].subMenuItems.length){
      this.menuList[m_Index].subMenuItems.forEach((submenu: any) => {
        if(submenu.path == this.route.url) {
          submenu.active = true;
          this.selectedSM = submenu;
        }
      })
    }else{
      this.route.navigate([this.menuList[m_Index].path]);
    }
    //this.sharedService.toggleSideBar(false);
  }

  onSubMenuSelect(menu: any, subMenu: any, sm_Index: number){
    menu.subMenuItems.forEach((sm: any) => {
      sm.active = false;
    });
    menu.subMenuItems[sm_Index].active = !menu.subMenuItems[sm_Index].active;
    this.selectedSM = subMenu;
    //this.route.navigateByUrl(smitem.path);
    this.route.navigate([subMenu.path]);


    if(!this.expandMenu){
      var element = document.getElementById("submenu");
      element?.classList.remove("show");
    }

  }

  logoutUser(){
   // this.route.navigate(['/user-auth']);
    this.authService.logout().subscribe(
      data => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'You are logged out successfully'});
        this.route.navigate(['/user-auth']);
        this.authService.resetToken();
        this.authService.roleList.length = 0;
      }
    )
  }

  // add to hide menu on outside click
  onClick() {
    if(!this.expandMenu){
      var element = document.getElementById("submenu");
      element?.classList.remove("show");
    }
  }

}
