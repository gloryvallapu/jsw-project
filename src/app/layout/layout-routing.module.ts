import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'user-mgmt',
        loadChildren: ()=> import('./modules/user-management/user-management.module').then(m => m.UserManagementModule)
      },
      {
        path: 'master',
        loadChildren: ()=> import('./modules/master/master.module').then(m => m.MasterModule)
      },
      {
        path: 'ppc',
        loadChildren: ()=> import('./modules/ppc/ppc.module').then(m => m.PpcModule)
      },
      {
        path: 'brm',
        loadChildren: ()=> import ('./modules/brm/brm.module').then(m => m.BrmModule)
      },
      {
        path: 'sms',
        loadChildren: ()=> import ('./modules/sms/sms.module').then(m => m.SmsModule)
      },
      {
        path: 'brm-qa',
        loadChildren: ()=> import ('./modules/brm-qa/brm-qa.module').then(m => m.BrmQaModule)
      },
      {
        path: 'interface',
        loadChildren: ()=> import ('./modules/interface/interface.module').then(m => m.InterfaceModule)
      },
      {
        path: 'reports',
        loadChildren: ()=> import ('./modules/reports/reports.module').then(m => m.ReportsModule)
      },
      {
        path: 'dashboard',
        loadChildren: ()=> import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
    ]
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class LayoutRoutingModule { }
