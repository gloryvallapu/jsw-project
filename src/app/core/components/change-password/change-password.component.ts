import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Router } from '@angular/router';
import { CustomValidators, MustMatch } from 'src/app/shared/directives/must-match.validatior';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  public changePasswordForm: FormGroup;
  public token: any;
  public userData: any;
  public submitted = false;

  constructor(public authService: AuthService, public sharedService: SharedService, private router: Router, public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: [null, Validators.required],
      newPassword: [null,
        Validators.compose([
        Validators.required,
        // check whether the entered password has a number
        CustomValidators.patternValidator(/\d/, {
          hasNumber: true
        }),
        // check whether the entered password has upper case letter
        CustomValidators.patternValidator(/[A-Z]/, {
          hasCapitalCase: true
        }),
        // check whether the entered password has a lower case letter
        CustomValidators.patternValidator(/[a-z]/, {
          hasSmallCase: true
        }),
        // check whether the entered password has a special character
        CustomValidators.patternValidator(
          /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
          {
            hasSpecialCharacters: true
          }
        ),
        Validators.minLength(8)
      ])],
      confirmPassword: [null, Validators.required]
    }, {
      validator: MustMatch('newPassword', 'confirmPassword')
    });
  }

  get formFields() { return this.changePasswordForm.controls; }

  public changePassword(){
    this.submitted = true;
    if (this.changePasswordForm.invalid) {
      return;
    }
    var data = {
      userId: this.sharedService.loggedInUserDetails.userId,
      oldPassword: this.changePasswordForm.controls['oldPassword'].value,
      newPassword: this.changePasswordForm.controls['confirmPassword'].value
    }
    this.authService.changePassword(data).subscribe(data => {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Password is updated successfully. Please login using new password.'});
      this.authService.roleList.length = 0;
      this.router.navigate(['/user-auth']);
    }, error => {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, error.error);
    })
  }
}
