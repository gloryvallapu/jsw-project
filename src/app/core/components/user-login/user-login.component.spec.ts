import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MENU } from 'src/assets/constants/MENU/menu-list';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient, HttpHeaders,HttpHandler } from '@angular/common/http';
import { UserLoginComponent } from './user-login.component';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


describe('UserLoginComponent', () => {
  let component: UserLoginComponent;
  let fixture: ComponentFixture<UserLoginComponent>;
  let sharedService :SharedService;
  let authService :AuthService;
  const toastrService = {
    success: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { },
    error: (message?: string, title?: string, override?: Partial<IndividualConfig>) => { }
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports:[RouterTestingModule],
      declarations: [ UserLoginComponent ],
      providers:[SharedService,FormBuilder,FormControl,
      AuthService,FormBuilder,HttpClient,HttpHandler,
      { provide: ToastrService, useValue: toastrService },]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
