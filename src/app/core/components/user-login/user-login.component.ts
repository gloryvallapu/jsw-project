import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MENU } from 'src/assets/constants/MENU/menu-list';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { UserDetails } from 'src/app/shared/interfaces/common-interfaces';


@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  public userForm = new FormGroup({});
  public menuItems: any;
  public isRoleRequired: boolean = false;
  public roleData: any = [];
  public showForgotPassword: boolean = false;
  public roleList: any = [];
  public menuData = new MENU();
  public navigationModule: any = [];
  constructor(private formB: FormBuilder, private router: Router, public authService: AuthService, public sharedService: SharedService) { }

  ngOnInit() {
    this.menuItems = JSON.parse(JSON.stringify(this.menuData.getMenuList()));
    this.userForm = this.formB.group({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      userRole: ['']
    });
    this.userForm.setValue({
      username: 'AA00007',
      password: 'Ganesh@555',
      userRole: ''
    });
  }

  get userRole() {
    return this.userForm.get('userRole');
  }

  loginUser() {
    var userDetails = {
      "userId": this.userForm.controls['username'].value,
      "password": this.userForm.controls['password'].value
    }
    console.log(window.location.href);
    // var currentUrl = window.location.href;
    // if(currentUrl =='https://jswispllpmes-uat.jsw.in/monnet-ui/#/user-auth/login'
    // || currentUrl=='https://jswispllpmes-uat.jsw.in/monnet-ui/#/user-auth/login'
    // || currentUrl=='http://localhost:4200/#/user-auth/login'){
      this.authService.login(userDetails).subscribe(data => {
        if(data && data.message){
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: data.message});
          return;
        }else{
          this.authService.setToken(data.jwttoken);
        // this.getLocalUserRights();
          this.getLoggedInUserRights();
        }
      }, error => {
        if(error.status == 301){
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'User has resigned, cannot login into MES'});
        }else{
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, error && error.error ? error.error : error);
        }
      })
    // }
    // else{
    //   this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: 'Please Enter Proper URL'});
    // }
    // this.authService.login(userDetails).subscribe(data => {
    //   if(data && data.message){
    //     this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, { message: data.message});
    //     return;
    //   }else{
    //     this.authService.setToken(data.jwttoken);
    //   // this.getLocalUserRights();
    //     this.getLoggedInUserRights();
    //   }
    // }, error => {
    //   if(error.status == 301){
    //     this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: 'User has resigned, cannot login into MES'});
    //   }else{
    //     this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, error && error.error ? error.error : error);
    //   }
    // })
  }

  // getUserRights(){
  //   this.roleList = [{
  //     role: 'Super Admin'
  //   }]
  //   this.userRole?.setValue(this.roleList[0].role, {
  //     onlySelf: true
  //   })
  //   this.isRoleRequired = true;
  // }

  getLocalUserRights(){
    this.authService.getLocalUserRights().subscribe(
      data => {
        this.roleData = data;
        if(data && data.roleList.length){
          this.authService.roleList = data.roleList;
          if(!this.authService.switchedRole){
            this.roleList = data.roleList;
          }else{
            this.roleData.roleList = data.roleList.filter(role => role.role == this.authService.switchedRole);
            this.roleList = this.roleData.roleList;
            this.userRole.setValue(this.roleList[0].role, {
              onlySelf: true
            })
          }
          if(this.authService.roleList.length == 1){
            this.userRole.setValue(data.roleList[0].role, {
              onlySelf: true
            })
          }
          this.isRoleRequired = true;
        }else{
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access the application. Please contact administrator." });
          this.authService.roleList.length = 0;
          this.roleList.length = 0;
        }
      }, error => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, error && error.error ? error.error : error);
      }
    )
  }

  getLoggedInUserRights(){
    this.authService.getLoggedInUserRights().subscribe(
      data => {
        this.roleData = data;
        if(data && data.roleList.length){
          this.authService.roleList = data.roleList;
          if(!this.authService.switchedRole){
            this.roleList = data.roleList;
          }else{
            this.roleData.roleList = data.roleList.filter(role => role.role == this.authService.switchedRole);
            this.roleList = this.roleData.roleList;
            this.userRole.setValue(this.roleList[0].role, {
              onlySelf: true
            })
          }
          if(this.authService.roleList.length == 1){
            this.userRole.setValue(data.roleList[0].role, {
              onlySelf: true
            })
          }
          this.isRoleRequired = true;
        }else{
          this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access the application. Please contact administrator." });
          this.authService.roleList.length = 0;
          this.roleList.length = 0;
        }
      }, error => {
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, error && error.error ? error.error : error);
      }
    )
  }

  updateMenuList(){
    this.sharedService.setMenuList(this.menuItems);
  }

  changeRole(event:any){
    this.userRole?.setValue(event.target.value, {
      onlySelf: true
    })
  }

  navigateToModule(){
    this.updateMenuList();
    this.router.navigate(['/pages/user-mgmt/userMaster']);
  }

  public navigateToModule_(){
    // console.log("val", this.userForm.controls['userRole'].value);
    // console.log("val11", this.userRole.value);
    this.authService.switchedRole = null;
    if(this.roleData && this.roleData.roleList.length){
      let userRole = this.roleData.roleList.filter(roleObj => roleObj.role == this.userRole.value)[0].role;  //data.roleList[0].role;
      let roleData = this.roleData.roleList.filter(roleObj => roleObj.role == this.userRole.value)[0].screenList; //EAF MANAGER // POST PROD MANAGER
      // this.authService.setLoggedInUserDetails({
      //   userName: this.roleData.userName,
      //   firstName: this.roleData.firstName,
      //   role: userRole
      // })

      let userData: UserDetails = {
        username: this.roleData.userName,
        userId: this.roleData.userName,
        firstName: this.roleData.firstName,
        emailId: this.roleData.emailId,
        currentRole: userRole,
        assignedModules: roleData
      }
      this.sharedService.setLoggedInUserDetails(userData);

      let userRights = Object.keys(roleData).map(key => (
        {
          screenId: roleData[key].screenId,
          screenName: roleData[key].screenName,
          buttons: roleData[key].buttons,
          subScreens: roleData[key].subScreens
        })
      );

      userRights.forEach(moduleEle => {
      let mIndex = this.menuItems.findIndex(ele => ele.id == moduleEle.screenId);
      if(mIndex != -1){
          this.menuItems[mIndex].permission = true;
          moduleEle.subScreens.forEach(screenEle => {
            let smIndex = this.menuItems[mIndex].subMenuItems.findIndex(ele => ele.id == screenEle.screenId);
            if(smIndex != -1){
              this.menuItems[mIndex].subMenuItems[smIndex].permission = true;
            }
          });
        }
      });
      this.sharedService.setMenuList(this.menuItems);
      this.authService.setUserRights(userRights);
      this.navigationModule = this.menuItems.filter(ele => ele.permission == true);
      console.log(this.menuItems)
      if(this.navigationModule.length){
        // this.sharedService.localStorageObj.username = this.roleData.userName;
        this.sharedService.loggedInUserDetails.username = this.roleData.userName;
        // localStorage.setItem('userDetails', JSON.stringify(this.sharedService.localStorageObj));

        //Below code is not required as everytime user will land to dashboard after login
        //let subMenuItems = this.navigationModule[0].subMenuItems.filter(sm => sm.permission == true);
        //this.router.navigate([subMenuItems[0].path]);
        this.router.navigate(['/pages/dashboard']);
      }else{
        this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access the application. Please contact administrator." });
      }
    }else{
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Warning, { message: "You are not authorized to access the application. Please contact administrator." });
      this.authService.roleList.length = 0;
      this.roleList.length = 0;
    }
  }


}
