import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  public forgotPasswordForm = new FormGroup({});
  constructor(public authService: AuthService, public sharedService: SharedService, private router: Router) { }

  ngOnInit(): void {
    this.forgotPasswordForm = new FormGroup({
      username: new FormControl('', [Validators.required])
    });
  }

  public sendLink(){
    let userId = this.forgotPasswordForm.controls['username'].value;
    this.authService.forgotPassword(userId).subscribe(data => {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: data});
      this.router.navigate(['user-auth/userLogin']);
    }, error => {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Error, error && error.error ? error.error : error);
    })
  }

}
