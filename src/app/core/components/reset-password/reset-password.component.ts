import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from 'src/app/shared/services/shared.service';
import { CustomValidators, MustMatch } from 'src/app/shared/directives/must-match.validatior';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  public resetPasswordForm = new FormGroup({});
  public token: any;
  public userData: any;
  public submitted: boolean = false;
  constructor(private route: ActivatedRoute, public sharedService: SharedService,
              private router: Router, public formBuilder: FormBuilder, public authService: AuthService) { }

  ngOnInit(): void {
   this.token = this.route.snapshot.paramMap.get("registrationToken");
   this.resetPasswordForm = this.formBuilder.group({
    newPassword: [null,
      Validators.compose([
      Validators.required,
      // check whether the entered password has a number
      CustomValidators.patternValidator(/\d/, {
        hasNumber: true
      }),
      // check whether the entered password has upper case letter
      CustomValidators.patternValidator(/[A-Z]/, {
        hasCapitalCase: true
      }),
      // check whether the entered password has a lower case letter
      CustomValidators.patternValidator(/[a-z]/, {
        hasSmallCase: true
      }),
      // check whether the entered password has a special character
      CustomValidators.patternValidator(
        /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
        {
          hasSpecialCharacters: true
        }
      ),
      Validators.minLength(8)
    ])],
    confirmPassword: [null, Validators.required]
  }, {
    validator: MustMatch('newPassword', 'confirmPassword')
  });
  this.getDetailsForPasswordReset();
  }

  get formFields() { return this.resetPasswordForm.controls; }

  public getDetailsForPasswordReset(){
    this.authService.getDetailsForPasswordReset(this.token).subscribe(data => {
      this.userData = data;
    })
  }

  public resetPassword(){
    this.submitted = true;
    if (this.resetPasswordForm.invalid) {
      return;
    }
    var password = this.resetPasswordForm.controls['confirmPassword'].value;
    this.userData.encryptedPassword = password;
    this.authService.resetPassword(this.userData, this.token).subscribe(data => {
      this.sharedService.displayToastrMessage(this.sharedService.toastType.Success, { message: 'Password is updated successfully..!!'});
      this.authService.roleList.length = 0;
      this.router.navigate(['/user-auth']);
    })
  }
}
