import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { CoreComponent } from './core.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';

const routes: Routes = [
  {
    path: '',
    component: CoreComponent,
    children: [
      { path: '', redirectTo: "login" },
      {
          path: "login",
          component: UserLoginComponent
      },
      {
        path: 'resetPassword/:registrationToken',
        component: ResetPasswordComponent
      },
      {
        path: "changePassword",
        component: ChangePasswordComponent
      },
      {
        path: "forgotPassword",
        component: ForgotPasswordComponent
      }
  ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})
export class CoreRoutingModule { }
