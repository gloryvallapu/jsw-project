export class UserAuthEndPoints {
  //Login Screen API End Points
  public readonly authenticate = '/authenticate';
  public readonly getUserRights = '/getUserRights';
  //End of Login Screen End Points

  //Forgot Password Screen API End Points
  //End of Forgot Password Screen End Points

  //Reset Password Screen API End Points
  //End of Reset Password Screen End Points

  //Change Password Screen API End Points
  //End of Change Password Screen End Points
}
