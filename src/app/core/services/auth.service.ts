import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { API_Constants } from 'src/app/shared/constants/api-constants';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public baseUrl: any;
  public token:any;
  public userRights: any;
  public localBaseUrl = 'assets/mock-json';
  public loggedInUserDetails: any;
  public roleList: any = [];
  public switchedRole: any;

  constructor(public restApi:HttpClient, private apiURL: API_Constants ) {
    this.baseUrl = `${this.apiURL.baseUrl}`; //${this.apiConstants.userGateWay}
  }

  public setToken(value: string){
    this.token = value;
  }

  public getToken(){
    return this.token;
  }

  public resetToken(){
    this.token = '';
  }

  public login(userDetails: any){
    return this.restApi.post(`${this.baseUrl}/authenticate`, userDetails).pipe(map((response: any) => response));
  }

  public getUserRights(){
    return this.userRights;
  }

  public setUserRights(userRights:any){
    this.userRights = userRights;
  }

  public setLoggedInUserDetails(userDetails:any){
    this.loggedInUserDetails = userDetails;
  }

  public getLoggedInUserDetails(){
    return this.loggedInUserDetails;
  }

  public getLocalUserRights(){
    return this.restApi.get(`${this.localBaseUrl}/user-rights.json`).pipe(map((response: any) => response));
  }

  public getLoggedInUserRights(){
    return this.restApi.get(`${this.baseUrl}/getUserRights`).pipe(map((response: any) => response));
  }

  public isAuthenticated(moduleId: any, screenId: any): boolean {
    if(this.token){
      if(this.isAuthorizedUser(moduleId, screenId))
        return true;
      else
        return false;
    }else{
      return false;
    }
  }

  public isAuthorizedUser(moduleId: any, screenId: any): boolean{
    let moduleData = this.getUserRights().filter((ele: any) => ele.screenId == moduleId);
    if(moduleData && moduleData.length > 0){
      let screenData;
      if(moduleId==screenId)
      screenData = moduleData[0].subScreens;
      else
      screenData = moduleData[0].subScreens.filter((screen:any) => screen.screenId == screenId);
      if(screenData && screenData.length > 0){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  public getDetailsForPasswordReset(token: any){
    return this.restApi.get(`${this.baseUrl}/jispl/master/users/activateAccount?registrationToken=` + token).pipe(map((response: any) => response));
  }

  public resetPassword(data: any, token: any){
    let httpOptions = new HttpHeaders().set('Authorization', 'Bearer ' + token);
    return this.restApi.put(`${this.baseUrl}/jispl/master/users/setPassword`, data, { headers: httpOptions, responseType: 'text' }).pipe(map((response: any) => response));
  }

  public changePassword(data: any){
    let reqBody = {};
    return this.restApi.put(`${this.baseUrl}/jispl/master/users/changePassword?userId=` + data.userId + '&oldPassword=' + data.oldPassword + '&newPassword=' + data.newPassword, reqBody, { responseType: 'text' }).pipe(map((response: any) => response));
  }

  public forgotPassword(userId: any){
    return this.restApi.get(`${this.baseUrl}/forgotPassword?userId=` + userId, { responseType: 'text'}).pipe(map((response: any) => response));
  }

  public logout(){
    return this.restApi.get(`${this.baseUrl}/appLogout`, { responseType: 'text'}).pipe(map((response: any) => response));
  }
}

