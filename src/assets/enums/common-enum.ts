export enum ColumnType {
   number = 'Number',
   string = 'String',
   date = "Date",
   checkbox = 'Checkbox',
   radio = 'radio',
   text = "Text",
   dropdown = "Dropdown",
   tooltip = "Tooltip",
   action = "Action",
   uniqueKey = "Key",
   hyperlink = "hyperlink",
   textWithValidations = "textWithValidations",
   numericValidationField = 'numericValidationField',
   textField = 'textField',
   calender = 'calender',
   checkboxField = 'checkboxField',
   ellipsis = 'ellipsis'
}

export enum TableType {
  small = 'p-datatable-sm',
  normal = '',
  large = 'p-datatable-lg',
  gridlines = 'p-datatable-gridlines'
}

export enum ToastrType {
  "Success" = 0,
  "Info",
  "Warning",
  "Error"
}

export enum FieldType {
  number,
  numberMask,
  textWithFilter ,
  text,
  email,
  dropdown,
  multiselect,
  calendar,
  textarea,
  checkbox,
  radio
}

export enum LovCodes {
  status = 'status',
  userType = 'User_type',
  designation = 'Designation',
  userAccountLocked = 'User_Account_Locked',
  screen_type = "Object_Type",
  materialType = 'Material_Type',
  propDataType = 'Data_Type',
  testType = 'Test_Type',
  validationType = 'Validation_Type',
  propType= 'Property_type',
  scheduleStatus = 'Schedule_Status',
  rolledShape ='Rolled_Shape',
  loadCellWeight ='load_c_w',
  int_stat='int_stat',
  lp_sms='lp_sms',
  Output_Product='Output_Product',
  mat_c='mat_c',
  displayType='disp_type',
  sap_loc='sap_loc',
  input_Product = 'Input_Product',
  //22-11
  sms_lp='sms_lp'
  
}

export enum CommonAPIObject{
  displayName = 'objectStatus',
  modelValue = 'objectStatusId'
}

export enum WorkCenter {
  displayName = 'wcDesc',
  modelValue = 'wcCode'
}

export enum Product {
  displayName = 'productName',
  modelValue = 'productId'
}

export enum SizeCode {
  displayName = 'sizeDesc',
  modelValue = 'sizeCode'
}

export enum LOV {
  displayName = 'valueDescription',
  modelValue = 'valueShortCode'
}

export enum SMSAction {
  Create_Schedule = 'crt_sch',  // added on UI side
  View_and_Insert_Order = 'vw_in_ord',
  Build_Heat = "build_ht",
  Release_Schedule = "rls_schd",
  Add_Unplanned_Heat = "ad_un_ht",
  Build_Unplanned_Heat = "build_uht",
  Release_Unplanned_Heat = 'rls_uht',
  Clear = 'clear',
  Delete_Order = 'dlt_ord',
  Cancel_Schedule = 'can_schd'
}

export enum SMSStatus {
  Created_Schedule = 'crtd_schd',
  Ready_to_Build = 'rdy_build',
  Ready_to_Release = 'rdy_releas',
  Release_to_SMS = 'releas_sms',
  Ready_to_Build_Unplanned_Heat = 'rd_bld_uht',
  Ready_to_Release_Unplanned_Heat = 'rd_rls_uht',
  Release_Unplanned_Heat = 'rls_un_sms',
  Schedule_Completed = 'schd_cmplt',
  Cancel_Schedule = 'can_schd'
}

export enum LPAction {
  Delete_Order = 'dlt_ord',
  View_and_Insert_Order = 'vw_in_ord',
  Release_Schedule = "rls_schd",
  Add_Edit_or_Delete_Order = "ad_ed_dlt",
  Add_Unplanned_Heat = "add_un",
  Release_Unplanned_Heat = "rls_un",
  Charged = "charged",
  Rolled = "rolled",
  Clear = "clear",
  Cancel_Schedule = "can_schd",
  Create_Schedule = 'crt_sch' // added on UI side
}


export enum SchedulStatus {
  Cancel_Schedule = 'can_schd',
  Created_Schedule = 'crtd_schd',
  Ready_to_Build = 'rdy_build',
  Ready_to_Release = 'rdy_releas',
  Release_to_SMS = 'releas_sms',
  Released_to_Mill = 'rls_mill'

}


export enum ReportType{
  Billet_Consumption_Report = 'billetConsumptionReport',
  Furnace_Residence_Report = 'furnaceResidenceReport',
  SizeWise_Monthly_Report = 'sizeWiseMonthlyReport',
  Lp_Production_Details_Report ='lpProductionDetailsReport',
  HeatWise_Production_Report = 'heatWiseProductionReport',
  LP_Production_SummaryReport = 'lpProductionSummaryReport',
  Hot_Out_Pending_Report = 'hotOutPendingReport',
  Lp_Rolling_Schedule = 'LpRollingSchduleReport',
  LP_Stock_Report='LPStockReport',
  Mech_Test_Report = 'mechTestReport',
  Shift_Wise_Prod_Report = 'shiftWiseProductionReport',
  LPStock_Summary_Report = 'LPStockSummaryReport',
  Billet_Stock_Report='billetStockReport',
  Batch_Detail_Report='batchDetailReport',
  So_Detail_Report='soDetailReport'
}


