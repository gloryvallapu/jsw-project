export interface ITableHeader {
  field: string;
  header: string;
  columnType: string;
  width: string;
  sortFieldName: string;
  isError?: boolean;
  errorMsg?: string;
  rowIndex?: number;
  list?: Array<any>;
  disableCol?: boolean;
  dateType?:string
}
