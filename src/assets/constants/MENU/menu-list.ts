export const moduleIds = {
  User_Management: 10,
  Master: 20,
  PPC: 30,
  BRM: 40,
  SMS_Operation: 50,
  BRM_QA: 60,
  Interface: 70,
  Reports: 80
}

// User Management Module Structure
export const userMgmtScreenIds = {
  User_Master: {
    id: 101,
    buttons: null
  },
  Screen_Master: {
    id: 102,
    buttons: null
  },
  Role_Screen_Mapping: {
    id: 103,
    buttons: null
  }
}

export const userMasterSectionIds = {
  Add_New_Record: {
    id: 1011,
    buttons: {
      add: "ADD",
      reset: "RESET"
    }
  },
  List: {
    id: 1012,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF",
      edit: "EDIT",
    }
  },
  Edit_Record: {
    id: 1013,
    buttons: {
      update: "UPDATE",
      cancel: "CANCEL"
    }
  },
}

export const screenMasterSectionIds = {
  Filter_Criteria: {
    id: 1021,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  List: {
    id: 1022,
    buttons: {
      edit: "EDIT"
    }
  },
  Edit_Record: {
    id: 1023,
    buttons: {
      update: "UPDATE",
      cancel: "CANCEL"
    }
  },
}

export const roleScreenMappingSectionIds = {
  List: {
    id: 1031,
    buttons: {
      createRole: "CREATE ROLE",
      edit: "EDIT"
    }
  },
  Edit_Record: {
    id: 1032,
    buttons: {
      update: "UPDATE",
      close: "CLOSE"
    }
  },
  Add_New_Record: {
    id: 1033,
    buttons: {
      save: "SAVE",
      close: "CLOSE"
    }
  },
}
// End of User Management Module Structure

// Master Module Structure
export const masterMgmtScreenIds = {
  Product_Attribute_Master: {
    id: 201,
    buttons: null
  },
  Property_Master: {
    id: 202,
    buttons: null
  },
  Grade_Master: {
    id: 203,
    buttons: null
  },
  Gradewise_Density_Master: {
    id: 204,
    buttons: null
  },
  LOV_Master: {
    id: 205,
    buttons: null
  },
  Yard_Master: {
    id: 206,
    buttons: null
  },
}

export const prodAttrMasterSectionIds = {
  Product_List: {
    id: 2011,
    buttons: {
      edit: "EDIT",
      add: "ADD PRODUCT",
      save: "SAVE",
      close: "CLOSE"
    }
  },
  Work_Center_List: {
    id: 2012,
    buttons: {
      edit: "EDIT",
      add: "ADD WORK CENTER",
      save: "SAVE",
      close: "CLOSE"
    }
  },
  Size_Master_List: {
    id: 2013,
    buttons: {
      edit: "EDIT",
      add: "ADD SIZE",
      save: "SAVE",
      close: "CLOSE"
    }
  },
  WC_Prod_Size_List: {
    id: 2014,
    buttons: {
      edit: "EDIT",
      add: "ADD PRODUCT",
      save: "SAVE",
      close: "CLOSE"
    }
  }
}

export const propertyMasterSectionIds = {
  Add_New_Record: {
    id: 2021,
    buttons: {
      add: "ADD",
      reset: "RESET"
    }
  },
  List: {
    id: 2022,
    buttons: {
      edit: "EDIT"
    }
  },
  Edit_Record: {
    id: 2023,
    buttons: {
      update: "UPDATE",
      cancel: "CANCEL"
    }
  }
}

export const gradeMasterSectionIds = {
  Add_New_Record: {
    id: 2031,
    buttons: {
      add: "ADD",
      reset: "RESET"
    }
  },
  List: {
    id: 2032,
    buttons: {
      edit: "EDIT"
    }
  },
  Edit_Record: {
    id: 2033,
    buttons: {
      update: "UPDATE",
      cancel: "CANCEL"
    }
  },
  Grade_Chemistry: {
    id: 2034,
    buttons: null
  },
  Grade_Mech_Property: {
    id: 2035,
    buttons: {
      save: "SAVE"
    }
  },
  Metallurgy: {
    id: 2036,
    buttons: {
      save: "SAVE"
    }
  },
  NDT: {
    id: 2037,
    buttons: {
      save: "SAVE"
    }
  }
}

export const gradeWiseDensityMasterSectionIds = {
  Filter_Criteria: {
    id: 2041,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  List: {
    id: 2042,
    buttons: {
      add: "ADD",
      edit: "EDIT",
      save: "SAVE",
      close: "CLOSE"
    }
  }
}

export const LOVMasterSectionIds = {
  Filter_Criteria: {
    id: 2051,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET",
      addlov: "ADD LOV",
      savelov: "SAVE",
    }
  },
  ValueList: {
    id: 2052,
    buttons: {
      addvalue: "ADD VALUE"
    }
  },
  DepdendentList: {
    id: 2053,
    buttons: {
      savemapping: "SAVE MAPPINGS"
    }
  }
}
export const YardMasterSectionIds = {
  Add_New_Record: {
    id: 2061,
    buttons: {
      add: "ADD",
      reset: "RESET"
    }
  },
  List: {
    id: 2062,
    buttons: {
      edit: "EDIT"
    }
  },
  Edit_Record: {
    id: 2063,
    buttons: {
      update: "UPDATE",
      cancel: "CANCEL"
    }
  }
}
// End of Master Module Structure

// PPC Module Structure
export const ppcScreenIds = {
  Order_Book: {
    id: 301,
    buttons: null
  },
  Caster_Schedule: {
    id: 302,
    buttons: null
  },
  LP_Schedule: {
    id: 303,
    buttons: null
  },
  Re_Application: {
    id: 304,
    buttons: null
  },
  Batch_Ack: {
    id: 305,
    buttons: null
  },
  SO_Modification: {
    id: 306,
    buttons: null
  },
  Batch_Attach: {
    id: 307,
    buttons: null
  },
  Sequence_Change: {
    id: 308,
    buttons: null
  },
  PO_Amend: {
    id: 309,
    buttons: null
  },
  Batch_transfer: {
    id: 310,
    buttons: null
  }
}

export const orderBookSectionIds = {
  List: {
    id: 3011,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF",
      refresh:"REFRESH"
    }
  }
}

export const casterSchSectionIds = {
  Action_Line: {
      id: 3021,
      buttons: {
        search: "SEARCH",
        proceed: "PROCEED"
      }
  },
  Schedule_Header: {
    id: 3022,
      buttons: {
        save: "SAVE",
        reset: "RESET"
      }
  },
  Schedule_List: {
    id: 3023,
      buttons: {
        save: "SAVE"
      }
  },
  Heat_Line: {
    id: 3024,
      buttons: {
        save: "SAVE"
      }
  },
  View_And_Insert: {
    id: 3026,
      buttons: {
        retrieve: "RETRIEVE",
        refresh: "REFRESH",
        insertOrder: "INSERT ORDER"
      }
  },
  Unplanned_Heat: {
    id: 3027,
      buttons: {
        retrieve: "RETRIEVE",
        refresh: "REFRESH",
        insertOrder: "INSERT ORDER"
      }
  }
}

export const lpSchSectionIds = {
  Action_Line: {
      id: 3031,
      buttons: {
        search: "SEARCH",
        proceed: "PROCEED"
      }
  },
  Schedule_Header: {
    id: 3032,
      buttons: {
        save: "SAVE",
        reset: "RESET"
      }
  },
  Schedule_List: {
    id: 3033,
      buttons: {
        save: "SAVE"
      }
  },
  View_And_Insert: {
    id: 3034,
      buttons: {
        retrieve: "RETRIEVE",
        refresh: "REFRESH",
        insertOrder: "INSERT ORDER"
      }
  },
  Unplanned_Heat: {
    id: 3035,
      buttons: {
        retrieve: "RETRIEVE",
        refresh: "REFRESH",
        insertOrder: "INSERT ORDER"
      }
  }
}

export const reApplicationSectionIds = {
  List: {
    id: 3041,
    buttons: {
      reapply: "REAPPLY"
    }
  },
  Reapply_Modal: { // section missing in DB
    id: 3042,
    buttons: {
      reapply: "REAPPLY",
      close: "CLOSE"
    }
  }
}

export const batchAckSectionIds = {
  Filter_Criteria: {
    id: 3051,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  List: {
    id: 3052,
    buttons: {
      acknowledge: "ACKNOWLEDGEMENT"
    }
  }
}

export const soModificationSectionIds = {
  Filter_Criteria: {
    id: 3061,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  List: {
    id: 3062,
    buttons: {
      save: "SAVE"
    }
  },
  Edit_History: {
    id: 3063,
    buttons: null
  }
}

export const batchAttachSectionIds = {
  Filter_Criteria: {
    id: 3071,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  List: {
    id: 3072,
    buttons: {
      attach: "ATTACH",
      unattach: "UNATTACH"
    }
  },
  Attach_Screen: {
    id: 3073,
    buttons: {
      save: "SAVE"
    }
  },
  Unattach_Screen: {
    id: 3074,
    buttons: {
      save: "SAVE"
    }
  }
}

export const seqChangeSectionIds = {
  Filter_Criteria: {
    id: 3081,
    buttons: {
      retrieve: "RETRIEVE"
    }
  },
  List: {
    id: 3082,
    buttons: {
      save: "SAVE"
    }
  }
}

export const poAmendSectionIds = {
  Filter_Criteria: {
    id: 3091,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  List: {
    id: 3092,
    buttons: {
      save: "SAVE"
    }
  }
}
export const BatchTransferPPCSectionIds = {
  filter_Criteria: {
    id: 3101,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  Batch_List: {
    id: 3102,
    buttons: {
      transfer: "TRANSFER"
    }
  }
}
// End of PPC Module Structure

// BRM Module Structure
export const BRMScreenIds = {
  Charging_Screen: {
      id: 401,
      buttons: null
    },
    DisCharging_Screen: {
      id: 402,
      buttons: null
    },
    Rolling_Screen: {
      id: 403,
      buttons: null
    },
    Bundle_Production: {
      id: 404,
      buttons: null
    },
    Production_confirmation: {
      id: 405,
      buttons: null
    },
    Batch_transfer: {
      id: 406,
      buttons: null
    },
    Additional_Production: {
      id: 407,
      buttons: null
    },
    Schedule_Consumption: {
      id: 408,
      buttons: null
    },
    Small_Batch_Production_Screen: {
      id: 409,
      buttons: null
    },
}
export const ChargingScreenSectionIds = {
  Filter_Criteria : {
      id: 4011,
      buttons: {
          retrieve: "RETRIEVE",
          reset: "RESET"
      }
  },
  PlantandShiftwise_List : {
    id: 4012,
    buttons: null
  },
    Charging_List : {
      id: 4013,
      buttons: {
          charge: "CHARGE",
          delete: "DELETE",
      }
    },
    dummyBatch_List : {
      id: 4014,
      buttons: {
          add: "ADD",
      }
    },
    Charge : {
      id: 4015,
      buttons:null
    },
  }
export const DisChargingScreenSectionIds = {
  Filter_Criteria: {
    id: 4021,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  PlantandShiftwise_List: {
    id: 4022,
    buttons: null
  },
  DisCharging_List: {
    id: 4023,
    buttons: {
      save: "SAVE",
    }
  },
  Discharge: {
    id: 4024,
    buttons: null
  },
}
export const RollingScreenSectionIds = {
  Filter_Criteria: {
    id: 4031,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  PlantandShiftwise_List: {
    id: 4032,
    buttons: null
  },
  Rolling_List: {
    id: 4033,
    buttons: {
      save: "SAVE",
    }
  },
  RollingCharge: {
    id: 4034,
    buttons: null
  },
}
export const BundleProdSectionIds = {
  Filter_Criteria: {
    id: 4041,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  PlantandShiftwise_List: {
    id: 4042,
    buttons: null
  },
  Bundle_List: {
    id: 4043,
    buttons: {
      generate: "GENERATE",
    }
  },
}
export const ProdConformationSectionIds = {
  ProductType_List: {
    id: 4051,
    buttons: {
      refresh: "REFRESH",
      release: "RELEASE"
    }
  }
}
export const BtachTransferSectionIds = {
  filter_Criteria: {
    id: 4061,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  Batch_List: {
    id: 4062,
    buttons: {
      transfer: "TRANSFER"
    }
  }
}
export const AdditionalProdSectionIds = {
  filter_Criteria: {
    id: 4071,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  Heat_List: {
    id: 4072,
    buttons: {
        save: "SAVE"
    }
},
  Batch_List: {
    id: 4073,
    buttons: {
        save: "SAVE"
    }
},
  }
export const ScheduleConsumptionSectionIds = {
  filter_Criteria: {
      id: 4081,
      buttons: {
          retrieve: "RETRIEVE",
          reset: "RESET"
      }
  },
    BatchStock_List: {
    id: 4082,
    buttons: {
      senttostock: "SENT TO STOCK",
      sentosap: "SENT TO SAP"
    }
  },
  }
// End of BRM Module Structure

// SMS operation declation
export const smsOperationScreenIds = {
  Conditioning_Screen: {
    id: 501,
    buttons: null
  },
  Small_Batch_Production_Screen: {
    id: 502,
    buttons: null
  },
  Qa_Hold_Screen: {
    id: 503,
    buttons: null
  },
  Qa_Release_Screen: {
    id: 504,
    buttons: null
  },
  Dummy_Batch_Screen: {
    id: 505,
    buttons: null
  },
  WtoF_Screen: {
    id: 506,
    buttons: null
  },
  Batch_transfer: {
    id: 507,
    buttons: null
  }
}

export const conditioningSectionIds ={
  Filter_Criteria : {
    id: 5011,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  Original_ProductDetails: {
    id: 5012,
    buttons: {
      addConditioning: "ADD CONDITIONING"
    }
  },
  Plan_Details: {
    id: 5013,
    buttons: null
  },
  Balance_Details: {
    id: 5014,
    buttons: null
  },
  conditioning_History: {
    id: 5015,
    buttons: null
  },
  conditioning_Details: {
    id: 5016,
    buttons: {
      finalize: "FINALIZE",
      insert: "INSERT",
      reset: "RESET",
      save: "SAVE"
    }
  }
}

export const smallBatchProductionSectionIds = {
  Filter_Criteria : {
    id: 5021,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  batchStock_Details: {
    id:5022,
    buttons:null
  },
  small_batch_prod: {
    id:5023,
    buttons:{
      add:"ADD",
      save:"SAVE"
    }
  }
}

//new
export const smallBatchBrmProductionSectionIds = {
  Filter_Criteria : {
    id: 4091,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  batchStock_Details: {
    id:4092,
    buttons:null
  },
  small_batch_prod: {
    id:4093,
    buttons:{
      add:"ADD",
      save:"SAVE"
    }
  }
}
export const qAholdSectionIds = {
  Filter_Criteria : {
    id: 5031,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  BatchDetails_hold:{
    id: 5032,
    buttons: {
        hold: "HOLD",
        release: "RELEASE"
    }
  }
}

export const qAReleaseSectionIds ={
  Filter_Criteria : {
    id: 5041,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  BatchDetails_Release:{
    id: 5042,
    buttons: {
      save:"SAVE",
        hold: "HOLD",
        release: "RELEASE"
    }
  }
}

export const dummyBatchSectionIds ={
  Filter_Criteria : {
    id: 5051,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  BatchDetails: {
    id: 5052,
    buttons: {
      save:"SAVE"
    }
  }
}

export const wTofSectionIds = {
  Filter_Criteria: {
    id: 5061,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  BatchDetails: {
    id: 5062,
    buttons: {
      dispatch:"DISPATCH"
    }
  }
}
export const BatchTransferSectionIds = {
  filter_Criteria: {
    id: 5071,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  Batch_List: {
    id: 5072,
    buttons: {
      transfer: "TRANSFER"
    }
  }
}
// End Of SMS operation Module Structure

// BRM QA Declation
export const brmQAScreenIds = {
  STICKER_PRINTING_SCRREN: {
    id: 601,
    buttons: null
  },
  SAMPLE_MECH_PROPERTY_SCREEN: {
    id: 602,
    buttons: null
  },
  BUNDLE_QUALITY_CLEARANCE_SCREEN: {
    id: 603,
    buttons: null
  },
  TPI_HOLD_RELEASE_SCREEN: {
    id: 604,
    buttons: null
  },
  FINAL_MATERIAL_BATCH_UPDATE_SCREEN: {
    id: 605,
    buttons: null
  },
  NDT: {
    id: 606,
    buttons: null
  }
}

export const STICKER_PRINTING_SCRREN_SectionIds = {
  Filter_Criteria: {
    id: 6011,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  List: {
    id: 6012,
    buttons: {
        print:"PRINT",
        close: "CLOSE"
    }
  }
}

export const SAMPLE_MECH_PROPERTY_SCREEN_SectionIds = {
  Filter_Criteria: {
    id: 6021,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  List: {
    id: 6022,
    buttons: null
  },
  MechProperties:{
    id: 6023,
    buttons: {
        save: "SAVE",
        mapMaterial: "MAP MATERIAL"
    }
  }
}

export const BUNDLE_QUALITY_CLEARANCE_SCREEN_SectionIds = {
  List: {
    id: 6031,
    buttons: {
      save:"SAVE"
    }
  }
}

export const TPI_HOLD_RELEASE_SCREEN_SectionIds = {
  Filter_Criteria: {
    id: 6041,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  List: {
    id: 6042,
    buttons: {
      save:"SAVE",
      release:"RELEASE"
    }
  }
}

export const  FINAL_MATERIAL_BATCH_UPDATE_SCREEN_SectionIds ={
  Filter_Criteria : {
    id: 6051,
    buttons: {
        retrive: "RETRIEVE",
        reset: "RESET"
    }
  },
  List: {
    id: 6052,
    buttons: {
      save:"SAVE"
    }
  }
}

export const NDT_SectionIds = {
  Filter_Criteria: {
    id: 6061,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  NDT_Details: {
    id: 6062,
    buttons: null
  },
  NDT_Test_Details: {
    id: 6063,
    buttons: {
      save: "SAVE"
    }
  }
}
// End of BRM QA declartion

// Interface Module Structure
export const InterfaceScreenIds = {
   LP_Confirmation: {
      id: 701,
      buttons: null
    },
    ReceiveSalesOrder: {
      id: 702,
      buttons: null
    },
    SO_modification: {
      id: 703,
      buttons: null
    },
    BtachQualityClearance: {
      id: 704,
      buttons: null
    },
    BatchConditioning: {
      id: 705,
      buttons: null
    },
    LocationChange: {
      id: 706,
      buttons: null
    },
    HeatChemistry: {
      id: 707,
      buttons: null
    },
    BatchUpdate: {
      id: 708,
      buttons: null
    },
    FGBatchSwap: {
      id: 709,
      buttons: null
    },
    BatchDispatch: {
      id: 710,
      buttons: null
    },
    ProcureBatch: {
      id: 711,
      buttons: null
    },
    LP_SMS: {
      id: 712,
      buttons: null
    },
    SMS_LP: {
      id: 716,
      buttons: null
    },
    SMS_Prod: {
      id: 713,
      buttons: null
    },
    LP_QA: {
      id: 714,
      buttons: null
    }
}

export const LPConfirmationSectionIds = {
  Filter_Criteria: {
    id: 7011,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  LPConfirmation_List: {
    id: 7012,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD",
      repushInterface: "RE-PUSH INTERFACE",
    }
  },
}

export const ReceiveSalesSectionIds = {
  ReceiveSalesFilter_Criteria: {
    id: 7021,
    buttons: {
      retrieve: 'RETRIEVE',
      reset: 'RESET'
    }
  },
  HeatFilter_Criteria: {
    id: 7022,
    buttons: {
      runInterface: 'RUN INTERFACE',
      download: 'DOWNLOAD'
    }
  },
  Attri_save: {
    id: 7023,
    buttons: {
      save: 'SAVE'
    }
  },
  Mech_Save: {
    id: 7024,
    buttons: {
      save: 'SAVE'
    }
  }
};   //iska karana hai

export const SOModificationSectionIds = {
  Filter_Criteria: {
    id: 7031,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  SOModification_List: {
    id: 7032,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD"
    }
  },
}
export const BtachQualitySectionIds = {
  Filter_Criteria: {
    id: 7041,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  BatchQqality_List: {
    id: 7042,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD",
      repushInterface: "RE-PUSH INTERFACE",
    }
  },
}
export const BtachConditioningSectionIds = {
  Filter_Criteria: {
    id: 7051,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  BatchConditioning_List: {
    id: 7052,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD",
      repushInterface: "RE-PUSH INTERFACE",
    }
  },
}

export const  LocationChangeSectionsIds = {
  Filter_Criteria : {
    id: 7061,
    buttons: {
        retrieve: "RETRIEVE",
        reset: "RESET"
    }
  },
  List : {
    id: 7062,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD",
      repushInterface: "RE-PUSH INTERFACE",
    }
  }
}

export const  HeatChemInterfaceSectionIds = {
  Filter_Criteria : {
    id: 7071,
    buttons: {
        retrieve: "RETRIEVE",
        reset: "RESET"
    }
  },
  List : {
    id: 7072,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD",
      repushInterface: "RE-PUSH INTERFACE",
    }
  }
}

export const  BatchUpdateInterfaceSectionIds = {
  Filter_Criteria : {
    id: 7081,
    buttons: {
        retrieve: "RETRIEVE",
        reset: "RESET"
    }
  },
  List : {
    id: 7082,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD"
    }
  }
}

export const  FGBatchSwapInterfaceSectionsIds = {
  Filter_Criteria : {
    id: 7091,
    buttons: {
        retrieve: "RETRIEVE",
        reset: "RESET"
    }
  },
  List : {
    id: 7092,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD"
    }
  }
}

export const  BatchDispatchInterfaceSectionsIds = {
  Filter_Criteria : {
    id: 7101,
    buttons: {
        retrieve: "RETRIEVE",
        reset: "RESET"
    }
  },
  List : {
    id: 7102,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD"
    }
  }
}

export const ProcureBatchSectionIds = {
  Filter_Criteria: {
    id: 7111,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  ProcureBatch_List: {
    id: 7112,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD"
    }
  },
}
export const LP_SMSSectionIds = {
  Filter_Criteria: {
    id: 7121,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  LPSMS_List: {
    id: 7122,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD"
    }
  },
}

//new compnent
export const SMS_LPSectionIds = {
  Filter_Criteria: {
    id: 7161,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  SMSLP_List: {
    id: 7162,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD"
    }
  },
}
export const SMS_ProdSectionIds = {
  Filter_Criteria: {
    id: 7131,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  SMSProd_List: {
    id: 7132,
    buttons: {
      runInterface: "RUN INTERFACE",
      download: "DOWNLOAD"
    }
  },
}

export const LP_QA_SectionIds = {
  Filter_Criteria: {
    id: 7141,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  List: {
    id: 7142,
    buttons: {
      download: "DOWNLOAD",
      repushInterface: "RE-PUSH INTERFACE",
    }
  },
}
// End of Interface Module Structure

// Start of Report Module Structure
export const ReportsScreenIds = {
  Billet_Report: {
    id: 801,
    buttons: null
  },
  FurnanceResidence_Report: {
    id: 802,
    buttons: null
  },
  SizeWise_Report: {
    id: 803,
    buttons: null
  },
  LPProdDetails_Report: {
    id: 804,
    buttons: null
  },
  HeatWise_Report: {
    id: 805,
    buttons: null
  },
  HotOut_Report: {
    id: 806,
    buttons: null
  },
  LPProdSummary_Report: {
    id: 807,
    buttons: null
  },
  LPStock_Report: {
    id: 808,
    buttons: null
  },
  MechTest_Report: {
    id: 809,
    buttons: null
  },
  LPRolling_Report: {
    id: 810,
    buttons: null
  },
}
export const Billet_SectionIds = {
  Filter_Criteria: {
    id: 8011,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  Consumption_List: {
    id: 8012,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF"
    }
  },
}
export const FurnanceResidence_SectionIds = {
  Filter_Criteria: {
    id: 8021,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  Residence_List: {
    id: 8022,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF"
    }
  },
}
export const SizeWise_SectionIds = {
  Filter_Criteria: {
    id: 8031,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  SizeWise_List: {
    id: 8032,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF"
    }
  },
}
export const LPProdDetails_SectionIds = {
  Filter_Criteria: {
    id: 8041,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  LPProd_List: {
    id: 8042,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF"
    }
  },
}
export const HeatWise_SectionIds = {
  Filter_Criteria: {
    id: 8051,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  HeatWise_List: {
    id: 8052,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF"
    }
  },
}
export const HotOut_SectionIds = {
  Filter_Criteria: {
    id: 8061,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  HotOut_List: {
    id: 8062,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF"
    }
  },
}
export const LPProdSummary_SectionIds = {
  Filter_Criteria: {
    id: 8071,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  LPProdSummary_List: {
    id: 8072,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF"
    }
  },
}
export const LPStock_SectionIds = {
  Filter_Criteria: {
    id: 8081,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  LPStock_List: {
    id: 8082,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF"
    }
  },
}
export const MechTest_SectionIds = {
  Filter_Criteria: {
    id: 8091,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  MechTest_List: {
    id: 8092,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF"
    }
  },
}
export const LPRolling_SectionIds = {
  Filter_Criteria: {
    id: 8101,
    buttons: {
      retrieve: "RETRIEVE",
      reset: "RESET"
    }
  },
  LPRolling_List: {
    id: 8102,
    buttons: {
      excel: "EXCEL",
      pdf: "PDF"
    }
  },
}
// End of Report Module Structure

// Main MENU structure
export class MENU {

  public getMenuList() {
    return this.menuList;
  }

  public menuList = [
    {
      "name": "Dashboard",
      "id": null,
      "path": "/pages/dashboard",
      "icon": "fas fa-user",
      "active": false,
      "permission": true,
      "tooltip": "Dashboard",
      "showDropdown": false,
      "subMenuItems": null
    },
    {
      "name": "User",
      "id": moduleIds.User_Management,
      "path": "/pages/user-mgmt",
      "icon": "fas fa-user",
      "active": false,
      "permission": false,
      "tooltip": "User Management",
      "showDropdown": false,
      "subMenuItems": [
        {
          "name": "User Master",
          "path": "/pages/user-mgmt/userMaster",
          "active": false,
          "permission": false,
          "id": userMgmtScreenIds.User_Master.id
        },
        {
          "name": "Screen Master",
          "path": "/pages/user-mgmt/screenMaster",
          "active": false,
          "permission": false,
          "id": userMgmtScreenIds.Screen_Master.id
        },
        {
          "name": "Role Screen Mapping",
          "path": "/pages/user-mgmt/roleScreenMapping",
          "active": false,
          "permission": false,
          "id": userMgmtScreenIds.Role_Screen_Mapping.id
        }
      ]
    },
    {
      "name": "Master",
      "id": moduleIds.Master,
      "path": "/pages/master",
      "icon": "fas fa-list-alt",
      "active": false,
      "permission": false,
      "tooltip": "Master",
      "showDropdown": false,
      "subMenuItems": [
        {
          "name": "Product Attribute Master",
          "path": "/pages/master/materialMaster",
          "active": false,
          "permission": false,
          id: masterMgmtScreenIds.Product_Attribute_Master.id
        },
        {
          "name": "Property Master",
          "path": "/pages/master/propertyMaster",
          "active": false,
          "permission": false,
          id: masterMgmtScreenIds.Property_Master.id
        },
        {
          "name": "Grade Master",
          "path": "/pages/master/gradeMaster",
          "active": false,
          "permission": false,
          id: masterMgmtScreenIds.Grade_Master.id
        },
        {
          "name": "Gradewise Density Master",
          "path": "/pages/master/gradewiseDensityMaster",
          "active": false,
          "permission": false,
          id: masterMgmtScreenIds.Gradewise_Density_Master.id
        },
        {
          "name": "LOV Master",
          "path": "/pages/master/LovMaster",
          "active": false,
          "permission": false,
          id: masterMgmtScreenIds.LOV_Master.id
        },
        {
          "name": "Yard Master",
          "path": "/pages/master/YardMaster",
          "active": false,
          "permission": false,
          id: masterMgmtScreenIds.Yard_Master.id
        }
      ]
    },
    {
      "name": "PPC",
      "id": moduleIds.PPC,
      "path": "/pages/ppc",
      "icon": "fas fa-clipboard-list",
      "active": false,
      "permission": false,
      "tooltip": "PPC",
      "showDropdown": false,
      "subMenuItems": [
        {
          "name": "Order Book",
          "path": "/pages/ppc/orderBook",
          "active": false,
          "permission": false,
          id: ppcScreenIds.Order_Book.id
        },
        {
          "name": "Caster Schedule",
          "path": "/pages/ppc/casterSchedule",
          "active": false,
          "permission": false,
          id: ppcScreenIds.Caster_Schedule.id
        },
        {
          "name": "Long Product Schedule",
          "path": "/pages/ppc/longProductSchedule",
          "active": false,
          "permission": false,
          id: ppcScreenIds.LP_Schedule.id
        },
        {
          "name": "Re-application",
          "path": "/pages/ppc/reApplication",
          "active": false,
          "permission": false,
          id: ppcScreenIds.Re_Application.id
        },
        {
          "name": "Batch Acknowledge",
          "path": "/pages/ppc/billetAcknowledge",
          "active": false,
          "permission": false,
          id: ppcScreenIds.Batch_Ack.id
        },
        {
          "name": "Sales Order Modification",
          "path": "/pages/ppc/soModification",
          "active": false,
          "permission": false,
          id: ppcScreenIds.SO_Modification.id
        },
        {
          "name": "Batch Attach",
          "path": "/pages/ppc/billetAttach",
          "active": false,
          "permission": false,
          id: ppcScreenIds.Batch_Attach.id
        },
        {
          "name": "Sequence Change",
          "path": "/pages/ppc/sequenceChange",
          "active": false,
          "permission": false,
          id: ppcScreenIds.Sequence_Change.id
        },
        {
          "name": "PO Amend",
          "path": "/pages/ppc/poAmend",
          "active": false,
          "permission": false,
          id: ppcScreenIds.PO_Amend.id
        },
        {
          "name": "Batch Transfer",
          "path": "/pages/ppc/batchTransfer",
          "active": false,
          "permission": false,
          "id": ppcScreenIds.Batch_transfer.id
        }
      ]
    },
    {
      "name": "BRM",
      "id": moduleIds.BRM,
      "path": "/pages/brm",
      "icon": "fas fa-list-alt",
      "active": false,
      "permission": false,
      "tooltip": "BRM",
      "showDropdown": false,
      "subMenuItems": [
        {
          "name": "Charging Screen",
          "path": "/pages/brm/chargingScreen",
          "active": false,
          "permission": false,
          id: BRMScreenIds.Charging_Screen.id
        },
        {
          "name": "Discharging Screen",
          "path": "/pages/brm/dischargingScreen",
          "active": false,
          "permission": false,
          id: BRMScreenIds.DisCharging_Screen.id
        },
        {
          "name": "Rolling Screen",
          "path": "/pages/brm/rollingScreen",
          "active": false,
          "permission": false,
          id: BRMScreenIds.Rolling_Screen.id
        },
        {
          "name": "Bundle Production",
          "path": "/pages/brm/bundleProduction",
          "active": false,
          "permission": false,
          id: BRMScreenIds.Bundle_Production.id
        },
        {
          "name": "Production Confirmation",
          "path": "/pages/brm/productionConfirmation",
          "active": false,
          "permission": false,
          id: BRMScreenIds.Production_confirmation.id
        },
        {
          "name": "Batch Transfer",
          "path": "/pages/brm/batchTransfer",
          "active": false,
          "permission": false,
          id: BRMScreenIds.Batch_transfer.id
        },
        {
          "name": "Additional Production",
          "path": "/pages/brm/addiBundleProduction",
          "active": false,
          "permission": false,
          id: BRMScreenIds.Additional_Production.id
        },
        {
          "name": "Schedule Consumption",
          "path": "/pages/brm/scheduleConsumption",
          "active": false,
          "permission": false,
          id: BRMScreenIds.Schedule_Consumption.id
        },
        {
          "name": "Small Batch Production",
          "path": "/pages/sms/smallBatchProduction",
          "active": false,
          "permission": false,
          "id": BRMScreenIds.Small_Batch_Production_Screen.id,
        }
      ]
    },
    {
      "name": "SMS Operation",
      "id": moduleIds.SMS_Operation,
      "path": "/pages/sms",
      "icon": "fas fa-list-alt",
      "active": false,
      "permission": false,
      "tooltip": "SMS",
      "showDropdown": false,
      "subMenuItems": [
        // {
        //   "name": "Inspection Screen",
        //   "path": "/pages/sms/inspectionScreen",
        //   "active": false,
        //   "permission": false
        // },
        {
          "name": "Conditioning Screen",
          "path": "/pages/sms/conditioningScreen",
          "active": false,
          "permission": false,
          "id": smsOperationScreenIds.Conditioning_Screen.id,
        },
        {
          "name": "Small Batch Production",
          "path": "/pages/sms/smallBatchProduction",
          "active": false,
          "permission": false,
          "id": smsOperationScreenIds.Small_Batch_Production_Screen.id,
        },
        {
          "name": "QA Hold",
          "path": "/pages/sms/QAHold",
          "active": false,
          "permission": false,
          "id": smsOperationScreenIds.Qa_Hold_Screen.id,
        },
        {
          "name": "QA Release",
          "path": "/pages/sms/QARelease",
          "active": false,
          "permission": false,
          "id": smsOperationScreenIds.Qa_Release_Screen.id,
        },

        {
          "name": "Dummy Batch",
          "path": "/pages/sms/dummyBatch",
          "active": false,
          "permission": false,
          "id": smsOperationScreenIds.Dummy_Batch_Screen.id,
        },
        {
          "name": "W To F",
          "path": "/pages/sms/wtof",
          "active": false,
          "permission": false,
          "id": smsOperationScreenIds.WtoF_Screen.id
        },
        {
          "name": "Batch Transfer",
          "path": "/pages/sms/batchTransfer",
          "active": false,
          "permission": false,
          "id": smsOperationScreenIds.Batch_transfer.id
        }
      ]
    },
    {
      "name": "BRM QA",
      "id": moduleIds.BRM_QA,
      "path": "/pages/brm-qa",
      "icon": "fas fa-list-alt",
      "active": false,
      "permission": false,
      "tooltip": "SMS",
      "showDropdown": false,
      "subMenuItems": [
        // {
        //   "name": "Bundle Production",
        //   "path": "/pages/brm-qa/bundleProduction",
        //   "active": false,
        //   "permission": false
        // },
        // {
        //   "name": "Production Confirmation",
        //   "path": "/pages/brm-qa/productionConfirmation",
        //   "active": false,
        //   "permission": false
        // },
        {
          "name": "Sticker Printing",
          "path": "/pages/brm-qa/stickerPrinting",
          "active": false,
          "permission": false,
          "id": brmQAScreenIds.STICKER_PRINTING_SCRREN.id
        },
        {
          "name": "Sample Mech Property",
          "path": "/pages/brm-qa/sampleMechProperty",
          "active": false,
          "permission": false,
          "id": brmQAScreenIds.SAMPLE_MECH_PROPERTY_SCREEN.id
        },
        {
          "name": "NDT",
          "path": "/pages/brm-qa/ndt",
          "active": false,
          "permission": true,
          "id": brmQAScreenIds.NDT.id
        },
        {
          "name": "Bundle Quality Clearance",
          "path": "/pages/brm-qa/bundleQualityClearance",
          "active": false,
          "permission": false,
          "id": brmQAScreenIds.BUNDLE_QUALITY_CLEARANCE_SCREEN.id
        },

        {
          "name": "TPI Hold Release",
          "path": "/pages/brm-qa/tpiHoldRelease",
          "active": false,
          "permission": false,
          "id": brmQAScreenIds.TPI_HOLD_RELEASE_SCREEN.id
        },
        {
          "name": "Final Material Batch Update",
          "path": "/pages/brm-qa/finalMaterialBatchUpdate",
          "active": false,
          "permission": false,
          "id": brmQAScreenIds.FINAL_MATERIAL_BATCH_UPDATE_SCREEN.id
        }
      ]
    },
    {
      "name": "Interface",
      "id": moduleIds.Interface,
      "path": "/pages/interface",
      "icon": "fas fa-list-alt",
      "active": false,
      "permission": false,
      "tooltip": "Interface",
      "showDropdown": false,
      "subMenuItems": [
        {
          "name": "FROM SMS MES",
          "path": "/pages/interface/SmsLp",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.SMS_LP.id,
          
        },
        {
          "name": "TO SMS MES",
          "path": "/pages/interface/LpSms",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.LP_SMS.id,
          
        },
        {
          "name": "Receive Sales Order",
          "path": "/pages/interface/receiveSalesOrder",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.ReceiveSalesOrder.id,
          "type":'FROMSAP'
        },
        {
          "name": "SO Modification",
          "path": "/pages/interface/soModification",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.SO_modification.id,
          "type":'FROMSAP'
        },
        {
          "name": "FG Batch Swapping Interface",
          "path": "/pages/interface/batchSwapping",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.FGBatchSwap.id,
          "type":'FROMSAP'
        },
        {
          "name": "Batch Dispatch Interface",
          "path": "/pages/interface/BatchDispatch",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.BatchDispatch.id,
          "type":'FROMSAP'
        },
        {
          "name": "Procure Batch",
          "path": "/pages/interface/procurBatch",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.ProcureBatch.id,
          "type":'FROMSAP'
        },
        {
          "name": "SMS PROD Interface",
          "path": "/pages/interface/SmsProd",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.SMS_Prod.id,
          "type":'FROMSAP'
        },
        {
          //"name": "Batch Quality Clearance",
          "name": "SMS-QA Interface",
          "path": "/pages/interface/batchQualityClearance",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.BtachQualityClearance.id,
          "type":'TOSAP'
        },
        {
          "name": "Batch Conditioning",
          "path": "/pages/interface/batchConditioning",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.BatchConditioning.id,
          "type":'TOSAP'
        },        
        {
          "name": "Heat Chemistry Interface",
          "path": "/pages/interface/heatChemistry",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.HeatChemistry.id,
          "type":'TOSAP'
        },
        {
          "name": "Batch Update Interface",
          "path": "/pages/interface/batchUpdate",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.BatchUpdate.id,
          "type":'TOSAP'
        },
        {
          "name": "Location Change ",
          "path": "/pages/interface/locationChange",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.LocationChange.id,
          "type":'TOSAP'
        },
        {
          "name": "LP Confirmation",
          "path": "/pages/interface/lpProdConfirmation",
          "active": false,
          "permission": false,
          "id": InterfaceScreenIds.LP_Confirmation.id,
          "type":'TOSAP'
        },
        {
          "name": "LP-QA Interface",
          "path": "/pages/interface/lpQA",
          "active": false,
          "permission": true,
          "id": InterfaceScreenIds.LP_SMS.id,
          "type":'TOSAP'
        },
        // {
        //   "name": "LP-SMS Interface",
        //   "path": "/pages/interface/LpSms",
        //   "active": false,
        //   "permission": true,
        //   "id": InterfaceScreenIds.LP_SMS.id,
        //   "type":'TOSAP'
        // },
        // {
        //   "name": "SMS-LP Interface",
        //   "path": "/pages/interface/SmsLp",
        //   "active": false,
        //   "permission": true,
        //   "id": InterfaceScreenIds.SMS_LP.id,
        //   "type":'TOSAP'
        // },
      ]
    },

    {
      "name": "Reports",
      "id": moduleIds.Reports,
      "path": "/pages/reports",
      "icon": "fas fa-list-alt",
      "active": false,
      "permission": false,
      "tooltip": "Reports",
      "showDropdown": false,
      "subMenuItems": []
    }
  ]
}
